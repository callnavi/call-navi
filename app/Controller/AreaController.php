<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
App::uses('CakeText', 'Utility');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class AreaController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array('Area');
	public $helpers = array('Paginator','Html');
    public $components = array('Session','CloudSearch', 'Util','Paginator');
    public $paginate = array();
/**
 * Displays a view
 *
 * @return void
 * @throws NotFoundException When the view file could not be found
 *	or MissingViewException in debug mode.
 */

	public function getGroup()
	{
		$group = Configure::read('webconfig.search_area_group');
		echo json_encode($group);
		die;
	}

	public function getProvinceByGroup($group) 
	{
		$provinces = $this->Area->getProvinceByGroup($group);
		echo json_encode($provinces);
		die;
	}
	
	//get city and district 
	public function getAreaByParentIDAndLevel($parentId, $level) 
	{
		$area = $this->Area->getAreaByParentIDAndLevel($parentId, $level);
		echo json_encode($area);
		die;
	}
}
