<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
App::uses('CakeText', 'Utility');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class KeisaiguideController extends AppController
{

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array('Contact');
    public $helpers = array('Paginator','Html','Form');
    public $components = array('Session', 'Cookie', 'Util','Sendmail','ContactFun');
    public $paginate = array();


    public function index() 
    {

        //14 August 2017 validation if mobile or not
       	if($this->is_mobile)
		{
           // pr($this->is_mobile);die;
			$this->layout = 'keisaiguide_layout-sp';
			$this->render('index_sp');
		}else
        {
            $this->layout = 'keisaiguide_layout';
            
        }

    }
	
    public function contact() {
        //setting data session for index page
        $post = $this->ContactFun->GetIndexSessionData();

        //setting data session for error 
        $this->set('post', $post);
         if($this->Session->check('contact_date_error'))
        {
            $date_error = $this->Session->read('contact_date_error');
            $this->set('date_error', $date_error);    
         }
        
        //display layout mobile or pc 
        if($this->is_mobile)
        {
            $this->layout = 'keisaiguide_layout-sp';
            $this->render('contact_v2_sp');
        }else{
            $this->layout = 'keisaiguide_layout';
            $this->render('contact_v2');           
        }

    }

    //Index Routing For Confirm 
    public function confirm() {
        //setting post data to session
        if($this->request->is('post'))
        {
            $data = $this->request->data;
            $this->set('data', $data);
            
            $this->Session->write('contact_data',$data);
            
            
        }else{
            $this->redirect('/keisaiguide/contact/');
        }
        
        //checking session data is errir or not
        if($this->Session->check('contact_data')){
            $data = $this->Session->read('contact_data');
            
            $error =  $this->ContactFun->CheckValidation($data);
            if($error <> ""){
                $this->Session->write('contact_date_error',$data);
                $this->redirect('/keisaiguide/contact');                
            }
            
        }
                  
        //display layout mobile or pc 
        if($this->is_mobile)
        {

            $this->layout = 'keisaiguide_layout-sp';
            $this->render('confirm_v2-sp');
        }else{
            $this->layout = 'keisaiguide_layout';
            $this->render('confirm_v2');           
        }

    }


    //Index Routing For Confirm 
    public function thanks() {
        $rs = false;

        //setting validation for sent mail
        if(!isset($this->request->query['testmode'])){
            $testmode = false;
        }else{
            $testmode = true;
        }

        //processing data for contact
        if($this->Session->check('contact_data')){
            $data = $this->Session->read('contact_data');
            if($testmode == true){
                $result = $this->Contact->add_new_data_testmode($data);
                $result = $this->ContactFun->sent_mail_testmode($data);
            }else{
                $result = $this->Contact->add_new_data($data);
                $result = $this->ContactFun->sent_mail_main($data);
            }

        }else{
            $this->redirect('/keisaiguide/contact/');
        }
        
        //checking result corrent or not
        if($result == false){
            $this->redirect('/keisaiguide/contact/');
        }

        //delete session if tesmode is false
        if($testmode == false){
            $this->Session->delete('contact_data');
        }

        if($this->is_mobile)
        {
            $this->layout = 'keisaiguide_layout-sp';
            $this->render('thanks_v2-sp');
        }else{
            $this->layout = 'keisaiguide_layout';
            $this->render('thanks_v2');           
        }

    }



}