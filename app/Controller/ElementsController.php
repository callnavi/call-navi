<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
App::uses('CakeText', 'Utility');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ElementsController extends AppController
{

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array('ContentsEmail', 'Contents', 'BlogsCategory', 'Blogs', 'CareerOpportunity', 'PickUp', 'Keywords');
    public $helpers = array('Paginator', 'Html');
    public $components = array('Session', 'CloudSearch');
    public $paginate = array();

    /**
     * Displays a view
     *
     * @return void
     * @throws NotFoundException When the view file could not be found
     *    or MissingViewException in debug mode.
     */
    public function index()
    {

        $this->paginate = array(
            'fields' => array('created', 'featured_picture', 'id', 'Subtitle', 'view', 'Title'),
            'limit' => 20,
            'order' => array(
                'ContentsEmail.created' => 'asc'
            )
        );
        $val = $this->paginate('ContentsEmail');
        //pr($val);die;
        $this->set("list", $val);

    }

    public function slider($isSp = null)
    {
        $val = empty($isSp) ? $this->ContentsEmail->getRankList(3) : $this->ContentsEmail->getRankList(5);
        return $val;
    }

    public function content_similar_aticles($category = null)
    {
        $size = 3;
        $this->paginate = array(
            'fields' => array('id','secret_job_id', 'view', 'priority', 'Contents.status', 'site_id', 'province_id', 'url', 'category', 'title_hint', 'title','content', 'image', 'category_alias',
                'title_alias', 'Contents.created', 'category.category',
                'TIMESTAMPDIFF(hour, Contents.created, CURDATE() ) AS totalHour'),
            'conditions' => array('Contents.status' => 1, 'Contents.category_alias' => $category),
            'joins' => array(
                array(
                    'table' => 'category',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'category.category_alias = Contents.category_alias',
                    ),
                ),
            ),
            'limit' => $size,
            'order' => 'rand()'
        );


        $val = $this->paginate('Contents');
        //pr($val);die;
        return $val;
    }

    public function news_similar_aticles($category = null)
    {
        $size = 3;
        $val = $this->ContentsEmail->getList(1, $size, $category, 1);
        //pr($val);die;
        return $val;
    }


    public function Blogs_info($title_alias = null)
    {

        $item = $this->BlogsCategory->getBlogCatInfo($title_alias);
        return $item;
    }

    public function list_blog_view($alias = null)
    {
        $condition = "";
        if (!empty($alias)) {
            $condition = "category_alias <>'" . $alias . "'";
        }
        $item = $this->BlogsCategory->find('all', array(
            'fields' => array('cat_id', 'title', 'pic', 'avatar', 'short', 'category_alias'),
            'conditions' => array(
                $condition
            ),
            'limit' => 6,
            'order' => array(
                'BlogsCategory.created' => 'DESC'
            )
        ));
        return $item;


    }


    public function list_job_search($tag = null)
    {
        $params = array('keyword' => $tag);
        $page = 1;
        $size = 3;
        $data = $this->CloudSearch->search($params, $page, $size);
        return $data;
    }


    public function ranking()
    {
        $data = $this->Contents->viewRankingNewsContentsBlogs(3, 5);
        return $data;
    }

    public function rankingSP()
    {
        $data = $this->Contents->viewRankingNewsContentsBlogs(2, 5);
        return $data;
    }

    public function secret_slider()
    {
        $data = $this->CareerOpportunity->getSecretsForSlider(5);
        return $data;
    }

    public function secret_top_banner()
    {
        $data = $this->CareerOpportunity->getSecretsForSlider(3);
        return $data;
    }

    public function search_total()
    {
        //TODO: search_total
        $searchResult = $this->CloudSearch->search([], 1, 1);
        return $searchResult[1];
    }

    public function secret_total()
    {
        //TODO: secret_total
        $secret_total = $this->CareerOpportunity->countActiveJob();
        return $secret_total;
    }

    public function getPickUp()
    {
        //TODO: getPickUp

        $data = $this->PickUp->getPickUp();

        if ($data) {
            return $data;
        }

        return null;
    }


    public function getSecretRelatedJob()
    {
        $params = $this->request->query;
        unset($params['hotkey']);
        return $this->CareerOpportunity->getListSecret(1, 2, $params);
    }

    public function getArticleRelatedJob()
    {
        $params = $this->request->params['pass'];

        return $this->Contents->viewNewsContentsBlogs(1, 2, $params);
    }


    public function getHotKeys()
    {
        $hotkey_data = $this->Keywords->getHotkey();
        return $hotkey_data;
    }


}
