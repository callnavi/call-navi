<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
App::uses('CakeText', 'Utility', 'Area');
App::import('model', 'Contents');
App::import('model', 'CorporatePrs');
App::import('model', 'Categories');
/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class SecretController extends AppController
{

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $components = array('Session', 'CloudSearch', 'Cookie', 'Search', 'SearchRoute');
    public $uses = array('CareerOpportunity', 'CorporatePrs', 'Keywords');

    /**
     * Displays a view
     *
     * @return void
     * @throws NotFoundException When the view file could not be found
     *    or MissingViewException in debug mode.
     */
    public function index()
    {
        //declare
        $size = 20;
        $render = 'index';
        $params = $this->request->query;

        //save list key area to view
        if (isset($params['area'])) {
            $params['list_key_area'] = $params['area'];
        }
        $page = isset($_GET["page"]) ? intval($_GET["page"]) : 1;
        $hotkey_data = $this->Keywords->getHotkey();

        if ($this->is_mobile) {
            $size = 12;
            $this->layout = 'single_column_sp';
            $render = 'index-sp';
        } else {
            $this->layout = 'single_column';
        }

        if ($params) {
            $params = $this->Search->convertParamsKeyToText($params);
        }

        //TODO: get secret jobs
        $data = $this->_secret($params, $page, $size);
        $val = $data['data'];
        $total = $data['total'];
        $pageCount = ceil($total / $size);
        $area_data = Configure::read('webconfig.area_data');
        $employment_data = Configure::read('webconfig.employment_data');

        $em = '';
        if (isset($params['data_em'])) {
            if ($params['data_em']) {
                $em = $employment_data[$params['data_em']];
            }
        }
        $params['em'] = $em;

        $this->set('data', $val);
        $this->set('size', $size);
        $this->set('params', $params);
        $this->set('search_total', $total);
        $this->set("currentPage", $page);
        $this->set('area_data', $area_data);
        $this->set('employment_data', $employment_data);
        $this->set('hotkeyList', $hotkey_data);
        $this->set('currentKeyWord', $this->newGenCurrentKeyWord($params));

        $this->set("pageCount", $pageCount);

        $this->setValueOnView($params, $area_data, $employment_data, $hotkey_data);
        $this->render($render);

    }


    public function countcondition()
    {
        //TODO: countcondition
        $params = $this->request->query;
        if(!empty($params['area']))
        {
            $params['list_key_area'] = $params['area'];
        }
        $params = $this->Search->convertParamsKeyToText($params);

        $customPassedArgs = $this->SearchRoute->customPassedArgs($params);
        $this->SearchRoute->matchUrl($customPassedArgs);
        $params = $this->SearchRoute->getPars();
        
        $data = $this->CareerOpportunity->getListSecret(1, 0, $params);
        $total = $data['total'];

        $this->layout = null;
        $this->render(false);
        echo $total;
    }


    public function ajaxSearch()
    {
        $size = 20;
        $params = $this->request->query;
        $page = isset($this->request->query['page']) ? intval($this->request->query['page']) : 1;
        if ($this->is_mobile) {
            $size = 12;
        }

        //disable layout render
        $this->layout = 'ajax';
        $this->render(false);

        //TODO: get secret jobs
        $data = $this->_secret($params, $page, $size);
        $val = $data['data'];


        if ($val) {
            foreach ($val as $index => $record) {
                $val[$index] = $this->contentProcess($record);
            }

            echo json_encode($val);
        } else {
            echo json_encode(array());
        }
    }


    public function SendMail()
    {
        $AppController = new AppController;
        $arr = $this->request->data;
		
		if(
			empty($arr['mail']) ||
			empty($arr['name']) ||
			empty($arr['birthday']) ||
			empty($arr['gender']) ||
			empty($arr['phonetic']) ||
			empty($arr['tel']) ||
			empty($arr['id_job'])
		)
		{
			echo 'invalid data';
			die;
		}

        $id = $arr['id_job'];
		
		if(intval($arr['id_job']) < 1)
		{
			echo 'invalid id_job';
			die;
		}
		
        $item = $this->CareerOpportunity->find('first', array(
            'fields' => array('job', 'application_email', 'corporate_prs.company_name'),
            'conditions' => array('CareerOpportunity.id' => $id),
            'joins' => array(
                array(
                    'table' => 'corporate_prs',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'corporate_prs.id = CareerOpportunity.company_id',
                    ),
                ),
            ),
        ));
		
		if(!$item)
		{
			echo 'no job';
			die;
		}

        $job_name = $item['CareerOpportunity']['job'];
        $company_name = $item['corporate_prs']['company_name'];
        $subject = "【コールナビ】" . $company_name . "【" . $job_name . "】プレエントリー完了の連絡";

        //$mail_to_company = $item['CareerOpportunity']['application_email'];
        $mail_to_company = 'kyujin@callnavi.jp';
        //$mail_to_company = 'xuong.banh@jellyfishhr.com';
        $arr['job_name'] = $job_name;
        $arr['company_name'] = $company_name;
        $arr['link'] = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . $this->Util->createSecretDetailHref($item['CareerOpportunity']);
        
        $temp = 'secret';
        $temp_admin = 'secret_admin';
        $success = $AppController->send_email(
            'default',
            //['info@callnavi.jp' => 'Callnavi窓口'],
            ['no-reply@callnavi.jp' => 'Callnavi窓口'],
            $mail_to_company,
            $subject,
            $message = null,
            $format = 'text',
            $template = $temp_admin,
            array('arr' => $arr),
            'it@jellyfishhr.com',
            Configure::read('EMAIL')
        );

        $success_thank_you = $AppController->send_email(
            'default',
            //['info@callnavi.jp' => 'Callnavi窓口'],
            ['no-reply@callnavi.jp' => 'Callnavi窓口'],
            $arr["mail"],
            $subject,
            $message = null,
            $format = 'text',
            $template = $temp,
            array('arr' => $arr),
            'it@jellyfishhr.com',
            Configure::read('EMAIL')
        );

        $result = array("Send Admin" => $success, "Send User" => $success_thank_you);

        $this->autoRender = false;
    }


    public function apply($id = null)
    {

        $id = $this->request->query['id'];


        $rs = $this->CareerOpportunity->find('first', array(
            'fields' => array('CareerOpportunity.id', 'CareerOpportunity.job', 'corporate_prs.id', 'corporate_prs.company_name'),
            'conditions' => array('CareerOpportunity.id' => $id, 'CareerOpportunity.status' => 1),
            'joins' => array(
                array(
                    'table' => 'corporate_prs',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'corporate_prs.id = CareerOpportunity.company_id',
                    ),
                ),
            ),
        ));

        if (empty($rs)) {
            return $this->redirect('/secret');
        }

        $this->set('job_id', $rs['CareerOpportunity']['id']);
        $this->set('company', $rs['corporate_prs']);
        $this->set('secret', $rs['CareerOpportunity']);

        if ($this->is_mobile) {
            $this->layout = 'single_column_sp';
            $this->render('apply-sp');
        } else {
            $this->layout = 'single_column';
        }

    }


    public function detail($idOrTitle = null)
    {
        /*Invalid*/
        if ($idOrTitle == null) {
            return $this->redirect('/');
        }

        $secret = $this->CareerOpportunity->getSecret($idOrTitle);

        /*Got data*/
        if ($secret) {
            $this->set('CareerOpportunity', $secret['CareerOpportunity']);
            $this->set('CorporatePrs', $secret['CorporatePrs']);

            if ($this->is_mobile) {
                //if Admin checked ショー: ON + 必須: ON => don't show
                if (!($secret['CareerOpportunity']['is_upload_cv'] && $secret['CareerOpportunity']['required_upload_cv'])){
                //delete height funtion
                //$heightFooter = 'height-730';
                //$this->set('heightFooter',$heightFooter);
                }
                $this->layout = 'single_column_sp';
                $this->render('detail-sp');
            } else {
                $this->layout = 'single_column';
            }

        } else {
//			return $this->redirect('/secret/'.$idOrTitle);
            $data = $item = $this->getContentsDetail(null,null,$idOrTitle);
            // Checking the contents data have company link amd secret job link
            if($data && $data['Contents']['secret_job_id'] != "" && $data['Contents']['secret_job_id'] != "0" && $data['Contents']['company_id'] != "" && $data['Contents']['company_id'] != "0" ){
                $this->contentsView($idOrTitle);
            }else{
                $this->seoSearch($idOrTitle);
                
            }
//			$this->set('CareerOpportunity', []);
//			$this->set('CorporatePrs',   []);
        }
    }

    public function preview($id = null, $kindPreview = 'PC')
    {
        /*Invalid*/
        if (!$this->Session->check('User')) {
            $this->redirect('/login');
        }
        if ($id == null || intval($id) < 1) {
            return $this->redirect('/');
        }
        $id = intval($id);
        $secret = $this->CareerOpportunity->getSecretShare($id);

        /*Got data*/
        if ($secret) {
            if (!$secret['CareerOpportunity']['senior_avata']) {
                $secret['CareerOpportunity']['senior_avata'] = '';
            }

            $this->set('CareerOpportunity', $secret['CareerOpportunity']);
            $this->set('CorporatePrs', $secret['CorporatePrs']);
            $this->set("isPreview", true);
        } else {
            return $this->redirect('/?348');
            $this->set('CareerOpportunity', []);
            $this->set('CorporatePrs', []);
        }
        if ($this->request->is('post')) {

            $param['CareerOpportunity'] = $this->request->data['PublishJob'];
            $id = !empty($id) ? $id : $this->CareerOpportunity->getLastInsertId();
            $this->CareerOpportunity->id = $id;
            $this->CareerOpportunity->set($param);
            $redirectUrl = $param['CareerOpportunity']['status'] == 1 ? '/secret/' . $id : '/secret/preview/' . $id;
            if ($this->CareerOpportunity->save()) {
                $this->Session->setFlash(__('The article has been saved.'));
                return $this->redirect($redirectUrl);
            } else {
                $this->Session->setFlash(__('The article could not be saved. Please, try again.'));
            }
        }
        if ($kindPreview == 'SP') {
            $this->set('zoom', true);
            $this->layout = 'single_column_sp';
            $this->render('detail-sp-preview');
        } else {
            $this->layout = 'single_column';
        }

    }


    public function previewCompany($id = null, $kindPreview = 'PC')
    {
        /*Invalid*/
        if (!$this->Session->check('User')) {
            $this->redirect('/login');
        }
        $secret = $this->CareerOpportunity->getSecretCompany($id);
        
        if (!$secret['CorporatePrs']['president_avata']) {
            $secret['CorporatePrs']['president_avata'] = 'no-img2.jpg';
        }
        if (!$secret['CorporatePrs']['corporate_logo']) {
            $secret['CorporatePrs']['corporate_logo'] = 'no-img3.jpg';
        }
        /*Got data*/
        if ($secret) {
            $CareerOpportunity = [];
            $CareerOpportunity['release_date'] = ' ';
            $CareerOpportunity['benefit'] = ' ';
            $CareerOpportunity['employment'] = '雇用形態';
            $CareerOpportunity['hourly_wage'] = ' ';
            $CareerOpportunity['job'] = ' ';
            $CareerOpportunity['business'] = ' ';
            $CareerOpportunity['branch_name'] = $secret['CorporatePrs']['company_name'];
            $CareerOpportunity['comment'] = $secret['CorporatePrs']['comment'];
            $CareerOpportunity['is_upload_cv'] = '1';
            $CareerOpportunity['required_upload_cv'] = '0';

            $this->set('CareerOpportunity', $CareerOpportunity);

            $this->set('CorporatePrs', $secret['CorporatePrs']);
            $this->set("isPreview", true);
        }
        if ($this->request->is('post')) {

            $param['CorporatePrs'] = $this->request->data['PublishJob'];
            $id = !empty($id) ? $id : $this->CorporatePrs->getLastInsertId();
            $this->CorporatePrs->id = $id;
            $this->CorporatePrs->set($param);
            $redirectUrl = '/secret/previewCompany/' . $id;
            if ($this->CorporatePrs->save()) {
                $this->Session->setFlash(__('The article has been saved.'));
                return $this->redirect($redirectUrl);
            } else {
                $this->Session->setFlash(__('The article could not be saved. Please, try again.'));
            }
        }

        if ($kindPreview == 'SP') {
            $this->layout = 'single_column_sp';
            $this->render('preview_company-sp');
        } else {
            $this->layout = 'single_column';
        }

    }


    public function test_sendmail()
    {
//		var_dump($arr);
//		var_dump(Configure::read('EMAIL'));
//		var_dump($success);
//		die;
        $default = array(
            'transport' => 'Smtp',
            //        'from' => array('jellyfisherror@gmail.com' => 'Hikaku-jan.com'),
            //        'host' => 'ssl://smtp.gmail.com',
            //        'port' => 465,
            //        'timeout' => 30,
            //        'username' => 'jellyfisherror@gmail.com',
            //        'password' => 'Jellyfish74189532',
            //        'client' => null,
            //        'log' => true,
            //        'emailFormat' => 'html',
            //        'charset' => 'utf-8',
            //        'headerCharset' => 'utf-8',

            /*      'host' => 'ssl://smtp.gmail.com',
                    'port' => 465,
                    'timeout' => 30,
                    'from' => array('info@callnavi.jp' => 'Callnavi??'),
                    'username' => 'info@jellyfishhr.com',
                    'password' => 'ABCD@123.com',
                    'client' => null,
                    'log' => false,
                    'charset' => 'utf-8',
                    'auth'=>true*/

            'host' => 'localhost',
            'port' => 25,
            'timeout' => 30,
            'from' => array('no-reply@callnavi.jp' => 'Callnavi'),
            'client' => null,
            'log' => false,
            'charset' => 'utf-8',
            'auth' => true
        );
        $AppController = new AppController;
        $temp = 'secret';
        $temp_admin = 'secret_admin';
        $mail_to_company = 'cuong.trinh@jellyfishhr.com';
        $subject = "test mail";
        $arr['job_name'] = "job name";
        $arr['company_name'] = "company name";
        $arr['link'] = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . Router::url(array('controller' => 'secret', 'action' => 'detail', 1), false);
        $success = $AppController->send_email(
            'default',
            ['cuong.trinh@jellyfishhr.com' => 'Callnavi窓口'],
            $mail_to_company,
            $subject,
            $message = null,
            $format = 'text',
            $template = $temp_admin,
            array('arr' => $arr),
            'cuong.trinh@jellyfishhr.com',
            $default
        );

        var_dump($arr);
        var_dump(Configure::read('EMAIL'));
        var_dump($success);
        die;
    }

    /*--------*/
    private function _search($params, $page, $size)
    {
        return $this->CloudSearch->search($params, $page, $size);
    }


    private function genCurrentKeyWord($arr)
    {
        $currentKeyWord = '';
        $area_data = Configure::read('webconfig.area_data');
        $employment_data = Configure::read('webconfig.employment_data');
        if (!empty($arr['area'])) {
            $currentKeyWord .= $area_data[$arr['area']];
        }

        if (!empty($arr['em'])) {
            $currentKeyWord .= ' / ' . $employment_data[$arr['em']];
        }

        if (!empty($arr['keyword'])) {
            $currentKeyWord .= ' / ' . $arr['keyword'];
        }
        if (!empty($arr['hotkey'])) {

            $hotkey_data = Configure::read('webconfig.hotkey_data');
            $hotKey = [];
            for ($x = 0; $x < count($arr['hotkey']); $x++) {
                $hotKey[$x] = $hotkey_data[$arr['hotkey'][$x]];
            }
            $hotKey = implode(' / ', $hotKey);
            $currentKeyWord .= ' / ' . $hotKey;

        }

        return $currentKeyWord;
    }

    private function newGenCurrentKeyWord($arr)
    {
        $currentKeyWord = '';
        if (!empty($arr['area'])) {
            $currentKeyWord .= $arr['area'];
        }

        if (!empty($arr['em'])) {
            $currentKeyWord .= ' / ' . $arr['em'];
        }

        if (!empty($arr['keyword'])) {
            $currentKeyWord .= ' / ' . $arr['keyword'];
        }
        if (!empty($arr['hotkey'])) {

            $hotkey_data = Configure::read('webconfig.hotkey_data');
            $hotKey = [];
            for ($x = 0; $x < count($arr['hotkey']); $x++) {
                $hotKey[$x] = $hotkey_data[$arr['hotkey'][$x]];
            }
            $hotKey = implode(' / ', $hotKey);
            $currentKeyWord .= ' / ' . $hotKey;

        }

        return $currentKeyWord;
    }

    private function _secret($params, $page, $size)
    {
        return $this->CareerOpportunity->getListSecret($page, $size, $params);
    }

    private function contentProcess($item)
    {
        $rsItem = array();

        $career = $item['CareerOpportunity'];
        $corporate = $item['corporate_prs'];
        $contents = $item['contents']; 
        //-picture--------------------------------------------------
        if (empty($career['corporate_logo'])) {
            $rsItem['pic_url'] = '/img/instead/img_no-image_' . sprintf("%02d", rand(1, 10)) . '.jpg';
        } else {
            $rsItem['pic_url'] = "/upload/secret/jobs/" . $career['corporate_logo'];
        }


        //-salary----------------------------------------------------
        if(isset($career['hourly_wage'])){
            $rsItem['salary'] = strip_tags($career['hourly_wage']);
            if (mb_strlen($rsItem['salary']) > 50) {
                $rsItem['salary'] = mb_substr($item['salary'], 0, 50, 'UTF-8') . '...';
            }

        }else{
            $rsItem['salary'] = "";
        }

        //-title
        $rsItem['title'] = $career['job'];
        $rsItem['clip_title'] = mb_substr(strip_tags($career['job']), 0, 65, 'utf-8');

        //-company
        $rsItem['company'] = $career['branch_name'];
        if (mb_strlen($career['branch_name']) > 25) {
            $rsItem['company'] = mb_substr($career['branch_name'], 0, 25, 'UTF-8') . '...';
        }

        //-isnew
        $rsItem['isNew'] = 0;
        //604800 = 7 days
        $timeInAWeek = time() - 604800;
        if (strtotime($career['created']) > $timeInAWeek) {
            $rsItem['isNew'] = 1;
        }

        $rsItem['secret'] = "1";

        //-label
        $rsItem['labels'] = explode(',', $career['employment']);
        //Sort category follows length
        usort($rsItem['labels'], function ($a, $b) {
            return strlen($a) - strlen($b);
        });
        $rsItem['maxCatLeng'] = 30;

        //-href
        //$rsItem['href'] = '/secret/detail/' . $career['id'];
        $rsItem['href'] = '/secret/' . $career['job'];

        //-benefit
        $rsItem['benefit'] = strip_tags($career['celebration']);

        //CONTENT CHECKING
        //IF INTERVIEW CHANGE HREF PLUS GIVE LABEL

        


        if(isset($contents['title'])){
            $rsItem['isinterview'] = '1';
            $rsItem['href'] = '/secret/' . $contents['title'];
        }

        return $rsItem;
    }



    private function setValueOnView($params, $area_data, $employment_data, $hotkey_data)
    {
        /*Setting*/
        $_keyResult = '';
        $_keyCurrent = '';
        $_area = '';
        $_keyword = '';
        $_em = '';
        $_selectedArea = 0;
        $_selectedEm = 0;
        $_checkedHotkey = [];

        $_hotkeyList_Checked = [];

        if (!empty($params['hotkey'])) {
            $_checkedHotkey = $params['hotkey'];
            $hotkeyList = Configure::read('webconfig.hotkey_data');
            foreach ($_checkedHotkey as $key => $val) {
                $hotkeyValue = $hotkeyList[$val];
                //$_keyResult .= "「{$hotkeyValue}」";
                $_hotkeyList_Checked[$key] = $hotkeyValue;
            }
        }

        if (!empty($params['area'])) {
            //$_area = isset($area_data[$params['area']])? $area_data[$params['area']]:'';
            $_area = isset($params['area']) ? $params['area'] : '';
            $_keyResult .= empty($_area) ? '' : "「{$_area}」";
        }


        if (!empty($params['em'])) {
            //$_em = isset($employment_data[$params['em']])? $employment_data[$params['em']]:'';
            $_em = isset($params['em']) ? $params['em'] : '';
            if (!empty($_em)) {
                $_keyResult .= "「{$_em}」";
            }
        }

        if (!empty($params['keyword'])) {
            $_keyword = $params['keyword'];
            $_keyResult .= "「{$_keyword}」";
        }

        if (!empty($params['area'])) {
            $_selectedArea = intval($params['area']);
        }

        if (!empty($params['em'])) {
            $_selectedEm = intval($params['em']);
        }

        $currentPage = isset($params['page']) ? $params['page'] : 1;
        $this->set('_keyResult', $_keyResult);
        $this->set('_keyCurrent', $_keyCurrent);
        $this->set('_area', $_area);
        $this->set('_keyword', $_keyword);
        $this->set('_em', $_em);
        $this->set('_selectedArea', $_selectedArea);
        $this->set('_selectedEm', $_selectedEm);
        $this->set('_checkedHotkey', $_checkedHotkey);
        $this->set('currentPage', $currentPage);
        $this->set('_hotkeyList_Checked', $_hotkeyList_Checked);
        // $area_data = Configure::read('webconfig.area_data');
        // $employment_data = Configure::read('webconfig.employment_data');
        // $hotkeyList = $this->requestAction('Elements/getHotKeys');
        // $__checkedHotkey = array();
    }

    public function seoSearch($id)
    {
           
        $query = $this->request->query;
        $this->SearchRoute->matchUrl($this->passedArgs);
        $params = $this->SearchRoute->getPars();
        $params['page'] = isset($query['page']) ? intval($query['page']) : 1;
        $this->request->query = $this->SearchRoute->getQuery();
        $this->searchAction($params);
        //$this->detailContents(null, $id);
        //$this->detailContents(null , $id);
        //$item = $this->getContentsDetail(null, null , $id);
        //$this->set("list",$item);
        //$this->layout = null;
        //$this->render('contents');
        //$this->redirect('/contents/'. $id);

    }
    public function contentsView($idOrTitle = null) {
        if ($idOrTitle == null) {
            return $this->redirect('/');
        }
        $this->detailContents(null , $idOrTitle);
        
    }   

    private function searchAction($params)
    {
        //print_r($params);
        $size = 20;
        $render = 'index';
        $valueEm = $params['em'];
        $page = $params['page'];

        if ($this->is_mobile) {
            $size = 12;
            $this->layout = 'single_column_sp';
            $render = 'index-sp';

        } else {
            $this->layout = 'single_column';
            $render = 'index';

        }

        $hotkey_data = $this->Keywords->getHotkey();

        $area_data = Configure::read('webconfig.area_data');
        $employment_data = Configure::read('webconfig.employment_data');
        $em = '';

        //TODO: get secret jobs
        $data = $this->_secret($params, $page, $size);
        $val = $data['data'];
        $total = $data['total'];
        $pageCount = ceil($total / $size);
        if (isset($params['data_em'])) {
            if ($params['data_em']) {
                $em = $employment_data[$params['data_em']];
            }
        }
        $this->set('data', $val);
        $this->set('search_total', $total);
        $this->set("pageCount", $pageCount);
        $params['em'] = $em;

        if(empty($params['area']) && !empty($params['group']))
        {
            $params['area'] = $params['group'];
        }
        $this->set('valueEm', $valueEm);
        $this->set('size', $size);
        $this->set('params', $params);
        $this->set('area_data', $area_data);
        $this->set('employment_data', $employment_data);
        $this->set('hotkeyList', $hotkey_data);
        $this->set('currentKeyWord', $this->newGenCurrentKeyWord($params));
        $this->set('request_query', $this->request->query);
        $this->setValueOnView($params, $area_data, $employment_data, $hotkey_data);
        $this->render($render);
    }

    public function detailContents($category =null, $title=null) {
		$contentsModel = new Contents();
        $corporatePrsModel = new CorporatePrs();
        $categoriesModel = new Categories();
		if(empty($category) && empty($title)){
			$this->redirect('/contents/');
		}
		
		$item = $this->getContentsDetail(null,$category,$title);
        if($item['Contents']['category_alias'] == 'interview'){
            $secret = $this->CareerOpportunity->getSecret($item['Contents']['secret_job_id']);
            $secret = $this->CareerOpportunity->getSecret($item['Contents']['secret_job_id']);
            if(!empty($secret)) {
                $item['CareerOpportunity'] = $secret['CareerOpportunity'];
                $item['CorporatePrs'] = $secret['CorporatePrs'];
            }else{
                $CorporatePrs = $corporatePrsModel->find('first',array(
                    'conditions' => array('id'=>$item['Contents']['company_id'])
                ));
				//pr($CorporatePrs);
				//die;
				if($CorporatePrs)
				{
					$item['CorporatePrs'] = $CorporatePrs['CorporatePrs'];
				}
				
				else
				{
					$item['CorporatePrs']['company_name'] = $item['Contents']['company_name'];
					$item['CorporatePrs']['representative'] = $item['Contents']['ceo_name'];
				}
                
            }
        }
        

		$catList= $categoriesModel->find('all', array(
			'conditions' => array('category_type' => 2 ),
		));
		
		//Catching content doesn't exists 
		if(empty($item))
		{
			if(empty($category))
			{
				$this->redirect('/contents/');
			}
			else
			{
				$this->redirect('/contents/'.$category);
			}
		}
				
		$contentsModel->id = $item['Contents']['id'];
		$contentsModel->query("UPDATE `contents` SET `view` = `view` + 1 WHERE id = ".$item['Contents']['id']);
		
		//create breadrum link
	    $page= array('name'=>'特集記事', 'link'=>Router::url(array('controller' => 'Contents', 'action' => 'index' )));
	    $category= array('name'=>$item['category']['category'], 'link'=>Router::url('/contents/'. $item['category']['category'] ));
	    $detail= array('name'=> strlen( $item['Contents']['title'])> 30 ? mb_substr( $item['Contents']['title'],0,30,'UTF-8').'...' : $item['Contents']['title'], 'link'=>'');
	    $breadrumLink = $this->breadrumLinkDetail($page,$category,$detail);

		//debug($this->ContentsEmail->getDataSource()->getLog(false, false) );die;
		//pr($item);die;
		$this->set("catList",$catList);
		$this->set("list",$item);
		$this->set("breadrumLink",$breadrumLink);
		

        /*Must in bottom*/
        if($this->is_mobile)
        {

            $heightFooter = 'height-730';
            $this->set('heightFooter',$heightFooter);
            $this->layout = 'default_sp';
            $this->render('../Contents/detailsp');
        }
		else
		{
			$this->layout = 'single_column';
			$this->render('../Contents/detail');
		}
		
	}

    private function getContentsDetail($id = null,$category= null, $title = null ){
        $contentsModel = new Contents();
		
		if($id)
		{
			$conditions = array('Contents.id' =>$id );
		}
		else
		{
			if(empty($category))
			{
				$conditions = array('Contents.title'=>$title);
			}
			else
			{
				//if Category is Japanese
				if (!preg_match("/^\w+$/i", $category)) {
					$findCate = $contentsModel->find('first', array(
						'conditions' => array('Categories.category' => $category ),
					));

					if($findCate)
					{
						$category = $findCate['Categories']['category_alias'];
					}

					$conditions = array('Contents.category_alias' 	=>$category,
										'Contents.title' 			=>$title );
				}
				else
				{
					$conditions = array('Contents.category_alias' 	=>$category,
										'Contents.title_alias' 		=>$title );
				}
			}
		}
		
        $item = $contentsModel->find('first',array(
            'fields' => array('id',
                              'company_id',
							  'company_name',
							  'ceo_name',
							  'secret_job_id',
							  'priority',
							  'status',
							  'site_id',
							  'province_id',
							  'url',
							  'view', 
							  'title_hint',
							  'title',
							  'description',
							  'content',
							  'content2',
							  'content3',
							  'content4',
                              'content_introduction',
							  'image',
                              'image_profile',
                              'image_profile_sp',
							  'category_alias',
							  'meta_description',
							  'meta_keyword',
                			  'title_alias','created','category.category',
                			  'TIMESTAMPDIFF(hour, Contents.created, CURDATE() ) AS totalHour'),
			
            'conditions' => $conditions,
            'joins' => array(
                array(
                    'table' => 'category',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'category.category_alias = Contents.category_alias',
                    ),
                ),
            ),
        ));
        return $item;
    }  
    private  function  breadrumLinkDetail($page = null,$category = null, $detail= null){
		return array(
			'page' 	 => $detail['name'],
			'parent' => array(
								array('link' => $page['link'], 'title' => $page['name']),
								array('link' => $category['link'], 'title' => $category['name']),
							 )
		);
    }    
}
