<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
App::uses('CakeText', 'Utility');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ContentsController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array('Contents','Categories','CareerOpportunity','CorporatePrs');
	public $helpers = array('Paginator','Html');
    public $components = array('Session', 'Util');
    public $paginate = array();
	
	public $sp_paginate = 12;
	public $pc_paginate = 21;
/**
 * Displays a view
 *
 * @return void
 * @throws NotFoundException When the view file could not be found
 *	or MissingViewException in debug mode.
 */

	public function ajax_index($category =null)
	{
		//TODO: ajax_index
		$data = $this->pagination($category);
		
		$this->layout = 'ajax';
		$this->render(false);
		
		if($data['list'])
		{
			foreach( $data['list'] as $index=>$record)
			{
				$data['list'][$index] = $this->contentProcess($record);
			}
			
			echo json_encode($data['list']);
		}
		else
		{
			echo json_encode(array());
		}
		
	}
	
	
	
	public function index($category =null) {
		
		$findCate = $this->Categories->find('first', array(
            'conditions' => array('Categories.category' => $category ),
        ));
		
		
		if(empty($category) || $findCate)
		{
			//TODO: index
			$data = $this->pagination($category);

			$this->set("catList",$data['catList']);
			$this->set("curCat",$data['curCat']);
			$this->set("total", $data['total']);
			$this->set("list",$data['list']);
            $this->set("category",$category);
            
			//$this->getNumberStartEndinPage($this->params['paging']['Contents']);

			/*Must in bottom*/
			if($this->is_mobile)
			{	
				//delete height #345
				//$heightFooter = 'height-730';
				//$this->set('heightFooter',$heightFooter);
				$this->set("totalPage", ceil($data['total']/$this->sp_paginate));
				$this->render('indexsp');
			}else{
				$this->set("totalPage", ceil($data['total']/$this->pc_paginate));
				$this->layout = 'single_column';
				$this->render('index_v2');
			}
		}
		
		//try go to detail
		else
		{
			$this->detail(null, $category);
		}
	}
	
	
	
	public function detail($category =null, $title=null) {
		
		if(empty($category) && empty($title)){
			$this->redirect('/contents/');
		}
        
		$item = $this->getContentsDetail(null,$category,$title);
        
        //redirect to secret page if page is interview with secret is 
        if($item && $item['Contents']['secret_job_id'] != "" && $item['Contents']['secret_job_id'] != "0" && $item['Contents']['company_id'] != "" && $item['Contents']['company_id'] != "0" ){
                $this->redirect('/secret/'.$title);
        }
        
        if($item['Contents']['category_alias'] == 'interview'){
            $secret = $this->CareerOpportunity->getSecret($item['Contents']['secret_job_id']);
            if(!empty($secret)) {
                $item['CareerOpportunity'] = $secret['CareerOpportunity'];
                $item['CorporatePrs'] = $secret['CorporatePrs'];
            }else{    
                $CorporatePrs = $this->CorporatePrs->find('first',array(
                    'conditions' => array('id'=>$item['Contents']['company_id'])
                ));
				//pr($CorporatePrs);
				//die;
				if($CorporatePrs)
				{
					$item['CorporatePrs'] = $CorporatePrs['CorporatePrs'];
				}
				
				else
				{
					$item['CorporatePrs']['company_name'] = $item['Contents']['company_name'];
					$item['CorporatePrs']['representative'] = $item['Contents']['ceo_name'];
				}
                
            }
        }


		$catList= $this->Categories->find('all', array(
			'conditions' => array('category_type' => 2 ),
		));
		
		//Catching content doesn't exists 
		if(empty($item))
		{
			if(empty($category))
			{
				$this->redirect('/contents/');
			}
			else
			{
				$this->redirect('/contents/'.$category);
			}
		}
				
		$this->Contents->id = $item['Contents']['id'];
		$this->Contents->query("UPDATE `contents` SET `view` = `view` + 1 WHERE id = ".$item['Contents']['id']);
		
		//create breadrum link
	    $page= array('name'=>'特集記事', 'link'=>Router::url(array('controller' => 'Contents', 'action' => 'index' )));
	    $category= array('name'=>$item['category']['category'], 'link'=>Router::url('/contents/'. $item['category']['category'] ));
	    $detail= array('name'=> strlen( $item['Contents']['title'])> 30 ? mb_substr( $item['Contents']['title'],0,30,'UTF-8').'...' : $item['Contents']['title'], 'link'=>'');
	    $breadrumLink = $this->breadrumLinkDetail($page,$category,$detail);

		//debug($this->ContentsEmail->getDataSource()->getLog(false, false) );die;
		//pr($item);die;
		$this->set("catList",$catList);
		$this->set("list",$item);
		$this->set("breadrumLink",$breadrumLink);
		

        /*Must in bottom*/
        if($this->is_mobile)
        {

            $heightFooter = 'height-730';
            $this->set('heightFooter',$heightFooter);
            $this->layout = 'default_sp';
            $this->render('detailsp');
        }
		else
		{
			$this->layout = 'single_column';
			$this->render('detail');
		}
		
	}

    public function preview($id = null, $kindPreview = 'PC') {
        /*Invalid*/
        if(!$this->Session->check('User')){
            $this->redirect('/login');
        }
        if(!empty($id)){

            $item = $this->getContentsDetail($id, null, null);

            $catList= $this->Categories->find('all', array(
                'conditions' => array('category_type' => 2 ),
            ));

			//create breadrum link
            $page= array('name'=>'コンテンツ（特集）', 'link'=>Router::url(array('controller' => 'Contents', 'action' => 'index' )));
            $category= array('name'=>$item['category']['category'], 'link'=>Router::url('/contents/'. $item['Contents']['category_alias'] ));
            $detail= array('name'=> strlen( $item['Contents']['title'])> 30 ? mb_substr( $item['Contents']['title'],0,30,'UTF-8').'...' : $item['Contents']['title'], 'link'=>'');
            $breadrumLink = $this->breadrumLinkDetail($page,$category,$detail);

            //debug($this->ContentsEmail->getDataSource()->getLog(false, false) );die;
            //pr($item);die;
            $this->set("catList",$catList);
            $this->set("list",$item);
            $this->set("breadrumLink",$breadrumLink);
            $this->set("isPreview",true);
        }

        if($this->request->is('post')){

            $param['Contents'] = $this->request->data['PublishJob'];
            $id= !empty($id) ? $id :$this->Contents->getLastInsertId();
            $this->Contents->id = $id;
            $this->Contents->set($param);
            $redirectUrl = $param['Contents']['status'] == 1 ?
                '/contents/'.$item['Contents']['category_alias'].'/'.$item['Contents']['title_alias'] : '/contents/preview/'.$id;
            if ($this->Contents->save()) {
                //pr($item);die;
                $this->Session->setFlash(__('The article has been saved.'));
                return $this->redirect($redirectUrl);
            } else {
                $this->Session->setFlash(__('The article could not be saved. Please, try again.'));
            }
        }

        /*Must in bottom*/
        if ($kindPreview == 'SP') {
            $this->set('zoom', true);
            $heightFooter = 'height-730';
            $this->set('heightFooter',$heightFooter);
            $this->layout = 'default_sp';
            $this->render('detailsp');
        } else {
	        $this->layout = 'single_column';
	        $this->render('detail');
	    }
    }


    private function getNumberStartEndinPage($arr = null){
        if(!empty($arr)){
            if($arr['count'] != 0) {
                $start = (($arr['page'] - 1) * 20) + 1;
                $end = $arr['page'] == $arr['pageCount'] ? $arr['count'] : $arr['page'] * 20;
            }else{
                $start = 0;
                $end = 0;
            }
            $this->set ('numberStartEnd', array('start' => $start, 'end' => $end ));
        }
    }

    private  function  breadrumLinkDetail($page = null,$category = null, $detail= null){
		return array(
			'page' 	 => $detail['name'],
			'parent' => array(
								array('link' => $page['link'], 'title' => $page['name']),
								array('link' => $category['link'], 'title' => $category['name']),
							 )
		);
    }

    private function getContentsDetail($id = null,$category= null, $title = null ){
		
		if($id)
		{
			$conditions = array('Contents.id' =>$id );
		}
		else
		{
			if(empty($category))
			{
				$conditions = array('Contents.title'=>$title);
			}
			else
			{
				//if Category is Japanese
				if (!preg_match("/^\w+$/i", $category)) {
					$findCate = $this->Categories->find('first', array(
						'conditions' => array('Categories.category' => $category ),
					));

					if($findCate)
					{
						$category = $findCate['Categories']['category_alias'];
					}

					$conditions = array('Contents.category_alias' 	=>$category,
										'Contents.title' 			=>$title );
				}
				else
				{
					$conditions = array('Contents.category_alias' 	=>$category,
										'Contents.title_alias' 		=>$title );
				}
			}
		}
		
        $item = $this->Contents->find('first',array(
            'fields' => array('id',
                              'company_id',
							  'company_name',
							  'ceo_name',
							  'secret_job_id',
							  'priority',
							  'status',
							  'site_id',
							  'province_id',
							  'url',
							  'view', 
							  'title_hint',
							  'title',
							  'description',
							  'content',
							  'content2',
							  'content3',
							  'content4',
                              'content_introduction',
							  'image',
                              'image_profile',
                              'image_profile_sp',
							  'category_alias',
							  'meta_description',
							  'meta_keyword',
                			  'title_alias','created','category.category',
                			  'TIMESTAMPDIFF(hour, Contents.created, CURDATE() ) AS totalHour'),
			
            'conditions' => $conditions,
            'joins' => array(
                array(
                    'table' => 'category',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'category.category_alias = Contents.category_alias',
                    ),
                ),
            ),
        ));
        return $item;
    }
	
	private function pagination($category =null)
	{
		//TODO: pagination
		if($this->is_mobile)
		{
			$size = $this->sp_paginate;
		}
		else
		{
			$size = $this->pc_paginate;
		}
		$page = isset($this->request->query['page'])?$this->request->query['page']:1;
        $curCat = null;
		if(!empty($category)){
			
			/*
				check If Jappanese or Latin
			*/
			if (!preg_match("/^\w+$/i", $category)) {
				
				$findCate = $this->Categories->find('first', array(
            		'conditions' => array('Categories.category' => $category ),
        		));

				if($findCate)
				{
					$category = $findCate['Categories']['category_alias'];
				}
			}
			
			
            $this->paginate = array(
                'fields' => array('id','priority','Contents.status','company_id','secret_job_id',    'site_id','province_id','url','category','view', 'title_hint','title','image','category_alias',
                    'title_alias','Contents.created','category.category',
                    'TIMESTAMPDIFF(hour, Contents.created, NOW() ) AS totalHour'),
                'conditions' => array('Contents.status' => 1, 'Contents.category_alias' => $category ),
                'joins' => array(
                    array(
                        'table' => 'category',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'category.category_alias = Contents.category_alias',
                        ),
                    ),
                ),
                'limit' => $size,
                'order' => array(
                    'Contents.created' => 'DESC'
                ),
				'page' => $page
            );
            $total = $this->Contents->find('count',array(
                'conditions' => array('status' => 1, 'Contents.category_alias' => $category ),
            ));

            $curCat = $category;

		}else {
            $this->paginate = array(
                'fields' => array('id', 'priority', 'Contents.status','company_id', 'site_id', 'secret_job_id', 'province_id', 'url', 'category','view', 'title_hint', 'title', 'image', 'category_alias',
                    'title_alias', 'created','company_id', 'category.category',
                    'TIMESTAMPDIFF(hour, Contents.created, NOW() ) AS totalHour'),
                'conditions' => array('Contents.status' => 1),
                'joins' => array(
                    array(
                        'table' => 'category',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'category.category_alias = Contents.category_alias',
                        ),
                    ),
                ),
                'limit' => $size,
                'order' => array(
                    'Contents.created' => 'DESC'
                ),
				'page' => $page
            );
            $total = $this->Contents->find('count');
        }
		
		try
		{
			$val= $this->paginate('Contents');
		}
		catch(Exception $e)
		{
			$val= array();
		}

		
        $catList= $this->Categories->find('all', array(
            'conditions' => array('Categories.category_type' => 2 ),
			'order'	=> 'Categories.priority'
        ));
		
		
		return array(
			"catList" => $catList,
			"curCat" => $curCat,
			"total" => $total,
			"list" => $val,
		);
	}
	
	private function contentProcess($item)
	{
		//Default
		$article_base_url = '/contents/';
		$article_table = 'Contents';
		$article_title = 'title';
		$article_image = 'image';
		$article_view = 'view';
		$article_created = 'created';
		$article_image_pos_compare = "ccwork";
		$article_image_base_url = "/upload/contents/";
		$article_id_url = 'title_alias';
		
		$image = $item[$article_table][$article_image];
		$isNew = $item[0]['totalHour'] <= 3;
		$idUrl = $item[$article_table][$article_id_url];
		
		$item[$article_table]['isNew'] = $isNew;
		
		$dt = new DateTime($item[$article_table][$article_created]);
		

		$dtFormat = 'Y年m月d日';
		$item[$article_table]['createdDate'] = $dt->format($dtFormat);
		
		if (mb_strlen($item[$article_table]['title']) > 28) {
			$item[$article_table]['splited_title'] = mb_substr($item[$article_table]['title'], 0, 28, 'UTF-8') . '...';
		}
		else
		{
			$item[$article_table]['splited_title'] = $item[$article_table]['title'];
		}

		$cateList = array();
		$cateNames = array($item['category']['category']);
		$cateAlias = array($item[$article_table]['category_alias']);
		$firstCateAlias = $cateAlias[0];


		//MAKE IMAGE LINK
		if (!empty($image)) {
			$pos = strpos($image, $article_image_pos_compare);
			if ($pos === false) {
				$image = $article_image_base_url . $image;

			} else {
				$image = $image;
			}
		} else {
			$image = '/img/instead/img_no-image_' . sprintf("%02d", rand(1, 10)) . '.jpg';
		}
		
		$item[$article_table]['image'] = $image;

		//MAKE CATEGORY
		foreach ($cateNames as $i => $cate) {
			if (isset($cateAlias[$i])) {

				$dataCat = [];
				$dataCat['category'] = $cate;
				$dataCat['category_alias'] = $cateAlias[$i];
				$catHref = $this->Util->createContentCategoryHref($dataCat);

				$cateList[] = array(
					'href' => $catHref,
					'name' => $cate
				);
			}
		}
		
		$item['category']['cateList'] = $cateList;
		
		//MAKE HREF
		$dataDetail = [];
		$dataDetail['title'] = $item[$article_table]['title'];
		$dataDetail['id'] = $item[$article_table]['id'];
		$dataDetail['cat_names'] = $item['category']['category'];
		$dataDetail['cat_alias'] =	$item[$article_table]['category_alias'];
		$dataDetail['title_alias'] = $item[$article_table]['title_alias'];

		$detailHref = $this->Util->createContentDetailHref($dataDetail);
		$item[$article_table]['url'] = $detailHref;
		//$item[$article_table]['url'] = $article_base_url . $firstCateAlias . '/' . $idUrl;
		
		
		return $item;

	}
}
