<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
App::uses('CakeText', 'Utility', 'Area');
App::uses('SessionHelper', 'View/Helper');
/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ApplyController extends AppController
{

    /**
     * This controller does not use a model
     *
     * @var array
     */
    
    public $components = array('Session', 'Cookie', 'Util','Code','Sendmail');
    public $uses = array('CareerOpportunity', 'CorporatePrs', 'Keywords', 'Entry');
    public $helpers = array('Paginator','Html','Form');
    
    public function apply_step1()
    {
        $id = $this->request->query['id'];
        $rs = $this->CareerOpportunity->find('first', array(
            'fields' => array('CareerOpportunity.id', 
							  'CareerOpportunity.job', 
							  'CareerOpportunity.is_upload_cv', 
							  'CareerOpportunity.required_upload_cv',
                              'CareerOpportunity.employment',
                              'CareerOpportunity.branch_name',
							  'corporate_prs.id', 
							  'corporate_prs.company_name'),
            'conditions' => array('CareerOpportunity.id' => $id, 'CareerOpportunity.status' => 1),
            'joins' => array(
                array(
                    'table' => 'corporate_prs',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'corporate_prs.id = CareerOpportunity.company_id',
                    ),
                ),
            ),
        ));

        if (empty($rs)) {
            return $this->redirect('/secret');
        }
		
		if($this->Session->check('apply_post_'.$id))
		{
			$post = $this->Session->read('apply_post_'.$id);
			$this->set('post', $post);
		}
		else
		{
			$this->set('post', array(
				'step1FullName' => '',
				'step1Phonetic' =>  '',
				'step1Birth' => '',
				'step1gender' => '',
				'step1Phone' => '',
				'step1Email' => '',
				'step1Content' => '',
				'step1cv1' => '',
				'step1cv2' => '',
                'step1time' => '',
                'step1-skill1' => '',
			));
		}
        
		$id = $rs['CareerOpportunity']['id'];
        $this->set('workingTime', $this->checkingWorkingTime($rs['CareerOpportunity']['employment']));
        $this->set('job_id', $rs['CareerOpportunity']['id']);
        $this->set('is_upload_cv', $rs['CareerOpportunity']['is_upload_cv']);
        $this->set('required_upload_cv', $rs['CareerOpportunity']['required_upload_cv']);
        $this->set('company', $rs['corporate_prs']);
        $this->set('secret', $rs['CareerOpportunity']);
		
		
		
		//set session
		$this->Session->write('apply_'.$id, array(
			'job_id' 	=> $id,
			'company' 	=> $rs['corporate_prs'],
			'secret' 	=> $rs['CareerOpportunity'],
		));

		if ($this->is_mobile) {
            $this->layout = 'single_column_sp';
            $this->render('apply_step1-sp');
        } else {
            $this->layout = 'single_column';
        }

    }
	
	public function apply_step2(){
		
		$id = $this->request->query['id'];
		if($id==null || $this->Session->check('apply_'.$id) == false)
		{
			return $this->redirect('/secret?1');
		}
		
		if($this->request->is('post'))
		{

			$data = $this->request->data;
			
			if(!empty($this->params['form']['step1cv1']))
			{
                if($this->params['form']['step1cv1']['name'] != ""){
                    $data['step1cv1'] = $this->params['form']['step1cv1']['name'];
                    $data['step1cv1_file'] = $this->params['form']['step1cv1'];
                    $tmpCv = $this->Entry->saveTmpCv1($data['step1cv1_file']['tmp_name'], 
                                                      $data['step1cv1_file']['name'], $data['job_id']);
                    $data['step1cv1_file']['tmp_name'] = $tmpCv;                    
                }
			}
			
			if(!empty($this->params['form']['step1cv2']))
			{
                 if($this->params['form']['step1cv2']['name'] != ""){
                     $data['step1cv2'] = $this->params['form']['step1cv2']['name'];
                    $data['step1cv2_file'] = $this->params['form']['step1cv2'];
                    $tmpCv = $this->Entry->saveTmpCv2($data['step1cv2_file']['tmp_name'], 
                                                      $data['step1cv2_file']['name'], $data['job_id']);
                    $data['step1cv2_file']['tmp_name'] = $tmpCv;
                 }				
			}

			$this->Session->write('apply_post_'.$id, $data);
			return $this->redirect('/secret/apply-step2?id='.$id);
		}
		
		if(!$this->Session->check('apply_post_'.$id))
		{
			return $this->redirect('/secret?2');
		}
        
       
       $apply = $this->Session->read('apply_'.$id);
		$post = $this->Session->read('apply_post_'.$id);
		$job_id = $apply['job_id'];
		
        //check date for apply page if wrong redirect to page applt
        $date = $post['step1Birth'];
        if(checkdate(substr($date,4,2), substr($date,6,2), substr($date,0,4)) == false){
            return $this->redirect('/secret/apply-step1?id='.$id);
        }

		 
		$rs = $this->CareerOpportunity->find('first', array(
            'fields' => array(
							  'CareerOpportunity.id', 
							  'CareerOpportunity.is_upload_cv', 
							  'CareerOpportunity.required_upload_cv',
							 ),
            'conditions' => array('CareerOpportunity.id' => $job_id, 
								  'CareerOpportunity.status' => 1),
        ));
		
		$this->set('is_upload_cv', $rs['CareerOpportunity']['is_upload_cv']);
        $this->set('required_upload_cv', $rs['CareerOpportunity']['required_upload_cv']);
		
		$this->set('job_id', $job_id);
        $this->set('company', $apply['company']);
        $this->set('secret', $apply['secret']);
        $this->set('post', $post);
		
		
		if ($this->is_mobile) {
			$this->layout = 'single_column_sp';
			$this->render('apply_step2-sp');
		} else {
			$this->layout = 'single_column';
		}
	}
	
	public function apply_thanks(){
	   	if(!isset($this->request->query['testmode'])){
			
			$id = $this->request->query['id'];
			
			if($id==null || $this->Session->check('apply_'.$id)==false || $this->Session->check('apply_post_'.$id)==false)
			{
				 return $this->redirect('/secret');
			}
			
			$post = $this->Session->read('apply_post_'.$id);
			$this->set('post', $post);
			//check if have file or not 
	        $apply_code = $this->Code->genApplyCode($post);
	        $this->set('apply_code', $apply_code);

	         
	        if(!empty($post['step1cv1_file']) || !empty($post['step1cv2_file']))
			{
				$post['password'] = $this->Util->generateRandomString();
				$post['entry_id'] = $this->Entry->add_new($post , $apply_code);
			}else{
	            $post['entry_id'] = $this->Entry->add_new_nocv($post , $apply_code);
	        }
	        
	        
	        //this test sent to email
			//$rs = $this->sent_id_mail($post , $apply_code);
	        //this is the new sent mail
	        //$post['password'] = "testpass";
	        //$post['entry_id'] = "108";
	        //$rs = $this->sentMailV2($post , 'C0001-CAL-J0001-A0001-E01');
	        $rs = $this->sentMailV2($post , $apply_code);
			
	        
	        if(!$rs)
			{
		 		return $this->redirect('/secret?error=1');
			}
			
			$apply = $this->Session->read('apply_'.$id);
			$this->set('company', $apply['company']);
			$this->set('secret', $apply['secret']);
			
			$this->Session->delete('apply_'.$id);
	        $this->Session->delete('apply_post_'.$id);
			
			if ($this->is_mobile) {
	            $this->layout = 'single_column_sp';
	            $this->render('apply_thanks-sp');
	        } else {
	            $this->layout = 'single_column';
	        }
			
		//this codes work only with test mode
		//todo: show thanks page only
	    }else{
			
	    	$secret['branch_name'] = "(test mode)Company Branch";
	    	$this->set('secret', $secret);
		    if ($this->is_mobile) {
	            $this->layout = 'single_column_sp';
	            $this->render('apply_thanks-sp');
	        } else {
	            $this->layout = 'single_column';
	        }	
	    }

	}
	
	
    /**
     * for testing only
     */
    public function test_sendmail()
    {
		$temp_corporate = 'text/apply_cv_corporation';
		//$temp_corporate = 'text/apply_nocv_corporation';
		$subj_corporate = '【コールナビ】求人に応募がありました。';
		
		$email_corporate = 'buihuybinh1994@gmail.com';
		$toname_corporate = 'Tester';
		
		$bcc = Configure::read('email_apply.test_bcc');
		
		$data = array(
            'arr' => array(
                'entry_id' => 999,
                'apply_code' => 'C0099-CAL-J0099-A0099-E99',
                'full_name' => 'Tester X',
                'phonetic_name' => '09012345678',
                'birth_date' => '1990-09-09',
                'gender' => 'male',
                'phone_number' => '09087654321',
                'e-mail' => 'xuong.banh@jellyfishhr.com',
                'skill' => 'skills',
                'working_time' => '8 hours',
                'content' => 'test content',
                'cv1_path' => '',
                'cv2_path' => '',
                'job_title' => 'this is job title',
                'company_name' => 'this is company name',
                'classification' => 'this is classification',
                'password' => 'this is password',
               
            )
        );
		
		$this->Sendmail->fromlName = Configure::read('webconfig.mail_from_name');
		$this->Sendmail->fromlAdress = Configure::read('webconfig.mail_from');
		
		$this->Sendmail->render($temp_corporate,$data);
		//bcc
		$this->Sendmail->addBCC($bcc);
        //Send: to Email, to Name, subject
        $result = $this->Sendmail->send($email_corporate,$toname_corporate, $subj_corporate);
		
		echo '<pre>';
		var_dump($bcc);
		var_dump($data);
		var_dump($result);
		die;
    }

    /*--------*/
    private function _search($params, $page, $size)
    {
        return $this->CloudSearch->search($params, $page, $size);
    }


    private function genCurrentKeyWord($arr)
    {
        $currentKeyWord = '';
        $area_data = Configure::read('webconfig.area_data');
        $employment_data = Configure::read('webconfig.employment_data');
        if (!empty($arr['area'])) {
            $currentKeyWord .= $area_data[$arr['area']];
        }

        if (!empty($arr['em'])) {
            $currentKeyWord .= ' / ' . $employment_data[$arr['em']];
        }

        if (!empty($arr['keyword'])) {
            $currentKeyWord .= ' / ' . $arr['keyword'];
        }
        if (!empty($arr['hotkey'])) {

            $hotkey_data = Configure::read('webconfig.hotkey_data');
            $hotKey = [];
            for ($x = 0; $x < count($arr['hotkey']); $x++) {
                $hotKey[$x] = $hotkey_data[$arr['hotkey'][$x]];
            }
            $hotKey = implode(' / ', $hotKey);
            $currentKeyWord .= ' / ' . $hotKey;

        }

        return $currentKeyWord;
    }

    private function newGenCurrentKeyWord($arr)
    {
        $currentKeyWord = '';
        if (!empty($arr['area'])) {
            $currentKeyWord .= $arr['area'];
        }

        if (!empty($arr['em'])) {
            $currentKeyWord .= ' / ' . $arr['em'];
        }

        if (!empty($arr['keyword'])) {
            $currentKeyWord .= ' / ' . $arr['keyword'];
        }
        if (!empty($arr['hotkey'])) {

            $hotkey_data = Configure::read('webconfig.hotkey_data');
            $hotKey = [];
            for ($x = 0; $x < count($arr['hotkey']); $x++) {
                $hotKey[$x] = $hotkey_data[$arr['hotkey'][$x]];
            }
            $hotKey = implode(' / ', $hotKey);
            $currentKeyWord .= ' / ' . $hotKey;

        }

        return $currentKeyWord;
    }

    private function _secret($params, $page, $size)
    {
        return $this->CareerOpportunity->getListSecret($page, $size, $params);
    }

    private function contentProcess($item)
    {
        $rsItem = array();

        $career = $item['CareerOpportunity'];
        $corporate = $item['corporate_prs'];

        //-picture--------------------------------------------------
        if (empty($corporate['corporate_logo'])) {
            $rsItem['pic_url'] = '/img/instead/img_no-image_' . sprintf("%02d", rand(1, 10)) . '.jpg';
        } else {
            $rsItem['pic_url'] = $this->webroot . "upload/secret/companies/" . $corporate['corporate_logo'];
        }


        //-salary----------------------------------------------------
        $rsItem['salary'] = strip_tags($career['hourly_wage']);
        if (mb_strlen($rsItem['salary']) > 50) {
            $rsItem['salary'] = mb_substr($item['salary'], 0, 50, 'UTF-8') . '...';
        }

        //-title
        $rsItem['title'] = $career['job'];
        $rsItem['clip_title'] = mb_substr(strip_tags($career['job']), 0, 65, 'utf-8');

        //-company
        $rsItem['company'] = $corporate['company_name'];
        if (mb_strlen($corporate['company_name']) > 25) {
            $rsItem['company'] = mb_substr($corporate['company_name'], 0, 25, 'UTF-8') . '...';
        }

        //-isnew
        $rsItem['isNew'] = 0;
        //604800 = 7 days
        $timeInAWeek = time() - 604800;
        if (strtotime($career['created']) > $timeInAWeek) {
            $rsItem['isNew'] = 1;
        }

        //-label
        $rsItem['labels'] = explode(',', $career['employment']);
        //Sort category follows length
        usort($rsItem['labels'], function ($a, $b) {
            return strlen($a) - strlen($b);
        });
        $rsItem['maxCatLeng'] = 30;

        //-href
        //$rsItem['href'] = '/secret/detail/' . $career['id'];
        $rsItem['href'] = '/secret/' . $career['job'];

        //-benefit
        $rsItem['benefit'] = strip_tags($career['benefit']);
        return $rsItem;
    }


    private function setValueOnView($params, $area_data, $employment_data, $hotkey_data)
    {
        /*Setting*/
        $_keyResult = '';
        $_keyCurrent = '';
        $_area = '';
        $_keyword = '';
        $_em = '';
        $_selectedArea = 0;
        $_selectedEm = 0;
        $_checkedHotkey = [];

        $_hotkeyList_Checked = [];

        if (!empty($params['hotkey'])) {
            $_checkedHotkey = $params['hotkey'];
            $hotkeyList = Configure::read('webconfig.hotkey_data');
            foreach ($_checkedHotkey as $key => $val) {
                $hotkeyValue = $hotkeyList[$val];
                //$_keyResult .= "「{$hotkeyValue}」";
                $_hotkeyList_Checked[$key] = $hotkeyValue;
            }
        }

        if (!empty($params['area'])) {
            //$_area = isset($area_data[$params['area']])? $area_data[$params['area']]:'';
            $_area = isset($params['area']) ? $params['area'] : '';
            $_keyResult .= empty($_area) ? '' : "「{$_area}」";
        }


        if (!empty($params['em'])) {
            //$_em = isset($employment_data[$params['em']])? $employment_data[$params['em']]:'';
            $_em = isset($params['em']) ? $params['em'] : '';
            if (!empty($_em)) {
                $_keyResult .= "「{$_em}」";
            }
        }

        if (!empty($params['keyword'])) {
            $_keyword = $params['keyword'];
            $_keyResult .= "「{$_keyword}」";
        }

        if (!empty($params['area'])) {
            $_selectedArea = intval($params['area']);
        }

        if (!empty($params['em'])) {
            $_selectedEm = intval($params['em']);
        }

        $currentPage = isset($params['page']) ? $params['page'] : 1;
        $this->set('_keyResult', $_keyResult);
        $this->set('_keyCurrent', $_keyCurrent);
        $this->set('_area', $_area);
        $this->set('_keyword', $_keyword);
        $this->set('_em', $_em);
        $this->set('_selectedArea', $_selectedArea);
        $this->set('_selectedEm', $_selectedEm);
        $this->set('_checkedHotkey', $_checkedHotkey);
        $this->set('currentPage', $currentPage);
        $this->set('_hotkeyList_Checked', $_hotkeyList_Checked);
        // $area_data = Configure::read('webconfig.area_data');
        // $employment_data = Configure::read('webconfig.employment_data');
        // $hotkeyList = $this->requestAction('Elements/getHotKeys');
        // $__checkedHotkey = array();
    }

    public function seoSearch()
    {
        $query = $this->request->query;
        $this->SearchRoute->matchUrl($this->passedArgs);
        $params = $this->SearchRoute->getPars();
        $params['page'] = isset($query['page']) ? intval($query['page']) : 1;
        $this->request->query = $this->SearchRoute->getQuery();
        $this->searchAction($params);

    }

    private function searchAction($params)
    {
        //print_r($params);
        $size = 20;
        $render = 'index';
        $valueEm = $params['em'];
        $page = $params['page'];

        if ($this->is_mobile) {
            $size = 12;
            $this->layout = 'single_column_sp';
            $render = 'index-sp';

        } else {
            $this->layout = 'single_column';
            $render = 'index';

        }

        $hotkey_data = $this->Keywords->getHotkey();

        $area_data = Configure::read('webconfig.area_data');
        $employment_data = Configure::read('webconfig.employment_data');
        $em = '';

        //TODO: get secret jobs
        $data = $this->_secret($params, $page, $size);
        $val = $data['data'];
        $total = $data['total'];
        $pageCount = ceil($total / $size);
        if (isset($params['data_em'])) {
            if ($params['data_em']) {
                $em = $employment_data[$params['data_em']];
            }
        }
        $this->set('data', $val);
        $this->set('search_total', $total);
        $this->set("pageCount", $pageCount);
        $params['em'] = $em;
        $this->set('valueEm', $valueEm);
        $this->set('size', $size);
        $this->set('params', $params);
        $this->set('area_data', $area_data);
        $this->set('employment_data', $employment_data);
        $this->set('hotkeyList', $hotkey_data);
        $this->set('currentKeyWord', $this->newGenCurrentKeyWord($params));
        $this->set('request_query', $this->request->query);
        $this->setValueOnView($params, $area_data, $employment_data, $hotkey_data);
        $this->render($render);
    }
    
    //function for cheking the working time is need or not
    private function checkingWorkingTime($employment){
        $flag = false;
        $emp_arr = explode(",",$employment);
        if (in_array("アルバイト", $emp_arr) || in_array("パート", $emp_arr)){
            $flag=true;
        }
        return $flag;
    }
    
    //start sent mail v2
    //New Sent Mail Function
    private function sentMailV2($arr, $apply_code)
    {
        //get working time data
        $working_time = $this->Code->getWorkingtime($arr);
        if($working_time == ""){
            $part_time = false;
        }else{
            $part_time = true;
        }

        //combine skill into 1 string
        $skill = $this->Code->comSkill($arr);
            
        //get career oportuniry data    
        $id = $arr["job_id"];
        $item = $this->Code->retMailCarrerr($id);
        
        //checking which email will be use
        
        //checking if it have upload file or not into sent mail
        $sendMailCv = false;
        $cv1_path = "";
        $cv2_path = "";
        
        if($item['CareerOpportunity']['is_upload_cv'] && !empty($arr['entry_id']))
		{
			$entry = $this->Entry->find('first', array(
													   'conditions' => 
													   array('id' => $arr['entry_id'])
													  )
									   );
			if($entry)
			{
				if(isset($arr['step1cv1'])){
					$cv1_path =$arr['step1cv1'];
		        }
		        if(isset($arr['step1cv2'])){
            		$cv2_path = $arr['step1cv2'];
        		}   			
				

                if( $cv1_path != "" || $cv2_path != ""){
                    $sendMailCv = true;
                }
			}
		}
        
        //set cv_path
        if(!isset($cv1_path)){
            $cv1_path = "";
        }
        if(!isset($cv2_path)){
            $cv2_path = "";
        }   
        
        //set job title
        $job_name = $item['CareerOpportunity']['job'];

         //set mail corporate
        $company_mail = $item['CareerOpportunity']['application_email'];

        //set company name
        $company_name = $item['corporate_prs']['company_name'];       

        //set clasification
        $classification = $this->Code->genClassificationData($apply_code);
            
     
        //set arr data for sent mail
        $data = array(
            'arr' => array(
                'entry_id' => $arr['entry_id'],
                'apply_code' => $apply_code,
                'full_name' => $arr['step1FullName'],
                'phonetic_name' => $arr['step1Phonetic'],
                'birth_date' => $arr['step1Birth'],
                'gender' => $arr['step1gender'],
                'phone_number' => $arr['step1Phone'],
                'e-mail' => $arr['step1Email'],
                'skill' => $skill,
                'working_time' => $working_time,
                'content' => $arr['step1Content'],
                'cv1_path' => $cv1_path,
                'cv2_path' => $cv2_path,
                'job_title' => $job_name,
                'company_name' => $company_name,
                'classification' => $classification,
               
            )
        );
		
        if(isset($arr['password'])){
            $data['arr']['password'] = $arr['password'];
        }
		
        //set Apple id  for validation
        $apple_id = "28";
        $com_id = $item['CareerOpportunity']['company_id'];
               
        //validate company_id with apple id
        if($apple_id == $com_id){
            $appleFlag = true;
        }else{
            $appleFlag = false;
        }
        
        //sent email to
        $email_user = $arr['step1Email'];
        $email_corporate = $company_mail;
        $email_admin = Configure::read('webconfig.mail_from_concierge');
               
        
        //set sent mail to test
        //$email_user = $arr['step1Email'];
        //$email_corporate = $arr['step1Email'];
        //$email_admin = $arr['step1Email'];
        
        //set email sent to name
        $toname_user = $arr['step1FullName'];
        $toname_corporate = $company_name;
        $toname_admin = Configure::read('webconfig.$mail_from_name');
        
        //check part_time or not
        //sent mail check
        if($sendMailCv)
        {
            //sent mail with CV
            if($part_time == true)
            {
                if($appleFlag == true){
                    //set email use
                    $temp_admin = 'text/apple_cv_only_applicant';
                    $temp_user = 'text/apple_cv_only_applicant';
                    
                    //set subject
                    $subj_text = Configure::read('email_apply.admin_subject_apple_cv_only_applicant');
                    $subj_admin = $subj_text;
                    $subj_user = $subj_text;
                }else{
                    //set email use
                    $temp_admin = 'text/apply_cv_applicant';
                    $temp_user = 'text/apply_cv_applicant';
                    
                    //set subject
                    $subj_text = Configure::read('email_apply.user_subject_apply_cv_applicant');
                    $subj_text = str_replace("{company_name}",$company_name,$subj_text);
                    $subj_text = str_replace("{job_name}",$job_name,$subj_text);
                    $subj_admin = $subj_text;
                    $subj_user = $subj_text;
                    
                }
					//sent part_time email
                    //set email use
                    $temp_corporate = 'text/apply_cv_corporation';
                    
                    //set subject
                    //$subj_corporate = '【コールナビ】求人に応募がありました。';
                    $subj_corporate = Configure::read('email_apply.corporation_subject_apply_cv_corporation');
            }else{
                	//sent not part_time email
                    //set email use
                    $temp_admin = 'text/apply_cv_applicant';
                    $temp_user = 'text/apply_cv_applicant';
                    $temp_corporate = 'text/apply_cv_corporation';
                    
                    //set subject
                    $subj_text = Configure::read('email_apply.user_subject_apply_cv_applicant');
                    $subj_text = str_replace("{company_name}",$company_name,$subj_text);
                    $subj_text = str_replace("{job_name}",$job_name,$subj_text);                    
                    $subj_admin = $subj_text;
                    $subj_user = $subj_text;
                    $subj_corporate = Configure::read('email_apply.corporation_subject_apply_cv_corporation');
                    
            }     

        }else{
            // sent mail with out cv
            if($part_time == true)
            {
                //sent part_time email
                    //set email use
                    $temp_admin = 'text/apply_nocv_applicant';
                    $temp_user = 'text/apply_nocv_applicant';
                    $temp_corporate = 'text/apply_nocv_corporation';
                    
                    //set subject
                    $subj_text = Configure::read('email_apply.user_subject_apply_nocv_applicant');
                    $subj_text = str_replace("{company_name}",$company_name,$subj_text);
                    $subj_text = str_replace("{job_name}",$job_name,$subj_text);                    
                    $subj_admin = $subj_text;
                    $subj_user = $subj_text;
                    $subj_corporate = Configure::read('email_apply.corporation_subject_apply_nocv_corporation');
                    
            }else{
                //sent not part_time email
                    //set email use
                    $temp_admin = 'text/apply_nocv_applicant';
                    $temp_user = 'text/apply_nocv_applicant';
                    $temp_corporate = 'text/apply_nocv_corporation';
                    
                    //set subject
                    $subj_text = Configure::read('email_apply.user_subject_apply_nocv_applicant');
                    $subj_text = str_replace("{company_name}",$company_name,$subj_text);
                    $subj_text = str_replace("{job_name}",$job_name,$subj_text);                    
                    $subj_admin = $subj_text;
                    $subj_user = $subj_text;
                    $subj_corporate = Configure::read('email_apply.corporation_subject_apply_nocv_corporation');
                    
            }  
        }
               
		//--- 0 ----
        //Default find the ctp file in View/Emails
        $this->Sendmail->render($temp_user,$data);

        //Send from Address and from Name
        $this->Sendmail->fromlName = Configure::read('webconfig.mail_from_name');
        $this->Sendmail->fromlAdress = Configure::read('webconfig.mail_from');
        
        
        // validate sent mail not sent to another  delete
        //if($sendMailCv == false){
            //Send: to Email, to Name, subject
            $this->Sendmail->addBCC(Configure::read('email_apply.bcc'));
            $result = $this->Sendmail->send($email_user,$toname_user, $subj_user);
            CakeLog::write('activity', 'mail user template sent to user bcc to admin');
        //}
        
		//--- 1 ----
		/*Send mail to Admin*/
        //Default find the ctp file in View/Emails
        //$this->Sendmail->render($temp_admin,$data);

        //bcc sent to admin
        //$this->Sendmail->addBCC(Configure::read('email_apply.bcc'));

        //Send: to Email, to Name, subject
        //$result = $this->Sendmail->send($email_admin,$toname_admin, $subj_admin);
        
        //CakeLog::write('activity', 'mail user template sent to admin bcc to admin');

        //--- 2 ----
		/*Send mail to Corporation*/
        //Default find the ctp file in View/Emails
        $this->Sendmail->render($temp_corporate,$data);
		//bcc
		$this->Sendmail->addBCC(Configure::read('email_apply.bcc'));
        //Send: to Email, to Name, subject
        $result = $this->Sendmail->send($email_corporate,$toname_corporate, $subj_corporate);

        CakeLog::write('activity', 'mail company template sent to company bcc to admin');

        // sending mail for getting cv
        if($sendMailCv){
            $temp_pass = "text/apply_sent_password";
            $subj_pass = Configure::read('email_apply.corporation_subject_apply_sent_password');
                
			//Default find the ctp file in View/Emails
			$this->Sendmail->render($temp_pass,$data);


            $this->Sendmail->addBCC(Configure::read('email_apply.bcc'));
			//Send: to Email, to Name, subject
			$result = $this->Sendmail->send($email_corporate,$toname_corporate, $subj_pass);
    
            CakeLog::write('activity', 'mail company password template sent to company bcc to admin');
        
        }
        
        return true;
        
    }
    
    
    private function sent_id_mail($arr, $apply_code)
    {
        //cheking the sent mail data is have or not
        if(
			empty($arr['step1Email']) ||
			empty($arr['step1FullName']) ||
			empty($arr['step1Birth']) ||
			empty($arr['step1gender']) ||
			empty($arr['step1Phonetic']) ||
			empty($arr['step1Phone']) ||
			empty($arr['job_id'])
		)
		{
			return false;
		}

        $id = $arr['job_id'];
		
		if(intval($arr['job_id']) < 1)
		{
			return false;
		}
        
        //set the arr data for sent email
        $data = array(
            'arr' => array(
                'apply_code' => $apply_code,
                'email' => $arr['step1Email'],
            )
        );
        
         $temp_user = 'text/apply_test_sent_code';
       

        //Default find the ctp file in View/Emails
        $this->Sendmail->render($temp_user,$data);

        //Send from Address and from Name
        $this->Sendmail->fromlName = Configure::read('webconfig.mail_from_name');
        $this->Sendmail->fromlAdress = Configure::read('webconfig.mail_from');

        //Send: to Email, to Name, subject
        $result = $this->Sendmail->send($arr['step1Email'],'nama', '【コールナビ】お祝い金申請完了のおしらせ。');

        return true;
    }    

	
	/*********** Mails start ***********/
	
	/**
	 * Created at: 2017-09-05
	 * Author: Xuong
	 */
	private function send_mail_to_apple()
	{
		//building...
		return 0;
	}
	
	/**
	 * Created at: 2017-09-05
	 * Author: Xuong
	 */
	private function send_mail_to_admin()
	{
		//building...
		return 0;
	}
	
	/**
	 * Created at: 2017-09-05
	 * Author: Xuong
	 */
	private function send_mail_to_user()
	{
		//building...
		return 0;
	}
	
	/**
	 * Created at: 2017-09-05
	 * Author: Xuong
	 */
	private function send_mail_to_corporation()
	{
		//building...
		return 0;
		
		$this->send_mail_method($mail_address, $mail_name, $subject, $template, $data, true);
	}
	
	/**
	 * Created at: 2017-09-05
	 * Author: Xuong
	 */
	private function send_mail_password_to_corporation($mail_address, $mail_name, $data, $sendMailCv=false)
	{
		if($sendMailCv){
            $template = Configure::read('email_apply.corporation_template_apply_sent_password');
            $subject = Configure::read('email_apply.corporation_subject_apply_sent_password');
			$this->send_mail_method($mail_address, $mail_name, $subject, $template, $data);
        }
	}
	
	/*********** Mails end ***********/
	
	private function send_mail_method($mail_address, $mail_name, $subject, $template, $data, $bcc=false)
	{
		//Default find the ctp file in View/Emails
		$this->Sendmail->render($template,$data);
		
		$this->Sendmail->fromlName = Configure::read('webconfig.mail_from_name');
		$this->Sendmail->fromlAdress = Configure::read('webconfig.mail_from');
		
		if($bcc)
		{
			$this->Sendmail->addBCC(Configure::read('email_apply.bcc'));
		}
		
		//Send: to Email, to Name, subject
        $result = $this->Sendmail->send($mail_address,$mail_name, $subject);
		
		return $result;
	}
}