<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
App::uses('CakeText', 'Utility');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class TestController extends AppController {

  /**
   * This controller does not use a model
   *
   * @var array
   */
  public $uses = array('');
  public $helpers = array('Paginator','Html');
  public $components = array('Imagethumb', 'SearchRoute', 'MixSearch');
  public $paginate = array();
  /**
   * Displays a view
   *
   * @return void
   * @throws NotFoundException When the view file could not be found
   *	or MissingViewException in debug mode.
   */
  public function index() {
    $data = $this->Imagethumb->get_pic_modules('blogs/img_cat.jpg', $w=300,$h=300,1,"1:1");

    echo $data;
    $this->autoRender = false;
  }
	
  public function area(){
//	  $this->SearchRoute->matchUrl(array('東京都',
//										 '世田谷区',
//										 '南烏山',
//										 '正社員',
//										 '服装髪型自由',
//										 '時短勤務')
//								  );
	  $this->SearchRoute->matchUrl(array('東京都','世田谷区','入社祝い金'));
	 
  	 var_dump($this->SearchRoute->getPars());
	 die;
  }

	public function preview()
	{
		$this->layout = 'single_column';
		$rs = $this->MixSearch->getMixSearch($this->SearchRoute->getPars());
		$this->set('mix_search', $rs);
	}
    public function getMixSearch($page)
    {
        $this->MixSearch->test(10, 1000, $page, 20, 1, 4);
    }
}
