<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
App::uses('CakeText', 'Utility');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ProxyController extends AppController
{
	
	/**
	 * This controller does not use a model
	 *
	 * @var array
	 */
	public $uses = array();
	public $helpers = array();
	public $components = array();
	/**
	 * Displays a view
	 *
	 * @return void
	 * @throws NotFoundException When the view file could not be found
	 *	or MissingViewException in debug mode.
	 */
	public function index()
	{
		
		//Check isset of URL
		if (!isset($this->request->query['url']))
		{
			die('empty url');
		}
		
		
		$url = $this->request->query['url'];
		if(strpos($url,'//') === 0)
		{
			$url = 'http:'.$url;
		}
		
		//Check invalid URL
		if (filter_var($url, FILTER_VALIDATE_URL) === false)
		{
			die('invalid url');
		}
		$this->layout = null;
		echo $this->getSslPage($url);
		die;
	}
	
	
	private function getSslPage($url)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_REFERER, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$result = curl_exec($ch);
		$ctype  = "image/jpeg";
		header('Content-type: ' . $ctype);
		curl_close($ch);
		return $result;
	}
	
	private function getNormalPage($url)
	{
		$user_agent = 'Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0';
		$options    = array(
			CURLOPT_RETURNTRANSFER => true, // return web page
			CURLOPT_HEADER => true, // return headers
			CURLOPT_FOLLOWLOCATION => true, // follow redirects
			CURLOPT_ENCODING => "", // handle all encodings
			CURLOPT_AUTOREFERER => true, // set referer on redirect
			CURLOPT_CONNECTTIMEOUT => 120, // timeout on connect
			CURLOPT_TIMEOUT => 120, // timeout on response
			CURLOPT_MAXREDIRS => 10, // stop after 10 redirects
			CURLOPT_USERAGENT => $user_agent
		);
		
		$ch = curl_init($url);
		curl_setopt_array($ch, $options);
		$remoteSite = curl_exec($ch);
		$header     = curl_getinfo($ch);
		curl_close($ch);
		
		if (!$remoteSite)
		{
			die('response empty');
		}
		
		
		list($h, $image) = explode("\r\n\r\n", $remoteSite, 2);
		
		if (!$image)
		{
			die('data empty');
		}
		
		
		
		if (isset($header['content_type']))
		{
			if ($header['content_type'] == 'image/jpeg' || $header['content_type'] == 'image/png' || $header['content_type'] == 'image/gif')
			{
				header('Content-Type: ' . $header['content_type']);
				echo $image;
				die;
			}
			else
			{
				die('invalid content type');
			}
		}
		else
		{
			die('no content type');
		}
	}
	
}
