<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
App::uses('CakeText', 'Utility');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class SearchController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $helpers = array('Paginator','Html');
    public $components = array('Session', 'CloudSearch','Util', 'Search', 'SearchRoute', 'MixSearch');
    public $paginate = array();
	public $uses = array('Keywords', 'ClickedJob', 'CareerOpportunity', 'Area');
/**
 * Displays a view
 *
 * @return void
 * @throws NotFoundException When the view file could not be found
 *	or MissingViewException in debug mode.
 */
	// public function index() {	
	// 	$size = 30;
	// 	$render = 'index';
	// 	$params = $this->request->query;
	// 	$page = isset($this->request->query['page'])?intval($this->request->query['page']):1;
		
	// 	if($this->is_mobile)
	// 	{
	// 		$size = 20;
	// 		$this->layout = 'single_column_sp';
	// 		$render = 'index-sp';
	// 	}
	// 	else
	// 	{
	// 		$this->layout = 'single_column';
	// 	}
	// 	$searchResult = $this->_search($params, $page, $size);
	// 	$secretResult = $this->_secret($params, $page, 5);
		
	// 	//$this->_savedKeyword($params, $searchResult[1]);
	// 	$hotkey_data = $this->Keywords->getHotkey();

	// 	$area_data = Configure::read('webconfig.area_data');
	// 	$employment_data = Configure::read('webconfig.employment_data');

	// 	$this->set('size', $size);
	// 	$this->set('params', $params);
	// 	$this->set('search_result', $searchResult[0]);
	// 	$this->set('secret_result', $secretResult);
	// 	$this->set('search_total', $searchResult[1]);

	// 	$this->set('area_data', $area_data);
	// 	$this->set('employment_data', $employment_data);
	// 	$this->set('hotkeyList', $hotkey_data);
	// 	$this->set('currentKeyWord', $this->newGenCurrentKeyWord($params));

	// 	$this->setValueOnView($params, $area_data, $employment_data, $hotkey_data);
	// 	$this->render($render);
	// }

	public function index() {
		
		$params = $this->Search->getParams($this->request->query);
		$this->searchAction($params);
	}
	
	public function seoSearch()
	{
		$query = $this->request->query;
		$this->SearchRoute->matchUrl($this->passedArgs);
		$params = $this->SearchRoute->getPars();
		$params['page'] = isset($query['page'])?intval($query['page']):1;
		$this->request->query = $this->SearchRoute->getQuery();
		$this->searchAction($params);
		
	}

	public function ajaxSearch() {		
		
		$size = 30;
		$params = $this->Search->getParams($this->request->query);
		$page = $params['page'];
		
		if($this->is_mobile)
		{
			$size = 20;
		}
		
		$this->layout = 'ajax';
		$this->render(false);
		
		$searchResult = $this->MixSearch->getMixSearch($params, $page);
		
		if($searchResult['current_total'])
		{
			$searchResult = $this->MixSearch->asynProcess($searchResult);
			echo json_encode($searchResult);
		}
		else
		{
			echo json_encode(array());
		}
	}
	
	public function countcondition()
	{
		$params = $this->request->query;
		if(!empty($params['area']))
		{
			$params['list_key_area'] = $params['area'];
		}
		$params = $this->Search->convertParamsKeyToText($params);

		$customPassedArgs = $this->SearchRoute->customPassedArgs($params);
		$this->SearchRoute->matchUrl($customPassedArgs);
		$params = $this->SearchRoute->getPars();
		
		$searchResult = $this->MixSearch->getCountOnly($params);
		//$searchResult = $this->_search($params, 1, 0);
		echo $searchResult;
		die;
	}
	
	public function job($id=null)
	{
		/*
			 null -> Redirect to Top of search page
			 $id -> Find in Amazon DB -> Result:
				1: -> Link -> Save click -> Redirect to Link
				2: -> Nothing -> Redirect to Top of search page
		*/
		if($id)
		{			
			$decodeId = $this->Util->jobUrlDecode($id);
			$data = $this->CloudSearch->searchById($decodeId);
			if(!empty($data))
			{
				$result = $this->ClickedJob->raiseUpClick($data);
				if(!empty($data['url']))
				{
					//URL
					$redirectUrl = $data['url'];
					
					/*
						For only site_id = 3
						Change url
						http://doda.jp/DodaFront/View/JobSearchDetail/j_jid__3001251007/-tab__jd/
						To
						http://doda.jp/DodaFront/View/JobSearchDetail.action?jid=3001251007&ndrs=a55dfe9600b751
						
						With {3001251007} is dynamic ID
					*/
					if($data['site_id'] == 3)
					{
						$output_array = array();
						preg_match("/\/j_jid__(\w+)\//", $data['url'], $output_array);
						
						if(isset($output_array[1]))
						{
							$redirectUrl = 'http://doda.jp/DodaFront/View/JobSearchDetail.action?jid='.$output_array[1].'&ndrs=a55dfe9600b751';
						}
					}
					///j_jid__(\w+)/
					
					return $this->redirect($redirectUrl);
				}
			}
		}

		return $this->redirect(array('controller' => 'search', 'action' => 'index'));
	}
	
	
	public function urlEncode($unique_id)
	{
		return $this->Util->jobUrlEncode($unique_id);
	}
	
	/*
		This Action for old links of search jobs can work on new CALLNAVI.
		
		Input: https://callnavi.jp/search/result?page=82&area=滋賀or三重or京都or奈良or大阪or和歌山or兵庫&keyword=
		Ouput: 
			If area is many places -> change to keyword
			else one place -> area
	*/
	public function result()
	{
		//TODO: result
		$input = $this->request->query;
		
		if(isset($input['keyword']))
		{
			$input['keyword'] = str_replace(array('or', 'OR'), ',', $input['keyword']);
		}
		
		if(isset($input['area']))
		{
			$arrArea = explode('or', $input['area']);
			$strArea = '';
			if(count($arrArea) > 1)
			{
				$strArea = implode(',', $arrArea);
			}
			else
			{
				$strArea = $input['area'];
			}
			
			if(isset($input['keyword']))
			{
				if(!empty($strArea))
				{
					$strArea .= ',';
				}
				$input['keyword'] = $strArea . $input['keyword'];
			}
			else
			{
				$input['keyword'] = $strArea;
			}
			unset($input['area']);
		}
		
		
		$output = http_build_query($input);
		return $this->redirect('/search?'.$output);
	}
	
	//Update result over one hour
	public function updateHotKeyResult()
	{
		$arrKeywords = $this->Keywords->getHotKeyOverOneHour();
		//Conditions: when search only Hotkey and Hotkey only 1 value
		if(count($arrKeywords) >= 1)
		{
			foreach($arrKeywords as $keyword)
			{
				$hotkey = $keyword['keywords']['id'];
				if($hotkey)
				{
					$params = array('hotkey' => $hotkey);
					$searchResult = $this->_search($params, 1, 0);
					
					$result = $searchResult[1];
					$this->Keywords->setResultByKeyword($hotkey,$result);
				}
			}

			
		}
		die;
		header('Content-Type: image/gif');
		echo base64_decode('R0lGODlhAQABAJAAAP8AAAAAACH5BAUQAAAALAAAAAABAAEAAAICBAEAOw==');
		die;
	}
	
	public function getEmployment()
	{
		$employment_data = Configure::read('webconfig.employment_data');
		$employment_data['0'] =  '選択しない';
		echo json_encode($employment_data);
		die;
	}
	/*--------*/
	private function _search($params, $page, $size)
	{
		return $this->CloudSearch->search($params, $page, $size);
	}

	private function _secret($params, $page, $size)
	{
		return $this->CareerOpportunity->search($params, $page, $size);
	}
	
	//Update result of specify keyword
	private function _savedKeyword($params, $result)
	{
		//Conditions: when search only Hotkey and Hotkey only 1 value
		if(count($params) >= 1)
		{
			if(isset($params['hotkey']))
			{
				$hotkey = '';
				if(count($params['hotkey'])==1)
				{
					$hotkey = $params['hotkey'][0];
				}
				unset($params['hotkey']);
				foreach($params as $key=>$val)
				{
					if(!empty($val))
					{
						$hotkey = '';
						break;
					}
				}
				if($hotkey)
				{
					$this->Keywords->setResultByKeyword($hotkey,$result);
				}
			}
		}
	}


	private  function  genCurrentKeyWord($arr){
		$currentKeyWord ='';
		$area_data =Configure::read('webconfig.area_data');
		$employment_data= Configure::read('webconfig.employment_data');
		if(!empty($arr['area'])){
			$currentKeyWord .= $area_data[$arr['area']];
		}

		if(!empty($arr['em'])){
			$currentKeyWord .= ' / '.$employment_data[$arr['em']];
		}

		if(!empty($arr['keyword'])){
			$currentKeyWord .= ' / '.$arr['keyword'];
		}
		if(!empty($arr['hotkey'])){

			$hotkey_data = Configure::read('webconfig.hotkey_data');
			$hotKey=[];
			for($x= 0; $x< count($arr['hotkey']); $x++){
				$hotKey[$x] = $hotkey_data[$arr['hotkey'][$x]];
			}
			$hotKey = implode(' / ',$hotKey );
			$currentKeyWord .= ' / '.$hotKey;

		}

		return $currentKeyWord;
	}

	private  function  newGenCurrentKeyWord($arr){
		$currentKeyWord ='';
		$area_data =Configure::read('webconfig.area_data');
		$employment_data= Configure::read('webconfig.employment_data');
		if(!empty($arr['area'])){
			$currentKeyWord .= $arr['area'];
			$currentKeyWord = str_replace(","," / ", $currentKeyWord);
		}

		if(!empty($arr['em'])){
			$currentKeyWord .= ' / '.$arr['em'];
		}

		if(!empty($arr['keyword'])){
			$currentKeyWord .= ' / '.$arr['keyword'];
		}
		if(!empty($arr['hotkey'])){

			$hotkey_data = Configure::read('webconfig.hotkey_data');
			$hotKey=[];
			for($x= 0; $x< count($arr['hotkey']); $x++){
				$hotKey[$x] = $hotkey_data[$arr['hotkey'][$x]];
			}
			$hotKey = implode(' / ',$hotKey );
			$currentKeyWord .= ' / '.$hotKey;

		}
		return $currentKeyWord;
	}
	
	private function contentProcess($item)
	{
		//TODO: youshouldwritesomethinghere
		//---------------------------------------------------
		if(empty($item['pic_url']))
		{
			$item['pic_url'] = '/img/instead/img_no-image_' . sprintf("%02d", rand(1, 21)) . '.jpg';
		}
		else
		{
			if($item['pic_url'][0] != "/")
			{
				$item['pic_url'] = $this->Util->proxy_image($item['pic_url']);
			}
		}
		
		$item['salary'] 	= strip_tags($item['salary']);
		if(mb_strlen($item['salary'])>50)
		{
			$item['salary'] = mb_substr($item['salary'],0,50,'UTF-8').'...';
		}

		$item['clip_title'] = mb_substr(strip_tags($item['title']),0,65,'utf-8');

		if(mb_strlen($item['company'])>25)
		{
			$item['company'] = mb_substr($item['company'],0,25,'UTF-8').'...';
		}


		$item['isNew'] = 0;
		//604800 = 7 days
		$timeInAWeek = time()-604800;
		if($item['updatetime'] > $timeInAWeek)
		{
			$item['isNew'] = 1;
		}

		//Sort category follows length
		usort($item['labels'], function($a, $b) {
			return strlen($a) - strlen($b);
		});

		$item['maxCatLeng'] = 30;

		$encodeId = $this->Util->jobUrlEncode($item['unique_id']);
		$item['href'] = '/search/job/'.$encodeId;
		unset($item['workplace']);
		unset($item['desc']);
		return $item;
	}

	private function setValueOnView($params, $area_data, $employment_data, $hotkey_data)
	{
		/*Setting*/
		$_keyResult = '';
		$_keyCurrent = '';
		$_area = '';
		$_keyword = '';
		$_em = '';
		$_selectedArea = 0;
		$_selectedEm = 0;
		$_checkedHotkey = [];

		$_hotkeyList_Checked = [];

		if(!empty($params['hotkey']))
		{
			$_checkedHotkey = $params['hotkey'];
			$hotkeyList = Configure::read('webconfig.hotkey_data');
			foreach($_checkedHotkey as $key=>$val)
			{
				$hotkeyValue = $hotkeyList[$val];
				//$_keyResult .= "「{$hotkeyValue}」";
				$_hotkeyList_Checked[$key] = $hotkeyValue;
			}
		}

		if(!empty($params['area']))
		{
			//$_area = isset($area_data[$params['area']])? $area_data[$params['area']]:'';
			$_area = isset($params['area'])? $params['area']:'';
			$_keyResult .= empty($_area)?'':"「{$_area}」";
		}

		
		if(!empty($params['em']))
		{
			//$_em = isset($employment_data[$params['em']])? $employment_data[$params['em']]:''; 
			$_em = isset($params['em'])? $params['em']:''; 
			if(!empty($_em))
			{
				$_keyResult .= "「{$_em}」";
			}
		}

		if(!empty($params['keyword']))
		{
			$_keyword = $params['keyword'];
			$_keyResult .= "「{$_keyword}」";
			
		}

		if(!empty($params['area']))
		{
			$_selectedArea = intval($params['area']);
		}

		if(!empty($params['em']))
		{
			$_selectedEm = intval($params['em']);
		}

		$currentPage = isset($params['page'])?$params['page']:1;
		$this->set('_keyResult',$_keyResult);
		$this->set('_keyCurrent',$_keyCurrent);
		$this->set('_area',$_area);
		$this->set('_keyword',$_keyword);
		$this->set('_em',$_em);
		$this->set('_selectedArea',$_selectedArea);
		$this->set('_selectedEm',$_selectedEm);
		$this->set('_checkedHotkey',$_checkedHotkey);
		$this->set('currentPage', $currentPage);
		$this->set('_hotkeyList_Checked', $_hotkeyList_Checked);
		// $area_data = Configure::read('webconfig.area_data');
		// $employment_data = Configure::read('webconfig.employment_data');
		// $hotkeyList = $this->requestAction('Elements/getHotKeys');
		// $__checkedHotkey = array();
	}

	private function searchAction($params)
	{
		//print_r($params);
		$size = 30;
		$render = 'index';
		$valueEm = $params['em'];
		$page = $params['page'];
		
		if($this->is_mobile)
		{
            $heightFooter = 'height-730';
            $this->set('heightFooter',$heightFooter);
			$size = 20;
			$this->layout = 'single_column_sp';
			$render = 'index-sp';
//			$searchResult = $this->_search($params, $page, $size);
//			$secretResult = $this->_secret($params, $page, 5);
//			
//			$this->set('search_result', $searchResult[0]);
//			$this->set('secret_result', $secretResult);
//			$this->set('search_total', $searchResult[1]);
		}
		else
		{
			$this->layout = 'single_column';
			$render = 'index_mix';
	
		}
		
		$searchResult = $this->MixSearch->getMixSearch($params, $page);
		$this->set('search_result', $searchResult);
		$this->set('search_total', $searchResult['total_data']);

	
		$hotkey_data = $this->Keywords->getHotkey();

		$area_data = Configure::read('webconfig.area_data');
		$employment_data = Configure::read('webconfig.employment_data');
		$em = '';
		if(isset($params['data_em']))
		{
			if($params['data_em'])
			{
				$em = $employment_data[$params['data_em']];
			}
		}
		$params['em'] = $em;

		if(empty($params['area']) && !empty($params['group']))
		{
			$params['area'] = $params['group'];
		}
		$this->set('isSearch', 'Search');
		$this->set('valueEm', $valueEm);
		$this->set('size', $size);
		$this->set('params', $params);
		$this->set('area_data', $area_data);
		$this->set('employment_data', $employment_data);
		$this->set('hotkeyList', $hotkey_data);
		$this->set('currentKeyWord', $this->newGenCurrentKeyWord($params));
		$this->set('request_query', $this->request->query);
		$this->setValueOnView($params, $area_data, $employment_data, $hotkey_data);
		$this->render($render);
	}
}
