<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
App::uses('CakeText', 'Utility');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class FeedbackController extends AppController {

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array('Feedback');
    public $helpers = array();
    public $components = array('Session','Sendmail');
    public $paginate = array();

	/*
		@type: Ajax
		@pars: STRING_T message
		@response: {1: success, 0: failed, -1: spam}
		@avoid_spam: session
		
		@flow: 
			1. check message and session
			2. insert to feedback table
			3. send mail to admin
			4. response
			5. done
	*/
    public function sendmail(){
		
		$this->layout = 'ajax'; 
		$this->render(false);
		
		if($this->Session->check('__feedback'))
		{
			//Avoid spam
			echo -1;
			die;
		}
		
		if(!isset($this->request->data['message']) &&  
		   empty(trim($this->request->data['message']))
		  )
		{
			//Invalid message
			echo 0;
			die;
		}
		$message = trim($this->request->data['message']);
		$data = array('message' => $message);
		
		//TODO: Save to database
		$this->Feedback->create();
		$this->Feedback->save($data);
		
		
		//TODO: send mail
		
		//template
        $temp = 'text/feedback_mail';
        
        //Send from Address and from Name
        $this->Sendmail->fromlName = 'Callnavi窓口';
        $this->Sendmail->fromlAddress = 'no-reply@callnavi.jp';

        //send mail to admin
        //Default find the ctp file in View/Emails
        $this->Sendmail->render($temp,$data);

        //Send: to Email, to Name, subject
        $resultAdmin = $this->Sendmail->send('info@callnavi.jp','管理者', 'Callnavi:意見');
		$this->Session->write('__feedback', 1);
		
		//Success anyway
		echo 1;
    }
}
