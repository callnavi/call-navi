<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
App::uses('CakeText', 'Utility');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class OiwaikinController extends AppController
{

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array('Oiwaikin');
    public $components = array('Session', 'Cookie', 'Util','Sendmail');
    public $helpers = array('Paginator','Html','Form');


    private function SendMail($arr)
    {
        $data = array(
            'arr' => array(
                'entryId' => $arr['entryId'],
                'complete' => $arr['complete'],
                'name' => $arr['name'],
                'mail' => $arr['mail'],
                'company' => $arr['company'],
                'startDay_year' => $arr['startDay_year'],
                'startDay_month' => $arr['startDay_month'],
                'startDay_day' => $arr['startDay_day'],
                'account_lastName' => $arr['account_lastName'],
                'account_firstName' => $arr['account_firstName'],
                'remarks' => $arr['remarks']
                
                
               
            )
        );
        
        $temp_admin = 'text/oiwaikin';
        $temp_user = 'text/oiwaikin_user_thanks';
       

        //Default find the ctp file in View/Emails
        $this->Sendmail->render($temp_user,$data);

        //Send from Address and from Name
        $this->Sendmail->fromlName = Configure::read('webconfig.mail_from_name');
        $this->Sendmail->fromlAdress = Configure::read('webconfig.mail_from');

        //Send: to Email, to Name, subject
        $result = $this->Sendmail->send($arr['mail'],$arr['name'], '【コールナビ】お祝い金申請完了のおしらせ。');

        //send mail to admin
        //Default find the ctp file in View/Emails
        $this->Sendmail->render($temp_admin,$data);

       
        //Send: to Email, to Name, subject
        $resultAdmin = $this->Sendmail->send($arr['mail'],'管理者', '【コールナビ】お祝い金の申請がありました。');
        //$resultAdmin = $this->Sendmail->send('kyujin@callnavi.jp','管理者', '【コールナビ】お祝い金の申請がありました。');


        //If error will be appear in this function
        //pr($this->Sendmail->error());
        //
        //die;
		return true;
    }


    public function index() 
    {
        if($this->Session->check('oiwaikin_data'))
		{
			$post = $this->Session->read('oiwaikin_data');
			$this->set('post', $post);
		}else{
           $this->set('post', array(
				'entryId' => '',
				'complete' =>  '',
				'name' => '',
				'mail' => '',
				'company' => '',
				'startDay_year' => '',
				'startDay_month' => '',
				'startDay_day' => '',
				'account_lastName' => '',
                'account_firstName' => '',
                'remarks' => '',
			));
        }
         if($this->Session->check('oiwakin_date_error'))
		{
            $date_error = $this->Session->read('oiwakin_date_error');
			$this->set('date_error', $date_error);    
         }
        
       	if($this->is_mobile)
		{
           // pr($this->is_mobile);die;
			$this->layout = 'oiwaikin_sp_layout';
			$this->render('index_sp');
		}else
        {
            $this->layout ="oiwaikin_layout";
            
        }

    }
	
	public function confirm(){
        if($this->request->is('post'))
		{
			$data = $this->request->data;
            $this->set('data', $data);
            
            $this->Session->write('oiwaikin_data',$data);
            
            
        }else{
            $this->redirect('/oiwaikin/');
        }
        

        if($this->Session->check('oiwaikin_data')){
            $data = $this->Session->read('oiwaikin_data');
            $newdate = $this->combineDateString($data['startDay_year'],$data['startDay_month'],$data['startDay_day']);
            
            list($y, $m, $d) = explode("-", $newdate);
            
            if(checkdate($m, $d, $y)){
               
                
            }else{
                $this->Session->write('oiwakin_date_error',$data);
                $this->redirect('/oiwaikin/');
            }
        }
                    
        
       	if($this->is_mobile)
		{
           // pr($this->is_mobile);die;
			$this->layout = 'oiwaikin_sp_layout';
            $this->render('confirm_sp');
		}else
        {
            $this->layout ="oiwaikin_layout";
			$this->render('confirm');
            
        }
		
		
	}
	
	public function thanks(){
        $resultQuery = "";
        $r = false;
        $data = "";
        if($this->Session->check('oiwaikin_data')){
         $data = $this->Session->read('oiwaikin_data');
            $result = $this->Oiwaikin->add_new_data($data);
            $result = true;
        }else{
            $this->redirect('/oiwaikin/');
        }
        
        if($result == true){
            $rs = $this->SendMail($data);
            $this->Session->delete('oiwaikin_data');
        }else{
            $this->redirect('/oiwaikin/');
            
        }
        
        if($rs = false){
            $this->redirect('/oiwaikin/');
        }
        
        $this->set('data', $data);
        if($this->is_mobile)
		{
           // pr($this->is_mobile);die;
			$this->layout = 'oiwaikin_sp_layout';
			$this->render('thanks_sp');
		}else
        {
            $this->layout ="oiwaikin_layout";
            $this->render('thanks');
        }
		
	}

    public function test_sendmail()
    {
//		var_dump($arr);
//		var_dump(Configure::read('EMAIL'));
//		var_dump($success);
//		die;
        $default = array(
            'transport' => 'Smtp',
            //        'from' => array('jellyfisherror@gmail.com' => 'Hikaku-jan.com'),
            //        'host' => 'ssl://smtp.gmail.com',
            //        'port' => 465,
            //        'timeout' => 30,
            //        'username' => 'jellyfisherror@gmail.com',
            //        'password' => 'Jellyfish74189532',
            //        'client' => null,
            //        'log' => true,
            //        'emailFormat' => 'html',
            //        'charset' => 'utf-8',
            //        'headerCharset' => 'utf-8',

            /*      'host' => 'ssl://smtp.gmail.com',
                    'port' => 465,
                    'timeout' => 30,
                    'from' => array('info@callnavi.jp' => 'Callnavi??'),
                    'username' => 'info@jellyfishhr.com',
                    'password' => 'ABCD@123.com',
                    'client' => null,
                    'log' => false,
                    'charset' => 'utf-8',
                    'auth'=>true*/

            'host' => 'localhost',
            'port' => 25,
            'timeout' => 30,
            'from' => array('no-reply@callnavi.jp' => 'Callnavi'),
            'client' => null,
            'log' => false,
            'charset' => 'utf-8',
            'auth' => true
        );
        $AppController = new AppController;
        $temp = 'secret';
        $temp_admin = 'secret_admin';
        $mail_to_company = 'cuong.trinh@jellyfishhr.com';
        $subject = "test mail";
        $arr['job_name'] = "job name";
        $arr['company_name'] = "company name";
        $arr['link'] = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['SERVER_NAME'] . Router::url(array('controller' => 'secret', 'action' => 'detail', 1), false);
        $success = $AppController->send_email(
            'default',
            ['cuong.trinh@jellyfishhr.com' => 'Callnavi窓口'],
            $mail_to_company,
            $subject,
            $message = null,
            $format = 'text',
            $template = $temp_admin,
            array('arr' => $arr),
            'cuong.trinh@jellyfishhr.com',
            $default
        );

        var_dump($arr);
        var_dump(Configure::read('EMAIL'));
        var_dump($success);
        die;
    }

    /*--------*/
    private function _search($params, $page, $size)
    {
        return $this->CloudSearch->search($params, $page, $size);
    }
 
    private function combineDateString($year, $month, $day){
        $year_str = '';
        $month_str = '';
        $day_str = '';
        
        $year_str = $year ;
        $month_str = $month ;
        $day_str = $day ;
        
        //set string to date
        $time = $year_str . "-" .$month_str . "-" .  $day_str ;
        
        //convert string to date
        $date_ret = date("Y-m-d", strtotime($time));

        //return date time
        return  $time ;
                            
    }

  
}