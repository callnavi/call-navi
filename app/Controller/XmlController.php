<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
App::uses('CakeText', 'Utility');
App::uses('Xml', 'Utility');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class XmlController extends AppController 
{
  public $uses = array('BlogsCategory', 'Blogs', 'Categories', 'Contents', 'CareerOpportunity', 'Area');

  public function index($category_alias= null) {
    $data_type = array();

    // get date
    $data['urlset']['xmlns:'] = 'http://www.sitemaps.org/schemas/sitemap/0.9';
    $data['urlset']['xmlns:xsi'] = 'http://www.w3.org/2001/XMLSchema-instance';

    // type 1
    $type_1 = $this->get_data_type_1();
    $type_3 = $this->get_data_type_3();
    $type_2 = $this->get_data_type_2();

    // Megre data.        
    $data_type = array_merge($data_type, $type_1, $type_3, $type_2);
    
    $fullBaseUrl = FULL_BASE_URL;

    foreach ($data_type as $key => $value) {
      $url = $value['loc'];
      $url = str_replace($fullBaseUrl, "" , $url);
      $url = urlencode($url);
      $url = str_replace("%2F", "/" , $url);
      $data_type[$key]['loc'] = $fullBaseUrl . $url;
    }

    $data['urlset']['url'] = $data_type;
    $xmlObject = Xml::fromArray($data);
    $xmlString = $xmlObject->asXML();

    // File xml
    $fileLocation = WWW_ROOT."sitemap.xml";
    $file = fopen($fileLocation,"w");
    fwrite($file,$xmlString);
    fclose($file);


    pr(count($data_type));die;
  }

  public function get_data_type_1(){
    $results = array();
    $results[] = array('loc' => FULL_BASE_URL, 'priority' => '1');
    $results[] = array('loc' => FULL_BASE_URL.'/search', 'priority' => '0.8');
    $results[] = array('loc' => FULL_BASE_URL.'/contents', 'priority' => '0.8');
    $results[] = array('loc' => FULL_BASE_URL.'/contents/働く人の声', 'priority' => '0.8');
    $results[] = array('loc' => FULL_BASE_URL.'/contents/スキル・テクニック', 'priority' => '0.8');
    $results[] = array('loc' => FULL_BASE_URL.'/contents/採用・面接', 'priority' => '0.8');
    $results[] = array('loc' => FULL_BASE_URL.'/contents/ビジネス・キャリア', 'priority' => '0.8');
    $results[] = array('loc' => FULL_BASE_URL.'/contents/注目トピックス', 'priority' => '0.8');
    $results[] = array('loc' => FULL_BASE_URL.'/contents/エリア特集 ', 'priority' => '0.8');
    $results[] = array('loc' => FULL_BASE_URL.'/secret', 'priority' => '0.8');
    $results[] = array('loc' => FULL_BASE_URL.'/callcenter_matome', 'priority' => '0.8');
    $results[] = array('loc' => FULL_BASE_URL.'/concept', 'priority' => '0.8');
    $results[] = array('loc' => FULL_BASE_URL.'/contact', 'priority' => '0.8');
    $results[] = array('loc' => FULL_BASE_URL.'/contact/thanks', 'priority' => '0.8');
    $results[] = array('loc' => FULL_BASE_URL.'/company', 'priority' => '0.8');
    $results[] = array('loc' => FULL_BASE_URL.'/blog', 'priority' => '0.8'); 

    return $results;
  }
  public function get_data_type_3(){
    $results = array();
    $blog = $this->get_data_type_3_blog();
    $content = $this->get_data_type_3_content();
    $secret = $this->get_data_type_3_secret();

    $results = array_merge($results, $blog, $content, $secret);
    return $results;
  }
  public function get_data_type_3_blog(){
    $results = array();

    // Blogs category
    $options_blogs_category['fields'] = array('BlogsCategory.cat_id', 'BlogsCategory.category_alias');
    $blogs_category = $this->BlogsCategory->find('list', $options_blogs_category);
    if(!empty($blogs_category)){
      foreach ($blogs_category as $alias) {
        $results[] = array('loc' => FULL_BASE_URL.'/blog/'.trim($alias), 'priority' => '0.7');
      }
    }

    // Blogs
    $options_blog['fields'] = array('Blogs.id', 'BlogsCategory.category_alias');
    $options_blog['joins'][] = array(
      'type' => 'LEFT', 
      'table' => 'blogs_category', 
      'alias' => 'BlogsCategory',
      'conditions' => array('BlogsCategory.cat_id = Blogs.cat_id')
    );
    $blog = $this->Blogs->find('list', $options_blog);
    if(!empty($blog)){
      foreach ($blog as $id => $alias) {
        $results[] = array('loc' => FULL_BASE_URL.'/blog/'.trim($alias).'/'.$id, 'priority' => '0.7');
      }
    }

    return $results;
  }
  public function get_data_type_3_content(){
    $results = array();

    // Category
    $options_categories['fields'] = array('id', 'category');
    $categories = $this->Categories->find('list', $options_categories);
    if(!empty($categories)){
      foreach ($categories as $alias) {
        $results[] = array('loc' => FULL_BASE_URL.'/contents/'.trim($alias), 'priority' => '0.7');
      }
    }

    // Contents
    $options_blog['fields'] = array('Contents.title', 'Categories.category');
    $options_blog['joins'][] = array(
      'type' => 'LEFT', 
      'table' => 'category', 
      'alias' => 'Categories',
      'conditions' => array('Categories.category_alias = Contents.category_alias')
    );
    $contents = $this->Contents->find('list', $options_blog);
    if(!empty($contents)){
      foreach ($contents as $title => $alias) {
        $results[] = array('loc' => FULL_BASE_URL.'/contents/'.trim($title), 
						   'priority' => '0.7');
      }
    }

    return $results; 
  }
  public function get_data_type_3_secret(){
    $results = array();

    $options['fields'] = array('id', 'job');
    $CareerOpportunity = $this->CareerOpportunity->find('list', $options);
    if(!empty($CareerOpportunity)){
      foreach ($CareerOpportunity as $job) {
        $results[] = array('loc' => FULL_BASE_URL.'/secret/'.trim($job), 
						   'priority' => '0.7');
      }
    }

    return $results; 
  }

  public function get_data_type_2(){
    $results = array();
    $priority_1 = 0.6;
    $priority_2 = 0.5;
    $priority_3 = 0.8;
    $priority_4 = 0.8;
    //23区
    $results[] = array('loc' => FULL_BASE_URL.'/search/千代田区', 'priority' => $priority_1);
    $results[] = array('loc' => FULL_BASE_URL.'/search/中央区', 'priority' => $priority_1);
    $results[] = array('loc' => FULL_BASE_URL.'/search/港区', 'priority' => $priority_1);
    $results[] = array('loc' => FULL_BASE_URL.'/search/新宿区', 'priority' => $priority_1);
    $results[] = array('loc' => FULL_BASE_URL.'/search/文京区', 'priority' => $priority_1);
    $results[] = array('loc' => FULL_BASE_URL.'/search/台東区', 'priority' => $priority_1);
    $results[] = array('loc' => FULL_BASE_URL.'/search/墨田区', 'priority' => $priority_1);
    $results[] = array('loc' => FULL_BASE_URL.'/search/江東区', 'priority' => $priority_1);
    $results[] = array('loc' => FULL_BASE_URL.'/search/品川区', 'priority' => $priority_1);
    $results[] = array('loc' => FULL_BASE_URL.'/search/目黒区', 'priority' => $priority_1);
    $results[] = array('loc' => FULL_BASE_URL.'/search/大田区', 'priority' => $priority_1);
    $results[] = array('loc' => FULL_BASE_URL.'/search/世田谷区', 'priority' => $priority_1);
    $results[] = array('loc' => FULL_BASE_URL.'/search/渋谷区', 'priority' => $priority_1);
    $results[] = array('loc' => FULL_BASE_URL.'/search/中野区', 'priority' => $priority_1);
    $results[] = array('loc' => FULL_BASE_URL.'/search/杉並区', 'priority' => $priority_1);
    $results[] = array('loc' => FULL_BASE_URL.'/search/豊島区', 'priority' => $priority_1);
    $results[] = array('loc' => FULL_BASE_URL.'/search/北区', 'priority' => $priority_1);
    $results[] = array('loc' => FULL_BASE_URL.'/search/荒川区', 'priority' => $priority_1);
    $results[] = array('loc' => FULL_BASE_URL.'/search/板橋区', 'priority' => $priority_1);
    $results[] = array('loc' => FULL_BASE_URL.'/search/練馬区', 'priority' => $priority_1);
    $results[] = array('loc' => FULL_BASE_URL.'/search/足立区', 'priority' => $priority_1);
    $results[] = array('loc' => FULL_BASE_URL.'/search/葛飾区', 'priority' => $priority_1);
    $results[] = array('loc' => FULL_BASE_URL.'/search/江戸川区', 'priority' => $priority_1);

    //雇用形態
    $results[] = array('loc' => FULL_BASE_URL.'/search/正社員', 'priority' => $priority_2);
    $results[] = array('loc' => FULL_BASE_URL.'/search/パート', 'priority' => $priority_2);
    $results[] = array('loc' => FULL_BASE_URL.'/search/アルバイト', 'priority' => $priority_2);
    $results[] = array('loc' => FULL_BASE_URL.'/search/契約社員', 'priority' => $priority_2);
    $results[] = array('loc' => FULL_BASE_URL.'/search/派遣', 'priority' => $priority_2);

    //都道府県
    $results[] = array('loc' => FULL_BASE_URL.'/search/北海道', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/青森県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/岩手県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/宮城県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/秋田県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/山形県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/福島県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/茨城県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/栃木県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/群馬県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/埼玉県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/千葉県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/東京都', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/神奈川県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/新潟県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/富山県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/石川県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/福井県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/山梨県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/長野県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/岐阜県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/静岡県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/愛知県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/三重県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/滋賀県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/京都府', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/大阪府', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/兵庫県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/奈良県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/和歌山県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/鳥取県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/島根県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/岡山県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/広島県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/山口県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/徳島県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/香川県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/愛媛県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/高知県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/福岡県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/佐賀県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/長崎県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/熊本県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/大分県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/宮崎県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/鹿児島県', 'priority' => $priority_3);
    $results[] = array('loc' => FULL_BASE_URL.'/search/沖縄県', 'priority' => $priority_3);


    //県庁所在地
    $results[] = array('loc' => FULL_BASE_URL.'/search/札幌', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/青森', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/盛岡', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/仙台', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/秋田', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/山形', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/福島', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/水戸', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/宇都宮', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/前橋', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/千葉', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/横浜', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/新潟', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/富山', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/金沢', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/福井', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/甲府', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/長野', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/岐阜', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/静岡', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/名古屋', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/津', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/大津', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/大阪', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/神戸', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/奈良', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/和歌山', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/鳥取', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/松江', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/岡山', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/広島', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/山口', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/徳島', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/高松', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/松山', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/高知', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/福岡', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/佐賀', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/長崎', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/熊本', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/大分', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/宮崎', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/鹿児島', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/那覇', 'priority' => $priority_4);
    $results[] = array('loc' => FULL_BASE_URL.'/search/埼玉', 'priority' => $priority_4);

    return $results;
  }

  // public function get_data_type_2(){
  //   $results = array();
  //   //group
  //   $group = $this->get_data_type_2_group();

  //   // area
  //   $province = $this->get_data_type_2_province();
  //   $city = $this->get_data_type_2_city();


  //   $results = array_merge($results, $group, $province, $city);
  //   return $results;
  // }


  public function get_data_type_2_group(){
    $results = array();
    $data_group = Configure::read('webconfig.search_area_group');
    $element = Configure::read('webconfig.employment_data');
    $hotkey = Configure::read('webconfig.hotkey_data');

    // Group
    foreach ($data_group as $value) {
      $results[] = array('loc' => FULL_BASE_URL.'/search/'.trim($value['area']['name']), 'priority' => '0.6');
    }

    // Group em
    foreach ($data_group as $value) 
      foreach ($element as $val)
        $results[] = array('loc' => FULL_BASE_URL.'/search/'.trim($value['area']['name']).'/'.trim($val), 'priority' => '0.6');

    // Group hotkey
    foreach ($data_group as $value) 
      foreach ($hotkey as $val)
        $results[] = array('loc' => FULL_BASE_URL.'/search/'.trim($value['area']['name']).'/'.trim($val), 'priority' => '0.6');

    // Group em hotkey
    foreach ($data_group as $value) 
      foreach ($element as $val)
        foreach ($hotkey as $v)
          $results[] = array('loc' => FULL_BASE_URL.'/search/'.trim($value['area']['name']).'/'.trim($val).'/'.trim($v), 'priority' => '0.6');

    return $results;
  }

  public function get_province(){
    $results = array();

    $options['fields'] = array('name' );
    $options['conditions'] = array( 'level' => 0 );
    $results = $this->Area->find( 'list', $options );

    return $results;
  }
  public function get_city(){
    $results = $city = array();

    $options['fields'] = array('id', 'name' );
    $options['conditions'] = array( 'name' => array('北海道','宮城県','大阪府','東京都','神奈川県','埼玉県','徳島県','福岡県','沖縄県'), 'level' => 0 );
    $province = $this->Area->find( 'list', $options );

    $options_city['conditions'] = array( 'parent_id' => array_keys($province), 'level' => 1 );
    $city = $this->Area->find( 'all', $options_city );

    foreach ($city as $key => $value) {

      $results[ $key ]['province'] = $province[ $value['Area']['parent_id'] ];
      $results[ $key ]['city'] = $value['Area']['name'];
    } 

    return $results;
  }
  public function get_data_type_2_province(){
    $results = array();    
    $province = $this->get_province();

    $element = Configure::read('webconfig.employment_data');
    $hotkey = Configure::read('webconfig.hotkey_data');

    if(!empty($province)){
      // Province
      foreach ($province as $value) 
        $results[] = array('loc' => FULL_BASE_URL.'/search/'.trim($value), 'priority' => '0.6');
      
      // Province em
      foreach ($province as $value) 
        foreach ($element as $val)
          $results[] = array('loc' => FULL_BASE_URL.'/search/'.trim($value).'/'.trim($val), 'priority' => '0.6');

      // Province hotkey
      foreach ($province as $value) 
        foreach ($hotkey as $val)
          $results[] = array('loc' => FULL_BASE_URL.'/search/'.trim($value).'/'.trim($val), 'priority' => '0.6');

      // Province em hotkey
      foreach ($province as $value) 
        foreach ($element as $val)
          foreach ($hotkey as $v)
            $results[] = array('loc' => FULL_BASE_URL.'/search/'.trim($value).'/'.trim($val).'/'.trim($v), 'priority' => '0.6');
    } 

    return $results;
  }
  public function get_data_type_2_city(){
    $results = array();    
    $city = $this->get_city();

    $element = Configure::read('webconfig.employment_data');
    $hotkey = Configure::read('webconfig.hotkey_data');

    if(!empty($city)){
      // City
      foreach ($city as $value) 
        $results[] = array('loc' => FULL_BASE_URL.'/search/'.trim($value['province']).'/'.trim($value['city']), 'priority' => '0.6');
      
      // City em
      foreach ($city as $value) 
        foreach ($element as $val)
          $results[] = array('loc' => FULL_BASE_URL.'/search/'.trim($value['province']).'/'.trim($value['city']).'/'.trim($val), 'priority' => '0.6');

      // City hotkey
      foreach ($city as $value) 
        foreach ($hotkey as $val)
          $results[] = array('loc' => FULL_BASE_URL.'/search/'.trim($value['province']).'/'.trim($value['city']).'/'.trim($val), 'priority' => '0.6');

      // City em hotkey
      foreach ($city as $value) 
        foreach ($element as $val)
          foreach ($hotkey as $v)
            $results[] = array('loc' => FULL_BASE_URL.'/search/'.trim($value['province']).'/'.trim($value['city']).'/'.trim($val).'/'.trim($v), 'priority' => '0.6');
    } 

    return $results;
  }

}
