<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
App::uses('CakeText', 'Utility', 'Area');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class SecretShareController extends AppController
{

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $components = array('Session', 'Util');
    public $uses = array('CareerOpportunity', 'CorporatePrs', 'Keywords');


    public function index($id = null)
    {
        /*Invalid*/
        if ($id == null) {
            return $this->redirect('/SecretShare/timeout');
        }
		
		$expire = $this->request->query['e'];
		$hash = $this->request->query['h'];
		
		if(!$this->Util->checkShareLink($id, $expire, $hash))
		{
			return $this->redirect('/SecretShare/timeout');
		}
		
		$secret = $this->CareerOpportunity->getSecretShare($id);
        /*Got data*/
        if ($secret) {
            $this->set('CareerOpportunity', $secret['CareerOpportunity']);
            $this->set('CorporatePrs', $secret['CorporatePrs']);

            if ($this->is_mobile) {
                $this->layout = 'single_column_sp';
                $this->render('../Secret/detail-sp');
            } else {
                $this->layout = 'single_column';
				$this->render('../Secret/detail');
            }
		}
		else
		{
			return $this->redirect('/SecretShare/timeout');
		}
    }
	
	public function timeout()
	{
		$this->render('../Errors/errorTimeOut');
	}
   
}
