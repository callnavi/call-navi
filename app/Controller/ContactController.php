<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
App::uses('CakeText', 'Utility');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ContactController extends AppController {

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array('');
    public $helpers = array('Paginator','Html','Form');
    public $components = array('Session','Sendmail');
    public $paginate = array();
    /**
     * Displays a view
     *
     * @return void
     * @throws NotFoundException When the view file could not be found
     *	or MissingViewException in debug mode.
     */
    public function index() {
        $this->layout ="single_column";
        if($this->is_mobile)
        {

            $this->layout = 'single_column_sp';
            $this->render('ContactSp');
        }

    }

    public function SendContactMail(){
        $data = array(
            'arr' => array(
                'name' => $this->request->data['name'],
                'mail' => $this->request->data['mail'],
                'content' => $this->request->data['content'],
                'companyName' => $this->request->data['companyName'],
                'departmentNamePosition' => $this->request->data['departmentNamePosition'],
                'tel' => $this->request->data['tel'],
                'answer' => $this->request->data['answer'],
            )
        );
        if(empty($data['arr']['companyName']))
        {
            $temp_admin = 'text/contact';
            $temp_user = 'text/contact_thanks_mail';
        }
        else
        {
            $temp_admin = 'text/contact_company';
            $temp_user = 'text/contact_company_thanks_mail';
        }

        //Default find the ctp file in View/Emails
        $this->Sendmail->render($temp_user,$data);

        //Send from Address and from Name
        $this->Sendmail->fromlName = Configure::read('webconfig.mail_from_name');
        $this->Sendmail->fromlAdress = Configure::read('webconfig.mail_from');

        //Send: to Email, to Name, subject
        $result = $this->Sendmail->send($data['arr']["mail"],$data['arr']['name'], 'Callnavi:お問い合わせ');

        //send mail to admin
        //Default find the ctp file in View/Emails
        $this->Sendmail->render($temp_admin,$data);

        //Send: to Email, to Name, subject
        $resultAdmin = $this->Sendmail->send('info@callnavi.jp','管理者', 'Callnavi:お問い合わせ');


        //If error will be appear in this function
        pr($this->Sendmail->error());
        die;
    }

}
