<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
App::uses('CakeText', 'Utility');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class DownloadcvController extends AppController {

    /**
     * This controller does not use a model
     *
     * @var array
     */
	public $uses = array('CareerOpportunity', 'Entry');
    public $helpers = array('Paginator','Html');
    public $components = array('Session','CloudSearch');
    public $paginate = array();
    /**
     * Displays a view
     *
     * @return void
     * @throws NotFoundException When the view file could not be found
     *	or MissingViewException in debug mode.
     */
    public function index() {
		$id = $this->request->query['id'];
		
		$rs = $this->Entry->check_only($id);
		
		if(!$rs)
		{
			$this->redirect('/downloadcv/error?m=outofdate');
		}
		$this->set('id', $id);
        /*Must in bottom*/
        if($this->is_mobile)
        {
            $this->layout = 'downloadcv';
            $this->render('index_sp');
        }
		else
		{
			$this->layout ="downloadcv";
			$this->render('index');
		}
		
		
    }
	
	public function get(){
		
		$id = $this->request->query['id'];
		$password = $this->request->data['password'];
		
		if(empty($password))
		{
			$this->set('errorMess', 'Please input password');
		}
		else
		{
			$rs = $this->Entry->check_and_get($id, $password);

			$cv = array();
			if($rs)
			{
				if(!empty($rs['cv1_path']))
				{
					$cv[] = array(
						'name' => '履歴書',
						'link' => $this->getLink($id, $password, 'cv1')
					);
				}

				if(!empty($rs['cv2_path']))
				{
					$cv[] = array(
						'name' => '職務経歴書',
						'link' => $this->getLink($id, $password, 'cv2')
					);
				}
			}
			else
			{
				$this->set('errorMess', 'Password is incorrect');
			}


			$this->set('id', $id);
			$this->set('cv', $cv);
		}
		//2files		
		if($this->is_mobile)
        {
            $this->layout = 'downloadcv';
            $this->render('index_sp');
        }
		else
		{
			$this->layout ="downloadcv";
			$this->render('index');
		}
		
	}
	
	public function cv()
	{
		$id = $this->request->query['id'];
		$password = $this->request->query['password'];
		$file = $this->request->query['file'];
		$filePath = $this->Entry->check_and_get_file($id, $password, $file);
		$this->autoRender = false;
		
		if($filePath)
		{
			$this->response->file($filePath);
		}
		else
		{
			$this->redirect('/downloadcv/error?m=nofile');
		}
	}
	
	public function error()
	{
		$this->layout =null;
		$this->render('error');
	}
	
	private function getLink($id, $password, $fileName)
	{
		$link = "/downloadcv/cv?id=$id&password=$password&file=$fileName";
		return $link;
	}
}
