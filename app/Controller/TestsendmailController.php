<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
App::uses('CakeText', 'Utility');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class TestsendmailController extends AppController {
	
    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array('');
    public $helpers = array('Paginator','Html','Form');
    public $components = array('Session','Sendmail');
    public $paginate = array();
    /**
     * Displays a view
     *
     * @return void
     * @throws NotFoundException When the view file could not be found
     *	or MissingViewException in debug mode.
     */
    public function index() {
        $this->layout ="single_column";
        if($this->is_mobile)
        {

            $this->layout = 'single_column_sp';
            $this->render('ContactSp');
        }

    }
	
	public function SendMail(){
		echo 111;
        $data = array(
            'arr' => array(
                'name' => 'xuong',
                'mail' => 'xuong.banh@jellyfishhr.com',
                'content' => 'aaa',
                'companyName' => 'bbb',
                'departmentNamePosition' => 'cccc',
                'tel' => '09012344578',
                'answer' => 'dasdsadsad',
                'title' => 'dasdsadsad',
            )
        );
        if(empty($data['arr']['companyName']))
        {
            $temp_admin = 'text/contact';
            $temp_user = 'text/contact_thanks_mail';
        }
        else
        {
            $temp_admin = 'text/contact_company';
            $temp_user = 'text/contact_company_thanks_mail';
        }
//		var_dump($temp_user);
//		var_dump($data);
//		$this->Sendmail->render($temp_user,$data);
//		var_dump($this->Sendmail->getRender());
//		exit;
//		$result = $this->Sendmail->send('xuong.banh@jellyfishhr.com', 'xuong', $subject='xuong test', $content='hihihi', $isText=true);
//		var_dump($result);
//		exit;
        //Default find the ctp file in View/Emails
        $this->Sendmail->render($temp_user,$data);
		
        //Send from Address and from Name
        $this->Sendmail->fromlName = 'Callnavi窓口';
        $this->Sendmail->fromlAddress = 'no-reply@callnavi.jp';

        //Send: to Email, to Name, subject
        $result = $this->Sendmail->send($data['arr']["mail"],$data['arr']['name'], 'Callnavi:お問い合わせ');

        //send mail to admin
        //Default find the ctp file in View/Emails
        //$this->Sendmail->render($temp_admin,$data);

        //Send: to Email, to Name, subject
        //$resultAdmin = $this->Sendmail->send('info@callnavi.jp','管理者', 'Callnavi:お問い合わせ');


        //If error will be appear in this function
        pr($this->Sendmail->error());
        exit;
    }
	
    public function SendContactMail(){
		echo 'ready';
		
        $AppController = new AppController;

        $arr = $this->request->data;
        if(count($arr) == 3)
        {
            $temp_admin = 'contact';
            $temp_user = 'contact_thanks_mail';
        }
        else
        {
            $temp_admin = 'contact_company';
            $temp_user = 'contact_company_thanks_mail';
        }

       /* $success = $AppController->send_email(
            'default',
            'xuong.banh@jellyfishhr.com',
            'xuong.banh@jellyfishhr.com',
            'Callnavi:お問い合わせ (from test Callnavi 1)',
            $message = null,
            $format = 'html',
            $template = $temp_admin,
            array('arr' => $arr),
            Configure::read('EMAIL')
        );*/
		
		
		
        $success_thank_you = $AppController->send_email(
            'default',
            'xuong.banh@jellyfishhr.com',
            'xuong.banh@jellyfishhr.com',
            'Callnavi:お問い合わせ (from test Callnavi 2)',
            $message = null,
            $format = 'text',
            $template = $temp_user,
            array('arr' => $arr),
            Configure::read('EMAIL')
        );
		echo '<br>';
		echo 'send mail';
        $result =  array("Send Admin" => $success,"Send User" => $success_thank_you);
        echo '<br>';
		echo 'Result';
		echo '<br>';
		pr($result);
		die;
    }
	
	public function xxx()
	{
		$AppController = new AppController;
		
		$arr = array();
		$mail_from = ['no-reply@callnavi.jp' => 'Callnavi窓口'];
		$mail_to = 'khanh.pham@jellyfishhr.com';
		$arrBCC = ['tan.doan@jellyfishhr.com'];
		$success = $AppController->send_email(
			'default',
			$mail_from,
			$mail_to,
			'【コールナビ】TEST BCC',
			$message = null,
			$format = 'text',
			$template = 'test_mail',
			array('arr' => $arr),
			$arrBCC
		);
		
		echo 222;
		die;
	}
	
	public function uuu()
	{
		echo 111;
		die;
	}

}
