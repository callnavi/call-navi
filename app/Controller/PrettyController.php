<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
App::uses('CakeText', 'Utility');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class PrettyController extends AppController {

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array('Pretty','PrettyPV');
    public $helpers = array('Paginator','Html');
    public $components = array('Session','Cookie','RequestHandler');
    public $paginate = array();
	
    /**
     * Displays a view
     *
     * @return void
     * @throws NotFoundException When the view file could not be found
     *	or MissingViewException in debug mode.
     */
    public function index($gender=null) {

        $this->layout = 'single_column';


                //set pv
        if ($this->RequestHandler->isAjax()) {
            $id = $this->request->data['id'];

            $check = $this->PrettyPV->find('first', array(
              'fields' => array(),
              'conditions' => array(
                'client' => $_SERVER['REMOTE_ADDR'],
                'pretty_id' => $id
              )
            ));
            if(empty($check)){
                $param['PrettyPV']['client'] = $_SERVER['REMOTE_ADDR'];
                $param['PrettyPV']['pretty_id'] = $id;
                $this->PrettyPV->create();
                $this->PrettyPV->save($param);
            }
            $result = $this->set_view($id,$check);
            return $result;
        }

        if(!empty($gender)){

            $cond = array();
            if($gender=="美男"){
                $cond[] = array(
                  'AND'=>array(
                    'gender' => '男',
                  ));
            }else{
                $cond[] = array(
                  'AND'=>array(
                    'gender' => '女',
                  ));
            }


            $data = $this->Pretty->find('all', array(
              'fields' => array(),
              'conditions' => array(
                'Pretty.status' => 1,
                $cond
              ),
              'order' => 'Pretty.point_pv DESC',
            ));
            $this->set('show_num',1);


        }else{
            $data = $this->Pretty->find('all', array(
              'fields' => array(),
              'conditions' => array(
                'Pretty.status' => 1,
              ),
              'order' => 'Pretty.created DESC',
            ));
            $this->set('show_num',0);
            
        }
        
        $this->set('total',count($data));
        $this->set('data',$data);
//        pr($data);die;

        if($this->is_mobile)
        {
            $this->layout = 'default_sp';
            $this->render('index_sp');
        }

    }

    public function set_view($id=null,$check) {
        $this->autoRender = false;
        
        if(empty($check)){
            $this->Pretty->updateAll(
              array('Pretty.point_pv' => 'Pretty.point_pv + 1'),
              array('Pretty.id' => $id)
            );
        }

        $data = $this->Pretty->find('first', array(
          'fields' => array('point_pv'),
          'conditions' => array(
            'Pretty.id' => $id,
          )
        ));
        
        echo json_encode($data['Pretty']);

    }



}
