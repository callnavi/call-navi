<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
App::uses('CakeText', 'Utility');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class NewsController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array('ContentsEmail','Categories');
	public $helpers = array('Paginator','Html');
    public $components = array('Session');
    public $paginate = array();
/**
 * Displays a view
 *
 * @return void
 * @throws NotFoundException When the view file could not be found
 *	or MissingViewException in debug mode.
 */
	public function index($category_alias= null) {

		$page = isset($_GET["page"])?intval($_GET["page"]):1;
        $size = 20;
		if(isset($category_alias) && !empty($category_alias)){
			$data = $this->ContentsEmail->getList($page,$size,$category_alias);
            $total = $this->ContentsEmail->countListByAlias($category_alias);
		}else{
            $data = $this->ContentsEmail->getList($page,$size);
            $total = $this->ContentsEmail->countListByAlias();
        }

        $catList= $this->Categories->find('all', array(
            //'conditions' => array('category_type' => 1),
            'conditions' => array('category_type' => 1, 
								  'id != 3', 
								  'id != 6'),
        ));
       // pr($data);die;

        $this->set("catList",$catList);
		$pageCount = ceil($total/$size);
        $this->set("catCurr", $category_alias);
        $this->set("total", $total);
		$this->set("list",$data);
		$this->set("currentPage",$page);
		$this->set("pageCount",$pageCount);

		$this->getNumberStartEndinPage($total, $page, $pageCount);
        /*Must in bottom*/
        if($this->is_mobile)
        {
            $this->layout = 'default_sp';
            $this->render('indexsp');
        }


	}

	public function detail($category_alias= null,$slug= null) {

		if(!empty($slug)){

			$item = $this->getContentNews($category_alias,$slug);

            if(empty($item)){
                $this->redirect('/errors/error404');
            }
            //pr($item);die;
			$this->ContentsEmail->id = $slug;
			$this->ContentsEmail->query("UPDATE `contents_email` SET `view` = `view` + 1 WHERE id = $slug");

            $catList= $this->Categories->find('all', array(
                'conditions' => array('category_type' => 1 ),
            ));
            //pr($item);die;
//            create breadrum link
            $page= array('name'=>'ニュース', 'link'=>Router::url(array('controller' => 'news', 'action' => 'index')));
            $category= array('name'=>$item['category']['category'], 'link'=>Router::url('/news/'. $item['category']['category_alias']));
            $detail= array('name'=> strlen( $item['ContentsEmail']['Title'])> 30 ? mb_substr( $item['ContentsEmail']['Title'],0,30,'UTF-8').'...' : $item['ContentsEmail']['Title'], 'link'=>'');
            $breadrumLink = $this->breadrumLinkDetail($page,$category,$detail);

            //debug($this->ContentsEmail->getDataSource()->getLog(false, false) );die;
            $this->set("catList",$catList);
			$this->set("list",$item);
			$this->set("breadrumLink",$breadrumLink);

		}

        /*Must in bottom*/
        if($this->is_mobile)
        {

            $this->layout = 'default_sp';
            $this->render('detailsp');
        }

	}

    public function preview($id= null) {
        /*Invalid*/
        if(!$this->Session->check('User')){
            $this->redirect('/login');
        }
        if(!empty($id)){

            $item = $this->getContentNews(null,$id);
            if(empty($item)){
                $this->redirect('/errors/error404');
            }

            $catList= $this->Categories->find('all', array(
                'conditions' => array('category_type' => 1 ),
            ));
//            create breadrum link
            $page= array('name'=>'ニュース', 'link'=>Router::url(array('controller' => 'news', 'action' => 'index')));
            $category= array('name'=>$item['category']['category'], 'link'=>Router::url('/news/'. $item['category']['category_alias']));
            $detail= array('name'=> strlen( $item['ContentsEmail']['Title'])> 30 ? mb_substr( $item['ContentsEmail']['Title'],0,30,'UTF-8').'...' : $item['ContentsEmail']['Title'], 'link'=>'');
            $breadrumLink = $this->breadrumLinkDetail($page,$category,$detail);

            //debug($this->ContentsEmail->getDataSource()->getLog(false, false) );die;
            $this->set("catList",$catList);
            $this->set("list",$item);
            $this->set("breadrumLink",$breadrumLink);
            $this->set("isPreview",true);

        }

        if($this->request->is('post')){

            $param['ContentsEmail'] = $this->request->data['PublishJob'];
            $id= !empty($id) ? $id :$this->ContentsEmail->getLastInsertId();
            $this->ContentsEmail->id = $id;
            $this->ContentsEmail->set($param);
            $redirectUrl = $param['ContentsEmail']['status'] == 1 ?
                '/news/detail/'.$item['category']['category_alias'].'/'.$id : '/news/preview/'.$id;
            if ($this->ContentsEmail->save()) {
                //pr($item);die;
                $this->Session->setFlash(__('The article has been saved.'));
                return $this->redirect($redirectUrl);
            } else {
                $this->Session->setFlash(__('The article could not be saved. Please, try again.'));
            }
        }

        /*Must in bottom*/
        $this->render('detail');

    }


    private function getNumberStartEndinPage($count, $page, $pageCount){

		if($count != 0) {
			$start = (($page - 1) * 20) + 1;
			$end = $page == $pageCount ? $count : $page * 20;
		}else{
			$start = 0;
			$end = 0;
		}

		$this->set('numberStartEnd', array('start' => $start, 'end' => $end ));

    }

    private  function  breadrumLinkDetail($page = null,$category = null, $detail= null){
		
		return array(
			'page' 	 => $detail['name'],
			'parent' => array(
								array('link' => $page['link'], 'title' => $page['name']),
								array('link' => $category['link'], 'title' => $category['name']),
							 )
		);
    }

    private function getContentNews($category_alias= null,$slug= null){
        $conditions = array('ContentsEmail.id' =>$slug  );
        if(!empty($category_alias)){
            $conditions = array('ContentsEmail.id' =>$slug,'category.category_alias' =>$category_alias,'ContentsEmail.status' =>1  );
        }
        $item = $this->ContentsEmail->find('first',array(
            'fields' => array('created','source','featured_picture','send_date','id','status','header','Subtitle','Excerpt','content','relation_link','subject','view','Title', 'category.category','category.category_alias','category.id', 'TIMESTAMPDIFF(hour, ContentsEmail.send_date, CURDATE() ) AS totalHour'),
            'conditions' => $conditions ,
            'joins' => array(
                array(
                    'table' => 'contents_email_category',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'contents_email_category.content_id = ContentsEmail.id',
                    ),
                ),
                array(
                    'table' => 'category',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'category.email_to = contents_email_category.email_to',
                    ),
                ),
            ),
        ));

        return $item;
    }

}
