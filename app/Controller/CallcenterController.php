<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
App::uses('CakeText', 'Utility');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class CallcenterController extends AppController {

/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array('Area', 'Callcenter', 'CareerOpportunity');
	public $helpers = array('Paginator','Html');
    public $components = array('Session','CloudSearch', 'Util','Paginator');
    public $paginate = array();
/**
 * Displays a view
 *
 * @return void
 * @throws NotFoundException When the view file could not be found
 *	or MissingViewException in debug mode.
 */
	public function index() {
	    
		if($this->request->is('get')){
	        $keySearch = $this->request->query;
            if(!empty($keySearch['keySearch'])){
				$clearlykeySearch = $this->Util->replaceSpecialKey($keySearch['keySearch']);
                $data = $this->Callcenter->getListCallcenterV2($clearlykeySearch);
                
				//go to Secret job to get Company info
				$moreData = $this->CareerOpportunity->getAndConvertInfoByBranchName($keySearch['keySearch']);

				//merge $data and $moreData
				$data = array_merge($data, $moreData);
								
				$this->set('listCallcenter', $data);
                $this->set('keySearch', $keySearch['keySearch']);
            }
        }
		
		$pickup = $this->CareerOpportunity->pickup();
		//$groupArea = $this->Area->getGroup();
		//$this->set('groupArea', $groupArea);
		$this->set('pickup', $pickup);

		if($this->is_mobile)
		{
           // pr($this->is_mobile);die;
			$this->layout = 'single_column_v2_callcenter_sp';
			$this->render('index-google-map-sp');
		}else
        {
            $this->layout ="single_column";
            $this->render('index-google-map');
        }
	}

	public function getListCallCenter()
	{
		if($this->request->is('get')){
	        $keySearch = $this->request->query;
            if(!empty($keySearch['keySearch'])){
				$clearlykeySearch = $this->Util->replaceSpecialKey($keySearch['keySearch']);
                $data = $this->Callcenter->getListCallcenterV2($clearlykeySearch);
                $this->set('listCallcenter', $data);
                $this->set('keySearch', $keySearch['keySearch']);
            }
        }
		
		$pickup = $this->CareerOpportunity->pickup();
		//$groupArea = $this->Area->getGroup();
		//$this->set('groupArea', $groupArea);
		$this->set('pickup', $pickup);
	}

	public function preview($id = null, $kindPreview = 'PC')
	{
		if($id)
		{
			$data = $this->Callcenter->getCallCenterById($id);
			if($data)
			{
				$companyName = !empty($data[0]['Callcenter']['company_name'])?$data[0]['Callcenter']['company_name']:"";
			}

            $this->set('listCallcenter', $data);
            $this->set('keySearch', $companyName);
		}
		
		$pickup = $this->CareerOpportunity->pickup();
		//$groupArea = $this->Area->getGroup();
		//$this->set('groupArea', $groupArea);
		$this->set('pickup', $pickup);

		if($kindPreview == 'SP')
		{
           	$this->set('zoom', true);
			$this->layout = 'single_column_v2_sp';
			$this->render('index-google-map-sp');
		}else
        {
            $this->layout ="single_column";
            $this->render('index-google-map');
        }
	}
	
	public function area($area_alias= null,$city_id= null) {
		
		$size = $this->is_mobile ? 30 : 0 ;
		$page = isset($this->request->query['page'])?intval($this->request->query['page']):1; 
		$areaList = $this->Callcenter->getListByArea($page, $size, $area_alias, $city_id);
		$areaListData = $areaList['data'];
		$areaListTotal = $areaList['total'];
		$currentArea = $this->Area->getAreaByAliasName($area_alias);
		$groupArea = $this->Area->getGroup();
		$groupAreaInMap = $this->Area->getGroup($currentArea[0]['area']['group']);
		$currentCity = $this->Area->getAreaById($city_id);

		$this->set('currentCity', $currentCity);
		$this->set('currentArea', $currentArea);
		$this->set('groupArea', $groupArea);
		$this->set('groupAreaInMap', $groupAreaInMap);
		$this->set('areaList', $areaListData);
		$this->set('areaTotal', $areaListTotal);
		$this->set('area_alias', $area_alias);
		$this->set('city_id', $city_id);
		$this->set('page', $page);
		$this->set('size', $size);

        if($this->is_mobile)
		{
			$this->layout = 'single_column_sp';
			$this->render('area-sp');
		}else{
			$this->layout = 'single_column';
		}
	}
	
	public function detail($area_alias= null,$city_id= null, $callcenter_id=null) {
		
		
		$detail = $this->Callcenter->getDetail($area_alias, $city_id, $callcenter_id);
		
		if(empty($detail))
		{
			return $this->redirect('/callcenter_matome/area/'.$area_alias);
		}
		
		
		//$detailTitle = $detail['c']['company_name'];
        $keywords = $detail['c']['keywords'];
		$params = array('keyword' => $keywords);
        //pr($keywords);
        //print($keywords);
		$relatedJob = $this->CloudSearch->search_job($params, 1, 3);

		$relatedJob = $relatedJob[0];

		$currentArea = $this->Area->getAreaByAliasName($area_alias);
		$groupArea = $this->Area->getGroup();
		$groupAreaInMap = $this->Area->getGroup($currentArea[0]['area']['group']);

		$this->set('groupAreaInMap', $groupAreaInMap);
		$this->set('currentArea', $currentArea);
		$this->set('groupArea', $groupArea);

		$this->set('detail', $detail);
		$this->set('relatedJob', $relatedJob);
		$this->set('area_alias', $area_alias);
		$this->set('city_id', $city_id);
        
        if($this->is_mobile)
		{
			$this->layout = 'single_column_sp';
			$this->render('detail-sp');
		}else{
			$this->layout = 'single_column';
		}
	}
	
	public function around_search($area_alias= null,$city_id= null, $callcenter_id=null) {
		
		$detail = $this->Callcenter->getDetail($area_alias, $city_id, $callcenter_id);
		$ccId = $detail['c']['id'];
		$relatedCC = $this->Callcenter->getRelatedCallCenter($ccId, $city_id);

		$currentArea = $this->Area->getAreaByAliasName($area_alias);
		$groupArea = $this->Area->getGroup();
		$groupAreaInMap = $this->Area->getGroup($currentArea[0]['area']['group']);
		
		$this->set('detail', $detail);
		$this->set('relatedCC', $relatedCC);
		$this->set('city_id', $city_id);
		
		$this->set('groupAreaInMap', $groupAreaInMap);
		$this->set('currentArea', $currentArea);
		$this->set('groupArea', $groupArea);
		
        if($this->is_mobile)
		{
			$this->layout = 'single_column_sp';
			$this->render('around_search-sp');
		}
	}
	
	public function getcitybyalias($area_alias= null)
	{
		$city = $this->Area->getCityByAliasName($area_alias);
		echo json_encode($city);
		die;
	}
	
	public function testmap()
	{
		$this->Paginator->settings = array(
			'fields' => array('id', 'company_name','address'),
        	'limit' => 20
		);
		$list = $this->Paginator->paginate('Callcenter');
//		$list = $this->Callcenter->find('all', array(
//			'fields' => array('id', 'company_name','address'),
//		));
		$this->layout = null;
		$this->set('list', $list);
	}
	
    private function getNumberStartEndinPage($count, $page, $pageCount){

		if($count != 0) {
			$start = (($page - 1) * 20) + 1;
			$end = $page == $pageCount ? $count : $page * 20;
		}else{
			$start = 0;
			$end = 0;
		}

		$this->set('numberStartEnd', array('start' => $start, 'end' => $end ));

    }
}
