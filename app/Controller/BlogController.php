<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');
App::uses('CakeText', 'Utility');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class BlogController extends AppController
{

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array('BlogsCategory', 'Blogs');
    public $helpers = array('Paginator', 'Html');
    public $components = array('Session');
    public $paginate = array();

    /**
     * Displays a view
     *
     * @return void
     * @throws NotFoundException When the view file could not be found
     *    or MissingViewException in debug mode.
     */
    public function index($title_alias = null)
    {

        $page = isset($_GET["page"]) ? intval($_GET["page"]) : 1;
        if (empty($title_alias)) {
            $size = 10;

            $total = $this->BlogsCategory->find('count');
            //$val= $this->paginate('BlogsCategory');

            $data = $this->BlogsCategory->getBlogCat($page, $size);
            // pr($data);
            //debug($this->BlogsCategory->getDataSource()->getLog(false, false) );die;
            $this->layout = 'single_column';
            $this->set("total", $total);
            $this->set("list", $data);


            $pageCount = ceil($total / $size);
            $this->set("currentPage", $page);
            $this->set("pageCount", $pageCount);

            if ($this->is_mobile) {
                $heightFooter = 'height-730';
                $this->set('heightFooter', $heightFooter);
                $this->layout = 'default_sp';
                $this->render('indexsp');
            }

        } else {

            $size = 10;
            $item = $this->BlogsCategory->find('first', array(
                'fields' => array('cat_id', 'title', 'pic', 'short', 'category_alias', 'friendly_title', 'metadesc', 'metakey'),
                'conditions' => array('category_alias' => $title_alias),
            ));


            //other blog category
            $other_blog_category = $this->BlogsCategory->getBlogCat(1, 6);


            $total = $this->Blogs->find('count', array('conditions' => array('cat_id' => $item['BlogsCategory']['cat_id'])));

            //show list blog
            $start = ($page - 1) * $size;
            $cat_id = $item['BlogsCategory']['cat_id'];
            $val = $this->Blogs->getBlogs(" AND Blogs.cat_id=" . $cat_id, $start, $size);
            //debug($this->BlogsCategory->getDataSource()->getLog(false, false) );die;
            $this->layout = 'single_column';

            $this->set("other_category", $other_blog_category);
            $this->set("total", $total);
            $this->set("list", $val);
            $this->set("info_cat", $item);
            $this->set("title_cat", $item['BlogsCategory']['title']);
            $pageCount = ceil($total / $size);
            $this->set("currentPage", $page);
            $this->set("pageCount", $pageCount);
            $this->getNumberStartEndinPage($total, $size, $page, $pageCount);
            $this->render('category');


            if ($this->is_mobile) {
                $this->layout = 'default_sp';
                $this->render('categorysp');
            }
        }


    }


    private function getNumberStartEndinPage($count, $size, $page, $pageCount)
    {

        if ($count != 0) {
            $start = (($page - 1) * $size) + 1;
            $end = $page == $pageCount ? $count : $page * $size;
        } else {
            $start = 0;
            $end = 0;
        }

        $this->set('numberStartEnd', array('start' => $start, 'end' => $end));

    }


    public function detail($category_alias = null, $id = null)
    {


        if (!empty($id)) {
            $this->Blogs->id = $id;
            $this->Blogs->query("UPDATE `blogs` SET `view` = `view` + 1 WHERE id = $id");

            $info = $this->BlogsCategory->find('first', array(
                'fields' => array('cat_id', 'title', 'category_alias'),
                'conditions' => array('category_alias' => $category_alias),
            ));


            //tags category footer detail page
            $category_all = $this->BlogsCategory->find('all', array(
                'fields' => array('cat_id', 'title', 'category_alias'),
                'limit' => 6
            ));
            $this->set("category_tags", $category_all);
            # end


            $item = $this->Blogs->find('first', array(
                'fields' => array('*'),
                'conditions' => array('id' => $id)
            ));


            /******other news******/
            $other_items_blog = $this->Blogs->find('all', array(
                'fields' => array('*'),
                'conditions' => array('cat_id' => $info['BlogsCategory']['cat_id'], 'id <>' . $id),
                'limit' => 2,
                'order' => array('date_post' => 'DESC')
            ));
            /***********/


//            create breadrum link
            $page = array('name' => 'CCブログ', 'link' => Router::url(array('controller' => 'blog', 'action' => 'index')));
            $category = array('name' => $info['BlogsCategory']['title'], 'link' => Router::url(array('controller' => 'blog', 'action' => 'index')) . "/" . $category_alias);
            $detail = array('name' => strlen($item['Blogs']['title']) > 30 ? mb_substr($item['Blogs']['title'], 0, 30, 'UTF-8') . '...' : $item['Blogs']['title'], 'link' => '');
            $breadrumLink = $this->breadrumLinkDetail($page, $category, $detail);

            //debug($this->Blogs->getDataSource()->getLog(false, false) );die;
            //pr($item);die;
            $this->layout = 'single_column';
            $this->set("other_list", $other_items_blog);
            $this->set("list", $item);
            $this->set("info", $info);
            $this->set("category_alias", $category_alias);
            $this->set("breadrumLink", $breadrumLink);

            if ($this->is_mobile) {
                $heightFooter = 'height-730';
                $this->set('heightFooter',$heightFooter);
                $this->layout = 'default_sp';
                $this->render('detailsp');
            }

        }


    }

    public function preview($id = null, $kindPreview = 'PC')
    {
        /*Invalid*/
        if (!$this->Session->check('User')) {
            $this->redirect('/login');
        }

        if (!empty($id)) {

            //tags category footer detail page
            $category_all = $this->BlogsCategory->find('all', array(
                'fields' => array('cat_id', 'title', 'category_alias'),
                'limit' => 6
            ));
            $this->set("category_tags", $category_all);
            # end

            $item = $this->Blogs->find('first', array(
                'fields' => array('Blogs.*', 'BlogsCategory.*'),
                'joins' => array(
                    array(
                        'table' => 'blogs_category',
                        'alias' => 'BlogsCategory',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'BlogsCategory.cat_id = Blogs.cat_id',
                        ),
                    ),
                ),
                'conditions' => array('id' => $id)
            ));

            /******other news******/
            $other_items_blog = $this->Blogs->find('all', array(
                'fields' => array('*'),
                'conditions' => array('cat_id' => $item['Blogs']['cat_id'], 'id <>' . $id),
                'limit' => 3,
                'order' => array('date_post' => 'DESC')
            ));
            /***********/

//            create breadrum link
            $page = array('name' => 'CCブログ', 'link' => Router::url(array('controller' => 'blog', 'action' => 'index')));
            $category = array(
                'name' => $item['BlogsCategory']['title'],
                'link' => Router::url(array('controller' => 'blog', 'action' => 'index')) . "/" . $item['BlogsCategory']['category_alias']);
            $detail = array(
                'name' => strlen($item['Blogs']['title']) > 30 ?
                    mb_substr($item['Blogs']['title'], 0, 30, 'UTF-8') . '...' :
                    $item['Blogs']['title'], 'link' => '');
            $breadrumLink = $this->breadrumLinkDetail($page, $category, $detail);

            
            $this->set("other_list", $other_items_blog);
            $this->set("list", $item);
            $this->set("info", $item);
            $this->set("category_alias", $item['BlogsCategory']['category_alias']);
            $this->set("breadrumLink", $breadrumLink);
            $this->set("isPreview", true);

            if ($this->request->is('post')) {

                $param['Blogs'] = $this->request->data['PublishJob'];
                $id = !empty($id) ? $id : $this->Blogs->getLastInsertId();
                $this->Blogs->id = $id;
                $this->Blogs->set($param);
                $redirectUrl = $param['Blogs']['status'] == 1 ?
                    '/blog/' . $item['BlogsCategory']['category_alias'] . '/' . $id : '/blog/preview/' . $id;
                if ($this->Blogs->save()) {
                    //pr($item);die;
                    $this->Session->setFlash(__('The article has been saved.'));
                    return $this->redirect($redirectUrl);
                } else {
                    $this->Session->setFlash(__('The article could not be saved. Please, try again.'));
                }
            }

            if ($kindPreview == 'SP') {
                $this->set('zoom', true);
                $heightFooter = 'height-730';
                $this->set('heightFooter',$heightFooter);
                $this->layout = 'default_sp';
                $this->render('detailsp');
            }
            else {
                $this->layout = 'single_column';
                $this->render('detail');
            }
        }

    }


    private function breadrumLinkDetail($page = null, $category = null, $detail = null)
    {

        switch ($category['name']) {
            case 'コールセンター':
                $category['link'] = $page['link'] . '?cat=1';
                break;
            case 'IT通信':
                $category['link'] = $page['link'] . '?cat=2';
                break;
            // case 'スマホ':
            //   $category['link']= $page['link'].'?cat=3';
            //   break;
            case '保険':
                $category['link'] = $page['link'] . '?cat=4';
                break;
            case '電力':
                $category['link'] = $page['link'] . '?cat=5';
                break;
            // case '不動産':
            //   $category['link']= $page['link'].'?cat=6';
            //   break;
        }
        $breadrumLink = "<a href='" . $page['link'] . "'>" . $page['name'] . "</a>　>　<a href='" . $category['link'] . "'>" . $category['name'] . "</a>　>　" . $detail['name'];
        return $breadrumLink;
    }


}
