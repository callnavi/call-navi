<?php
class CallcenterComponent extends Component {
	final PC_ZOOM = array(
		0 => 6,
		1 => 17,
		2 => 14,
		3 => 11,
		4 => 8,
	);
	
	final SP_ZOOM = array(
		0 => 6,
		1 => 17,
		2 => 14,
		3 => 11,
		4 => 8,
	);
	
	
	final $ZOOM_JAPAN = 0;
	final $ZOOM_A_ELEMENT = 1;
	final $ZOOM_A_TOWN = 2;
	final $ZOOM_A_CITY = 3;
	final $ZOOM_A_PROVINCE = 4;
	
	
	private $callcenter = null;
	private $keyword = '';
	
	public function setCallcenter($_callcenter)
	{
		$this->callcenter = $_callcenter;
	}
	
	public function setKeyword($_keyword)
	{
		$this->keyword = $_keyword;
	}
	
	public function getPcZoom($level)
	{
		return $this->PC_ZOOM[$level];
	}
	
	public function getSpZoom($level)
	{
		return $this->SP_ZOOM[$level];
	}
	
	public function isOnlyElement()
	{
		if($this->callcenter && $this->callcenter->count() == 1)
		{
			return true;
		}
		return false;
	}
	
	public function isArea()
	{
		if(!empty($this->keyword))
		{
			
		}
		
		return $this->ZOOM_JAPAN; 
	}
	
	public function getLevelZoom()
	{
		if($this->isOnlyElement())
		{
			return $this->ZOOM_A_ELEMENT;
		}
		return $this->isArea();
	}
}
