<?php
/*
	Urls		
		/正社員 
		=> {em}
		/服装髪型自由
		=> {hotkey}
		/東京都/世田谷区/南烏山
		=> /{pro}/{city}/{town}
		/東京都/正社員/髪型服装自由/時短勤務
		=> /{pro}/{em}/{hotkey}/{freekey}

	Full URL
		/東京都/世田谷区/南烏山/正社員/髪型服装自由/時短勤務
		=> /{pro}/{city}/{town}/{em}/{hotkey}/{freekey}

	Out
		pro  		=> 東京都,
		city 		=> 世田谷区,
		town  		=> 南烏山,
		em  		=> 正社員,
		hotkey  	=> 髪型服装自由,
		freekey  	=> 時短勤務,
*/

App::uses('Component', 'Controller');
class SearchRouteComponent extends Component  {
	
	/*
	Array
	(
		[area] => 関東,茨城県,牛久市,中央
		[data_em] => 1
		[data_step] => 0
		[data_group] => 0
		[data_province] => 0
		[data_city] => 0
		[data_town] => 0
		[em] => 
		[keyword] => 
		[list_key_area] => 2-8-479-11664
		[page] => 1
	)
	*/
	protected $pars = array('area' => array(), 
							'em' => '', 
							'data_group' => 0, 
							'data_province' => 0, 
							'data_city' => 0, 
							'data_town' => 0, 
							'data_em' => 0, 
							'list_key_area' => array());
	protected $countKeys = 0;
	protected $gotGroup = 0;
	
	protected function setPars($key, $value)
	{
		$this->pars[$key] = $value;
	}
	
	protected function setArea($value)
	{
		$this->pars['area'][] = $value; 
	}
	protected function setListKeyArea($value)
	{
		$this->pars['list_key_area'][] = $value; 
	}
	
	public function getPars()
	{
		$pars = $this->pars;
		$pars['area'] = implode(',',$pars['area']);
		$pars['list_key_area'] = implode('-',$pars['list_key_area']);
		return $pars;
	}
	
	public function getQuery()
	{
		$pars = $this->pars;
		$pars['area'] = implode('-',$pars['list_key_area']);
		$pars['em'] = $pars['data_em'];
		return $pars;
	}
	
	public function setGroupByProvinceId($id)
	{
		$data = Configure::read('webconfig.search_area_group');

		foreach( $data as $val )
		{
			$range = $val['area']['range'];
			if($id >= $range[0] && $id <= $range[1])
			{
				$this->setPars('group', $val['area']['name']);
				//$this->setArea($val['area']['name']);
				$this->setListKeyArea($val['area']['id']);
				return true;
			}
		}
		return false;
	}
	
	public function isGroup($key)
	{
		$data = Configure::read('webconfig.search_area_group');
		foreach( $data as $val)
		{
			$area = $val['area'];
			if($key == $area['name'])
			{
				$this->gotGroup = 1;
				$this->setPars('group', $key);
				//$this->setArea($key);
				$this->setListKeyArea($area['id']);
				return true;
			}
		}
		return false;
	}
	
	public function isProvince($key)
	{
		$data = Configure::read('webconfig.area_data');
		foreach( $data as $index=>$val)
		{
			if($key == $val)
			{
				if(!$this->goGroup)
				{
					$this->setGroupByProvinceId($index);
				}
				$this->setPars('province', $key);
				$this->setArea($key);
				$this->setListKeyArea($index);
				return true;
			}
		}
		return false;
		
	}
	public function isCity($key)
	{
		//市
		//区
		//町
		//村
		$tail = array('市', '区', '町', '村');
		$tailKey = mb_substr($key, -1);
		if(in_array($tailKey, $tail))
		{
//			$this->setPars('city', $key);
//			$this->setArea($key);
//			return true;
			$area = ClassRegistry::init('Area');
			$finding = $area->find('first', array('conditions' => array('name' => $key)));
			if($finding)
			{
				$this->setPars('city', $key);
				$this->setArea($key);
				$this->setListKeyArea($finding['Area']['id']);
				return true;
			}
			
		}
		return false;
		
	}
	
	public function isTown($key)
	{
		$area = ClassRegistry::init('AreaTown');
		$finding = $area->find('first', array('conditions' => array('name' => $key)));
		if($finding)
		{
			$this->setPars('town', $key);
			$this->setArea($key);
			$this->setListKeyArea($finding['AreaTown']['id']);
			return true;
		}
		return false;
	}
	
	public function isEm($key)
	{
		$data = Configure::read('webconfig.employment_data');
		foreach( $data as $index=>$val)
		{
			if($key == $val)
			{
				$this->setPars('data_em', $index);
				$this->setPars('em', $val);
				return true;
			}
		}
		return false;
	}
	
	public function isHotkey($key)
	{
		$data = Configure::read('webconfig.hotkey_data');
		foreach( $data as $index=>$val)
		{
			if($key == $val)
			{
				if(isset($this->pars['hotkey_string']))
				{
					array_push($this->pars['hotkey_string'], $val);
				}
				else
				{
					$this->setPars('hotkey_string', array($val));
				}

				if(isset($this->pars['hotkey']))
				{
					array_push($this->pars['hotkey'], (string)$index);
				}
				else
				{
					$this->setPars('hotkey', array((string)$index));
				}
				return true;
			}
		}
		return false;
	}
	
	
	public function start($keys, $index, $case)
	{
		if($this->countKeys <= $index)
		{
			return;
		}
		
		$currentKey = $keys[$index];
		switch($case)
		{
			case 'group':
				if($this->isGroup($currentKey))
				{
					$this->start($keys, $index+1, 'province');
				}
				else
				{
					$this->start($keys, $index, 'province');
				}
				break;
			//Checking Province
			case 'province':
				if($this->isProvince($currentKey))
				{
					$this->start($keys, $index+1, 'city');
				}
				else
				{
					$this->start($keys, $index, 'em');
				}
				break;
			
			//Checking Employment
			case 'city':
				if($this->isCity($currentKey))
				{
					$this->start($keys, $index+1, 'town');
				}
				else
				{
					$this->start($keys, $index, 'em');
				}
				break;
			
			//Checking Town		
			case 'town':
				if($this->isTown($currentKey))
				{
					$this->start($keys, $index+1, 'em');
				}
				else
				{
					$this->start($keys, $index, 'em');
				}
				break;
			
			case 'em':
				if($this->isEm($currentKey))
				{

					$this->start($keys, $index+1, 'hotkey');
				}
				else
				{
					$this->start($keys, $index, 'hotkey');
				}
				break;
				
			case 'hotkey':
				if($this->isHotkey($currentKey))
				{
					//$this->start($keys, $index+1, 'keyword');
					$this->start($keys, $index+1, 'hotkey');
				}
				else
				{
					$this->start($keys, $index, 'keyword');
				}
				break;
			
				
			case 'keyword':
				$this->setPars('keyword', $currentKey);
				break;
		}
	}
	
	
	/*
		TODO: call this function to get pars
	*/
	public function matchUrl($keys)
	{
		$this->countKeys = count($keys);
		$this->start($keys, 0, 'group');
	}


	public function customPassedArgs($params)
	{
		$result = [];
		if(!empty($params['area']))
		{
			$result = explode(",", $params['area']);
		}

		if(!empty($params['em']))
		{
			array_push($result, $params['em']);
		}

		if(!empty($params['keyword']))
		{
			array_push($result, $params['keyword']);
		}

		if(!empty($params['hotkey_string']))
		{
			foreach ($params['hotkey_string'] as $key => $value) {
				array_push($result, $value);
			}
		}

		return $result;
	}
}