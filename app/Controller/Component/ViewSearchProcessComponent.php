<?php
App::uses('Component', 'Controller');
class ViewSearchProcessComponent extends Component {
	
	public $components = array('Util');
	
	/*
		clip_title:"[派]コールセンタースタッフ【22-03242752】"
		company:"株式会社スタッフサービス"
		employment_type:"派遣社員"
		href:"/search/job/clc_1992538361-SL4SH-joid_D2989746"
		isNew:1
		labels:["駅チカ", "高収入", "未経験OK", "長期歓迎", "平日のみOK"]
		maxCatLeng:30
		pic_url:"/img/instead/img_no-image_17.jpg"
		salary:"時給1600～1650円"
		site_id:"10"
		site_name:"タウンワーク"
		title:"[派]コールセンタースタッフ【22-03242752】"
		unique_id:"clc_1992538361/joid_D2989746"
		update_before:"13時間前"
		updatetime:"1480369923"
		url:"http://townwork.net/detail/clc_1992538361/joid_D2989746/"
	*/
	
	public function search($item)
	{
		//TODO: youshouldwritesomethinghere
		//---------------------------------------------------
		if(empty($item['pic_url']))
		{
			$item['pic_url'] = '/img/instead/img_no-image_' . sprintf("%02d", rand(1, 21)) . '.jpg';
		}
		else
		{
			if($item['pic_url'][0] != "/")
			{
				$item['pic_url'] = $this->Util->proxy_image($item['pic_url']);
			}
		}
		
		$item['salary'] 	= strip_tags($item['salary']);
		if(mb_strlen($item['salary'])>50)
		{
			$item['salary'] = mb_substr($item['salary'],0,50,'UTF-8').'...';
		}

		$item['clip_title'] = mb_substr(strip_tags($item['title']),0,65,'utf-8');

		if(mb_strlen($item['company'])>25)
		{
			$item['company'] = mb_substr($item['company'],0,25,'UTF-8').'...';
		}


		$item['isNew'] = 0;
		//604800 = 7 days
		$timeInAWeek = time()-604800;
		if($item['updatetime'] > $timeInAWeek)
		{
			$item['isNew'] = 1;
		}

		//Sort category follows length
		usort($item['labels'], function($a, $b) {
			return strlen($a) - strlen($b);
		});

		$item['maxCatLeng'] = 30;
		
		$encodeId = $this->Util->jobUrlEncode($item['unique_id']);
		$item['href'] = '/search/job/'.$encodeId;
		$item['class'] = "";
		
		$item['secret'] = "0";
		unset($item['workplace']);
		unset($item['desc']);
		return $item;
	}
	
	public function secret($item)
	{
		$rsItem = array();
		
		$career = $item['CareerOpportunity'];
		$corporate = $item['corporate_prs'];
		
		//-picture--------------------------------------------------
		if(empty($career['corporate_logo']))
		{
			$rsItem['pic_url'] = '/img/instead/img_no-image_' . sprintf("%02d", rand(1, 10)) . '.jpg';
		}
		else
		{
			$rsItem['pic_url'] = "/upload/secret/jobs/".$career['corporate_logo'];
		}
		
		//-salary----------------------------------------------------
		$rsItem['salary'] 	= strip_tags($career['hourly_wage']);
		if(mb_strlen($rsItem['salary'])>50)
		{
			$rsItem['salary'] = mb_substr($item['salary'],0,50,'UTF-8').'...';
		}
		
		//-title
		$rsItem['title'] = $career['job'];
		$rsItem['clip_title'] = mb_substr(strip_tags($career['job']),0,65,'utf-8');
		
		//-company
		$rsItem['company'] = $corporate['company_name'];
		if(mb_strlen($corporate['company_name'])>25)
		{
			$rsItem['company'] = mb_substr($corporate['company_name'],0,25,'UTF-8').'...';
		}

		//-isnew
		$rsItem['isNew'] = 0;
		//604800 = 7 days
		$timeInAWeek = time()-604800;
		if(strtotime($career['created']) > $timeInAWeek)
		{
			$rsItem['isNew'] = 1;
		}
		
		//-label
		$rsItem['labels'] = explode(',',$career['employment']);
		//Sort category follows length
		usort($rsItem['labels'], function($a, $b) {
			return strlen($a) - strlen($b);
		});
		$rsItem['maxCatLeng'] = 30;
		
		//-href
		$rsItem['href'] = '/secret/'.urlencode($rsItem['title']);
		
		//-benefit
		$rsItem['benefit'] = strip_tags($career['benefit']);
		
		$rsItem['class'] = "sc-li";
		
		$rsItem['secret'] = "1";
		return $rsItem;
	}
}