<?php  
App::uses('Component', 'Controller');
class FlashExComponent extends Component {

    public $components = array('Session');
    
    public function set($obj)
    {
        $this->Session->write('FlashExOTSS', $obj);
    }
}
