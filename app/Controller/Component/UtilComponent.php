<?php

App::uses('Component', 'Controller');
class UtilComponent extends Component {
	
	public static $_salt = '*_%%!~DFDSD^%@!@&#@(&@!^(%*^*%^&$#@&#*@*!@#**()CVXNCCKX';
	//one day
	public static function getShareLink($link,$id,$day=1)
	{
		$time=86400;
		$expire = time() + ($day*$time);
		$hash = md5($id.$expire.self::$_salt);
		return $link."?day=$day&e=$expire&h=$hash";
	}
	
	public static function checkShareLink($id, $expire, $hash)
	{
		$_expire = time();
		if($expire < $_expire)
		{
			//time out
			return false;
		}
		
		$_hash = md5($id.$expire.self::$_salt);
		if($_hash != $hash)
		{
			//access deny
			return false;
		}
		
		return true;
	}
	
	
	public function isMobile()
    {
        $useragent=$_SERVER['HTTP_USER_AGENT'];
        if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
            return true;
        return false;
    }

    public function replaceSpecialKey($keywordString)
    {
    	$replaceSpecialKeys = [' ', '（', '）', '　', '(', ')', '／'];
    	$keywordString = str_replace($replaceSpecialKeys, ' ', $keywordString);

    	$replaceKeys = ['株式会社', '本社', '株'];
    	$keywordString = str_replace($replaceKeys, ' ', $keywordString);

    	$keywordString = trim($keywordString);

		$keywords = explode(' ', $keywordString);

    	return $keywords;
    	
    }	
	
	public function jobUrlEncode($unique_id)
	{
		//TODO: jobUrlEncode
		return str_replace('/', '-SL4SH-', $unique_id);
	}
	
	public function jobUrlDecode($unique_id)
	{
		//TODO: jobUrlDecode
		return str_replace('-SL4SH-', '/', $unique_id);
	}
	
	public function proxy_image($url)
  	{
  	 	return '/proxy?url='.rawurlencode($url);
  	}


    //--HREF--------------------
    /*
    $data = [
        [category] => 働く人の声,
        [category_alias] => work
    ]
     */
    public function createContentCategoryHref($data)
    {
        return '/contents/'.$data['category'];
    }
    
    /*
    $data = [
        [title] => 沖縄に移住したい！？　それなら那覇市のコールセンターがおすすめですよ！,
        [id] => 594,
        [cat_names] => エリア特集,
        [cat_alias] => japanesecallcenter,
        [title_alias] => okinawa_callcenter
    ]
     */
    public function createContentDetailHref($data)
    {
        return '/contents/'.$this->_urlencode($data['title']);
    }
    
    /*
    $data = [
        [id] => 1,
        [job] => 東京本社で新メンバー積極募集中★ＩＴの専門知識が身に付くお仕事です！！
    ]
     */
    public function createSecretDetailHref($data)
    {
        //return '/secret/'.'「'. $data['job'] .'」';
        return '/secret/'.rawurlencode($data['job']);
    }

    public function countJobToday()
    {
        $contents = ClassRegistry::init('Contents');

        $totalJobToDay = $contents->countJobToday();
        $totalSecretJobToday = $contents->countSecretJobToday();

        $total = $totalJobToDay + $totalSecretJobToday;

        return $total;
    }
	
	public function _urlencode($value)
	{
		$find = array(
			'%',
			' ',  
			'"',	
			'#',	
			'&',	
			"'",	
			'+',	
			'.',	
			'/',	
			'?',	
			'^',	
			'`',	
			'{',	
			'|',	
			'}',
			'\\',
		);	

		$replace = array(
			'%25',
			'%20',
			'%22',
			'%23',
			'%26',
			'%27',
			'%2B',
			'%2E',
			'%2F',
			'%3F',
			'%5E',
			'%60',
			'%7B',
			'%7C',
			'%7D',
			'%5C',
		);
		
		return str_replace($find, $replace, $value);
	}
	
	public function generateRandomString($length = 6) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
}
