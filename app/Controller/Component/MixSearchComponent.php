<?php
App::uses('Component', 'Controller');
class MixSearchComponent extends Component {
	
	/*
		Search by Conditions A
		Result:
			Job: 50
			Secret: 15
			
		Pagination: 20
		1. 
	*/
	public $components = array('CloudSearch', 'ViewSearchProcess');
	
	protected $fromSecret = 0;
	protected $toSecret = 0;
	protected $fromJob = 0;
	protected $toJob = 0;
	protected $total = 0;
	
	protected $totalSearch = 0;
	protected $totalSecret = 0;
	protected $size = 30;
	protected $ratioSecret = 1;
	protected $ratioJob = 4;
	
	
	public function initialize(Controller $controller) {
		//Add Secret tables
		$this->Secret = ClassRegistry::init('CareerOpportunity');
    }
	
	public function setSize($_size)
	{
		$this->size = $_size;
	}
	
	public function setRatio($secret, $job)
	{
		$this->ratioSecret = $secret;
		$this->ratioJob = $job;
	}
	
	public function getCountOnly($conditions)
	{
		$totalJob = $this->CloudSearch->countOnly($conditions);
		$totalSecret = $this->getSecretCountOnly($conditions);
		
		return ($totalJob + $totalSecret);
	}
	
	public function asynProcess($data)
	{
		$current_total = $data['current_total'];
		$total_job = $data['current_job'];
		$total_secret = $data['current_secret'];
		$index_job = 0;
		$index_secret = 0;
		
		$arrData = array();
		for($i=0; $i<$current_total; $i++)
		{
			if($i%5 == 0 && $index_secret < $total_secret)
			{
				$arrData[] = $this->ViewSearchProcess->secret($data['data_secret'][$index_secret]);
				$index_secret++;
			}
			else
			{
				if($index_job < $total_job)
				{
					$arrData[] = $this->ViewSearchProcess->search($data['data_job'][0][$index_job]);
					$index_job++;
				}
			}
		}
		return $arrData;
	}
		
	public function getMixSearch($conditions, $page=1)
	{
		$size = $this->size;
		$ratioSecret = $this->ratioSecret;
		$ratioJob = $this->ratioJob;
		
		$totalJob = $this->CloudSearch->countOnly($conditions);
		$totalSecret = $this->getSecretCountOnly($conditions);
		
		$this->mixTwoDataPagination($totalSecret, $totalJob, $page, $size, $ratioSecret, $ratioJob);

//		print_r(array(
//			$size,
//			$page,
//			$this->fromJob,
//			$this->toJob,
//			$totalJob,
//			$totalSecret
//		));
//		die;
		//Get CloudSearch
		$this->CloudSearch->setPageByIndex($this->fromJob, $this->toJob);
		$CloudSearch =  $this->CloudSearch->search($conditions);
		
		//Get Secret
		$Secret = $this->getSecretResult($conditions, $size);
		
		
		$total = $totalJob+$totalSecret;
		$current_job = count($CloudSearch[0]);
		$current_secret =  count($Secret);
		$current_total = $current_job+$current_secret;
		
		return array(
			"total_job" => $totalJob,
			"total_secret" => $totalSecret,
			"total_data" => $total,
			"total_page" => ceil($total/$size),
			"current_page" => $page,
			"current_total" => $current_total,
			"current_job" => $current_job,
			"current_secret" => $current_secret,
			"size" => $size,
			"ratioSecret" => $ratioSecret,
			"ratioJob" => $ratioJob,
			"data_job" => $CloudSearch,
			"data_secret" => $Secret,
		);
	}
	
	public function getTotal()
	{
		$conditions = array(
				'page' => 1,
				'em'   => 0,
			);

		$this->Secret = ClassRegistry::init('CareerOpportunity');
		$size = $this->size;
		$ratioSecret = $this->ratioSecret;
		$ratioJob = $this->ratioJob;

		$totalJob = $this->CloudSearch->countOnly($conditions);
		$totalSecret = $this->getSecretCountOnly($conditions);
		return $totalJob + $totalSecret;
	}

	protected function mixTwoDataPagination($totalSecret, $totalJob, $page, $size, $ratioSecret, $ratioJob)
	{
		$totalRatio = $ratioSecret + $ratioJob;
		$totalSecretInPage = $size / $totalRatio * $ratioSecret;
		$totalJobInPage = $size / $totalRatio * $ratioJob;

		$noFullJobPage = intval($totalJob / $totalJobInPage) + 1;
		$incompleteAtNoFullJobPage = $totalJobInPage - ($totalJob % $totalJobInPage);
		$endSecretAtNoFullJobPage = $totalSecretInPage * $noFullJobPage + $incompleteAtNoFullJobPage;

		$noFullSecretPage = intval($totalSecret / $totalSecretInPage) + 1;
		$incompleteAtNoFullSecretPage = $totalSecretInPage - ($totalSecret % $totalSecretInPage);
		$endJobAtNoFullSecretPage = $totalJobInPage * $noFullSecretPage + $incompleteAtNoFullSecretPage;

		if($page > $noFullJobPage && $page > $noFullSecretPage)
		{
			$currentSecretInPage = 0;
			$currentJobInPage = 0;
		}
		else
		{
			if($noFullSecretPage < $noFullJobPage)
			{
				if($page < $noFullSecretPage)
				{
					$currentSecretInPage = $totalSecretInPage;
					$currentJobInPage = $totalJobInPage;
				}
				else if($page == $noFullSecretPage)
				{
					$currentSecretInPage = $totalSecretInPage - $incompleteAtNoFullSecretPage;
					$currentJobInPage = $size - ($totalSecretInPage - $incompleteAtNoFullSecretPage);
				}
				else //$page > $noFullSecretPage
				{
					$currentSecretInPage = 0;
					
					if($totalJob >= $endJobAtNoFullSecretPage + $size * ($page - $noFullSecretPage))
					{
						$currentJobInPage = $size;
					}
					else
					{
						$currentJobInPage =  $totalJob - ($endJobAtNoFullSecretPage + ($page - $noFullSecretPage -1) * $size);
						if($currentJobInPage < 0)
						{
							$currentJobInPage = 0;
						}
					}
				}
			}
			else if($noFullSecretPage == $noFullJobPage)
			{
				$currentSecretInPage = $totalSecretInPage - $incompleteAtNoFullSecretPage;
				$currentJobInPage = $totalJobInPage - $incompleteAtNoFullJobPage;
			}
			else //$noFullSecretPage > $noFullJobPage
			{

				$currentSecretInPage = $totalSecretInPage;
				if($noFullJobPage == $page)
				{
					$currentJobInPage = $totalJobInPage - $incompleteAtNoFullJobPage;
					if($currentJobInPage + $totalSecret < $size)
					{
						$currentSecretInPage = $totalSecret;
					}

				}
				else if($noFullJobPage > $page)
				{
					$currentJobInPage = $totalJobInPage;
				}
				else
				{
					$currentJobInPage = 0;
					$currentSecretInPage = $size;
				}
			}
		}
		
		if($currentSecretInPage == $totalSecretInPage && $currentJobInPage == $totalJobInPage)
		{
			$fromSecret = $totalSecretInPage * $page - $totalSecretInPage;
			$toSecret = $totalSecretInPage * $page;

			$fromJob = $totalJobInPage * ($page - 1);
			$toJob = $totalJobInPage * $page;
		}
		else 
		{
			if($currentJobInPage >= $totalJobInPage) //&& currentSecretInPage < totalSecretInPage
			{
				$incompleteSecret = $totalSecretInPage - $currentSecretInPage;
				if($currentSecretInPage != 0)
				{
					$fromSecret = $totalSecretInPage * ($page - 1);
		 			$toSecret = $totalSecretInPage * ($page - 1) + $currentSecretInPage;

		 			$fromJob = $totalJobInPage * ($page - 1);
		 			$toJob = $totalJobInPage * $page + $incompleteSecret;
				}
				else // $currentSecretInPage == 0
				{
					$fromSecret = 0;
		 			$toSecret = 0;
		 			if($page == $noFullSecretPage)
		 			{	
		 				$fromJob = $totalJobInPage * ($page - 1);
		 				$toJob = $totalJobInPage * $page + $incompleteSecret;
		 			}
		 			else //$page > $noFullSecretPage
		 			{
		 				$fromJob = $endJobAtNoFullSecretPage + $size * ($page - $noFullSecretPage - 1);
		 				$toJob = $fromJob + $size;
		 			}
				}

				if($toJob > $totalJob)
				{
					$toJob = $totalJob;
				}
			}
			else if($currentSecretInPage >= $totalSecretInPage) //&& currentJobInPage < totalJobInPage
			{
				$incompleteJob = $totalJobInPage - $currentJobInPage;
				
				if($currentJobInPage != 0)
				{
					$fromJob = $totalJobInPage * ($page - 1);
		 			$toJob = $fromJob + $currentJobInPage;

		 			$fromSecret = $totalSecretInPage * ($page - 1);
		 			$toSecret = $totalSecretInPage * $page + $incompleteJob;

		 			if($toSecret > $totalSecret)
		 			{
		 				$toSecret = $totalSecret;
		 			}
				}
				else // $currentSecretInPage == 0
				{
					$fromJob = 0;
		 			$toJob = 0;

					if($page == $noFullJobPage)
		 			{
		 				$fromSecret = $totalSecretInPage * ($page - 1);
		 				$toSecret = $totalSecretInPage * $page + $incompleteJob;

		 				if($toSecret > $totalSecret)
		 				{
		 					$toSecret = $totalSecret;
		 				}
		 			}
		 			else
		 			{
		 				$incompleteAtNoFullJobPage = $totalJobInPage - ($totalJob % $totalJobInPage);

		 				//$fromSecret = $totalSecretInPage * ($page - 1) + $incompleteAtNoFullJobPage;
		 				$fromSecret = $endSecretAtNoFullJobPage + ($page - $noFullJobPage - 1) * $size;
		 				$toSecret = $fromSecret + $currentSecretInPage;


		 				if($toSecret > $totalSecret)
		 				{
		 					$toSecret = $totalSecret;
		 				}

		 				if($fromSecret >= $totalSecret)
		 				{
		 					$fromSecret = 0;
		 					$toSecret = 0;
		 				}
		 			}

				}
			}
			else //currentSecretInPage < totalSecretInPage && currentJobInPage < totalJobInPage
			{
				
				if($currentJobInPage == 0)
				{
					$fromJob = 0;
					$toJob = 0;
				}

				if($currentSecretInPage ==0)
				{
					$fromSecret = 0;
					$toSecret = 0;
				}

				if($currentJobInPage != 0)
				{
					$fromJob = $endJobAtNoFullSecretPage + ($page - $noFullSecretPage - 1) * $size;
					$toJob = $fromJob + $currentJobInPage;
				}

				if($endJobAtNoFullSecretPage > $totalJob && $page == 1)
				{
					$fromJob = 0;
					$toJob = $totalJob;
				}

				if($endSecretAtNoFullJobPage > $totalSecret && $page == 1)
				{
					$fromSecret = 0;
					$toSecret = $totalSecret;
				}
			}
		}

		$this->fromSecret = $fromSecret;
		$this->toSecret = ($toSecret-$fromSecret);
		$this->fromJob = $fromJob;
		$this->toJob = ($toJob-$fromJob);
	}
	
	protected function getSecretCountOnly($conditions)
	{
		return $this->Secret->getTotalOnly($conditions);
	}
	
	protected function getSecretResult($conditions)
	{
		return $this->Secret->getSecretByRange($conditions, $this->fromSecret, $this->toSecret);
	}
	
	
}