<?php
class SendmailComponent extends Component {
	
	protected $__ErrorInfo ='';
	protected $__render = '';
	protected $__bccMail = '';
	
	public $CharSet = "UTF-8";
	public $SMTPDebug = 0;
	public $Debugoutput = 'html';
	public $fromlAdress = 'no-reply@callnavi.jp';
	public $fromlName = 'PHPmailer';
	
	//Set the hostname of the mail server
	public $Host = "localhost";
	//Set the SMTP port number - likely to be 25, 465 or 587
	public $Port = 25;
	//Whether to use SMTP authentication
	public $SMTPAuth = true;
	//Username to use for SMTP authentication
	public $Username = null;
	//Password to use for SMTP authentication
	public $Password = null;
	
	public function __construct() {	
		
		require $_SERVER['DOCUMENT_ROOT'].'/vendor/PHPMailer/PHPMailerAutoload.php';
		
		$this->Host = Configure::read('webconfig.email_host');
		//Set the SMTP port number - likely to be 25, 465 or 587
		$this->Port = Configure::read('webconfig.email_port');
		//Whether to use SMTP authentication
		$this->SMTPAuth = Configure::read('webconfig.email_SMTPAuth');
		//Username to use for SMTP authentication
		$this->Username = Configure::read('webconfig.email_username');
		//Password to use for SMTP authentication
		$this->Password = Configure::read('webconfig.email_password');
	}
	
	public function error()
	{
		return $this->__ErrorInfo;
	}
	
	public function getRender()
	{
		return $this->__render;
	}
	
	public function render($path, $data=array())
	{
		$view = new View(null, false);
		$view->viewPath='Emails';  // Directory inside view directory to search for .ctp files
		$view->layout=false; // if you want to disable layout
		
		// set your variables for view here
		if(!empty($data))
		{
			foreach( $data as $key=>$val)
			{
				$view->set($key,$val);
			}
		}
		
		$this->__render=$view->render($path);
	}
	
	public function addBCC($mail)
	{
		$this->__bccMail = $mail;
	}
	
	public function send($emailAdress, $emailName, $subject='', $content='', $isText=true) {
		if ($emailAdress) {
			//Create a new PHPMailer instance
			$mail = new PHPMailer;		
			$mail->CharSet = $this->CharSet;
			//Tell PHPMailer to use SMTP
			$mail->isSMTP();
			$mail->SMTPAuth = $this->SMTPAuth;
			//Enable SMTP debugging
			// 0 = off (for production use)
			// 1 = client messages
			// 2 = client and server messages
			$mail->SMTPDebug = 0;
			//Ask for HTML-friendly debug output
			$mail->Debugoutput = $this->Debugoutput;
			
			//Set the hostname of the mail server
			$mail->Host = $this->Host;
			//Set the SMTP port number - likely to be 25, 465 or 587
			$mail->Port = $this->Port;
			//Whether to use SMTP authentication
			$mail->SMTPAuth = $this->SMTPAuth;
			//Username to use for SMTP authentication
			$mail->Username = $this->Username;
			//Password to use for SMTP authentication
			$mail->Password = $this->Password;
			
			
			//Set who the message is to be sent from
			$mail->setFrom($this->fromlAdress, $this->fromlName);
			//Set an alternative reply-to address
			//$mail->addReplyTo('replyto@example.com', 'First Last');
			//Set who the message is to be sent to
			

			$mail->addAddress($emailAdress,$emailName);

			/*
			$mail->addAddress('naoya.natsuhara@jellyfishhr.com', 'Naoya Natsuhara');
			$mail->addAddress('tomoyo.miyano@jellyfishhr.com', 'Tomoyo Miyano');
			$mail->addAddress('gonakao@jellyfish-vn.com', 'Go Nakao');
			$mail->addAddress('kida@jellyfish-vn.com', 'Hiroki Kida');
			*/
			
			$mail = $this->addBccIntoMail($mail);
			//$mail->AddBCC('haruka.tsujino@jellyfishhr.com','Tsujino Haruka');
			//$mail->AddBCC("tan.doan@jellyfishhr.com", "Tan Doan");
			//$mail->AddBCC('khanh.pham@jellyfishhr.com','Khanh Pham');
			
			
			
			//Set the subject line
			$mail->Subject = $subject;
			//Read an HTML message body from an external file, convert referenced images to embedded,
			//convert HTML into a basic plain-text alternative body
			//$mail->msgHTML(file_get_contents($content), dirname(__FILE__));
			
			if(!empty($this->__render))
			{
				$content = $this->__render;
			}
			
			if($isText)
			{
				//Replace the plain text body with one created manually
				//$mail->AltBody = $content;
				$mail->Body = $content;
			}
			else
			{
				$mail->msgHTML($content);
			}
			
			
			//Attach an image file
			//$mail->addAttachment('images/phpmailer_mini.png');
			//$mail->addAttachment($fullPathfileName);

			//send the message, check for errors
			if (!$mail->send()) {
				$this->__ErrorInfo = $mail->ErrorInfo;
				return false;//echo "Mailer Error: " . $mail->ErrorInfo . "<br/>";
			} else {
				return true;//echo "Message sent! <br/>";
			}
		}
	}
	
	/**
	 * Created at: 2017-09-05
	 * Author: Xuong
	 * Description: the open of Bcc function, allow add more than 1 email address
	 * @param  Class $mail
	 * @return Class $mail
	 */
	protected function addBccIntoMail($mail)
	{
		if($this->__bccMail)
		{
			if(is_array($this->__bccMail))
			{
				foreach( $this->__bccMail as $bccMail)
				{
					$mail->AddBCC($bccMail);
				}
			}
			else
			{
				$mail->AddBCC($this->__bccMail);
			}

			//$mail->AddBCC('xuong.banh@jellyfishhr.com', 'Xuong - concierge');
		}
		
		//set empty for bcc
		$this->__bccMail = '';
		
		return $mail;
	}
}
