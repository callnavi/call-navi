<?php
/**
 * SearchedQuery model
 *
 * @category    Model
 */
use Aws\CloudSearchDomain\CloudSearchDomainClient;
use Aws\CloudSearch\CloudSearchClient;
App::uses('Component', 'Controller');
App::uses('Helper', 'View');
App::uses('UtilComponent', 'Controller/Component');
App::uses('MixSearchComponent', 'Controller/Component');
App::uses('SessionHelper', 'View/Helper');
App::import("Model", "Contents");
App::import("Model", "CareerOpportunity");
App::import("Model", "CorporatePrs");
App::import("Model", "Entry");

class CodeComponent extends Component {
    //set code for generatee
    public function genApplyCode($post){
        $job_id = $post['job_id'];
        
        $skill = $post['step1skill'];
        if(isset($post['step1time'])){
            $working_time = $post['step1time'];
            
        }else{
            $working_time = "";
        }
        
        $media_id = "CAL";
        //get corporate code
        $corporate_code = $this->retCompanyCode($job_id);
        if( $corporate_code == ""){
            $corporate_code = "C0000";
        }        
        //get the job code no need job code
        $job =  $this->retCompanyId($job_id);
        $job_code = $job[0]['career_opportunities']['job_code'];
        
        if($job_code == ""){
            $job_code = "J0000";
        }
        
        //combining 4 value into 1 string
        $val = $corporate_code . "-" . $media_id ."-" .$job_code;

        //Generate New Apply id 
        $apply_result = intval($this->genApplyID($val));
        do { 
        //generate patern for apply id ( first string , leght, new id) 
        $apply_result = $apply_result + 1;
        $apply_code = $this->codeGenerated("A",5,$apply_result);
            
        //checking if the code already exiist
        } while ($this->CheckIdAvailable($val . "-" .$apply_code) == false );    
        
        $val = $val . "-" .$apply_code;
        
        //generate rank id
        $rank_code = $this->genRankId($skill , $job_id , $working_time);
        
        //generate data 
        $val = $val . "-" .$rank_code;
            
        return $val;
    }   
    
    //get code from company data
    public function retCompanyCode($job_id){
        $com_id = $this->retCompanyId($job_id);
        $val = $com_id[0]['career_opportunities']['company_id'];
        
         $CorporatePrs = new CorporatePrs();
        
        $result =  $CorporatePrs->query("SELECT corporate_code  FROM corporate_prs where corporate_prs.id = '".$val."';");
        
        return $result[0]['corporate_prs']['corporate_code'];
    }   
    
    //retriving company id for getting the company code
    public function retCompanyId($job_id){
        $CareerOpportunity = new CareerOpportunity();
        
        $result =  $CareerOpportunity->query("SELECT company_id , job_code  FROM career_opportunities where career_opportunities.id = '".$job_id."';");
        
        return $result;
    }
    
    //public funtion get biggest apply code
    public function genApplyID($val){
        $Entry = new Entry();
        
        $result =  $Entry->query("SELECT SUBSTRING(apply_code , 18 , 4) as apply_code FROM entry where entry.apply_code like '".$val."%' ORDER BY SUBSTRING(apply_code, 18, 4) DESC;");
        if (isset($result)){
             $val_result = "0";
        }else{
             $val_result = $result[0][0]['apply_code'];
        }
       
        return $val_result;
    }
    
    //function for generate new code
	public function codeGenerated($FirstString, $lenCode, $value){
        $val = $value;
        $len = $lenCode;

        while(strlen($val) < $len -1){
            $val = "0" . $val;
        }
        $val = $FirstString . $val;
        return $val;
    }
    
    //function for chenk the code already use or not
    public function CheckIdAvailable($id_val){
        
        $flag = true;
        
        $Entry = new Entry();
            
        $check =  $Entry->query("SELECT count(*) as count FROM entry where entry.apply_code like '".$id_val."%';");
        
        if($check[0][0]['count'] > 0){
            $flag = false;
        }
        return $flag;
    }  
    
    //function making array comparition
    public function genRankId($skill, $job_id, $working_time)
    {
        $RankId = "";
        //makearray for validation
        $valArr = $this->makingArrSkill();
        
        //getting employment data
        $empArr = $this->getArrEmployment($job_id);
        
        //generating emplyment data validating
        $valiEmp = $this->makingArrEmployment();
        
        //generating data validation for working time
        $valiTime = $this->makingTimeVal();
        
        //validating for expert
        if((in_array($valArr['5'], $skill) == true) || ((in_array($valArr['3'], $skill) == true)&&(in_array($valArr['4'], $skill) == true))){
            //expert validationg employment
            if((in_array($valiEmp['2'], $empArr) == true) || (in_array($valiEmp['3'], $empArr) == true)){
                //expert validating if partime working value
                if($working_time == $valiTime['1']){
                    $RankId = "E02";                
                }elseif($working_time == $valiTime['2']){
                    $RankId = "E03";                
                }elseif($working_time == $valiTime['3']){
                    $RankId = "E04";                
                }
            }elseif((in_array($valiEmp['1'], $empArr) == true) || (in_array($valiEmp['4'], $empArr) == true)){
                $RankId = "E01";                
            }elseif((in_array($valiEmp['5'], $empArr) == true) || (in_array($valiEmp['6'], $empArr) == true)){
                $RankId = "S01";                
            }
        }elseif((in_array($valArr['1'], $skill) == true) || (in_array($valArr['2'], $skill) == true) || (in_array($valArr['3'], $skill) == true) || (in_array($valArr['4'], $skill) == true))
        {
            if((in_array($valiEmp['2'], $empArr) == true) || (in_array($valiEmp['3'], $empArr) == true)){
                if($working_time == $valiTime['1']){
                    $RankId = "N02";                
                }elseif($working_time == $valiTime['2']){
                    $RankId = "N03";                
                }elseif($working_time == $valiTime['3']){
                    $RankId = "N04";                
                }
            }elseif((in_array($valiEmp['1'], $empArr) == true) || (in_array($valiEmp['4'], $empArr) == true)){
                $RankId = "N01";                
            }elseif((in_array($valiEmp['5'], $empArr) == true) || (in_array($valiEmp['6'], $empArr) == true)){
                $RankId = "S01";                
            }            
        }elseif((in_array($valArr['6'], $skill) == true)){
            if((in_array($valiEmp['2'], $empArr) == true) || (in_array($valiEmp['3'], $empArr) == true)){
                if($working_time == $valiTime['1']){
                    $RankId = "B02";                
                }elseif($working_time == $valiTime['2']){
                    $RankId = "B03";                
                }elseif($working_time == $valiTime['3']){
                    $RankId = "B04";                
                }
            }elseif((in_array($valiEmp['1'], $empArr) == true) || (in_array($valiEmp['4'], $empArr) == true)){
                $RankId = "B01";                
            }elseif((in_array($valiEmp['5'], $empArr) == true) || (in_array($valiEmp['6'], $empArr) == true)){
                $RankId = "S01";                
            }
        }else{
            $RankId = "S01";            
        }
        return  $RankId;
    }
    
    //making array for validation
    public function makingArrSkill()
    {
        $arr = array(
            "1" => "コールセンターで働いたことがある",
            "2" => "営業経験がある",
            "3" => "マネージメント経験がある",
            "4" => "営業成績で表彰を受けたことがある",
            "5" => "コールセンターでSV経験がある",
            "6" => "どれにも当てはまらない",
        );
            return $arr;
    }
    
        
    //retrive data emloyment from career oportunity
    public function getArrEmployment($job_id)
    {
        $CareerOpportunity = new CareerOpportunity();
        
        $result =  $CareerOpportunity->query("SELECT employment  FROM career_opportunities where career_opportunities.id = '".$job_id."';");
        
        $arr = explode(",",$result[0]['career_opportunities']['employment']);
        return $arr;
    }

    
    //making array employment for validation
    public function makingArrEmployment()
    {
        $arr = array(
            "1" => "正社員",
            "2" => "アルバイト",
            "3" => "パート",
            "4" => "契約社員",
            "5" => "派遣",
            "6" => "業務委託",
        );
            return $arr;
    }
    
        //making array validating working timee
    public function makingTimeVal()
    {
        $arr = array(
            "1" => "120時間以上",
            "2" => "120時間未満",
            "3" => "36時間未満",
           
        );
            return $arr;
    }
    /*
    function working data
    author = andy william
    date created = 19 july 2017
    
    retrive data = post data
    
    return working data
    */
    public function getWorkingtime($data){
        // get string of rank first number
        $working_time = "";
        if(isset($data['step1time'])){
             $working_time = $data['step1time'];
             
        }else{
            $working_time ="";
           
        }
        return $working_time;
    }
  /*
    function get clasification data
    author = andy william
    date created = 19 july 2017
    
    retrive data = apply code
    
    return data classification name
    */
    public function genClassificationData($apply_code){
        // get string of rank first number
         $checkClass = substr($apply_code, 22 ,1);
        
        // check rank first number
        if( $checkClass == "E"){
            $classification = "エキスパート";
        }elseif( $checkClass == "N"){
            $classification = "ノーマル";
        }elseif( $checkClass == "B"){
            $classification = "ビギナー";
        }else{
            $classification = "";
        }
        return $classification;
    }       
    
    /*
    function combine skill to string
    author = andy william
    date created = 19 july 2017
    
    retrive data = post data
    
    return data string skill
    */
    public function comSkill($data){
        // get string of rank first number
         $skill = "";
         foreach($data['step1skill'] as $val){
            if ($skill == ""){
                $skill = $val;
            }else{
                $skill = $skill . "," . $val;
            }
        }
        return $skill;
    }   
    
    /*
    function getting data of career
    author = andy william
    date created = 19 july 2017
    
    retrive data = post data
    
    return data string skill
    */
   
    public function retMailCarrerr($id){
    $CareerOpportunity = new CareerOpportunity();
    $item = $CareerOpportunity->find('first', array(
            'fields' => array('job',
                              'company_id',
							  'application_email', 
							  'is_upload_cv', 
							  'required_upload_cv', 
							  'corporate_prs.company_name'
							 ),
            'conditions' => array('CareerOpportunity.id' => $id),
            'joins' => array(
                array(
                    'table' => 'corporate_prs',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'corporate_prs.id = CareerOpportunity.company_id',
                    ),
                ),
            ),
        ));
        return $item;
     }
    // Function For generate New JOb ID
    public function generateNewJobCode($company_code){
        $CareerOpportunity = new CareerOpportunity();
		$results = $CareerOpportunity->query("SELECT max(job_code) as max_job_code FROM career_opportunities where company_id = '".  $company_code ."';");
        $result_value = "";
        $return_result = "";
        
            
        if(strlen($results[0][0]['max_job_code']) > 4){
            $result_value = (substr($results[0][0]['max_job_code'],1,4));
            do {            
                //make new result from max id
                $result_value = $result_value + 1;
                 
                // Make the format of the code string
                $return_result = $this->Code->codeGenerated("J","5",$result_value);  
                
             

            } while ($this->Code->CheckIdAvailable("career_opportunities","job_code",$return_result) == false );    
        }
        
        return $return_result;
    }         
    

}