<?php
App::uses('Component', 'Controller');
class CsvComponent extends Component  {

	var $delimiter = ',';
	var $enclosure = '"';
	var $filename = 'Export.csv';
	var $tmpFilename = 'Export.csv';
	var $line = array();
	var $buffer;

	function Csv() {
		$this->clear();
	}

	function clear() {
		$this->line = array();
		$this->buffer = fopen('php://temp/maxmemory:'. (5*1024*1024), 'r+');
	}

	function addField($value) {
		$this->line[] = $value;
	}

	function endRow() {
		$this->addRow($this->line);
		$this->line = array();
	}

	function addRow($row) {
		fputcsv($this->buffer, $row, $this->delimiter, $this->enclosure);
	}

	function renderHeaders() {
		header("Content-type:application/vnd.ms-excel");
		header("Content-disposition:attachment;filename=".$this->filename);
	}

	function setFilename($filename) {
		$this->filename = $filename;
		if (strtolower(substr($this->filename, -4)) != '.csv') {
			$this->filename .= '.csv';
		}
	}

	function render($outputHeaders = true, $to_encoding = null, $from_encoding = "auto") {
		if ($outputHeaders) {
			if (is_string($outputHeaders)) {
				$this->setFilename($outputHeaders);
			}
			$this->renderHeaders();
		}
		rewind($this->buffer);
		$output = stream_get_contents($this->buffer);
		if ($to_encoding) {
			$output = mb_convert_encoding($output, $to_encoding, $from_encoding);
		}
		return $output;
	}
    
    
    /*---------------------*/
    function checkFile($filename = 'csv')
    {
        if(isset($_FILES[$filename]))
        if($_FILES[$filename]['type'] == 'application/vnd.ms-excel' || $_FILES[$filename]['type'] == 'text/csv')
        if($_FILES[$filename]['error'] == 0)
        if($_FILES[$filename]['size'] > 0) 
        {
            return 1;
        }
		
        return 0;
    }

    /**
     * New updated at: 2017-10-02
     * 
     * 1. check encode of file, if not UTF-8 => convert
     * 2. move file to backup folder
     * 3. read and return file
     * 
     * @param  string $path
     * @return array
     */
    function getCsv($path){
        
		//TODO:
        $row = 1;
        $arr = array();
        if (($handle = fopen($path, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $arr[] = $data;
            }
            fclose($handle);
            return $arr;
        }
        return "Can't open this CSV";
    }//End function
    
    /**
     * check and read tmp file, 
     * then convert to ARRAY, 
     * move to /upload_csv 
     * 
     * @param  string [$filename = 'csv'] 
     * @return ARRAY
     */
    function getFile($filename = 'csv')
    {
		$file = $_FILES[$filename];
        $path = $file['tmp_name'];
		$fileName = $file['name'];
		$this->tmpFilename = $fileName;
		if(!$this->isUTF8($path))
		{
			//convert and move to new folder
			file_put_contents($path, mb_convert_encoding(file_get_contents($path), 'UTF-8', 'Shift-JIS, EUC-JP, JIS, SJIS, JIS-ms, eucJP-win, SJIS-win, ISO-2022-JP,ISO-2022-JP-MS, SJIS-mac, SJIS-Mobile#DOCOMO, SJIS-Mobile#KDDI,SJIS-Mobile#SOFTBANK, UTF-8-Mobile#DOCOMO, UTF-8-Mobile#KDDI-A,UTF-8-Mobile#KDDI-B, UTF-8-Mobile#SOFTBANK, ISO-2022-JP-MOBILE#KDDI, SHIFT_JIS-2004'));
		}
        $arr = array();
        if (($handle = fopen($path, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 2000, ",")) !== FALSE) {
                $arr[] = $data;
            }
            fclose($handle);
        }
		$this->backupTMP($file);
		return $arr;
    }
    
    /**
     * Array to Array key
     */
    function changeToObj($obj, $file, $trim = true)
    {
        /*
            OBJ:
            [
                'header 1' => 'column1',
                'header 2' => 'column2',
                'header 3' => 'column3',
            ]
            
            FILE:
            [
                0 => ['header 1', 'header 2', 'header 3'],
                1 => ['data1', 'data 2', 'data 3'],
                2 => ['data1', 'data 2', 'data 3'],
            ]
            
            RESULT:
            [
                0 => ['column1' => 'data1', 'column2' => 'data 2', 'column3' => 'data 3'],
                1 => ['column1' => 'data1', 'column2' => 'data 2', 'column3' => 'data 3'],
            ]
        */
        
        $header = $file[0];
        $newHeader = [];
        //find pos and set
        foreach( $header as $index => $name)
        {//pr($obj[$name]);
            //$name = trim($name);
            //if(isset($obj[$name]) && $obj[$name] != '')
            //{
                //$newHeader[$obj[$name]] = $index;
            //} 
            $newHeader[$name] = $index;  
        }        
        unset($file[0]);
        $resultFile = [];
        foreach( $file as $record)
        {
            $pushObj = [];
            foreach( $newHeader as $key=>$index)
            {
                if(isset($record[$index]))
                {
                    $pushObj[$key]=
                        $trim?
                        trim($record[$index]):
                        $record[$index];
                }
                else
                {
                    $pushObj[$key] = '';
                }
            }
            if($pushObj)
            {
                $resultFile[] = $pushObj;
            }
        }
        
        return $resultFile;
    }

    function convertToObject($obj, $file, $trim = true)
    {
        /*
            OBJ:
            [
                'header 1' => 'column1',
                'header 2' => 'column2',
                'header 3' => 'column3',
            ]
            
            FILE:
            [
                0 => ['header 1', 'header 2', 'header 3'],
                1 => ['data1', 'data 2', 'data 3'],
                2 => ['data1', 'data 2', 'data 3'],
            ]
            
            RESULT:
            [
                0 => ['column1' => 'data1', 'column2' => 'data 2', 'column3' => 'data 3'],
                1 => ['column1' => 'data1', 'column2' => 'data 2', 'column3' => 'data 3'],
            ]
        */
        $header = $file[0];
        $newHeader = [];
        //find pos and set
        foreach( $header as $index => $name)
        {
            $newHeader[$obj[$index]] = $index;  
        }       
        unset($file[0]);
        $resultFile = [];
        foreach( $file as $record)
        {
            $pushObj = [];
            foreach( $newHeader as $key=>$index)
            {
                if(isset($record[$index]))
                {
                    $pushObj[$key]=
                        $trim?
                        trim($record[$index]):
                        $record[$index];
                }
                else
                {
                    $pushObj[$key] = '';
                }
            }
            if($pushObj)
            {
                $resultFile[] = $pushObj;
            }
        }
        
        return $resultFile;
    }
    
    /**
     * move tmp file (uploaded file) to new folder
     * @param  tmp object $file                      
     * @param  string [$folderName='upload_csv'] 
     * @return boolean
     */
    function backupTMP($file, $folderName='backup_uploaded_csv')
    {
        $path = ROOT.'/'.$folderName;
		$this->check_and_create_dir($path);
		$newDirectory = $path.'/'.$file['name'];
		if(file_exists($newDirectory))
		{
			$newDirectory = $path.'/'.$file['name'].date('_YmdHis');
		}
        $result = rename($file['tmp_name'], $newDirectory);
		return $result;
    }
	
	function backupTMPReport($content)
	{
		$folderName='backup_reported_csv';
		$path = ROOT.'/'.$folderName;
		$this->check_and_create_dir($path);
		$fileName = 'tmp'.date('_YmdHis').'.json';
		if($this->tmpFilename)
		{
			$fileName = $this->tmpFilename.'.json';
		}
		$fullPath = $path.'/'.$fileName;
		file_put_contents($fullPath, json_encode($content, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
		
		return $fileName;
	}
	
	function getTMPReport($fileName)
	{
		$fullPath = ROOT.'/backup_reported_csv/'.$fileName;
		if(file_exists($fullPath))
		{
			return file_get_contents($fullPath);
		}
		return "not found ".$fileName;
	}

    function microtime($second=0)
    {
        //Day:      86400
        //Month:    2592000
        //Year:     31536000
        return (microtime(true)+$second)*1000;
    }
	
	function isUTF8($path)
	{
		$encode = mb_detect_encoding(file_get_contents($path));
		if($encode == 'UTF-8')
		{
			return true;
		}
		return false;
	}
	
	function check_and_create_dir($path)
	{
		if (!file_exists($path)) {
			$result = mkdir($path, 0777, true);
		} else {
			$result = true;
		}
		return $result;
	}
	
	function makeHeaderPosition($obj, $header)
    {
        $newHeader = [];
		foreach ($header as $index => $name) {
			$name = trim($name);
			if (isset($obj[$name])) {
				$newHeader[$obj[$name]] = $index;
			}
		}
        return $newHeader;
    }
	
	public function convertToObjectByHeader($header, $record)
    {
        $pushObj = [];
		foreach ($header as $key => $index) {
			if (isset($record[$index])) {
				try{
					$pushObj[$key] = $record[$index];
				}catch (Exception $e){
					error_log($e->getMessage());
					throw $e;
				}
			} else {
				$pushObj[$key] = '';
			}
		}
        return $pushObj;
    }
}