<?php
App::uses('SearchRoute', 'Controller/Component');
class SearchComponent extends Component {
    
	public function __construct() {
        $this->Area = ClassRegistry::init('Area');
    }
	
	public function getParams($params)
	{
        //Page
        $page = isset($params['page'])?intval($params['page']):1;

		//save list key area to view
		if(isset($params['area']))
		{
			$params['list_key_area'] = $params['area'];
		}
		
		if($params)
		{
			$params = $this->convertParamsKeyToText($params);
            $collection = new ComponentCollection();
            $searchRouteComponent = new SearchRouteComponent($collection);
            $customPassedArgs = $searchRouteComponent->customPassedArgs($params);
            $searchRouteComponent->matchUrl($customPassedArgs);
            $params = $searchRouteComponent->getPars();
		}
		
		//Page
		$params['page'] = $page;
		
		//Em
		$params['em'] = isset($params['em']) ? $params['em'] : 0;

		return $params;
	}
	
	public function parseUrlAreaData($stringAreaKey)
	{
        $arrKeyArea = [];
        $listArea = [];
        $params = '';
        if($stringAreaKey)
        {
            $arrKeyArea = explode('-', $stringAreaKey);
            if(count($arrKeyArea) > 1)
            {
                $listArea = $this->Area->getAllArea();
            }
        }

        if($arrKeyArea)
        {
            $countArrayKeyArea = count($arrKeyArea);
            if($countArrayKeyArea == 1)
            {
                $searchAreaGroup = Configure::read('webconfig.search_area_group');
                $group = $searchAreaGroup[$arrKeyArea[0] - 1];
                $groupName = $group['area']['name'];
                $params .= $groupName;
            }
            else
            {
                if($listArea)
                {
                    for($i = 1; $i < count($arrKeyArea); $i++)
                    {
                        foreach ($listArea as $area) {
                            if($area['area']['id'] == $arrKeyArea[$i])
                            {
                                $params .= $area['area']['name'] . ',';
                                break;
                            }
                        }
                    }

                    $params = rtrim($params,",");
                }
            }
            
        }

        return $params;
	}

    public function parseUrlEmData($em)
    {
        $emData = Configure::read('webconfig.employment_data');

        return $emData[$em];
    }

    public function parseUrlHotKeyData($listHotKey)
    {
        $hotKeyData = Configure::read('webconfig.hotkey_data');
        $arrHotKey = [];
        foreach ($listHotKey as $key => $value) {
            array_push($arrHotKey, $hotKeyData[$value]);
        }

        

        return $arrHotKey;
    }


    public function convertParamsKeyToText($params)
    {
        if(!empty($params['area']))
        {
            $params['list_key_area'] = $params['area'];
            $paramArea = $this->parseUrlAreaData($params['area']);
            $params['area'] = $paramArea;
        }
        if(!empty($params['em']))
        {
            $paramEm = $this->parseUrlEmData($params['em']);
            $params['em'] = $paramEm;
        }

        if(!empty($params['hotkey']))
        {
            $paramHotKeyString = $this->parseUrlHotKeyData($params['hotkey']);
            $params['hotkey_string'] = $paramHotKeyString;
        }
        
        return $params;
    }
}
