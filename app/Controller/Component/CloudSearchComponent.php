<?php
/**
 * SearchedQuery model
 *
 * @category    Model
 */
use Aws\CloudSearchDomain\CloudSearchDomainClient;
use Aws\CloudSearch\CloudSearchClient;
App::uses('Component', 'Controller');
class CloudSearchComponent extends Component {
	
    protected $_config;
    protected $_client;
	
	private $_searchFields = ['title', 'workplace', 'company', 'desc'];

	private $_searchJobs = ['title', 'company'];

	private $_resultFields = ['title', 'company', 'pic_url', 'desc', 'workplace', 'salary','unique_id'];
	//private $_resultFields = ['url', 'unique_id'];
	private $_noImageUrl = '';
	private $_searchCondition = array();
	
	private $_customSize = 0;
	private $_start = 0;
	private $_size = 0;
	
	protected $_queryFunctions = array();
	

    public function __construct() {	
		
		//TODO: __construct()
		require $_SERVER['DOCUMENT_ROOT'].'/vendor/autoload.php';
		
        $this->_client = CloudSearchDomainClient::factory(array(
            'credentials' => array(
                'key'    => 'AKIAIXS4I4MLZSX7DJHQ',
                'secret' => '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',
            ),
            'region' => 'ap-northeast-1',
            'endpoint' => 'http://search-callnavi-db-zse5sce22zr7z4rfqgj4ezsgxu.ap-northeast-1.cloudsearch.amazonaws.com',
            'version' => '2013-01-01'
        ));
		
		
		$this->_queryFunctions = array(
			
			'area' => function($area, $chooseOnlyGroup = false)
			{
				$arrArea = explode(',', $area);

				if(!$chooseOnlyGroup)
				{
					if($arrArea)
					{
						$area = end($arrArea);
					}
					$query = " (phrase field=workplace '" . $area . "') ";
				}
				else
				{
					if($arrArea)
					{
						if(count($arrArea) == 1)
						{
							$query = " (phrase field=workplace '" . $arrArea[0] . "') ";
						}
						else //count($arrArea) > 1
						{
							$queryArea = '';
							foreach ($arrArea as $__area) {
								$queryArea .= "workplace:'" . $__area . "' ";
							}

							$query = "(or $queryArea)";
						}
					}
				}

				return $query;
			},
			
			'em' => function($em)
			{
				return " (phrase field=employment_type '" . $em . "') ";
			},
			
			'hotkey' => function($value)
			{
				return $this->_searchWithConvert($value, Configure::read('webconfig.hotkey_data'));
			},


			'keyword' => function($keyword)
			{
				$query = '';
				$dirtyKeys = ['）',')','）'];
				$replaceKeys = [' ',',','（','(','・'];
				$keywordString = str_replace($dirtyKeys,'', $keyword);
				$keywordString = str_replace($replaceKeys,',', $keywordString);
				$keywords = explode(',', $keywordString);
				$queryKeyword ='';
				$searchFields = $this->_searchFields;
				foreach( $keywords as $key)
				{
					$tmpQuery = '';
					if(!empty($key))
					{
						foreach ($searchFields as $target) 
						{
							//$queryKeyword .= $target . ":'" . $key . "' ";
							$tmpQuery .= " (phrase field=$target '" . $key . "') ";
						}

						if(!empty($tmpQuery))
						{
							$queryKeyword .= "(or $tmpQuery ) ";
						}
					}
				}
				if(!empty($queryKeyword))
				{
					$query .= "(and $queryKeyword )";
				}

				return $query;
			},


			'site_id' => function($site_id)
			{
				$query = "site_id:'" . site_id . "' ";
				return $query;
			}
		);
    }
	
	
	
	public function setPageByIndex($start, $size)
	{
		$this->_customSize = 1;
		$this->_start = $start;
		$this->_size = $size;
	}
	
	/*
		Find a offer by ID
	*/
	public function searchById($unique_id)
	{
		//TODO: searchById()
		
		$returnValue = array(
			'unique_id' => '',
			'site_id' 	=> 0,
			'url' 		=> '',
			'message' 		=> '',
		);
		try
		{
			$query = "unique_id:'$unique_id'";
			$conditions = [
                'query' 		=> $query,
                'queryParser' 	=> 'lucene',
				//'queryParser' 	=> 'structured',
				'size' 			=> 1,
				'start'			=> 0
            ];
			
			//TODO: :: Searching
			$results = $this->_client->search($conditions);
			
			//TODO: :: Get count number
        	$count = $results['hits']['found'];
			if($count >= 1)
			{
				$data = $results['hits']['hit'][0]['fields'];
				$returnValue['unique_id'] 	= isset($data['unique_id'][0])?$data['unique_id'][0]:'';
				$returnValue['site_id'] 	= isset($data['site_id'][0])?$data['site_id'][0]:0;
				$returnValue['url'] 		= isset($data['url'][0])?$data['url'][0]:'';
			}
			else
			{
				$returnValue['message'] = 'No data';
			}			
			return $returnValue;
		}
		catch(Exception $e)
		{
			$returnValue['message'] = $e->getMessage();
			return $returnValue;
		}
	}
	
	public function getSearchCondion()
	{
		return $this->_searchCondition;
	}
	
	public function getQuery($data)
	{
		//pr($data);die;
		$chooseOnlyGroup = false;

		//check choose only group
		if(empty($data['area']) && !empty($data['list_key_area']))
		{
			$chooseOnlyGroup = true;
			$area = ClassRegistry::init('Area');
			$provinces = $area->getProvinceByGroup($data['list_key_area']);
			
			foreach ($provinces as $province) {
				$data['area'] .= $province['area']['name'] . ',';
			}
			$data['area'] = rtrim($data['area'], ',');
		}

		$query = '';
		foreach( $data as $key=>$value)
		{
			if(!empty($value) && 
			   isset($this->_queryFunctions[$key]) && 
			   is_callable($this->_queryFunctions[$key]))
			{
				if($chooseOnlyGroup)
				{
					$query .= $this->_queryFunctions[$key]($value, $chooseOnlyGroup);
				}
				else
				{
					$query .= $this->_queryFunctions[$key]($value);
				}
				
			}
		}
		return $query;
	}
	
	public function getCount($conditions)
	{	
		/*
			Return data
			------------------------------------------------------------------
			When $size bigger zero, it will active
		*/
		//TODO: :: case: return data
		try
		{
			$conditions['size'] = 0;
			$conditions['start'] = 1;
			$resultsOfCount = $this->_client->search($conditions);
	
			$countOnly = $resultsOfCount['hits']['found'];
			if($countOnly < 1)
			{
				return array(array(), 0);
			}
			else
			{
				return array(array(), $countOnly);
			}

		}
		catch(Exception $e)
		{
			//Get error and return no data
			return array([],0);
		}
	}
	
	public function getCustomPage($conditions)
	{
		$conditions['size']  = $this->_size;
		$conditions['start'] = $this->_start;
		return $conditions;
	}
	
	public function getPageCondition($conditions, $page, $size, $maxPage)
	{
		//Set Order by and Pagination
        //$conditions['sort'] = '_rand desc';
        //$conditions['sort'] = 'title desc';
        //$conditions['sort'] = '_rand desc, updatetime asc';
		/*
			
			Solution for limit 10000 of Amazon API
			Exp:
			SIZE = 30
			MaxPage = round(10000/30) => 333
			
			Under 334 -> sort by _rand desc 
		*/
		//TODO: :::: Pagination		
		if($page > $maxPage)
		{
			$conditions['sort'] = '_rand asc';
		}
		else
		{
			$conditions['sort'] = '_rand desc';
			
			$maxPage = 0;
		}
		
        $conditions['size'] = $size;
		$conditions['start'] = ($page-1 - $maxPage) * $size;
		
		return $conditions;
	}
	
	public function getCondition($query)
	{
		//The lastest proccess $query
        if ($query != '') {
            $query = '(and ' . $query . ')';
            $conditions = [
                'query' => $query,
                'queryParser' => 'structured',
            ];
        } else {
            $conditions = [
                'query' => '*:*',
                'queryParser' => 'lucene',
            ];
        }
		return $conditions;
	}
	
	public function countOnly($data)
	{
		$query = $this->getQuery($data);
		$conditions = $this->getCondition($query);
		return $this->getCount($conditions)[1];
	}
	
     /**
     * ユーザページの広告検索フォームにおける検索結果を取得
     * @parm array data フォームに入力された、勤務地及びキーワードを格納した配列
     * @parm int $page どの検索結果ページに表示される検索結果かを指定(1ページ目が0)
     * @parm int $size 検索結果ページ1ページに表示される広告件数を指定
     * @return array 該当ページ検索結果の配列、及び全検索件数　　　　　　　　　　　　　　　　　　　　　　　
     */
    public function search($data, $page = 0, $size = 20) {

		//TODO: search()
        $query = $this->getQuery($data);

		
		$conditions = $this->getCondition($query);
		if($size <= 0)
		{
			$this->_searchCondition = $conditions;
			return $this->getCount($conditions);
		}
	
		$maxPage = round(10000/$size);
		$conditions = $this->getPageCondition($conditions, $page, $size, $maxPage);
		
		if($this->_customSize)
		{
			$conditions = $this->getCustomPage($conditions);
		}
		
		//Set this conditions for Global Variable to GET it anytime
		$this->_searchCondition = $conditions;
		
		//pr($conditions);die;
		
		if (isset($_GET['query_log']) and ! empty($_GET['query_log'])) {
			print_r($data);
			print_r($conditions);
		}

		try
		{
			//TODO: :::: Searching
			$results = $this->_client->search($conditions);
		}
		catch(Exception $e)
		{
			//Get error and return no data
			return array([],0);
		}
		
		//Get count number
        $count = $results['hits']['found'];
		
		//TODO: :::: Returl value
		
		//Return empty if $count == 0
		if($count < 1)
		{
			return array(array(), 0);
		}
		$resultObjects = [];
		
		//Check lastpage and large maxsize
		$restOfPage = 0;
		$lastPage = ceil($count/$size);
		if($page >= $lastPage && $page > $maxPage)
		{
			$restOfPage = $count%$size;
		}
		
		if($restOfPage != 0)
		{
			//Select fields and process them before return
			$startRestOfPage = 1;
			foreach ($results['hits']['hit'] as $hit) {
				
				//Just return enough data
				if($startRestOfPage > $restOfPage)
				{
					break;
				}
				$startRestOfPage ++;
				$eachResultObject = [];
				foreach ($hit['fields'] as $key => $valueArray) {
					$eachResultObject[$key] = $valueArray[0];
				}
				$resultObjects[] = $this->getResultInfo($eachResultObject);
			}
		}
		else
		{
			//Select fields and process them before return
			foreach ($results['hits']['hit'] as $hit) {
				$eachResultObject = [];
				foreach ($hit['fields'] as $key => $valueArray) {
					$eachResultObject[$key] = $valueArray[0];
				}
				$resultObjects[] = $this->getResultInfo($eachResultObject);
			}
		}
		
		//Return 1:DATA and 2:COUNT
        return array($resultObjects, $count);
    }


    public function replaceSpecialKey($keywordString)
    {
		//Special keywords
    	$replaceSpecialKeys = [' ', '（', '）', '　', '(', ')', '／','・'];
    	$keywordString = str_replace($replaceSpecialKeys, ' ', $keywordString);
		
		//Common keywords
    	$replaceKeys = [
						'コール' ,
						'センター' ,
						'スタッフ' ,
						'会社' ,
						'株式' ,
						'有限' ,
						'本社' ,
						'株',
						'支社',
						'大阪',
						'福岡',
						'高田馬場',
						'札幌',
						'ヒューマン',
						'日本郵便',
						'郵便事業総本部',
						'郵便局',
						'オフィス',
						'北九州',
						'コミュニケーションズ',
					   ];
    	$keywordString = str_replace($replaceKeys, ' ', $keywordString);

    	//$keywordString = trim($keywordString);
    	$keywordString = preg_replace('/^[\pZ\pC]+|[\pZ\pC]+$/u','',$keywordString);
		$keywords = explode(' ', $keywordString);

    	return $keywords;
    	
    }	

    public function search_job($data, $page = 0, $size = 20) {

        $query = '';
		$searchFields = $this->_searchJobs;
		
		if (isset($data['keyword']) and ! empty($data['keyword'])) {
			$keywords = $this->replaceSpecialKey($data['keyword']);
			$queryKeyword ='';
            foreach ($searchFields as $target) {
				foreach( $keywords as $key)
				{
					if(!empty($key))
					$queryKeyword .= $target . ":'" . $key . "' ";
				}
            }
			
			if(!empty($queryKeyword))
			{
				$query .= "(or $queryKeyword )";
			}

        }
		
        if ($query != '') {
            $query = '(and ' . $query . ')';
            $conditions = [
                'query' => $query,
                'queryParser' => 'structured',
            ];
        } else {
			//Return empty result
            return array([],0);
        }

        $conditions['sort'] = '_rand desc';
        $conditions['size'] = $size;
        $conditions['start'] = ($page-1) * $size;
		
		$this->_searchCondition = $conditions;

		try
		{
			$results = $this->_client->search($conditions);
		}
		catch(Exception $e)
		{
			return array([],0);
		}
		
        $count = $results['hits']['found'];
				
		$resultObjects = [];
        foreach ($results['hits']['hit'] as $hit) {
			$eachResultObject = [];
            foreach ($hit['fields'] as $key => $valueArray) {
                $eachResultObject[$key] = $valueArray[0];
            }
            $resultObjects[] = $this->getResultInfo($eachResultObject);
        }
        return array($resultObjects, $count);
    }



      
     /**
     * 検索結果を表示用に加工
     * @parm array $result 加工前の検索結果
     * @return array 加工後の検索結果　　　　　　　　　　　　　　　　　　　　　　
     */
    public function getResultInfo($result) {
        if(isset($result['labels'])){
            $result['labels'] = explode(':', $result['labels']);
			
			//Translate labels ENGLISH to JAPANESE
			$labels = Configure::read('webconfig.translated_labels');
			foreach( $result['labels'] as $i=>$val)
			{
				if(isset($labels[$val]))
				{
					$result['labels'][$i] = $labels[$val];
				}
			}
        }else{
            $result['labels'] = [];
        }

        foreach($result['labels'] as $key => $value){
            if($key >= 5){
                unset($result['labels'][$key]);
            }
        }
        $result['update_before'] = $this->_convertDateTimeToMinuteBefore(intval($result['updatetime']));

        foreach ($this->_resultFields as $target) {
			if(isset($result[$target])){
            	$result[$target] = mb_substr(strip_tags($result[$target]), 0, 150, 'UTF8');
            	$result[$target] = str_replace(["\r\n", "\r", "\n", " ", "　"], '', $result[$target]);
			}
			else
			{
				$result[$target] = '';
			}
        }

        $result['site_name'] = $this->_getSiteName($result['site_id']);

        if (!isset($result['pic_url']) or $result['pic_url'] == '') {
            $result['pic_url'] = $this->_noImageUrl;
        }

        return $result;
    }
	
      /**
     * site_idから広告媒体名を取得
     * @parm int $site_id 広告媒体を表すID
     * @return string 広告媒体名　　　　　　　　　　　　　　　　　　　　　
     */
    protected function _getSiteName($site_id) {
        if ($site_id == 1) {
            return 'マイナビバイト';
        } elseif ($site_id == 2) {
            return 'バイトル';
        } elseif ($site_id == 3) {
            return 'デューダ';
        } elseif ($site_id == 4) {
            return 'Eアイデム';
        } elseif ($site_id == 5) {
            return 'フロムエー';
        } elseif ($site_id == 6) {
            return 'ジョブセンス';
        } elseif ($site_id == 7) {
            return 'マイナビ転職';
            //20150320ここまで実装
        } elseif ($site_id == 8) {
            return 'an';
        } elseif ($site_id == 9) {
            return 'リクナビNEXT';
        } elseif ($site_id == 10) {
            return 'タウンワーク';
        } elseif ($site_id == 11) {
           return '無料広告';
        }
        
    }
     /**
     * 該当媒体についてのcloudsearch上の更新処理
     * @parm int $site_id 広告媒体を表す
     * @return void　　　　　　　　　　　　　　　　　　　　
     */
    public function refresh($siteId) {

        $this->_deletePreviousOffers($siteId);
        sleep(3);
        $this->_addOffers($siteId);
    }

    public function allCount() {
        $conditions = [
                'query' => '*:*',
                'queryParser' => 'lucene',
            ];
        $conditions['sort'] = '_rand desc';
        $conditions['size'] = 1;
        $conditions['start'] = 0;
        $results = $this->_client->search($conditions);
        $count = $results['hits']['found'];
        return $count;
    }


    public function _convertDateTimeToMinuteBefore($datetime) {
        if (time() - $datetime <= 3600) {
            return floor((time() - $datetime) / 60) . '分前';
        } elseif (time() - $datetime <= 24 * 60 * 60) {
            return floor((time() - $datetime) / (60 * 60)) . '時間前';
        } elseif (time() - $datetime <= 31 * 24 * 60 * 60) {
            return floor((time() - $datetime) / (24 * 60 * 60)) . '日前';
        } elseif (time() - $datetime <= 12 * 31 * 24 * 60 * 60) {
            return floor((time() - $datetime) / (31 * 24 * 60 * 60)) . 'ヶ月前';
        } else {
            return floor((time() - $datetime) / (12 * 31 * 24 * 60 * 60)) . '年前';
        }
    }
	
	//
	protected function _searchWithConvert($obj, $convert, $condition='or')
	{
		$searchFields = $this->_searchFields;
		
		$query = "";
		
			if(is_array($obj))
			{
				foreach ($searchFields as $target) {
					foreach ($obj as $key) {
						if(isset($convert[$key]))
						{
							$query .= $target . ":'" . $convert[$key] . "' ";
						}
					}
            	}
			}
			else
			{
				foreach ($searchFields as $target) {
					if(isset($convert[$obj]))
					{
						$query .= $target . ":'" . $convert[$obj] . "' ";
					}
            	}
			}
	   
	   if(!empty($query))
	   {
		   $query = "($condition $query)";
	   }

	   return $query;
	}

}
