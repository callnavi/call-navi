<?php
/**
 * SearchedQuery model
 *
 * @category    Model
 */
use Aws\CloudSearchDomain\CloudSearchDomainClient;
use Aws\CloudSearch\CloudSearchClient;
App::uses('Component', 'Controller');
App::uses('Helper', 'View');
App::uses('UtilComponent', 'Controller/Component');
App::uses('MixSearchComponent', 'Controller/Component');
App::uses('SessionHelper', 'View/Helper');


class ContactFunComponent extends Component {

    public $uses = array('Contact');
        public $components = array('Session', 'Cookie', 'Util','Sendmail');

    public function GetIndexSessionData(){
        if($this->Session->check('contact_data'))
        {
            $post = $this->Session->read('contact_data');
        }else{
            $post['company'] = '';
            $post['name'] = '';            
            $post['mail'] = '';            
            $post['address'] = '';            
            $post['phone'] = '';            
            $post['inquiry'] = '';            
            $post['remarks'] = ''; 
            $post['agreement'] = '0';           
        }
        return $post;
    }
    public function CheckValidation($data){
        $error = "";

        return $error;
    }
    public function sent_mail_main($data){
        $arr = $this->setData($data);

        //load contact configure
        Configure::load('contact', 'default');


        //set sent file
        $temp_admin = 'text/contact_v2_admin';
        $temp_user = 'text/contact_v2';

        //set name sent 
        $name_admin =  Configure::read('contact.admin_name');
        $name_user = $data['name'];        

        //set name sent 
        $mail_admin =  Configure::read('webconfig.mail_from_concierge');
        $mail_user = $data['mail'];        

        //set name sent 
        $subject_admin =  Configure::read('contact.subject_to_admin');
        $subject_user = Configure::read('contact.subject_to_user');        
        
        //Default find the ctp file in View/Emails
        $this->Sendmail->render($temp_user,$arr);

        //Send from Address and from Name
        $this->Sendmail->fromlName = Configure::read('webconfig.mail_from_name');
        $this->Sendmail->fromlAdress = Configure::read('webconfig.mail_from');

        //Send: to Email, to Name, subject
        $result = $this->Sendmail->send($mail_user, $name_user, $subject_user);

        //send mail to admin
        //Default find the ctp file in View/Emails
        $this->Sendmail->render($temp_admin,$arr);

       
        //Send: to Email, to Name, subject
        $resultAdmin = $this->Sendmail->send($mail_admin, $name_admin, $subject_admin);
        //$resultAdmin = $this->Sendmail->send('kyujin@callnavi.jp','管理者', '【コールナビ】お祝い金の申請がありました。');


        //If error will be appear in this function
        //pr($this->Sendmail->error());
        //
        //die;
        return true;
    }
    
    public function setData($data){
        $arr = array(
            'arr' => array(
                'company' => $data['company'],
                'name' => $data['name'],
                'mail' => $data['mail'],
                'address' => $data['address'],
                'phone' => $data['phone'],
                'inquiry' => $data['inquiry'],
                'remarks' => $data['remarks'],
                'agreement' => $data['agreement']
                               
            )
        );
         return $arr;
    }
    public function sent_mail_testmode($data){


        $arr = $this->setData($data);

        //load contact configure
        Configure::load('contact', 'default');


        //set sent file
        $temp_admin = 'text/contact_v2_admin';
        $temp_user = 'text/contact_v2';

        //set name sent 
        $name_admin =  Configure::read('contact.admin_name');
        $name_user = $data['name'];        

        //set name sent 
        $mail_admin =  'viinnoreply@gmail.com';
        $mail_user = $data['mail'];        

        //set name sent 
        $subject_admin =  Configure::read('contact.subject_to_admin');
        $subject_user = Configure::read('contact.subject_to_user');        
        
        //Default find the ctp file in View/Emails
        $this->Sendmail->render($temp_user,$arr);

        //Send from Address and from Name
        $this->Sendmail->fromlName = Configure::read('webconfig.mail_from_name');
        $this->Sendmail->fromlAdress = Configure::read('webconfig.mail_from');

        //Send: to Email, to Name, subject
        $result = $this->Sendmail->send($mail_user, $name_user, $subject_user);

        //send mail to admin
        //Default find the ctp file in View/Emails
        $this->Sendmail->render($temp_admin,$arr);

       
        //Send: to Email, to Name, subject
        $resultAdmin = $this->Sendmail->send($mail_admin, $name_admin, $subject_admin);
        //$resultAdmin = $this->Sendmail->send('kyujin@callnavi.jp','管理者', '【コールナビ】お祝い金の申請がありました。');


        //If error will be appear in this function
        //pr($this->Sendmail->error());
        //
        //die;
        return true;
    }
    
}