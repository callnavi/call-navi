<?php

App::uses('Component', 'Controller');

class NewFormatIdComponent extends Component {
	
	public function __construct() {
        $this->CorporatePrs = ClassRegistry::init('CorporatePrs');
        $this->CareerOpportunity = ClassRegistry::init('CareerOpportunity');
        $this->Entry = ClassRegistry::init('Entry');
    }


    public function index()
    {
    	$company = $this->CorporatePrs->find('count', array(
	        'conditions' => array(
	        	'OR' => array(
		            array('CorporatePrs.corporate_code' => null),
		            array('CorporatePrs.corporate_code' => ''),
		        )
	        )
	    ));

	    if($company)
	    {
	    	$this->companyId();
	    }


        $secret = $this->CareerOpportunity->find('count', array(
	        'conditions' => array(
	        	'OR' => array(
		            array('CareerOpportunity.job_code' => null),
		            array('CareerOpportunity.job_code' => ''),
		        )
	        )
	    ));

	    if($secret)
	    {
	    	$this->secretId();
	    }

	    // $entry = $this->CareerOpportunity->find('count', array(
	    //     'conditions' => array(
	    //     	'OR' => array(
		   //          array('CareerOpportunity.job_code' => null),
		   //          array('CareerOpportunity.job_code' => ''),
		   //      )
	    //     )
	    // ));
                            
     //    if($entry) $this->entryId();
    }


    public function companyId()
    {
        $list = $this->CorporatePrs->find('all', array(
            'order' => array('created' => 'desc')
        ));


        foreach ($list as $key => $value) {
        	$id = $value['CorporatePrs']['id'];

            $data = array('id' => $id, 'corporate_code' => $this->idFormat($id,'C'));

			$this->CorporatePrs->save($data);
        }
	}


	public function secretId()
	{
        $list = $this->CareerOpportunity->find('all', array(
            'order' => array('created' => 'desc')
        ));

        foreach ($list as $key => $value) {
        	$id = $value['CareerOpportunity']['id'];

            $data = array('id' => $id, 'job_code' => $this->idFormat($id, 'J', 4));

			$this->CareerOpportunity->save($data);
        }
	}


	private static function idFormat($id,$prefix,$length = 4)
	{
        $out_id = '';

        if(strlen($id) > 4){
            $length = strlen($id);
        }

        $out_id = sprintf("{$prefix}%0{$length}d",$id);

        return $out_id;
    }
}
