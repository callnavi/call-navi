<?php
	$translated_labels = [
		'choose_shift' 	=> '曜日・時間を選べる',
		'traffic' 		=> '交通費支給',
		'skill' 		=> 'スキルが身に付く',
		'close_man' 	=> '人と接する',
		'unexperienced' => '未経験者',
		'permanent' 	=> '正社員登用あり',
		'short_work' 	=> '短期・単発OK',
		'day_salary' 	=> '日払いOK',
		'color_pierced' => '茶髪・ピアスOK',
		'free_style' 	=> '服装自由',
		'highschool' 	=> '高校生OK',
		'smoking_time' 	=> 'タバコ休憩あり',
		'uniform' 		=> '制服あり',
		'no_age' 		=> '年齢不問',
		'contract' 		=> '契約社員登録あり',
		'no_smoking' 	=> '職場禁煙',
		'day_shift' 	=> '週1日からOK',
		'week_salary' 	=> '週払いOK',
		'use_language' 	=> '語学力を生かせる',
		'car' 			=> '車通勤OK',
		'bike' 			=> 'バイク通勤OK',
		'active' 		=> '体を動かす',
		'register' 		=> '登録制',
		'meal' 			=> '食事・まかない付き',
	];

    $area_data = [
        1 => 	"北海道",
		2 => 	"青森県",
		3 => 	"岩手県",
		4 => 	"宮城県",
		5 => 	"秋田県",
		6 => 	"山形県",
		7 => 	"福島県",
		8 => 	"茨城県",
		9 => 	"栃木県",
		10 => 	"群馬県",
		11 => 	"埼玉県",
		12 => 	"千葉県",
		13 => 	"東京都",
		14 => 	"神奈川県",
		15 => 	"新潟県",
		16 => 	"富山県",
		17 => 	"石川県",
		18 => 	"福井県",
		19 => 	"山梨県",
		20 => 	"長野県",
		21 => 	"岐阜県",
		22 => 	"静岡県",
		23 => 	"愛知県",
		24 => 	"三重県",
		25 => 	"滋賀県",
		26 => 	"京都府",
		27 => 	"大阪府",
		28 => 	"兵庫県",
		29 => 	"奈良県",
		30 => 	"和歌山県",
		31 => 	"鳥取県",
		32 => 	"島根県",
		33 => 	"岡山県",
		34 => 	"広島県",
		35 => 	"山口県",
		36 => 	"徳島県",
		37 => 	"香川県",
		38 => 	"愛媛県",
		39 => 	"高知県",
		40 => 	"福岡県",
		41 => 	"佐賀県",
		42 => 	"長崎県",
		43 => 	"熊本県",
		44 => 	"大分県",
		45 => 	"宮崎県",
		46 => 	"鹿児島県",
		47 => 	"沖縄県",
        99 =>  	"海外",
    ];


	$employment_data = [
		1 => '正社員',
		2 => 'アルバイト',
		3 => 'パート',
		4 => '契約社員',
		5 => '派遣',
		6 => '業務委託',
	];
	
	$hotkey_data = [
		1 => '服装髪型自由',
		2 => 'フリーター歓迎',
		3 => '即日勤務OK',
		4 => '駅近',
		5 => '日払い',
		11 => '主婦歓迎',
		6 => '転勤なし',
		7 => '短期',
		8 => '登録制',
		9 => '入社祝い金',
		10 =>'交通費支給',
	]; 

	$hotkey_data_tmp = [
		1 => ['name' => '服装髪型自由', 'result' => '354'],
		2 => ['name' => 'フリーター歓迎', 'result' => '2485'],
		3 => ['name' => '即日勤務OK', 'result' => '1478'],
		4 => ['name' => '駅近', 'result' => '9514'],
		5 => ['name' => '日払い', 'result' => '523'],
		6 => ['name' => '転勤なし', 'result' => '2546'],
		7 => ['name' => '短期', 'result' => '321'],
		8 => ['name' => '登録制', 'result' => '961'],
		9 => ['name' => '入社祝い金', 'result' => '20'],
		10 =>['name' => '交通費支給', 'result' => '15482'],
	];

    $google_map_API_key = 'AIzaSyDHZAJtARj1Qr-RbMWtiqApSW7HheKf7qs';


	$googleAnalytics ="<script>
	   (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	   (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	   m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	   })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

	   ga('create', 'UA-60449449-1', 'auto');
	   ga('send', 'pageview');

	 </script>";

 //facebook pixel header code
	$facebookPixel ="<script>
        !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '457644201279164'); // Insert your pixel ID here.
        fbq('track', 'PageView');
        </script>
        <noscript><img height='1' width='1' style='display:none'
        src='https://www.facebook.com/tr?id=457644201279164&ev=PageView&noscript=1'
        /></noscript>";
    // meta keyewords desciption and image
	$http = isset($_SERVER["HTTPS"])? 'https://' : 'http://';
	$meta_description = !empty(Cache::read('meta_desciption')) ? "<meta name='description' content= '".Cache::read('meta_desciption')."' >"
		: "<meta name='description' content= 'コールセンター求人掲載数No.1クラス「コールナビ」。在宅ワーク、最新の求人情報やテレマのコツ・面接のコツ・給与や働く人の声などのお役立ち記事が満載のコールセンター特化型求人サイトです。コールセンターで働くあなたも働いていないあなたも、コールセンター特化型求人サイト「コールナビ」で毎日がちょっと楽しくなる！' >";
    
    $meta_keywords = !empty(Cache::read('meta_keywords')) ? "<meta name='Keywords' content='".Cache::read('meta_keywords')."'>"
		: "<meta name='Keywords' content='コールセンター 求人,コールセンター 在宅,コールセンター バイト,テレアポ 募集,アルバイト 高時給,オペレーター,アウトバウンド'>";
    
    $apple_touch_icon_precomposed = !empty(Cache::read('apple_touch_img')) ? "<link rel='apple-touch-icon' href= '".$http.$_SERVER['SERVER_NAME']."/upload/meta/".Cache::read('apple_touch_img')."'>"
		: "<link rel='apple-touch-icon' href= '".$http.$_SERVER['SERVER_NAME']."/upload/meta/apple-touch-icon-precomposed.png'>" ;


    $ogp = !empty(Cache::read('ogp_img')) ? "<meta property='og:image' content='".$http.$_SERVER['SERVER_NAME']."/upload/meta/".Cache::read('ogp_img')."'>"
		: "<meta property='og:image' content='".$http.$_SERVER['SERVER_NAME']."/upload/meta/ogp.png'>";

	

	$callcenter_area_group = [
		1 => '北海道 ／ 東北',
		2 => '関東',
		3 => '甲信越 ／ 北陸',
		4 => '東海',
		5 => '関西',
		6 => '中国 ／ 四国',
		7 => '九州 ／ 沖縄',
	];

	$ogp_description = !empty(Cache::read('meta_desciption')) ? Cache::read('meta_desciption') : 'コールセンター求人掲載数No.1クラス「コールナビ」。在宅ワーク、最新の求人情報やテレマのコツ・面接のコツ・給与や働く人の声などのお役立ち記事が満載のコールセンター特化型求人サイトです。コールセンターで働くあなたも働いていないあなたも、コールセンター特化型求人サイト「コールナビ」で毎日がちょっと楽しくなる！';
	$ogp_keywords = !empty(Cache::read('meta_keywords')) ? Cache::read('meta_keywords') 
		: 'コールセンター 求人,コールセンター 在宅,コールセンター バイト,テレアポ 募集,アルバイト 高時給,オペレーター,アウトバウンド';
	$ogp_image = !empty(Cache::read('ogp_img')) ? $http.$_SERVER['SERVER_NAME']."/upload/meta/".Cache::read('ogp_img')
		: $http.$_SERVER['SERVER_NAME']."/upload/meta/ogp.png";
	$ogp_apple_touch_icon = !empty(Cache::read('apple_touch_img')) ? $http.$_SERVER['SERVER_NAME']."/upload/meta/".Cache::read('apple_touch_img')
		: $http.$_SERVER['SERVER_NAME']."/upload/meta/apple-touch-icon-precomposed.png" ;

	$mail_from = 'no-reply@callnavi.jp';
	$mail_from_concierge = 'kyujin@callnavi.jp';
	$mail_from_name = 'Callnavi窓口';

	$search_area_group = [
		0 => array(
				'area' => array(
						'id' 	=> 1,
						'range' 	=> [1,7],
						'name' 	=> '北海道・東北'
					)
			),
		1 => array(
				'area' => array(
						'id' 	=> 2,
						'range' 	=> [8,14],
						'name' 	=> '関東'
					)
			),
		2 => array(
				'area' => array(
						'id' 	=> 3,
						'range' 	=> [15,20],
						'name' 	=> '北陸・甲信越'
					)
			),
		3 => array(
				'area' => array(
						'id' 	=> 4,
						'range' 	=> [21,24],
						'name' 	=> '東海'
					)
			),
		4 => array(
				'area' => array(
						'id' 	=> 5,
						'range' 	=> [25,30],
						'name' 	=> '関西'
					)
			),
		5 => array(
				'area' => array(
						'id' 	=> 6,
						'range' 	=> [31,35],
						'name' 	=> '中国'
					)
			),
		6 => array(
				'area' => array(
						'id' 	=> 7,
						'range' 	=> [36,39],
						'name' 	=> '四国'
					)
			),
		7 => array(
				'area' => array(
						'id' 	=> 8,
						'range' 	=> [40,47],
						'name' 	=> '九州・沖縄'
					)
			),
	];
	
	//$web_title_default = 'コールセンターの求人・ノウハウ｜コールナビ';
	$web_title_default = 'コールセンター求人掲載数No.1クラス｜コールセンター特化型求人情報サイト【コールナビ】';
	
	$web_title = array(
		'default' 			=> 'コールセンター求人掲載数No.1クラス｜コールセンター特化型求人情報サイト【コールナビ】',
		'search' 			=> 'コールセンター求人検索結果 ｜ コールセンター特化型求人サイト「コールナビ」',
		'result' 			=> 'コールセンター求人検索結果  ｜ コールセンター特化型求人サイト「コールナビ」',
		'search_result' 	=> '「title」のコールセンター求人検索結果  ｜ コールセンター特化型求人サイト「コールナビ」',
		'contents' 			=> 'コールセンターのさまざまな情報を配信 ｜ コールセンター特化型求人サイト「コールナビ」',
		'contents_category' => '「title」の情報 ｜ コールセンター特化型求人サイト「コールナビ」',
		'contents_detail' 	=> '「title」｜ コールセンター特化型求人サイト「コールナビ」',
		'secret' 			=> 'コールセンター オリジナル求人まとめ ｜ コールセンター特化型求人サイト「コールナビ」',
		'secret_detail' 	=> '「title」のオリジナル求人情報 コールセンター特化型求人サイト「コールナビ」',
		'concept' 			=> 'コールナビとは？ ｜ コールセンター特化型求人サイト「コールナビ」',
		'about' 			=> 'コールナビとは？ ｜ コールセンター特化型求人サイト「コールナビ」',
		'contact' 			=> 'お問い合わせ｜ コールセンター特化型求人サイト「コールナビ」',
		'contact_thanks' 	=> 'お問い合わせ（完了ページ） ｜ コールセンター特化型求人サイト「コールナビ」',
		'company' 			=> '運営会社・プライバシポリシー｜ コールセンター特化型求人サイト「コールナビ」',
		'callcenter_matome' => '日本コールセンターまとめ ｜ コールセンター特化型求人サイト「コールナビ」',
		'pretty' 			=> '美男美女図鑑 ｜ コールセンター特化型求人サイト「コールナビ」',
		'concierge' 			=> 'コールセンター・求人コンサルタント ｜ コールセンター特化型求人サイト「コールナビ」',
	);


	$sitemap = [
		1 => '/',
		2 => '/contents',
		3 => '/contents/働く人の声',
		4 => '/contents/スキル・テクニック',
		5 => '/contents/ビジネス・キャリア',
		6 => '/contents/採用・面接',
		7 => '/contents/注目トピックス',
		8 => '/contents/エリア特集',
		9 => '/callcenter_matome',
		10 => '/company',
		11 => '/about',
		12 => '/contact',
		13 => '/blog',
		14 => '/blog/jinji',
		15 => '/blog/edit',
		16 => '/blog/supervisor',
		17 => '/blog/kikikanri',
		18 => '/blog/trainer',
		19 => '/blog/se',
		20 => '/blog/shimizu',
		21 => '/blog/human-resources',	
	];


	$secret_telephone = "0800-111-0641";


	$special_feature = [
		1 => array(
				'url' => '/contents/%E3%81%AA%E3%81%9C%E3%82%B3%E3%83%BC%E3%83%AB%E3%82%B7%E3%82%A7%E3%82%A2%E3%82%92%E5%A7%8B%E3%82%81%E3%81%9F%E3%81%AE%E3%81%8B%EF%BC%9F%E5%9C%A8%E5%AE%85%E3%83%86%E3%83%AC%E3%83%AF%E3%83%BC%E3%82%AF%E3%81%B8%E3%81%AE%E6%83%B3%E3%81%84%E3%82%92%E4%BA%8B%E6%A5%AD%E8%B2%AC%E4%BB%BB%E8%80%85%E3%81%8C%E8%AA%9E%E3%82%8B',
				'img' => 'callshare_contents_bnr.png'
			),
		2 => array(
				'url' => '/contents/コールセンターの雇用形態',
				'img' => 'employment_type.png'
			),
		3 => array(
				'url' => '/contents/コールセンターの専門用語！！',
				'img' => 'technicalwords.png'
			),
		4 => array(
				'url' => '/contents/ライフスタイルに合った働き方を見つけよう！',
				'img' => 'lifestyle.png'
			),
		5 => array(
				'url' => '/contents/%22%E3%82%B9%E3%83%88%E3%83%AC%E3%82%B9%E3%81%AB%E8%B2%A0%E3%81%91%E3%81%AA%E3%81%84%2521%22%E3%82%B3%E3%83%BC%E3%83%AB%E3%82%BB%E3%83%B3%E3%82%BF%E3%83%BC%E3%83%90%E3%82%A4%E3%83%88',
				'img' => 'stress.png'
			),
		6 => array(
				'url' => '/contents/saiyou/pre_interview',
				'img' => 'pre_interview.png'
			),
		7 => array(
				'url' => '/contents/【これだけで辞められる！】バイト辞めるときの伝え方3つのポイント',
				'img' => 'quitjob.png'
			),
		8 => array(
				'url' => '/contents/アイデアオフィスで疲れを吹き飛ばそう！',
				'img' => 'ideaoffice.png'
			),
	];
/*
	----------------------------------------------------------
	INSERT DATA TO CONFIG
*/
$config['webconfig']['area_data'] 		= $area_data;
$config['webconfig']['employment_data'] = $employment_data;
$config['webconfig']['hotkey_data'] = $hotkey_data;
$config['webconfig']['hotkey_data_tmp'] = $hotkey_data_tmp;
$config['webconfig']['google_map_api_key'] = $google_map_API_key;
$config['webconfig']['translated_labels'] = $translated_labels;
$config['webconfig']['google_analytics'] =$googleAnalytics ;
$config['webconfig']['facebook_pixel'] = $facebookPixel ;

$config['webconfig']['email_host'] = 'ssl://smtp.gmail.com';
$config['webconfig']['email_port'] = 465;
$config['webconfig']['email_SMTPAuth'] = true;
$config['webconfig']['email_username'] = 'info@jellyfishhr.com';
$config['webconfig']['email_password'] = 'ABCD@123.com';

//set param meta 
$config['webconfig']['meta_description'] = $meta_description;
$config['webconfig']['meta_keywords'] = $meta_keywords;
$config['webconfig']['apple_touch_icon_precomposed'] = $apple_touch_icon_precomposed;
$config['webconfig']['ogp'] = $ogp;

//set title
$config['webconfig']['web_title_default'] = $web_title_default;
$config['webconfig']['web_title'] = $web_title;

//set group name in callcenter
$config['webconfig']['callcenter_area_group'] = $callcenter_area_group;

//set meta
$config['webconfig']['ogp_description'] = $ogp_description;
$config['webconfig']['ogp_keywords'] = $ogp_keywords;
$config['webconfig']['ogp_image'] = $ogp_image;
$config['webconfig']['ogp_apple_touch_icon'] = $ogp_apple_touch_icon;

//set e-mail contact
$config['webconfig']['mail_from'] = $mail_from;
$config['webconfig']['mail_from_name'] = $mail_from_name;
$config['webconfig']['mail_from_concierge'] = $mail_from_concierge;

//set group for new search
$config['webconfig']['search_area_group'] = $search_area_group;

//set telephone for secret page
$config['webconfig']['secret_telephone'] = $secret_telephone;

//set special_feature in top page
$config['webconfig']['special_feature'] = $special_feature;
