<?php
$keisaiguide_content = 'コールセンター・テレオペの求人広告を掲載するならコールセンター特化型求人サイト「コールナビ」。完全成果報酬型なので、初期費用は０円で掲載可能！正社員・派遣社員・アルバイト・パートなど様々な雇用形態や全国各地の希望勤務地の求職者にアピールするチャンスです。掲載依頼はこちら。';

$title = '求人広告掲載ガイド｜コールセンター特化型求人情報サイト【コールナビ】';
$keyword = 'コールセンター 求人,コールセンター 在宅,コールセンター バイト,オペレーター,アウトバウンド,インバウンド,求人　掲載,求人　サイト,求人　広告,求人　成果報酬';

$subject_to_admin = "【コールナビ】求人広告掲載にお問い合わせがありました。";

$subject_to_user = "【コールナビ】お問い合わせいただきありがとうございました（自動配信）";
$admin_name = "管理者";

$title_contact = '求人広告掲載に関する資料請求・お問い合わせ｜コールセンター特化型求人情報サイト【コールナビ】';

$des_contact = 'コールセンター・テレオペの求人広告を掲載するならコールセンター特化型求人サイト「コールナビ」。完全成果報酬型なので、初期費用は０円で掲載可能！正社員・派遣社員・アルバイト・パートなど様々な雇用形態や全国各地の希望勤務地の求職者にアピールするチャンスです。掲載依頼はこちら。';
$keyword_contact = 'コールセンター 求人,コールセンター 在宅,コールセンター バイト,オペレーター,アウトバウンド,インバウンド,求人　掲載,求人　サイト,求人　広告,求人　成果報酬';

/*
	----------------------------------------------------------
	INSERT DATA TO CONFIG
*/


$config['contact']['keisaiguide_content'] = $keisaiguide_content;
$config['contact']['title'] = $title;
$config['contact']['keyword'] = $keyword;
$config['contact']['des_contact'] = $des_contact;
$config['contact']['title_contact'] = $title_contact;
$config['contact']['keyword_contact'] = $keyword_contact;

$config['contact']['subject_to_admin'] = $subject_to_admin;
$config['contact']['subject_to_user'] = $subject_to_user;
$config['contact']['admin_name'] = $admin_name;

?>