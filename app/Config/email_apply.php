<?php
$config['email_apply']['bcc'] = array('kyujin@callnavi.jp', 'concierge@callnavi.jp');
$config['email_apply']['test_bcc'] = array('xuong.banh@jellyfishhr.com', 'tan.doan@jellyfishhr.com');

$config['email_apply']['admin_template_apple_cv_only_applicant'] = 'text/apple_cv_only_applicant';
$config['email_apply']['admin_template_apply_cv_applicant'] = 'text/apply_cv_applicant';
$config['email_apply']['admin_template_apply_nocv_applicant'] = 'text/apply_nocv_applicant';

$config['email_apply']['user_template_apple_cv_only_applicant'] = 'text/apple_cv_only_applicant';
$config['email_apply']['user_template_apply_cv_applicant'] = 'text/apply_cv_applicant';
$config['email_apply']['user_template_apply_nocv_applicant'] = 'text/apply_nocv_applicant';

$config['email_apply']['corporation_template_apply_cv_corporation'] = 'text/apply_cv_corporation';
$config['email_apply']['corporation_template_apply_nocv_corporation'] = 'text/apply_nocv_corporation';
$config['email_apply']['corporation_template_apply_sent_password'] = 'text/apply_sent_password';


$config['email_apply']['admin_subject_apple_cv_only_applicant'] = '【コールナビ】Apple Japan合同会社【Apple製品テクニカルアドバイザー（在宅勤務アドバイザー）】プレエントリー完了のご連絡';
$config['email_apply']['admin_subject_apply_cv_applicant'] =   '【コールナビ】■重要■{company_name}【{job_name}】求人応募完了のおしらせ（自動配信）';
$config['email_apply']['admin_subject_apply_nocv_applicant'] = '【コールナビ】■重要■{company_name}【{job_name}】求人応募完了のおしらせ（自動配信）';

$config['email_apply']['user_subject_apple_cv_only_applicant'] = '【コールナビ】Apple Japan合同会社【Apple製品テクニカルアドバイザー（在宅勤務アドバイザー）】プレエントリー完了のご連絡';
$config['email_apply']['user_subject_apply_cv_applicant'] = '【コールナビ】■重要■{company_name}【{job_name}】求人応募完了のおしらせ（自動配信）';
$config['email_apply']['user_subject_apply_nocv_applicant'] = '【コールナビ】■重要■{company_name}【{job_name}】求人応募完了のおしらせ（自動配信）';

$config['email_apply']['corporation_subject_apply_cv_corporation'] = '【コールナビ】求人に応募がありました。';
$config['email_apply']['corporation_subject_apply_nocv_corporation'] = '【コールナビ】求人に応募がありました。';
$config['email_apply']['corporation_subject_apply_sent_password'] = '【コールナビ】パスワードのご連絡';