<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
Router::connect('/', array('controller' => 'Top', 'action' => 'index'));

Router::connect('/search', array('controller' => 'search', 'action' => 'index'));
Router::connect('/search/index', array('controller' => 'search', 'action' => 'index'));
Router::connect('/search/ajaxSearch', array('controller' => 'search', 'action' => 'ajaxSearch'));
Router::connect('/search/countcondition', array('controller' => 'search', 'action' => 'countcondition'));
Router::connect('/search/job/*', array('controller' => 'search', 'action' => 'job'));
Router::connect('/search/urlEncode', array('controller' => 'search', 'action' => 'urlEncode'));
Router::connect('/search/result', array('controller' => 'search', 'action' => 'result'));
Router::connect('/search/updateHotKeyResult', array('controller' => 'search', 'action' => 'updateHotKeyResult'));
Router::connect('/search/getEmployment', array('controller' => 'search', 'action' => 'getEmployment'));
Router::connect('/search/*', array('controller' => 'search', 'action' => 'seoSearch'));

//-------------contents pages--------------------------
$src = strpos($_SERVER['REQUEST_URI'], '/contents/index/page');
$pre = strpos($_SERVER['REQUEST_URI'], '/contents/preview');

//preview for ADMIN
Router::connect('/contents/preview/*', array('controller' => 'contents', 'action' => 'preview'));

//contents pages for ajax
Router::connect('/ajax/contents/:category_alias', array('controller' => 'contents', 'action' => 'ajax_index'),
    array(
        'pass' => array('category_alias'),
        'category_alias' => '[a-z]*',
    )
);


//detail
if ($src === false) {
    Router::connect('/contents/:category_alias/:title_alias', array('controller' => 'contents', 'action' => 'detail'),
        array('pass' => array('category_alias', 'title_alias'))
    );

    /*New SEO: Detail of Content*/
    Router::connect('/contents/:category「:title」', array('controller' => 'contents', 'action' => 'detail'),
        array(
            'pass' => array('category', 'title'),
        )
    );
}

//category
if ($pre === false) {
    Router::connect('/contents/:category_alias', array('controller' => 'contents', 'action' => 'index'),
        array(
            'pass' => array('category_alias'),
        )
    );
}
//-------------[end] contents pages--------------------------


//-------------news page here--------------------------
$src = strpos($_SERVER['REQUEST_URI'], '/news/index/page');
$pre = strpos($_SERVER['REQUEST_URI'], '/news/preview');
if ($pre === false) {
    Router::connect('/news/:category_alias', array('controller' => 'news', 'action' => 'index'),
        array(
            'pass' => array('category_alias'),
            'category_alias' => '[a-z]+',
        )
    );
}
Router::connect('/news/preview/*', array('controller' => 'news', 'action' => 'preview'));


if ($src === false) {
    Router::connect('/news/:category_alias/:slug', array('controller' => 'news', 'action' => 'detail'),
        array(
            'pass' => array('category_alias', 'slug'),
            'category_alias' => '[a-z]+',
            'slug' => '[0-9]+')
    );
}


//-------------call center pape here--------------------------
//Top: domain.com/callcenter_matome
Router::connect('/callcenter_matome',
    array('controller' => 'Callcenter', 'action' => 'index'));

//Area: domain.com/callcenter_matome/area/hokaido
Router::connect('/callcenter_matome/area/:area_alias',
    array('controller' => 'Callcenter', 'action' => 'area'),
    array(
        'pass' => array('area_alias', 'city_id'),
        'category_alias' => '[a-z]+',
    )
);

//Area+city: domain.com/callcenter_matome/area/hokaido/city_1
Router::connect('/callcenter_matome/area/:area_alias/city_:city_id',
    array('controller' => 'Callcenter', 'action' => 'area'),
    array(
        'pass' => array('area_alias', 'city_id'),
        'category_alias' => '[a-z]+',
        'city_id' => '[0-9]+'
    )
);

//Detail: domain.com/callcenter_matome/area/hokaido/city_1/c_1
Router::connect('/callcenter_matome/area/:area_alias/city_:city_id/c_:callcenter_id',
    array('controller' => 'Callcenter', 'action' => 'detail'),
    array(
        'pass' => array('area_alias', 'city_id', 'callcenter_id'),
        'category_alias' => '[a-z]+',
        'city_id' => '[0-9]+',
        'callcenter_id' => '[0-9]+',
    )
);

//Around: domain.com/callcenter_matome/area/hokaido/city_1/c_1/around
Router::connect('/callcenter_matome/area/:area_alias/city_:city_id/c_:callcenter_id/around-search',
    array('controller' => 'Callcenter', 'action' => 'around_search'),
    array(
        'pass' => array('area_alias', 'city_id', 'callcenter_id'),
        'category_alias' => '[a-z]+',
        'city_id' => '[0-9]+',
        'callcenter_id' => '[0-9]+',
    )
);

//Top: domain.com/callcenter_matome/preview
Router::connect('/callcenter_matome/preview/*',
    array('controller' => 'Callcenter', 'action' => 'preview'));

//-------------pretty page here--------------------------
Router::connect('/pretty/:gender', array('controller' => 'pretty', 'action' => 'index'),
    array('pass' => array('gender')));

//-------------blog page here--------------------------
Router::connect('/blog/:category_alias', array('controller' => 'blog', 'action' => 'index'),
    array('pass' => array('category_alias')));

Router::connect('/blog/preview/*', array('controller' => 'blog', 'action' => 'preview'));

$src = strpos($_SERVER['REQUEST_URI'], '/blog/index');
if ($src === false) {
    Router::connect('/blog/:category_alias/:id', array('controller' => 'blog', 'action' => 'detail'),
        array('pass' => array('category_alias', 'id')));
}

//-------------secrect page here--------------------------
//$src = strpos($_SERVER['REQUEST_URI'], '/secret/index');
Router::connect('/secret', array('controller' => 'secret', 'action' => 'index'));
Router::connect('/secret/countcondition', array('controller' => 'secret', 'action' => 'countcondition'));
Router::connect('/secret/ajaxSearch', array('controller' => 'secret', 'action' => 'ajaxSearch'));
Router::connect('/secret/SendMail', array('controller' => 'secret', 'action' => 'SendMail'));
Router::connect('/secret/apply', array('controller' => 'secret', 'action' => 'apply'));
Router::connect('/secret/apply-step1', array('controller' => 'apply', 'action' => 'apply_step1'));
Router::connect('/secret/apply-step2', array('controller' => 'apply', 'action' => 'apply_step2'));
Router::connect('/secret/apply-thanks', array('controller' => 'apply', 'action' => 'apply_thanks'));
Router::connect('/secret/detail', array('controller' => 'secret', 'action' => 'detail'));
Router::connect('/secret/preview/*', array('controller' => 'secret', 'action' => 'preview'));
Router::connect('/secret/previewCompany/*', array('controller' => 'secret', 'action' => 'previewCompany'));
Router::connect('/secret/test_sendmail', array('controller' => 'secret', 'action' => 'test_sendmail'));
Router::connect('/secret/:id', array('controller' => 'secret', 'action' => 'detail'), array('pass' => array('id')));
Router::connect('/secret/*', array('controller' => 'secret', 'action' => 'seoSearch'));

//-------------admin here--------------------------
Router::connect('/login', array('controller' => 'auth', 'action' => 'login', 'plugin' => 'admin'));
Router::connect('/admin', array('controller' => 'ManageCompany', 'action' => 'index', 'plugin' => 'admin'));



//manage interview
Router::connect('/admin/ManageContents/add-interview', array('controller' => 'ManageInterview', 'action' => 'add', 'plugin' => 'admin'));
Router::connect('/admin/ManageContents/edit-interview/*', array('controller' => 'ManageInterview', 'action' => 'edit', 'plugin' => 'admin'));
Router::connect('/admin/ManageInterview/getSecretJobByCompanyId/*', array('controller' => 'ManageInterview', 'action' => 'getSecretJobByCompanyId', 'plugin' => 'admin'));

//-------------oiwaikin Page here--------------------------
Router::connect('/oiwaikin', array('controller' => 'oiwaikin', 'action' => 'index'));

Router::connect('/keisaiguide', array('controller' => 'keisaiguide', 'action' => 'index'));
Router::connect('/keisaiguide/contact', array('controller' => 'keisaiguide', 'action' => 'contact'));
Router::connect('/keisaiguide/contact/confirm', array('controller' => 'keisaiguide', 'action' => 'confirm'));
Router::connect('/keisaiguide/contact/thanks', array('controller' => 'keisaiguide', 'action' => 'thanks'));


/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
require CAKE . 'Config' . DS . 'routes.php';
