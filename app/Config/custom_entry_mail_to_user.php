<?php
	$company_custom_entry_mail = [
		'Apple Japan 合同会社' => array(
									'template' => 'secret_apple_japan', 
									'subject'  => '【コールナビ】Apple Japan合同会社【Apple製品テクニカルアドバイザー（在宅勤務アドバイザー）】プレエントリー完了のご連絡'
								),
	];

//set company has custom entry mail
$config['webconfig']['company_custom_entry_mail'] = $company_custom_entry_mail;