<!DOCTYPE html>
<html lang="ja">
<head>
	<?php echo $this->Html->charset(); ?>
	<title><?php echo $title; ?></title>
	<meta name="robots" content="noindex, follow">
	<?php
	//    meta tag
	echo $this->Html->meta('icon');
	echo $this->fetch('meta');
	//    css tag
	echo $this->Html->css('font-awesome.min');
	echo $this->Html->css('jquery.mCustomScrollbar');
	echo $this->Html->css('jquery.steps');
	echo $this->Html->css('common_version2');
	echo $this->Html->css('bootstrap_noresponsive');
	echo $this->Html->css('ipad_version2');
	echo $this->Html->css('pre-load');
	echo $this->fetch('css');
	//    js tag
	echo $this->Html->script('modernizr-2.6.2.min');
	echo $this->Html->script('jquery.min');
	echo $this->Html->script('bootstrap.min');
	echo $this->Html->script('easing');
	echo $this->Html->script('custom');
	echo $this->Html->script('util');
	echo $this->fetch('script');
	?>
</head>
<body class="container-contact bg-gray">
<div id="goTop" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 header"></div>
<div class="container no-padding headerlogo">
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding logotext">
		<a title="コールナビ｜コールセンターの総合情報サイト" class="logo" href="<?php echo FULL_BASE_URL; ?>">
			<img src="/img/logo_pc.png" alt="コールナビ｜コールセンターの総合情報サイト" />
		</a>
	</div>
</div>
<div id="div-main" class="no-padding container" style="margin-top: 15px">
	<?php echo $this->fetch('content'); ?>
</div>
</body>
</html>