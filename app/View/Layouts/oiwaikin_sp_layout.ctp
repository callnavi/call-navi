<!DOCTYPE html>
<html lang="ja">
<head>
    <?php echo $this->Html->charset(); ?>
    <title><?php echo $title; ?></title>
    <meta name="viewport" content="width=device-width,initial-scale=0.5, maximum-scale=0.5, minimum-scale=0.5, user-scalable=no">
    <?php
//     	metat tag
        echo $this->Html->meta('icon');
        echo $this->fetch('meta');
//      css tag
        echo $this->Html->css('font-awesome.min');
        echo $this->Html->css('common_v2_sp');
        echo $this->fetch('css');
    ?>
    <!-- Facebook Pixel Code -->
    <?php echo Configure::read('webconfig.facebook_pixel'); ?>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->

    <?php echo Configure::read('webconfig.google_analytics'); ?>
</head>

<body  class="page-oiwaikin <?php echo $this->params['controller']; ?> <?php echo $this->params['action']; ?>" >
    
    
    <div id="header" class="header">
        <?php
        //new sp layout change 06-10-2017 by andy william
         echo $this->element('Header/Header_v2/Header_P1_SP');?>

    </div>
    <div  id="content" class="main-contain">
        <?php echo $this->fetch('content'); ?>
    </div>
    <div id="footer" data-togglebnr-trigger="footer" class="common-footer  <?php echo !empty($heightFooter) ? $heightFooter : '';?>">
        <?php
        //new sp layout change 06-10-2017 by andy william
         echo $this->element('Footer/Footer_v2/Footer_P1_SP');?>

    </div>
    <?php //echo $this->element('sql_dump'); ?>
	
  	
   	<?php 
		//   js tag
        echo $this->Html->script('lib/bundle.min');
        echo $this->Html->script('jquery.min');
        echo $this->Html->script('common_v2_sp');
		echo $this->Html->script('new-search.js');
        echo $this->Html->script('new-search-em.js');
		echo $this->Html->script('search_submit/create_seo');
        echo $this->Html->script('app_new');
        echo $this->fetch('script');
	?>
	
	<?php echo $this->element('Item/search_box_area_popup_sp'); ?>
    <?php echo $this->element('Item/search_box_em_popup_sp'); ?>
	<?php echo $this->fetch('bottom'); ?>
	<?php echo $this->element('Item/measurement'); ?>
    <?php echo $this->element('CountTag/count_tag'); ?>
    <?php echo $this->element('Chat/chat_plugin_sp'); ?>
    
    <?php
        $url = $_SERVER['REQUEST_URI'];
        switch ($url) {
            case '/concierge':
                echo "<script type='text/javascript' charset='utf-8' src='//clickanalyzer.jp/ClickIndex.js' id='wiz/20170407182211'></script>\n";
                echo "<noscript><img src='//clickanalyzer.jp/ClickIndex.php?mode=noscript&id=wiz&pg=20170407182211' width='0' height='0' /></noscript>\n";
                break;
            default:
                break;
        }
    ?>

</body>
</html> 