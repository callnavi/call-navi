<!DOCTYPE html>
<html lang="ja">

    <head>
        <meta name="viewport" content="width=1150">            
        <?php echo $this->Html->charset(); ?>
        <title><?php echo $title; ?></title>

    <?php
        echo $this->fetch('meta');
		echo $this->Html->meta('icon');
        echo $this->Html->css('font-awesome.min');
		echo $this->Html->css('common_version2.css');
        echo $this->Html->css('bootstrap_noresponsive');
        echo $this->Html->css('ipad_version2');
        echo $this->Html->css('pre-load');
        echo $this->Html->script('lib/bundle.min');
		echo $this->Html->script('jquery.min');
		echo $this->Html->script('bootstrap.min');
		echo $this->Html->script('easing');
		echo $this->Html->script('create-company');
		echo $this->Html->script('custom');
		echo $this->Html->script('ie10-viewport-bug-workaround');
		echo $this->Html->script('ie-emulation-modes-warning');
		echo $this->Html->script('util');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
    
<!-- Facebook Pixel Code -->
    <?php echo Configure::read('webconfig.facebook_pixel'); ?>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->

    <?php echo Configure::read('webconfig.google_analytics'); ?>
    </head>

    <body class="page-oiwaikin">
   
        <div id="fb-root"></div>
        <script>
            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.6";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>

    <?php 
        // change header in issues#345 date 06-10-2017
        echo $this->element('Header/Header_v2/Header_P1');?>
  		<?php echo $this->fetch('sub_header'); ?>
        
        
        <?php echo $this->fetch('content'); ?>
        
        
        <?php echo $this->fetch('sub_footer'); ?>
        <?php echo $this->element('Footer/Footer_v2/Footer_P1');?>
        <?php echo $this->element('Item/measurement'); ?>
        <?php echo $this->element('CountTag/count_tag'); ?>
        <?php echo $this->element('Chat/chat_plugin'); ?>
    </body>

    </html>