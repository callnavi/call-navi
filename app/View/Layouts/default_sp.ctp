<!DOCTYPE html>
<html lang="ja">

<head>
    <?php echo $this->Html->charset(); ?>
    <title><?php echo $title; ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php
        echo $this->fetch('meta');
        echo $this->Html->meta('icon');
		echo $this->Html->css('bootstrap.min');
		echo $this->Html->css('bootstrap-theme.min'); 
		echo $this->Html->css('style-sp.css?v=1.5'); 
		echo $this->Html->css('navbar.css?v=1.4'); 
		echo $this->Html->css('jumbotron'); 
		echo $this->Html->css('ie10-viewport-bug-workaround'); 
		echo $this->Html->css('common-sp'); 
		echo $this->Html->css('simple-sidebar.css?v=1.5');
        echo $this->Html->css('pre-load');
        echo $this->Html->script('jquery.min'); 
		echo $this->Html->script('bootstrap.min'); 
		echo $this->Html->script('easing'); 
		echo $this->Html->script('create-company'); 
		echo $this->Html->script('ie10-viewport-bug-workaround'); 
		echo $this->Html->script('ie-emulation-modes-warning');
		echo $this->Html->script('util');
		echo $this->fetch('css'); 
		echo $this->fetch('script');
    ?>
    <!--    preload animation js-->
    <?php if(empty($_GET['page'])){ ?>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
        <script>
            function getRandomInt(min, max) {
                return Math.floor(Math.random() * (max - min + 1000)) + min;
            }
            $(window).load(function() {
                // Animate loader off screen
                setTimeout(function(){
                    $(".page-pre").fadeOut('slow');
                }, 200);


            });
        </script>
    <?php } ?>
    <!-- Facebook Pixel Code -->
    <?php echo Configure::read('webconfig.facebook_pixel'); ?>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->  
    <?php echo Configure::read('webconfig.google_analytics'); ?>

</head>

<body>
    <!--        preload animation contents-->
    <?php if(empty($_GET['page'])){ ?>
        <div class="page-pre">
            <div class="pre-img">
                <!-- <img src="/img/logo.png" /><br> -->
                <img src="/img/preload/Preloader_2.gif" />
                <!-- <p>Loading...</p> -->
            </div>
        </div>
    <?php } ?>
    <div id="fb-root"></div>
    <script>
        (function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.6";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
    <div id="wrapper" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
        <?php echo $this->element('Header/menu_sp');?>
    </div>
    <div  id="goTop" style="clear: both;padding-top:51px;" class=" no-padding container ">
        <?php echo $this->Flash->render(); ?>

        <?php echo $this->fetch('content'); ?>

        <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 slider">
            <?php echo $this->element('slidersp');?>
        </div>
    </div>

    <div id="footer" class="container padding-10 ">
        <div class="row">
            <?php echo $this->element('Panel/keyword_panel_sp');?>
            <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 no-padding footersplink"></div>
			
		</div>
 </div>

<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 no-padding footersplink cus-footersplink">
    <?php echo $this->element('Footer/footer_sp');?>
</div>

<?php echo $this->element('Footer/footer_bottom_sp');?>
    <script>
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
            $("#goTop").toggleClass("translate3d-250");
            $("#footer").toggleClass("translate3d-250");
            $("#overlay").toggleClass("actived");
            $("#logo-img").toggleClass("actived");


        });

        $("#overlay").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
            $("#goTop").toggleClass("translate3d-250");
            $("#footer").toggleClass("translate3d-250");
            $("#overlay").toggleClass("actived");
            $("#logo-img").toggleClass("actived");
        });
        $("#sidebar-wrapper .nav .dropdown a.dropdown-toggle").click(function(e){
            e.preventDefault();
            if($(this).next('ul').hasClass("active")) {
                $(this).next('ul').slideUp(300);
                $(this).next('ul').removeClass("active");
            } else {
                $('#sidebar-wrapper .nav .dropdown ul').slideUp(300);
                $('#sidebar-wrapper .nav .dropdown ul').removeClass('active');
                $(this).next('ul').slideDown(300);
                $(this).next('ul').addClass("active");
            }
        });
    </script>

	<?php echo $this->element('Item/measurement'); ?>
</body>
</html>