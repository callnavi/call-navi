<?php
/*This layout for TOP page*/
?>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta name="viewport" content="width=1150">    
	<?php echo $this->Html->charset(); ?>
	<title><?php echo $title; ?></title>
	<?php
	echo $this->Html->meta('icon');
	echo $this->fetch('meta');
	echo $this->Html->css('font-awesome.min');
	echo $this->Html->css('common_version2.css');
	echo $this->Html->css('top_version2.css');
    echo $this->Html->css('bootstrap_noresponsive');
   	echo $this->Html->css('ipad_version2.css');
   	echo $this->Html->css('pre-load');
	echo $this->Html->script('jquery.min');
	echo $this->Html->script('bootstrap.min');
	echo $this->Html->script('easing');
	echo $this->Html->script('create-company');
	echo $this->Html->script('custom');
	echo $this->Html->script('ie10-viewport-bug-workaround');
	echo $this->Html->script('ie-emulation-modes-warning');
	$this->app->echoScriptScrollColumnJs();
	echo $this->Html->script('search_submit/create_seo');
	echo $this->fetch('css');
	echo $this->fetch('script');
	?>
    <!-- Facebook Pixel Code -->
    <?php echo Configure::read('webconfig.facebook_pixel'); ?>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->      
	<?php echo Configure::read('webconfig.google_analytics'); ?>
</head>

<body class="top-layout">

	
	<?php echo $this->element('Header/header_version2');?>
	
	<div id="div-main" class="mg-bottom-60">
		<div class="no-padding container">
			<div class="row">
				<?php echo $this->element('Item/top_banner_version2');?>
			</div>
		</div>
		<?php
		echo $this->element('Item/concierge_link_banner');
		//echo $this->element('Item/top_banner_secret');?>
			
		<div class="no-padding container ipad-padding">
			<div class="clearfix">
				<div class="pull-left content">
					<?php echo $this->fetch('content'); ?>
				</div>
				<div dzgsd class="pull-right slider">
					<div id="right-column">
						<?php  echo $this->element('../Top/_pickup'); ?>
						<?php echo $this->element('slider_top');?>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<?php echo $this->element('Item/bottom_banner');?>
	
	<?php echo $this->element('Footer/footer_version2');?>
	<?php //echo $this->element('sql_dump'); ?>
	<?php //echo $this->element('Item/facebook_sdk');?>
	<?php echo $this->element('Item/measurement'); ?>
	<?php echo $this->element('CountTag/count_tag'); ?>
</body>
</html>