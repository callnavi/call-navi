<?php
/*This layout for TOP page*/
?>
<!DOCTYPE html>
<html lang="ja">
<head>
    <meta name="viewport" content="width=1150">    
	<?php echo $this->Html->charset(); ?>
	<title><?php echo $title; ?></title>
	<?php
	echo $this->Html->meta('icon');
	echo $this->fetch('meta');
	echo $this->Html->css('font-awesome.min');
	echo $this->Html->css('common_version2.css');
	echo $this->Html->css('top_version2.css');
    echo $this->Html->css('bootstrap_noresponsive');
   	echo $this->Html->css('ipad_version2.css');
   	echo $this->Html->css('pre-load');
   	echo $this->Html->css('2017/top');
   	echo $this->Html->css('app_new');
	echo $this->Html->script('lib/bundle.min');
	echo $this->Html->script('jquery.min');
	echo $this->Html->script('bootstrap.min');
	echo $this->Html->script('easing');
	echo $this->Html->script('create-company');
	echo $this->Html->script('custom');
	echo $this->Html->script('ie10-viewport-bug-workaround');
	echo $this->Html->script('ie-emulation-modes-warning');
	$this->app->echoScriptScrollColumnJs();
	echo $this->Html->script('search_submit/create_seo');
	echo $this->Html->script('app_new');

	echo $this->fetch('css');
	echo $this->fetch('script');
	?>
    <!-- Facebook Pixel Code -->
    <?php echo Configure::read('webconfig.facebook_pixel'); ?>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->      
	<?php echo Configure::read('webconfig.google_analytics'); ?>
</head>

<body class="top-layout">
	<?php echo $this->element('Header/Header_v2/Header_P1');?>
	<div id="div-main">
		<?php echo $this->fetch('content'); ?>
	</div>
	<?php echo $this->element('Footer/Footer_v2/Footer_P1');?>
	<?php //echo $this->element('sql_dump'); ?>
	<?php //echo $this->element('Item/facebook_sdk');?>
	<?php echo $this->element('Item/measurement'); ?>
	<?php echo $this->element('CountTag/count_tag'); ?>
	<?php echo $this->element('Chat/chat_plugin'); ?>
</body>
</html>