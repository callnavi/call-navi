<!DOCTYPE html>
<html lang="ja">
<head>
    <meta name="viewport" content="width=1150">
    <?php echo $this->Html->charset(); ?>
    <title><?php echo $title; ?></title>
    <?php
//    meta tag
    echo $this->Html->meta('icon');
    echo $this->fetch('meta');
//    css tag
    echo $this->Html->css('font-awesome.min');
    echo $this->Html->css('jquery.mCustomScrollbar');
    //echo $this->Html->css('jquery.steps');
    echo $this->Html->css('common_version2');
    echo $this->Html->css('bootstrap_noresponsive');
    echo $this->Html->css('ipad_version2');
    echo $this->Html->css('pre-load');
    echo $this->fetch('css');
//    js tag
    echo $this->Html->script('lib/bundle.min');
    echo $this->Html->script('modernizr-2.6.2.min');
    echo $this->Html->script('jquery.min');
    echo $this->Html->script('jquery.validate.min');    
    echo $this->Html->script('bootstrap.min');
    echo $this->Html->script('easing');
    echo $this->Html->script('jquery.cookie-1.3.1');    
    echo $this->Html->script('custom');
	echo $this->Html->script('util');
    echo $this->fetch('script');
    ?>
    <?php echo Configure::read('webconfig.google_analytics'); ?>
</head>
<body class="page-keisaiguide">

<div class="page__inner">
    <?php echo $this->fetch('content'); ?>
</div>


</body>
</html>
