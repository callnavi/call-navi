<!DOCTYPE html>
<html lang="ja">
<head>
    <?php echo $this->Html->charset(); ?>
    <title><?php echo $title; ?></title>
    <meta name="viewport" content="width=device-width,initial-scale=0.5, maximum-scale=0.5, minimum-scale=0.5, user-scalable=no">
    <?php
//        metat tag
        echo $this->Html->meta('icon');
        echo $this->fetch('meta');
//        css tag
        echo $this->Html->css('bootstrap.min');
        echo $this->Html->css('jquery.mCustomScrollbar');
        echo $this->Html->css('font-awesome.min');
        echo $this->Html->css('bootstrap-theme.min');
        echo $this->Html->css('style-sp.css?v=1.5');
        echo $this->Html->css('navbar.css?v=1.4');
        echo $this->Html->css('simple-sidebar.css?v=1.5');
        echo $this->Html->css('common-sp');
        echo $this->Html->css('pre-load');
        echo $this->Html->css('app_new-sp');
        echo $this->Html->css('common_v2_sp');
        echo $this->fetch('css');

//    js tag
        echo $this->Html->script('jquery.min');
        echo $this->Html->script('bootstrap.min');
        echo $this->Html->script('easing');
        echo $this->Html->script('util');
        echo $this->Html->script('lib/bundle.min');
        echo $this->Html->script('search_submit/create_seo');
        echo $this->Html->script('common_v2_sp');
        echo $this->Html->script('app_new');
        echo $this->fetch('script');
    ?>
    <!--    preload animation js-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.2/modernizr.js"></script>
    <?php if(empty($_GET['page'])) {?>
        <script>
            function getRandomInt(min, max) {
                return Math.floor(Math.random() * (max - min + 1000)) + min;
            }
        </script>
        <?php if ($_SERVER['REQUEST_URI'] == '/') { ?>

            <script>
                $(window).load(function () {
                    // Animate loader off screen
                    setTimeout(function(){
                        $(".top-pre").fadeOut('slow');
                    }, 1000);
                });
            </script>
        <?php }else{
        ?>
            <script>
                $(window).load(function () {
                    // Animate loader off screen
                    setTimeout(function(){
                        $(".page-pre").fadeOut('slow');
                    },1000);

                });
            </script>

        <?php }
    }?>
    <!-- Facebook Pixel Code -->
    <?php echo Configure::read('webconfig.facebook_pixel'); ?>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->    
    <?php echo Configure::read('webconfig.google_analytics'); ?>
</head>

<body  class="bg-gray" >
    <?php if(empty($_GET['page'])) {
        if ( $_SERVER['REQUEST_URI'] == '/') {
            if (isset($_COOKIE["isFirstAccess"])) { // check first times access ?>
                <div class="top-pre">
                    <div class="pre-img">
                        <!-- <img src="/img/logo.png" /><br> -->
                        <img src="/img/preload/Preloader_2.gif"/>
                        <!-- <p>Loading...</p> -->
                    </div>

                </div>
            <?php } else {
                setcookie("isFirstAccess", true, time() + 900, "/"); // 86400 = 1 day
                ?>
                <div class="top-pre">
                    <div class="pre-img">
                        <img src="/img/logo.png" /><br>
                        <!-- <img src="/img/preload/Preloader_default.gif" /> -->
                        <p>Loading...</p>
                    </div>
                </div>

        <?php }
        } else {
            ?>
            <div class="page-pre">
                <div class="pre-img">
                    <!-- <img src="/img/logo.png"/><br> -->
                    <img src="/img/preload/Preloader_2.gif"/>
                    <!-- <p>Loading...</p> -->
                </div>

            </div>

        <?php }
    }?>
    
    <div id="wrapper" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding">
        <?php echo $this->element('Header/Header_v2/Header_P1_SP');?>
    </div>
    <div class="no-padding " id="goTop" style="clear: both;padding-top:100px;">    
        <?php echo $this->fetch('content'); ?>
    </div>
     <footer class="common-footer" data-togglebnr-trigger="footer">
        <?php
        //new sp layout change 06-10-2017 by andy william
         echo $this->element('Footer/Footer_v2/Footer_P1_SP');?>
    <?php //echo $this->element('sql_dump'); ?>
    </footer>
    <script>
        $("#menu-toggle").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
            $("#goTop").toggleClass("translate3d-250");
            $("#footer").toggleClass("translate3d-250");
            $("#overlay").toggleClass("actived");
            $("#logo-img").toggleClass("actived");


        });

        $("#overlay").click(function(e) {
            e.preventDefault();
            $("#wrapper").toggleClass("toggled");
            $("#goTop").toggleClass("translate3d-250");
            $("#footer").toggleClass("translate3d-250");
            $("#overlay").toggleClass("actived");
            $("#logo-img").toggleClass("actived");
        });
        $("#sidebar-wrapper .nav .dropdown a.dropdown-toggle").click(function(e){
            e.preventDefault();
            if($(this).next('ul').hasClass("active")) {
                $(this).next('ul').slideUp(300);
                $(this).next('ul').removeClass("active");
            } else {
                $('#sidebar-wrapper .nav .dropdown ul').slideUp(300);
                $('#sidebar-wrapper .nav .dropdown ul').removeClass('active');
                $(this).next('ul').slideDown(300);
                $(this).next('ul').addClass("active");
            }
        });
    </script>

	<?php echo $this->element('Item/measurement'); ?>
</body>
</html>