<!DOCTYPE html>
<html lang="ja">
<head>
    <?php echo $this->Html->charset(); ?>
    <title><?php echo $title; ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width,initial-scale=0.5, maximum-scale=0.5, minimum-scale=0.5, user-scalable=no">
    <?php
//    meta tag
    echo $this->Html->meta('icon');
    echo $this->fetch('meta');
//    css tag
    echo $this->Html->css('font-awesome.min');
    echo $this->fetch('css');
//    js tag
    echo $this->Html->script('lib/bundle.min');
    echo $this->Html->script('jquery.min');
    echo $this->Html->script('jquery.validate.min');
    echo $this->fetch('script');
    ?>
    <?php echo Configure::read('webconfig.google_analytics'); ?>
</head>
<body class="page-keisaiguide">

<div class="page__inner">
    <?php echo $this->fetch('content'); ?>
</div>


</body>
</html>
