<!DOCTYPE html>
<html lang="ja">
<head>
<?php
$controller_page = strtolower($this->params['controller']); 
?>
    <?php 
    //in earch, apply, concierge, secret this page the view port don`t want to see
    //all page except these pages
    if ($controller_page != 'secret' && $controller_page != 'concierge' && $controller_page    != 'apply' && $controller_page  != 'search' ){ ?>
        <meta name="viewport" content="width=1150">    
    <?php } ?>
    <?php echo $this->Html->charset(); ?>
    <title><?php echo $title; ?></title>
    <?php
//    meta tag
    echo $this->Html->meta('icon');
    echo $this->fetch('meta');
//    css tag
    echo $this->Html->css('font-awesome.min');
    echo $this->Html->css('jquery.mCustomScrollbar');
    //echo $this->Html->css('jquery.steps');
    echo $this->Html->css('common_version2');
    echo $this->Html->css('bootstrap_noresponsive');
    echo $this->Html->css('ipad_version2');
    echo $this->Html->css('pre-load');
    echo $this->Html->css('app_new');    
    echo $this->fetch('css');
//    js tag
    echo $this->Html->script('lib/bundle.min');
    echo $this->Html->script('modernizr-2.6.2.min');
    echo $this->Html->script('jquery.min');
    echo $this->Html->script('bootstrap.min');
    echo $this->Html->script('easing');
    echo $this->Html->script('custom');
    echo $this->Html->script('util');
    echo $this->Html->script('search_submit/create_seo');
    echo $this->Html->script('app_new');
    echo $this->fetch('script');
    ?>
    
    <!-- Facebook Pixel Code -->
    <?php echo Configure::read('webconfig.facebook_pixel'); ?>
    <!-- DO NOT MODIFY -->
    <!-- End Facebook Pixel Code -->    
    
    <?php echo Configure::read('webconfig.google_analytics'); ?>
</head>
<body class="container-contact <?php echo $this->params['controller']; ?> <?php echo $this->params['action']; ?>">
<?php 
        // change header in issues#345 date 06-10-2017
    echo $this->element('Header/Header_v2/Header_P1');?>
<?php echo $this->fetch('sub_header'); ?>
<div id="div-main" class="no-padding container ipad-padding">
    <?php echo $this->fetch('content'); ?>
</div>
<?php echo $this->fetch('sub_footer'); ?>
<?php 
    // change footer in issues#345 date 06-10-2017
    echo $this->element('Footer/Footer_v2/Footer_P1');?>
<?php echo $this->element('Item/measurement'); ?>
<?php echo $this->element('CountTag/count_tag'); ?>
<?php echo $this->element('Chat/chat_plugin'); ?>

<?php
    $url = $_SERVER['REQUEST_URI'];
    switch ($url) {
        case '/concierge':
            echo "<script type='text/javascript' charset='utf-8' src='//clickanalyzer.jp/ClickIndex.js' id='wiz/20170407182211'></script>\n";
            echo "<noscript><img src='//clickanalyzer.jp/ClickIndex.php?mode=noscript&id=wiz&pg=20170407182211' width='0' height='0' /></noscript>\n";
            break;
        default:
            break;
    }
?>
</body>
</html>
