<?php
?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <style type="text/css">
        /* Client-specific Styles */
        #outlook a{padding:0;} /* Force Outlook to provide a "view in browser" button. */
        body{font-family:Arial; width:100% !important;} .ReadMsgBody{width:100%;} .ExternalClass{width:100%;} /* Force Hotmail to display emails at full width */
        body{-webkit-text-size-adjust:none;} /* Prevent Webkit platforms from changing default text sizes. */

    </style>

</head>
<body>

<p><strong>管理者様へ</strong></p><br>
<p><strong>下記の会社からの問い合わせがありましたので、</strong></p>
<p><strong>ご確認ください。</strong></p><br>
<p>─ご送信内容───────────────────</p><br>
<p>お問い合わせ内容:<br>
    <?php echo $arr["content"] ;?>
</p>
<p>会社名: <?php echo $arr["companyName"] ;?></p>
<p>部署名・役職: <?php echo $arr["departmentNamePosition"] ;?></p>
<p>氏名: <?php echo $arr["name"] ;?> </p>
<p>メールアドレス: <?php echo $arr["mail"] ;?></p>
<p>電話番号: <?php echo $arr["tel"] ;?></p>
<p>お問い合わせ解答方法: <?php echo $arr["answer"] ;?></p><br>
<p>──────────────────────────</p>
</body>
</html>