管理者様へ
下記の会社からの問い合わせがありましたので、
ご確認ください。
─ご送信内容───────────────────

お問い合わせ内容:
    <?php echo $arr["content"] ;?>

会社名: <?php echo $arr["companyName"] ;?>

部署名・役職: <?php echo $arr["departmentNamePosition"] ;?>

氏名: <?php echo $arr["name"] ;?>

メールアドレス: <?php echo $arr["mail"] ;?>

電話番号: <?php echo $arr["tel"] ;?>

お問い合わせ解答方法: <?php echo $arr["answer"] ;?>

──────────────────────────