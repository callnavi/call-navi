管理者　様

下記内容で求人広告掲載の資料請求・お問い合わせがありました。
ご確認ください。

■申請内容

【会社名】<?php echo $arr["company"];  echo("\n"); ?>
【ご担当者様】<?php echo $arr["name"];  echo("\n"); ?>
【メールアドレス】<?php echo $arr["mail"];  echo("\n"); ?>
【ご住所】<?php echo $arr["address"];  echo("\n"); ?>
【電話番号】<?php echo $arr["phone"];  echo("\n"); ?>
【お問い合わせ内容】<?php echo $arr["inquiry"];  echo("\n"); ?>
【お問い合わせ内容詳細】<?php echo $arr["remarks"];  echo("\n"); ?>
