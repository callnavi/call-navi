<?php echo $arr["name"] ;?>　様

【コールナビ】をご利用いただき、ありがとうございます。
下記内容でお祝い金の申請受付を完了しました。

------------------------------------------------------

■申請内容

【応募ID】<?php echo $arr["entryId"] ;?>

【初出勤完了有無】<?php echo $arr["complete"] ;?>

【お名前】<?php echo $arr["name"] ;?>

【メールアドレス】<?php echo $arr["mail"] ;?>

【採用企業名】<?php echo $arr["company"] ;?>

【勤務開始日】<?php echo $arr["startDay_year"] ;?>年<?php echo $arr["startDay_month"] ;?>月<?php echo $arr["startDay_day"] ;?>日
【振込口座名義】<?php echo $arr["account_lastName"] ;?> <?php echo $arr["account_firstName"] ;?>

【備考欄】
    <?php echo $arr["remarks"] ;?>


■今後の流れについて

・採用、勤続確認
審査時に採用、勤務の確認が取れない場合は、確認書類の提示を求める場合がございます。
※審査には約2～3ヶ月のお時間をいただきます。予めご了承ください。
↓
・お祝い金のご入金手続きメールの受信
※楽天銀行より簡単振込（メルマネ）の手続きメールが届きますので、URLにアクセスし、お振込先情報の登録手続きをしてください。
※メールの受信設定で、【 @ac.rakuten-bank.co.jp 】が受信できないようになっている場合、 メールが届きません。設定を変更して、受信可能な状態にしてください。
↓
・お祝い金の受け取り
楽天銀行の簡単振込（メルマネ）の受け取り手続き完了後、当日または翌営業日にお祝い金が振り込まれます。

▼お祝い金受け取りまでの詳細はこちら
⇒https://callnavi.jp/oiwaikin/


－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－

尚、このメールは自動配信メールになります。
このメールにご返信頂きましてもこのメールでのご返信は致しておりません。

このメールに心当たりのない場合や、その他ご不明点などございましたら、下記までご連絡ください。

－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－


──────────────────────────
   ■配信元
   株式会社コールナビ
   コールナビ窓口
   info@callnavi.jp
   ■コールナビ
   https://callnavi.jp/
   Copyright © CALL Navi Inc.
──────────────────────────
