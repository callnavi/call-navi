<?php 
	$arr['link_download_cv'] = Router::fullBaseUrl().'/downloadcv/?id='.$arr['entry_id'];
?>
<?php echo $arr["company_name"] ;?>　採用ご担当者様

お客様より求人への応募がありましたのでご連絡致します。
詳細は以下の通りです。
■■応募内容■■■■■■■■■■■■■■■■■■■■■

対象求人： <?php echo $arr["job_title"] ; echo("\n");?>
URL：https://callnavi.jp/secret/<?php echo $arr["job_title"] ; echo("\n");?>

■応募内容
【お名前】 <?php echo $arr["full_name"] ; echo("\n");?>
【ふりがな】<?php echo $arr["phonetic_name"] ; echo("\n");?>
【生年月日】<?php echo $arr["birth_date"] ; echo("\n");?>
【性別】<?php echo $arr["gender"] ; echo("\n");?>
【電話番号】<?php echo $arr["phone_number"] ; echo("\n");?>
【メールアドレス】<?php echo $arr["e-mail"] ; echo("\n");?>
【スキル】<?php echo $arr["skill"] ; echo("\n");?>
<?php if($arr["working_time"] == ""){
}else{
?>
【勤務可能時間】<?php echo $arr["working_time"] ; echo("\n");?>
<?php    
} ?>
【種別】<?php echo $arr["classification"] ; echo("\n");?>
【備考欄】
    <?php echo $arr["content"] ; echo("\n");?>

【履歴書・職務経歴書のダウンロード】
以下のURLにアクセスし、パスワードを入力するとダウンロードができます。
※パスワードは別メール（パスワードのご連絡）をご確認ください。
<?php echo $arr['link_download_cv'];echo"\n";?>
※このURLの有効期限は3日間です。期限内にダウンロードしてください。

■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
尚、このメールは自動配信メールになります。
このメールにご返信をいただいても返信は致しておりません。
万が一、こちらのメールにお心当たりがない場合や、ご質問がある場合などは、下記までご連絡をお願いします。
－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－
■配信元
株式会社コールナビ
コールナビ窓口
info@callnavi.jp
■コールナビ
https://callnavi.jp/
Copyright © CALL Navi Inc.
