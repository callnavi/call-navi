<?php
$contactInfo = '';
if (!empty($arr["email"]) && !empty($arr["phone"])) {
    $contactInfo = $arr["email"] . '-' . $arr["phone"];
} else {
    $contactInfo = !empty($arr["email"]) ? $arr["email"] : $arr["phone"];
}
?>
この度はお問い合せ頂き誠にありがとうございました。

担当アドバイザーからサービスのご案内を差し上げます。 通常2営業日以内に対応致しますが、状況によってはご連絡に お時間をいただく場合がございます。

何卒、ご了承いただけますようお願い申し上げます。

─ご送信内容の確認─────────────────

[ お名前 ] <?php echo $arr["name"] ; echo"\n\n" ?>
[ 希望勤務地 ] <?php echo $arr["work_location"] ; echo"\n\n" ?>
[ ご連絡先 ]  <?php echo $contactInfo;echo"\n\n" ?>
[ 希望条件 ] <?php echo $arr["work_condition"] ; echo"\n\n" ?>
[ 個人情報取り扱い ] 同意する
<?php echo "\n\n" ?>
──────────────────────────

このメールに心当たりの無い場合は、お手数ですが下記連絡先までお問い合わせください。

この度はお問い合わせ重ねてお礼申し上げます。

━━━━━━━━━━━━━━━━━━━━━━━━━━

株式会社コールナビ

東京都新宿区高田馬場2-13-2

mail：info@callnavi.jp

━━━━━━━━━━━━━━━━━━━━━━━━━━