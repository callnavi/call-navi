<?php echo $arr["name"];?>様

プレエントリーが完了しましたのでご連絡致します。
<?php echo $arr["name"];?>様

が応募した内容は以下の通りです。
■■プレエントリー内容■■■■■■■■■■■■■■■■■■■■■

対象求人 <?php echo $arr['job_name'];echo"\n\n"; ?>
URL ：<?php echo $arr['link'];echo"\n\n"; ?>

<フォーム内容>
氏名：<?php echo $arr["name"]; echo"\n\n";?>
フリガナ：<?php echo $arr["phonetic"]; echo"\n\n";?>
年齢：<?php echo $arr["birthday"]; echo"\n\n";?>
性別：<?php echo $arr["gender"]; echo"\n\n";?>
メールアドレス：<?php echo $arr["mail"]; echo"\n\n";?>
TEL：<?php echo $arr["tel"]; echo"\n\n";?>
履歴書：<?php echo $arr['cv1_path']; echo"\n\n";?>
職務経歴書：<?php echo $arr['cv2_path']; echo"\n\n";?>
備考欄：<?php echo $arr["content"]; echo"\n\n";?>

■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
内容にお間違えがないかご確認の上、プレエントリーをした企業からのご連絡をお待ちください。
このメールはコールナビにて、プレエントリーをいただいた際に自動的に送信されます。
このメールにご返信をいただいても応募企業には届きません。
万が一、こちらのメールにお心当たりがない場合や、ご質問がある場合などは、
下記までご連絡をお願いします。
－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－

■配信元
株式会社コールナビ
コールナビ窓口
info@callnavi.jp
■コールナビ
https://callnavi.jp/
Copyright © CALL Navi Inc.

