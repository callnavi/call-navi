この度はお問い合せ頂き誠にありがとうございました。 担当者からご案内を差し上げます。 通常2営業日以内に対応致しますが、内容によっては返信に お時間をいただく場合がございます。 何卒、ご了承いただけますようお願い申し上げます。

─ご送信内容───────────────────

[ 氏名 ]　<?php echo $arr["name"]; echo"\n\n"?>
[ メールアドレス ]　<?php echo $arr["mail"]; echo"\n\n";?>
[ お問い合わせ内容 ]　<?php echo $arr["content"]; echo"\n";?>

──────────────────────────

このメールに心当たりの無い場合は、お手数ですが 下記連絡先までお問い合わせください。

この度はお問い合わせ重ねてお礼申し上げます。

━━━━━━━━━━━━━━━━━━━━━━━━━━ 株式会社コールナビ 東京都
宿区高田馬場2-13-2 mail：info@callnavi.jp 
━━━━━━━━━━━━━━━━━━━━━━━━━━