管理者　様

下記内容でお祝い金の申請受付がありました。
ご確認ください。

■申請内容

【応募ID】<?php echo $arr["entryId"] ;?>

【初出勤完了有無】<?php echo $arr["complete"] ;?>

【お名前】<?php echo $arr["name"] ;?>

【メールアドレス】<?php echo $arr["mail"] ;?>

【採用企業名】<?php echo $arr["company"] ;?>

【勤務開始日】<?php echo $arr["startDay_year"] ;?>年<?php echo $arr["startDay_month"] ;?>月<?php echo $arr["startDay_day"] ;?>日
【振込口座名義】<?php echo $arr["account_lastName"] ;?> <?php echo $arr["account_firstName"] ;?>

【備考欄】
    <?php echo $arr["remarks"] ;?>


