<?php echo $arr["full_name"] ;?> 様

【コールナビ】をご利用いただき、ありがとうございます。
下記求人の応募受付を完了しました。


■■応募内容■■■■■■■■■■■■■■■■■■■■■

応募企業名 <?php echo $arr["full_name"] ; echo("\n"); ?>
対象求人 <?php echo $arr["company_name"] ;  echo("\n");?>

URL：https://callnavi.jp/secret/<?php echo $arr["job_title"];  echo("\n");?>

■応募内容
【お名前】<?php echo $arr["full_name"] ;?>
【ふりがな】<?php echo $arr["phonetic_name"] ;?>
【生年月日】<?php echo $arr["birth_date"] ;?>
【性別】<?php echo $arr["gender"] ;?>
【電話番号】<?php echo $arr["phone_number"] ;?>
【メールアドレス】<?php echo $arr["e-mail"] ;?>
【スキル】<?php echo $arr["skill"] ;?>
【備考欄】
    <?php echo $arr["content"] ;?>

■応募ID
【応募ID】<?php echo $arr["apply_code"] ;?>
※応募IDはお祝い金の申請時に必要になります。必ず保存してお持ちください。

▼お祝い金申請に関する詳細はこちら
⇒ https://callnavi.jp/oiwaikin/

■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
   内容にお間違えがないかご確認の上、応募をした企業からのご連絡をお待ちください。
   このメールはコールナビにて、応募ををいただいた際に自動的に送信されます。
   このメールにご返信をいただいても応募企業には届きません。
   万が一、こちらのメールにお心当たりがない場合や、ご質問がある場合などは、
   下記までご連絡をお願いします。
   －－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－

   ■配信元
   株式会社コールナビ
   コールナビ窓口
   info@callnavi.jp
   ■コールナビ
   https://callnavi.jp/
   Copyright © CALL Navi Inc.
