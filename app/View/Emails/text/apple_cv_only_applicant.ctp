<?php echo $arr["full_name"] ;?> 様

お世話になっております。
Apple Japanの採用担当です。

この度は、Appleの求人募集にご応募いただき誠にありがとうございました。

ご提出いただいた応募書類にて、書類選考を進めさせていただきます。
恐れ入りますが、書類選考結果につきましては、次の面接に進む場合のみのご案内となります。

次の面接に進んで頂く場合のみ、1週間以内に別途ご連絡を差し上げます。

何卒、よろしくお願いいたします。


Apple AHA Recruiting

あなたの家で、ビジネスが始まります。

AHA
http://www.apple.com/jobs/jp/aha.html
FAQ
http://www.apple.com/jobs/jp/advisor_pro_faq.html

Apple
Corporate Recruiting, North East Asia 
Roppongi Hills
6-10-1 Roppongi, Minato-ku
Tokyo 106-6140 Japan
aha_recruiting@apple.com

■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
このメールはコールナビにて、エントリーをいただいた際に送信されます。
このメールにご返信をいただいても応募企業には届きません。
万が一、こちらのメールにお心当たりがない場合や、ご質問がある場合などは、
下記までご連絡をお願いします。
－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－

■配信元
株式会社コールナビ
コールナビ窓口
info@callnavi.jp
■コールナビ
https://callnavi.jp/
Copyright © CALL Navi Inc.
