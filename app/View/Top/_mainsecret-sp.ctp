      <div class="plaid-topic--secondary df-padding"><span>オリジナル求人</span></div>
      <div class="article-list">
        <ul>
             <?php
                foreach ($secret as $value){    
                $labels = explode(',',$value['CareerOpportunity']['employment']);
                
                $corporate = $this->App->PictureGetTop($value['CareerOpportunity']['company_id']);
                
                if(empty($value['CareerOpportunity']['corporate_logo']))
                {
                    $pic_url = '/img/instead/img_no-image_' . sprintf("%02d", rand(1, 10)) . '.jpg';
                }
                else
                {
                    $pic_url = $this->webroot."upload/secret/jobs/".$value['CareerOpportunity']['corporate_logo'];
                }
                $pic_error_url = '/img/instead/img_no-image_' . sprintf("%02d", rand(1, 10)) . '.jpg';
                                
                $href =$href = $this->App->TopinterviewLink($value['CareerOpportunity']);
                $href_cat = '/secret/'.$labels[0];
            ?>
          <li class="df-padding">
            <div class="article-box">
              <div class="display-table">
                <div class="search-image"><a href="<?php echo($href); ?>"><img src="<?php echo $pic_url; ?>" alt="<?php echo $title; ?>" onerror="this.onerror=null;this.src='<?php echo $pic_error_url; ?>';"></a></div>
                <div class="search-content">
                  <div class="top-part"><a class="search-content__title" href="<?php echo($href); ?>"><?php echo($value['CareerOpportunity']['job']);?></a></div>
                  <div class="middle-part--search"><?php  echo(strip_tags($value['CareerOpportunity']['branch_name']));  ?></div>
                  <div class="bottom-part">
                    <dl class="search-detail">
                      <dt class="search-detail__title">給与：</dt>
                      <dd class="search-detail__text"><?php echo(strip_tags($value['CareerOpportunity']['hourly_wage']));?></dd>
                    </dl>
                    <dl class="search-detail">
                      <dt class="search-detail__title">アクセス：</dt>
                      <dd class="search-detail__text"><?php echo(strip_tags($value['CareerOpportunity']['work_location']));?></dd>
                    </dl>
                    <dl class="search-detail">
                      <dt class="search-detail__title">条件：</dt>
                      <dd class="search-detail__text"><?php echo(strip_tags($value['CareerOpportunity']['work_features']));?></dd>
                    </dl>
                  </div>
                </div>
              </div>
            </div>
          </li>
            <?php
                }
            ?>
        </ul>
      </div>
    <div class="show-more df-padding mg-top-40"><a class="btn-green-2d" href="./secret">もっと見る</a></div>
