<?php
	/*
		requestAction('elements/search_total')
		-> go: app/Controller/ElementsController.php -> call: search_total()
		
		Configure::read('webconfig.area_data')
		-> go: app/Config/webconfig.php -> get: $config['webconfig']['area_data']
	*/
	$search_total = $this->requestAction('elements/search_total');
	$secret_total = $this->requestAction('elements/secret_total');
	$area_data =  Configure::read('webconfig.area_data');
	$employment_data = Configure::read('webconfig.employment_data');
	$search_area_group = Configure::read('webconfig.search_area_group');

	$totalJob = $this->app->countTotalJob();
?>
<div class="top-banner-bg">
	<div class="container">
		<div class="top-banner__h padding-lr-40">
			<h1 class="_h1 mg-top-120 color-df-white"><strong>“働きやすいコールセンター”<small class="color-df-white">が探せる求人サイト</small></strong></h1>
			<div class="comment-oval text-center">
				<div class="comment-oval__box"></div>
				<div class="comment-oval__content color-df-green">
					<div class="_s">本日のコールセンター求人数</div>
					<div class="_h1 lh-1"><strong><?php echo number_format($totalJob); ?><span class="_h2">件</span></strong></div>
				</div>
			</div>
		</div>
		<div class="clearfix float-swp mg-top-20 padding-lr-40">
			<div class="search-wrap-verticle-box pull-left">

				<div class="text-center all-middle mg-bottom-10">
					<span class="icon-circle-plus"></span>

					<span class="_h2 color-df-white">条件を指定して検索</span>
				</div>

				<form action="/search" method="GET" data-seo="submit" class="hori-modern-form">
					<div class="custom-modern-textbox add-modern-arrow area mg-bottom-10">
						<input data-search="area" id="selectArea" class="select-area" placeholder="地域を選択" type="text" data-popup="">
					</div>

					<div class="custom-modern-select mg-bottom-10">
						<select name="em" id="em" data-search="em">
							<option value="">雇用形態を選択</option>
							<?php foreach ($employment_data as $k=>$v): ?>
								<option value="<?php echo $k; ?>">
									<?php echo $v; ?>
								</option>
								<?php endforeach; ?>
						</select>
					</div>
					<div class="text-center mg-bottom-10 lh-1">
						<strong class="_h2 color-df-white">＋</strong>
					</div>
					<div class="custom-modern-textbox mg-bottom-10">
						<input data-search="keyword" placeholder="フリーキーワード" type="text" name="keyword">
					</div>
					<div class="text-center mg-bottom-10 lh-1">
						<span class="arrow-b-green"></span>
					</div>
					<button type="submit" class="btn-shadown-green">求人を検索する</button>




					<input id="" class="result-area" type="hidden" name="area">
					<input type="hidden" class="data-em" name="data_em" value="0">
					<input type="hidden" class="data-step" name="data_step" value="0">
					<input type="hidden" class="data-group" name="data_group" value="0">
					<input type="hidden" class="data-province" name="data_province" value="0">
					<input type="hidden" class="data-city" name="data_city" value="0">
					<input type="hidden" class="data-town" name="data_town" value="0">
					<input type="hidden" class="event-before-open-popup" value="">
					<input type="hidden" class="event-after-close-popup" value="">
				</form>
			</div>
			<div class="pull-right">
				<div class="text-center all-middle mg-bottom-10">
					<span class="icon-circle-plus"></span>
					<span class="_h2 color-df-white">地域を指定して検索</span>
				</div>
				<div class="padding-l-40">
					<div class="panel-trans panel-shadown padding-lr-40 padding-tb-1">
						<div class="mg-top-20"></div>
						<?php foreach ($search_area_group as $group): ?>
							<div class="area-ref">
								<a class="area-ref__th" href="/search/<?php echo $group['area']['name'] ?>"><?php echo $group['area']['name'] ?></a>
								<div class="area-ref__md">
									<span class="arrow-r-green"></span>
								</div>
								<div class="area-ref__tr">
									<div class="area-ref__tr__wp">
										<?php 
										$start = $group['area']['range'][0];
										$end = $group['area']['range'][1];
										for($i=$start;$i<=$end;$i++)
										{
											echo '<a href="/search/'.$area_data[$i].'">'.$area_data[$i].'</a>';
										}
										?>
									</div>
								</div>
							</div>
						<?php endforeach; ?>
						<div class="mg-top-20"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php echo $this->element('Item/search_box_area_popup'); ?>