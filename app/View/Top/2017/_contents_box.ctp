<div class="container no-padding">
	<div class="padding-lr-40 padding-tb-1 ipad-mg-lr-15">
		<div class="text-center lh-1">
			<p class="_p mg-top-60">コールセンターのさまざまな情報を配信</p>
			<p class="_h1 mg-top-5">contents</p>
			<p class="mg-top-15 mg-bottom-40">
				<span class="short-dash"></span>
			</p>
		</div>
		<?php echo $this->element('../Top/2017/__mainList');?>
		
		<div class="text-center mg-bottom-40">
			<a href="/contents" class="btn-normal-gray">もっと見る</a>
		</div>
	</div>
</div>