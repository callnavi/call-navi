<div class="bg-secret">
	<div class="secret-content">
		
		<div class="background">
    		<div class="center-bg">
    			
    		</div>
    	</div>
    	<div class="benefits">
			<div><img src="/img/secret/secret-benefit.png"></div>
		</div>
		<div class="container main">
			<img class="img-title" src="/img/secret/secret-title.png"><br>
			<span>オリジナル求人はコールナビでしか見られない、採用特典付きお得な求人が満載☆</span><br>
			<a class="btn-more-secret" href="/secret"><img src="/img/secret/more-secret.png"></a>
		</div>
	</div>

	
</div>