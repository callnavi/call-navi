    <div class="bg-secret no-padding">
        <div class="container padding-lr-40 padding-tb-1 ipad-mg-lr-15">
          <div class="text-center lh-1">
            <p class="_p mg-top-60">コールナビでしか見れない限定の求人が満載！</p>
            <p class="_h1 mg-top-5">original</p>
            <p class="mg-top-15 mg-bottom-40"><span class="short-dash"></span></p>
          </div>
          <div class="main-list-col-4">
            <ol class="none-type clearfix">
            <?php
                foreach ($secret as $value){    
                $labels = explode(',',$value['CareerOpportunity']['employment']);
                
                $corporate = $this->App->PictureGetTop($value['CareerOpportunity']['company_id']);
                
                if(empty($value['CareerOpportunity']['corporate_logo']))
                {
                    $pic_url = '/img/instead/img_no-image_' . sprintf("%02d", rand(1, 10)) . '.jpg';
                }
                else
                {
                    $pic_url = $this->webroot."upload/secret/jobs/".$value['CareerOpportunity']['corporate_logo'];
                }
                $pic_error_url = '/img/instead/img_no-image_' . sprintf("%02d", rand(1, 10)) . '.jpg';
                
                
                    
                $href = $this->App->TopinterviewLink($value['CareerOpportunity']);
                    
                $href_cat = '/secret/'.$labels[0];
            ?>
              <li class="mg-bottom-30">
                <div class="verticle_art_box">
                  <div class="verticle_art_box--cat"><span class="i-cat-green--secondary"><?php echo($labels[0]);?></span></div><a href="<?php echo($href); ?>" title="">
                    <div class="mark-image">
                      <div class="mark-image--wp fix-height-160"><img src="<?php echo $pic_url; ?>" alt="<?php echo $title; ?>" onerror="this.onerror=null;this.src='<?php echo $pic_error_url; ?>';" alt=""></div>
                    </div>
                    <div class="verticle_art_box--title--white mg-top-5"><span><?php echo($value['CareerOpportunity']['job']);?></span></div></a>
                  <div class="verticle_art_box--bottom clearfix mg-top-5">
                    <dl class="_art_box--detail">
                      <dt class="_art_box--detail__title"><span>給与</span></dt>
                      <dd class="_art_box--detail__text"><?php echo(strip_tags($value['CareerOpportunity']['hourly_wage']));?></dd>
                    </dl>
                    <dl class="_art_box--detail">
                      <dt class="_art_box--detail__title"><span>勤務地</span></dt>
                      <dd class="_art_box--detail__text"><?php echo(strip_tags($value['CareerOpportunity']['work_location']));?></dd>
                    </dl>
                    <dl class="_art_box--detail">
                      <dt class="_art_box--detail__title"><span>条件</span></dt>
                      <dd class="_art_box--detail__text"><?php echo($value['CareerOpportunity']['work_features']);?></dd>
                    </dl>
                  </div>
                </div>
              </li>
            <?php
                }
            ?>
 
            </ol>
          </div>
          <div class="text-center mg-bottom-50"><a class="btn-more-secret" href="./secret"><img src="./img/secret/more-secret.png"></a></div>
          <div class="text-center mg-bottom-100"><a class="btn-more-secret" href="./oiwaikin"><img src="./img/oiwaikin/bnr_top_pc.png" alt="【お祝い金】最大90,000円 採用が決まればプレゼント！"></a></div>
        </div>
      </div>