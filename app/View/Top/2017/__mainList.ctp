<div class="main-list-col-4">
	<ol class="none-type clearfix">
	<?php foreach ($list as $item): ?>
		<?php
			$type 	= $item['list']['type'];
			$title 	= $item['list']['title'];
			$org_title 	= $item['list']['title'];
			$image 	= $item['list']['image'];
			$view 	= $item['list']['view'];
			$isNew 	= $item['list']['totalHour']<= 3;

			$dt = new DateTime($item['list']['created']);
			$createDate = $dt->format('Y年m月d日');

			if(mb_strlen($title) > 28)
			{
				$title = mb_substr($title,0,28,'UTF-8').'...';
			}

			$cateList = [];
			$cateAias  = explode(',', $item['list']['cat_alias']);
			$cateNames = explode(',', $item['list']['cat_names']);
			$firstCateAlias = $cateAias[0];

			$idUrl = $item['list']['id'];
			$href = '#';
			switch($type)
			{
				case 'news':
					//MAKE IMAGE LINK
					if (!empty($image)){
						$pos = strpos($image, "http://");
						if($pos === false)
						{
							$image = '/upload/news/'.$image;
						}
						else
						{
							$image = $this->app->proxy_image($image);
						}
					}

					//MAKE CATEGORY
					foreach( $cateNames as $i=>$cate)
					{
						if(isset($cateAias[$i]))
						{
							$cateList[] = array(
								'href' => "/$type/".$cateAias[$i],
								'name' => $cate
							);
						}
					}
					break;
				case 'blog':
					//MAKE IMAGE LINK
					if (!empty($image)){
						$image = $this->webroot."upload/blogs/".$image;
					}

					//Make category
					$cateList[] = array(
								'href' => "/$type/",
								'name' => 'CCブログ'
					);
					break;
				case 'contents':
					$idUrl = $item['list']['title_alias'];

					//MAKE IMAGE LINK
					if (!empty($image)){
						$pos = strpos($image, "ccwork");
						if($pos === false)
						{
							$image= '/upload/contents/'.$image;
						}							
					}
					else
					{
						$image = '/img/instead/img_no-image_' . sprintf("%02d", rand(1, 10)) . '.jpg';
					}

					//MAKE CATEGORY
					foreach( $cateNames as $i=>$cate)
					{
						if(isset($cateAias[$i]))
						{
							$cateList[] = array(
								'href' => "/$type/".$cateAias[$i],
								'name' => $cate
							);
						}
					}
//					//Make category
//					$cateList[] = array(
//								'href' => "/$type/",
//								'name' => '特集'
//					);

                    $imgLink = "";
                    if($item['list']['secret_job_id'] == '' || $item['list']['secret_job_id'] == '0'){
                        $href = $this->App->createContentDetailHref($item['list']);
                    }else{
                        $href = $this->App->createContentInterDetailHref($item['list']);  
                        
                        $imgLink = $this->App->jobPR($item['list']['secret_job_id']);
                        if(isset($imgLink['CareerOpportunity']['corporate_logo'])){
                            $image = "/upload/secret/jobs/".$imgLink['CareerOpportunity']['corporate_logo'];        
                        }else{
                            $image = "/upload/secret/jobs/";
                        }
                    
                    }
					break;
			}

			//MAKE HREF
			if($type != 'contents')
			{
				$href = "/$type/$firstCateAlias/$idUrl";
			}
			

		
		?>
		<li class="mg-bottom-40">
			<?php echo $this->element('Item/item_vertical_art_box', array(
				'href' => $href,
				'org_title' => $org_title,
				'image' => $image,
				'view' => $view,
				'createDate' => $createDate,
				'isNew' => $isNew,
				'cateList' => $cateList,
				'title' => $title,
			)); ?>
		</li>
		<?php endforeach; ?>
	</ol>	
</div>