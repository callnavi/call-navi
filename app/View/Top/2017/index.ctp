<?php 
$this->set('title', Configure::read('webconfig.web_title_default'));
$this->start('meta');
    //meta description keywords image
    echo Configure::read('webconfig.meta_description');
    echo Configure::read('webconfig.meta_keywords');
    echo Configure::read('webconfig.apple_touch_icon_precomposed');
    echo Configure::read('webconfig.ogp');  
$this->end('meta');
?>




<?php echo $this->element('../Top/2017/_top_banner');?>
<?php echo $this->element('../Top/2017/_secret_banner');?>
<?php echo $this->element('../Top/2017/_secret_contents');?>
<?php echo $this->element('../Top/2017/_contents_box');?>

<?php
//change layout of top page
//echo $this->element('../Top/2017/_secret_box');
?>
<?php echo $this->element('../Top/2017/_special_feature_box');?>

<?php echo $this->element('../Top/2017/_footer_banner_list');?>
<?php echo $this->element('../Top/2017/_concierge_box');?>