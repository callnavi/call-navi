<?php 
	$arrSpecialFeature = Configure::read('webconfig.special_feature');
?>

<div class="container no-padding">
	<div class="padding-lr-40 padding-tb-1 mg-bottom-60">
		<div class="text-center lh-1">
			<p class="_p mg-top-60">特集記事</p>
			<p class="_h1 mg-top-5">special feature</p>
			<p class="mg-top-15 mg-bottom-40">
				<span class="short-dash"></span>
			</p>
		</div>
		
		<div class="special-feature">
			<ol class="none-type clearfix">
				<?php foreach ($arrSpecialFeature as $specialFeature): ?>
				<li class="mg-bottom-30">
					<div class="verticle_art_box">
						<a href="<?php echo $specialFeature['url']; ?>">
							<div class="mark-image">
								<div class="mark-image--wp fix-height-210">
									<img src="/img/special_feature/<?php echo $specialFeature['img']; ?>">
								</div>
							</div>
					    </a>
					</div>
				</li>
				<?php endforeach; ?>	
			</ol>	
		</div>
	</div>
</div>