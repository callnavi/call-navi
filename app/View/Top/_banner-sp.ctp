<?php 
	//-------
	$area_data = Configure::read('webconfig.area_data');
	$employment_data = Configure::read('webconfig.employment_data');

	$this->start('script');
?>
<script>
	$( document ).ready(function() {
	    $("#selectEm").change(function () {
			$('#em').val($("#selectEm").val());
		});

		$("#em").change(function () {
			$('#selectEm').val($("#em").val());
		});
	});
</script>
<?php
	$this->end('script');
?>
<div class="banner">
	<div class="background">
		<img src="/img/banner/sp_banner.jpg" alt="">
	</div>
	<div class="top-search df-padding">
		<h2>“働きやすいコールセンター”</h2>
		<p class="text-right">が探せる求人サイト</p>
		<form action="/search" method="GET" data-seo="submit">
			<div class="display-table">
				<div class="table-cell">
					<div class="custom-text-box">
						<input data-search="area" readonly="readonly" type="text" id="selectArea" name="" placeholder="地域を選択" class="text_field select-area" value="" >
					</div>
					<input id="" class="result-area" type="hidden" name="area" value="">
					<input type="hidden" class="data-em" name="data_em" value="0">
					<input type="hidden" class="data-step" name="data_step" value="0">
					<input type="hidden" class="data-group" name="data_group" value="0">
					<input type="hidden" class="data-province" name="data_province" value="0">
					<input type="hidden" class="data-city" name="data_city" value="0">
					<input type="hidden" class="data-town" name="data_town" value="0">
					<input type="hidden" class="event-before-open-popup" value="">
					<input type="hidden" class="event-after-close-popup" value="">
				</div>
				<div class="table-cell multiply-wp">
					<span class="multiply"></span>
				</div>
				<div class="table-cell">
					<div class="custom-text-box">
						<input data-search="em" readonly="readonly" type="text" id="em" name="" placeholder="雇用形態を選択" class="text_field select-em" value="" >
					</div>
				</div>
			</div>
			<div class="button-select-group">
				<button type="submit" class="btn-long-square">
				SEARCH　/　コールセンター求人
				<span class="icon-search"></span>
				</button>
			</div>
		</form>
		<div class="bottom-pr">
			<h3>コールセンター求人数 <big>12,193</big> 件</h3>
		</div>
	</div>
</div>