<?php $this->layout = 'single_column_v2_sp'; ?>
<?php
//-------
echo $this->Html->css('top-v2-sp');

$this->set('title', Configure::read('webconfig.web_title_default'));
$this->start('meta');
//meta description keywords image
echo Configure::read('webconfig.meta_description');
echo Configure::read('webconfig.meta_keywords');
echo Configure::read('webconfig.apple_touch_icon_precomposed');
echo Configure::read('webconfig.ogp');
$this->end('meta');
?>
<?php echo $this->element('../Top/_banner-sp'); ?>

    <div class="gray-border"></div>

<?php echo $this->element('Item/concierge_link_banner_sp');?>
<?php
// comment in 06-10-2017 issues#345
//echo $this->element('../Top/2017/_secret_banner-sp'); 
?>
<div class="secret-banner-advertisement mg-top-40 mg-bottom-40"><a href="./oiwaikin"><img src="./img/oiwaikin/bnr_top_sp.png" alt="【お祝い金】最大90,000円 採用が決まればプレゼント！"></a></div>
    <?php echo $this->element('../Top/_mainsecret-sp'); ?>
    <div class="plaid-topic df-padding mg-top-70">
        <span>総合トップ</span>
    </div>
<?php echo $this->element('../Top/_mainList-sp'); ?>

    <div class="show-more df-padding mg-top-40">
        <a href="/contents" class="btn-blue-3d">もっと見る</a>
    </div>

    <div class="secret-banner df-padding">
        <div class="display-table">
            <div class="banner-image table-cell">
                <img src="/img/logo-obi-sp.png" alt="">
            </div>
            <div class="banner-slogan table-cell">
                <strong>オリジナル求人は応募特典付き！</strong>
                <span>オリジナル求人はコールナビでしか見られない、採用特典付きお得な求人が満載☆</span>
            </div>
        </div>
    </div>
<?php echo $this->element('../Top/_secret_slider-sp'); ?>
<?php echo $this->element('../Top/_social-sp'); ?>
