<div class="article-list">
	<ul class="fullcolor-list">
<?php 
    $catvalidate = false;
    foreach ($list as $item): ?>
	<?php
		$type 	= $item['list']['type'];
		$title 	= $item['list']['title'];
		$image 	= $item['list']['image'];
		$view 	= $item['list']['view'];
		$isNew 	= $item['list']['totalHour']<= 3;

		$dt = new DateTime($item['list']['created']);
		$createDate = $dt->format('Y年m月d日');

        
        
		$title = str_replace("\r\n","", $title);
		if(mb_strlen($title) > 35)
		{
			$title = mb_substr($title,0,35,'UTF-8').'...';
		}
		
		$cateList = [];
		$cateAias  = explode(',', $item['list']['cat_alias']);
		$cateNames = explode(',', $item['list']['cat_names']);
		$firstCateAlias = $cateAias[0];

		$idUrl = $item['list']['id'];
		$href = '#';
		switch($type)
		{
			case 'news':
				//MAKE IMAGE LINK
				if (!empty($image)){
					$pos = strpos($image, "http://");
					if($pos === false)
					{
						$image = '/upload/news/'.$image;
					}
					else
					{
						$image = $this->app->proxy_image($image);
					}
				}
				
				//Make Category
				foreach( $cateNames as $i=>$cate)
				{
					if(isset($cateAias[$i]))
					{
						$cateList[] = array(
							'href' => "/$type/".$cateAias[$i],
							'name' => $cate
						);
					}
				}
				break;
			case 'blog':
				//MAKE IMAGE LINK
				if (!empty($image)){
					$image = $this->webroot."upload/blogs/".$image;
				}
				
				//Make category
				$cateList[] = array(
							'href' => "/$type/",
							'name' => 'CCブログ'
				);
				break;
			case 'contents':
				$idUrl = $item['list']['title_alias'];
				
				//MAKE IMAGE LINK
				if (!empty($image)){
					$pos = strpos($image, "ccwork");
					if($pos === false)
					{
						$image= '/upload/contents/'.$image;
					}							
				}

				//Make Category
				foreach( $cateNames as $i=>$cate)
				{
					if(isset($cateAias[$i]))
					{
                        if($cate == '企業インタビュー'){
                            $catvalidate = true;
                            $href = "secret/".$org_title;
                        }
						$cateList[] = array(
							'href' => "/$type/".$cateAias[$i],
							'name' => $cate
						);
					}
				}
				$href = $this->App->createContentDetailHref($item['list']);
				break;
		}

		//MAKE HREF
		if($type != 'contents')
		{
			$href = "/$type/$firstCateAlias/$idUrl";
		}
		
	?>
	<li class="df-padding">
			<div class="article-box">
				<div class="display-table">
					<div class="search-image">
						<a href="<?php echo $href; ?>">
							<img src="<?php echo $image; ?>">
						</a>
					</div>

					<div class="search-content">
						<div class="top-part clearfix">
							<div class="pull-left">
								<time><?php echo $createDate; ?></time>
								<?php if($isNew){?>
									<span class="new-label">NEW</span>
								<?php } ?>
							</div>
							<div class="pull-right" style="
                            <?php foreach ($cateList as $cat):
                                $catvalidate = false;
                                if($cat['name'] == '企業インタビュー'){
                                    $catvalidate = true;
                                    $href = "secret/".$org_title;
                                }
                            endforeach; ?>
                            <?php if($catvalidate == true){ echo('display:none;'); } ?>">
								<big><?php echo $view; ?></big> view
							</div>
						</div>
						<div class="middle-part">
							<a href="<?php echo $href; ?>"><?php echo $title; ?></a>
						</div>
						<div class="bottom-part text-right">
							<?php foreach ($cateList as $cat): ?>
							<a href="<?php echo $cat['href']; ?>"><?php echo $cat['name']; ?></a>
							<?php endforeach; ?>
						</div>
					</div>
				</div>
			</div>
		</li>
<?php endforeach; ?>
	</ul>
</div>