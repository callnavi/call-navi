<?php 
	$secret_result = $this->requestAction('elements/secret_slider');
?>
<?php $this->append('script'); ?>
<link rel="stylesheet" href="/js/owl.carousel/assets/owl.carousel.min.css">
<script src="/js/owl.carousel/owl.carousel.min.js"></script>
<script type="text/javascript">
$(function() {
	$("#slider-mode").owlCarousel({
      nav : true, // Show next and prev buttons
	  navText : ["",""],
	  rewindNav : true,
	  slideSpeed : 300,
      paginationSpeed : 400,
	  autoPlay: true,
	  itemsMobile : true,
	  items : 1, 
	  loop: true,
  });
});
</script>
<?php $this->end('fetch'); ?>

<style>
	
</style>

<div class="secret-slider df-padding" style="height:336px;">	
	<div class="slider-content">
		<div id="secretSlider" class="slider-wp">
			<div id="slider-mode" class="slides owl-carousel">
			<?php foreach ($secret_result as $secret): ?>
					<?php 
						$value 			= $secret['CareerOpportunity'];
						//$href 			= $this->Html->url(array('controller' => 'secret', 'action' => 'detail',$value['id']), true);
						$href 			= $this->app->createSecretDetailHref($value);
						$image 			= $this->webroot."upload/secret/jobs/".$secret['CareerOpportunity']['corporate_logo'];
						
						$companyName 	= $secret['CareerOpportunity']['branch_name'];
						$employment 	= $value['employment'];
						$salary 		= strip_tags($value['salary']);
						$job			= $value['job'];
						
						if(mb_strlen($job)> 20) 
						{	
							$job = mb_substr($job,0,20,'UTF-8').'...';
						}
					
						if(mb_strlen($salary)> 20) 
						{	
							$salary = mb_substr($salary,0,20,'UTF-8').'...';
						}
					
						if(mb_strlen($companyName)> 20) 
						{	
							$companyName = mb_substr($companyName,0,20,'UTF-8').'...';
						}
					
						$employmentArr = explode(',',$employment);
				?>
				
				<div class="item">
					<div class="article-box article-secret-box">
						<div class="display-table">
							<div class="search-image">
								<a href="<?php echo $href; ?>">
									<img data-u="image" src="<?php echo $image; ?>">
								</a>
							</div>

							<div class="search-content">
								<div class="top-part">
									<span><?php echo $companyName; ?></span>
								</div>
								<div class="middle-part">
									<a href="<?php echo $href; ?>">
										<div>
											職種：<?php echo $job; ?>
										</div>
										<div class="mg-top-20">
											給与：<span class="i-salary"><?php echo $salary; ?></span>
										</div>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php endforeach; ?>
			</div>
       	</div>
	</div>
</div>