<?php 
	$this->set('title', Configure::read('webconfig.web_title_default'));
	$this->start('script');
	$this->app->echoScriptScrollSidebarJs();
	echo $this->Html->script('top.js');
	$this->end('script');

$this->start('meta');
    //meta description keywords image
    echo Configure::read('webconfig.meta_description');
    echo Configure::read('webconfig.meta_keywords');
    echo Configure::read('webconfig.apple_touch_icon_precomposed');
    echo Configure::read('webconfig.ogp');  
$this->end('meta');
?>
<div id="left-column">
	<div class="list-header mg-top-30">
		<span>特集記事</span>
	</div>

	<?php  echo $this->element('../Top/_mainList'); ?>
	<div style="position: absolute;bottom: 0;" id="stopScrollPoint"></div>
	
	<div class="list-bottom">
<!--		<a href="/contents" class="btn-green-gardient">もっと見る</a>-->
		<a href="/contents" class="btn-green-color">もっと見る ＞</a>
	</div>
</div>