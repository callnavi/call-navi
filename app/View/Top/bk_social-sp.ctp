<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.7&appId=1579537585628485";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div class="social-box df-padding">
	
	<div class="fb-column">
		<div class="display-table">
			<div class="table-cell image-part">
				<img src="/img/social-sp-image.jpg">
			</div>
			<div class="table-cell content-part">
				<div class="i-title">コールナビに「いいね」して 最新情報をGET☆</div>
				<div class="i-desc">コールセンター求人数日本No.1サイト、コールナビ。最新の求人情報とお役立ち記事が満載！</div>

				<div class="fb-like" data-href="https://callnavi.jp" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
		</div>
		</div>
		
	</div>
	
	<div class="twitter-button clearfix mg-top-40">
       「コールナビ」Twitterアカウント
        <a  class="pull-right"
        	target="_blank" 
        	href="https://twitter.com/intent/tweet?url=https://callnavi.jp/&via=コールナビ&text=コールセンター求人数日本No.1サイト、コールナビ。最新の求人情報とお役立ち記事が満載！&related=コールナビ">
        </a>
		<script>
			! function (d, s, id) {
				var js, fjs = d.getElementsByTagName(s)[0],
					p = /^http:/.test(d.location) ? 'http' : 'https';
				if (!d.getElementById(id)) {
					js = d.createElement(s);
					js.id = id;
					js.src = p + '://platform.twitter.com/widgets.js';
					fjs.parentNode.insertBefore(js, fjs);
				}
			}(document, 'script', 'twitter-wjs');
		</script>
	</div>
	
	
</div>