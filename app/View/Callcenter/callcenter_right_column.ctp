<?php
    $breadrumTitleList = Configure::read('webconfig.callcenter_area_group');
?>
<div class="right-breadrum">
        <p><?php echo $breadrumTitleList[$currentArea[0]['area']['group']].'エリア'  ?></p>
</div>

<div class="right-area group-height-<?php echo $currentArea[0]['area']['group'];?> ">
    <img class="img-group-<?php echo $currentArea[0]['area']['group'];?>" src="/img/callcenter/group_area/group_<?php echo $currentArea[0]['area']['group'];?>.png" />

    <div class="callcenter-area">
        <div class="group-area group-<?php echo $currentArea[0]['area']['group'];?> right-group-<?php echo $currentArea[0]['area']['group'];?> ">
            <ul>
                <?php
                $count =0;
                foreach($groupAreaInMap as $groupArea){
                    $count++;
                    $groupArea = $groupArea['area'];
                    $href ='/callcenter_matome/area/'.$groupArea['alias'];
                    $active = $groupArea['name'] == $currentArea[0]['area']['name'] ? 'active' : null ;
                    ?>
                    <li class="pro-<?php  echo $groupArea['id'] ?>">
                        <a class="btn-area <?php echo $active;?>" href="<?php echo $href; ?>">
                            <span><?php echo str_replace(array('県','府'),'',$groupArea['name']); ?></span><?php
                            if ($groupArea['callcenter_amount']): ?>(<?php echo $groupArea['callcenter_amount']; ?>)<?php endif; ?>
                        </a>
                    </li>

                <?php } ?>
            </ul>
        </div>
    </div>


</div>

<div class="group-search">
    <div class="right-breadrum">
        <p>その他のエリア</p>
    </div>
    <div class="clearfix">
        <div class="pull-left custom-select">
            <select name="area" id="per">
                <?php
                foreach ($listGroupArea as $area):

                    $areaChecked = '';
                    if($currentArea[0]['area']['alias'] == $area['area']['alias'])
                    {
                        $areaChecked = 'selected';
                    }
                    ?>
                    <option <?php echo $areaChecked; ?> value="<?php echo $area['area']['alias']; ?>"><?php echo $area['area']['name']; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <button class="pull-right" id="btnFilterArea">エリア変更</button>
    </div>
</div>