<?php
    $this->set('title', $this->app->getWebTitle('callcenter_matome'));
    
    $this->start('css');
    echo $this->Html->css('callcenter-sp.css');
    $this->end('css');

$this->start('meta');
    //meta description keywords image
    echo Configure::read('webconfig.meta_description');
    echo Configure::read('webconfig.meta_keywords');
    echo Configure::read('webconfig.apple_touch_icon_precomposed');
    echo Configure::read('webconfig.ogp');  
$this->end('meta');


$groupName = [
	1 => '北海道・東北',
	2 => '関東',
	3 => '甲信越・北陸',
	4 => '東海',
	5 => '関西',
	6 => '中国・四国',
	7 => '九州・沖縄',
];
?>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-10 contentnew callcentersp-around callcenter-top">
        <div class="callcentersp-header">
            <div class="img">
                <img src="/img/callcenter_logo.png" />
            </div>
            <div class="title">
                <p>CallCenter Map</p>
                <h1>日本コールセンターまとめ</h1>
                <h2>あなたの住んでいる地域にはどんなコールセンターがあるのでしょうか。都道府県ごとに日本全国のコールセンターをまとめてご紹介！</h2>
            </div>
        </div>
        
        <div class="container">
            <div class="row croll-boxtop">
                <div class="col-md-4">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">                  
                        <?php foreach ($groupName as $groupKey => $name): $totalCC=0; ?>
                        <div class="panel panel-primary">
                           
                            <div class="panel-heading" role="tab" id="heading<?php echo $groupKey; ?>">
                                <h4 class="panel-title">
                            <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?php echo $groupKey; ?>" aria-expanded="false" aria-controls="collapse<?php echo $groupKey; ?>" class="collapsed" >
                            	<span id="grp_<?php echo $groupKey; ?>"><?php echo $name; ?></span><span class="glyphicon glyphicon-plus pull-right"  aria-hidden="true"></span>
                            </a>
                                </h4>
                            </div>
                            <div id="collapse<?php echo $groupKey; ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading<?php echo $groupKey; ?>" aria-expanded="false" style="height: 0px;">
                                <div class="panel-body">
                                   <?php foreach ($groupArea as $area): $area = $area['area'];?>
							            <?php if ($area['group'] == $groupKey): 
										  $totalCC +=$area['callcenter_amount']; 
							             ?>
                                    <div class="list-group"> 
                                        <a href="/callcenter_matome/area/<?php echo $area['alias']; ?>" class="list-group-item"> <span class="glyphicon glyphicon-link" aria-hidden="true"></span> <?php echo $area['name']; ?> <strong><?php if ($area['callcenter_amount']): ?>
									（<?php echo $area['callcenter_amount']; ?>）
									<?php endif; ?></strong></a>                   
                                    </div>
                                    <?php endif; ?>
                                    <?php endforeach; ?>
                                </div>
                                <script type="text/javascript">
						          document.getElementById('grp_<?php echo $groupKey; ?>').innerHTML = '<?php echo $name. '<span>（'.$totalCC.'）</span>'; ?>';
					            </script>
                            </div> 
                             
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="callcenter-pickup">
        <div class="pickup-title">
            <p>
            Pick-UP <span>コールセンター</span>
            </p>
        </div>
			<?php foreach ($pickup as $secret): ?>
           	<?php 
						$value 			= $secret['CareerOpportunity'];
						$link 			= $this->Html->url(array('controller' => 'secret', 'action' => 'detail',$value['id']), true);
						$image 			= $this->webroot."upload/secret/companies/".$secret['corporate_prs']['corporate_logo'];
						$companyName 	= $secret['corporate_prs']['company_name'];
						$salary 		= $value['salary'];
						$job			= $value['job'];
						$employmentArr = explode(',',$value['employment']);
						if(mb_strlen($job)> 60) 
						{	
							$job = mb_substr($job,0,60,'UTF-8').'...';
						}
					
						if(mb_strlen($salary)> 37) 
						{	
							$salary = mb_substr($salary,0,37,'UTF-8').'...';
						}
					
						if(mb_strlen($companyName)> 12) 
						{	
							$companyName = mb_substr($companyName,0,12,'UTF-8').'...';
						}
			?>
           	<div class="secret-box">
				<div class="secret-image">
					<a href="<?php echo $link;?>">
						<img src="<?php echo $image ;?>" />
					</a>
					<div class="secret-work secret-label">シークレット求人</div>
				</div>
				<div class="info">
					<div class="secret-title"><h2><a href="<?php echo $link;?>"> <?php echo $job; ?></a></h2></div>
					<div class="secret-foot clearfix">
						<div class="pull-left"><div class="secret-salary"><p><?php echo $salary ;?></p></div></div>
						<div class="pull-right">
							<div class="type">
								<?php foreach ($employmentArr as $em): ?>
									<span class="cachet-btn"><?php echo $em; ?></span>
								<?php endforeach; ?>
							</div>
							<div class="secret-company"><?php echo $companyName ;?></div>
						</div>
					</div>

				</div>
			</div>            
        <?php endforeach; ?>
        </div>

    </div>

    <div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 no-padding ranking">
        <?php  echo $this->element('slidersp');?>
    </div>