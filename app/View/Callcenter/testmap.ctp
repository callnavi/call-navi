<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Callnavi</title>
	<style>
		body{
			font-size: 12px;
		}
		.paginator{
			font-size: 16px;
		}
		table{
			width: 100%;
		}
		table tr td,
		table tr th{
			border: 1px solid gray;
		}
		table tr strong{
			color: cadetblue;
		}
	</style>
</head>
<body>
	<h1>Callcenter list</h1>
	<div class="paginator">
		<?php echo $this->Paginator->numbers(array('modulus' => 100)); ?>
	</div>
	<table>
		<tr>
			<th>Id</th>
			<th>Name</th>
			<th>Address</th>
			<th>by Name</th>
			<th>by Address</th>
		</tr>
		<?php foreach ($list as $item): ?>
		<?php $item=$item['Callcenter']; ?>
		<tr>
			<td><strong><?php echo $item['id'] ?></strong></td>
			<td><?php echo $item['company_name'] ?></td>
			<td><?php echo $item['address'] ?></td>
			<td>

				<iframe
				  width="100%"
				  height="200"
				  frameborder="0"
				  style="border:0" allowfullscreen=""
				  src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDHZAJtARj1Qr-RbMWtiqApSW7HheKf7qs&q=<?php echo $item['company_name'] ?>" allowfullscreen>
				</iframe>
			</td>
			<td>
				<iframe
				  width="100%"
				  height="200"
				  frameborder="0"
				  style="border:0" allowfullscreen=""
				  src="https://www.google.com/maps/embed/v1/place?key=AIzaSyDHZAJtARj1Qr-RbMWtiqApSW7HheKf7qs&q=<?php echo $item['address'] ?>" allowfullscreen>
				</iframe>
			</td>
		</tr>
		<?php endforeach; ?>
	</table>
	<div class="paginator">
		<?php echo $this->Paginator->numbers(array('modulus' => 100)); ?>
	</div>
</body>
</html>