<?php
$this->layout = 'single_column';
$this->start('script'); 
    // echo $this->Html->script('scroll-column.js');
   	// echo $this->Html->script('scroll-right.js');
	echo $this->Html->script('jquery.mCustomScrollbar.concat.min');
   	echo '<script src="https://maps.google.com/maps/api/js?sensor=false&libraries=geometry"></script>';
   	echo $this->Html->script('around-search.js');
	echo $this->Html->script('callcenter.js?v=1');
$this->end('script');

$this->start('css');
	echo $this->Html->css('jquery.mCustomScrollbar');
	echo $this->Html->css('callcenter.css');
$this->end('css');

$title = $detail['c']['company_name'];
$areaName = $detail['per']['area_name'];
$postionName = $areaName.$detail['city']['city_name'];

$breadcrumb = array(
	'page' => $postionName.'のコールセンター「'.$title.'」',
	'parent' => array(
		array('link'=> $this->webroot.'callcenter_matome/', 'title' => '日本コールセンターまとめ')
	)
	
);

$detailAddress = array(
	'address' => $detail['c']['address'],
	'city_id' => $detail['c']['city_id'],
	'name' => $detail['c']['company_name'],
	'lat' => $detail['c']['lat'],
	'lng' => $detail['c']['lng'],
);
$relatedAddresses = [];
foreach ($relatedCC as $record)
{
	$isInCity = 0;
	if($city_id == $record['c']['city_id'])
	{
		$isInCity = 1;
	}	
	$relatedAddresses[] = array(
		'address' => $record['c']['address'],
		'city_id' => $record['c']['city_id'],
		'name' => $record['c']['company_name'],
		'lat' => $record['c']['lat'],
		'lng' => $record['c']['lng'],
		'isInCity' => $isInCity,
	);
}
$relatedAddressesJson = json_encode($relatedAddresses);
$detailAddressJson = json_encode($detailAddress);
$this->set('title', $title);

$this->start('sub_footer');
	echo $this->element('Item/bottom_banner');
$this->end('sub_footer');
?>
<?php $this->start('sub_header'); ?>
<div class="no-padding container">
	<div class="row">
		<?php echo $this->element('Item/top_banner_version2');?>
	</div>
</div>
<?php $this->end('sub_header'); ?>

	<script type="text/javascript">
		var __origin = <?php echo $detailAddressJson; ?>;
		var __aroundAddress = <?php echo $relatedAddressesJson; ?>;
	</script>
	
	<?php
		$this->start('header_breadcrumb'); 
	?>
		<?php echo $this->element('Item/breadcrumb', $breadcrumb); ?>
	<?php
	$this->end('header_breadcrumb');
	?>
	
<div class="clearfix">
	<div id="maincolumn" class=" pull-left content callcenter-layout">
		<div class="list-header clearfix">
				<span class="callnavi-icon"></span>
				<h1>「<?php echo $title; ?>」周辺のコールセンター</h1>
		</div>
		<div class="callcenter-map">
			<div id="map_canvas" style="width: 100%; height: 350px"></div>
		</div>
		
		<div class="call-center-list relation-callcenter mCustomScrollbar">
			<ol>
				<?php $index = 1; ?>
				<?php foreach ($relatedCC as $cc): ?>	
					<?php if($cc['c']['city_id'] == $city_id): ?>
						<?php 
							$url = $cc['per']['area_alias'].'/city_'.$cc['c']['city_id'].'/c_'.$cc['c']['id'];
							$_title = $cc['c']['company_name'];
							$address = $cc['c']['address'];
							$_title = trim($_title);
						?>
						<li>
							<span><i><?php echo $index; ?></i></span>
							<div>
								<h4><a title="<?php echo $_title; ?>" href="/callcenter_matome/area/<?php echo $url; ?>"><?php echo $_title; ?></a> | <?php echo $address; ?></h4>
							</div>
						</li>
						<?php $index++; ?>
					<?php endif; ?>
				<?php endforeach; ?>
			</ol>
		</div>
	</div>

	<div class="pull-right slider callcenter-rightcolumn">
		<?php echo $this->element('../Callcenter/callcenter_right_column', array(
			'currentArea' => $currentArea,
			'groupAreaInMap' => $groupAreaInMap,
			'listGroupArea' =>$groupArea,
		)); ?>
	</div>
</div>