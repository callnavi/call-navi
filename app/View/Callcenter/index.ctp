<?php
$this->layout = 'single_column';
$this->set('title', $this->app->getWebTitle('callcenter_matome'));

$this->start('script'); 
    // echo $this->Html->script('scroll-column.js');
    // echo $this->Html->script('scroll-right.js');
	echo $this->Html->script('init-googlemap'); 
	echo '<script src="http://maps.google.com/maps/api/js?libraries=geometry&key='.Configure::read('webconfig.google_map_api_key').'"></script>';
$this->end('script');

$this->start('css');
	echo $this->Html->css('callcenter.css');
$this->end('css');

$this->start('meta');
    //meta description keywords image
    echo Configure::read('webconfig.meta_description');
    echo Configure::read('webconfig.meta_keywords');
    echo Configure::read('webconfig.apple_touch_icon_precomposed');
    echo Configure::read('webconfig.ogp');  
$this->end('meta');

$groupName = [
	1 => '北海道・東北',
	2 => '関東',
	3 => '甲信越・北陸',
	4 => '東海',
	5 => '関西',
	6 => '中国・四国',
	7 => '九州・沖縄',
];


$breadcrumb = array('page' => '日本コールセンターまとめ');


$this->start('sub_footer');
	echo $this->element('Item/bottom_banner');
$this->end('sub_footer');
?>

<?php $this->start('sub_header'); ?>
<div class="no-padding container">
	<div class="row">
		<?php echo $this->element('Item/top_banner_version2');?>
	</div>
</div>
<?php $this->end('sub_header'); ?>  	

<?php echo $this->element('Item/breadcrumb', $breadcrumb); ?>
<div class="clearfix callcenter">
	<div class="pull-left content">
		<div class="list-header clearfix">
			<span class="callnavi-icon"></span>
			<h1>日本コールセンターまとめ</h1>
		</div>
		<h2 class="des-header">あなたの住んでいる地域にはどんなコールセンターがあるのでしょうか。 <br />都道府県ごとに日本全国のコールセンターをまとめてご紹介！</h2>
	</div>
	<div class="pull-right slider">
	</div>
</div>
	
<div class="callcenter-top">
		<div class="callcenter-full-map">
			<img src="/img/callcenter/group_area/group_all.png" alt="">
		</div>
		<div class="callcenter-area">
				<?php foreach ($groupName as $groupKey => $name): ?>
					<div class="group-area group-<?php echo $groupKey; ?>">
						<ul>
						<?php foreach ($groupArea as $area): $area = $area['area'];?>
							<?php if ($area['group'] == $groupKey): ?>
							<li class="pro-<?php  echo $area['id'] ?>">
								<a class="btn-area" href="/callcenter_matome/area/<?php echo $area['alias']; ?>">
									<span><?php echo str_replace(array('県','府'),'',$area['name']); ?></span><?php if ($area['callcenter_amount']): ?>(<?php echo $area['callcenter_amount']; ?>)<?php endif; ?>
								</a>
							</li>
							<?php endif; ?>
						<?php endforeach; ?>
						</ul>
					</div>
				<?php endforeach; ?>
		</div>
</div>