<?php 
$this->start('script'); 
    echo $this->Html->script('scroll-column.js');
   	echo $this->Html->script('scroll-right.js');
$this->end('script');

$this->start('css');
	echo $this->Html->css('callcenter.css');
$this->end('css');

$title = $detail['c']['company_name'];
$address = $detail['c']['address'];
$id = $detail['c']['id'];
$areaName = $detail['per']['area_name'];
$postionName = $areaName.$detail['city']['city_name'];
$this->set('title', $title);



?>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 headerlink">
		<a href="<?php echo $this->webroot; ?>">トップ</a>　>　
		<a href="<?php echo $this->webroot; ?>callcenter_matome/">日本のコールセンターまとめ</a>　>　
		<a href="<?php echo $this->webroot; ?>callcenter_matome/area/<?php echo $area_alias; ?>/"><?php echo $areaName; ?>のコールセンター</a>　>　
		<a href="<?php echo $this->webroot; ?>callcenter_matome/area/<?php echo $area_alias; ?>/city_<?php echo $city_id; ?>/"><?php echo $postionName; ?>のコールセンター</a>　>　
		<?php echo $title; ?>
</div>


<div id="maincolumn" class="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-padding content callcenter-layout">	
	<div class="common-panel callcenter-header">
			<div class="title">
				<p>CallCenter Map</p>sdsa
				<h1><?php echo $postionName; ?><small>のコールセンター</small></h1>
			</div>
			<div class="intro">
				<h2>あなたの住んでいる地域にはどんなコールセンターがあるのでしょうか。
				<br>都道府県ごとに日本全国のコールセンターをまとめてご紹介！</h2>
			</div>
		</div>
	
	<div class="common-panel callcenter-detail">
		<div>
			<h2><?php echo $title; ?></h2>
		</div>
		<div class="callcenter-contact">
			<p><span>住所：</span><?php echo $address; ?></p>
			<p><span>TEL：</span><?php echo $detail['c']['tel']; ?></p>
			<p><span>HP：</span><a target="_blank" href="<?php echo $detail['c']['link']; ?>"><?php echo $detail['c']['link']; ?></a></p>
			<p><span>業務・業種：</span> <?php echo $detail['c']['business']; ?></p>
		</div>
		
		<div class="callcenter-map">
			<iframe width="100%" height="400" frameborder="0" style="border:0" allowfullscreen="" 
			 src="https://www.google.com/maps/embed/v1/place?key=AIzaSyArVpOmeiSwrvEC4kP-j_yKJ4yNtJA0Wqk&q=<?php echo $address; ?>"></iframe>
		</div>
		
		<div class="find-way-link"><a href="https://maps.google.com/maps?ll=<?php echo $detail['c']['lat'];?>,<?php echo $detail['c']['lng'];?>&z=16&t=m&hl=ja-JP&gl=JP&mapclient=embed&daddr=<?php echo $title; ?>+<?php echo $address; ?>" target="_blank">「<?php echo $title; ?>」までのルートを検索</a></div>
		
		<div class="relation-job">
			<div class="title">「<?php echo $title; ?>」で現在募集中の求人情報一覧</div>
			<ul>
				<?php foreach ($relatedJob as $job): ?>			
				<li>
					<p><a target="_blank" href="<?php echo $job['url']; ?>"><?php echo $job['title']; ?></a></p>
					<p><?php echo $job['salary']; ?></p>
					<p><span><b>提供元：</b></span> <?php echo $job['site_name']; ?></p>
				</li>
				<?php endforeach; ?>
            </ul>
		</div>
		<div class="find-around-link">			
			<a href="/callcenter_matome/area/<?php echo $area_alias; ?>/city_<?php echo $city_id; ?>/c_<?php echo $id; ?>/around-search" class="brick-btn">周辺のコールセンターを探す</a>
		</div>
	</div>
	
</div>