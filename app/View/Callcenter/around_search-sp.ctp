<?php
    $this->start('script'); 
   	echo '<script src="https://maps.google.com/maps/api/js?sensor=false&libraries=geometry"></script>';
   	echo $this->Html->script('around-search.js');
    $this->end('script');
    
    $this->start('css');
    echo $this->Html->css('callcenter-sp.css');
    $this->end('css');

$title = $detail['c']['company_name'];
$areaName = $detail['per']['area_name'];
$postionName = $areaName.$detail['city']['city_name'];


$detailAddress = array(
	'address' => $detail['c']['address'],
	'city_id' => $detail['c']['city_id'],
	'name' => $detail['c']['company_name'],
	'lat' => $detail['c']['lat'],
	'lng' => $detail['c']['lng'],
);
$relatedAddresses = [];
foreach ($relatedCC as $record)
{
	$isInCity = 0;
	if($city_id == $record['c']['city_id'])
	{
		$isInCity = 1;
	}	
	$relatedAddresses[] = array(
		'address' => $record['c']['address'],
		'city_id' => $record['c']['city_id'],
		'name' => $record['c']['company_name'],
		'lat' => $record['c']['lat'],
		'lng' => $record['c']['lng'],
		'isInCity' => $isInCity,
	);
}
$relatedAddressesJson = json_encode($relatedAddresses);
$detailAddressJson = json_encode($detailAddress);

$this->set('title', $title);
?>
	<script type="text/javascript">
		var __origin = <?php echo $detailAddressJson; ?>;
		var __aroundAddress = <?php echo $relatedAddressesJson; ?>;
	</script>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-10 contentnew callcentersp-around">
     <div class="callcentersp-header">
       <div class="img">
           <img src="/img/callcenter_logo.png"/>
       </div>
        <div class="title">
           
            <p>CallCenter Map</p>
            <h1><?php echo $postionName; ?></h1>
            <h2>コールセンターまとめ</h2>
        </div>
    </div>
    <div class="commonsp-panel">
		
		<div class="callcebter-toptic">
			<h2>「<?php echo $title; ?>」周辺のコールセンター一覧</h2>
		</div>
        <div class="callcenter-map">
            <div id="map_canvas" style="width: 100%;"></div>
        </div>
        <div class="call-center-list relation-callcenter">
			<ol>
		    	<?php $index = 1; ?>
			    <?php foreach ($relatedCC as $cc): ?>
			    	<?php if($cc['c']['city_id'] == $city_id): ?>
						<?php 
							$url = $cc['per']['area_alias'].'/city_'.$cc['c']['city_id'].'/c_'.$cc['c']['id'];
							$title = $cc['c']['company_name'];
							$address = $cc['c']['address'];
							$title = trim($title);
						?>
						<li>
							<span><i><?php echo $index; ?></i></span>
							<div>
								<h4><a href="/callcenter_matome/area/<?php echo $url; ?>"><?php echo $title; ?></a></h4>
								<p><?php echo $address; ?></p>
							</div>
						</li>
						<?php $index++; ?>
					<?php endif; ?>
				<?php endforeach; ?>
			</ol>
		</div>
    </div>

</div>