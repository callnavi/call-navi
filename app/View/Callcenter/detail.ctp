<?php 
$this->start('script'); 
    // echo $this->Html->script('scroll-column.js');
   	// echo $this->Html->script('scroll-right.js');
	echo $this->Html->script('callcenter.js?v=1');
	echo $this->Html->script('init-googlemap'); 
?>
	<script src="https://maps.google.com/maps/api/js?libraries=geometry&key=<?php
		echo Configure::read('webconfig.google_map_api_key'); ?>"></script>
<?php $this->end('script');

$this->start('css');
	echo $this->Html->css('callcenter.css');
$this->end('css');
$this->start('meta');
	//meta description keywords image
	echo Configure::read('webconfig.meta_description');
	echo Configure::read('webconfig.meta_keywords');
	echo Configure::read('webconfig.apple_touch_icon_precomposed');
	echo Configure::read('webconfig.ogp');
$this->end('meta');

$this->start('sub_footer');
	//comment in 06/10/2017 issues#345
	//echo $this->element('Item/bottom_banner');
$this->end('sub_footer');

$title = $detail['c']['company_name'];
$address = $detail['c']['address'];
$id = $detail['c']['id'];
$areaName = $detail['per']['area_name'];
$postionName = $areaName.$detail['city']['city_name'];
$this->set('title', $title);

$this->start('sub_header'); ?>
<div class="no-padding container">
	<div class="row">
		<?php echo $this->element('Item/top_banner_version2');?>
	</div>
</div>
<?php $this->end('sub_header');
	$breadcrumb = array(
		'parent' =>array(
			0 => array(
				'title'=> '日本コールセンターまとめ',
				'link' => '/callcenter_matome/',
			),
			1 => array(
				'title'=> $areaName.'のコールセンター',
				'link' => '/callcenter_matome/area/'.$area_alias,
			),
			2 => array(
				'title'=> $postionName.'のコールセンター',
				'link' => '/callcenter_matome/area/'.$area_alias.'/city_'.$city_id,
			)
		),
		'page' => $detail['c']['company_name']);
	$this->start('header_breadcrumb'); 
	echo $this->element('Item/breadcrumb', $breadcrumb);
?>
<?php $this->end('header_breadcrumb');

?>
<div class="clearfix">
	<div id="maincolumn" class=" pull-left content callcenter-layout">

		<div class="callcenter-detail">
			<div class="list-header clearfix keyword-result">
				<span class="callnavi-icon"></span>
					<h1><?php echo $title; ?></h1>
				<section class="number-result">
					<p><span>業務・業種：</span> <?php echo $detail['c']['business']; ?></p>
				</section>
			</div>
			<div class="clearfix">
				<div class="pull-left callcenter-contact">
					<p class="pull-left"><span>住所：</span><?php echo $address; ?></p>
					<p class="pull-left clearfix" style="padding-right: 27px;"><span>TEL：</span><?php echo $detail['c']['tel']; ?></p>
					<p class="pull-left">
						<span>HP：</span><a target="_blank"
										   href="<?php echo $detail['c']['link']; ?>"><?php echo $detail['c']['link']; ?></a>
					</p>
				</div>
				<div class="pull-right find-around-link">
					<a href="https://maps.google.com/maps?ll=<?php echo $detail['c']['lat'];?>,<?php echo $detail['c']['lng'];?>&z=16&t=m&hl=ja-JP&gl=JP&mapclient=embed&daddr=<?php echo $title; ?>+<?php echo $address; ?>"
					   target="_blank" class="brick-btn">ルート検索   ▶︎</a>
				</div>
			</div>
			<div class="">
				<div id="map"></div>
			</div>
			<div class="clearfix">
				<div class="pull-right find-around-link">
					<a href="/callcenter_matome/area/<?php echo $area_alias; ?>/city_<?php echo $city_id; ?>/c_<?php echo $id; ?>/around-search"
						target="_blank" class="brick-btn">周辺のコールセンターを検索   ▶︎</a></div>
			</div>
			<div class="relation-job clearfix">
				<div class="title">「<?php echo $title; ?>」で現在募集中の求人情報一覧</div>
				<ul>
					<?php foreach ($relatedJob as $job): ?>
					<li>
						<?php echo $this->element('Item/search_box', $job); ?>
					</li>
					<?php endforeach; ?>
				</ul>
			</div>

		</div>

	</div>

	<div class="pull-right slider callcenter-rightcolumn">
		<?php echo $this->element('../Callcenter/callcenter_right_column', array(
			'currentArea' => $currentArea,
			'groupAreaInMap' => $groupAreaInMap,
			'listGroupArea' =>$groupArea,
		)); ?>
	</div>
</div>
<script>
	$( document ).ready(function() {
		// set google map
		// input lat, lng , inforwindow, zoom
		<?php $latLng = !empty($detail['c']['lat']) && !empty($detail['c']['lng']) ? $detail['c']['lat'].' , '.$detail['c']['lng'] : '0,0'; ?>
		var contentString = "<h4 style='font-weight: bold;'><?php echo $title; ?></h4><a href='<?php echo $detail['c']['link']; ?>' ><p><?php echo $detail['c']['link']; ?></p></a><p><?php echo $address; ?> </p> ";
		initMap( <?php echo $latLng;?>,contentString, 16   )

	});
</script>