<?php
    $this->start('script'); 
       	echo $this->Html->script('callcenter-sp.js');
    $this->end('script');   
    
    $this->start('css');
    echo $this->Html->css('callcenter-sp.css');
    $this->end('css');


$pageCount = ceil($areaTotal/$size);

$area = $currentArea[0]['area']['name'];
$city = '';
$cityId = 0;
if(isset($currentCity[0]['area']))
{
	$city = $currentCity[0]['area']['name'];
	$cityId = $currentCity[0]['area']['id'];
}

$title = $area.$city;
$this->set('title', $title);

?>
   	<input type="hidden" id="currentCityId" value="<?php echo $cityId; ?>">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-10 contentnew callcentersp-around callcenter-area">
        <div class="callcentersp-header">
            <div class="img">
                <img src="/img/callcenter_logo.png" />
            </div>
            <div class="title">

                <p>CallCenter Map</p>
                <h1><?php echo $title; ?></h1>
                <h2>コールセンターまとめ</h2>
            </div>
        </div>
        <div class="clearfix search-group">
                <div class="custom-select">

                    <select name="area" id="per">
                       <?php foreach ($groupArea as $area): ?>
                           <?php 
									$areaChecked = '';
									if($area_alias == $area['area']['alias'])
									{
										$areaChecked = 'selected';
									}
								?>
                        <option <?php echo $areaChecked; ?> value="<?php echo $area['area']['alias']; ?>"><?php echo $area['area']['name']; ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="custom-select">
                    <select name="em" id="city">
                        <option value="">市町村の選択</option>                        
                    </select>
                </div>           
        </div>
        <div class="clearfix search-result">
        	<p><?php echo $title; ?><span>エリア</span></p>
        	<label><span><?php echo $areaTotal; ?></span>件見つかりました</label>
        </div>
        <div id="maincolumn" class="common-panel callcenter-area-list">
            <ol>
               <?php $cityId = 0; ?>
						<?php foreach ($areaList as $cc): ?>
							
							<?php 
								$companyName = $cc['c']['company_name'];
								$address = $cc['c']['address'];
								$tel = $cc['c']['tel'];
								if(mb_strlen($companyName) > 45)
								{
									$companyName = mb_substr($companyName,0,45,'UTF-8').'...';
								}		

								$tel = trim($tel);
							?>    
                <li>
                    <?php if ($cityId != $cc['c']['city_id']): $cityId = $cc['c']['city_id'];?>
                   	<label><?php echo $cc['a']['name']; ?></label>
                   	<?php endif; ?>
                    <h4><a href="/callcenter_matome/area/<?php echo $area_alias; ?>/city_<?php echo $cityId; ?>/c_<?php echo $cc['c']['id']; ?>"><?php echo $companyName; ?></a></h4>
                    <p><?php echo $address; ?></p>
                    <p class="tel"><?php if ($tel): ?><span>TEL</span><?php echo $tel; ?><?php endif; ?></p>
                </li>
                <?php endforeach; ?>
                
            </ol>
            <?php echo $this->element('Item/pagination_sp', array('currentPage' => $page, 'pageCount' => $pageCount)); ?>
        </div>
    </div>
    <?php echo $this->element('../Top/_social-sp'); ?>