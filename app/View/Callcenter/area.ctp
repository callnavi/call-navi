<?php
$this->start('script'); 
    // echo $this->Html->script('scroll-column.js');
   	// echo $this->Html->script('scroll-right.js');
	echo $this->Html->script('jquery.mCustomScrollbar.concat.min');
   	echo $this->Html->script('callcenter.js');
$this->end('script');

$this->start('css');
	echo $this->Html->css('jquery.mCustomScrollbar');
	echo $this->Html->css('callcenter.css');
$this->end('css');

$this->start('meta');
	//meta description keywords image
	echo Configure::read('webconfig.meta_description');
	echo Configure::read('webconfig.meta_keywords');
	echo Configure::read('webconfig.apple_touch_icon_precomposed');
	echo Configure::read('webconfig.ogp');
$this->end('meta');

$this->start('sub_footer');
	echo $this->element('Item/bottom_banner');
$this->end('sub_footer');

//$pageCount = ceil($areaTotal/$size);

//CITY: 市町村区（北海道）
//PRE: 北海道
$area = $currentArea[0]['area']['name'];

$city = '';
$cityId = 0;
if(isset($currentCity[0]['area']))
{
	$city = $currentCity[0]['area']['name'];
	$cityId = $currentCity[0]['area']['id'];
}

$title = $area.$city;
$this->set('title', $title);

	$this->start('sub_header'); ?>
<div class="no-padding container">
	<div class="row">
		<?php echo $this->element('Item/top_banner_version2');?>
	</div>
</div>
<?php
	$this->end('sub_header');
	
	$this->start('header_breadcrumb'); 
	
	$breadcrumb = array('page' => $title.'コールセンターまとめ');
 	echo $this->element('Item/breadcrumb', $breadcrumb);

	$this->end('header_breadcrumb');
?>
<input type="hidden" id="currentCityId" value="<?php echo $cityId; ?>">
	<div class="clearfix">
		<div class="pull-left content callcenter-layout">
			<div class="list-header clearfix">
				<span class="callnavi-icon"></span>
				<h1><?php echo $title;?>コールセンターまとめ</h1>
			</div>
			<div class="list-sub-header">
				<p>あなたの住んでいる地域にはどんなコールセンターがあるのでしょうか。 都道府県ごとに日本全国のコールセンターをまとめてご紹介！</p>
			</div>
			<div class="call-center ">
				<div id="maincolumn" class="call-center-list mCustomScrollbar">
					<ol>
						<?php $cityId = 0; ?>
						<?php foreach ($areaList as $cc): ?>

							<?php
								$companyName = $cc['c']['company_name'];
								$address = $cc['c']['address'];
								$tel = $cc['c']['tel'];
								if(mb_strlen($companyName) > 45)
								{
									$companyName = mb_substr($companyName,0,45,'UTF-8').'...';
								}

								if(mb_strlen($address) > 32)
								{
									$address = mb_substr($address,0,32,'UTF-8').'...';
								}

								if(mb_strlen($tel) > 16)
								{
									$tel = mb_substr($tel,0,16,'UTF-8');
								}


							?>

							<li <?php echo $cityId != $cc['c']['city_id'] ? "class='first-row'" : null ;?> >
							<?php if ($cityId != $cc['c']['city_id']): $cityId = $cc['c']['city_id'];?>
								<div class="list-label group-<?php echo $currentArea[0]['area']['group'] ;?>">
									<a href="/callcenter_matome/area/<?php
									echo $area_alias ;?>/city_<?php echo $cityId; ?>"><?php echo $cc['a']['name']; ?></a>
								</div>
							<?php endif; ?>
								<div class="list-city">
									<h4><a href="/callcenter_matome/area/<?php echo $area_alias; ?>/city_<?php echo $cityId; ?>/c_<?php
										echo $cc['c']['id']; ?>"><?php echo $companyName; ?></a></h4>
									<p><?php echo $address; ?></p>
									<p style="call-phone">
										<?php if ($tel): ?>
										TEL <?php echo $tel; ?>
										<?php endif; ?>
									</p>
								</div>
							</li>
						<?php endforeach; ?>
					</ol>
				</div>
			</div>
		</div>
		<div class="pull-right slider callcenter-rightcolumn">
			<?php echo $this->element('../Callcenter/callcenter_right_column', array(
				'currentArea' => $currentArea,
				'groupAreaInMap' => $groupAreaInMap,
				'listGroupArea' =>$groupArea,
			)); ?>
		</div>
	</div>
</div>