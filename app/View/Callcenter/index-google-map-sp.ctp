<?php
$this->set('title', $this->app->getWebTitle('callcenter_matome'));
$this->start('meta');
echo '<meta name="description" content= "コールセンター・テレオペのアルバイト・バイト・求人情報を探すならコールナビ。勤務地や雇用形態、フリーワードといった様々な条件からバイト、正社員、派遣の求人情報が検索できます。コールセンター・テレオペのお仕事探しは採用実績が豊富なコールナビにお任せください！" >';
$this->end('meta');

$this->start('css');
echo $this->Html->css('jquery.mCustomScrollbar');
echo $this->Html->css('callcenter-google-map/callcenter-google-map-sp');
$this->end('css');

$this->start('script');
echo '<script src="https://maps.google.com/maps/api/js?sensor=false&libraries=geometry&key=AIzaSyDHZAJtARj1Qr-RbMWtiqApSW7HheKf7qs"></script>';

if (!empty($listCallcenter)) {

    $arrtpm = [];

    foreach ($listCallcenter as $callcenter) {
        $callcenter = $callcenter['Callcenter'];
        $arrtpm[] = array(
            'id' => $callcenter['id'],
            'address'=> $callcenter['address'],
            'tel'=>$callcenter['tel'],
            'hp'=>$callcenter['link'],
            'business' =>$callcenter['business'],
            'city_id' => $callcenter['id'],
            'name' => $callcenter['company_name'],
            'lat'=> $callcenter['lat'],
            'lng' =>$callcenter['lng'],
            'keywords' => $callcenter['keywords']);

    }
    //pr($arrtpm);die;
    $__aroundAddress = 'var __aroundAddress ='.json_encode($arrtpm);
} else {
    $__aroundAddress = 'var __aroundAddress = []';
}
$this->Html->scriptStart(array('inline' => false));
echo $__aroundAddress;
$this->Html->scriptEnd();

echo $this->Html->script('callcenter-google-map/jquery.line.js');
echo $this->Html->script('callcenter-google-map/callcenter-google-map-sp');
$this->end('script');
?>

<?php $this->start('script'); ?>
    <script type="text/javascript">
        $(document).ready(function() {
            <?php if(!empty($zoom) && $zoom): ?>
                $("html").css("zoom", "0.5");
            <?php endif; ?>
        });
    </script>
<?php $this->end('script'); ?>
<div class="google-map-wrapper">
    <div id="map_canvas" class="google-map"></div>
</div>


      <div class="callnavi-search-panel" id="callnavi-search-panel"  <?php 
           if (!empty($keySearch)){
             echo("style='display:none;'"); 
           }
           
           ?>  data-toggleSearchBox-target="target">
        <div class="panel-wp">
          <div class="panel-div padding">
            <form method="get" action="">
                <?php
                if (!empty($keySearch)){
                ?>
                    <div class="panel-result-txt"><span><i><?php echo count($listCallcenter); ?></i>件</span>のコールセンターが見つかりました</div>
                <?php
                }else{
                ?>
                
                <div class="panel-lead padding">
                    <h1 class="panel-lead-title">“日本コールセンターまとめ”</h1>
                    <p class="panel-lead-txt">あなたの住んでいる地域にはどんなコールセンターがあるのでしょうか。都道府県ごとに日本全国のコールセンターをまとめてご紹介！</p>
                </div>
                <?php
                }
                ?>

              <div class="padding mg-top-20 input-box">
                <input type="text" name="keySearch" value="<?php if (!empty($keySearch)) {echo($keySearch); } ?>"  placeholder="都道府県・市区町村を入力" data-txtClear-target="clear"><span class="clear-btn" data-txtClear-trigger="clear"><i></i><i></i></span>
              </div>
            </form>
          </div>
        </div>
      </div>

<div class="callnavi-search-panel-list">
		<div class="panel-list <?php echo !empty($keySearch) ? 'not-empty' : null; ?> ">
				<ul class="nav-horizontal__mask ul-callcenter-list">
					<?php
					$count = 0;
					if (!empty($listCallcenter)):
						foreach ($listCallcenter as $callcenter) {
							$count++;
							$callcenter = $callcenter['Callcenter'];
							$title = "「" .$callcenter['company_name']. "」\n" .$callcenter['address'];
							?>

							<li title="<?php echo $title ?>" data-index="<?php echo $count; ?>" data-id="<?php echo $callcenter['id'] ?>">
								 <div class="display-box">
                                     <div class="display-table">
                                         <div class="cc-count-label table-cell"><span class="green-circle"><?php echo $count; ?></span></div>
                                         <div class="callcenter-company table-cell" title="<?php echo $callcenter['address']; ?>">
                                            <div class="callcenter-company__inner"><?php echo $callcenter['company_name']; ?></div>
                                         </div>
                                        </div>
                                        <div class="callcenter-btn">詳細を確認する</div>
                                     
									
								 </div>
							</li>
						<?php }
					endif;
					?>
				</ul>
		</div>
</div>


<!--POP-UP-->
<?php $this->start('bottom'); ?>
<div class="top-popup-search">
      <div class="popup-container">
        <div class="clearfix"><span class="close-button"><i></i><i></i>close</span></div>
        <div class="banner-search-wrap-box" id="cc_detail">
          <div class="cc-pp-header">
            <div class="cc-pp-name" data-cc="company"></div>
            <div class="cc-pp-cat text-center">業務 / 業種 ：<i data-cc="cat">ネット回線事業</i></div>
          </div>
          <div class="cc-pp-content">
            <div class="cc-pp-item"><span class="cc-pp-item__address"><span>住所</span><a class="icon-access" href="#" data-cc="dir" target="_blank"><span class="icon-2ways"></span></a></span>
              <big data-cc="address"></big>
            </div>
            <div class="cc-pp-item"><span>TEL</span>
              <big data-cc="tel"></big>
            </div>
            <div class="cc-pp-item"><span>HP</span>
              <big data-cc="hp"><a href="#" target="_blank"></a></big>
            </div>
          </div>
          <!--.cc-pp-hr-->
          <!--    hr-->
          <div class="cc-pp-footer">
            <div class="cc-pp-footer_txt"><i></i>現在募集中の求人が<span class="green"><i data-cc="total">2</i>件</span></div>
            <form method="get" data-cc="search" action="/search" target="_blank">
              <input type="hidden" name="keyword" data-cc="keyword">
              <button class="btn-green" type="submit">求人情報を確認する</button>
            </form>
          </div>
        </div>
        
    </div>

</div>
<?php $this->end('bottom'); ?>

<?php //echo $this->element('sql_dump'); ?>