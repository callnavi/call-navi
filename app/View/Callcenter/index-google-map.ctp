<?php
$this->layout = 'callcenter_matome';
$this->set('title', $this->app->getWebTitle('callcenter_matome'));
$this->start('meta');
echo '<meta name="description" content= "コールセンター・テレオペのアルバイト・バイト・求人情報を探すならコールナビ。勤務地や雇用形態、フリーワードといった様々な条件からバイト、正社員、派遣の求人情報が検索できます。コールセンター・テレオペのお仕事探しは採用実績が豊富なコールナビにお任せください！" >';
$this->end('meta');

$this->start('css');
echo $this->Html->css('jquery.mCustomScrollbar');
echo $this->Html->css('callcenter-google-map/callcenter-google-map.css');
$this->end('css');

$this->start('script');
echo $this->Html->script('jquery.mCustomScrollbar.concat.min');
echo $this->Html->script('app_new');
echo '<script src="https://maps.google.com/maps/api/js?sensor=false&libraries=geometry&key=AIzaSyDHZAJtARj1Qr-RbMWtiqApSW7HheKf7qs"></script>';

if (!empty($listCallcenter)) {

    $arrtpm = [];
    foreach ($listCallcenter as $callcenter) {
        $callcenter = $callcenter['Callcenter'];
        $arrtpm[] = array(
            'id' => $callcenter['id'],
            'address'=> $callcenter['address'],
            'tel'=>$callcenter['tel'],
            'hp'=>$callcenter['link'],
            'business' =>$callcenter['business'],
            'city_id' => $callcenter['id'],
            'name' => $callcenter['company_name'],
            'lat'=> $callcenter['lat'],
            'lng' =>$callcenter['lng'],
            'keywords' => $callcenter['keywords']);

    }
    //pr($arrtpm);die;
    $__aroundAddress = 'var __aroundAddress ='.json_encode($arrtpm);
	$zoom =  $this->App->getZoomNumber($listCallcenter);
} else {
    $__aroundAddress = 'var __aroundAddress = []';
	$zoom =  $this->App->getZoomNumber(null);
}


$this->Html->scriptStart(array('inline' => false));
echo $__aroundAddress.';';
echo 'var zoom = '.$zoom.';';
$this->Html->scriptEnd();

echo $this->Html->script('callcenter-google-map/jquery.line.js');
echo $this->Html->script('callcenter-google-map/callcenter-google-map.js');
$this->end('script');
?>
<div class="google-map-wrapper">
    <div id="map_canvas" class="google-map"></div>
</div>

<div class="callnavi-search-menu" data-searchMenu-target="target">
<div class="panel-btn" data-searchMenu-trigger="trigger"><span>close</span></div>
<div class="callnavi-search-panel">
  <div class="panel-wp">
	<div class="panel-div">
	  <form method="get" action="">
		<div class="panel-input-box clearfix">
		  <div class="pull-left">
			<input type="search" name="keySearch" value="<?php if (!empty($keySearch)) {echo($keySearch); } ?>" placeholder="都道府県・市区町村を入力" data-txtClear-target="clear"><span class="clear-btn" data-txtClear-trigger="clear"><i></i><i></i></span>
		  </div>
		  <div class="text-right pull-right">
			<input class="input-submit-btn" type="submit" value="検索">
		  </div>
		</div>
	  </form>
	</div>
	
	<?php if (empty($keySearch)): ?>
	<div class="panel-list">
	  <div class="panel-lead-block">
		<h1>“日本コールセンターまとめ”</h1>
		<p>あなたの住んでいる地域にはどんなコールセンターがあるのでしょうか。都道府県ごとに日本全国のコールセンターをまとめてご紹介！</p>
	  </div>
	</div>
	<?php else: ?>
	<div class="panel-div">
	  <div class="panel-hit-result"><span><i><?php echo count($listCallcenter); ?></i>件</span>のコールセンターが見つかりました。</div>
	</div>
	<div class="panel-list mCustomScrollbar not-empty">
			<ul class="ul-callcenter-list">
				<?php
				$count = 0;

				if (!empty($listCallcenter)):
					foreach ($listCallcenter as $callcenter) {
						$count++;
						$callcenter = $callcenter['Callcenter'];
						$title = "「" .$callcenter['company_name']. "」\n" .$callcenter['address'];
						?>
						<li title="<?php echo $title ?>" data-index="<?php echo $count; ?>" data-id="<?php echo $callcenter['id'] ?>">
							<div class="display-table">
								<div class="table-cell">
									<span class="green-circle"><?php echo $count; ?></span>
								</div>
								<div class="table-cell">
									<section><?php echo $callcenter['company_name']; ?></section>
									<p><?php echo $callcenter['address']; ?></p>
                                    <div class="btn-green">詳細</div>
								</div>
							</div>
						</li>

					<?php }
				endif;
				?>
			</ul>
		</div>
	<?php endif; ?>
  </div>
</div>
</div>


<!-- POP-UP-->
<div class="top-popup-search">
<div class="popup-container">
  <div class="clearfix"><span class="close-button"><i></i><i></i>close</span></div>
  <div class="banner-search-wrap-box" id="cc_detail">
	<div class="cc-pp-header">
	  <div class="cc-pp-name" data-cc="company"></div>
	  <div class="cc-pp-cat">業務 / 業種 ：<i data-cc="cat">ネット回線事業</i></div>
	</div>
	<div class="cc-pp-content clearfix">
	  <div class="pull-left">
		<div class="cc-pp-item"><span>住所</span>
		  <big data-cc="address"></big>
		</div>
		<div class="cc-pp-item"><span>TEL</span>
		  <big data-cc="tel"></big>
		</div>
		<div class="cc-pp-item"><span>HP</span>
		  <big data-cc="hp"><a href="#" target="_blank"></a></big>
		</div>
	  </div>
	  <div class="pull-left">
		<div class="cc-pp-dir"><a href="#" data-cc="dir" target="_blank"><span class="icon-2ways"></span>アクセス</a></div>
	  </div>
	</div>
	<div class="cc-pp-footer">
	  <div class="cc-pp-footer-txt"><span>現在募集中の求人が<span class="green"><i data-cc="total">2</i>件</span></span></div>
	  <div class="cc-pp-footer-btn">
		<form method="get" data-cc="search" action="/search" target="_blank">
		  <input type="hidden" name="keyword" data-cc="keyword">
		  <input class="btn-green" type="submit" value="求人情報を確認する">
		</form>
	  </div>
	</div>
  </div>
</div>
</div>

<?php //echo $this->element('sql_dump'); ?>