<?php
    $this->start('css');
    echo $this->Html->css('callcenter-sp.css');
    $this->end('css');

$title = $detail['c']['company_name'];
$address = $detail['c']['address'];
$id = $detail['c']['id'];
$areaName = $detail['per']['area_name'];
$postionName = $areaName.$detail['city']['city_name'];
$this->set('title', $title);
?>


<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 padding-10 contentnew callcentersp-around callcenter-detail">

     <div class="callcentersp-header">
       <div class="img">
           <img src="/img/callcenter_logo.png"/>
       </div>
        <div class="title">
            <p>CallCenter Map</p>
            <h1><?php echo $postionName; ?></h1>
            <h2>コールセンターまとめ</h2>
        </div>
    </div>

    <div class="commonsp-panel">
		
		<div class="callcebter-toptic">
			<h2><?php echo $title; ?></h2>
		</div>
		<div class="callcenter-contact">
			<p><span>住所：</span><?php echo $address; ?></p>
			<p><span>電話：</span><?php echo $detail['c']['tel']; ?></p>
			<p><span>HP：</span><a target="_blank" href="<?php echo $detail['c']['link']; ?>"><?php echo $detail['c']['link']; ?></a></p>
			<p><span>業務・業種：</span> <label><?php echo $detail['c']['business']; ?></label></p>
		</div>
		
        <div class="callcenter-map">
                <iframe width="100%" frameborder="0" style="border:0" allowfullscreen="" 
			 src="https://www.google.com/maps/embed/v1/place?key=<?php echo Configure::read('webconfig.google_map_api_key'); ?>&q=<?php echo $address; ?>"></iframe>
        </div>
        <div class="find-way-link"><a href="https://maps.google.com/maps?ll=<?php echo $detail['c']['lat'];?>,<?php echo $detail['c']['lng'];?>&z=16&t=m&hl=ja-JP&gl=JP&mapclient=embed&daddr=<?php echo $title; ?>+<?php echo $address; ?>" target="_blank">「<?php echo $title; ?>」までのルートを検索</a></div>
        <div class="relation-job">
			<div class="title">「<?php echo $title; ?>」で現在募集中の求人情報一覧</div>
			<div class="relatiin-box">
                <div class="relation-ol">
                    <ol>		
                       <?php foreach ($relatedJob as $job): ?>				
                        <li>
                            <p><a target="_blank" href="<?php echo $job['url']; ?>"><?php echo $job['title']; ?></a></p>
                            <p><?php echo $job['salary']; ?> <span> 提供元：</span><?php echo $job['site_name']; ?></p>
                        </li>
                        <?php endforeach; ?>
                    </ol>
                </div>
            </div>
		</div>
		<div class="call-bottom">
		<div class="find-around-link">			
			<a href="/callcenter_matome/area/<?php echo $area_alias; ?>/city_<?php echo $city_id; ?>/c_<?php echo $id; ?>/around-search" class="brick-btn">周辺のコールセンターを探す</a>
		</div>
		</div>
	</div>

</div>

    <?php echo $this->element('../Top/_social-sp'); ?>