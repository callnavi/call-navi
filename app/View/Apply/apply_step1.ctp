<?php
$this->set('title', 'エントリーフォーム');
$breadcrumb = array('page' => 'エントリーフォーム');
$this->start('css');
echo $this->Html->css('contact-page');
echo $this->Html->css('jquery.steps.css');
echo $this->Html->css('apply_v2.css');
$this->end('css');

$this->start('script');
echo $this->Html->script('jquery.cookie-1.3.1');
echo $this->Html->script('jquery.validate.min');
echo $this->Html->script('app_new');
?>
<?php $this->end('script'); ?>


<?php $this->start('sub_header'); ?>
    <div class="top-banner">
      <div class="single-banner">
        <div class="main-title">応募フォーム</div>
        <ul class="apply-steps">
          <li class="apply-steps__item is_active">
            <div class="apply-steps__mark"><span>STEP1</span><span class="apply-steps__ttl">入力</span></div>
          </li>
          <li class="apply-steps__item">
            <div class="apply-steps__mark"><span>STEP2</span><span class="apply-steps__ttl">確認</span></div>
          </li>
          <li class="apply-steps__item">
            <div class="apply-steps__mark"><span>STEP3</span><span class="apply-steps__ttl">完了</span></div>
          </li>
        </ul>
      </div>
    </div>
<?php $this->end('sub_header'); ?>
<body class="container-contact apply apply_step1">
<?php $this->start('header_breadcrumb'); ?>
<?php echo $this->element('Item/breadcrumb', $breadcrumb); ?>
<?php $this->end('header_breadcrumb'); ?>

<div class="single-form-layout">
    <div class="content">
        <div class="no-padding container-contact">
            <div id="contact-main" class="mg-bottom-120">
                <div class="list-header-simple mg-top-50">
                    <h1><?php echo $secret['branch_name']; ?><small>のオリジナル求人</small></h1>
                </div>
               	<div class="div-contact-form mg-top-30">
                    <!-- Form 1 -->
                    <form id="frm-contact-form1" action="/secret/apply-step2?id=<?php echo $job_id; ?>" method="post" accept-charset="utf-8"
                          enctype="multipart/form-data" novalidate="novalidate" >
                        <div id="contact-form">
                            <section class="step1">
                                <div class="form-group" id="step1FullName">
                                    <div class="wp-st" id="st1-fullname">
                                        <label for="step1FullName" class="contact-lablel control-label">
                                            <span class="require">必須</span>お名前
                                        </label>

                                        <div class="contact-input">
                                            <input maxlength="100" type="text" name="step1FullName" id="step1-full-name"
                                                   value="<?php echo $post['step1FullName']; ?>" 
                                                   class="form-control" aria-required="true"
                                                   aria-invalid="false" placeholder="お名前を入力">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" id="step1Phonetic">
                                    <div class="wp-st" id="st1-fullname">
                                        <label class="contact-lablel control-label" for="step1FullName"><span class="require">必須</span>ふりがな</label>

                                        <div class="contact-input">
                                            <input maxlength="100" type="text" name="step1Phonetic" id="step1Phonetic"
                                                   value="<?php echo $post['step1Phonetic']; ?>" 
                                                   class="form-control" aria-required="true"
                                                   aria-invalid="false" placeholder="ふりがなを入力">
                                        </div>
                                    </div>
                                </div>
								<div class="form-panel">
									<div class="form-group" id="step1Birth">
										<div class="wp-st" id="st1-fullname">
											<label class="contact-lablel control-label" for="step1FullName"><span class="require">必須</span>生年月日</label>
                                            <!-- change placeholder use the sp design place holder not pc --> 
											<div class="contact-input">
												<input maxlength="8" type="text" name="step1Birth" id="step1Birth" 
												   	   value="<?php echo $post['step1Birth']; ?>" 
													   class="form-control" aria-required="true" aria-invalid="false"
													   placeholder="例）20160912">

											</div>


										</div>
									</div>

									<div class="form-group" id="step1gender">
										<div class="wp-st" id="st1-fullname">
											<label class="contact-lablel--half control-label" for="step1FullName"><span class="require">必須</span>性別</label>

											<div class="contact-input div_gender">
												<div class="custom-checkbox">
													<label>
														<input type="radio" value="男性" name="step1gender" id="Male"
															   <?php echo (empty($post['step1gender']) || $post['step1gender']=='男性')?'checked':''; ?> 
															   aria-required="true">

														<div class="vir-label circle-style">
															<span class="vir-content">男性</span>
															<span class="vir-checkbox"></span>
														</div>
													</label>
												</div>

												<div class="custom-checkbox">
													<label>
														<input type="radio" value="女性" name="step1gender" id="Female"
														   	   <?php echo $post['step1gender']=='女性'?'checked':''; ?>
															   aria-required="true">

														<div class="vir-label circle-style">
															<span class="vir-content">女性</span>
															<span class="vir-checkbox"></span>
														</div>
													</label>
												</div>
											</div>
										</div>
									</div>
								</div>

                                <div class="form-group" id="step1Phone">
                                    <div class="wp-st" id="st1-fullname">
                                        <label class="contact-lablel control-label" for="step1FullName"><span class="require">必須</span>電話番号</label>

                                        <div class="contact-input">
                                            <input maxlength="14" placeholder="電話番号を入力" type="text" name="step1Phone"
                                                   id="step1Phone" 
                                                   value="<?php echo $post['step1Phone']; ?>"
                                                   class="form-control" aria-required="true"
                                                   aria-invalid="false">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" id="step1Email">
                                    <div class="wp-st" id="st1-email">
                                        <label class="contact-lablel control-label" for="step1Email"><span class="require">必須</span>メールアドレス</label>

                                        <div class="contact-input">
                                            <input maxlength="100" type="email" name="step1Email" id="step1-email"
                                                   value="<?php echo $post['step1Email']; ?>"
                                                   class="form-control" aria-required="true"
                                                   aria-invalid="false" placeholder="アドレスを入力">

                                        </div>
                                    </div>
                                </div>
								<?php echo $this->element('../Apply/_upload_cv_step1'); ?>
                                <?php echo $this->element('../Apply/_skill_check_step1'); ?>
                                <?php echo $this->element('../Apply/_time_work_step1'); ?>
                               
                                <div class="form-group" id="step1Content">
                                    <div class="wp-st" id="st1-content">
                                        <label for="step1Content" class="contact-lablel control-label">
                                            備考欄<!-- <span class="require">必須</span> -->
                                        </label>

                                        <div class="contact-input">
                                            <textarea maxlength="2000" name="step1Content" id="step1-content" value=""
                                                      class="form-control" aria-required="true" aria-invalid="false"
                                                      placeholder="ご質問等あればご入力ください。"><?php echo $post['step1Content']; ?></textarea>
                                            <p class="form-group-custom-text">【お問い合わせフォームに関する注意事項】<br>
                                                「@callnavi.jp」からのメール受信が可能な設定にしていただきますようお願いいたします。<br>
                                                15分以上以内に返信メールが届かない場合は、迷惑メールフォルダをご確認ください。</p>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <div class="actions clearfix tabcontrol">
							<ul role="menu" aria-label="Pagination">
								<li class="disabled" aria-disabled="true"><a id="btnClearContent" href="#previous" role="menuitem">入力内容のリセット</a></li>
								<li aria-hidden="false" aria-disabled="false"><a id="btnSubmit" href="#next" role="menuitem">確認画面へ進む</a></li>
							</ul>
						</div>
                        <input type="hidden" name="job_id" id="job_id" value="<?php echo $job_id ?>"/>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>


      <script>
        $(document).ready(function () {
            $('#select-form-contact1').change(function () {
                $("#frm-contact-form2").hide();
                $("#frm-contact-form1").show();
                $("#frm-contact-form2").validate().resetForm();
                $("#step1FullName").append($("#st1-fullname"));
                $("#step1Email").append($("#st1-email"));
                $("#step1Content").append($("#st1-content"));
                $("#step2FullName").append($("#st2-fullname"));
                $("#step2Email").append($("#st2-email"));
                $("#step2Content").append($("#st2-content"));
            });
            //Check validate form 1
            var form1 = $("#frm-contact-form1");
            form1.validate({
                errorPlacement: function errorPlacement(error, element) {
                    //console.log(element.attr('name'));
                    switch(element.attr("name")) {
                        case "step1skill[]":
                            error.appendTo("#error-msg--skill");
                            break;
                        case "step1time":
                            error.appendTo("#error-msg--time");
                            break;
                        case "step1cv1":
                            error.appendTo("#error-msg--cv1");
                            break;
                        case "step1cv2":
                            error.appendTo("#error-msg--cv2");
                            break;
                        default:
                            error.insertAfter(element);
                            break;
                    };
                },
                onkeyup: false,
                rules: {
                    step1FullName: {
                        required: true,
                    },
                    step1Phonetic: {
                        required: true,
                    },
                    step1Birth: {
                        required: true,
                        birthday_valid: true
                    },
                    step1Phone: {
                        required: true,
                        phone_valid: true
                    },
                    step1Email: {
                        required: true,
                        email: true
                    },
                    /*************************/
                    <?php if($required_upload_cv): ?>
                    step1cv1: {
                        required: true
                    },
                    step1cv2: {
                        required: true
                    },
                    <?php endif; ?>
                    'step1skill[]': {
                        required: true
                    },
                    step1time: {
                        required: true
                    }
                    /*************************/
                },
                messages: {
                    step1FullName: {
                        required: "お名前は必須です。"
                    },
                    step1Phonetic: {
                        required: "ふりがなは必須です。"
                    },
                    step1Birth: {
                        required: "生年月日は必須です。"
                    },
                    step1Phone: {
                        required: "電話番号は必須です。"
                    },
                    step1Email: {
                        required: "アドレスは必須です。"
                    },
                    <?php if ($required_upload_cv): ?>
                    step1cv1: {
                        required: "履歴書は必須です。"
                    },
                    step1cv2: {
                        required: "職務経歴書は必須です。"
                    },
                    <?php endif; ?> 
                    'step1skill[]': {
                        required: "この項目は必須です。"
                    },
                    step1time: {
                        required: "この項目は必須です。"
                    }
                }
            });
            $("#btnClearContent").click(function () {
                $('input[name="step1FullName"]').val("");
                $('input[name="step1Phonetic"]').val("");
                $('input[name="step1Birth"]').val("");
                $('input[name="step1Phone"]').val("");
                $('input[name="step1Email"]').val("");
                $('input[name="step1cv1"]').val("");
                $('input[name="step1cv2"]').val("");
                $('[data-target-file]').text('選択されていません');
                $('input[name="step1skill[]"]').removeAttr('checked');
                $('input[name="step1skill[]"]').each( function () {
                    if($(this).attr('value') === 'コールセンターで働いたことがある'){
                        $(this).prop('checked', true);
                    }
                });
                $('select[name="step1time"]').val("");
                $('select[name="step1time"]' + ' option').removeAttr('selected');
        
                $('textarea[name="step1Content"]').val("");
                $('#Male').prop('checked', true);
            });
            jQuery.validator.addMethod("phone_valid", function (phone_number, element) {
                phone_number = phone_number.replace(/\\s+/g, "");
                return this.optional(element) || phone_number.length > 9 &&
                    phone_number.match(/^[0-9]{3}[-\\s]{0,1}[0-9]{4}[-\s]{0,1}[0-9]{3,4}$/);
            }, "携帯電話・固定電話をご入力ください。");
            jQuery.validator.addMethod("birthday_valid", function (value, element) {
                value = value.replace(/\\s+/g, "");
                return /^\d{8}$/.test(value);
            }, "生年月日が不正です。");
            jQuery.extend(jQuery.validator.messages, {
                //required: "この項目は必須です。",
                email: "メールアドレスが不正です。"
            });
            function disableRadioButton() {
                if ($("#select-form-contact1").is(':checked')) {
                    $("#select-form-contact2").attr('disabled', true);
                    $("#color-disable2").addClass("color-disable");
                }
                else {
                    $("#select-form-contact1").attr('disabled', true);
                    $("#color-disable1").addClass("color-disable");
                }
            }
            function disableButtonStep() {
                $("li.first.done").addClass('disabled');
                $("li.current").addClass('disabled');
            }
            $('#btnSubmit').click(function () {
                $("#frm-contact-form1").submit();
            });
        });
        
        <?php // give warning of date time is in correct
              if($post['step1Birth']){
                    $date = $post['step1Birth'];
                    if(checkdate(substr($date,4,2), substr($date,6,2), substr($date,0,4)) == false){
                    ?>
                       alert("Date Is invalid!!");
                    <?php

                    }
                  }
        ?>
           
      </script>
        <script>
         <?php        //selected value if have data
            if($workingTime){
                if($workingTime == "1"){
                    if($post['step1Time']){
            ?>
                        $('#step1-time option[value=<?php echo($post['step1Time']); ?>]').attr('selected','selected'); 
            <?php
                    }
               }
            }
            ?>
    </script>


