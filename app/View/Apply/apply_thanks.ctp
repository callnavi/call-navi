<?php
$this->set('title', 'エントリーフォーム');
$breadcrumb = array('page' => 'エントリーフォーム');
$this->start('css');
echo $this->Html->css('contact-page');
echo $this->Html->css('jquery.steps.css');
echo $this->Html->css('apply.css');
$this->end('css');

$this->start('script');
echo $this->Html->script('jquery.cookie-1.3.1');
echo $this->Html->script('jquery.steps.min');
echo $this->Html->script('jquery.validate.min');
?>
<?php $this->end('script'); ?>

<?php $this->start('sub_header'); ?>
    <div class="top-banner">
      <div class="single-banner">
        <div class="main-title">応募フォーム</div>
        <ul class="apply-steps">
          <li class="apply-steps__item">
            <div class="apply-steps__mark"><span>STEP1</span><span class="apply-steps__ttl">入力</span></div>
          </li>
          <li class="apply-steps__item">
            <div class="apply-steps__mark"><span>STEP2</span><span class="apply-steps__ttl">確認</span></div>
          </li>
          <li class="apply-steps__item is_active">
            <div class="apply-steps__mark"><span>STEP3</span><span class="apply-steps__ttl">完了</span></div>
          </li>
        </ul>
      </div>
    </div>
<?php $this->end('sub_header'); ?>

<?php $this->start('header_breadcrumb'); ?>
<?php echo $this->element('Item/breadcrumb', $breadcrumb); ?>
<?php $this->end('header_breadcrumb'); ?>

<?php echo $this->element('../Apply/_special_tracking_page'); ?>

    <div class="no-padding container ipad-padding" id="div-main">
      <div class="single-form-layout">
        <div class="content">
          <div class="no-padding container-contact">
            <div class="mg-bottom-120" id="contact-main">
              <div class="list-header-simple mg-top-50">
                <h1><?php echo $secret['branch_name']; ?><small>のオリジナル求人</small></h1>
              </div>
              <div class="div-contact-form mg-top-120">
                <!-- Form 1-->
                <div id="contact-form">
                  <section class="finish df-padding">
                    <p class="contact-main-txt">この度はお問い合せ頂き誠にありがとうございました。<br>改めて担当者よりご連絡をさせていただきます。</p>
                    <div class="contact-custom-text mg-top-50">
                      <p class="text-center">【お問い合わせフォームに関する注意事項】</p>
                      <p>「@callnavi.jp」からのメール受信が可能な設定にしていただきますようお願いいたします。<br>15分以上以内に返信メールが届かない場合は、迷惑メールフォルダをご確認ください。</p>
                    </div>
                    <div class="contact-btn-wrap"><a class="btn-finish" href="../">ページトップに戻る</a></div>
                  </section>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
<?php echo $this->element('Item/script_google_yahoo_no_script'); ?>