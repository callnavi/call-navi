<?php $this->layout = 'single_column_v2_sp'; ?>
<?php
$this->set('title', 'エントリーフォーム');
$this->start('css');
echo $this->Html->css('jquery.steps-sp');
echo $this->Html->css('apply-v2-sp');
$this->end('css');
?>

<?php echo $this->element('../Apply/_special_tracking_page'); ?>
     
      <div class="banner">
        <div class="background clip-bottom-arrow" id="background--apply">
          <div class="main-title">応募フォーム</div>
          <ul class="apply-steps">
            <li class="apply-steps__item">
              <div class="apply-steps__mark"><span class="apply-steps__num">STEP1</span><span class="apply-steps__ttl">入力</span></div>
            </li>
            <li class="apply-steps__item">
              <div class="apply-steps__mark"><span class="apply-steps__num">STEP2</span><span class="apply-steps__ttl">確認</span></div>
            </li>
            <li class="apply-steps__item is_active">
              <div class="apply-steps__mark"><span class="apply-steps__num">STEP3</span><span class="apply-steps__ttl">完了</span></div>
            </li>
          </ul>
        </div>
      </div>

      <div id="contact-main">
        <div class="company-title mg-top-60">
          <h1><?php echo $secret['branch_name']; ?><small>のオリジナル求人</small></h1>
        </div>
        <div class="div-contact-form apply-form mg-top-60">
          <div id="contact-form">
            <section class="finish df-padding">
              <p class="contact-main-txt">この度はお問い合せ頂き誠にありがとうございました。<br>                   改めて担当者よりご連絡をさせていただきます。</p>
              <div class="contact-custom-text">
                <p class="text-center">【お問い合わせフォームに関する注意事項】</p>
                <p>「@callnavi.jp」からのメール受信が可能な設定にしていただきますようお願いいたします。<br>15分以上以内に返信メールが届かない場合は、迷惑メールフォルダをご確認ください。</p>
              </div>
              <div class="contact-btn-wrap"><a class="btn-finish" href="../">ページトップに戻る</a></div>
            </section>
          </div>
        </div>
      </div>
      <div class="social-box df-padding">
        <div class="fb-column">
          <div class="advertisement__item"><a href="../pretty"><img src="../img/pretty_sp.png"></a></div>
          <div class="advertisement__item"><a href="../callcenter_matome"><img src="../img/map_sp.png"></a></div>
          <div class="advertisement__item"><img src="../img/sns_sp.png">
            <div class="fb-like" data-href="https://callnavi.jp" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
          </div>
        </div>
        <div class="twitter-button clearfix"><span class="description">「コールナビ」Twitterアカウント</span><a class="pull-right" target="_blank" href="https://twitter.com/intent/tweet?url=https://callnavi.jp/&amp;via=コールナビ&amp;text=コールセンター求人数日本No.1サイト、コールナビ。最新の求人情報とお役立ち記事が満載！&amp;related=コールナビ"></a>
          <script>
            !function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0],
                    p = /^http:/.test(d.location) ? 'http' : 'https';
                if (!d.getElementById(id)) {
                    js = d.createElement(s);
                    js.id = id;
                    js.src = p + '://platform.twitter.com/widgets.js';
                    fjs.parentNode.insertBefore(js, fjs);
                }
            }(document, 'script', 'twitter-wjs');
          </script>
        </div>
      </div>

<?php echo $this->element('Item/script_google_yahoo_no_script'); ?>


