<?php if($workingTime){
    if($workingTime == "1"){

?>
                                <div class="form-group" id="step1Time">

                                    <div class="wp-st" id="st1-time">
                                      <label class="contact-lablel control-label" for="step1Time"><span class="require">必須</span>勤務可能時間</label>
                                      <div class="contact-input contact-input--select">月
                                        <div class="contact-select-triangle">
                                          <select class="select-box" id="step1-time" name="step1time">
                                            <option value=""></option>
                                            <option value="120時間以上" 
                                            <?php
                                            if(isset($post['step1time'])){
                                                if($post['step1time'] == "120時間以上"){
                                                    echo ('selected');
                                                }
                                            }
                                            ?>
                                                    >120時間以上</option>
                                            <option value="120時間未満"
                                            <?php
                                                if(isset($post['step1time'])){
                                                    if($post['step1time'] == "120時間未満"){
                                                        echo ('selected');
                                                    }
                                                }
                                            ?>                                                    
                                                    >120時間未満</option>
                                            <option value="36時間未満"
                                            <?php
                                            if(isset($post['step1time'])){
                                                if($post['step1time'] == "36時間未満"){
                                                    echo ('selected');
                                                }
                                            }
                                            ?>                                                    
                                                    >36時間未満</option>
                                          </select>
                                        </div>働ける
                                        <div class="mg-top-5">※アルバイトにご応募の方のみ選択してください</div>
                                        <div id="error-msg--time"></div>
                                      </div>
                                    </div>
                                  </div> 
<?php
    }
}

?>

