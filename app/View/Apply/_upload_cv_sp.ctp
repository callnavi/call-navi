<?php if ($is_upload_cv): ?>
<?php 
$rq = '';
if($required_upload_cv){
	$rq = '<span class="require">必須</span>';
} 
?>
                            <div class="form-group" id="step1cv1">
                              <div class="wp-st" id="st1-cv1">
                                <label class="contact-lablel control-label" for="step1-cv1"><?php echo $rq; ?>履歴書</label>
                                <div class="contact-input">
                                  <input class="contact-select-file" id="step1-cv1" type="file" name="step1cv1" value="" aria-required="true" data-trigger-file="select">
                                  <label class="contact-btn--file" for="step1-cv1">ファイルを選択</label><span class="contact-file-name" data-target-file="name">選択されていません</span>
                                  <div id="error-msg--cv1"></div>
                                </div>
                              </div>
                            </div>
                            <div class="form-group" id="step1cv2">
                              <div class="wp-st" id="st1-cv2">
                                <label class="contact-lablel control-label" for="step1-cv2"><?php echo $rq; ?>職務経歴書</label>
                                <div class="contact-input">
                                  <input class="contact-select-file" id="step1-cv2" type="file" name="step1cv2" value="" aria-required="true" data-trigger-file="select">
                                  <label class="contact-btn--file" for="step1-cv2">ファイルを選択</label><span class="contact-file-name" data-target-file="name">選択されていません</span>
                                  <div id="error-msg--cv2"></div>
                                </div>
                              </div>
                            </div>
<?php endif; ?>