<?php $this->layout = 'single_column_v2_sp'; ?>
<?php
$this->set('title', 'エントリーフォーム');
$this->start('css');
echo $this->Html->css('jquery.steps-sp');
echo $this->Html->css('apply-v2-sp');
$this->end('css');
?>
<div class="banner">
        <div class="background clip-bottom-arrow" id="background--apply">
          <div class="main-title">応募フォーム</div>
          <ul class="apply-steps">
            <li class="apply-steps__item">
              <a href="/secret/apply-step1?id=<?php echo $job_id; ?>"><div class="apply-steps__mark"><span class="apply-steps__num">STEP1</span><span class="apply-steps__ttl">入力</span></div></a>
            </li>
            <li class="apply-steps__item is_active">
              <div class="apply-steps__mark"><span class="apply-steps__num">STEP2</span><span class="apply-steps__ttl">確認</span></div>
            </li>
            <li class="apply-steps__item">
              <div class="apply-steps__mark"><span class="apply-steps__num">STEP3</span><span class="apply-steps__ttl">完了</span></div>
            </li>
          </ul>
        </div>
      </div>


      
      <div id="contact-main">
        <div class="company-title mg-top-60">
          <h1><?php echo $secret['branch_name']; ?><small>のオリジナル求人</small></h1>
        </div>
        <div class="div-contact-form apply-form mg-top-60">
          <div id="contact-form">
            <section class="step2 df-padding">
              <div class="form-group" id="step2FullName">
                <div id="st2-fullname">
                  <label class="contact-lablel control-label" for="step2FullName"><span class="require">必須</span>お名前</label>
                  <div class="contact-input">
                    <input class="form-control" id="step2-full-name" type="text" name="step2FullName" disabled="" aria-required="true" aria-invalid="false" placeholder="お名前を入力" value="<?php echo $post['step1FullName']; ?>">
                  </div>
                </div>
              </div>
              <div class="form-group" id="step2Phonetic">
                <div id="st2-Phonetic">
                  <label class="contact-lablel control-label" for="step2Phonetic"><span class="require">必須</span>ふりがな</label>
                  <div class="contact-input">
                    <input class="form-control" id="step2Phonetic" type="text" name="step2Phonetic" disabled="" aria-required="true" aria-invalid="false" placeholder="ふりがなを入力" value="<?php echo $post['step1Phonetic']; ?>">
                  </div>
                </div>
              </div>
              <div class="form-group" id="step2Birth">
                <div id="st2-Birth">
                  <label class="contact-lablel control-label" for="step2Birth"><span class="require">必須</span>年齢</label>
                  <div class="contact-input">
                    <input class="form-control" id="step2Birth" type="text" name="step2Birth" value="<?php echo $post['step1Birth']; ?>" aria-required="true" aria-invalid="false" disabled="" placeholder="20160518">
                  </div>
                </div>
              </div>
              <div class="form-group" id="step2gender">
                <div id="st1-gender">
                  <label class="contact-lablel control-label" for="step2FullName"><span class="require">必須</span>性別</label>
                  <div class="contact-input div_gender"><?php echo $post['step1gender']; ?>
                    <!--.custom-checkbox-->
                    <!--    label-->
                    <!--        input#Malestep2(disabled='', type='radio', value='男性', name='step2gender', checked='', aria-required='true')-->
                    <!--        .vir-label.circle-style-->
                    <!--            span.vir-content 男性-->
                    <!--            span.vir-checkbox-->
                    <!--.custom-checkbox-->
                    <!--    label-->
                    <!--        input#Femalestep2(disabled='', type='radio', value='女性', name='step2gender', aria-required='true')-->
                    <!--        .vir-label.circle-style-->
                    <!--            span.vir-content 女性-->
                    <!--            span.vir-checkbox-->
                  </div>
                </div>
              </div>
              <div class="form-group" id="step2Phone">
                <div id="st1-Phone">
                  <label class="contact-lablel control-label" for="step2Phone"><span class="require">必須</span>電話番号</label>
                  <div class="contact-input">
                    <input class="form-control" id="step2Phone" type="text" name="step2Phone" value="<?php echo $post['step1Phone']; ?>" aria-required="true" disabled="" aria-invalid="false">
                  </div>
                </div>
              </div>
              <div class="form-group" id="step2Email">
                <div id="st2-email">
                  <label class="contact-lablel control-label" for="step2Email"><span class="require">必須</span>メールアドレス</label>
                  <div class="contact-input">
                    <input class="form-control" id="step2-email" type="email" name="step2Email" disabled="" aria-required="true" aria-invalid="false" placeholder="test@test.com" value="<?php echo $post['step1Email']; ?>">
                  </div>
                </div>
              </div>
                <!-- include upload CV -->
              <?php echo $this->element('../Apply/_upload_cv_sp_2'); ?>
              <div class="form-group" id="step1Skill">
                <div class="wp-st" id="st1-skill">
                  <label class="contact-lablel control-label"><span class="require">必須</span>スキル</label>
                  <div class="contact-input">
                    <ul class="contact-list">
                            <!-- Showwing array of data skill-->
                            <?php
                              foreach($post['step1skill'] as $skill){
                                  echo("<li class='contact-list__item'>".$skill."</li>");
                              }
                            ?>
                    </ul>
                  </div>
                </div>
              </div>
              <?php if(isset($post['step1time'])){
              ?>
              <div class="form-group" id="step1time">
                <div class="wp-st" id="st1-time">
                  <label class="contact-lablel control-label"><span class="require">必須</span>勤務可能時間</label>
                  <div class="contact-input">
                    <div class="contact-input__inner"><?php echo $post['step1time']; ?></div>
                  </div>
                </div>
              </div>
                <?php
                        }
                ?>
                
             <?php
                if(isset($post['step1Content']) && $post['step1Content'] <> ""){ ?>
              <div class="form-group" id="step2Content">
                <div id="st2-content">
                  <label class="contact-lablel control-label" for="step2Content">備考欄</label>
                  <div class="contact-input">
                    
                    <textarea class="form-control" id="step2-content" name="step2Content" disabled="" aria-required="true" aria-invalid="false" placeholder="質問等ございましたらご入力ください。"><?php echo $post['step1Content']; ?></textarea>
                    </div>
                </div>
              </div>
                    <?php } ?>
              <section class="contact-custom-text">
                <p class="text-center">【お問い合わせフォームに関する注意事項】</p>
                <p>「@callnavi.jp」からのメール受信が可能な設定にしていただきますようお願いいたします。<br>15分以上以内に返信メールが届かない場合は、迷惑メールフォルダをご確認ください。</p>
              </section>
            </section>
          </div>
          <div class="tabcontrol">
            <div class="actions clearfix">
              <ul role="menu" aria-label="Pagination">
                <li aria-hidden="false" aria-disabled="false">
                    <a href="/secret/apply-thanks?id=<?php echo $job_id; ?>"  role="menuitem">上記内容で送信する</a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
      <div class="social-box df-padding">
        <div class="fb-column">
          <div class="advertisement__item"><a href="../pretty"><img src="../img/pretty_sp.png"></a></div>
          <div class="advertisement__item"><a href="../callcenter_matome"><img src="../img/map_sp.png"></a></div>
          <div class="advertisement__item"><img src="../img/sns_sp.png">
            <div class="fb-like" data-href="https://callnavi.jp" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
          </div>
        </div>
        <div class="twitter-button clearfix"><span class="description">「コールナビ」Twitterアカウント</span><a class="pull-right" target="_blank" href="https://twitter.com/intent/tweet?url=https://callnavi.jp/&amp;via=コールナビ&amp;text=コールセンター求人数日本No.1サイト、コールナビ。最新の求人情報とお役立ち記事が満載！&amp;related=コールナビ"></a>
          <script>
            !function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0],
                    p = /^http:/.test(d.location) ? 'http' : 'https';
                if (!d.getElementById(id)) {
                    js = d.createElement(s);
                    js.id = id;
                    js.src = p + '://platform.twitter.com/widgets.js';
                    fjs.parentNode.insertBefore(js, fjs);
                }
            }(document, 'script', 'twitter-wjs');
            
          </script>
        </div>
      </div>
   


