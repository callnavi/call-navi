<?php $this->layout = 'single_column_v2_sp'; ?>
	<?php
$this->set('title', 'エントリーフォーム');
$this->start('css');
echo $this->Html->css('jquery.steps-sp_v2');
echo $this->Html->css('apply-v2-sp');
$this->end('css');
?>

      <div class="banner">
        <div class="background clip-bottom-arrow" id="background--apply">
          <div class="main-title">応募フォーム</div>
          <ul class="apply-steps">
            <li class="apply-steps__item is_active">
              <div class="apply-steps__mark"><span class="apply-steps__num">STEP1</span><span class="apply-steps__ttl">入力</span></div>
            </li>
            <li class="apply-steps__item">
              <div class="apply-steps__mark"><span class="apply-steps__num">STEP2</span><span class="apply-steps__ttl">確認</span></div>
            </li>
            <li class="apply-steps__item">
              <div class="apply-steps__mark"><span class="apply-steps__num">STEP3</span><span class="apply-steps__ttl">完了</span></div>
            </li>
          </ul>
        </div>

		<div id="contact-main">
            <div class="company-title mg-top-60">
                <h1><?php echo $secret['branch_name']; ?><small>のオリジナル求人</small></h1>
            </div>
			
			<div class="div-contact-form apply-form mg-top-20">
				<form id="frm-contact-form1" action="/secret/apply-step2?id=<?php echo $job_id; ?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" novalidate="novalidate">
					<div id="contact-form">
						<section class="step1 df-padding">
							<div class="form-group" id="step1FullName">
								<div id="st1-fullname">
                                    
									<label class="contact-lablel control-label" for="step1FullName"><span class="require">必須</span>お名前</label>

									<div class="contact-input">
                                        <input class="form-control" id="step1-full-name" type="text" name="step1FullName"  aria-required="true" value="<?php echo $post['step1FullName']; ?>" aria-invalid="false" placeholder="お名前を入力">
										
									</div>
								</div>
							</div>
							<div class="form-group" id="step1Phonetic">
								<div id="st1-fullname">
									<label class="contact-lablel control-label" for="step1FullName"><span class="require">必須</span>ふりがな</label>

									<div class="contact-input">
										<input type="text" name="step1Phonetic" id="step1Phonetic" value="<?php echo $post['step1Phonetic']; ?>" class="form-control" aria-required="true" aria-invalid="false" placeholder="ふりがなを入力">
									</div>
								</div>
							</div>

							<div class="form-group" id="step1Birth">
								<div id="st1-fullname">
									<label class="contact-lablel control-label" for="step1FullName"><span class="require">必須</span>生年月日</label>

									<div class="contact-input">
										<input type="text" name="step1Birth" id="step1Birth" value="<?php echo $post['step1Birth']; ?>" class="form-control" aria-required="true" aria-invalid="false" placeholder="例）20160912">

									</div>
								</div>
							</div>

							<div class="form-group" id="step1gender">
								<div id="st1-fullname">
									<label class="contact-lablel control-label" for="step1FullName"><span class="require">必須</span>性別</label>

									<div class="contact-input div_gender">
										<div class="custom-checkbox">
											<label>
												<input checked type="radio" value="男性" name="step1gender" id="Male" <?php echo (empty($post[ 'step1gender']) || $post[ 'step1gender']=='男性' )? 'checked': ''; ?> aria-required="true">

												<div class="vir-label circle-style">
													<span class="vir-content">男性</span>
													<span class="vir-checkbox"></span>
												</div>
											</label>
										</div>

										<div class="custom-checkbox">
											<label>
												<input type="radio" value="女性" name="step1gender" id="Female" <?php echo $post[ 'step1gender']=='女性' ? 'checked': ''; ?> aria-required="true">

												<div class="vir-label circle-style">
													<span class="vir-content">女性</span>
													<span class="vir-checkbox"></span>
												</div>
											</label>
										</div>
									</div>
								</div>
							</div>


							<div class="form-group" id="step1Phone">
								<div id="st1-fullname">
									<label class="contact-lablel control-label" for="step1FullName"><span class="require">必須</span>電話番号</label>

									<div class="contact-input">
										<input type="text" name="step1Phone" id="step1Phone" value="<?php echo $post['step1Phone']; ?>" class="form-control" aria-required="true" aria-invalid="false" placeholder="電話番号を入力">
									</div>
								</div>
							</div>

							<div class="form-group" id="step1Email">
								<div id="st1-email">
									<label class="contact-lablel control-label" for="step1Email"><span class="require">必須</span>メールアドレス</label>

									<div class="contact-input">
										<input type="email" name="step1Email" id="step1-email" value="<?php echo $post['step1Email']; ?>" class="form-control" aria-required="true" aria-invalid="false" placeholder="メールアドレスを入力">
									</div>
								</div>
							</div>
                                <?php echo $this->element('../Apply/_upload_cv_sp'); ?>
                                <?php echo $this->element('../Apply/_skill_check_step1_sp'); ?>
                                <?php echo $this->element('../Apply/_time_work_step1_sp'); ?>
							<div class="form-group" id="step1Content">
								<div id="st1-content">
								    <label class="contact-lablel control-label" for="step1Content">備考欄</label>

									<div class="contact-input">
										<textarea name="step1Content" id="step1-content" value="" class="form-control" aria-required="true" aria-invalid="false" 
										placeholder="志望動機/自己PR/ご質問など※ご記入いただくことにより選考に進みやすくなります。"><?php echo $post['step1Content']; ?></textarea>
									</div>
								</div>
							</div>
                            <section class="contact-custom-text">
                                <p class="text-center">【お問い合わせフォームに関する注意事項】</p>
                                <p>「@callnavi.jp」からのメール受信が可能な設定にしていただきますようお願いいたします。<br>15分以上以内に返信メールが届かない場合は、迷惑メールフォルダをご確認ください。</p>
                            </section>
						</section>
					</div>
					<div class="tabcontrol">
						<div class="actions clearfix">
							<ul role="menu" aria-label="Pagination">
								<li aria-hidden="false" aria-disabled="false"><a id="btnSubmit" href="#next" role="menuitem">確認画面へ進む</a></li>
							</ul>
						</div>
					</div>
					<input type="hidden" name="job_id" id="job_id" value="<?php echo $job_id ?>" />
				</form>

			</div>
		</div>
      <div class="social-box df-padding">
        <div class="fb-column">
          <div class="advertisement__item"><a href="../pretty"><img src="../img/pretty_sp.png"></a></div>
          <div class="advertisement__item"><a href="../callcenter_matome"><img src="../img/map_sp.png"></a></div>
          <div class="advertisement__item"><img src="../img/sns_sp.png">
            <div class="fb-like" data-href="https://callnavi.jp" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
          </div>
        </div>
        <div class="twitter-button clearfix"><span class="description">「コールナビ」Twitterアカウント</span><a class="pull-right" target="_blank" href="https://twitter.com/intent/tweet?url=https://callnavi.jp/&amp;via=コールナビ&amp;text=コールセンター求人数日本No.1サイト、コールナビ。最新の求人情報とお役立ち記事が満載！&amp;related=コールナビ"></a>
          <script>
            !function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0],
                    p = /^http:/.test(d.location) ? 'http' : 'https';
                if (!d.getElementById(id)) {
                    js = d.createElement(s);
                    js.id = id;
                    js.src = p + '://platform.twitter.com/widgets.js';
                    fjs.parentNode.insertBefore(js, fjs);
                }
            }(document, 'script', 'twitter-wjs');
            
          </script>
        </div>
      </div>          

			<?php
$this->start('script');
        ?>
          <script>
        <?php // give warning of date time is in correct
              if($post['step1Birth']){
                    $date = $post['step1Birth'];
                    if(checkdate(substr($date,4,2), substr($date,6,2), substr($date,0,4)) == false){
                    ?>
                       alert("Date Is invalid!!");
                    <?php

                    }
                  }
        ?>
         <?php        //selected value if have data
            if($workingTime){
                if($workingTime == "1"){
                    if($post['step1time']){
            ?>
                        $('#step1-time option[value=<?php echo($post['step1time']); ?>]').attr('selected','selected'); 
            <?php
                    }
               }
            }
        ?>
          </script>
          <?php
	echo $this->Html->script('jquery.cookie-1.3.1.js');
	echo $this->Html->script('jquery.validate.min.js');
    if($required_upload_cv){
	   echo $this->Html->script('apply/apply-v3-sp.js');
    }else{
       echo $this->Html->script('apply/apply-v3-sp-nocv.js'); 
    }
    echo $this->Html->script('app_new');
$this->end('script');
?>