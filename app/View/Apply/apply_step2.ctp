<?php
$this->set('title', 'エントリーフォーム');
$breadcrumb = array('page' => 'エントリーフォーム');
$this->start('css');
echo $this->Html->css('contact-page');
echo $this->Html->css('jquery.steps.css');
echo $this->Html->css('apply.css');
$this->end('css');

$this->start('script');
echo $this->Html->script('jquery.cookie-1.3.1');
echo $this->Html->script('jquery.steps.min');
echo $this->Html->script('jquery.validate.min');
?>
<?php $this->end('script'); ?>
<?php $this->start('sub_header'); ?>
    <div class="top-banner">
      <div class="single-banner">
        <div class="main-title">応募フォーム</div>
        <ul class="apply-steps">
          <li class="apply-steps__item">
            <div class="apply-steps__mark"><span>STEP1</span><span class="apply-steps__ttl">入力</span></div>
          </li>
          <li class="apply-steps__item is_active">
            <div class="apply-steps__mark"><span>STEP2</span><span class="apply-steps__ttl">確認</span></div>
          </li>
          <li class="apply-steps__item">
            <div class="apply-steps__mark"><span>STEP3</span><span class="apply-steps__ttl">完了</span></div>
          </li>
        </ul>
      </div>
    </div>
<?php $this->end('sub_header'); ?>

<?php $this->start('header_breadcrumb'); ?>
<?php echo $this->element('Item/breadcrumb', $breadcrumb); ?>
<?php $this->end('header_breadcrumb'); ?>

<div class="single-form-layout">
    <div class="content">
        <div class="no-padding container-contact">
            <div id="contact-main" class="mg-bottom-120">
                <div class="list-header-simple mg-top-50">
                    <h1><?php echo $secret['branch_name']; ?> <small>のオリジナル求人</small></h1>
                </div>
                <!-- new form design -->
                <div class="div-contact-form mg-top-30">
                <!-- Form 1-->
                <div id="contact-form">
                  <section class="step2">
                    <div class="form-group" id="step2FullName">
                      <div class="wp-st" id="st2-fullname">
                        <label class="contact-lablel control-label" for="step2FullName"><span class="require">必須</span>氏名</label>
                        <div class="contact-input">
                          <input class="form-control" id="step2-full-name" type="text" name="step2FullName" disabled="" aria-required="true" aria-invalid="false" placeholder="お名前を入力" value="<?php echo $post['step1FullName']; ?> ">
                        </div>
                      </div>
                    </div>
                    <div class="form-group" id="step2Phonetic">
                      <div class="wp-st" id="st2-Phonetic">
                        <label class="contact-lablel control-label" for="step2Phonetic"><span class="require">必須</span>ふりがな</label>
                        <div class="contact-input">
                          <input class="form-control" id="step2Phonetic" type="text" name="step2Phonetic" disabled="" aria-required="true" aria-invalid="false" placeholder="ふりがなを入力" value="<?php echo $post['step1Phonetic']; ?>">
                        </div>
                      </div>
                    </div>
                    <div class="form-panel">
                      <div class="form-group" id="step2Birth">
                        <div class="wp-st" id="st2-Birth">
                          <label class="contact-lablel control-label" for="step2Birth"><span class="require">必須</span>生年月日</label>
                          <div class="contact-input">
                            <input class="form-control" id="step2Birth" type="text" name="step2Birth" aria-required="true" aria-invalid="false" disabled="" placeholder="20160518" value="<?php echo $post['step1Birth']; ?>">
                          </div>
                        </div>
                      </div>
                      <div class="form-group" id="step2gender">
                        <div class="wp-st" id="st1-gender">
                          <label class="contact-lablel--half control-label" for="step2FullName"><span class="require">必須</span>性別</label>
                          <div class="contact-input div_gender">
                            <div class="contact-input__inner"><?php echo $post['step1gender']; ?></div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="form-group" id="step2Phone">
                      <div class="wp-st" id="st1-Phone">
                        <label class="contact-lablel control-label" for="step2Phone"><span class="require">必須</span>電話番号</label>
                        <div class="contact-input">
                          <input class="form-control" id="step2Phone" type="text" name="step2Phone" aria-required="true" disabled="" value="<?php echo $post['step1Phone']; ?>" aria-invalid="false">
                        </div>
                      </div>
                    </div>
                    <div class="form-group" id="step2Email">
                      <div class="wp-st" id="st2-email">
                        <label class="contact-lablel control-label" for="step2Email"><span class="require">必須</span>アドレス</label>
                        <div class="contact-input">
                          <input class="form-control" id="step2-email" type="email" name="step2Email" disabled="" aria-required="true" aria-invalid="false" value="<?php echo $post['step1Email']; ?>" placeholder="メールアドレス">
                        </div>
                      </div>
                    </div>
                    
                    <?php echo $this->element('../Apply/_upload_cv_step2'); ?>
                    <div class="form-group" id="step1Skill">
                      <div class="wp-st" id="st1-skill">
                        <label class="contact-lablel control-label"><span class="require">必須</span>スキル</label>
                        <div class="contact-input">
                          <ul class="contact-list">
                            <?php
                              foreach($post['step1skill'] as $skill){
                                  echo("<li class='contact-list__item'>".$skill."</li>");
                              }
                            ?>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <?php if(isset($post['step1time'])){
                    ?>
                    <div class="form-group" id="step1Time">
                      <div class="wp-st" id="st1-time">
                        <label class="contact-lablel control-label"><span class="require">必須</span>勤務可能時間</label>
                        <div class="contact-input">
                          <div class="contact-input__inner"><?php echo $post['step1time']; ?></div>
                        </div>
                      </div>
                    </div>
                      <?php
                        }
                        ?>
                      <?php 
                      //validate if the content was not show
                      if(isset($post['step1Content']) && $post['step1Content'] <> ""){ ?>
                    <div class="form-group" id="step2Content">
                      <div class="wp-st" id="st2-content">
                        <label class="contact-lablel control-label" for="step2Content">備考欄</label>
                        <div class="contact-input">
                          
                          <textarea class="form-control" id="step2-content" name="step2Content" disabled="" aria-required="true" aria-invalid="false" ><?php echo $post['step1Content']; ?></textarea>
                          <p class="form-group-custom-text">【お問い合わせフォームに関する注意事項】<br>「@callnavi.jp」からのメール受信が可能な設定にしていただきますようお願いいたします。<br>15分以上以内に返信メールが届かない場合は、迷惑メールフォルダをご確認ください。</p>
                        </div>
                      </div>
                    </div>
                        <?php } ?>                      
                  </section>
                </div>
                <div class="actions clearfix tabcontrol">
                  <ul role="menu" aria-label="Pagination">
                    <li class="disabled" aria-disabled="true"><a class="btn-pre" href="../secret/apply-step1?id=<?php echo $job_id; ?>" role="menuitem">入力をやり直す</a></li>
                    <li aria-hidden="false" aria-disabled="false"><a href="/secret/apply-thanks?id=<?php echo $job_id; ?>" role="menuitem">送信する</a></li>
                  </ul>
                </div>
              </div>
            </div>
        </div>
    </div>
</div>


