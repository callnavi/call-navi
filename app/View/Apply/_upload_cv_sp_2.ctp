<?php if ($is_upload_cv): ?>
<?php 
$rq = '';
if($required_upload_cv){
	$rq = '<span class="require">必須</span>';
} 
?>  
        <?php if (isset($post['step1cv1'])): ?>
            <div class="form-group" id="step1cv1">
                <div class="wp-st" id="st1-cv1">
                  <label class="contact-lablel control-label"><?php echo $rq; ?>履歴書</label>
                  <div class="contact-input"><span class="contact-file-name"><?php echo $post['step1cv1']; ?></span></div>
                </div>
              </div>
              <?php endif; ?>
              <?php if (isset($post['step1cv2'])): ?>
              <div class="form-group" id="step1cv2">
                <div class="wp-st" id="st1-cv2">
                  <label class="contact-lablel control-label"><?php echo $rq; ?>職務経歴書</label>
                  <div class="contact-input"><span class="contact-file-name"><?php echo $post['step1cv2']; ?></span></div>
                </div>
              </div>
              <?php endif; ?>
<?php endif; ?>