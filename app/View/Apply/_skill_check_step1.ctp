                                <div class="form-group" id="step1Skill">
                                    <div class="wp-st" id="st1-skill">
                                      <div class="contact-lablel control-label"><span class="require">必須</span>スキル</div>
                                      <div class="contact-input">
                                        <p class="contact-input-txt">ご自身のスキルに当てはまるものにチェックを付けてください(複数可)</p>
                                        <ul class="contact-list">
                                          <li class="contact-list__item">
                                            <input class="contact-checkbox" id="step1-skill1" type="checkbox" name="step1skill[]" value="コールセンターで働いたことがある" 
                                            <?php
                                                   if(isset($post['step1skill'])){
                                                   if (in_array("コールセンターで働いたことがある", $post['step1skill'])) {
                                                    echo "checked";
                                                }
                                                   }
                                            ?>
                                                   >
                                            <label class="contact-checkbox-label" for="step1-skill1">コールセンターで働いたことがある</label>
                                          </li>
                                          <li class="contact-list__item">
                                            <input class="contact-checkbox" id="step1-skill2" type="checkbox" name="step1skill[]" value="営業経験がある"
                                            <?php
                                                   if(isset($post['step1skill'])){
                                                   if (in_array("営業経験がある", $post['step1skill'])) {
                                                    echo "checked";
                                                }
                                                   }
                                            ?>
                                                   >
                                            <label class="contact-checkbox-label" for="step1-skill2">営業経験がある</label>
                                          </li>
                                          <li class="contact-list__item">
                                            <input class="contact-checkbox" id="step1-skill3" type="checkbox" name="step1skill[]" value="マネージメント経験がある"
                                            <?php
                                                   if(isset($post['step1skill'])){
                                                   if (in_array("マネージメント経験がある", $post['step1skill'])) {
                                                    echo "checked";
                                                }
                                                   }
                                            ?>
                                                   >
                                            <label class="contact-checkbox-label" for="step1-skill3">マネージメント経験がある</label>
                                          </li>
                                          <li class="contact-list__item">
                                            <input class="contact-checkbox" id="step1-skill4" type="checkbox" name="step1skill[]" value="営業成績で表彰を受けたことがある"
                                            <?php                                                   if(isset($post['step1skill'])){
                                                   if(isset($post['step1skill'])){
                                                        if (in_array("営業成績で表彰を受けたことがある", $post['step1skill'])) {
                                                            echo "checked";
                                                        }
                                                   }
                                                   }
                                            ?>
                                            >
                                            <label class="contact-checkbox-label" for="step1-skill4">営業成績で表彰を受けたことがある</label>
                                          </li>
                                          <li class="contact-list__item">
                                            <input class="contact-checkbox" id="step1-skill5" type="checkbox" name="step1skill[]" value="コールセンターでSV経験がある"
                                            <?php
                                                   if(isset($post['step1skill'])){
                                                   if (in_array("コールセンターでSV経験がある", $post['step1skill'])) {
                                                    echo "checked";
                                                }
                                                   }
                                            ?>
                                            >
                                            <label class="contact-checkbox-label" for="step1-skill5">コールセンターでSV経験がある</label>
                                          </li>
                                          <li class="contact-list__item">
                                            <input class="contact-checkbox" id="step1-skill6" type="checkbox" name="step1skill[]" value="どれにも当てはまらない"
                                            <?php
                                                   if(isset($post['step1skill'])){
                                                   if (in_array("どれにも当てはまらない", $post['step1skill'])) {
                                                    echo "checked";
                                                }
                                                   }
                                            ?>
                                            >
                                            <label class="contact-checkbox-label" for="step1-skill6">どれにも当てはまらない</label>
                                          </li>
                                        </ul>
                                        <div id="error-msg--skill"></div>
                                      </div>
                                    </div>
                                  </div>
