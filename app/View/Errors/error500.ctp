
<?php $this->layout = 'error404' ?>
<div class="content">
	<div class="title">
		<a href="/"><img src="/img/logo_pc.png"></a>
		<p><span>500</span>an Internal Error Has Occurred.</p>
		<br>
		<?php
		if (Configure::read('debug') > 0):
			echo $this->element('exception_stack_trace');
		endif;
		?>
		<a href="/" class="content-link">TOPページに戻る</a>
	</div>
</div>