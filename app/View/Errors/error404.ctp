<?php $this->layout = 'error404' ?>


<div class="content">
	<div class="title">
		<a href="/"><img src="/img/logo_pc.png"></a>
		<p><span>404</span>file not found.</p>
		<br>お探しのページが見つかりません。移動した可能性があります。
		<br><a href="/" class="content-link">TOPページに戻る</a>
	</div>
</div>