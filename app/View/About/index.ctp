<?php
    $this->set('title', $this->App->getWebTitle('about'));
$breadcrumb = array('page' => 'コールナビとは？');
$this->start('meta');
    //meta description keywords image
    echo Configure::read('webconfig.meta_description');
    echo Configure::read('webconfig.meta_keywords');
    echo Configure::read('webconfig.apple_touch_icon_precomposed');
    echo Configure::read('webconfig.ogp');  
$this->end('meta');

$this->start('css');
echo $this->Html->css('about');
$this->end('css');

$this->start('header_breadcrumb');
echo $this->element('Item/breadcrumb', $breadcrumb);
$this->end('header_breadcrumb');

$this->start('sub_footer');
echo $this->element('Item/bottom_banner');
$this->end('sub_footer');
?>

<?php $this->start('sub_header'); ?>
<?php //echo $this->element('../Top/2017/_top_banner');?>
<?php $this->end('sub_header'); ?> 


<div class="about_us no-padding container ipad-padding padding-t-170">
    <div class="container no-padding">
        <div class="row_about row1">
            <h1 class="h1_title"><span>“働きやすいコールセンター”</span>が探せる求人サイト</h1>
            <p class="short">WEB上のコールセンター求人だけをまとめて検索可能にすることで、手間なく、スピーディに、あなたにとってベストなお仕事との出会いを演出。多彩なコンテンツによりあなたに新たな「気づき」を提供していくことを目指しています。</p>
        </div>
    </div>

    <div class="bg_about">
        <div class="container no-padding">
            <div class="row_about row1">
                <div class="title_about">
                    <img alt="about" src="/img/about/icon_title1.png" \>
                    <span>様々なこだわりからぴったりの仕事が見つかる</span>
                </div>
                <div class="content_about">
                    <p>コールセンター業務といっても、扱う内容や勤務時間帯などによって細分化されていきます。コールナビは様々な雇用形態、勤務時間帯、商材のお仕事を通して、あなたにぴったりの仕事・アルバイトが見つかります。</p>
                    <center><img alt="about" src="/img/about/img_ct1.png" \></center>
                </div>
            </div>
        </div>
    </div>

    <div class="container no-padding">
        <div class="row_about row1">
            <div class="title_about">
                <img alt="about" src="/img/about/icon_title2.png" \>
                <span>コールセンターで快適に働くヒントがたくさん</span>
            </div>
            <div class="content_about">
                <p>働く人の声、特集コラム、業界ニュースなど多彩なコンテンツにより毎日コールセンターの最新情</p>
                <p>報を得ることができます。</p>
                <p>仕事えらびのポイントがまるわかり！スキルアップ術なども盛りだくさん！</p>
                <center><img alt="about" src="/img/about/img_ct2.png" \></center>
            </div>
        </div>
    </div>

    <div class="bg_about">
        <div class="container no-padding">
            <div class="row_about row1">
                <div class="title_about">
                    <img alt="about" src="/img/about/icon_title3.png" \>
                    <span>応募特典付きのオリジナル求人も多彩</span>
                </div>
                <div class="content_about">
                    <p>コールナビオリジナルのオリジナル求人に応募すれば入社祝いとして、素敵な特典がついてきます！</p>
                    <center><img alt="about" src="/img/about/img_ct3.png" \></center>
                </div>
            </div>
        </div>
    </div>

    <div class="container no-padding last_about">
        <div class="row_about row1">
            <div class="title_about_last">
                <img alt="about" src="/img/about/logo_about.png" \>
                WEB上のコールセンター求人だけをまとめて検索可能にすることで、
                手間なく、スピーディに、あなたにとってベストなお仕事との出会いを演出。
            </div>
            <div class="content_about">
                <p>多彩なコンテンツによりあなたに</p>
                <p>新たな「気づき」を提供していくことを目指しています。</p>
            </div>
        </div>
    </div>
</div>