<?php $this->layout = 'single_column_v2_sp'; ?>
<?php
$this->set('title', $this->App->getWebTitle('about'));

$this->start('meta');
//meta description keywords image
echo Configure::read('webconfig.meta_description');
echo Configure::read('webconfig.meta_keywords');
echo Configure::read('webconfig.apple_touch_icon_precomposed');
echo Configure::read('webconfig.ogp');
$this->end('meta');
?>

<div style="padding-bottom: 1px;background: #f2f2f2;"> <img class="width-100 format-image" src="/img/about/about_sp.jpg" /></div>
<div><img class="width-100 format-image" src="/img/about/about_sp-02.jpg" /></div>
<div><img class="width-100 format-image" src="/img/about/about_sp-03.jpg" /></div>
<div><img class="width-100 format-image" src="/img/about/about_sp-04.jpg" /></div>
<div><img class="width-100 format-image" src="/img/about/about_sp-05.jpg" /></div>

<?php echo $this->element('../Top/_social-sp'); ?>