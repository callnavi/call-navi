<?php if(!empty($secret_result)): ?>
			<div class="secret-slider secret-box">
				<div id="secretSearchSlider" class="slider-wp">
				<div data-u="slides" class="clearfix slides">
					<?php 
						//Add more item if not enough
						$amount = count($secret_result);
						switch($amount)
						{
							case 1: 
								$secret_result[] = $secret_result[0];
								$secret_result[] = $secret_result[0];
								$secret_result[] = $secret_result[0];
								$secret_result[] = $secret_result[0];
								$secret_result[] = $secret_result[0];
								break;
							case 2: 
								$secret_result[] = $secret_result[0];
								$secret_result[] = $secret_result[1];
								$secret_result[] = $secret_result[2];
								$secret_result[] = $secret_result[3];
								$secret_result[] = $secret_result[4];
								break;
							case 3: 
								$secret_result[] = $secret_result[0];
								$secret_result[] = $secret_result[1];
								$secret_result[] = $secret_result[2];
								break;
							case 4: 
								$secret_result[] = $secret_result[0];
								$secret_result[] = $secret_result[1];
								break;
							case 5:
								$secret_result[] = $secret_result[0];
								break;
						}
					?>
				
					<?php foreach ($secret_result as $secret): ?>
					<?php 
						$value 			= $secret['CareerOpportunity'];
						$link 			= $this->Html->url(array('controller' => 'secret', 'action' => 'detail',$value['id']), true);
						$image 			= $this->webroot."upload/secret/companies/".$secret['corporate_prs']['corporate_logo'];
						$companyName 	= $secret['corporate_prs']['company_name'];
						$employment 	= $value['employment'];
						$salary 		= strip_tags($value['salary']);
						$job			= $value['job'];
						
						if(mb_strlen($job)> 35) 
						{	
							$job = mb_substr($job,0,35,'UTF-8').'...';
						}
					
						if(mb_strlen($salary)> 35) 
						{	
							$salary = mb_substr($salary,0,35,'UTF-8').'...';
						}
					
						if(mb_strlen($companyName)> 20) 
						{	
							$companyName = mb_substr($companyName,0,20,'UTF-8').'...';
						}
					
						$employmentArr = explode(',',$employment);
					?>
					<div data-p="80.125" style="display: none;">

						<div class="item">
							<div class="top-part">
								<a href="<?php echo $link; ?>">
									<img data-u="image" src="<?php echo $image; ?>" />
								</a>
							</div>
							<div class="content-part">
								<p class="i-title">
									<a href="<?php echo $link; ?>">
										<?php echo $job; ?>
									</a>
								</p>
							</div>
							<div class="bottom-part">
								<div>
									<span class="i-company"><?php echo $companyName; ?></span>

									<?php foreach ($employmentArr as $label): ?>
										<span class="i-cat"><?php echo $label; ?></span>
									<?php endforeach; ?>
								</div>
								<div>
									<span class="i-salary"><?php echo $salary; ?></span>
								</div>
							</div>
						</div>
					</div>
					<?php endforeach; ?>
				</div>
				
				<div class="container">
					<span data-u="arrowleft" class="arrow-left" data-autocenter="2"></span>
        			<span data-u="arrowright" class="arrow-right" data-autocenter="2"></span>	
				</div>
				</div>
			</div>
<?php endif; ?>