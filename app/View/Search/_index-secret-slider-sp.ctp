<li class="special-box">
	<div class="y_secret_banner_top"></div>
	<div class="secret-banner df-padding">
		<div class="display-table">
			<div class="banner-image table-cell">
				<img src="/img/logo-obi-sp.png" alt="">
			</div>
			<div class="banner-slogan table-cell">
				<strong>オリジナル求人は応募特典付き！</strong>
				<span>オリジナル求人はコールナビでしか見られない、採用特典付きお得な求人が満載☆</span>
			</div>
		</div>
	</div>
	<?php echo $this->element('../Top/_secret_slider-sp'); ?>
</li>