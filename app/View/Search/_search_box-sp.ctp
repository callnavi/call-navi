<div id="popup_search_box" class="popup-search-box">
	<div class="popup-content box-search">
		<div class="close-button" id="close_popup">
				<span class="icon-close"></span>
				<span class="title-close">close</span>
		</div>
		<div class="top-box-header">
			<div class="logo">
				<div class="image"><img src="/img/only-logo-white.png"></div>
				<div class="title">地域を選択する</div>
			</div>
		</div>
		<div class="top-box-title">
			<span class="back-to-previous-step"><img src="/img/arrow_back.png" id="back_to_previous_step"></span>
			<span class="title">地域を選択</span>
		</div>
		<div class="search-box-metal" id="box_search">
			<div class="metal-item">
				<span class="i-label">北海道・東北</span>
				<span class="i-change"></span>
			</div>
			<div class="metal-item">
				<span class="i-label">関東</span>
				<span class="i-change"></span>
			</div>
			<div class="metal-item">
				<span class="i-label">北陸・甲信越</span>
				<span class="i-change"></span>
			</div>
			<div class="metal-item">
				<span class="i-label">東海</span>
				<span class="i-change"></span>
			</div>
		</div>
	</div>
</div>