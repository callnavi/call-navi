<?php
	$currentPage = isset($params['page'])?$params['page']:1;
	$secretJobsHasInterview = $this->app->getListSecretJobsHasInterview();
?>
<div class="secret-banner-benefits df-padding mg-bottom-40"><a href="/oiwaikin"><img width="100%" src="/img/oiwaikin/bnr_top_sp.png" alt="【お祝い金】最大90,000円 採用が決まればプレゼント！"></a></div>
<div class="search-layout">
	<div class="search-result">
		<?php if ($search_total): ?>
			
			<ul class="clearfix">
				
				<?php 
					$this->App->viewMixData($search_result, 
					//job
					function($data, $index){
						if(empty($data['pic_url']))
						{
							$data['pic_url'] = $this->app->getNoImage($index);
						}
						echo '<li>';
						echo $this->element('Item/search_box_sp', $data);
						echo '</li>';
					},

					//secret
					function($data, $index, $secretJobsHasInterview){
						echo '<li class="sc-li">';
						echo $this->element('Item/secret_box_sp', array('item' => $data, 'secretJobsHasInterview' => $secretJobsHasInterview));
						echo '</li>';
					}
					,						
					//injection
					function($index){
						if($index == 6){
							echo $this->element('../Search/_index-secret-slider-sp');
							echo '<li class="y_secret_banner_bottom"></li>';
						}
					},
					$secretJobsHasInterview				
					);
				?>
				
				<?php if ($search_result['current_total'] < 6): ?>
					<?php echo $this->element('../Search/_index-secret-slider-sp'); ?>
				<?php endif; ?>
			</ul>
			<?php else: ?>
			<div style="padding:3rem;background-color: #eeeeee;">
				<span style="font-size:14px"><b>データなし</b></span>
				<div style="margin-top:3rem;">
					<p>検索のヒント:</p>
					<div style="padding-left:2rem">
						<p>キーワードに誤字・脱字がないか確認します。</p>
						<p>別のキーワードを試してみます。</p>
						<p>もっと一般的なキーワードに変えてみます。</p>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
	<?php 
	$pageCount = ceil($search_total / $size);
	if($pageCount > 1): ?>
		<?php if (count($search_result) < 6): ?>
		<div class="mg-top-30"></div>
		<?php endif; ?>
	<div class="y_pagination_sp pagination">
	<?php 
		echo $this->element('Item/y_pagination_sp', ['currentPage'=>$currentPage, 'pageCount'=>$pageCount]); 
	?>
	</div>
	<?php endif; ?>
</div>
