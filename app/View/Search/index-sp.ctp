<?php $this->layout = 'single_column_v2_sp'; ?>
    <?php 
     $this->set('title', $this->App->getSearchTitle(null));
	$this->start('css'); 	
		echo $this->Html->css('search-sp');
	$this->end('css');
?>
<?php 
$this->start('meta');
    //meta description keywords image
    echo Configure::read('webconfig.meta_description');
    echo Configure::read('webconfig.meta_keywords');
    echo Configure::read('webconfig.apple_touch_icon_precomposed');
    echo Configure::read('webconfig.ogp');  
$this->end('meta');
?>




<?php echo $this->element('../Search/_index-search-sp'); ?>
<?php echo $this->element('../Search/_index-result-sp'); ?>
<?php echo $this->element('../Top/_social-sp'); ?>
<?php echo $this->element('Item/concierge_link_banner_sp');?>

<img src="/search/updateHotKeyResult" style="display: none;">
