<?php
	//'posts/index/sort:created/direction:asc/limit:5'
	$article = $this->requestAction('Elements/getArticleRelatedJob' , array('pass' => $_keywordArr));
?>
<div class="slider-wrap">
	<div id="right-column-search" class="material-box">
		<div id="right_history_box" class="material-wapper"></div>
		<hr>
		<div class="material-related">
			<div class="material-topic">
				<?php foreach( $_keywordArr as $val)
				{
					echo '<span class="i-white-cat">'.$val.'</span>';
				} ?>
				<span><?php echo $_keywordArr?'に':''; ?>関連するおすすめ情報をご紹介！</span>
			</div>
			

			<?php if ($article): ?>
			
			<div class="related-box related-article">
				<div class="related-header">おすすめ情報！</div>
				<div class="related-list">
					<?php foreach ($article as $record): ?>
					<?php 
						  $record = $record['list']; 
						  $cateList = [];
						  $cateAias  = explode(',', $record['cat_alias']);
						  $firstCateAlias = $cateAias[0];
						  $type = $record['type'];
							$idUrl = $record['id'];
							$href = '#';
						  $image = $record['image']; 
							switch($type)
							{
								case 'news':
									//MAKE IMAGE LINK
									if (!empty($image)){
										$pos = strpos($image, "http://");
										if($pos === false)
										{
											$image = '/upload/news/'.$image;
										}
										else
										{
											$image = $this->app->proxy_image($image);
										}
									}
									//MAKE HREF
									$href = "/$type/$firstCateAlias/$idUrl";
									break;
								case 'blog':
									//MAKE IMAGE LINK
									if (!empty($image)){
										$image = $this->webroot."upload/blogs/".$image;
									}
									//MAKE HREF
									$href = "/$type/$firstCateAlias/$idUrl";
									break;
								case 'contents':
									$idUrl = $record['title_alias'];

									//MAKE IMAGE LINK
									if (!empty($image)){
										$pos = strpos($image, "ccwork");
										if($pos === false)
										{
											$image= '/upload/contents/'.$image;
										}							
									}
									else
									{
										$image = '/img/instead/img_no-image_' . sprintf("%02d", rand(1, 10)) . '.jpg';
									}
									//MAKE HREF
									$href = $this->app->createContentDetailHref($record);
									break;
							}

							
					?>
					<div class="search-box">
						<div class="display-table">
							<div class="search-image">
								<a href="<?php echo $href; ?>">
									<img src="<?php echo $image; ?>">
								</a>
							</div>

							<div class="search-content">
								<div class="top-part content-style">
									<a href="<?php echo $href; ?>"><?php echo $record['title']; ?></a>
									<div><big><?php echo $record['view']; ?></big> view</div>
								</div>
							</div>
						</div>
					</div>
					<?php endforeach; ?>
				</div>
                <div class="mg-top-30">
                    <a href="../oiwaikin"><img src="../img/oiwaikin/bnr_search_side.png" alt="【お祝い金】最大90,000円 採用が決まればプレゼント！"></a>
                </div>                
			</div>
			<?php endif; ?>
		</div>        
	</div>
</div>