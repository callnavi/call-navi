<?php
$this->start('css');
echo $this->Html->css('search');
$this->end('css');

/*Setting*/
$_keyResult = '';
$_keyResultBreadCrumb = '';
$_keyCurrent = '';
$_area = '';
$_keyword = '';
$_em = '';
// $_selectedArea = 0;
// $_selectedEm = 0;
$_selectedArea = '';
$_selectedEm = '';
$_checkedHotkey = [];
$_keywordArr = [];
$_keyResultForHeader = '';

if (!empty($params['hotkey'])) {
    $_checkedHotkey = $params['hotkey'];
    $hotkeyList = Configure::read('webconfig.hotkey_data');
    foreach ($_checkedHotkey as $key => $val) {
        $_keyResultBreadCrumb .= "「{$hotkeyList[$val]}」";
    }
}

if (!empty($params['area'])) {
    //$_area = isset($area_data[$params['area']])? $area_data[$params['area']]:'';
    $_area = isset($params['area']) ? $params['area'] : '';
    $_area = empty($_area) ? '' : str_replace(',', '/', $_area);
    $_keyResult .= "「{$_area}」";;
    $_keyResultBreadCrumb .= "「{$_area}」";
    $_keywordArr[] = $_area;
}

if (!empty($params['keyword'])) {
    $_keyword = $params['keyword'];
    $_keyResultBreadCrumb .= "「{$_keyword}」";
    $_keywordArr[] = $_keyword;
}

if (!empty($params['em'])) {
    //$_em = isset($employment_data[$params['em']])? $employment_data[$params['em']]:'';
    $_em = isset($params['em']) ? $params['em'] : '';
    if (!empty($_em)) {
        $_keyResult .= "「{$_em}」";
        $_keyResultBreadCrumb .= "「{$_em}」";
        $_keywordArr[] = $_em;
    }
}

if (!empty($params['area'])) {
    //$_selectedArea = intval($params['area']);
    $_selectedArea = $params['area'];
}

if (!empty($params['em'])) {
    //$_selectedEm = intval($params['em']);
    $_selectedEm = $params['em'];
}

$seoKeyword = $_keyResultBreadCrumb;

$arrBreadrm = [];

if (!empty($_keyResultBreadCrumb)) {
    $arrBreadrm['parent'] = [['title' => 'コールセンター求人検索結果', 'link' => '/search']];
    if (!empty($_area)) {
        $arrBreadrm['page'] = $_keyResultBreadCrumb . "の求人情報";
    } else {
        $arrBreadrm['page'] = $_keyResultBreadCrumb . "のコールセンター求人情報";
    }

} else {
    $arrBreadrm['page'] = 'コールセンター求人検索結果';
}


if (empty($currentKeyWord)) {
    $currentKeyWord = '検索条件を選択してください';
}

$currentPage = isset($params['page']) ? $params['page'] : 1;

$pageCount = ceil($search_total / $size);
$pars = http_build_query($request_query);
$setting = array(
    '__lastPage' => $pageCount,
    '__url' => '/search/ajaxSearch?' . $pars
);

if ($_keywordArr) {
    foreach ($_keywordArr as $val) {
        $_keyResultForHeader .= "「{$val}」";
    }
} else {
    $_keyResultForHeader = '';
}


$secretJobsHasInterview = $this->app->getListSecretJobsHasInterview();
?>

<?php

$this->set('title', $this->App->getSearchTitle($seoKeyword));

$this->start('meta');
//meta description keywords image
echo '<meta name="description" content= "' . htmlspecialchars($seoKeyword) . 'コールセンター・テレオペのアルバイト・バイト・求人情報を探すならコールナビ。勤務地や雇用形態、フリーワードといった様々な条件からバイト、正社員、派遣の求人情報が検索できます。コールセンター・テレオペのお仕事探しは採用実績が豊富なコールナビにお任せください！" >';
echo '<meta name="Keywords" content="コールセンター 求人,テレアポ 募集,アルバイト 高時給,コールナビ,オペレーター,アウトバウンド,' . str_replace('」「', '」,「', htmlspecialchars($seoKeyword)) . '">';
echo Configure::read('webconfig.apple_touch_icon_precomposed');
echo Configure::read('webconfig.ogp');
$this->end('meta');
?>


<?php $this->start('script'); ?>
<script type="text/javascript">
    var setting = <?php echo json_encode($setting); ?> ;
</script>

<?php
echo $this->Html->script('search_popup/search_popup.js');
echo $this->Html->script('search_history/search_history.js');
echo $this->Html->script('search_more/search__contentTemplate.js');
echo $this->Html->script('search_more/search_more.js');
echo $this->Html->script('scroll-history-box.js');
echo $this->Html->script('secret.js');
?>
<?php $this->end('script'); ?>


<?php $this->start('header_breadcrumb'); ?>
<?php echo $this->element('Item/breadcrumb', $arrBreadrm); ?>
<?php $this->end('header_breadcrumb'); ?>

<?php $this->start('sub_header'); ?>
<div class="no-padding container">
    <div class="row">
        <?php echo $this->element('Item/top_banner_search', array(
            'keyword' => $_keyword,
            'area' => $_selectedArea,
            'em' => $_selectedEm,
            'hotkey' => $_checkedHotkey,
            'total' => $search_total,

            'currentKeyWord' => $currentKeyWord,
            '_keyResult' => $_keyResult,
            'countUrl' => '/search/countcondition',

            'params' => $params,
        )); ?>
    </div>
</div>
<?php $this->end('sub_header'); ?>

<?php 
    $this->start('sub_footer');
?>
<div class="_footer"></div>
<?php 
    echo $this->element('Item/concierge_link_banner');
    $this->end('sub_footer'); 
?>

<div class="search-layout mg-top-30">
    <div class="pull-left content">
        <div id="search-result" class="search-result">
            <div class="search-header">
                <h2><?php echo $_keyResultForHeader; ?>コールセンター求人検索結果</h2>
                <div class="desc mg-top-5 mg-bottom-40">勤務地や雇用形態、フリーワードといった様々な条件からアルバイト、バイト、正社員、派遣の求人情報が検索できます。<br>
                    コールセンター・テレオペのお仕事探しは採用実績が豊富なコールナビにお任せください！
                </div>
                <p class="present">
                    <strong><?php echo $_keyResultForHeader; ?></strong>のコールセンター求人が<?php echo $search_total; ?>
                    件見つかりました。</p>
            </div>
            <div class="search-result">
                <?php if ($search_total): ?>
                    <ul class="clearfix" id="showmore_load">
                        <?php
                        $this->App->viewMixData($search_result,
                            //job
                            function ($data, $index) {
                                if (empty($data['pic_url'])) {
                                    $data['pic_url'] = $this->app->getNoImage($index);
                                }
                                echo '<li>';
                                echo $this->element('Item/search_big_box', $data);
                                echo '</li>';
                            },

                            //secret
                            function ($data, $index, $secretJobsHasInterview) {
                                echo '<li class="sc-li">';
                                echo $this->element('Item/secret_big_box', array('item' => $data, 'secretJobsHasInterview' => $secretJobsHasInterview));
                                echo '</li>';
                            },
                            null,
                            $secretJobsHasInterview
                        );
                        ?>
                    </ul>
                <?php else: ?>
                    <span style="font-size:14px"><b>データなし</b></span>
                    <div style="margin-top:40px;">
                        <p>検索のヒント:</p>
                        <div style="padding-left:20px">
                            <p>キーワードに誤字・脱字がないか確認。</p>
                            <p>別のキーワードに変更。</p>
                            <p>もっと一般的なキーワードに変更。</p>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>


        <div class="article-more mg-bottom-60">
            <?php if ($pageCount > 1): ?>
                <label data-showmore="ajax">
                    さらに表示する
                    <br>
                    <span class="icon-arrow-down"></span>
                </label>
            <?php endif; ?>
        </div>

    </div>

    <div class="pull-left slider">
        <?php echo $this->element('../Search/_index_right', array('_keywordArr' => $_keywordArr)); ?>
    </div>

    <div class="clearfix"></div>
</div>

<?php echo $this->element('Item/search_box_area_popup'); ?>

<img src="/search/updateHotKeyResult" style="display: none;">