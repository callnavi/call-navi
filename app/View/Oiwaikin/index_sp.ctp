<?php $this->layout = 'oiwaikin_sp_layout'; ?>
	<?php
$this->set('title', 'お祝い金について');
$this->start('css');
echo $this->Html->css('jquery.steps-sp_v2');
echo $this->Html->css('app_new-sp');
$this->end('css');
?>
<?php
$this->start('meta');
echo $this->Html->meta('description',  Configure::read('metatag.oiwaikin_description') );
echo $this->Html->meta('keywords',   Configure::read('metatag.oiwaikin_keyword'));
?>
<?php $this->end('meta');?>
    <div class="main-contain" id="content">
      <div class="banner">
        <div class="background" id="mainvisual--oiwaikin">
          <h1 class="main-visual"><img src="../img/oiwaikin/img_mainvisual_sp.png" alt="コールナビのオリジナル求人に応募して採用が決まれば、お祝い金最大90.000え円プレゼント！"></h1>
          <div class="btn-area">
            <div class="btn-area__item"><a class="btn-shadow" href="/secret">
                <div class="btn--primary"><img class="btn__img--search" src="../img/oiwaikin/txt_btn_search.svg" alt="さっそく求人を探す"></div></a></div>
            <div class="btn-area__item"><a class="btn-shadow" href="#application">
                <div class="btn--secondary"><img class="btn__img--apply" src="../img/oiwaikin/txt_btn_apply.svg" alt="お祝い金を申請する"></div></a></div>
          </div>
        </div>
      </div>

      <div id="contact-main">
        <article>
          <h2 class="heading--primary"><span class="heading--primary__inner">お祝い金詳細</span></h2>
          <section class="section--bg-beige">
            <div class="section__inner">
              <h3 class="heading--secondary"><span class="heading--secondary__inner">コールナビのお祝い金とは？</span></h3>
              <p>コールナビに掲載中のオリジナル求人に応募し、採用になった皆様に、<br>これからの新生活を応援したい！ という「おめでとう」の想いを込めて！<br>最大90,000円のお祝い金をお贈りしています。</p>
              <ul class="note u-mt40">
                <li class="u-fs--s">※お祝い金の金額は、応募した求人ごとに異なります。求人ページに表示されている金額をご確認ください。</li>
              </ul>
            </div>
          </section>
          <section class="section" data-trigger-menu="oiwaikin">
            <div class="section__inner">
              <h3 class="heading--secondary"><span class="heading--secondary__inner">お祝い金をもらうまでの流れ</span></h3>
              <ol class="step-box">
                <li class="step-box__item">
                  <h4 class="step-box__ttl"><img class="step-box__ttl__img" src="../img/oiwaikin/img_step_01_sp.png" alt="01 応募"></h4>
                  <p class="u-mt40">コールナビのオリジナル求人に応募</p>
                </li>
                <li class="step-box__item">
                  <h4 class="step-box__ttl"><img class="step-box__ttl__img" src="../img/oiwaikin/img_step_02_sp.png" alt="02 採用"></h4>
                  <p class="u-mt40">応募した企業で採用が決定</p>
                </li>
                <li class="step-box__item">
                  <h4 class="step-box__ttl"><img class="step-box__ttl__img" src="../img/oiwaikin/img_step_03_sp.png" alt="03 申請"></h4>
                  <p class="u-mt40">初出勤後、<a class="txt-link" href="#application">こちら</a>の申請フォームよりお祝い金を申請ください。<br>採用の確認を取らせていただきます。</p>
                </li>
                <li class="step-box__item">
                  <h4 class="step-box__ttl"><img class="step-box__ttl__img" src="../img/oiwaikin/img_step_04_sp.png" alt="04 審査"></h4>
                  <p class="u-mt40">採用確認とお祝い金贈呈に必要な勤続条件を満たしているかの確認をさせていただきます。</p>
                </li>
                <li class="step-box__item">
                  <h4 class="step-box__ttl"><img class="step-box__ttl__img" src="../img/oiwaikin/img_step_05_sp.png" alt="05 手続き"></h4>
                  <p class="u-mt40">お祝い金のご入金手続きメールをお送りいたします。</p>
                </li>
                <li class="step-box__item">
                  <h4 class="step-box__ttl"><img class="step-box__ttl__img" src="../img/oiwaikin/img_step_06_sp.png" alt="06 お祝い金"></h4>
                  <p class="u-mt40">弊社より、お祝い金の贈呈</p>
                  <ul class="note">
                    <li class="u-fs--s">※申請から贈呈までの目安は約3ヶ月になります。</li>
                  </ul>
                </li>
              </ol>
            </div>
          </section>
          <section class="section--bg-beige">
            <div class="section__inner">
              <h3 class="heading--secondary"><span class="heading--secondary__inner">応募要項</span></h3>
              <dl class="dl-list">
                <dt class="dl-list__dt">応募対象者</dt>
                <dd class="dl-list__dd">コールナビサイト内のオリジナル求人に応募して採用が決まり、勤務を開始された方</dd>
              </dl>
              <dl class="dl-list">
                <dt class="dl-list__dt">申請開始日</dt>
                <dd class="dl-list__dd">初勤務後</dd>
              </dl>
              <dl class="dl-list">
                <dt class="dl-list__dt">申請期限</dt>
                <dd class="dl-list__dd">初勤務日から30日間</dd>
              </dl>
              <dl class="dl-list">
                <dt class="dl-list__dt">適応条件</dt>
                <dd class="dl-list__dd">アルバイト・派遣：7日間　正社員：30日間の勤続が必須</dd>
              </dl>
              <dl class="dl-list">
                <dt class="dl-list__dt">申請方法</dt>
                <dd class="dl-list__dd">
                  <ol class="list-block--ol">
                    <li>初勤務を終えた後、コールナビの<a class="txt-link" href="#application">お祝い金申請フォーム</a>より申請を行ってください。</li>
                    <li>申請いただいた内容を元に、弊社にて審査を行います。審査を通過した場合、楽天銀行が提供するサービス「<a class="txt-link" href="https://www.rakuten-bank.co.jp/transfer/mailmoney/" target="_blank">かんたん振込（メルマネ）</a>」から入金手続きのメールが届きます。（審査の目安は約2～3ヶ月になります。）</li>
                    <li>楽天銀行の「<a class="txt-link" href="https://www.rakuten-bank.co.jp/transfer/mailmoney/" target="_blank">かんたん振込（メルマネ）</a>」から届いたメール内に記載されたURLにアクセスすると、お祝い金の受け取り方法を選択する画面が立ち上がりますので、案内にしたがって受け取り手続きをしてください。</li>
                    <li>受け取り手続き完了後、当日または翌営業日にお祝い金が振込まれます。</li>
                  </ol>
                </dd>
              </dl>
              <dl class="dl-list">
                <dt class="dl-list__dt">注意事項</dt>
                <dd class="dl-list__dd">
                  <ul class="list-block--ul">
                    <li>申請の権利は、応募を行い採用されたご本人様のみ有効です。譲渡はできません。</li>
                    <li>コールナビ以外の求人媒体から応募し、採用に至った場合はお祝い金の申請ができませんので、予めご了承ください。</li>
                    <li>お祝い金の料金は最大90,000円ですが、応募した求人によって異なります。求人ページに記載されている金額をご確認ください。</li>
                    <li>お祝い金申請後に弊社より応募企業に採用、勤続の事実確認を取らせていただきますので、予めご了承ください。</li>
                    <li>審査時に勤務実績を確認するため書類の提示を求める場合がございます。</li>
                    <li>審査時に採用、勤続の事実確認が取れない場合は、申請の取り消しをさせていただく場合がございます。</li>
                    <li>お祝い金のご入金方法につきましては、楽天銀行が提供するサービス「<a class="txt-link" href="https://www.rakuten-bank.co.jp/transfer/mailmoney/" target="_blank">かんたん振込（メルマネ）</a>」を通じて贈呈しています。（振込手数料は弊社負担）</li>
                    <li>お祝い金申請後にメールアドレスを変更している場合、メルマネからのメールは届きませんのでご注意ください。</li>
                    <li>振込先（口座名義、口座番号）は、応募を行い採用されたご本人様のものにしてください。もし違う場合はお手続きはできませんので、ご注意ください。</li>
                    <li>口座番号を間違えてしまいますと、再振込みはできませんので、必ず事前に正しい口座情報をご確認ください。</li>
                    <li>お祝い金の振込時期に関しては、ご申請をいただいた後から約3ヶ月を予定しております。状況により前後する場合もありますので、予めご了承ください。</li>
                    <li>お祝い金はコールナビを運営する株式会社コールナビよりお振込させていただきます。</li>
                    <li>お祝い金振込後に、万が一不正申請が発覚した場合は、お祝い金の返還を要求させていただきます。</li>
                    <li>お祝い金に関するお問い合わせは<a class="txt-link" href="../contact/">こちら</a>のフォームからお願いいたします。</li>
                  </ul>
                </dd>
              </dl>
            </div>
          </section>
          <section class="section" id="application" data-trigger-menu02="oiwaikin">
            <div class="section__inner">
              <h3 class="heading--secondary"><span class="heading--secondary__inner">お祝い金申請フォーム</span></h3>
              <p>お祝い金の贈呈は、指定の口座へお振込致します。<br>こちらのフォームに必要事項をご入力ください。</p>
              <form class="form-area" name="" action="/oiwaikin/confirm#application" method="post" accept-charset="utf-8" data-form="oiwaikin" enctype="multipart/form-data" novalidate="novalidate" >
                <dl class="form-block">
                  <dt class="form-block__ttl">
                    <div class="c-tag--require">必須</div>
                    <label for="entryId">応募ID</label>
                  </dt>
                  <dd class="form-block__content">
                    <input class="form--text" id="entryId" type="text" name="entryId" placeholder="例）C0001-CAL-J0001-A0001-E01"  value="<?php echo($post['entryId']) ?>">
                    <ul class="note u-mt20">
                      <li>※求人番号は応募完了メールに記載されています。</li>
                      <li>※半角英数字、ハイフンも省略せずにご入力ください。</li>
                    </ul>
                    <div class="error-msg" data-target-msg="error"></div>
                  </dd>
                </dl>
                <dl class="form-block">
                  <dt class="form-block__ttl">
                    <div class="c-tag--require">必須</div>初出勤完了有無
                  </dt>
                  <dd class="form-block__content">
                    <input class="form--checkbox" id="complete" type="checkbox" style="visibility:visible;" name="complete" value="初日の勤務を完了しました。">
                    <label class="form--checkbox-label" for="complete"
                <?php 
                    if(isset($post['complete']) ){
                        if($post['complete'] == "初日の勤務を完了しました。"){
                            echo("checked");
                        } 
                        
                    }
                ?>       
                                            
                           >初日の勤務を完了しました。</label>
                    <ul class="note u-mt20">
                      <li>※虚偽の申請を行った場合は、申請を無効とさせていただきます。</li>
                    </ul>
                    <div class="error-msg" data-target-msg="error"></div>
                  </dd>
                </dl>
                <dl class="form-block">
                  <dt class="form-block__ttl">
                    <div class="c-tag--require">必須</div>
                    <label for="name">お名前</label>
                  </dt>
                  <dd class="form-block__content">
                    <input class="form--text" id="name" type="text" name="name" placeholder="お名前を入力してください" value="<?php echo($post['name']) ?>">
                    <div class="error-msg" data-target-msg="error"></div>
                  </dd>
                </dl>
                <dl class="form-block">
                  <dt class="form-block__ttl">
                    <div class="c-tag--require">必須</div>
                    <label for="email">メールアドレス</label>
                  </dt>
                  <dd class="form-block__content">
                    <input class="form--text" id="email" type="email" name="mail" placeholder="例）example@callnavi.jp" value="<?php echo($post['mail']) ?>">
                    <div class="error-msg" data-target-msg="error"></div>
                  </dd>
                </dl>
                <dl class="form-block">
                  <dt class="form-block__ttl">
                    <div class="c-tag--require">必須</div>
                    <label for="company">採用企業名</label>
                  </dt>
                  <dd class="form-block__content">
                    <input class="form--text" id="company" type="text" name="company" value="<?php echo($post['company']) ?>">
                    <div class="error-msg" data-target-msg="error"></div>
                  </dd>
                </dl>
                <dl class="form-block">
                  <dt class="form-block__ttl">
                    <div class="c-tag--require">必須</div>
                    <label for="startDay">勤務開始日</label>
                  </dt>
                  <dd class="form-block__content">
                    <div class="form-input-block--date">
                      <div class="form-input-block__item">
                        <div class="form-select-triangle">
                          <select class="form--select" id="startDay_year"  name="startDay_year">
                            <option value="">--</option>
                            <option value="2018">2018年</option>
                            <option value="2017">2017年</option>
                            <option value="2016">2016年</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-input-block__item">
                        <div class="form-select-triangle">
                          <select class="form--select" id="startDay_month" name="startDay_month">
                            <option value="">--</option>
                            <option value="1">1月</option>
                            <option value="2">2月</option>
                            <option value="3">3月</option>
                            <option value="4">4月</option>
                            <option value="5">5月</option>
                            <option value="6">6月</option>
                            <option value="7">7月</option>
                            <option value="8">8月</option>
                            <option value="9">9月</option>
                            <option value="10">10月</option>
                            <option value="11">11月</option>
                            <option value="12">12月</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-input-block__item">
                        <div class="form-select-triangle">
                          <select class="form--select" id="startDay_day" name="startDay_day">
                            <option value="">--</option>
                            <option value="1">1日</option>
                            <option value="2">2日</option>
                            <option value="3">3日</option>
                            <option value="4">4日</option>
                            <option value="5">5日</option>
                            <option value="6">6日</option>
                            <option value="7">7日</option>
                            <option value="8">8日</option>
                            <option value="9">9日</option>
                            <option value="10">10日</option>
                            <option value="11">11日</option>
                            <option value="12">12日</option>
                            <option value="13">13日</option>
                            <option value="14">14日</option>
                            <option value="15">15日</option>
                            <option value="16">16日</option>
                            <option value="17">17日</option>
                            <option value="18">18日</option>
                            <option value="19">19日</option>
                            <option value="20">20日</option>
                            <option value="21">21日</option>
                            <option value="22">22日</option>
                            <option value="23">23日</option>
                            <option value="24">24日</option>
                            <option value="25">25日</option>
                            <option value="26">26日</option>
                            <option value="27">27日</option>
                            <option value="28">28日</option>
                            <option value="29">29日</option>
                            <option value="30">30日</option>
                            <option value="31">31日</option>
                          </select>
                        </div>
                      </div>
                    </div>
                    <div class="error-msg" data-target-msg="error"></div>
                  </dd>
                </dl>
                <dl class="form-block">
                  <dt class="form-block__ttl">
                    <div class="c-tag--require">必須</div>
                    <label for="account">振込口座名義（カナ）</label>
                  </dt>
                  <dd class="form-block__content">
                    <div class="form-input-block--half">
                      <div class="form-input-block__item">
                        <input class="form--text" id="account" type="text" name="account_lastName" placeholder="セイ"  value="<?php echo($post['account_lastName']) ?>">
                      </div>
                      <div class="form-input-block__item">
                        <input class="form--text" type="text" name="account_firstName" placeholder="メイ" value="<?php echo($post['account_firstName']) ?>">
                      </div>
                    </div>
                    <ul class="note u-mt20">
                      <li>※振込先は必ず応募を行い採用されたご本人様のものにしてください。</li>
                      <li>※詳細な振込先情報は審査完了後に楽天銀行の「<a class="txt-link" href="https://www.rakuten-bank.co.jp/transfer/mailmoney/" target="_blank">かんたん振込（メルマネ）</a>」から確認を取らせていただきます。</li>
                    </ul>
                    <div class="error-msg" data-target-msg="error"></div>
                  </dd>
                </dl>
                <dl class="form-block">
                  <dt class="form-block__ttl">
                    <label for="remarks">備考欄</label>
                  </dt>
                  <dd class="form-block__content">
                    <p>応募した求人内容から採用条件が変更になったなど、何か追記事項等ございましたらご記入ください</p>
                    <textarea class="form--textarea u-mt10" id="remarks" name="remarks" rows="4" placeholder="例）求人にはインバウンドのアルバイトのお仕事と書いてありましたが、アウトバウンドで正社員の仕事内容での採用になりました。"><?php echo($post['remarks']) ?></textarea>
                  </dd>
                </dl>
                <div class="u-tac u-mt30">
                  <p class="u-tal"><a class="txt-link" href="../company/">個人情報の取り扱い</a>に同意の上、確認画面に進んでください。</p>
                  <label class="agreement-checkbox-label" for="agreement">
                    <input class="agreement-checkbox" id="agreement" type="checkbox" name="agreement" value=""><span>個人情報の取り扱いに同意する</span>
                  </label>
                  <div class="error-msg" id="agreementError" data-target-msg="error"></div>
                </div>
                <div class="confirm-button"><a class="btn-shadow" data-form-submit="comfirm">
                    <div class="btn--secondary">確認画面へ進む</div></a></div>
              </form>
            </div>
          </section>
        </article>
        <div class="btn-area--compliance is_hide" data-target-menu="oiwaikin">
          <div class="btn-area__item"><a class="btn-shadow" href="/secret">
              <div class="btn--primary"><img class="btn__img--search" src="../img/oiwaikin/txt_btn_search.svg" alt="さっそく求人を探す"></div></a></div>
          <div class="btn-area__item"><a class="btn-shadow" href="#application">
              <div class="btn--secondary"><img class="btn__img--apply" src="../img/oiwaikin/txt_btn_apply.svg" alt="お祝い金を申請する"></div></a></div>
        </div>
      </div>
      <section class="bg-gray">
        <ul class="banner-list">
          <li class="banner-list__item"><a href="https://share.callnavi.jp/" target="_blank"><img src="../img/secret/bnr_top_callshare_sp.png" alt=""></a></li>
          <li class="banner-list__item"><a href="../secret/%E3%80%90%E6%97%A9%E7%A8%B2%E7%94%B0%E5%A4%A7%E3%81%8B%E3%82%89%E3%82%B9%E3%82%B0%EF%BC%81%E3%80%91%E6%9C%8D%E8%A3%85%E9%AB%AA%E5%9E%8B%E8%87%AA%E7%94%B1%E2%98%85%E3%82%B7%E3%83%95%E3%83%88%E8%87%AA%E7%94%B1%E3%81%A7%E7%A9%BA%E3%81%8D%E6%99%82%E9%96%93%E3%81%AB%E3%82%B5%E3%82%AF%E3%83%83%E3%81%A8%E7%A8%BC%E3%81%92%E3%82%8B%E2%99%AA" target="_blank"><img src="../img/secret/secret_sp.png" alt="早大生×高田馬場 コールセンターで働こう！！"></a></li>
          <li class="banner-list__item"><a href="../pretty/" target="_blank"><img src="../img/pretty_sp.png" alt="コールセンター美男美女図鑑"></a></li>
        </ul>
      </section>      
        <!--
      <div class="social-box df-padding">
          comment data in issues#345
        <div class="fb-column">
          <div class="advertisement__item"><a href="../pretty"><img src="../img/pretty_sp.png"></a></div>
          <div class="advertisement__item"><a href="../callcenter_matome"><img src="../img/map_sp.png"></a></div>
          <div class="advertisement__item"><img src="../img/sns_sp.png">
            <div class="fb-like" data-href="https://callnavi.jp" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
          </div>
        </div>
      
        <div class="twitter-button clearfix"><span class="description">「コールナビ」Twitterアカウント</span><a class="pull-right" target="_blank" href="https://twitter.com/intent/tweet?url=https://callnavi.jp/&amp;via=コールナビ&amp;text=コールセンター求人数日本No.1サイト、コールナビ。最新の求人情報とお役立ち記事が満載！&amp;related=コールナビ"></a>
          <script>
            !function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0],
                    p = /^http:/.test(d.location) ? 'http' : 'https';
                if (!d.getElementById(id)) {
                    js = d.createElement(s);
                    js.id = id;
                    js.src = p + '://platform.twitter.com/widgets.js';
                    fjs.parentNode.insertBefore(js, fjs);
                }
            }(document, 'script', 'twitter-wjs');
            
          </script>
        </div>
      </div>
        -->
    </div>          

 
    
    



			<?php
$this->start('script');
	echo $this->Html->script('jquery.cookie-1.3.1.js');
	echo $this->Html->script('jquery.validate.min.js');
	echo $this->Html->script('apply/apply-v3-sp.js');
    echo $this->Html->script('app_new');
?>
<script>
    <?php
    if($post['startDay_day'] != ""){
    ?>
        $('#startDay_day option[value=<?php echo($post['startDay_day']); ?>]').attr('selected','selected'); 
    <?php
       }
    ?>
    <?php
    if($post['startDay_month'] != ""){
    ?>
        $('#startDay_month option[value=<?php echo($post['startDay_month']); ?>]').attr('selected','selected'); 
    <?php
       }
    ?>
    <?php
    if($post['startDay_year'] != ""){
    ?>
        $('#startDay_year option[value=<?php echo($post['startDay_year']); ?>]').attr('selected','selected'); 
    <?php
       }
    ?>   
    </script>
<?php
    
$this->end('script');
?>