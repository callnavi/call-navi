<?php
/**
 * Application level View Helper
 *
 * This file is application-wide helper file. You can put all
 * application-wide helper-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Helper
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Helper', 'View');
App::uses('UtilComponent', 'Controller/Component');
App::uses('MixSearchComponent', 'Controller/Component');
App::uses('SessionHelper', 'View/Helper');
App::import("Model", "Contents");
App::import("Model", "CareerOpportunity");
App::import("Model", "CorporatePrs");
/**
 * Application helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       app.View.Helper
 */
class CodeHelper extends Helper {
	public function codeGenerated($FirstString, $lenCode, $value){
        $val = $value;
        $len = $lenCode;

        while(strlen($val) < $len -1){
            $val = "0" . $val;
        }
        $val = $FirstString . $val;
        return $val;
    }
    public function CheckIdAvailable($table, $column, $id_val){
        
        $flag = true;
        if($table == "corporate_prs"){
            $QueryCheck = new CorporatePrs();
            
        }elseif($table == "career_opportunities"){
             $QueryCheck = new CareerOpportunity();
        }
        
        
        $check =  $QueryCheck->query("SELECT count(*) as count FROM ". $table ." where ". $table .".". $column ." = '".$id_val."';");
        
        if($check[0][0]['count'] > 0){
            $flag = false;
        }
        return $flag;
    }
    public function genClassificationData($apply_code){
        // get string of rank first number
         $checkClass = substr($apply_code, 22 ,1);
        
        // check rank first number
        if( $checkClass == "E"){
            $classification = "エキスパート";
        }elseif( $checkClass == "N"){
            $classification = "ノーマル";
        }elseif( $checkClass == "B"){
            $classification = "ビギナー";
        }else{
            $classification = "";
        }
        return $classification;
    }        
 
}