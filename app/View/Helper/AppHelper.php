<?php
/**
 * Application level View Helper
 *
 * This file is application-wide helper file. You can put all
 * application-wide helper-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Helper
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Helper', 'View');
App::uses('UtilComponent', 'Controller/Component');
App::uses('MixSearchComponent', 'Controller/Component');
App::uses('SessionHelper', 'View/Helper');
App::import("Model", "Contents");
App::import("Model", "CareerOpportunity");
App::import("Model", "CorporatePrs");
App::import("Model", "CareerOpportunity");
/**
 * Application helper
 *
 * Add your application-wide methods in the class below, your helpers
 * will inherit them.
 *
 * @package       app.View.Helper
 */
class AppHelper extends Helper {
	
     public $helpers = array('Code');
	public function getShareLink($link,$id,$day=1)
	{
		$collection = new ComponentCollection();
		$util = new UtilComponent($collection);
		return $util->getShareLink($link,$id,$day);
	}
	
	public function _convertDateTimeToMinuteBefore($datetime) {
		$datetime = strtotime($datetime);
		if (time() - $datetime <= 3600) {
			return floor((time() - $datetime) / 60) . '分前';
		} elseif (time() - $datetime <= 24 * 60 * 60) {
			return floor((time() - $datetime) / (60 * 60)) . '時間前';
		} elseif (time() - $datetime <= 31 * 24 * 60 * 60) {
			return floor((time() - $datetime) / (24 * 60 * 60)) . '日前';
		} elseif (time() - $datetime <= 12 * 31 * 24 * 60 * 60) {
			return floor((time() - $datetime) / (31 * 24 * 60 * 60)) . 'ヶ月前';
		} else {
			return floor((time() - $datetime) / (12 * 31 * 24 * 60 * 60)) . '年前';
		}
	}

	function general_url($page)
	{
		$uri = strtok($_SERVER['REQUEST_URI'],'?');
		$request = $_REQUEST;
		$request['page']=$page;
		return $uri.'?'.http_build_query($request);
	}

	public function jobUrlEncode($unique_id)
	{
		$collection = new ComponentCollection();
		$util = new UtilComponent($collection);

		return $util->jobUrlEncode($unique_id);
	}

	public function proxy_image($url)
	{
		return '/proxy?url='.rawurlencode($url);
	}
	
	public function createLabel($dataString, $className='',$exChar=',')
	{
		$html = '';
		$arrString = explode($exChar, $dataString);
		foreach( $arrString as $index => $string)
		{
			if(!empty($string))
			{
				$html.= '<span class="'.$className.'">'.$string.'</span>';
			}
		}
		return $html;
	}
	
	public function getNoImageIndex($currentPage)
	{
		if($currentPage <= 1)
		{
			return 1;
		}
		else
		{
			$currentPage = $currentPage - 1;
		}

		if(CakeSession::check('no_image_index_page_'.$currentPage))
		{
			return CakeSession::read('no_image_index_page_'.$currentPage);
		}
		return 1;
	}
	
	public function setNoImageIndex($currentPage, $noImageIndex)
	{
		CakeSession::write('no_image_index_page_'.$currentPage, $noImageIndex);
	}
	
	public function getNoImage($no_image_index=1)
	{
		$no_image_index=$no_image_index%21;
		$no_image_index=$no_image_index==0?1:$no_image_index;
		return '/img/instead/img_no-image_' . sprintf("%02d",$no_image_index) . '.jpg';
	}
	
	public function isNew($dateTime, $inHours=3)
	{
		$isNew = false;
		$diff = microtime(true) - strtotime($dateTime); 
		if($diff < $inHours)
		{
			$isNew = true;
		}
		return $isNew;
	}
	
	public function getContentImageSrc($image)
	{
		if (!empty($image)){
			$pos = strpos($image, "ccwork");
			if($pos === false)
			{
				$image= '/upload/contents/'.$image;
			}							
		}
		return $image;
	}

	public function meta_ogp(&$obj, $title = '', $description = '', $keywords = '', $url = '', $image = '', $canonical = false)
	{
		$obj->start('meta');
	    //meta description keywords image
	    //echo Configure::read('webconfig.meta_description');
			//echo Configure::read('webconfig.meta_keywords');
	    echo Configure::read('webconfig.apple_touch_icon_precomposed');
	    echo Configure::read('webconfig.ogp');

	    if($title == '')
	    {
	    	$title = $obj->get('title');
	    }
	    if($description == '')
	    {
	    	$description = Configure::read('webconfig.ogp_description');
	    }
	    else
	    {
	    	$description = strip_tags($description);
	    }
	    if($keywords == '')
	    {
	    	$keywords = Configure::read('webconfig.ogp_keywords');
	    }
	    if($url == '')
	    {
	    	$url = $obj->Html->url(null, true);
	    }
	   	if($image == '')
	   	{
	   		$image = Configure::read('webconfig.ogp_image');
	   	}
	    $arr_ogp_meta = [
				'title' 			=> $title,
				'description' 		=> $description,
				'keywords'			=> $keywords,
				'url'				=> $url,
				'image'				=> $image,
				'canonical'			=> $canonical
			];

	    echo $obj->element('Header/ogp_seo', $arr_ogp_meta);
		$obj->end('meta');
	}

    public function getZoomNumber($callcenter = null)
    {
        $zoom = 6;
        if(!empty($callcenter)){
            if(count($callcenter) ==1){
                $zoom = 17;
            }
        }
        return $zoom;
    }
	
	//--HREF--------------------
	/*
	$data = [
		[category] => 働く人の声,
		[category_alias] => work
	]
	 */
	public function createContentCategoryHref($data)
	{
		return '/contents/'.$data['category'];
	}
	
	/*
	$data = [
		[title] => 沖縄に移住したい！？　それなら那覇市のコールセンターがおすすめですよ！,
		[id] => 594,
		[cat_names] => エリア特集,
		[cat_alias] => japanesecallcenter,
		[title_alias] => okinawa_callcenter
	]
	 */
	public function createContentDetailHref($data)
	{
		//2017-06-15 add English link for 2 contents
		if($data['id'] == 525 || $data['id'] == 502 || $data['id'] == 516 || $data['id'] == 664)
		{
			return "/contents/{$data['cat_alias']}/{$data['title_alias']}";
		}
		
		return '/contents/'.$this->_urlencode($data['title']);
	}
    public function createContentInterDetailHref($data)
	{
        
        $contents = new Contents();
		
        $results  = $contents->find('first', array(
        'conditions' => array('title' => $data['title'])
        ));;
        
        //pr('/contents/'.$data['title']);
        $Secret_id = $results['Contents']['secret_job_id'];
        
        if($Secret_id == "" || $Secret_id == "0"){
            return '/contents/'.$this->_urlencode($data['title']);
            
        }else{
            return '/secret/'.$this->_urlencode($data['title']);
            
        }
		//return '/contents/'.$data['title'];
		//return '/contents/'.rawurlencode($data['title']);
		//return '/contents/'.$data['cat_names'].'「'. rawurlencode($data['title']) .'」';
	}
	
	/*
	$data = [
		[id] => 1,
		[job] => 東京本社で新メンバー積極募集中★ＩＴの専門知識が身に付くお仕事です！！
	]
	 */
	public function createSecretDetailHref($data)
	{
		//return '/secret/'.'「'. $data['job'] .'」';
		return '/secret/'.rawurlencode($data['job']);
	}
	
	//---------------------------
	public function getWebTitle($key)
	{
		$titleList = Configure::read('webconfig.web_title');
		if(isset($titleList[$key]))
		{
			return $titleList[$key];
		}
		return $titleList['default'];
	}

	public function getSearchTitle($data){
		if(!empty($data)){
			return $data.'のコールセンター求人検索結果 ｜ コールセンター特化型求人サイト「コールナビ」';
		}else{
			$titleList = Configure::read('webconfig.web_title');
			return $titleList['search'];
		}
	}


	public function getSecretDetailBreadcrumbPage($data)
	{
		return $data['job'].'の求人情報';
	}
	
    public function getSecretDetailTitle($data)
	{
		$titleList = Configure::read('webconfig.web_title');
		if(isset($titleList['secret_detail']))
		{
			return str_replace('「title」', $data['job'], $titleList['secret_detail']);
		}
		return $titleList['default'];
	}
    
    public function SecretDetailTitle1($data)
	{
		$titleList = Configure::read('webconfig.web_title');
		if(isset($titleList['secret_detail']))
		{
			return str_replace('「title」', $data['job'], $titleList['secret_detail']);
		}
		return $titleList['default'];
	}    
    
	public function getContentsCategoryTitle($categoryName)
	{
		$titleList = Configure::read('webconfig.web_title');
		if(isset($titleList['contents_category']))
		{
			return str_replace('「title」', $categoryName, $titleList['contents_category']);
		}
		return $titleList['default'];
	}



	public function getContentsDetailTitle($data)
	{
		$titleList = Configure::read('webconfig.web_title');
		if(isset($titleList['contents_detail']))
		{
			return str_replace('「title」', $data['Contents']['title'] , $titleList['contents_detail']);
		}
		return $titleList['default'];
	}

	
	//------------------Support VIEW
	public function viewMixData($data, $callbackJob, $callbackSecret, $callbackInjection = null, $checkSecretHasInterview = null)
	{
		$current_total = $data['current_total'];
		$total_job = $data['current_job'];
		$total_secret = $data['current_secret'];
		$index_job = 0;
		$index_secret = 0;
		$isInjection = $callbackInjection != null && is_callable($callbackInjection);
		
		
		for($i=0; $i<$current_total; $i++)
		{
			if($isInjection)
			{
				$callbackInjection($i);
			}
			
			if( ($i%5 == 0 && $index_secret < $total_secret) || ($index_job == $total_job && $index_secret < $total_secret) )
			{
				$callbackSecret($data['data_secret'][$index_secret], $index_secret, $checkSecretHasInterview);
				$index_secret++;
			}
			else
			{
				if($index_job < $total_job)
				{
					$callbackJob($data['data_job'][0][$index_job], $index_job);
					$index_job++;
				}
			}
		}
	}

	public function echoScriptScrollColumnJs()
	{
		//if(!stripos($_SERVER['HTTP_USER_AGENT'],"iPad"))
		//{
			echo '<script type="text/javascript" src="/js/scroll-column.js"></script>';
		//}
	}

	public function echoScriptScrollSidebarJs()
	{
		//if(!stripos($_SERVER['HTTP_USER_AGENT'],"iPad"))
		//{
			echo '<script type="text/javascript" src="/js/scroll-sidebar.js"></script>';
		//}
	}

	public function echoScriptScrollHistoryBoxJs()
	{
		//if(!stripos($_SERVER['HTTP_USER_AGENT'],"iPad"))
		//{
			echo '<script type="text/javascript" src="/js/scroll-history-box.js"></script>';
		//}
	}


	public function replaceCharacter2Byte($source)
	{
		$source = str_replace('【' , '[' , $source);
		$source = str_replace('】' , ']' , $source);
		$source = str_replace('「' , '[' , $source);
		$source = str_replace('」' , '[' , $source);
		$source = str_replace('（' , '(' , $source);
		$source = str_replace('）' , ')' , $source);
		return $source;
	}

	public function countJobToday()
	{
		$collection = new ComponentCollection();
		$util = new UtilComponent($collection);
		
		return $util->countJobToDay();
	}

	public function countTotalJob()
	{
		$collection = new ComponentCollection();
		$mixSearch = new MixSearchComponent($collection);

		return $mixSearch->getTotal();
	}
	
	public function _urlencode($value)
	{
		$find = array(
			'%',
			' ',  
			'"',	
			'#',	
			'&',	
			"'",	
			'+',	
			'.',	
			'/',	
			'?',	
			'^',	
			'`',	
			'{',	
			'|',	
			'}',
			'\\',
		);	

		$replace = array(
			'%25',
			'%20',
			'%22',
			'%23',
			'%26',
			'%27',
			'%2B',
			'%2E',
			'%2F',
			'%3F',
			'%5E',
			'%60',
			'%7B',
			'%7C',
			'%7D',
			'%5C',
		);
		
		return str_replace($find, $replace, $value);
	}

	public static function getListSecretJobsHasInterview()
	{
		$contents = new Contents();
		$results = $contents->getListSecretJobsHasInterview();

		return $results;
	}
    public function orderData($data, $list , $callbackJob)
	{
		$callbackJob(0, $list);
	}
    public function CompanyPR($comID)
	{
        $CorporatePrs = new CorporatePrs();
		$results = $CorporatePrs->find('first', array(
        'conditions' => array('id' => $comID)
    ));;

    //new getting data for corporate logo

		return $results;
	}
    public function jobPR($jobID)
	{
        $CareerOpportunity = new CareerOpportunity();
		$results = $CareerOpportunity->find('first', array(
        'conditions' => array('id' => $jobID)
    ));;
    	return $results;
	}
        
    public function arrayGetInter($data){
        /* divide the array */
        $count = count($data);
        $arrayInter = array();
        $arrayNotInter = array();
        $returnArr = array();
        for($i=0;$i<$count;$i++){
            if($data[$i]['Contents']['category_alias'] == 'interview'){
                $arrayInter[] = $data[$i];                
            }else{
                $arrayNotInter[] =  $data[$i];
            }
        }
        
        /* making variable for count array */
        $run = 0;
        $runNotinter = 0;
        $runinter = 0;
        $countArr = count($arrayInter);
        $countNotArr = count($arrayNotInter);

        /*input New Array*/
        while($runNotinter <  $countNotArr or $runinter <  $countArr){
            if($run%5 == 0){
                if($runinter <  $countArr){
                $returnArr[] = $arrayInter[$runinter];
                $runinter += 1;
                }
                $run += 1;
                
            }else{
                if($runNotinter <  $countNotArr){
                $returnArr[] = $arrayNotInter[$runNotinter];
                $runNotinter += 1;
                }
                $run += 1;

            }
        }
        
        return $returnArr;
    }
	
	public function ContentsDetailTitle($data)
	{
		$titleList = Configure::read('webconfig.web_title');
		if(isset($titleList['contents_detail']))
		{
			return str_replace('「title」', $data['Contents']['title'] , $titleList['secret_detail']);
		}
		return $titleList['default'];
	}
	
	public function ContentsDetailTitle2($data)
	{
		$titleList = Configure::read('webconfig.web_title');
		if(isset($titleList['contents_detail']))
		{
			return str_replace('「title」', $data['Contents']['title'] , $titleList['contents_detail']);
		}
		return $titleList['default'];
	}	

    
    // funtion to make code string
    public function returnValue($first_char, $charlen, $valueNew ){
        $value = $valueNew;
        while(strlen($value) < $charlen - 1){
            $value = "0" . $value;
        }
        $value = $first_char . $value;
        return $value;
    }
    
    // Function For generate New Company ID
    public function generateNewCompanyCode(){
        
        $CorporatePrs = new CorporatePrs();
		$results = $CorporatePrs->query("SELECT max(corporate_code) as max_corporate_code FROM corporate_prs;");
        
        //Declare variable for not error null
        
        $result_value = "";
        $return_result = "";
        
            
        if(strlen($results[0][0]['max_corporate_code']) > 4){
            $result_value = (substr($results[0][0]['max_corporate_code'],1,4));
            do {            
                
                //make new result from max id
                $result_value = $result_value + 1;
                
                // Make the format of the code string
                $return_result = $this->Code->codeGenerated("C","5",$result_value);        

            // validation if the code alread have or not
            } while ($this->Code->CheckIdAvailable("corporate_prs","corporate_code",$return_result) == false);    
        }
       
        return $return_result;
    }    

    // Function For generate New job code addd new validation for can more than 2 job code
    // the function moce to manage job controller


    
    public function generateNewJobCode(){
        $CareerOpportunity = new CareerOpportunity();
		$results = $CareerOpportunity->query("SELECT max(job_code) as max_job_code FROM career_opportunities;");
        $result_value = "";
        $return_result = "";
        
            
        if(strlen($results[0][0]['max_job_code']) > 4){
            $result_value = (substr($results[0][0]['max_job_code'],1,4));
            do {            
                //make new result from max id
                $result_value = $result_value + 1;
                 
                // Make the format of the code string
                $return_result = $this->Code->codeGenerated("J","5",$result_value);  
                
             

            } while ($this->Code->CheckIdAvailable("career_opportunities","job_code",$return_result) == false );    
        }
        
        return $return_result;

    }     
    public function PictureGetTop($company_id){
        $img_val = '' ;
        
        $CorporatePrs = new CorporatePrs();
		$results = $CorporatePrs->find('first', array(
        'conditions' => array('id' => $company_id)
        ));;
            
        return $results['CorporatePrs'];
    }
    public function TopinterviewLink($data){
        $contents = new Contents();
		$results = $contents->topInterviewGetId($data['id']);
        $href  = "";
        if(isset($results[0]['Contents']['title'])){
            $href = "/secret/".$results[0]['Contents']['title'];
        }else{
            $href = "/secret/".$data['job'];
        }
		return $href;
    }


}