<?php
    $this->set('title', 'コールナビ｜'.$list['ContentsEmail']['Title']);
	$this->start('css');
        echo $this->Html->css('detail');
    $this->end('css');
// add meta ogt
$this->start('meta');
$courseimage_fb ='';
if (!empty($list['ContentsEmail']['featured_picture'])) {
    $pos = strpos($list['ContentsEmail']['featured_picture'], "http://");
    if ($pos !== false)
        $courseimage_fb = $list['ContentsEmail']['featured_picture'];
    else
        $courseimage_fb = $this->Html->url('/', true) . 'upload/news/' . $list['ContentsEmail']['featured_picture'];
}
    echo $this->Html->meta('canonical', $this->Html->url(null, true), array('rel'=>'canonical', 'type'=>null, 'title'=>null));
echo $this->Html->meta('description',$list['ContentsEmail']['Title'] );
echo $this->Html->meta('keywords',$list['ContentsEmail']['Title'] );

?>
    <meta property='og:type' content='website'/>
    <meta property='og:site_name' content='コールナビ'/>
    <meta property='og:title' content='<?php echo 'コールナビ｜'.$list['ContentsEmail']['Title'] ;?>'/>
    <meta property='og:image' content='<?php echo $courseimage_fb?>'/>
    <meta property='og:url' content='<?php echo $this->Html->url(null, true); ?>'/>
    <meta property='og:description' content=''/>
<?php $this->end('meta');?>

<div class="topis">
    <p class="detail_subtitle">NEWS TOPICS<span class="detail_subtitle_innner">
        <a href="<?php echo $this->Html->url(array('controller' => 'news', 'action' => 'index', 'category_alias'=>$list['category']['category_alias']), true);?>"><?php echo $list['category']['category'];?></a></span>
        </p>
        <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 number">
            <?php $CreateDate = new DateTime($list['ContentsEmail']['send_date']);?>
            <p>
                <?php echo $CreateDate->format('Y.m.d');?> <img class="view_eye" src="/img/view.png">VIEW
                <?php echo $list['ContentsEmail']['view'];?>
            </p>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 titlehed">
           <h1><?php echo $list['ContentsEmail']['Title'];?></h1>
<!--			<h2>より効果的な英語習得法の「How to」を手の中に のコピー</h2>-->
        </div>
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="link">
            <?php $newsDetailHref = $this->Html->url(array('controller' => 'news', 'action' => 'detail','category_alias'=>$list['category']['category_alias'],'slug'=> $list['ContentsEmail']['id']), true); ?>
            <div class="fb-like" data-href="<?php echo $newsDetailHref ;?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
            <a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
            <script>
                ! function (d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0],
                        p = /^http:/.test(d.location) ? 'http' : 'https';
                    if (!d.getElementById(id)) {
                        js = d.createElement(s);
                        js.id = id;
                        js.src = p + '://platform.twitter.com/widgets.js';
                        fjs.parentNode.insertBefore(js, fjs);
                    }
                }(document, 'script', 'twitter-wjs');
            </script>
        </div>

    </div>
</div>
<div class="col-xs-12 detailfix">
    
    <div class="col-xs-12 contentsp detailsp">
        <div class="col-xs-12 contentspborder">
        </div> 		
		  <?php if(!empty($list['ContentsEmail']['Excerpt'])){ ?>
				<div class="col-xs-12 comment">
					<p><?php echo $list['ContentsEmail']['Excerpt'];?></p>
				</div>
         <?php } ?>

		<div class="col-xs-12 content1">
			<?php echo $list['ContentsEmail']['content'];?>
		</div>

		 <div class="col-xs-12 content1">
			<?php echo $list['ContentsEmail']['relation_link'];?>
		</div>

        <div class="col-xs-12 content1 detail-source">
            コンテンツ引用元：<?php echo $list['ContentsEmail']['source'];?>
        </div>

    </div>
	<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <div class="link link-but">
            <div class="fb-like" data-href="<?php echo $newsDetailHref ;?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
            <a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
            <script>
                ! function (d, s, id) {
                    var js, fjs = d.getElementsByTagName(s)[0],
                        p = /^http:/.test(d.location) ? 'http' : 'https';
                    if (!d.getElementById(id)) {
                        js = d.createElement(s);
                        js.id = id;
                        js.src = p + '://platform.twitter.com/widgets.js';
                        fjs.parentNode.insertBefore(js, fjs);
                    }
                }(document, 'script', 'twitter-wjs');
            </script>
        </div>
    </div>
    <p class="titlesp">「<?php echo $list['category']['category'];?>」に関連する記事</p>
    <?php echo $this->element('Item/news_similar_aticles_sp',
        ['category'=>  $list['category']['category_alias']]
    );?>
</div>