<?php 
$this->set('title', 'コールナビ｜コールセンターの最新ニュースを配信');
$this->start('script');
    // echo $this->Html->script('scroll-column.js');
    // echo $this->Html->script('left-menu.js');
$this->end('script');

switch($catCurr) {
    case 'callcenter':
        $title = 'コールセンター';
        break;
    case 'it':
        $title = 'IT通信';
        break;
    // case 'smartphone':
    //     $title = 'スマホ';
    //     break;
    case 'insurance':
        $title = '保険';
        break;
    case 'electric':
        $title = '電力';
        break;
    // case 'estate':
    //     $title = '不動産';
    //     break;
    default:
        $title = 'ニュース';
        break;
}
if($title == 'ニュース')
{
	$breadcrumb = array('page' => 'ニュース');
}
else
{
	$breadcrumb = array('page' => $title, 
						'parent' => array(
											array('link'=>'/news/', 'title'=>'ニュース')
										 )
					   );
}

$this->start('meta');
    echo $this->Html->meta('canonical', $this->Html->url(null, true), array('rel'=>'canonical', 'type'=>null, 'title'=>null));
    //meta description keywords image
    echo Configure::read('webconfig.meta_description');
    echo Configure::read('webconfig.meta_keywords');
    echo Configure::read('webconfig.apple_touch_icon_precomposed');
    echo Configure::read('webconfig.ogp');  
$this->end('meta');

$this->start('sub_footer');
	echo $this->element('Item/bottom_banner');
$this->end('sub_footer');
?>

<?php $this->start('sub_header'); ?>
<div class="no-padding container">
	<div class="row">
		<?php echo $this->element('Item/top_banner_version2');?>
	</div>
</div>
<?php $this->end('sub_header'); ?>
	
<?php echo $this->element('Item/breadcrumb', $breadcrumb);?>

<div class="article">
	<div class="list-header non-type">
		<h1>コールセンターの求人情報や業界ニュースなど、役立つ情報が満載</h1>
	</div>

	<div class="cat-list">
		<ul>
			<?php foreach($catList as $cat){
				$param = $cat['Categories'];
			?>
			<li><span>▷</span> 
				<a href="/news/<?php echo $param['category_alias']; ?>"><?php echo $param['category'] ;?>  (<?php echo $param['article_amount'] ;?>)</a>
			</li>	
			<?php } ?>

		</ul>

	</div>

	<div class="article-list">
		<?php echo $this->element('Item/article_list', array('list'=>$list));?>
	</div>

	<?php echo $this->element('Item/pagination', ['currentPage' => $currentPage, 'pageCount' => $pageCount]); ?>
</div>