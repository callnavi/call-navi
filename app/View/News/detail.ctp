<?php 
$title 		= $list['ContentsEmail']['Title'];
$CreateDate = new DateTime($list['ContentsEmail']['send_date']);
$newsDetailHref = $this->Html->url(array('controller' => 'news', 'action' => 'detail',
										 'category_alias'=>$list['category']['category_alias'],
										 'slug'=> $list['ContentsEmail']['id']), 
								   true);
$this->set('title', 'コールナビ｜'.$title);
$breadcrumb = $breadrumLink;
?>
<?php $this->start('script'); ?>
<!-- <script type="text/javascript" src="/js/scroll-column.js"></script> -->
<!-- <script>
$(window).load(function () {
	var stopPoint = $('#stopScrollPoint').offset().top;
	if ($('#right-menu').length > 0) {
		var keywordHeight = $('.contentnew .saycontent').outerHeight(true) * -1.37;
		var scrollRight = new scrollColumn($('#right-menu'), stopPoint, 'right-menu-fix', keywordHeight);

	}
});
</script> -->
<?php $this->end('script');
// add meta ogt
$this->start('meta');
echo $this->Html->meta('canonical', 
					   $this->Html->url(null, true), 
					   array('rel'=>'canonical', 'type'=>null, 'title'=>null));
echo $this->Html->meta('description',$title );
echo $this->Html->meta('keywords',$title );

$courseimage_fb ='';
if (!empty($list['ContentsEmail']['featured_picture'])) {
	$pos = strpos($list['ContentsEmail']['featured_picture'], "http://");
	if ($pos !== false)
		$courseimage_fb = $list['ContentsEmail']['featured_picture'];
	else
		$courseimage_fb = $this->Html->url('/', true) . 
							'upload/news/' . 
							$list['ContentsEmail']['featured_picture'];
}
?>
	<meta property='og:type' content='website'/>
	<meta property='og:site_name' content='コールナビ'/>
	<meta property='og:title' content='<?php echo 'コールナビ｜'.$title ;?>'/>
	<meta property='og:image' content='<?php echo $courseimage_fb?>'/>
	<meta property='og:url' content='<?php echo $this->Html->url(null, true); ?>'/>
	<meta property='og:description' content=''/>
<?php $this->end('meta');?>

<?php $this->start('sub_header'); ?>
<div class="no-padding container">
	<div class="row">
		<?php echo $this->element('Item/top_banner_version2');?>
	</div>
</div>
<?php $this->end('sub_header'); ?> 

<?php echo $this->element('Item/breadcrumb', $breadcrumb);?>


<?php if(!empty($isPreview)){ ?>
	<?php echo $this->element('Item/btn_publish', array('status' => $list['ContentsEmail']['status'])); ?>
<?php } ?>

<div class="article-detail">
	<div class="list-header non-type">
		<h1><?php echo $title;?></h1>
	</div>
	
	<div class="article-accessory">
		<div class="clearfix">
			<div class="pull-left">
				<div class="social-buttons">
					<div class="fb-like" style="padding-right: 10px;" data-href="<?php echo $newsDetailHref ;?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
					<a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
					<script>
						! function (d, s, id) {
							var js, fjs = d.getElementsByTagName(s)[0],
								p = /^http:/.test(d.location) ? 'http' : 'https';
							if (!d.getElementById(id)) {
								js = d.createElement(s);
								js.id = id;
								js.src = p + '://platform.twitter.com/widgets.js';
								fjs.parentNode.insertBefore(js, fjs);
							}
						}(document, 'script', 'twitter-wjs');
					</script>
				</div>
			</div>
			<div class="pull-right">
				<time class="datetime"><?php echo $CreateDate->format('Y.m.d');?></time>
				<span class="view"><big><?php echo $list['ContentsEmail']['view'];?></big>view</span>
			</div>
		</div>
	</div>
	<div class="article-main">
		<?php if(!empty($list['ContentsEmail']['Subtitle'])){ ?>
<!--
		<div class="article-subtitle">
			<h2><?php echo $list['ContentsEmail']['Subtitle'];?></h2>
		</div>
-->
		<?php } ?>
	
		<?php if(!empty($list['ContentsEmail']['Excerpt'])){ ?>
		<div class="article-excerpt">
			<p><?php echo $list['ContentsEmail']['Excerpt'];?></p>
		</div>
		<?php } ?>
		
		<div class="article-content">
			<?php echo $list['ContentsEmail']['content'];?>
		</div>

		<div class="article-relation-link">
			<?php echo $list['ContentsEmail']['relation_link'];?>
		</div>
		<div class="article-source">
			コンテンツ引用元：<?php echo $list['ContentsEmail']['source'];?>
		</div>
	</div>
</div>

<div class="article-relation">
	<div class="relation-header">関連する記事</div>
	<div class="relation-list">
		<?php echo $this->element('Item/news_similar_aticles',
				   ['category'=>  $list['category']['category_alias']]
		);?>
	</div>
</div>
