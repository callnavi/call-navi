<?php 
$this->set('title', 'コールナビ｜コールセンターの最新ニュースを配信');
$this->start('script'); 
	echo $this->Html->script('news-sp.js');
$this->end('script');


switch($catCurr) {
    case 'callcenter':
        $title = 'コールセンター';
        break;
    case 'it':
        $title = 'IT通信';
        break;
    case 'smartphone':
        $title = 'スマホ';
        break;
    case 'insurance':
        $title = '保険';
        break;
    case 'electric':
        $title = '電力';
        break;
    case 'estate':
        $title = '不動産';
        break;
    default:
        $title = 'ニュース';
        break;
}

$this->start('meta');
    echo $this->Html->meta('canonical', $this->Html->url(null, true), array('rel'=>'canonical', 'type'=>null, 'title'=>null));
    //meta description keywords image
    echo Configure::read('webconfig.meta_description');
    echo Configure::read('webconfig.meta_keywords');
    echo Configure::read('webconfig.apple_touch_icon_precomposed');
    echo Configure::read('webconfig.ogp');  
$this->end('meta');
?>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 newtop">
    <div class="col-xs-12 col-sm-10 col-md-12 col-lg-12">
        <p class="detail_title"><strong class="ptop">NEWS TOPICS</strong><span class="detail_title_innner"><?php echo $title ;?></span></p>
    </div>

    <div class="col-xs-12 col-sm-10 col-md-12 col-lg-12">
        <div class="row">
            <div class="custom-news-select">
                <select name="" id="category">
                    <option value="" >ニュース TOP</option>
                    <?php foreach($catList as $cat){
                        $param = $cat['Categories'];
                        $selected = !empty($param['category_alias']) && $catCurr == $param['category_alias'] ? ' selected="selected" ' : '';
                        ?>
                        <option <?php echo $selected;?>  value="<?php echo $param['category_alias'];?>"><?php echo $param['category'] ;?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>

</div>

<div id="wrapper-content" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 contentnew">
            <p class="last">「<?php echo $title ;?>」の一覧<span class="span1"><?php echo $total ;?>件中 <?php echo $numberStartEnd['start'] ;?> - <?php echo $numberStartEnd['end'] ;?>件を表示 </span></p>

            <?php foreach($list as $item){
                $catIds = explode(',',$item['group_cat']['cat_ids']);
                $catNames = explode(',',$item['group_cat']['cat_names']);
                $catAlias = explode(',',$item['group_cat']['cat_alias']);
                $CreateDate = new DateTime($item['econtents']['created']);
                $newsDetailHref =$this->Html->url(array('controller' => 'news', 'action' => 'detail','category_alias'=>$catAlias[0],'slug'=> $item['econtents']['id']), true);
                $title = strlen($item['econtents']['Title'])> 25 ? mb_substr(  $item['econtents']['Title'],0,25,'UTF-8').'...' : $item['econtents']['Title'] ;
			?>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3  sayimg ">
                    <div>
                        <?php if (!empty($item['econtents']['featured_picture'])){
                                    $pos = strpos($item['econtents']['featured_picture'], "http://");
                                    if($pos !== false)
									{
										$src = $this->app->proxy_image($item['econtents']['featured_picture']);
									}
									else
                                        $src= '/upload/news/'.$item['econtents']['featured_picture'];
                        ?>

                            <img src="<?php echo $src ;?>" />
                        <?php } ?>
                    </div>
                    <?php if($item[0]['totalHour']<= 3){?>
                            <p class="new-article" >New</p>
                    <?php } ?>
                </div>
                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 saytext ">
                    <span class="created"><?php echo $CreateDate->format('Y年m月d日'); ?></span>
                    <a href="<?php echo $newsDetailHref;?>">
                    <h4><?php echo $title ;?> </h4></a>

                    <?php for($x = 0; $x < count($catAlias); $x++):
                        $href = '/news/'.$catAlias[$x];
                    ?>

                        <a href="<?php echo $href; ?>" style="float: left">
                            <p>
                                <?php echo $catNames[$x];?>
                            </p>
                        </a>
                            
                    <?php endfor;?>
                        <span>VIEW <?php echo $item['econtents']['view']?></span>
                </div>
                <!-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 contentborder"></div> -->
            <?php }?>
            
            
                
	    	<?php echo $this->element('Item/pagination_sp', ['currentPage' => $currentPage, 'pageCount' => $pageCount]); ?>
	    
        </div>
</div>
<script>
    $(document).ready(function(){
        $('#category').on('change', function() {
            window.location.href = "/news/" + this.value;
        });
    });
</script>