<?php
$currentPage = $this->Paginator->params()['page'];
$pageCount = $this->Paginator->params()['pageCount'];
$this->set('title', 'コールナビ｜コールセンターのさまざまな情報を配信');



$this->start('css');
echo $this->Html->css('contents');
$this->end('css');

//ogp meta
$this->app->meta_ogp($this);

switch($curCat) {
    case "work" :
        $title = '働く人の声';
        break;
    case "skill":
        $title = 'スキル・テクニック';
        break;
    case "saiyou":
        $title = '採用・面接';
        break;
    case "business":
        $title = 'ビジネス・キャリア';
        break;
    case "topics":
        $title = '注目トピックス';
        break;
    case "japanesecallcenter":
        $title = 'エリア特集';
        break;
    default:
        $title = '特集記事';
        break;
}
if($title == '特集記事')
{
	$breadcrumb = array('page' => '特集記事');
}
else
{
	$breadcrumb = array('page' => $title, 
						'parent' => array(
											array('link'=>$curCat, 'title'=>'特集記事')
										 )
					   );
}

$this->start('sub_footer');
	echo $this->element('Item/bottom_banner');
$this->end('sub_footer');
?>

<?php $this->start('sub_header'); ?>
<?php echo $this->element('../Top/2017/_top_banner');?>
<?php $this->end('sub_header'); ?> 

<?php $this->start('header_breadcrumb'); ?>
	<?php echo $this->element('Item/breadcrumb', $breadcrumb);?>
<?php $this->end('header_breadcrumb'); ?>

<?php $this->append('script'); ?>
	<script type="text/javascript">
		var setting = {};
		setting.__page = 1;
		setting.__lastPage  = <?php echo $pageCount; ?>;
		setting.__url  = "/ajax/contents/<?php echo $curCat; ?>";
	</script>
    <script type="text/javascript" src="/js/scrolling_page/content__contentTemplate.js"></script>
    <script type="text/javascript" src="/js/scrolling_page/scrolling_page.js"></script>
	<?php $this->app->echoScriptScrollSidebarJs(); ?>
    <script type="text/javascript">
		$(window).load(function () {
			var stopPoint = $('#stopScrollPoint').offset().top;
			if ($('#right-column').length > 0 && $('#left-column').length > 0) {
				var scrollRight = new scrollColumn($('#right-column'), stopPoint, 0);
			}
		});
	</script>
<?php $this->end('script'); ?>


<div class="article">

    <div class="pick-up-row mg-top-30">
        <?php echo $this->element('../Contents/_pick_up_row', array('catList' => $catList));?>
    </div>
	<div class="article-list mg-bottom-60">
		<?php echo $this->element('../Contents/_article_list', array('list'=>$list));?>
	</div>

	<?php //echo $this->element('Item/pagination', ['currentPage' => $currentPage, 'pageCount' => $pageCount]); ?>
	<div style="position: absolute;bottom: 0;" id="stopScrollPoint"></div>
</div>
 