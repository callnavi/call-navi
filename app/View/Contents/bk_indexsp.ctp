<?php 
$this->set('title', 'コールナビ｜コールセンターのさまざまな情報を配信');
$this->start('script'); 
	echo $this->Html->script('news-sp.js');
$this->end('script');

$this->start('meta');
    //meta description keywords image
    echo Configure::read('webconfig.meta_description');
    echo Configure::read('webconfig.meta_keywords');
    echo Configure::read('webconfig.apple_touch_icon_precomposed');
    echo Configure::read('webconfig.ogp');  
$this->end('meta');

switch($curCat) {
    case "work" :
        $title = '働く人の声';
        break;
    case "skill":
        $title = 'スキル・テクニック';
        break;
    case "saiyou":
        $title = '採用・面接';
        break;
    case "business":
        $title = 'ビジネス・キャリア';
        break;
    case "topics":
        $title = '注目トピックス';
        break;
    case "japanesecallcenter":
        $title = 'エリア特集';
        break;
    default:
        $title = '特集';
        break;
}
?>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 newtop">
    <div class="col-xs-12 col-sm-10 col-md-12 col-lg-12">
        <p class="detail_title"><strong class="ptop">CONTENTS</strong><span class="detail_title_innner"><?php echo $title ;?></span></p>
    </div>

    <div class="col-xs-12 col-sm-10 col-md-12 col-lg-12">
        <div class="row">
            <div class="custom-news-select">
                <select name="" id="category">
                    <option value="">特集 TOP</option>
                    <?php foreach($catList as $cat){
                        $param = $cat['Categories'];
                        $selected = !empty($param['category_alias']) && $curCat == $param['category_alias'] ? ' selected="selected" ' : '';
                        ?>
                        <option <?php echo $selected; ?> value="<?php echo $param['category_alias'];?>"><?php echo $param['category'] ;?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
    </div>

</div>

<div id="wrapper-content" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 ">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 contentnew">
            <p class="last">「<?php echo $title ;?>」の一覧<span class="span1"><?php echo $total ;?>件中 <?php echo $numberStartEnd['start'] ;?> - <?php echo $numberStartEnd['end'] ;?>件を表示 </span></p>

            <?php foreach($list as $item){
                $CreateDate = new DateTime($item['Contents']['created']);?>
                <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3  sayimg ">
                    <div>
                        <a href="<?php echo "/contents/".$item['Contents']['category_alias'].'/'.$item['Contents']['title_alias'];?>">
                            <?php if (!empty($item['Contents']['image'])){
                                $pos = strpos($item['Contents']['image'], "ccwork");
                                if($pos !== false)
                                    $src = $item['Contents']['image'];
                                else
                                    $src= '/upload/contents/'.$item['Contents']['image'];
                                ?>
                                <img src="<?php echo $src ;?>"/>
                            <?php } ?>
                        </a>
                    </div>
                    <?php if($item[0]['totalHour']<= 3){?>
                            <p class="new-article" >New</p>
                    <?php } ?>
                </div>
                <div class="col-xs-9 col-sm-9 col-md-9 col-lg-9 saytext ">
                <span class="created"><?php echo $CreateDate->format('Y年m月d日'); ?></span>
                    <!--<a href="<?php //echo $this->Html->url(array('controller' => 'contents', 'action' => 'detail', 'category_alias'=>$item['Contents']['category_alias'], 'title_alias'=>$item['Contents']['title_alias']), true);?>"><h4><?php //echo strlen($item['Contents']['title'])> 30 ? mb_substr(  $item['Contents']['title'],0,30,'UTF-8').'...' : $item['Contents']['title'] ;?> </h4></a> -->
                    <a href="<?php echo "/contents/".$item['Contents']['category_alias'].'/'.$item['Contents']['title_alias'];?>">
						<h4><?php echo strlen($item['Contents']['title'])> 30 ? mb_substr(  $item['Contents']['title'],0,30,'UTF-8').'...' : $item['Contents']['title'] ;?> </h4>
					</a>
                    <a href="<?php echo $this->Html->url(array('controller' => 'contents', 'action' => 'index', 'category_alias'=>$item['Contents']['category_alias']), true);?>" style="float: left">
                        <p><?php echo $item['category']['category'] ;?></p>
                    </a>
                    <span>VIEW <?php echo $item['Contents']['view']?></span>
                </div>
                <!-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 contentborder"></div> -->
            <?php }?>
            <div  class="col-xs-12 col-sm-12 col-md-12 col-lg-12 prev">
                <div class="next-prev-block">
                    <?php
                        if($this->Paginator->hasPage(null,2)){
                            echo $this->Paginator->prev('PREV', null, null, array('class' => 'button1')); //Shows the next and previous links
                    ?>

                    <?php
                            echo $this->Paginator->next('NEXT', null, null, array('class' => 'button3')); //Shows the next and previous links
                        }
                    ?>
                </div>

                <div class="pagination-number" >
                <?php
                    if($this->Paginator->hasPage(null,2)){
                        $currentPage = $this->Paginator->current();
                        $lastPage = $this->Paginator->counter('{:pages}');
                        if($currentPage >= 4)
                        {
                            echo " &nbsp; ".$this->Paginator->first(" 1 ", array('class' => 'button2'))."  &nbsp; ";
                            echo "•••" ;
                        }
                        echo " &nbsp; ".$this->Paginator->numbers(array('modulus' => 2 , 'class' => 'button2','separator'=> " &nbsp; "))."  &nbsp; "; //Shows the page numbers
                        if($lastPage >= $currentPage+4)
                        {
                            echo "•••" ;
                            echo " &nbsp; ".$this->Paginator->last(" ".$this->Paginator->counter('{:pages}')." ", array('class' => 'button2'))."  &nbsp; ";
                        }
                    }
                ?>
                </div>
            </div>
        </div>
</div>
<script>
    $(document).ready(function(){

        $('#category').on('change', function () {
            window.location.href = "/contents/" + this.value;
        });
    });
</script>