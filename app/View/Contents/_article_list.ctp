<?php
$this->append('script');
	echo $this->Html->script('lazyload/jquery.lazyload.js');
	echo $this->Html->script('lazyload/lazyload_setup.js');
$this->end('script');

//Default
$article_base_url = '/contents/';
$article_table = 'Contents';
$article_title = 'title';
$article_image = 'image';
$article_view = 'view';
$article_created = 'created';
$article_image_pos_compare = "ccwork";
$article_image_base_url = "/upload/contents/";
$article_id_url = 'title_alias';
?>
<div class="main-list">
    <ol id="scroll_load">
        <?php foreach ($list as $item):

            $title = $item[$article_table][$article_title];
            $org_title = $title;
            $image = $item[$article_table][$article_image];
            $view = $item[$article_table][$article_view];
            $isNew = $item[0]['totalHour'] <= 3;
            $idUrl = $item[$article_table][$article_id_url];

            $dt = new DateTime($item[$article_table][$article_created]);
            $createDate = $dt->format('Y.m.d');

            if (mb_strlen($title) > 28) {
                $title = mb_substr($title, 0, 28, 'UTF-8') . '...';
            }

            $cateList = array();
            $cateNames = array($item['category']['category']);
            $cateAlias = array($item[$article_table]['category_alias']);
            $firstCateAlias = $cateAlias[0];


            //MAKE IMAGE LINK
            if (!empty($image)) {
                $pos = strpos($image, $article_image_pos_compare);
                if ($pos === false) {
                    $image = $article_image_base_url . $image;

                } else {
                    $image = $image;
                }
            } else {
                $image = '/img/instead/img_no-image_' . sprintf("%02d", rand(1, 10)) . '.jpg';
            }

            //MAKE CATEGORY
            foreach ($cateNames as $i => $cate) {
                if (isset($cateAlias[$i])) {
                    $cateList[] = array(
                        'href' => $article_base_url . $cateAlias[$i],
                        'name' => $cate
                    );
                }
            }
            //MAKE HREF
            $href = $article_base_url . $firstCateAlias . '/' . $idUrl;
            ?>
            <li>
               	<?php echo $this->element('Item/item_dark_lazy_load', array(
					'href' => $href,
					'org_title' => $org_title,
					'image' => $image,
					'view' => $view,
					'createDate' => $createDate,
					'isNew' => $isNew,
					'cateList' => $cateList,
					'title' => $title,
				)); ?>
            </li>
        <?php endforeach; ?>
    </ol>
</div>