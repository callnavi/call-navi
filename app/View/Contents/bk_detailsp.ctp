<?php
     $this->set('title', 'コールナビ｜'.$list['Contents']['title']);
    $this->start('css');
        echo $this->Html->css('base');
        echo $this->Html->css('ccwork');
        echo $this->Html->css('contents');
        echo $this->Html->css('format');
    $this->end('css');  
$this->start('script'); ?>
    <script type="text/javascript" src="/js/scroll-column.js"></script>
    <script>
        $(window).load(function () {
            if ($('#right-menu').length > 0) {
                var stopPoint = $('#mainContents').offset().top + $('#mainContents').outerHeight();
                var scrollRight = new scrollColumn($('#right-menu'), stopPoint, 'right-menu-fix', -192);
            }
        });
    </script>
    <?php $this->end('script'); ?>
        <div class="topis">
            <p class="detail_subtitle">CONTENTS<span class="detail_subtitle_innner">
        <a href="<?php echo $this->Html->url(array('controller' => 'contents', 'action' => 'index', 'category_alias'=>$list['Contents']['category_alias']), true);?>"><?php echo $list['category']['category'];?></a></span>
            </p>
            <div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 number">
                <?php $CreateDate = new DateTime($list['Contents']['created']);?>
                    <p>
                        <?php echo $CreateDate->format('Y.m.d');?>
                    </p>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 titlehed">
                <h1><?php echo $list['Contents']['title'];?></h1>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="link">
                    <?php $contentsDetailHref = $this->Html->url(array('controller' => 'contents', 'action' => 'detail','category_alias'=>$list['Contents']['category_alias'],'title_alias'=> $list['Contents']['title_alias']), true); ?>
                    <div class="fb-like" data-href="<?php echo $contentsDetailHref ;?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
                    <a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
                    <script>
                        ! function (d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0]
                                , p = /^http:/.test(d.location) ? 'http' : 'https';
                            if (!d.getElementById(id)) {
                                js = d.createElement(s);
                                js.id = id;
                                js.src = p + '://platform.twitter.com/widgets.js';
                                fjs.parentNode.insertBefore(js, fjs);
                            }
                        }(document, 'script', 'twitter-wjs');
                    </script>
                </div>
            </div>
        </div>




        <div id="contents">
            <div class="contentsInner contents-sp">
                <div id="contentsContainer">
                    <div id="mainContents">
                        <!-- InstanceBeginEditable name="mainContents" -->

                        <section class="entryA01 ccwork_entry">
                            <!--                             <header>
                                <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">基礎知識</span></div>
                                <h1>忙しいママこそ、<br>コールセンターの仕事が向いている９つの理由</h1>
                            </header> -->

                            <section class="sect01">
                                <?php echo $list['Contents']['description'];?>
                                <?php echo $list['Contents']['content'];?>
                            </section>

                            <!--                             <aside>
                                <dl>
                                    <dt class="headingG01">「コールセンター基礎知識」オススメのページ</dt>
                                    <dd>
                                        <ul class="linkListA01">
                                            <li><a href="/ccwork/detail82">後悔しないためのコールセンター選び５つのポイント～時給、場所、休憩スペース･･･</a></li>
                                            <li><a href="/ccwork/detail45">テレアポ・テレオペ・テレマの違いって？自分にピッタリの仕事を見つける！</a></li>
                                        </ul>
                                    </dd>
                                </dl>
                            </aside>

                            <aside class="tabNav_ccwork">
                                <ul>
                                    <li>
                                        <a href="/ccwork#tabContents01"><img src="/img/test/6.png" alt="基礎知識" class="mediaPC" height="" width="192"><img src="/img/test/5.png" alt="基礎知識" class="mediaSP" height="" width="286"></a>
                                    </li>
                                    <li>
                                        <a href="/ccwork#tabContents02"><img src="/img/test/4.png" alt="知っ得情報" class="mediaPC" height="" width="192"><img src="/img/test/3.png" alt="知っ得情報" class="mediaSP" height="" width="286"></a>
                                    </li>
                                    <li>
                                        <a href="/ccwork#tabContents03"><img src="/img/test/2.png" alt="働く人の声" class="mediaPC" height="" width="192"><img src="/img/test/1.png" alt="働く人の声" class="mediaSP" height="" width="286"></a>
                                    </li>
                                </ul>
                            </aside> -->
                            <!-- / tabNav -->

                        </section>



                        <div class="pickupConteiner">
                        </div>
                        <!-- pickupConteiner -->

                        <!-- InstanceEndEditable -->

                    </div>
                    <!-- / mainContents -->



                </div>
                <!-- / asideContents -->

<!--                 <p class="titlesp">「
                    <?php echo $list['category']['category'];?>」に関連する記事</p>
                <?php echo $this->element('Item/content_similar_aticles',
        ['category'=>  $list['Contents']['category_alias']]
    );?> -->
                    
                    <div id="pageTop" style="display: none;" class="">
                        <a href="#globalHeader" class="smoothScroll"><img src="/public/images/common/pagetop_img_01.gif" width="63" height="63" alt="pagetop"></a>
                    </div>
            </div>
            <!-- / contentsInner -->

            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="link link-but">
                    <div class="fb-like" data-href="<?php echo $contentsDetailHref ;?>" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
                    <a href="https://twitter.com/share" class="twitter-share-button">Tweet</a>
                    <script>
                        ! function (d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0]
                                , p = /^http:/.test(d.location) ? 'http' : 'https';
                            if (!d.getElementById(id)) {
                                js = d.createElement(s);
                                js.id = id;
                                js.src = p + '://platform.twitter.com/widgets.js';
                                fjs.parentNode.insertBefore(js, fjs);
                            }
                        }(document, 'script', 'twitter-wjs');
                    </script>
                </div>
            </div>

                <p class="titlesp">「
                    <?php echo $list['category']['category'];?>」に関連する記事</p>
                <?php echo $this->element('Item/content_similar_aticles_sp',
        ['category'=>  $list['Contents']['category_alias']]
    );?>
        </div>