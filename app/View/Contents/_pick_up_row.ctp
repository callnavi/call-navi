<?php
	$article = $this->requestAction('elements/getPickUp');
	$curCat = !empty(explode('/',$_SERVER["REQUEST_URI"])[2]) ? explode('/',$_SERVER["REQUEST_URI"])[2] : null; 
?>
<div class="main-list">
    <ol>
        <li class="second">
            <div class="list-header non-type">
                <h1>特集記事</h1>
            </div>
            <div class="cat-list">
                <ul>
                    <li>
                        <a class="<?php echo empty($curCat) ? 'active' : null; ?> " href="/contents/"><span>#</span> すべての記事</a>
                    </li>
                    <?php foreach ($catList as $cat) {
                        $param = $cat['Categories'];
                        $href = $this->app->createContentCategoryHref($param);
						$active = urldecode($curCat) == $param['category'] ? 'active' : null;
                        ?>
                        <li>
                            <a class="<?php echo $active; ?>" href="<?php echo $href; ?>"><?php echo '<span>#</span> '.$param['category']; ?></a>
                        </li>
                    <?php } ?>
                </ul>

            </div>
        </li>

    </ol>
</div>