<div class="article-list">
	<ul class="fullcolor-list" id="show_more_content">
<?php
    $arrInter = $this->App->arrayGetInter($list);
?>
<?php foreach ($arrInter as $item): ?>
	<?php
		$CreateDate = new DateTime($item['Contents']['created']);
		$createDate = $CreateDate->format('Y年m月d日');

		if($item[0]['totalHour']<= 3)
			$isNew = true;
		else
			$isNew = false;

		$view = $item['Contents']['view'];

		$title = $item['Contents']['title'];
		$data['id'] = $item['Contents']['id'];
        $data['title'] = $item['Contents']['title'];
        $data['title_alias'] = $item['Contents']['title_alias'];
        $cats_names = $item['category']['category'];
        $cat_alias = $item['Contents']['category_alias'];
        $data['cat_names'] = $cats_names;
        $data['cat_alias'] = $cat_alias;
		//$href = "/contents/".$item['Contents']['category_alias'].'/'.$item['Contents']['title_alias'];
            if($item['Contents']['company_id'] == '' || $item['Contents']['company_id'] == '0'){
                $href = $this->app->createContentDetailHref($data);                
            }else{
                $href = $this->app->createContentInterDetailHref($data);
            }
	?>
	<li class="df-padding">
			<div class="article-box">
				<div class="display-table">
					<div class="search-image">
						<a href="<?php echo $href;?>">
							<?php 
                            if($item['Contents']['company_id'] == '' || $item['Contents']['company_id'] == '0'){    
                                if (!empty($item['Contents']['image'])){
                                $pos = strpos($item['Contents']['image'], "ccwork");
                                if($pos !== false)
                                    $image = $item['Contents']['image'];
                                else
                                    $image= '/upload/contents/'.$item['Contents']['image'];
                                 } 
                            }else{
                                    
                                    //change the logo to carreroportunity database
                                    $imgLink = $this->App->jobPR($item['Contents']['secret_job_id']);
                                    if(isset($imgLink['CareerOpportunity']['corporate_logo'])){
                                        $image = "/upload/secret/jobs/".$imgLink['CareerOpportunity']['corporate_logo'];        
                                    }else{
                                        $image = "/upload/secret/jobs/";
                                    }
                            }
                            ?>
                            
                            
                            <img src="<?php echo ( $image); ?>"/>
						</a>
					</div>

					<div class="search-content">
						<div class="top-part clearfix">
							<div class="pull-left">
								<time><?php echo $createDate; ?></time>
								<?php if($isNew){?>
									<span class="new-label">NEW</span>
								<?php } ?>
							</div>
							<div class="pull-right" style="<?php if($item['category']['category'] == '企業インタビュー'){ echo('display:none;'); } ?>">
								<big><?php echo $view; ?></big> view
							</div>
						</div>
						<div class="middle-part">
							<a href="<?php echo $href; ?>"><?php echo $title; ?></a>
						</div>
						<div class="bottom-part text-right">
							<?php 
								$dataHrefCat = [];
								$dataHrefCat['category'] = $cats_names;
								$dataHrefCat['category_alias'] = $cat_alias;
								$hrefCat = $this->app->createContentCategoryHref($dataHrefCat);
							?>
							<a href="<?php echo $hrefCat;?>"><?php echo $item['category']['category'] ;?></a>
							
						</div>
					</div>
				</div>
			</div>
		</li>
<?php endforeach; ?>
	</ul>
</div>