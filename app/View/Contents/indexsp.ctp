<?php $this->layout = 'single_column_v2_sp'; ?>
<?php 

if($list){
    if ($curCat) {
        $categoryName = isset($list[0]['category']['category']) ? $list[0]['category']['category'] : '';
        if ($categoryName) {
            $this->set('title', $this->app->getContentsCategoryTitle($categoryName));
        }
    } else {
        $this->set('title', $this->app->getWebTitle('contents'));
    }
    
}else{
    $this->set('title', $this->app->getContentsCategoryTitle($category));
}


//ogp meta
$this->app->meta_ogp($this);

switch($curCat) {
    case "work" :
        $title = '働く人の声';
        break;
    case "skill":
        $title = 'スキル・テクニック';
        break;
    case "saiyou":
        $title = '採用・面接';
        break;
    case "business":
        $title = 'ビジネス・キャリア';
        break;
    case "topics":
        $title = '注目トピックス';
        break;
    case "japanesecallcenter":
        $title = 'エリア特集';
        break;
    default:
        $title = '特集';
        break;
}
?>

<div class="banner">
    <div class="background">
        <img src="/img/banner/top_contents_sp_banner.jpg">
    </div>
    <div class="middle-content banner-title">
        <div class="banner-title-wp">
            <h1>特集記事</h1>
            <h2>コールセンターの業界ニュースなど役立つ情報が満載</h2>
        </div>
    </div>
</div>

<?php echo $this->element('../Contents/_article_cat_nav-sp'); ?>

<?php echo $this->element('../Contents/_mainList-sp'); ?>

<?php echo $this->element('../Contents/_article_content_template-sp'); ?>


<?php if ($totalPage > 1): ?>
<div class="show-more df-padding mg-top-40">
    <a 	href="javascript:void(0)" 
    	class="lazy-loading-page btn-blue-3d"
    	data-total="<?php echo $totalPage; ?>"
    	data-page="1"
    	data-link="/ajax/contents/<?php echo $curCat; ?>"
    	data-append-to="#show_more_content"
    	data-template="#content-template"
    >もっと見る</a>
    <div class="loading-effect">
    	<span class="loading-icon"></span>
    </div>
</div>
<?php endif; ?>

<?php echo $this->element('../Top/_pickup-sp'); ?>
<?php echo $this->element('../Top/_social-sp'); ?>
<?php echo $this->element('Item/concierge_link_banner_sp');?>
