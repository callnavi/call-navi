<?php
$title = $list['Contents']['title'];
$breadcrumb = $breadrumLink;
$CreateDate = new DateTime($list['Contents']['created']);
$contentsDetailHref = $this->Html->url(array('controller' => 'contents',
    'action' => 'detail',
    'category_alias' => $list['Contents']['category_alias'],
    'title_alias' => $list['Contents']['title_alias']),
    true);
$this->set('title', $this->App->getContentsDetailTitle($list));
$this->start('css');
echo $this->Html->css('base');
echo $this->Html->css('ccwork');
echo $this->Html->css('contents');
echo $this->Html->css('format');
echo $this->Html->css('content-detail');
$this->end('css');

$courseimage_fb = '';
$article_image_base_url = '/upload/contents/';
$article_image_pos_compare = "ccwork";
$image = $list['Contents']['image'];
$pos = strpos($image, $article_image_pos_compare);
if ($pos === false) {
    $courseimage_fb = FULL_BASE_URL . $article_image_base_url . $image;
} else {
    $courseimage_fb = FULL_BASE_URL . $image;
}
//ogp meta
$this->app->meta_ogp(
    $this,
    //title
    $title,
    //decription
    $list['Contents']['meta_description'],
    //keywords
    $list['Contents']['meta_keyword'],
    //url
    $this->Html->url(null, true),
    //image
    $courseimage_fb,
    true
);
?>

<?php $this->start('sub_header'); ?>
<?php 
// delete in new design #345
//echo $this->element('../Top/2017/_top_banner'); ?>
<?php $this->end('sub_header'); ?>

<?php
$this->start('header_breadcrumb');
echo $this->element('Item/breadcrumb', $breadcrumb);
$this->end('header_breadcrumb');

$this->start('sub_footer'); ?>
<div class="bg-gray-2 article-relation">
    <div class="container ipad-padding-sub mg-top-50 no-padding">
        <!--	<div class="relation-header">関連する記事</div>-->
        <div class="relation-header-2">> recommend contents</div>
        <div class="relation-list">
            <?php echo $this->element('Item/content_similar_aticles',
                ['category' => $list['Contents']['category_alias']]
            ); ?>
        </div>
    </div>

    <div class="article-more article-bg-black mg-top-30 mg-bottom-40">
        <a href="/contents/<?php echo $list['category']['category']; ?>">
            <img src="/img/arrow-down-white.png" >
        </a>

    </div>
</div>

<?php
echo $this->element('Item/bottom_banner');
$this->end('sub_footer');
?>

<?php if (!empty($isPreview)) { ?>
    <?php echo $this->element('Item/btn_publish', array('status' => $list['Contents']['status'])); ?>
<?php } ?>


<div class="no-padding container ipad-padding padding-t-170">
    <div class="article-title">
        <h1><?php echo $title; ?></h1>
    </div>

    <div class="article-accessory">
        <div class="clearfix">
            <div class="pull-left">
                <div class="social-buttons">


                    <a class="facebook-button-article"
                       onClick="window.open('http://www.facebook.com/sharer.php?s=100&amp;p[title]=<?php echo $title; ?>&amp;p[summary]=&amp;p[url]=<?php echo $contentsDetailHref; ?>&amp;p[images][0]=<?php echo $courseimage_fb; ?>','sharer','toolbar=0,status=0,width=548,height=325');"
                       href="javascript: void(0)">
                        <i class="fa fa-facebook" aria-hidden="true"></i>
                    </a>


                    <a target="_blank"
                       href="https://twitter.com/intent/tweet?url=<?php echo $contentsDetailHref; ?>&via=コールナビ&text=<?php echo $title; ?>&related=コールナビ"
                       class="twitter-button-article">
                        <i class="fa fa-twitter" aria-hidden="true"></i>
                    </a>


                </div>
            </div>
            <div class="pull-right mg-top-10">
                <time class="datetime"><?php echo $CreateDate->format('Y.m.d'); ?></time>
                <span class="view"><big><?php echo $list['Contents']['view']; ?></big>view</span>
            </div>
        </div>
    </div>
    <div class="article-main">

        <div class="article-excerpt">
            <p><?php echo $list['Contents']['description']; ?></p>
        </div>

        <div class="entryA01 ccwork_entry article-content">
            <?php echo $list['Contents']['content']; ?>
        </div>
    </div>
</div>

<!--<div class="article-relation content-center-column">-->
    <!--	<div class="relation-header">関連する記事</div>-->
<!--    <div class="relation-header">関連するブログ＆コラム</div>-->
<!--    <div class="relation-list">-->
<!--        --><?php //echo $this->element('Item/content_similar_aticles',
//            ['category' => $list['Contents']['category_alias']]
//        ); ?>
<!--    </div>-->
<!--</div>-->
<!---->
<!--<div class="article-more mg-top-40 mg-bottom-60">-->
<!--    <a href="/contents/--><?php //echo $list['category']['category']; ?><!--">-->
<!--        一覧に戻る-->
<!--        <br>-->
<!--        <span class="icon-arrow-down"></span>-->
<!--    </a>-->
<!---->
<!--</div>-->

