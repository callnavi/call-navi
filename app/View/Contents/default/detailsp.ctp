<?php
	$title 		= $list['Contents']['title'];
    $this->set('title', $this->App->getContentsDetailTitle($list));
    $this->start('css');
		echo $this->Html->css('ccwork-sp');
		echo $this->Html->css('contents-sp');
    $this->end('css');



	$courseimage_fb ='';
	$article_image_base_url = '/upload/contents/';
	$article_image_pos_compare = "ccwork";
	$image = $list['Contents']['image'];
	$pos = strpos($image, $article_image_pos_compare);
	if($pos === false)
	{
		$courseimage_fb = FULL_BASE_URL.$article_image_base_url.$image;
	}
	else
	{
		$courseimage_fb = FULL_BASE_URL.$image;
	}
	//ogp meta
	$this->app->meta_ogp(
		$this,
		//title
		$title,
		//decription
		$list['Contents']['meta_description'],
		//keywords
		$list['Contents']['meta_keyword'],
		//url
		$this->Html->url(null, true),
		//image
		$courseimage_fb,
		true
	); 
?>

<?php $this->start('script'); ?>
 	<script type="text/javascript">
	 	$(document).ready(function() {
	 		<?php if(!empty($zoom) && $zoom): ?>
				$("html").css("zoom", "0.5");
			<?php endif; ?>
		});
	</script>
<?php $this->end('script'); ?>

<div class="banner">
	<div class="background clip-bottom-arrow">
		<img class="darker-75" src="<?php echo $this->app->getContentImageSrc($list['Contents']['image']); ?>">
	</div>
	<div class="bottom-u-content">
		<div class="bottom-u-content-wp df-padding">
			<span class="contents-detail-category"><?php echo $list['category']['category']; ?></span>
			<div class="contents-detail-view">
				<span class="clip-of-logo"></span> <big><?php echo $list['Contents']['view']; ?></big> view
			</div>
		</div>
	</div>
</div>

<div class="contents-detail-info df-padding blog-title">
	<h1 class="mg-top-30"><?php echo $list['Contents']['title'];?></h1>
</div>

<div class="detail-datetime df-padding mg-top-20">
	<?php $CreateDate = new DateTime($list['Contents']['created']);?>
	<datetime><?php echo $CreateDate->format("Y年m月d日"); ?></datetime>
	<?php 
	if($this->App->isNew($list['Contents']['created']))
	{
		echo '<span class="new-label">NEW</span>';
	}
	?>
</div>

<div class="detail-social df-padding mg-top-20">
	<div class="link">
		<?php $contentsDetailHref = $this->Html->url(array('controller' => 'contents', 'action' => 'detail','category_alias'=>$list['Contents']['category_alias'],'title_alias'=> $list['Contents']['title_alias']), true); ?>

		<a class="icon-facebook" href="http://www.facebook.com/sharer.php?u=<?php echo $contentsDetailHref; ?>" target="_blank">
			 <img src="/img/icons/icon-facebook.png" alt="Facebookでシェアする">
		</a>

		<a class="icon-twitter" href="https://twitter.com/share?url=<?php echo $contentsDetailHref; ?>" target="_blank">
			 <img src="/img/icons/icon-twitter.png" alt="Tweetする">
		</a>

	</div>
</div>


<div class="ccwork_entry df-padding mg-top-50 mg-bottom-60 image-rmp-content">
	<?php echo $list['Contents']['description'];?>
	<?php echo $list['Contents']['content'];?>
</div>

<?php echo $this->element('../Contents/_related-list-sp'); ?>

<div class="show-more df-padding mg-top-40 mg-bottom-50">
	<a href="/contents/<?php echo $list['category']['category']; ?>" class="btn-blue-3d">もっと見る</a>
</div>


