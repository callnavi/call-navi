<nav class="nav-horizontal <?php echo $curCat; ?>">
	<div class="nav-horizontal__mask">
		<ul class="nav-horizontal__list u-fs--s">
			<?php
				$selected = '';
				if(empty($curCat))
				{
					$selected = 'active';
				}
			?>
			<li class="nav-horizontal__lists <?php echo $selected; ?>"><a href="/contents/">一覧トップ</a></li>
			<?php foreach($catList as $cat){ 
				$param = $cat['Categories'];
				$href = $this->app->createContentCategoryHref($param);
				$selected = !empty($param['category_alias']) && $curCat == $param['category_alias'] ? ' active' : '';
				echo '<li class="nav-horizontal__lists'.$selected.'">
						<a href="'.$href.'">'.$param['category'].'</a>
					 </li>';
			}?>
		</ul>
	</div>
</nav>
<?php $this->append('script'); ?>
<script type="text/javascript">
$(function() {
	var getCenterOffset = function(windowWidth, elWidth, elOffset)
	{
		var offset;
		if (elWidth < windowWidth) {
			offset = elOffset - ((windowWidth / 2) - (elWidth / 2));
		}
		else {
			offset = elOffset;
		}
		return offset;
	}
	
	$('.nav-horizontal__mask').click(function () {
		localStorage.last_left_nav = $('.nav-horizontal__mask').scrollLeft();
	});
	
	var activeObj = $('.nav-horizontal__lists.active:eq(0)');
	var elOffset = activeObj.offset().left;
  	var elWidth = activeObj.outerWidth();
	var windowWidth = $(window).width();
	var offset;
	
	offset = getCenterOffset(windowWidth, elWidth, elOffset);
	if(localStorage.last_left_nav !== undefined)
	{
		$('.nav-horizontal__mask').scrollLeft(localStorage.last_left_nav);
	}
	setTimeout(function (arguments) {
		$('.nav-horizontal__mask').animate({ scrollLeft: offset }, 300);
	}, 1000);
	
});
</script>
<?php $this->end(); ?>