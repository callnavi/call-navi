<div class="search-box secret-search-box secret-search-box__v3">
    <div class="display-table">
        <div class="search-image">
            <img
                src="<?php echo $this->webroot . "upload/secret/jobs/" . $list['CareerOpportunity']['corporate_logo']; ?>"
                alt="">
        </div>

        <div class="search-content secret-content">
            <p class="_h3"><strong><?php echo $list['CareerOpportunity']['job']; ?></strong></p>
            <?php if (!empty($list['CareerOpportunity']['benefit'])) { ?>
            <div class="secret--celebration content--benefit _p">
                特典：<?php echo strip_tags($list['CareerOpportunity']['benefit']); ?></div>
            <?php } ?>
            <?php if (!empty($list['CareerOpportunity']['hourly_wage'])) { ?>
                <div class="search-salary gray-bg content--salary">
                    <span>給与</span>
                    <p class="_p"><?php echo $list['CareerOpportunity']['hourly_wage']; ?></p>
                </div>
            <?php } ?>

            <?php if (!empty($list['CareerOpportunity']['business'])) { ?>
                <div class="search-salary gray-bg content--salary">
                    <span>業種</span>
                    <p class="_p"><?php echo $list['CareerOpportunity']['business']; ?></p>
                </div>
            <?php } ?>
            <?php if (!empty($list['CareerOpportunity']['employment'])) { ?>
            <div class="search-salary gray-bg content--salary">
                <span>雇用形態</span>
                <p class="_p"><?php echo $list['CareerOpportunity']['employment']; ?></p>
            </div>
            <?php } ?>

        </div>
    </div>
</div>