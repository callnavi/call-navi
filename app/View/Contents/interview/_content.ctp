<?php 
	echo $this->element('GoogleMap/_execute_map_pc', 
						  array(
							'CareerOpportunity' => $list['CareerOpportunity'],
						)); 
?>

<?php
if (!empty($list['CareerOpportunity']['id'])) {
    $href = $this->Html->url(array('controller' => 'secret', 'action' => 'apply-step1?id=' . $list['CareerOpportunity']['id']), true);
    $mgTop70 = '';
}else{
    $mgTop70 = 'mg-top-70';
}
//pr($list['Contents']) ?>
<div class="secret-v3 interview-v3 padding-t-170" id="secret_content">
    <div class="container">
        <?php if (!empty($list['CareerOpportunity']['id'])) { ?>
            <div class="padding-lr-40 text-left mg-top-30 mg-bottom-5">
                <span class="label-green">インタビュー兼オリジナル求人</span>
                <strong>
                    <time class="_ss" datetime="2016-07-08 00:00:00">
                        &#160;公開日：<?php echo $CreateDate->format('Y年m月d日') ?></time>
                </strong>
            </div>
        <?php } ?>

        <div class="padding-lr-40 <?php echo $mgTop70 ;?>">
            <div class="bg-white panel-v3">

                <div class="clearfix">
                    <div class="pull-left">
                        <h2 class="_h3 mg-0"><strong><?php echo $list['CareerOpportunity']['branch_name'] ?></strong></h2>
                    </div>
                    <?php if (!empty($list['CareerOpportunity']['id'])) { ?>
                        <div class="pull-right group-label">
                            <?php echo $this->app->createLabel($list['CareerOpportunity']['employment'], 'label-green'); ?>
                        </div>
                    <?php } ?>
                </div>


                <hr class="hr--v3">
                <h1 class="_h2 mg-0"><strong><?php echo $title ?></strong></h1>
                <hr class="hr--v3">
                <div class="content--p content--p-18">
                    <?php
                    echo $list['Contents']['content_introduction'];
                    ?>
                </div>
                <?php
                if($list['Contents']['image_profile'] != ""){
                    
                ?>
                <div class="df-padding">
                    <img class="darker-75" src="<?php echo  $this->app->getContentImageSrc($list['Contents']['image_profile']); ?>" width="100%;">
                </div>
                <?php
                }
                ?>
                <div class="content--p content--p-18">
                    <?php
                    echo $list['Contents']['content'];
                    ?>
                </div>
            </div>

            <?php if (!empty($list['CareerOpportunity']['id'])) { ?>
                <div class="topic-v4 text-center mg-top-60"><?php echo $list['CorporatePrs']['company_name'] ?>
                    オリジナル求人詳細
                </div>

                <!--Secret job-->

                <div class="bg-white panel-v3">
                    <?php
                    echo $this->element('../Contents/interview/_job_header', array('list' => $list));
                    echo $this->element('../Secret/detail/detail3/_button_apply', array('href' => $href));
                    ?>

                    <div class="topic-v3 topic-gray  mg-top-30 ">
                        募集情報
                    </div>

                    <div class="secret-table3">
                        <?php if (!empty($list['CareerOpportunity']['specific_job'])) { ?>
                        <div class="display-table">
                            <div>仕事内容</div>
                            <div><?php echo str_replace("\n", "<br>", $list['CareerOpportunity']['specific_job']); ?></div>
                        </div>
                        <?php } ?>
                        <?php if (!empty($list['CareerOpportunity']['access']) || !empty($list['CareerOpportunity']['work_location'])) { ?>
                        <div class="display-table">
                            <div>勤務地 
                                <?php
                                // function checking the database show map true or false
                                  if(isset($list['CareerOpportunity']['map_show'])){
                                      if($list['CareerOpportunity']['map_show'] == "1"){
                                ?>
                                <a target="_blank"
                                        href="/callcenter_matome?keySearch=<?php echo rawurlencode($list['CareerOpportunity']['branch_name']); ?>"><span
                                        class="label-blue">地図を確認する</span>
                                </a>
                                <?php
                                      }
                                  }                               
                                ?>
                                
                                
                            
                            
                            </div>
                            <div>
                                <?php echo str_replace("\n", "<br>", $list['CareerOpportunity']['work_location']); ?>
                                <div class="mg-top-20">
									<div class="google-map" id="map" style="height: 320px;"></div>
								</div>
                                <?php
                                $access = str_replace("◆", '<span class="ul-icon"></span>', $list['CareerOpportunity']['access']);
                                $access = explode("\n", $access);
                                foreach ($access as $val) {
                                    if (!empty($val))
                                        echo '<p>' . $val . '</p>';
                                }
                                ?>
                            </div>
                        </div>
                        <?php } ?>
                        <?php if (!empty($list['CareerOpportunity']['working_hours'])) { ?>
                        <div class="display-table">
                            <div>勤務時間</div>
                            <div><?php echo str_replace("\n", "<br>", $list['CareerOpportunity']['working_hours']); ?></div>
                        </div>
                        <?php } ?>

                        <?php if (!empty($list['CareerOpportunity']['salary'])) { ?>
                        <div class="display-table">
                            <div>給与</div>
                            <div><?php echo str_replace("\n", "<br>", $list['CareerOpportunity']['salary']); ?></div>
                        </div>
                        <?php } ?>

                        <?php if (!empty($list['CareerOpportunity']['person_of_interest'])) { ?>
                        <div class="display-table">
                            <div>対象となる方</div>
                            <div><?php echo $list['CareerOpportunity']['person_of_interest']; ?></div>
                        </div>
                        <?php } ?>

                        <?php if (!empty($list['CareerOpportunity']['skill_experience'])) { ?>
                        <div class="display-table">
                            <div>こんな経験・スキルが活かせます</div>
                            <div>
                                <?php
                                if (!empty($list['CareerOpportunity']['skill_experience'])) {
                                    $skill_experience = str_replace('○', '<span class="circle-icon"></span>', $list['CareerOpportunity']['skill_experience']);
                                    echo str_replace("\n", '<br>', $skill_experience);
                                }
                                ?>
                            </div>
                        </div>
                        <?php } ?>

                        <?php if (!empty($list['CareerOpportunity']['holiday_vacation'])) { ?>
                        <div class="display-table">
                            <div>休日・休暇</div>
                            <div>
                                <?php
                                $holiday_vacation = str_replace('◆', '<span class="ul-icon"></span>', $list['CareerOpportunity']['holiday_vacation']);
                                echo str_replace("\n", '<br>', $holiday_vacation);
                                ?>
                            </div>
                        </div>
                        <?php } ?>
                        <?php if (!empty($list['CareerOpportunity']['health_welfare'])) { ?>
                        <div class="display-table">
                            <div>待遇・福利厚生</div>
                            <div>
                                <?php
                                $string = str_replace('■', '<span class="circle-icon"></span>', $list['CareerOpportunity']['health_welfare']);
                                $string = str_replace('◆', '<span class="circle-icon"></span>', $string);
                                $string = explode("\n", $string);
                                foreach ($string as $val) {
                                    if (!empty($val))
                                        echo '<p>' . $val . '</p>';

                                }
                                ?>
                            </div>
                        </div>
                        <?php } ?>
                        <?php if (!empty($list['CareerOpportunity']['application_method'])) { ?>
                        <div class="display-table">
                            <div>応募方法</div>
                            <div>
                                <?php
                                $string = str_replace('▼', '', $list['CareerOpportunity']['application_method']);
                                $string = str_replace('◆', '', $string);
                                $string = explode("\n", $string);
                                foreach ($string as $val) {
                                    if (!empty($val))
                                        echo '<p>' . $val . '</p>';
                                }
                                ?>
                            </div>
                        </div>
                        <?php } ?>
                        <?php if (!empty($list['CareerOpportunity']['other_remark'])) { ?>
                        <div class="display-table">
                            <div>その他備考</div>
                            <div><?php echo $list['CareerOpportunity']['other_remark']; ?></div>
                        </div>
                        <?php } ?>
                    </div>

                </div>

                <div class="bg-white panel-v3">
                    <div class="topic-v3 topic-gray">社内風景</div>
                    <div>
                        <div class="own-carousel mg-top-20">
                            <?php
                            echo (!empty($list['CareerOpportunity']['thumb_1'])) ? "<div class='item'><img src='" . $this->webroot . "upload/secret/jobs/" . $list['CareerOpportunity']['thumb_1'] . "' /> </div>" : "";
                            echo (!empty($list['CareerOpportunity']['thumb_2'])) ? "<div class='item'><img src='" . $this->webroot . "upload/secret/jobs/" . $list['CareerOpportunity']['thumb_2'] . "' /> </div>" : "";
                            echo (!empty($list['CareerOpportunity']['thumb_3'])) ? "<div class='item'><img src='" . $this->webroot . "upload/secret/jobs/" . $list['CareerOpportunity']['thumb_3'] . "' /> </div>" : "";
                            ?>
                        </div>
                    </div>

                    <?php echo $this->element('../Secret/detail/detail3/_button_apply', array('href' => $href)); ?>
                </div>

                <!--End Secret job-->
            <?php } ?>

            <?php if (!empty($list['Contents']['content4'])) { ?>
                <div class="bg-white panel-v3 mg-top-60">
                    <div class="content--p content--rm">
                        <?php
                        echo $list['Contents']['content4'];
                        ?>
                    </div>
                </div>
            <?php } ?>

            <!--Secret Company-->
            <?php if (!empty($list['CareerOpportunity']['id'])) { ?>
            <div class="topic-v4 text-center mg-top-60"><?php echo $list['CorporatePrs']['company_name'] ?>
                    オリジナル求人詳細
                </div>
                <div class="bg-white panel-v3 ">
                    <?php
                    echo $this->element('../Contents/interview/_job_header', array('list' => $list));
                    ?>

                    <div class="topic-v3 topic-gray mg-top-30">企業情報</div>

                    <div class="secret-table3">
                        <div class="display-table">
                            <div>企業メッセージ</div>
                            <div>
                                <?php echo $list['CorporatePrs']['corporate']; ?>
                            </div>
                        </div>

                        <div class="display-table">
                            <div>会社名</div>
                            <div><?php echo $list['CorporatePrs']['company_name']; ?></div>
                        </div>

                        <div class="display-table">
                            <div>代表者</div>
                            <div><?php echo $list['CorporatePrs']['representative']; ?></div>
                        </div>

                        <div class="display-table">
                            <div>設立日</div>
                            <div><?php echo date('Y年m月d日', strtotime($list['CorporatePrs']['establishment_date'])); ?></div>
                        </div>

                        <div class="display-table">
                            <div>資本金</div>
                            <div><?php echo $list['CorporatePrs']['capital']; ?></div>
                        </div>

                        <?php if (!empty($list['CorporatePrs']['employee_number'])): ?>
                            <div class="display-table">
                                <div>従業員</div>
                                <div><?php echo $list['CorporatePrs']['employee_number']; ?></div>
                            </div>
                        <?php endif; ?>

                        <?php if (!empty($list['CorporatePrs']['website'])): ?>
                            <div class="display-table">
                                <div>Web</div>
                                <div><a class="website-company"
                                        href="<?php echo $list['CorporatePrs']['website']; ?>"><?php echo $list['CorporatePrs']['website']; ?></a>
                                </div>
                            </div>
                        <?php endif; ?>


                        <div class="display-table">
                            <div>本社・支社</div>
                            <div>
                                <?php if (!empty($list['CorporatePrs']['other_location'])): ?>
                                    <?php echo $list['CorporatePrs']['other_location']; ?>
                                <?php endif; ?>
                            </div>
                        </div>


                        <div class="display-table">
                            <div>事業内容</div>
                            <div><?php echo $list['CorporatePrs']['business']; ?></div>
                        </div>
                    </div>

                </div>
            <div class="panel-v3">
            <?php 
                
                    echo $this->element('../Secret/detail/detail3/_button_apply', array('href' => $href));
            ?>
            </div>
            <?php
            } ?>
            <!--End Secret Company-->

        </div>


        <div class="mg-top-60"></div>
    </div>
</div>



