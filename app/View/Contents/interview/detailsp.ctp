<?php
if(isset($list['Contents']['secret_job_id'])){
    if( $list['Contents']['secret_job_id'] == "" || $list['Contents']['secret_job_id'] == "0" ){
        $title_head = $this->App->getContentsDetailTitle($list);
    }else{
        $title_head = $this->App->ContentsDetailTitle($list);
    }
}else{
    $title_head = $this->App->getContentsDetailTitle($list);
}
$title = $list['Contents']['title'];

$this->set('title', $title_head);
$this->start('css');
echo $this->Html->css('ccwork-sp');
echo $this->Html->css('contents-sp');
echo $this->Html->css('secret-sp');
$this->end('css');


$courseimage_fb = '';
$article_image_base_url = '/upload/contents/';
$article_image_pos_compare = "ccwork";
$image = $list['Contents']['image'];
$pos = strpos($image, $article_image_pos_compare);
if ($pos === false) {
    $courseimage_fb = FULL_BASE_URL . $article_image_base_url . $image;
} else {
    $courseimage_fb = FULL_BASE_URL . $image;
}
//ogp meta
$this->app->meta_ogp(
    $this,
    //title
    $title,
    //decription
    $list['Contents']['meta_description'],
    //keywords
    $list['Contents']['meta_keyword'],
    //url
    $this->Html->url(null, true),
    //image
    $courseimage_fb,
    true
);
$CreateDate = new DateTime($list['Contents']['created']);
//pr($list['CorporatePrs']);
?>

<?php $this->start('script'); ?>
<script type="text/javascript">
    $(document).ready(function () {
        <?php if(!empty($zoom) && $zoom): ?>
        $("html").css("zoom", "0.5");
        <?php endif; ?>
    });
</script>
<?php 
	echo $this->element('GoogleMap/_execute_map_sp', array(
							'CareerOpportunity' => $list['CareerOpportunity'],
						)); 
?>
<?php $this->end('script'); ?>

<div class="interview-v3">
    <div class="banner">
        <div class="background clip-bottom-arrow">
            <?php    
             if(isset($list['CareerOpportunity']['corporate_logo'])){
            ?>
                 <img class="darker-75" src="<?php echo $this->webroot . "upload/secret/jobs/" . $list['CareerOpportunity']['corporate_logo']; ?>">
            <?php 
            }else{
            ?>
                <img class="darker-75" src="<?php echo  $this->app->getContentImageSrc($list['Contents']['image_profile_sp']); ?>">
            <?php 
            }
            ?>
        </div>
    </div>

    <div class="df-padding mg-top-30">
        <div class="topic-v4">
            <span class="">インタビュー兼オリジナル求人</span>
            <span class="pull-right">公開日：<?php echo $CreateDate->format("Y年m月d日"); ?></span>
        </div>

        <div class="clearfix bg-white">
            <div class="df-padding mg-top-60 mg-bottom-20 inline-block">
                <div class="pull-left">
                    <h2 class="_h3 mg-0"><strong><?php echo $list['CareerOpportunity']['branch_name'] ?></strong></h2>
                </div>
                <?php if (!empty($list['CareerOpportunity']['id'])) { ?>
                    <div class="pull-right group-label">
                        <?php echo $this->app->createLabel($list['CareerOpportunity']['employment'], 'label-green'); ?>
                    </div>
                <?php } ?>
            </div>

            <div class="df-padding">
                <hr class="hr--v3">
                <h1 class="_h2 mg-0"><strong><?php echo $list['Contents']['title']; ?></strong></h1>
                <hr class="hr--v3">
            </div>
            <div class="df-padding content--p mg-bottom-60">
                <?php echo $list['Contents']['content_introduction']; ?>
            </div>
                <?php
                if($list['Contents']['image_profile'] != ""){
                    
                ?>
                <div class="df-padding">
                    <img class="darker-75" src="<?php echo  $this->app->getContentImageSrc($list['Contents']['image_profile_sp']); ?>" width="100%;">
                </div>
                <?php
                }
                ?>
            <div class="df-padding content--p mg-bottom-60">
                <?php echo $list['Contents']['content']; ?>
            </div>
        </div>


    </div>

    <?php if (!empty($list['CareerOpportunity'])) { ?>
        <div class="topic-v4 text-center mg-top-80">
            <span class="_h2"><?php echo $list['CorporatePrs']['company_name'] ?></span><br>
            <span class="_h4">オリジナル求人詳細</span>
        </div>
        <!--Secret Job-->
        <div class=" search-box bg-white">
            <div class="search-image">
                <img class="darker-75" src="<?php echo $this->webroot . "upload/secret/jobs/" . $list['CareerOpportunity']['corporate_logo']; ?>" style="width:100%;">
            </div>

            <div class="df-padding search-content secret-content secret-search-box secret-search-box__v3 ">
                <p class="_h3"><strong><?php echo $list['CareerOpportunity']['job']; ?></strong></p>
                <?php if(!empty($list['CareerOpportunity']['benefit'])){ ?>
                <div class="secret--celebration content--benefit _p mg-top-30 mg-bottom-30">
                    特典：<?php echo strip_tags($list['CareerOpportunity']['benefit']); ?>
                </div>
                <?php } ?>

                <?php if (!empty($list['CareerOpportunity']['hourly_wage'])) { ?>
                    <div class="display-table">
                        <div class="table-cell">
                            <span class="label-brick">給与</span>
                        </div>
                        <div class="table-cell">
                            <p><?php echo $list['CareerOpportunity']['hourly_wage']; ?></p>
                        </div>
                    </div>
                <?php } ?>

                <?php if (!empty($list['CareerOpportunity']['business'])) { ?>
                    <div class="display-table">
                        <div class="table-cell">
                            <span class="label-brick">業種</span>
                        </div>
                        <div class="table-cell">
                            <p><?php echo $list['CareerOpportunity']['business']; ?></p>
                        </div>
                    </div>
                <?php } ?>

                <?php if (!empty($list['CareerOpportunity']['employment'])) { ?>
                    <div class="display-table">
                        <div class="table-cell">
                            <span class="label-brick">雇用形態</span>
                        </div>
                        <div class="table-cell">
                            <p><?php echo $list['CareerOpportunity']['employment']; ?></p>
                        </div>
                    </div>
                <?php } ?>

            </div>

            <div class="tab__title">募集情報</div>
            <div class="common_secret-box df-padding">

                <?php if (!empty($list['CareerOpportunity']['specific_job'])) { ?>
                    <div class="secret-title mg-top-60">
                        <div>お仕事内容</div>
                    </div>
                    <div><?php echo str_replace("\n", "<br>", $list['CareerOpportunity']['specific_job']); ?></div>
                <?php } ?>

                <?php if (!empty($list['CareerOpportunity']['work_location']) || !empty($list['CareerOpportunity']['access'])) { ?>
                    <div class="secret-title mg-top-60">
                        <div>勤務地</div>
                    </div>
                    <div>
                        <?php echo str_replace("\n", "<br>", $list['CareerOpportunity']['work_location']); ?>
                        <div class="mg-top-20">
							<div class="google-map" id="map" style="height: 270px;"></div>
						</div>
                        <?php
                        $access = str_replace("◆", '<span class="ul-icon"></span>', $list['CareerOpportunity']['access']);
                        $access = explode("\n", $access);
                        foreach ($access as $val) {
                            if (!empty($val))
                                echo '<p>' . $val . '</p>';
                        }
                        ?>
                    </div>
                <?php } ?>

                <?php if (!empty($list['CareerOpportunity']['working_hours'])) { ?>
                    <div class="secret-title mg-top-60">
                        <div>勤務時間</div>
                    </div>
                    <div><?php echo str_replace("\n", "<br>", $list['CareerOpportunity']['working_hours']); ?></div>
                <?php } ?>

                <?php if (!empty($list['CareerOpportunity']['salary'])) { ?>
                    <div class="secret-title mg-top-60">
                        <div>給与</div>
                    </div>
                    <div><?php echo str_replace("\n", "<br>", $list['CareerOpportunity']['salary']); ?></div>
                <?php } ?>

                <?php if (!empty($list['CareerOpportunity']['person_of_interest'])) { ?>
                    <div class="secret-title mg-top-60">
                        <div>対象となる方</div>
                    </div>
                    <div><?php echo $list['CareerOpportunity']['person_of_interest']; ?></div>
                <?php } ?>

                <?php if (!empty($list['CareerOpportunity']['skill_experience'])) { ?>
                    <div class="secret-title mg-top-60">
                        <div>こんな経験・スキルが活かせます</div>
                    </div>
                    <div>
                        <?php
                        $skill_experience = str_replace('○', '<span class="circle-icon"></span>', $list['CareerOpportunity']['skill_experience']);
                        echo str_replace("\n", '<br>', $skill_experience);
                        ?>
                    </div>
                <?php } ?>

                <?php if (!empty($list['CareerOpportunity']['holiday_vacation'])) { ?>
                    <div class="secret-title mg-top-60">
                        <div>休日・休暇</div>
                    </div>
                    <div>
                        <?php
                        $holiday_vacation = str_replace('◆', '<span class="ul-icon"></span>', $list['CareerOpportunity']['holiday_vacation']);
                        echo str_replace("\n", '<br>', $holiday_vacation);
                        ?>
                    </div>
                <?php } ?>

                <?php if (!empty($list['CareerOpportunity']['health_welfare'])) { ?>
                    <div class="secret-title mg-top-60">
                        <div>待遇・福利厚生</div>
                    </div>
                    <div>
                        <?php
                        $string = str_replace('■', '<span class="circle-icon"></span>', $list['CareerOpportunity']['health_welfare']);
                        $string = str_replace('◆', '<span class="circle-icon"></span>', $string);
                        $string = explode("\n", $string);
                        foreach ($string as $val) {
                            if (!empty($val))
                                echo '<p>' . $val . '</p>';

                        }
                        ?>
                    </div>
                <?php } ?>

                <?php if (!empty($list['CareerOpportunity']['application_method'])) { ?>
                    <div class="secret-title mg-top-60">
                        <div>応募方法</div>
                    </div>
                    <div>
                        <?php
                        $string = str_replace('▼', '', $list['CareerOpportunity']['application_method']);
                        $string = str_replace('◆', '', $string);
                        $string = explode("\n", $string);
                        foreach ($string as $val) {
                            if (!empty($val))
                                echo '<p>' . $val . '</p>';
                        }
                        ?>
                    </div>
                <?php } ?>

                <?php if (!empty($list['CareerOpportunity']['other_remark'])) { ?>
                    <div class="secret-title mg-top-60">
                        <div>その他備考</div>
                    </div>
                    <div><?php echo $list['CareerOpportunity']['other_remark']; ?></div>
                <?php } ?>
                <?php if(!empty($CareerOpportunity['thumb_1']) || !empty($CareerOpportunity['thumb_2']) || !empty($CareerOpportunity['thumb_3'])){ ?>

                <div class="secret-title mg-top-60">
                    <div>社内風景</div>
                </div>
                <?php echo $this->element('../Contents/interview/_detail-slider-sp', array('CareerOpportunity' => $list['CareerOpportunity'])); ?>
                <?php } ?>
                
                <div class="mg-top-60 inline-block"></div>
            </div>

        </div>
    <?php } ?>
    <!--/Secret Job-->

    <div class="df-padding mg-top-80 ">
        <?php if (!empty($list['Contents']['content4'])) { ?>
            <div class="df-padding bg-white content--p ">
                <div class="mg-top-40 inline-block"></div>
                <?php
                echo $list['Contents']['content4'];
                ?>
            </div>
        <?php } ?>
    </div>

    <!--Secret Company-->
    <div class="search-box bg-white ">
        <div class="tab__title mg-top-80">企業情報</div>
            <?php    
             if(isset($list['CareerOpportunity']['corporate_logo'])){
            ?>
                 <img class="darker-75" src="<?php echo $this->webroot . "upload/secret/jobs/" . $list['CareerOpportunity']['corporate_logo']; ?>" width="100%">
            <?php 
            }else{
            ?>
                <img class="darker-75" src="<?php echo  $this->app->getContentImageSrc($list['Contents']['image_profile_sp']); ?>"  width="100%">
            <?php 
            }
             ?>
        <div class="df-padding common_secret-box mg-bottom-80">
            <?php if (!empty($list['CorporatePrs']['corporate'])) { ?>
                <div class="secret-title mg-top-60">
                    <div>企業メッセージ</div>
                </div>
                <div><?php echo $list['CorporatePrs']['corporate']; ?></div>
            <?php } ?>

            <?php if (!empty($list['CorporatePrs']['company_name'])) { ?>
                <div class="secret-title mg-top-60">
                    <div>会社名</div>
                </div>
                <div><?php echo $list['CorporatePrs']['company_name']; ?></div>
            <?php } ?>

            <?php if (!empty($list['CorporatePrs']['representative'])) { ?>
                <div class="secret-title mg-top-60">
                    <div>代表者</div>
                </div>
                <div><?php echo $list['CorporatePrs']['representative']; ?></div>
            <?php } ?>

            <?php if (!empty($list['CorporatePrs']['establishment_date'])) { ?>
                <div class="secret-title mg-top-60">
                    <div>設立日</div>
                </div>
                <div><?php echo date('Y年m月d日', strtotime($list['CorporatePrs']['establishment_date'])); ?></div>
            <?php } ?>

            <?php if (!empty($list['CorporatePrs']['capital'])) { ?>
                <div class="secret-title mg-top-60">
                    <div>資本金</div>
                </div>
                <div><?php echo $list['CorporatePrs']['capital']; ?></div>
            <?php } ?>

            <?php if (!empty($list['CorporatePrs']['employee_number'])) { ?>
                <div class="secret-title mg-top-60">
                    <div>従業員</div>
                </div>
                <div><?php echo $list['CorporatePrs']['employee_number']; ?></div>
            <?php } ?>

            <?php if (!empty($list['CorporatePrs']['website'])) { ?>
                <div class="secret-title mg-top-60">
                    <div>Web</div>
                </div>
                <div><a class="website-company" target="_blank"
                        href="<?php echo $list['CorporatePrs']['website']; ?>"><?php echo $list['CorporatePrs']['website']; ?></a>
                </div>
            <?php } ?>

            <?php if (!empty($list['CorporatePrs']['other_location'])) { ?>
                <div class="secret-title mg-top-60">
                    <div>本社・支社</div>
                </div>
                <div><?php echo $list['CorporatePrs']['other_location']; ?></div>
            <?php } ?>

            <?php if (!empty($list['CorporatePrs']['business'])) { ?>
                <div class="secret-title mg-top-60">
                    <div>事業内容</div>
                </div>
                <div><?php echo $list['CorporatePrs']['business']; ?></div>
            <?php } ?>

        </div>

    </div>
    <!--/Secret Company-->

</div>

