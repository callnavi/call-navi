<?php
$classImage = '';
$count_thumb = 0;
if (!empty($CareerOpportunity['thumb_1'])) {
    $count_thumb++;
}
if (!empty($CareerOpportunity['thumb_2'])) {
    $count_thumb++;
}
if (!empty($CareerOpportunity['thumb_3'])) {
    $count_thumb++;
}

if ($count_thumb < 2) {
    $classImage = 'img-child-full';
}

?>
<style>
    .own-carousel {
        overflow-x: scroll;
        overflow-y: hidden;
        white-space: nowrap;
        overflow-scrolling: touch;
        -webkit-overflow-scrolling: touch;
    }

    .own-carousel .item {
        display: inline-block;
        margin-right: 20px;
        vertical-align: top;
    }

    .own-carousel .item img {
        height: 320px;
    }

    .own-carousel .item:last-child {
        margin-right: 0;
    }
</style>

<div id="slide_images" class="<?php echo empty($classImage) ? 'own-carousel' : $classImage; ?>">
    <?php
    echo (!empty($CareerOpportunity['thumb_1'])) ? "<div class='item'><img src='" . $this->webroot . "upload/secret/jobs/" . $CareerOpportunity['thumb_1'] . "' /> </div>" : "";
    echo (!empty($CareerOpportunity['thumb_2'])) ? "<div class='item'><img src='" . $this->webroot . "upload/secret/jobs/" . $CareerOpportunity['thumb_2'] . "' /> </div>" : "";
    echo (!empty($CareerOpportunity['thumb_3'])) ? "<div class='item'><img src='" . $this->webroot . "upload/secret/jobs/" . $CareerOpportunity['thumb_3'] . "' /> </div>" : "";
    ?>
</div>
