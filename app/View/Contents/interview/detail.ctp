<?php
if(isset($list['Contents']['secret_job_id'])){
    if( $list['Contents']['secret_job_id'] == "" || $list['Contents']['secret_job_id'] == "0" ){
        $title_head = $this->App->getContentsDetailTitle($list);
    }else{
        $title_head = $this->App->ContentsDetailTitle($list);
    }
}else{
    $title_head = $this->App->getContentsDetailTitle($list);
}
$title = $list['Contents']['title'];
$breadcrumb = $breadrumLink;
$CreateDate = new DateTime($list['Contents']['created']);
$contentsDetailHref = $this->Html->url(array('controller' => 'contents',
    'action' => 'detail',
    'category_alias' => $list['Contents']['category_alias'],
    'title_alias' => $list['Contents']['title_alias']),
    true);
$this->set('title', $title_head);
$this->start('css');
echo $this->Html->css('base');
echo $this->Html->css('ccwork');
echo $this->Html->css('contents');
echo $this->Html->css('format');
echo $this->Html->css('content-detail');
echo $this->Html->css('secret2');
$this->end('css');


$courseimage_fb = '';
$article_image_base_url = '/upload/contents/';
$article_image_pos_compare = "ccwork";
$image = $list['Contents']['image'];
$pos = strpos($image, $article_image_pos_compare);
if ($pos === false) {
    $courseimage_fb = FULL_BASE_URL . $article_image_base_url . $image;
} else {
    $courseimage_fb = FULL_BASE_URL . $image;
}
//ogp meta
$this->app->meta_ogp(
    $this,
    //title
    $title,
    //decription
    $list['Contents']['meta_description'],
    //keywords
    $list['Contents']['meta_keyword'],
    //url
    $this->Html->url(null, true),
    //image
    $courseimage_fb,
    true
);
?>

<?php
$this->start('header_breadcrumb');
echo $this->element('Item/breadcrumb', $breadcrumb);
$this->end('header_breadcrumb');
?>

<?php if (!empty($isPreview)) { ?>
    <?php echo $this->element('Item/btn_publish', array('status' => $list['Contents']['status'])); ?>
<?php } ?>


<?php $this->start('sub_header'); ?>
<?php //delete in issues#345
//echo $this->element('../Top/2017/_top_banner'); ?>
<?php echo $this->element('../Contents/interview/_content'
    , array(
        'title' => $title,
        'CreateDate' => $CreateDate,
        'list' => $list
    )
); ?>
<?php $this->end('sub_header'); ?>

