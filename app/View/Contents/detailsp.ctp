<?php
$this->layout = 'single_column_v2_sp';
$cat = $list['Contents']['category_alias'];
if ($cat == 'interview' &&  isset($list['CareerOpportunity'])) {
    echo $this->element('../Contents/interview/detailsp');
} else {
    echo $this->element('../Contents/default/detailsp');
}
echo $this->element('../Top/_social-sp');
if ($cat == 'interview' &&  isset($list['CareerOpportunity'])) {
    echo $this->element('../Secret/detail-sp/_detail-fixed-button-sp',['CareerOpportunity'=> $list['CareerOpportunity']]);
}else {
    echo $this->element('Item/concierge_link_banner_sp');
}
?>

