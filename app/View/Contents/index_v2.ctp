<?php
$currentPage = $this->Paginator->params()['page'];
$pageCount = $this->Paginator->params()['pageCount'];

if($list){
    if ($curCat) {
        $categoryName = isset($list[0]['category']['category']) ? $list[0]['category']['category'] : '';
        if ($categoryName) {
            $this->set('title', $this->app->getContentsCategoryTitle($categoryName));
        }
    } else {
        $this->set('title', $this->app->getWebTitle('contents'));
    }
    
}else{
    $this->set('title', $this->app->getContentsCategoryTitle($category));
}

$this->start('css');
echo $this->Html->css('contents');
$this->end('css');

//ogp meta
$this->app->meta_ogp($this);

switch ($curCat) {
    case "work" :
        $title = '働く人の声';
        break;
    case "skill":
        $title = 'スキル・テクニック';
        break;
    case "saiyou":
        $title = '採用・面接';
        break;
    case "business":
        $title = 'ビジネス・キャリア';
        break;
    case "topics":
        $title = '注目トピックス';
        break;
    case "japanesecallcenter":
        $title = 'エリア特集';
        break;
    default:
        $title = '特集記事';
        break;
}
if ($title == '特集記事') {
    $breadcrumb = array('page' => '特集記事');
} else {
    $breadcrumb = array('page' => $title,
        'parent' => array(
            array('link' => $curCat, 'title' => '特集記事')
        )
    );
}

$this->start('sub_footer');
echo $this->element('Item/bottom_banner');
$this->end('sub_footer');
?>

<?php $this->start('sub_header'); ?>
<?php $this->end('sub_header'); ?> 

<?php $this->start('header_breadcrumb'); ?>
<?php echo $this->element('Item/breadcrumb', $breadcrumb); ?>
<?php $this->end('header_breadcrumb'); ?>

<?php $this->append('script'); ?>
<script type="text/javascript">
    var setting = {};
    setting.__page = 1;
    setting.__lastPage = <?php echo $pageCount; ?>;
    setting.__url = "/ajax/contents/<?php echo $curCat; ?>";
</script>
<script type="text/javascript" src="/js/scrolling_page/content__contentTemplate.js"></script>
<script type="text/javascript" src="/js/clicking_page/clicking_page.js"></script>
<?php $this->app->echoScriptScrollSidebarJs(); ?>
<script type="text/javascript">
    $(window).load(function () {
        var stopPoint = $('#stopScrollPoint').offset().top;
        if ($('#right-column').length > 0 && $('#left-column').length > 0) {
            var scrollRight = new scrollColumn($('#right-column'), stopPoint, 0);
            scrollRight.scrollBottomEvent = function (sb) {
                sb.setstopPoint($('#stopScrollPoint').offset().top);
            }
        }
    });
</script>
<?php $this->end('script'); ?>

<div class="no-padding container ipad-padding padding-t-170">
    <div class="clearfix">
        <div class="pull-left content">
            <div class="article" id="left-column">
                <div class="pick-up-row mg-top-30">
                    <?php echo $this->element('../Contents/_pick_up_row', array('catList' => $catList)); ?>
                </div>
                <div class="article-list">
                    <?php echo $this->element('../Contents/_article_list_v2', array('list' => $list)); ?>
                </div>
                <div class="article-more mg-top-20">
                    <label data-action="lazy-click">
                        さらに表示する
                        <br>
                        <span class="icon-arrow-down"></span>
                    </label>
                </div>

                <div style="position: absolute;bottom: 0;" id="stopScrollPoint"></div>
            </div>

        </div>

        <div class="pull-right slider">
            <div id="right-column">
                <?php echo $this->element('../Top/_pickup'); ?>
                <?php echo $this->element('slider_top'); ?>
            </div>
        </div>
    </div>
</div>
<div class="mg-bottom-120"></div>