<?php
$this->start('css');
echo $this->Html->css('secret');
$this->end('css');

$this->start('sub_footer');
echo $this->element('Item/bottom_banner');
$this->end('sub_footer');

/*Setting*/
$_keyResult = '';
$_keyResultBreadCrumb = '';
$_keyCurrent = '';
$_area = '';
$_keyword = '';
$_em = '';
$_selectedArea = '';
$_selectedEm = '';
$_checkedHotkey = [];
$_keywordArr = [];
$_keyResultForHeader = '';

if(!empty($params['area']))
{
	$_area = isset($params['area'])? $params['area']:'';
	$_keyResultBreadCrumb = empty($_area)?'':"「{$_area}」";
	$_keyResult = $_keyResultBreadCrumb;
	$_keywordArr[] = $_area;
		
}

if(!empty($params['keyword']))
{
	$_keyword = $params['keyword'];
	$_keyResultBreadCrumb .= "「{$_keyword}」";
	$_keywordArr[] = $_keyword;
}

if(!empty($params['em']))
{
	$_em = isset($params['em'])? $params['em']:'';
	if(!empty($_em))
	{
		$_keyResultBreadCrumb .= "「{$_em}」";
		$_keyResult = $_keyResultBreadCrumb;
		$_keywordArr[] = $_em;
	}
}

if(!empty($params['area']))
{
	$_selectedArea = $params['area'];
}

if(!empty($params['em']))
{
	$_selectedEm = $params['em'];
}

if(!empty($params['hotkey']))
{
	$_checkedHotkey = $params['hotkey'];
	$hotkeyList = Configure::read('webconfig.hotkey_data');
	foreach($_checkedHotkey as $key=>$val)
	{
		$_keyResultBreadCrumb .= "「{$hotkeyList[$val]}」";
	}
}
$seoKeyword = $_keyResultBreadCrumb;
$breadcrumb = [];
$breadcrumb['parent'] 	= [['title'=>'コールセンター オリジナル求人', 'link'=> '/secret']];
if(!empty($_keyResultBreadCrumb)){
	$breadcrumb['page'] 	= $_keyResultBreadCrumb."のオリジナル求人検索結果";
}else{
	$breadcrumb['page'] 	= "求人情報";
}

if(empty($currentKeyWord))
{
	$currentKeyWord = '検索条件を選択してください';
}


$pars = http_build_query($this->request->query);
$setting = array(
	'__lastPage' => $pageCount,
	'__url'		 => '/secret/ajaxSearch?'.$pars
);

if($_keywordArr)
{
	foreach( $_keywordArr as $val)
	{
		$_keyResultForHeader .= "「{$val}」";
	}
}
else
{
	$_keyResultForHeader = '';
}


$secretJobsHasInterview = $this->app->getListSecretJobsHasInterview();
?>

<?php  $this->start('script');?>
<script type="text/javascript">
	var setting = <?php echo json_encode($setting); ?> ;
</script>
<?php 
echo $this->Html->script('search_popup/search_popup.js');
echo $this->Html->script('search_history/search_history.js');
echo $this->Html->script('search_more/search__contentTemplate.js');
echo $this->Html->script('search_more/search_more.js');
$this->app->echoScriptScrollHistoryBoxJs();
echo $this->Html->script('secret.js');
$this->end('script');
 ?>

<?php $this->start('header_breadcrumb'); ?>
<?php echo $this->element('Item/breadcrumb', $breadcrumb); ?>
<?php $this->end('header_breadcrumb'); ?>

<?php $this->end('sub_header'); ?>

<div class="no-padding container">
	<div class="row">
		<?php echo $this->element('Item/top_banner_search', array(
			'keyword' => $_keyword,
			'area'	  => $_selectedArea,
			'em'	  => $_selectedEm,
			'hotkey'  => $_checkedHotkey,
			'total'	  => $search_total,
	
			'currentKeyWord' => $currentKeyWord,
			'_keyResult' => $_keyResult,
			'countUrl'			=> '/secret/countcondition',

			'params' => $params,
		));?>
	</div>
</div>
<?php $this->end('sub_header'); ?>  	

<?php $this->start('sub_footer'); ?>
<div class="_footer"></div>
<?php $this->end('sub_footer'); ?>

<?php
	
	$this->set('title', $seoKeyword.'コールセンター オリジナル求人情報｜ コールセンター特化型求人サイト「コールナビ」');
	
	$this->start('meta');
		//meta description keywords image
		echo '<meta name="description" content= "'.$seoKeyword.'コールセンター・テレオペのアルバイト・バイト・求人情報を探すならコールナビ。勤務地や雇用形態、フリーワードといった様々な条件からバイト、正社員、派遣の求人情報が検索できます。コールセンター・テレオペのお仕事探しは採用実績が豊富なコールナビにお任せください！" >';
		echo '<meta name="Keywords" content="コールセンター 求人,コールセンター 在宅,コールセンター バイト,テレアポ 募集,アルバイト 高時給,オペレーター,アウトバウンド,'.str_replace('」「','」,「',$seoKeyword).'">';
		echo Configure::read('webconfig.apple_touch_icon_precomposed');
		echo Configure::read('webconfig.ogp');  
	$this->end('meta');
?>

<div class="search-layout mg-top-30">		
	<div class="pull-left content">			
		<div id="search-result" class="search-result">
			<div class="search-header">
				<h2><?php echo $_keyResultForHeader; ?>コールセンターオリジナル求人検索結果</h2>
				<div class="desc mg-top-5 mg-bottom-40">勤務地や雇用形態、フリーワードといった様々な条件からアルバイト、バイト、正社員、派遣の求人情報が検索できます。<br>
				コールセンター・テレオペのお仕事探しは採用実績が豊富なコールナビにお任せください！</div>
				<p class="present"><strong><?php echo $_keyResultForHeader; ?></strong>オリジナル求人が<?php echo $search_total; ?>件見つかりました。</p>
			</div>
		
			<?php if ($data): ?>
			<ul class="clearfix" id="showmore_load">
			<?php foreach($data as $item){ ?>
			<li>
				<?php echo $this->element('Item/secret_big_box', array('item' => $item, 'secretJobsHasInterview' => $secretJobsHasInterview)); ?>
			</li>
			<?php } ?>
			</ul>
			<?php else: ?>
			<span style="font-size:14px"><b>データなし</b></span>
			<div style="margin-top:40px;">
				<p>検索のヒント:</p>
				<div style="padding-left:20px">
					<p>キーワードに誤字・脱字がないか確認。</p>
					<p>別のキーワードに変更。</p>
					<p>もっと一般的なキーワードに変更。</p>
				</div>
			</div>
			<?php endif; ?>
		</div>
		
		<div class="article-more mg-bottom-60">
		<?php if ($pageCount > 1): ?>
			<label  data-showmore="ajax">
				さらに表示する
				<br>
				<span class="icon-arrow-down"></span>
			</label>
		<?php endif; ?>
		</div>
		
	</div>

	<div class="pull-left slider">
		<?php echo $this->element('../Secret/_search-right', array('_keywordArr'=>$_keywordArr)); ?>
	</div>

	<div class="clearfix"></div>
</div>

<?php echo $this->element('Item/search_box_area_popup'); ?>