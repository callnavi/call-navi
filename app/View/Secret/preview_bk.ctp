<?php
$this->set('title', 'コールナビ｜' . $CorporatePrs['company_name']);
$breadcrumb = array(
    'page' => $CorporatePrs['company_name'],
    'parent' => array(array('title' => 'シークレット求人', 'link' => '/secret'))
);

$this->start('css');
echo $this->Html->css('secret');
$this->end('css');

$this->start('script');
echo $this->Html->script('create-company.js');
$this->app->echoScriptScrollColumnJs();
echo $this->Html->script('init-googlemap');
echo '<script src="https://maps.google.com/maps/api/js?libraries=geometry&key=' . Configure::read('webconfig.google_map_api_key') . '"></script>';
?>
<script>
    $(window).load(function () {

        var scrollRight;
        var tabContent = $('#tab-content');
        var stopPoint = tabContent.offset().top + tabContent.outerHeight();

        if ($('#right-menu').length > 0) {
            var keywordHeight = $('#panel-keyword').outerHeight(true) * -1;
            scrollRight = new scrollColumn($('#right-menu'), stopPoint, 'right-menu-fix', keywordHeight);

        }


        /*[TAB]*/
        $('a[data-toggle="tab"]').on('shown.bs.tab',
            function (event) {
                var $relatedTarget, $target;

                /*set stopPoint again when click tab*/
                stopPoint = tabContent.offset().top + tabContent.outerHeight();
                scrollRight.setstopPoint(stopPoint);
            }
        );
        /*[TAB/]*/
    });
    $(function () {
        $('#tab-bottom').click(function () {
            $("html, body").animate({scrollTop: 0}, "slow");
        });


    });

</script>
<?php $this->end('script'); ?>
<?php

$this->start('sub_footer');
echo $this->element('Item/bottom_banner');
$this->end('sub_footer');
?>
<?php $this->start('sub_header'); ?>
<div class="no-padding container">
    <div class="row">
        <?php echo $this->element('Item/top_banner_version2'); ?>
    </div>
    <div class="row">
        <?php echo $this->element('Item/breadcrumb', $breadcrumb); ?>
    </div>
</div>
<?php $this->end('sub_header'); ?>

<div class="secret-layout">
    <div id="main-contain" class="pull-left content">
        <div class="company-header">
            <div class="company-logo">
                <div class="company-logo-wrap">
                    <?php if ($CorporatePrs['corporate_logo']): ?>
                        <img
                            src="<?php echo $this->webroot . "upload/secret/companies/" . $CorporatePrs['corporate_logo']; ?>"
                            alt="">
                    <?php endif; ?>
                </div>
                <?php if (!empty($CareerOpportunity['benefit'])): ?>
                <div class="ads benifit">
                    <div class="ads-wrap">入社特典：<?php echo $CareerOpportunity['benefit']; ?></div>
                </div>
                <?php endif; ?>
            </div>

            <div class="list-header non-type clearfix keyword-result">
                <h1><?php echo $CorporatePrs['company_name']; ?></h1>

                <section class="number-result">
                    公開日： <?php echo date('Y年m月d日', strtotime($CareerOpportunity['release_date'])); ?>
                </section>
            </div>

            <div class="company-info">
                <?php if (!empty($CareerOpportunity['employment'])) { ?>
                    <p>雇用形態： <?php echo $this->app->createLabel($CareerOpportunity['employment'], 'i-cat-label'); ?></p>
                <?php } ?>

                <?php if (!empty($CareerOpportunity['hourly_wage'])) { ?>
                    <p>時給：<?php echo $CareerOpportunity['hourly_wage']; ?></p>
                <?php } ?>
                <?php if (!empty($CareerOpportunity['job'])) { ?>
                    <p>職種：<?php echo $CareerOpportunity['job']; ?></p>
                <?php } ?>
                <?php if (!empty($CareerOpportunity['business'])) { ?>
                    <p>業種：<?php echo $CareerOpportunity['business']; ?></p>
                <?php } ?>
            </div>

            <div class="google-map" id="map"></div>
        </div>

        <ul id="tab-top" class="clearfix custom-nav-tabs">
            <li class="pull-left active"><a class="brick-btn" href="#tab1" data-toggle="tab">▼ 求人情報はこちら</a></li>
            <li class="pull-left"><a class="brick-btn" href="#tab2" title="2" data-toggle="tab">▼ 企業情報はこちら</a></li>

            <li class="pull-right entry-this-job">
                <button class="brick-btn brick-green"
                        onclick="location.href='<?php echo $this->Html->url(array('controller' => 'secret', 'action' => 'apply?id=' . $CareerOpportunity['id']), true) ?>'">
                    いますぐ応募する<span>&#10141;</span></button>
            </li>
        </ul>

        <div id="tab-content" class="tab-content">
            <div class="tab-pane fade active in" id="tab1">
                <?php echo $this->element('../Secret/index-tab1',
                    [
                        'CareerOpportunity' => $CareerOpportunity,
                        'CorporatePrs' => $CorporatePrs,
                    ]); ?>
            </div>
            <div class="tab-pane fade" id="tab2">
                <?php echo $this->element('../Secret/index-tab2',
                    [
                        'CareerOpportunity' => $CareerOpportunity,
                        'CorporatePrs' => $CorporatePrs,
                    ]); ?>
            </div>
        </div>


        <div class="entry-this-job bottom">
            <button class="brick-btn brick-green"
                    onclick="location.href='<?php echo $this->Html->url(array('controller' => 'secret', 'action' => 'apply?id=' . $CareerOpportunity['id']), true) ?>'">
                いますぐ応募する<span>&#10141;</span></button>
        </div>

    </div>

    <div class="pull-right slider">
        <div id="right-menu">
            <?php if (!empty($isPreview)) { ?>
                <?php echo $this->element('Item/btn_publish', array('status' => $CareerOpportunity['status'])); ?>
            <?php } ?>
            <?php echo $this->element('../Secret/index-right-column', ['CorporatePrs' => $CorporatePrs]); ?>
        </div>
    </div>
</div>


<script>
    $(document).ready(function () {
        var direction = '<a style="font-size:12px" href="https://maps.google.com/maps?ll=<?php echo $CorporatePrs['lat'];?>,<?php echo $CorporatePrs['lng'];?>&z=16&t=m&hl=ja-JP&gl=JP&mapclient=embed&daddr=<?php echo $CorporatePrs['map_location']; ?>" target="_blank"> ルート検索 ▶︎</a>';
        // set google map
        // input lat, lng , inforwindow, zoom
        <?php $latLng = !empty($CorporatePrs['lat']) && !empty($CorporatePrs['lng']) ? $CorporatePrs['lat'].' , '.$CorporatePrs['lng'] : '0,0'; ?>
        var contentString = "<h4 style='font-weight: bold;'><?php echo $CorporatePrs['company_name']; ?> " + direction + "</h4>" +
            "<p style='line-height:1;'><a href='<?php echo $CorporatePrs['website']; ?>' ><?php echo $CorporatePrs['website']; ?></a></p>" +
            "<div style='height: 24px;'><?php echo $CorporatePrs['office_location']; ?></div>";
        initMap(<?php echo $latLng;?>, contentString, 13);

    });
</script>