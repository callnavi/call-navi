<div class="banner">
    <div class="top-content banner-title clip-bottom-arrow gray-color">
        <div class="banner-title-wp">
            <?php if ($_keyResult): ?>
				<big><?php echo $_keyResult; ?></big>のオリジナル求人検索結果
			<?php else: ?>
				オリジナル求人検索結果
			<?php endif; ?>
        </div>

        <div class="banner-box-search">
        	<div class="search-part">
        		<div class="metal-item-left">
        			都道府県： <big class="mg-left-30"><?php echo $_area; ?></big>
        		</div>
        		<div class="metal-item-right">
        			<span class="i-change">変更</span>
        		</div>
        	</div>
        	<div class="search-part">
        		<div class="metal-item-left">
        			雇用形態： <big class="mg-left-30"><?php echo $_em; ?></big>
        		</div>
        		<div class="metal-item-right">
        			<span class="i-change">変更</span>
        		</div>
        	</div>
        	<div class="search-part">
        		<div class="metal-item-left">
        			フリーワード： <big><?php echo $_keyword; ?></big>
        		</div>
        		<div class="metal-item-right">
        			<span class="i-change">変更</span>
        		</div>
        	</div>
        	<div class="bottom-part">
        		<div class="metal-item-left bottom">
        			こだわり：<br>
        			<div class="metal-item-hotkey">
        			<?php 
        				foreach($_hotkeyList_Checked as $hotkey){
        					echo '<span class="i-hotkey">' .$hotkey. '</span>';
        				} 
        			?>
        			</div>
        		</div>
        		<div class="metal-item-right bottom">
        			<span class="i-change">変更</span>
        		</div>
        	</div>
        </div>

        <div class="count-job"><big><?php echo $search_total; ?></big>件のオリジナル求人が見つかりました</div>
    </div>
</div>