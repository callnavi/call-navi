<div class="common_secret-box df-padding">
	<div class="mg-top-60">
		<div class="label-brick">勤務地</div>
	</div>
	<div>
			<p><?php
				/*勤務地*/
				echo str_replace("\n", "<br>",$CareerOpportunity['work_location']);?>
			</p>
	</div>
	<div class="mg-top-20">
		<div class="google-map" id="map" style="height: 270px;"></div>
	</div>
	
	<div class="mg-top-30">
		<?php echo str_replace("\n", "<br>", $CareerOpportunity['work_location']); ?>
	</div>
	<div class="mg-top-20">
		<?php
			$access = str_replace("◆", '<span class="ul-icon"></span>', $CareerOpportunity['access']);
			$access = explode("\n", $access);
			foreach ($access as $val) {
				if (!empty($val))
					echo '<p>' . $val . '</p>';
			}
		 ?>
	</div>
	
	
	<div class="secret-title mg-top-50">
		<div>お仕事内容</div>

	</div>
	<div>
		<?php echo str_replace("\n", "<br>", $CareerOpportunity['what_job']); ?>
	</div>
	
	<div class="secret-title mg-top-50">
		<div>勤務時間</div>

	</div>
	<div>
		<?php echo str_replace("\n", "<br>", $CareerOpportunity['working_hours']); ?>
	</div>

	<div class="secret-title mg-top-50">
		<div>給与</div>
		
	</div>
	<div>
		<?php echo str_replace("\n", "<br>", $CareerOpportunity['salary']); ?>
	</div>
	
	<div class="secret-title mg-top-50">
		<div>対象となる方</div>
		
	</div>
	<div>
		<?php echo $CareerOpportunity['person_of_interest']; ?>
	</div>

	<div class="secret-title mg-top-50">
		<div>こんな経験・スキルが活かせます</div>

	</div>
	<div>
		<?php
                if (!empty($CareerOpportunity['skill_experience'])) {
                    $skill_experience = str_replace('○', '<span class="circle-icon"></span>', $CareerOpportunity['skill_experience']);
                    echo str_replace("\n", '<br>', $skill_experience);
                }
                ?>
	</div>


	<div class="secret-title mg-top-50">
		<div>休日・休暇</div>

	</div>
	<div>
		<?php
                $holiday_vacation = str_replace('◆', '<span class="ul-icon"></span>', $CareerOpportunity['holiday_vacation']);
                echo str_replace("\n", '<br>', $holiday_vacation);
                ?>
	</div>

	<div class="secret-title mg-top-50">
		<div>待遇・福利厚生</div>

	</div>
	<div>
		<?php
			$string = str_replace('■', '<br><span class="circle-icon"></span>', $CareerOpportunity['health_welfare']);
			$string = str_replace('◆', '<br><span class="circle-icon"></span>', $string);
			$string = explode("\r\n", $string);
			foreach ($string as $val) {
				if (!empty($val))
					echo '<p>' . $val . '</p>';
			}
		?>
	</div>

	<div class="secret-title mg-top-50">
		<div>選考プロセス</div>
	</div>
	<div>
		<?php
                $string = str_replace('▼', '', $CareerOpportunity['application_method']);
                $string = str_replace('◆', '', $string);
                $string = explode("\n", $string);
                foreach ($string as $val) {
                    if (!empty($val))
                        echo '<p>' . $val . '</p>';
                }
                ?>
	</div>

	<!-- <div class="secret-title mg-top-50">
		<div>お問い合わせ</div>
	</div>
	<div>
		<?php /*echo str_replace("\n", "<br>", $CareerOpportunity['contact']);*/ ?>
	</div> -->

	<div class="secret-title mg-top-50">
		<div>その他備考</div>
	</div>
	<div>
		<?php echo $CareerOpportunity['other_remark']; ?>
	</div>
</div>

