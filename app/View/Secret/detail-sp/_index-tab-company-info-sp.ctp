<div class="secret-title mg-top-50">
	企業情報
</div>


<div class="tab-company-info">
	<div class="secret-company-table">
		<div class="display-table">
			<div>会社名</div>
			<div><?php echo $CorporatePrs['company_name']; ?></div>
		</div>
		
		<?php if (!empty($CorporatePrs['website'])): ?>
		<div class="display-table">
			<div>Web</div>
			<div><a class="website-company" href="<?php echo $CorporatePrs['website']; ?>"><?php echo $CorporatePrs['website']; ?></a></div>
		</div>
		<?php endif; ?>
		
		<div class="display-table">
			<div>代表者</div>
			<div><?php echo $CorporatePrs['representative']; ?></div>
		</div>
		
		<div class="display-table">
			<div>設立日</div>
			<div><?php echo date('Y年m月d日', strtotime($CorporatePrs['establishment_date'])); ?></div>
		</div>
		
		<div class="display-table">
			<div>資本金</div>
			<div><?php echo $CorporatePrs['capital']; ?></div>
		</div>
			
		<?php if (!empty($CorporatePrs['employee_number'])): ?>
		<div class="display-table">
			<div>従業員</div>
			<div><?php echo $CorporatePrs['employee_number']; ?></div>
		</div>
		<?php endif; ?>
		
		
	</div>
</div>


<div class="secret-title mg-top-50">
	本社・支社
</div>

<div>
	<?php if(!empty($CorporatePrs['other_location'])): ?>
	<div class="branch-info">
	<?php echo $CorporatePrs['other_location']; ?>
	</div>
	<?php endif; ?>
</div>


<div class="secret-title mg-top-50">事業内容</div>	
<div><?php echo $CorporatePrs['business']; ?></div>	