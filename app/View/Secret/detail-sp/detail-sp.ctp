<?php $this->layout = 'single_column_v2_sp'; ?>
<?php
$this->set('title', $this->App->getSecretDetailTitle($CareerOpportunity));

$this->start('css');
echo $this->Html->css('secret-sp');
$this->end('css');

$this->start('script');
echo $this->Html->script('secret-sp.js');
echo $this->Html->script('init-googlemap');
echo '<script src="https://maps.google.com/maps/api/js?libraries=geometry&key='.Configure::read('webconfig.google_map_api_key').'"></script>';
$this->end('script');

?>

<?php echo $this->element('../Secret/detail-sp/_detail-banner-sp'); ?>
<?php echo $this->element('../Secret/detail-sp/_detail-head-info-sp'); ?>
<?php echo $this->element('../Secret/detail-sp/_detail-button-sp'); ?>
<?php echo $this->element('../Secret/detail-sp/_detail-tab-sp'); ?>
<?php echo $this->element('../Secret/detail-sp/_detail-button-sp'); ?>
<?php echo $this->element('../Top/_social-sp'); ?>
<?php echo $this->element('../Secret/detail-sp/_detail-fixed-button-sp'); ?>

<?php $this->start('script'); ?>
<script type="text/javascript">
	$(document).ready(function() {
		var direction = '<a style="font-size:12px" href="https://maps.google.com/maps?ll=<?php echo $CareerOpportunity['lat'];?>,<?php echo $CareerOpportunity['lng'];?>&z=16&t=m&hl=ja-JP&gl=JP&mapclient=embed&daddr=<?php echo $CareerOpportunity['map_location']; ?>" target="_blank"> ルート検索 ▶︎</a>';
		// set google map
		// input lat, lng , inforwindow, zoom
		<?php $latLng = !empty($CareerOpportunity['lat']) && !empty($CareerOpportunity['lng']) ? $CareerOpportunity['lat'].' , '.$CareerOpportunity['lng'] : '0,0'; ?>
		var contentString = "<h4 style='font-weight: bold;'><?php echo $CareerOpportunity['branch_name']; ?></h4>";
		var flagLoadedMap = false; 

		$('#btn-tab-2').on('click touch', function() {
			if(flagLoadedMap == false)
			{
				initMap(<?php echo $latLng;?>, contentString, 13);
				flagLoadedMap = true;
			}
			
		})
	});
</script>
<?php $this->end('script'); ?>