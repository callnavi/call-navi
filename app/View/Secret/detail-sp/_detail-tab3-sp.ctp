<div class="common_secret-box df-padding">
	<div class="secret-title mg-top-60">企業メッセージ</div>
	<p>
		<?php echo str_replace("\n", "<br>",$CorporatePrs['corporate']); ?>
	</p>


	<?php if (
				!empty($CorporatePrs['president_avata'])||
				!empty($CorporatePrs['president_name'])||
				!empty($CorporatePrs['president_description'])
			 ): ?>
		<div class="secret-title mg-top-50">
			社長はこんな人
		</div>
	<?php endif; ?>

	<div class="person-present clearfix president-present">
		<?php if (!empty($CorporatePrs['president_avata'])): ?>
			<div class="cell-img">
				<img src="<?php echo $this->webroot."upload/secret/companies/".$CorporatePrs['president_avata']; ?>" alt="">
			</div>
		<?php endif; ?>

		<?php if (!empty($CorporatePrs['president_name'])): ?>
			<div class="cell-content">
				代表取締役社長：<?php echo str_replace("\n", "<br>",$CorporatePrs['president_name']); ?>
			</div>
		<?php endif; ?>
	</div>
	<?php if (!empty($CorporatePrs['president_description'])): ?>
		<p class="mg-top-30">
			<?php echo str_replace("\n", "<br>",$CorporatePrs['president_description']); ?>
		</p>
	<?php endif; ?>
	
	
	<div class="secret-title mg-top-50">
		コールナビ編集部コメント
	</div>
	<p><?php echo str_replace("\n", "<br>",$CorporatePrs['comment']); ?></p>
	
	<hr class="secret-hr mg-top-60">
	
	<?php echo $this->element('../Secret/detail-sp/_index-tab-company-info-sp'); ?>
</div>