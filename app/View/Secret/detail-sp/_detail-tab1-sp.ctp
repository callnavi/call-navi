<div class="common_secret-box df-padding">
	
	<div class="secret-title mg-top-60">
		仕事内容
	</div>
	<p>
		<?php echo str_replace("\n", "<br>",$CareerOpportunity['specific_job']); ?>
	</p>	
				
	<?php if (!empty($CareerOpportunity['what_kind_person'])): ?>
	<div class="secret-title mg-top-50">
		どんな人が働いているか
	</div>
	<p>
		<?php echo str_replace("\n", "<br>",$CareerOpportunity['what_kind_person']); ?>
	</p>
	<?php endif; ?>
	
	<?php if (
				!empty($CareerOpportunity['senior_avata'])||
				!empty($CareerOpportunity['senior_name_of_staff'])||
				!empty($CareerOpportunity['senior_employee_interview'])
			 ): ?>
	<div class="secret-title mg-top-50">
		スタッフからのメッセージ
	</div>
	<?php endif; ?>
	
	
	
	<div class="person-present">
		<?php if (!empty($CareerOpportunity['senior_avata'])): ?>
		<div class="cell-img">
			<img src="<?php echo $this->webroot."upload/secret/jobs/".$CareerOpportunity['senior_avata']; ?>" alt="">
		</div>
		<?php endif; ?>
		<div class="cell-content">
			<p><strong><?php echo strip_tags($CareerOpportunity['senior_joining_history']); ?></strong></p>
			<p><?php echo strip_tags($CareerOpportunity['senior_name_of_staff']); ?></p>
		</div>
	</div>
	<?php if (!empty($CareerOpportunity['senior_employee_interview'])): ?>
		<p class="mg-top-30"><?php echo str_replace("\n", "<br>",$CareerOpportunity['senior_employee_interview']); ?></p>
	<?php endif; ?>
</div>

<?php 
	if(!empty($CareerOpportunity['thumb_1']) || !empty($CareerOpportunity['thumb_2']) || !empty($CareerOpportunity['thumb_3'])){
		echo $this->element('../Secret/detail-sp/_detail-slider-sp');
	}
 ?>
