<div class="secret-title mg-top-60">会社名</div>
<p><?php echo $CorporatePrs['company_name']; ?></p>


<div class="secret-title mg-top-50">代表者</div>
<p><?php echo $CorporatePrs['representative']; ?></p>



<div class="secret-title mg-top-50">設立日</div>
<p><?php echo date('Y年m月d日', strtotime($CorporatePrs['establishment_date'])); ?></p>



<div class="secret-title mg-top-50">資本金</div>
<p><?php echo $CorporatePrs['capital']; ?></p>


<?php if (!empty($CorporatePrs['employee_number'])): ?>
<div class="secret-title mg-top-50">従業員</div>
<p><?php echo $CorporatePrs['employee_number']; ?></p>
<?php endif; ?>


<?php if (!empty($CorporatePrs['website'])): ?>
<div class="secret-title mg-top-50">Web</div>
<p><a target="_blank" class="website-company" href="<?php echo $CorporatePrs['website']; ?>"><?php echo $CorporatePrs['website']; ?></a></p>
<?php endif; ?>


<div class="secret-title mg-top-50">
	本社・支社
</div>

<div>
	<?php if(!empty($CorporatePrs['other_location'])): ?>
	<div class="branch-info">
	<?php echo $CorporatePrs['other_location']; ?>
	</div>
	<?php endif; ?>
</div>


<div class="secret-title mg-top-50">事業内容</div>	
<div><?php echo $CorporatePrs['business']; ?></div>	
<div class="mg-top-50"></div>