<!-- issues #337 design new sp layout for
  routing domain/secret/title ot id -->
  <?php
  //explode word work_features
  $work_features = explode(",",$CareerOpportunity['work_features']);
  ?>
      <div class="company-info">
        <ul class="search-tag mg-top-30 df-padding">
          <li class="search-tag__item"><span class="interview-tag">企業インタビューあり</span></li>
        </ul>
        <h1 class="company-title"><?php echo $CareerOpportunity['job']; ?></h1>
        <p class="df-padding mg-top-15"><?php echo $CareerOpportunity['branch_name']; ?></p>
        <div class="company-sum df-padding">
          <?php if(!empty($CareerOpportunity['employment'])){ ?>
          <div class="display-table">
            <div class="table-cell"><span class="label-brick">雇用形態</span></div>
            <div class="table-cell">
              <p><?php echo str_replace(","," ",$CareerOpportunity['employment']); ?></p>
            </div>
          </div>
          <?php } ?>
          <?php if(!empty($CareerOpportunity['hourly_wage'])){ ?>
          <div class="display-table">
            <div class="table-cell"><span class="label-brick">給与</span></div>
            <div class="table-cell">
              <p><?php echo $CareerOpportunity['hourly_wage']; ?></p>
            </div>
          </div>
          <?php } ?>
          <?php if(!empty($CareerOpportunity['access'])){ ?>
          <div class="display-table">
            <div class="table-cell"><span class="label-brick">アクセス</span></div>
            <div class="table-cell">
              <p><?php echo strip_tags($CareerOpportunity['access']); ?></p>
            </div>
          </div>
          <?php } ?>
          <?php if(!empty($CareerOpportunity['celebration'])){ ?> 
          <div class="display-table">
            <div class="table-cell"><span class="label-brick--secondary">お祝い金</span></div>
            <div class="table-cell">
              <p class="u-fc--secondary"><?php echo $CareerOpportunity['celebration']; ?></p>
            </div>
          </div>
          <?php } ?>
          <?php if(!empty($CareerOpportunity['work_features'])){ ?> 
          <div class="display-table condition-box">
            <div class="table-cell condition-box__title">条件：</div>
            <div class="table-cell">
            <?php
             foreach ($work_features as $key => $value) {
              echo('<span class="i-condi">'.$value.'</span>');
            }
           ?>
          </div>
          <?php } ?>
        </div>
      </div>