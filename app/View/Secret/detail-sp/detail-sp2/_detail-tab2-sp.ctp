<div class="common_secret-box df-padding">
		<!-- update sp mode in issues#337 in september 2017 -->
        <?php if(!empty($CareerOpportunity['business'])){ ?>
        <div class="secret-title mg-top-30">
          <div>職種</div>
        </div>
        <div>
          <p><?php echo $CareerOpportunity['business']; ?></p>
        </div>
        <?php } ?>        
        <?php if(!empty($CareerOpportunity['work_location'])){ ?>
        <div class="secret-title mg-top-50">
          <div>勤務地</div>
        </div>
        <div class="w6">
           <p><?php
				/*勤務地*/
				echo str_replace("\n", "<br>",$CareerOpportunity['work_location']);?>
			</p>
		</div>
        <?php } ?>        
<?php
// function checking the database show map true or false
  if(isset($CareerOpportunity['map_show'])){
      if($CareerOpportunity['map_show'] == "1"){
?>
    <div class="mg-top-20">
		<div class="google-map" id="map" style="height: 270px;"></div>
	</div>
<?php
      }
  }                               
?>    
    <?php if(!empty($CareerOpportunity['access'])){ ?>
    <div class="mg-top-20">
		<?php
			$access = str_replace("◆", '<span class="ul-icon"></span>', $CareerOpportunity['access']);
			$access = explode("\n", $access);
			foreach ($access as $val) {
				if (!empty($val))
					echo '<p>' . $val . '</p>';
			}
		 ?>
	</div>
	<?php } ?>
    <?php if(!empty($CareerOpportunity['what_job'])){ ?>
	<div class="secret-title mg-top-50">
		<div>お仕事内容</div>

	</div>
	<div>
		<?php echo str_replace("\n", "<br>", $CareerOpportunity['what_job']); ?>
	</div>
	<?php } ?>
	
    <?php if(!empty($CareerOpportunity['working_hours'])){ ?>
	<div class="secret-title mg-top-50">
		<div>勤務時間</div>

	</div>
	<div>
		<?php echo str_replace("\n", "<br>", $CareerOpportunity['working_hours']); ?>
	</div>
	<?php } ?>

    <?php if(!empty($CareerOpportunity['salary'])){ ?>
	<div class="secret-title mg-top-50">
		<div>給与</div>
		
	</div>
	<div>
		<?php echo str_replace("\n", "<br>", $CareerOpportunity['salary']); ?>
	</div>
	<?php } ?>
	
    <?php if(!empty($CareerOpportunity['person_of_interest'])){ ?>
	<div class="secret-title mg-top-50">
		<div>対象となる方</div>
		
	</div>
	<div>
		<?php echo $CareerOpportunity['person_of_interest']; ?>
	</div>
	<?php } ?>

    <?php if(!empty($CareerOpportunity['skill_experience'])){ ?>
	<div class="secret-title mg-top-50">
		<div>こんな経験・スキルが活かせます</div>

	</div>
	<div>
		<?php
                if (!empty($CareerOpportunity['skill_experience'])) {
                    $skill_experience = str_replace('○', '<span class="circle-icon"></span>', $CareerOpportunity['skill_experience']);
                    echo str_replace("\n", '<br>', $skill_experience);
                }
                ?>
	</div>
	<?php } ?>


    <?php if(!empty($CareerOpportunity['holiday_vacation'])){ ?>
	<div class="secret-title mg-top-50">
		<div>休日・休暇</div>

	</div>
	<div>
		<?php
                $holiday_vacation = str_replace('◆', '<span class="ul-icon"></span>', $CareerOpportunity['holiday_vacation']);
                echo str_replace("\n", '<br>', $holiday_vacation);
                ?>
	</div>
	<?php } ?>

    <?php if(!empty($CareerOpportunity['health_welfare'])){ ?>
	<div class="secret-title mg-top-50">
		<div>待遇・福利厚生</div>

	</div>
	<div>
		<?php
			$string = str_replace('■', '<br><span class="circle-icon"></span>', $CareerOpportunity['health_welfare']);
			$string = str_replace('◆', '<br><span class="circle-icon"></span>', $string);
			$string = explode("\r\n", $string);
			foreach ($string as $val) {
				if (!empty($val))
					echo '<p>' . $val . '</p>';
			}
		?>
	</div>
	<?php } ?>

    <?php if(!empty($CareerOpportunity['health_welfare'])){ ?>
	<div class="secret-title mg-top-50">
		<div>応募方法</div>
	</div>
	<div>
		<?php
                $string = str_replace('▼', '', $CareerOpportunity['application_method']);
                $string = str_replace('◆', '', $string);
                $string = explode("\n", $string);
                foreach ($string as $val) {
                    if (!empty($val))
                        echo '<p>' . $val . '</p>';
                }
                ?>
	</div>
	<?php } ?>

	<!-- <div class="secret-title mg-top-50">
		<div>お問い合わせ</div>
	</div>
	<div>
		<?php /*echo str_replace("\n", "<br>", $CareerOpportunity['contact']);*/ ?>
	</div> -->

    <?php if(!empty($CareerOpportunity['other_remark'])){ ?>
	<div class="secret-title mg-top-50">
		<div>その他備考</div>
	</div>
	<div>
		<?php echo $CareerOpportunity['other_remark']; ?>
	</div>
	<?php } ?>
</div>

