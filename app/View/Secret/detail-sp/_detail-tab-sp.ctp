<div class="gray-background">
</div>
<ul id="tab-top" class="custom-nav-tabs">
	<li class="active" data-select="tab1"><a class="brick-btn" href="javascript:void(0)" data-toggle="tab">メッセージ</a></li>
	<li data-select="tab2"><a class="brick-btn" href="javascript:void(0)" title="2" data-toggle="tab" id="btn-tab-2">募集情報</a></li>
	<li data-select="tab3"><a class="brick-btn" href="javascript:void(0)" title="3" data-toggle="tab">企業情報</a></li>
</ul>


<div id="tab-content" class="tab-content">
  <div class="tab-pane fade active in" id="tab1">
	<?php echo $this->element('../Secret/detail-sp/_detail-tab1-sp',[
	  'CareerOpportunity' => $CareerOpportunity,
	  'CorporatePrs' => $CorporatePrs,
	]); ?>
  </div>
  <div class="tab-pane fade" id="tab2">
	<?php echo $this->element('../Secret/detail-sp/_detail-tab2-sp',[
	  'CareerOpportunity' => $CareerOpportunity,
	  'CorporatePrs' => $CorporatePrs,
	]); ?>
  </div>
  <div class="tab-pane fade" id="tab3">
	<?php echo $this->element('../Secret/detail-sp/_detail-tab3-sp',[
	  'CareerOpportunity' => $CareerOpportunity,
	  'CorporatePrs' => $CorporatePrs,
	]); ?>
  </div>
</div>
<div class="mg-top-40"></div>