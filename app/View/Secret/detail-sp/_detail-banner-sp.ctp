<?php
    if(isset($CareerOpportunity['corporate_logo'])){
        $href_img =  $this->webroot."upload/secret/jobs/". $CareerOpportunity['corporate_logo'];;
    }else{
        $href_img =  $this->webroot."upload/secret/companies/". $CorporatePrs['corporate_logo'];
    }
?>
<div class="banner">
    <div class="background">
          <img src="<?php echo $href_img; ?>" alt="">
    </div>
    
    <?php if (!empty($CareerOpportunity['benefit'])): ?>
	<div class="bottom-content df-padding">
		<div class="benifit">
			<div class="benifit-title">
			  <strong>コールナビ応募特典</strong>
			</div>
			<div class="benifit-content">
			  <?php echo strip_tags($CareerOpportunity['benefit']); ?>
			</div>
		</div>
	</div>
	<?php endif; ?>
</div>
