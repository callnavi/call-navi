<?php 
//2017-05-03: if Admin checked ショー: ON + 必須: ON => don't show remove validation
//if (!($CareerOpportunity['is_upload_cv'] && $CareerOpportunity['required_upload_cv'])): 
?>

<div class="display-table df-padding detail-button fixed-detail-button">
	<a href="<?php echo $this->Html->url(array('controller' => 'secret', 'action' => 'apply-step1?id='.$CareerOpportunity['id']), true)?>">
		<div>
			<img width="100%" src="/img/secret/secret_entry_button_sp.jpg" alt="">
		</div>
	</a>
</div>


<?php $this->append('script'); ?>
<script type="text/javascript">
var _top = $('#content').offset().top;
var _footer = $('[data-togglebnr-trigger=footer]').offset().top;
var triggerToTop = _footer - $(window).outerHeight();
	        
var _obj = $('.fixed-detail-button');
$(window).scroll(function (evt) {
	var y = $(this).scrollTop();
	if (y > _top) {
		if( y > triggerToTop ){
			_obj.fadeOut('slow');
		}else{
			_obj.fadeIn('slow');
		}
	}
	else
	{
		_obj.fadeOut('slow');
	}
});
</script>
<?php $this->end(); ?>


<?php 
//endif; 
?>