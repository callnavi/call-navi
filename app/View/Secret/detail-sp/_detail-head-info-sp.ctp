<div class="company-info">
  <h1 class="company-title df-padding"><?php echo $CareerOpportunity['branch_name']; ?><small>のオリジナル求人</small></h1>
  
  <div class="company-sum df-padding">
	  <?php if(!empty($CareerOpportunity['employment'])){ ?>
	  	<div class="display-table">
	  		<div class="table-cell">
	  			<span class="label-brick">雇用形態</span>
	  		</div>
	  		<div class="table-cell">
	  			<p>
		   			<?php echo $this->app->replaceCharacter2Byte($CareerOpportunity['employment']); ?>
		   		</p>
	  		</div>
	  	</div>
	  <?php } ?>
	  <?php if(!empty($CareerOpportunity['hourly_wage'])){ ?>
	  	<div class="display-table">
	  		<div class="table-cell">
	  			<span class="label-brick">給与</span>
	  		</div>
	  		<div class="table-cell">
					<p>
						<?php echo $this->app->replaceCharacter2Byte($CareerOpportunity['hourly_wage']); ?>
					</p>
	  		</div>
	  	</div>
	  <?php } ?>
	  
	  <?php if(!empty($CareerOpportunity['job'])){ ?>
	  	<div class="display-table">
	  		<div class="table-cell">
	  			<span class="label-brick">職種</span>
	  		</div>
	  		<div class="table-cell">
	  			<p>
		   			<?php echo $this->app->replaceCharacter2Byte($CareerOpportunity['job']); ?>
		   		</p>
	  		</div>
	  	</div>
	  <?php } ?>
	  <?php if(!empty($CareerOpportunity['celebration'])){ ?>	
	  	<div class="display-table">
	  		<div class="table-cell">
	  			<span class="label-brick" style="background: #d1ab4c;">お祝い金</span>
	  		</div>
	  		<div class="table-cell">
	  			<p  style="color: #d1ab4c;">
		   			<?php echo ($CareerOpportunity['celebration']); ?>
		   		</p>
	  		</div>
	  	</div>	    
	  <?php } ?>  
  </div>
</div>
