<?php
$breadcrumb = array(
	'parent' => array(
		0 => array(
			'link'=> '/secret',
			'title'=> 'シークレット求人'
		)
	),
	'page' => $CorporatePrs['company_name'],
);
$this->set('title', 'コールナビ｜'.$CorporatePrs['company_name']);
	$this->start('css'); 
		echo $this->Html->css('secret');
	$this->end('css');

	$this->start('script'); 
		echo $this->Html->script('create-company.js');
	$this->app->echoScriptScrollColumnJs();
		
?>
<script>
	$(window).load(function () {

		var scrollRight,scrollLeft;
		var tabContent = $('#tab-content');
		var stopPoint = tabContent.offset().top + tabContent.outerHeight();

		if ($('#right-menu').length > 0) {
			var keywordHeight = $('#panel-keyword').outerHeight(true) * -1;
			scrollRight = new scrollColumn($('#right-menu'), stopPoint, 'right-menu-fix', keywordHeight);

		}

		if ($('#left-menu').length > 0) {
			scrollLeft = new scrollColumn($('#left-menu'), stopPoint, 'menulist-fix', 40);
		}

		/*[TAB]*/
		$('a[data-toggle="tab"]').on('shown.bs.tab',
			function(event) {
				var $relatedTarget, $target;

				/*set stopPoint again when click tab*/
				stopPoint = tabContent.offset().top + tabContent.outerHeight();
				console.log(stopPoint);
				scrollRight.setstopPoint(stopPoint);
				scrollLeft.setstopPoint(stopPoint);
				/*---*/

				if (event.relatedTarget != null) {

					$relatedTarget = $(event.relatedTarget);
					$('a[data-toggle="' +
						$relatedTarget.attr('data-toggle') +
						'"][href="' +
						$relatedTarget.attr('href') +
						'"]').parent('li').removeClass('active');
				}

				if (event.target != null) {

					$target = $(event.target);
					$('a[data-toggle="' +
						$target.attr('data-toggle') +
						'"][href="' +
						$target.attr('href') +
						'"]').parent('li').addClass('active');
				}
			}
		);
		/*[TAB/]*/
	});


	$(function(){
		$('#tab-bottom').click(function(){
			$("html, body").animate({ scrollTop: 0 }, "slow");
		});


	});



</script>

<?php
	$this->end('script');
	$this->start('sub_header'); ?>
	<div class="no-padding container">
		<div class="row">
			<?php echo $this->element('Item/top_banner_version2');?>
		</div>
	</div>
<?php $this->end('sub_header'); ?>


	<div class="no-padding container secret-layout">
		
   		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding secret-header">
			<?php  echo $this->element('Item/breadcrumb', $breadcrumb); ?>
			<?php if(!empty($isPreview)){ ?>
				<?php echo $this->element('Item/btn_publish', array('status' => $CareerOpportunity['status'])); ?>
			<?php } ?>
		</div>
		
		<div id="main-contain" class="col-md-6 col-lg-6 no-padding content">
			<div class="common_secret-box company-header">
				<div class="clearfix">
					<div class="pull-left">公開日： <?php echo date('Y.m.d', strtotime($CareerOpportunity['release_date'])); ?></div>
					<div class="pull-right"><?php echo $CorporatePrs['company_name']; ?>シークレット求人のページ</div>
				</div>
				<div class="company-logo">
					<div class="company-logo-wrap">
						<?php if ($CorporatePrs['corporate_logo']): ?>
						<img src="<?php echo $this->webroot."upload/secret/companies/". $CorporatePrs['corporate_logo']; ?>" alt="">
						<?php endif; ?>
					</div>
				</div>
				<div class="company-info">
					<h1><?php echo $CorporatePrs['company_name']; ?></h1>
					<?php if(!empty($CareerOpportunity['employment'])){ ?>
						<p>雇用形態： <?php echo $CareerOpportunity['employment']; ?></p>
					<?php } ?>

					<?php if(!empty($CareerOpportunity['hourly_wage'])){ ?>
						<p>時給：<?php echo $CareerOpportunity['hourly_wage']; ?></p>
					<?php } ?>
					<?php if(!empty($CareerOpportunity['job'])){ ?>
						<p>職種：<?php echo $CareerOpportunity['job']; ?></p>
					<?php } ?>
					<?php if(!empty($CareerOpportunity['business'])){ ?>
						<p>業種：<?php echo $CareerOpportunity['business']; ?></p>
					<?php } ?>
				</div>
				<div class="ads">
					<div class="ads-wrap">
						<div class="secret-button top-back">
							<div class="benifit-title">
								<p>入社<br>特典</p>
							</div>
							<div class="benifit-content">
								<?php echo $CareerOpportunity['benefit']; ?>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<ul id="tab-top" class="custom-nav-tabs">
				<li class="active"><a class="brick-btn" href="#tab1" data-toggle="tab">求人情報</a></li>
				<li><a class="brick-btn" href="#tab2" title="2" data-toggle="tab" >企業情報</a></li>
			</ul>

			<div id="tab-content" class="tab-content">
				  <div class="tab-pane fade active in" id="tab1">	
				  	<?php echo $this->element('../Secret/index-tab1',
											  [
												'CareerOpportunity' => $CareerOpportunity,
												'CorporatePrs' => $CorporatePrs,
											  ]); ?>
				  </div>
				  <div class="tab-pane fade" id="tab2">				
				  	<?php echo $this->element('../Secret/index-tab2',
											  [
												'CareerOpportunity' => $CareerOpportunity,
												'CorporatePrs' => $CorporatePrs,
											  ]); ?>
				  </div>		  
			</div>
			
			<ul id="tab-bottom" class="custom-nav-tabs">
				<li class="active"><a class="brick-btn" href="#tab1" data-toggle="tab">求人情報</a></li>
				<li><a class="brick-btn" href="#tab2" title="2" data-toggle="tab" >企業情報</a></li>
			</ul>
			
			<div class="entry-this-job bottom">
				<button class="brick-btn brick-green">この求人にエントリーする</button>
			</div>

		</div>
		
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-padding slider">
			<div id="right-menu" style="overflow:hidden">
				<?php echo $this->element('../Secret/index-right-column',
										  [
										  	'CorporatePrs' => $CorporatePrs,
										  ]); ?>
			</div>
		</div>
		
	</div>