<div class="common_secret-box">
	<div class="secret-title">
		仕事内容
	</div>
	<p>
		<?php echo str_replace("\n", "<br>",$CareerOpportunity['what_job']); ?>
	</p>
	
	<div class="secret-table">
		<div class="display-table">
			<div>勤務地</div>
			<div>
				<?php echo str_replace("\n", "<br>",$CareerOpportunity['work_location']);	?>
				<?php 
					$access = str_replace("◆", '<span class="ul-icon"></span>',$CareerOpportunity['access']);
					$access = explode("\n", $access);
					foreach( $access as $val)
					{
						if(!empty($val))
						echo '<p>'.$val.'</p>';
					}
				?>
			</div>
		</div>
		
		<div class="display-table">
			<div>勤務時間</div>
			<div><?php echo str_replace("\n", "<br>",$CareerOpportunity['working_hours']); ?></div>
		</div>
		
		
		<div class="display-table">
			<div>給与</div>
			<div><?php echo str_replace("\n", "<br>",$CareerOpportunity['salary']); ?></div>
		</div>
		
		<div class="display-table">
			<div>こんな経験・スキルが活かせます</div>
			<div>
				<?php
				if(!empty($CareerOpportunity['skill_experience'])) {
					$skill_experience = str_replace('○', '<span class="circle-icon"></span>', $CareerOpportunity['skill_experience']);
					echo str_replace("\n", '<br>', $skill_experience);
				}
				?>
			</div>
		</div>
		
		<div class="display-table">
			<div>休日・休暇</div>
			<div>
				<?php 
					$holiday_vacation = str_replace('◆', '<span class="ul-icon"></span>', $CareerOpportunity['holiday_vacation']);
					echo str_replace("\n",'<br>',$holiday_vacation);
				?>
			</div>
		</div>
		<div class="display-table">
			<div>待遇・福利</div>
			<div>
				<?php 
					$string = str_replace('■', '<span class="circle-icon"></span>', $CareerOpportunity['health_welfare']);
					$string = str_replace('◆', '<span class="circle-icon"></span>', $string);
					$string = explode("\n", $string);
					foreach( $string as $val)
					{
						if(!empty($val))
						echo '<p>'.$val.'</p>';

					}
				?>
			</div>
		</div>
		<div class="display-table">
			<div>応募方法</div>
			<div>
				<?php 			
					$string = str_replace('▼', '', $CareerOpportunity['application_method']);
					$string = str_replace('◆', '', $string);
					$string = explode("\n", $string);
					foreach( $string as $val)
					{
						if(!empty($val))
						echo '<p>'.$val.'</p>';
					}
				?>			
			</div>
		</div>
		
		<div class="display-table">
			<div>お問い合わせ</div>
			<div><?php echo str_replace("\n", "<br>",$CareerOpportunity['contact']); ?></div>
		</div>
	</div>
</div>