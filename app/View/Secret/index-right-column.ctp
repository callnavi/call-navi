<div class="material-box sub-column">
	<div class="material-header">企業情報</div>
	<div class="material-content">
		<ul class="secret-group">
			<li>
				<small>会社名：</small>
				<div class="mg-col"></div>
				<strong><?php echo $CorporatePrs['company_name']; ?></strong>
			</li>
			<?php if (!empty($CorporatePrs['website'])): ?>
			<li>
					<small>WEBサイト：</small>
					<div class="mg-col"></div>
					<a href="<?php echo $CorporatePrs['website']; ?>"><?php echo $CorporatePrs['website']; ?></a>
			</li>
			<?php endif; ?>
			<li>
			   	<small>募集事業所：</small>
			   	<div class="mg-col"></div>
				<strong><?php echo str_replace(array("\n",'　',' '), "<br>",$CorporatePrs['office_location']); ?></strong>

				<?php if(!empty($CorporatePrs['other_location'])): ?>
					<br>
					<small>本社・その他事業所：</small>
					<div class="mg-col"></div>
					<div class="text-bold">
					<?php echo str_replace(array("\n",'　',' '), "<br>",$CorporatePrs['other_location']); ?>
					</div>
				<?php endif; ?>

			</li>
			<li class="balance"><span><small>代表者</small></span><div><?php echo $CorporatePrs['representative']; ?></div></li>
			<li class="balance"><span><small>設立日</small></span><div><?php echo date('Y年m月d日', strtotime($CorporatePrs['establishment_date'])); ?></div></li>
			<li class="balance"><span><small>資本金</small></span><div><?php echo $CorporatePrs['capital']; ?></div></li>
			<?php if (!empty($CorporatePrs['employee_number'])): ?>
			<li class="balance"><span><small>従業員</small></span><div><?php echo $CorporatePrs['employee_number']; ?></div></li>
			<?php endif; ?>
			<li>
				<small>事業内容</small>
				<div class="mg-col"></div>
				<strong><?php echo $CorporatePrs['business']; ?></strong>
			 </li>
		</ul>
	</div>
</div>
