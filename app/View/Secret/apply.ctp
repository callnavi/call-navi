<?php
$this->set('title', 'エントリーフォーム');
$breadcrumb = array('page' => 'エントリーフォーム');
$this->start('css');
echo $this->Html->css('contact-page');
echo $this->Html->css('jquery.steps.css');
echo $this->Html->css('apply.css');
$this->end('css');

$this->start('script');
echo $this->Html->script('jquery.cookie-1.3.1');
echo $this->Html->script('jquery.steps.min');
echo $this->Html->script('jquery.validate.min');
?>
<script>
    function sendContactMail(url, arr) {
        $.ajax({
            url: url,
            type: 'POST',
            // dataType: 'json',
            data: {
                name: arr[0], mail: arr[1], content: arr[2], phonetic: arr[3], birthday: arr[4],
                tel: arr[5], gender: arr[6], id_job: arr[7]
            },
            success: function (result) {
                return result;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                return errorThrown;
            }
        });
    }

</script>
<?php $this->end('script'); ?>


<?php $this->start('sub_header'); ?>
<div class="top-banner">
	<div class="single-banner"></div>
</div>
<?php $this->end('sub_header'); ?>

<?php $this->start('header_breadcrumb'); ?>
<?php echo $this->element('Item/breadcrumb', $breadcrumb); ?>
<?php $this->end('header_breadcrumb'); ?>

<div class="single-form-layout">
    <div class="content">
        <div class="no-padding container-contact">
            <div id="contact-main" class="mg-bottom-120">
                <div class="list-header-simple mg-top-50">
                    <h1><?php echo $company['company_name']; ?> <small>のオリジナル求人</small></h1>
                </div>

                <div class="div-contact-form">
                    <!-- Form 1 -->
                    <form id="frm-contact-form1" action="" method="post" accept-charset="utf-8"
                          enctype="multipart/form-data" novalidate="novalidate" autocomplete="off">
                        <div id="contact-form">
                            <h2>STEP1</h2>
                            <section class="step1">
                                <div class="form-group" id="step1FullName">
                                    <div class="wp-st" id="st1-fullname">
                                        <label for="step1FullName" class="contact-lablel control-label">
                                            氏名<span class="require">必須</span>
                                        </label>

                                        <div class="contact-input">
                                            <input maxlength="100" type="text" name="step1FullName" id="step1-full-name"
                                                   value="" class="form-control" aria-required="true"
                                                   aria-invalid="false" placeholder="お名前を入力">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" id="step1Phonetic">
                                    <div class="wp-st" id="st1-fullname">
                                        <label for="step1FullName" class="contact-lablel control-label">
                                            ふりがな<span class="require">必須</span>
                                        </label>

                                        <div class="contact-input">
                                            <input maxlength="100" type="text" name="step1Phonetic" id="step1Phonetic"
                                                   value="" class="form-control" aria-required="true"
                                                   aria-invalid="false" placeholder="ふりがなを入力">
                                        </div>
                                    </div>
                                </div>
								<div class="form-panel">
									<div class="form-group" id="step1Birth">
										<div class="wp-st" id="st1-fullname">
											<label for="step1FullName" class="contact-lablel control-label">
												年齢<span class="require">必須</span>
											</label>

											<div class="contact-input">
												<input maxlength="8" type="text" name="step1Birth" id="step1Birth" value=""
													   class="form-control" aria-required="true" aria-invalid="false"
													   placeholder="生年月日を入力">
											</div>


										</div>
									</div>

									<div class="form-group" id="step1gender">
										<div class="wp-st" id="st1-fullname">
											<label for="step1FullName" class="contact-lablel control-label">
												性別<span class="require">必須</span>
											</label>

											<div class="contact-input div_gender">
												<div class="custom-checkbox">
													<label>
														<input type="radio" value="男性" name="step1gender" id="Male"
															   checked="true" aria-required="true">

														<div class="vir-label circle-style">
															<span class="vir-content">男性</span>
															<span class="vir-checkbox"></span>
														</div>
													</label>
												</div>

												<div class="custom-checkbox">
													<label>
														<input type="radio" value="女性" name="step1gender" id="Female"
															   aria-required="true">

														<div class="vir-label circle-style">
															<span class="vir-content">女性</span>
															<span class="vir-checkbox"></span>
														</div>
													</label>
												</div>
											</div>
										</div>
									</div>
								</div>

                                <div class="form-group" id="step1Phone">
                                    <div class="wp-st" id="st1-fullname">
                                        <label for="step1FullName" class="contact-lablel control-label">
                                            電話番号<span class="require">必須</span>
                                        </label>

                                        <div class="contact-input">
                                            <input maxlength="14" placeholder="電話番号を入力" type="text" name="step1Phone"
                                                   id="step1Phone" value="" class="form-control" aria-required="true"
                                                   aria-invalid="false">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" id="step1Email">
                                    <div class="wp-st" id="st1-email">
                                        <label for="step1Email" class="contact-lablel control-label">
                                            アドレス<span class="require">必須</span>
                                        </label>

                                        <div class="contact-input">
                                            <input maxlength="100" type="email" name="step1Email" id="step1-email"
                                                   value="" class="form-control" aria-required="true"
                                                   aria-invalid="false" placeholder="アドレスを入力">

                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" id="step1Content">
                                    <div class="wp-st" id="st1-content">
                                        <label for="step1Content" class="contact-lablel control-label">
                                            備考欄<!-- <span class="require">必須</span> -->
                                        </label>

                                        <div class="contact-input">
                                            <textarea maxlength="2000" name="step1Content" id="step1-content" value=""
                                                      class="form-control" aria-required="true" aria-invalid="false"
                                                      placeholder="ご質問等あればご入力ください。"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </section>

                           
                           
                            <h2>STEP2</h2>
                            <section class="step2">
                                <div class="form-group" id="step2FullName">
                                    <div class="wp-st" id="st2-fullname">
                                        <label for="step2FullName" class="contact-lablel control-label">
                                            氏名<span class="require">必須</span>
                                        </label>

                                        <div class="contact-input">
                                            <input type="text" name="step2FullName" disabled id="step2-full-name"
                                                   class="form-control" aria-required="true" aria-invalid="false"
                                                   placeholder="お名前を入力">

                                        </div>
                                    </div>
                                </div>


                                <div class="form-group" id="step2Phonetic">
                                    <div class="wp-st" id="st2-Phonetic">
                                        <label for="step2Phonetic" class="contact-lablel control-label">
                                            ふりがな<span class="require">必須</span>
                                        </label>

                                        <div class="contact-input">
                                            <input type="text" name="step2Phonetic" disabled id="step2Phonetic" value=""
                                                   class="form-control" aria-required="true" aria-invalid="false"
                                                   placeholder="ふりがなを入力">
                                        </div>
                                    </div>
                                </div>
								
                               <div class="form-panel">
                                <div class="form-group" id="step2Birth">
                                    <div class="wp-st" id="st2-Birth">
                                        <label for="step2Birth" class="contact-lablel control-label">
                                            年齢<span class="require">必須</span>
                                        </label>

                                        <div class="contact-input">
                                            <input type="text" name="step2Birth" id="step2Birth" value=""
                                                   class="form-control" aria-required="true" aria-invalid="false"
                                                   disabled placeholder="20160518">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" id="step2gender">
                                    <div class="wp-st" id="st1-gender">
                                        <label for="step2FullName" class="contact-lablel control-label">
                                            性別<span class="require">必須</span>
                                        </label>

                                        <div class="contact-input div_gender">
                                            <div class="custom-checkbox">
                                                <label>
                                                    <input type="radio" disabled value="男性" name="step2gender"
                                                           id="Malestep2" aria-required="true">

                                                    <div class="vir-label circle-style">
                                                        <span class="vir-content">男性</span>
                                                        <span class="vir-checkbox"></span>
                                                    </div>
                                                </label>
                                            </div>

                                            <div class="custom-checkbox">
                                                <label>
                                                    <input type="radio" disabled value="女性" name="step2gender"
                                                           id="Femalestep2" aria-required="true">

                                                    <div class="vir-label circle-style">
                                                        <span class="vir-content">女性</span>
                                                        <span class="vir-checkbox"></span>
                                                    </div>
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
							</div>

                                <div class="form-group" id="step2Phone">
                                    <div class="wp-st" id="st1-Phone">
                                        <label for="step2Phone" class="contact-lablel control-label">
                                            電話番号<span class="require">必須</span>
                                        </label>

                                        <div class="contact-input">
                                            <input type="text" name="step2Phone" id="step2Phone" value=""
                                                   class="form-control" aria-required="true" disabled
                                                   aria-invalid="false">
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group" id="step2Email">
                                    <div class="wp-st" id="st2-email">
                                        <label for="step2Email" class="contact-lablel control-label">
                                            アドレス<span class="require">必須</span>
                                        </label>

                                        <div class="contact-input">
                                            <input type="email" name="step2Email" disabled id="step2-email"
                                                   class="form-control" aria-required="true" aria-invalid="false"
                                                   placeholder="メールアドレス">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" id="step2Content">
                                    <div class="wp-st" id="st2-content">
                                        <label for="step2Content" class="contact-lablel control-label">
                                            備考欄
                                        </label>

                                        <div class="contact-input">
                                            <textarea name="step2Content" disabled id="step2-content"
                                                      class="form-control" aria-required="true" aria-invalid="false"
                                                      placeholder="質問等ございましたらご入力ください。">質問等ございましたらご入力ください。</textarea>

                                        </div>
                                    </div>
                                </div>
                            </section>

                            <h2>完了</h2>
                            <section class="finish">
                                <p>
                                    <strong>
                                    この度はお問い合せ頂き誠にありがとうございました。 
                                    <br>
                                    改めて担当者よりご連絡をさせていただきます。
                                    </strong>
                                    
                                    <a href="/" class="btn-finish">ページトップに戻る</a>
                                </p>
                            </section>
                        </div>

                        <input type="hidden" name="job_id" id="job_id" value="<?php echo $job_id ?>"/>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>

<?php echo $this->element('Item/script_google_yahoo'); ?>




<script>
    $(document).ready(function () {
        $('#select-form-contact1').change(function () {

            $("#frm-contact-form2").hide();
            $("#frm-contact-form1").show();

            $("#frm-contact-form2").validate().resetForm();

            $("#step1FullName").append($("#st1-fullname"));
            $("#step1Email").append($("#st1-email"));
            $("#step1Content").append($("#st1-content"));

            $("#step2FullName").append($("#st2-fullname"));
            $("#step2Email").append($("#st2-email"));
            $("#step2Content").append($("#st2-content"));

        });

        //Check validate form 1
        var form1 = $("#frm-contact-form1");
        form1.validate({
            errorPlacement: function errorPlacement(error, element) {
                element.before(error);
            },
            onkeyup: false,
            rules: {
                step1FullName: {
                    required: true,
                },
                step1Phonetic: {
                    required: true,
                },
                step1Birth: {
                    required: true,
                    birthday_valid: true
                },
                step1Phone: {
                    required: true,
                    phone_valid: true
                },
                step1Email: {
                    required: true,
                    email: true
                },
                // step1Content: {
                //     required: true,
                // },

            },

        });

        //contact form step by step
        var stepForm = $("#contact-form").steps({
            headerTag: "h2",
            bodyTag: "section",
            transitionEffect: "fade",
            cssClass: "tabcontrol",
			labels: {
        		previous: "入力内容のリセット",
        		//previous: "入力をやり直す",
    		},
			onStepChanged: function (event, currentIndex, priorIndex) { 
				//first step
				if (currentIndex == 0) {
					$("#contact-form .actions  ul li a[href=#previous]").html("入力内容のリセット");
				}
			},
            onStepChanging: function (event, currentIndex, newIndex) {			
                //go to step 2
                if (newIndex == 1) {
                    form1.validate().settings.ignore = ":disabled,:hidden";
                    if (form1.valid()) {
						$("#contact-form .actions  ul li a[href=#previous]").html("入力をやり直す");
                        $("#contact-form .actions  ul li a[href=#next]").html("上記内容で送信する");
                        
                        $("#contact-main .div-contact-form .step2 .form-group #step2-full-name").attr("value", $("#contact-main .div-contact-form .step1 .form-group #step1-full-name").val());
                        $("#contact-main .div-contact-form .step2 .form-group #step2Phonetic").attr("value", $("#contact-main .div-contact-form .step1 .form-group #step1Phonetic").val());

                        $("#contact-main .div-contact-form .step2 .form-group #step2Birth").attr("value", $("#contact-main .div-contact-form .step1 .form-group #step1Birth").val());

                        if ($('#Male:checked').length == 1) {
                            $("#Malestep2").prop("checked", true)
                        }
                        else {
                            $("#Femalestep2").prop("checked", true)
                        }

                        $("#contact-main .div-contact-form .step2 .form-group #step2Phone").attr("value", $("#contact-main .div-contact-form .step1 .form-group #step1Phone").val());

                        $("#contact-main .div-contact-form .step2 .form-group #step2-email").attr("value", $("#contact-main .div-contact-form .step1 .form-group #step1-email").val());
                        $("#contact-main .div-contact-form .step2 .form-group #step2-content").html($("#contact-main .div-contact-form .step1 .form-group #step1-content").val());
                        disableRadioButton();
                    } else {
                        return form1.valid();
                    }
                }

                //go to step 3
                if (newIndex == 2) {
                    var arr = [];
                    arr[0] = $("#contact-main .div-contact-form .step1 .form-group #step1-full-name").val();
                    arr[1] = $("#contact-main .div-contact-form .step1 .form-group #step1-email").val();
                    arr[2] = $("#contact-main .div-contact-form .step1 .form-group #step1-content").val();
                    arr[3] = $("#contact-main .div-contact-form .step1 .form-group #step1Phonetic").val();
                    arr[4] = $("#contact-main .div-contact-form .step1 .form-group #step1Birth").val();
                    arr[5] = $("#contact-main .div-contact-form .step1 .form-group #step1Phone").val();
                    arr[6] = $('#contact-main .div-contact-form .step1 .form-group input[name=step1gender]:checked').val();
                    arr[7] = $('#contact-main .div-contact-form #job_id').val();

                    sendContactMail("<?php echo $this->Html->url(array('controller' => 'secret', 'action' => 'SendMail'), true);?>", arr);
                    $("#contact-form .actions").hide();
                    $("#contact-form .content").css("height", "300px");
                    disableButtonStep();

                    
					if(__add_yahoo_google_script !== undefined)
					{
						__add_yahoo_google_script();
					}
                }

                return true;
            },
            onFinishing: function (event, currentIndex) {
                return true;
            },
            onFinished: function (event, currentIndex) {
                //finish
                window.top.location.href = "http://" + document.location.hostname;
            }
        });
		
		$("#contact-form .actions  ul li a[href=#previous]").click(function () {
			if($(this).text()=='入力内容のリセット')
			{
                $('input[name="step1FullName"').val("");
                $('input[name="step1Phonetic"').val("");
                $('input[name="step1Birth"').val("");
                $('input[name="step1Phone"').val("");
                $('input[name="step1Email"').val("");
                $('textarea[name="step1Content"').val("");
				$('#Male').prop('checked', true);
			}
		})
		
        $("#contact-form .actions  ul li a[href=#next]").html("確認画面へ進む");

        jQuery.validator.addMethod("phone_valid", function (phone_number, element) {
            phone_number = phone_number.replace(/\s+/g, "");
            return this.optional(element) || phone_number.length > 9 &&
                phone_number.match(/^[0-9]{3}[-\s]{0,1}[0-9]{4}[-\s]{0,1}[0-9]{3,4}$/);
        }, "携帯電話・固定電話をご入力ください。");

        jQuery.validator.addMethod("birthday_valid", function (value, element) {
            value = value.replace(/\s+/g, "");
            return /^\d{8}$/.test(value);
        }, "生年月日が不正です。");

        jQuery.extend(jQuery.validator.messages, {
            required: "この項目は必須です。",
            email: "メールアドレスが不正です。",
        });

        function disableRadioButton() {
            if ($("#select-form-contact1").is(':checked')) {
                $("#select-form-contact2").attr('disabled', true);
                $("#color-disable2").addClass("color-disable");
            }
            else {
                $("#select-form-contact1").attr('disabled', true);
                $("#color-disable1").addClass("color-disable");
            }
        }

        function disableButtonStep() {
            $("li.first.done").addClass('disabled');
            $("li.current").addClass('disabled');
        }
    });
</script>


