<?php
$breadcrumb = array(
	'parent' => array(
		0 => array(
			'link'=> '/secret',
			'title'=> 'シークレット求人'
		)
	),
	'page' => $CorporatePrs['company_name'],
);
$this->set('title', 'コールナビ｜'.$CorporatePrs['company_name']);
	$this->start('css'); 
		echo $this->Html->css('secret');
	$this->end('css');

	$this->start('script'); 
		echo $this->Html->script('create-company.js');
		$this->app->echoScriptScrollColumnJs();
	$this->end('script');
	$this->start('sub_header'); ?>
		<div class="no-padding container">
			<div class="row">
				<?php echo $this->element('Item/top_banner_version2');?>
			</div>
		</div>
	<?php $this->end('sub_header'); ?>


	<div class="no-padding container secret-layout">
   		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding secret-header ">
			<?php  echo $this->element('Item/breadcrumb', $breadcrumb); ?>
			<?php if(!empty($isPreview)){ ?>
				<?php echo $this->element('Item/btn_publish', array('status' => $CorporatePrs['status'])); ?>
			<?php } ?>
		</div>
		
		<div id="main-contain" class="col-md-6 col-lg-6 no-padding content">

			<div id="tab-content" class="tab-content" style="margin-top:0;">
				  <div class="tab-pane fade active in" id="tab2">
				  	<?php echo $this->element('../Secret/_preview_company',
											  [
												'CorporatePrs' => $CorporatePrs,
											  ]); ?>
				  </div>
			</div>
		</div>
		
		<div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 no-padding slider" style="margin-bottom: 10px">
			<div id="right-menu" style="overflow:hidden">
				<?php echo $this->element('../Secret/index-right-column',
										  [
										  	'CorporatePrs' => $CorporatePrs,
										  ]); ?>
			</div>
		</div>
		
	</div>