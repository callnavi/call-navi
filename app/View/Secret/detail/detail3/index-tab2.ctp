<div class="padding-lr-40 mg-top-40">
	<div class="bg-white panel-v3">
		<div class="topic-v3">メッセージ</div>

		<div class="secret-table3">
        <?php if(!empty($CareerOpportunity['what_kind_person'])){ ?>
      	 <div class="display-table">
            <div>どんな人が働いているか</div>
            <div>
                <?php echo str_replace("\n", "<br>", $CareerOpportunity['what_kind_person']); ?>
			 </div>
		</div>
        <?php } ?>       
      	<?php if (
                    !empty($CareerOpportunity['senior_avata']) ||
                    !empty($CareerOpportunity['senior_name_of_staff']) ||
                    !empty($CareerOpportunity['senior_employee_interview'])
                ): ?>
        <div class="display-table">
            <div>スタッフからのメッセージ</div>
            <div class="tab-content">
                <div class="person-present clearfix person-present__v3">
                    <?php if (!empty($CareerOpportunity['senior_avata'])): ?>
                        <div class="cell-img">
                            <img
                                src="<?php echo $this->webroot . "upload/secret/jobs/" . $CareerOpportunity['senior_avata']; ?>"
                                alt="">
                        </div>
                    <?php endif; ?>
                    <div class="cell-content">
                        <strong class="cell-header">
                            <?php echo strip_tags($CareerOpportunity['senior_joining_history']); ?>
                        </strong>
                        <p><?php echo strip_tags($CareerOpportunity['senior_name_of_staff']); ?></p>
                    </div>
                </div>
                <div class="mg-top-10 clearfix">
                    <?php if (!empty($CareerOpportunity['senior_employee_interview'])): ?>
                        <p><?php echo str_replace("\n", "<br>", $CareerOpportunity['senior_employee_interview']); ?></p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
        <?php endif; ?>
		</div>
		
		<?php echo $this->element('../Secret/detail/detail3/_button_apply'); ?>
	</div>
</div>