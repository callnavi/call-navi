<div class="padding-lr-40 mg-top-40">
	<div class="bg-white panel-v3">
		<div class="topic-v3">企業情報</div>
		
		<div class="secret-table3">
			<div class="display-table">
				<div>企業メッセージ</div>
				<div>
					<?php echo $CorporatePrs['corporate']; ?>
				</div>
			</div>

			<?php if (
				!empty($CorporatePrs['president_avata']) ||
				!empty($CorporatePrs['president_name']) ||
				!empty($CorporatePrs['president_description'])
			): ?>
				<div class="display-table">
					<div>社長はこんな人</div>
					<div class="tab-content">
						<div class="person-present clearfix person-present__v3">
							<?php if (!empty($CorporatePrs['president_avata'])): ?>
								<div class="cell-img">
									<img src="<?php echo $this->webroot . "upload/secret/companies/" . $CorporatePrs['president_avata']; ?>"
										alt="">
								</div>
							<?php endif; ?>

							<?php if (!empty($CorporatePrs['president_name'])): ?>
								<div class="cell-content">
									<strong
										class="cell-header">代表取締役社長：<?php echo str_replace("\n", "<br>", $CorporatePrs['president_name']); ?></strong>
								</div>
							<?php endif; ?>
						</div>
						<?php if (!empty($CorporatePrs['president_description'])): ?>
							<div class="mg-top-10 clearfix">
								<?php if (!empty($CareerOpportunity['senior_employee_interview']) ): ?>
									<p><?php echo str_replace("\n", "<br>", $CorporatePrs['president_description']); ?></p>
								<?php endif; ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
			<?php endif; ?>

			<div class="display-table">
				<div>会社名</div>
				<div><?php echo $CorporatePrs['company_name']; ?></div>
			</div>

			<div class="display-table">
				<div>代表者</div>
				<div><?php echo $CorporatePrs['representative']; ?></div>
			</div>

			<div class="display-table">
				<div>設立日</div>
				<div><?php echo date('Y年m月d日', strtotime($CorporatePrs['establishment_date'])); ?></div>
			</div>

			<div class="display-table">
				<div>資本金</div>
				<div><?php echo $CorporatePrs['capital']; ?></div>
			</div>

			<?php if (!empty($CorporatePrs['employee_number'])): ?>
				<div class="display-table">
					<div>従業員</div>
					<div><?php echo $CorporatePrs['employee_number']; ?></div>
				</div>
			<?php endif; ?>

			<?php if (!empty($CorporatePrs['website'])): ?>
				<div class="display-table">
					<div>Web</div>
					<div><a class="website-company"
							href="<?php echo $CorporatePrs['website']; ?>"><?php echo $CorporatePrs['website']; ?></a></div>
				</div>
			<?php endif; ?>


			<div class="display-table">
				<div>本社・支社</div>
				<div>
					<?php if (!empty($CorporatePrs['other_location'])): ?>
						<?php echo $CorporatePrs['other_location']; ?>
					<?php endif; ?>
				</div>
			</div>


			<div class="display-table">
				<div>事業内容</div>
				<div><?php echo $CorporatePrs['business']; ?></div>
			</div>
		</div>
		
		<?php echo $this->element('../Secret/detail/detail3/_button_apply'); ?>
	</div>
</div>