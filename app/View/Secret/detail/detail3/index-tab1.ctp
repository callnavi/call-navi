<div class="padding-lr-40">
	<div class="bg-white panel-v3">
		<?php echo $this->element('../Secret/detail/detail3/_header'); ?>
		<?php echo $this->element('../Secret/detail/detail3/_button_apply'); ?>
		
		<div class="topic-v3 mg-top-40">
			募集情報
		</div>

		<div class="secret-table3">
			<?php if(!empty($CareerOpportunity['specific_job'])){ ?>
			<div class="display-table">
				<div>仕事内容</div>
				<div><?php echo str_replace("\n", "<br>", $CareerOpportunity['specific_job']); ?></div>
        	</div>
        	<?php } ?>

			<?php if(!empty($CareerOpportunity['work_location']) || !empty($CareerOpportunity['access']) ){ ?>
			<div class="display-table">
				<div>勤務地 
                <?php
                	// function checking the database show map true or false
                    if(isset($CareerOpportunity['map_show'])){
                        if($CareerOpportunity['map_show'] == "1"){
                        ?>
                        <!-- delete issues#327
                        <a target="_blank" href="/callcenter_matome?keySearch=<?php echo rawurlencode($CareerOpportunity['branch_name']); ?>"><span class="label-blue">地図を確認する</span></a>
                        -->
                        <?php
                        }
                    }                               
                ?>                    
                   
                
                </div>
				<div>
					<?php echo str_replace("\n", "<br>", $CareerOpportunity['work_location']); ?>
					<?php
					// function checking the database show map true or false
                    if(isset($CareerOpportunity['map_show'])){
                        if($CareerOpportunity['map_show'] == "1"){
                        ?>					
					<div class="mg-top-20">
						<div class="google-map" id="map" style="height: 320px;"></div>
					</div>
                        <?php
                        }
                    }                               
                	?> 					
					<?php
					$access = str_replace("◆", '<span class="ul-icon"></span>', $CareerOpportunity['access']);
					$access = explode("\n", $access);
					foreach ($access as $val) {
						if (!empty($val))
							echo '<p>' . $val . '</p>';
					}
					?>
				</div>
			</div>
        	<?php } ?>			

			<?php if(!empty($CareerOpportunity['working_hours'])){ ?>
			<div class="display-table">
				<div>勤務時間</div>
				<div><?php echo str_replace("\n", "<br>", $CareerOpportunity['working_hours']); ?></div>
			</div>
        	<?php } ?>


			<?php if(!empty($CareerOpportunity['salary'])){ ?>
			<div class="display-table">
				<div>給与</div>
				<div><?php echo str_replace("\n", "<br>", $CareerOpportunity['salary']); ?></div>
			</div>
        	<?php } ?>

			<?php if(!empty($CareerOpportunity['person_of_interest'])){ ?>
			<div class="display-table">
				<div>対象となる方</div>
				<div><?php echo $CareerOpportunity['person_of_interest']; ?></div>
			</div>
        	<?php } ?>


			<?php if(!empty($CareerOpportunity['skill_experience'])){ ?>
			<div class="display-table">
				<div>こんな経験・スキルが活かせます</div>
				<div>
					<?php
					if (!empty($CareerOpportunity['skill_experience'])) {
						$skill_experience = str_replace('○', '<span class="circle-icon"></span>', $CareerOpportunity['skill_experience']);
						echo str_replace("\n", '<br>', $skill_experience);
					}
					?>
				</div>
			</div>
        	<?php } ?>

			<?php if(!empty($CareerOpportunity['holiday_vacation'])){ ?>
			<div class="display-table">
				<div>休日・休暇</div>
				<div>
					<?php
					$holiday_vacation = str_replace('◆', '<span class="ul-icon"></span>', $CareerOpportunity['holiday_vacation']);
					echo str_replace("\n", '<br>', $holiday_vacation);
					?>
				</div>
			</div>
        	<?php } ?>

			<?php if(!empty($CareerOpportunity['health_welfare'])){ ?>
			<div class="display-table">
				<div>待遇・福利厚生</div>
				<div>
					<?php
					$string = str_replace('■', '<span class="circle-icon"></span>', $CareerOpportunity['health_welfare']);
					$string = str_replace('◆', '<span class="circle-icon"></span>', $string);
					$string = explode("\n", $string);
					foreach ($string as $val) {
						if (!empty($val))
							echo '<p>' . $val . '</p>';

					}
					?>
				</div>
			</div>
        	<?php } ?>

			<?php if(!empty($CareerOpportunity['application_method'])){ ?>
			<div class="display-table">
				<div>応募方法</div>
				<div>
					<?php
					$string = str_replace('▼', '', $CareerOpportunity['application_method']);
					$string = str_replace('◆', '', $string);
					$string = explode("\n", $string);
					foreach ($string as $val) {
						if (!empty($val))
							echo '<p>' . $val . '</p>';
					}
					?>
				</div>
			</div>
        	<?php } ?>

		   <!-- <div class="display-table">
				<div>お問い合わせ</div>
				<div><?php /*echo str_replace("\n", "<br>", $CareerOpportunity['contact']);*/ ?></div>
			</div> -->

			<?php if(!empty($CareerOpportunity['other_remark'])){ ?>
			<div class="display-table">
				<div>その他備考</div>
				<div><?php echo $CareerOpportunity['other_remark']; ?></div>
			</div>
        	<?php } ?>
			
			<?php if(!empty($CareerOpportunity['comment'])){ ?>
			<div class="display-table">
				<div>コールナビ編集部コメント</div>
				<div>
					<?php if (!empty($CareerOpportunity['comment'])): ?>
						<?php echo $CareerOpportunity['comment']; ?>
					<?php endif; ?>
				</div>
			</div>
        	<?php } ?>

		</div>
		
		<?php echo $this->element('../Secret/detail/detail3/_button_apply'); ?>
	</div>
</div>