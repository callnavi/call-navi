<?php
    if(isset($CareerOpportunity['corporate_logo'])){
        $href_img =  $this->webroot."upload/secret/jobs/". $CareerOpportunity['corporate_logo'];;
    }else{
        $href_img =  $this->webroot."upload/secret/companies/". $CorporatePrs['corporate_logo'];
    }
?>
<div class="clearfix">
	<div class="pull-left"><h2 class="_h3 mg-0"><strong><?php echo $CareerOpportunity['branch_name']; ?></strong></h2></div>
	<div class="pull-right group-label">
		<?php echo $this->app->createLabel($CareerOpportunity['employment'], 'label-green'); ?>
	</div>
</div>
<hr class="hr--v3">
<h1 class="_h2 mg-0"><strong><?php echo $CareerOpportunity['job']; ?></strong></h1>
<hr class="hr--v3">

<div class="search-box secret-search-box secret-search-box__v3">
	<div class="display-table">
		<div class="search-image">
			<img src="<?php echo $href_img; ?>" alt="">
		</div>

		<div class="search-content secret-content">
			<?php if(!empty($CareerOpportunity['what_job'])){ ?>
			<div class="_p"><?php echo str_replace("\n", "<br>", $CareerOpportunity['what_job']); ?></div>
			<?php } ?>
			
			<h3 class="href mg-top-15 mg-bottom-15 _p"><?php echo strip_tags($CareerOpportunity['benefit']);?></h3>
			
			
			<?php if(!empty($CareerOpportunity['hourly_wage'])){ ?>
			<div class="search-salary">
				<span>給与</span>
				<p class="_p"><?php echo $CareerOpportunity['hourly_wage']; ?></p>
			</div>
			<?php } ?>
			
			<?php if(!empty($CareerOpportunity['business'])){ ?>
			<div class="search-salary">
				<span>職種</span>
				<p class="_p"><?php echo $CareerOpportunity['business']; ?></p>
			</div>
			<?php } ?>
			<?php if(!empty($CareerOpportunity['celebration'])){ ?>
			<div class="search-salary ">
				<span  style="background: #d1ab4c; padding: 1px 10px;">お祝い金</span>
				<p class="_p" style="color: #d1ab4c;"><?php echo $CareerOpportunity['celebration']; ?></p>
			</div>
			<?php } ?>			
			
		</div>
	</div>
</div>