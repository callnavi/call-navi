<?php

$this->set('title', $this->App->SecretDetailTitle1($CareerOpportunity));
$breadcrumb = array(
						'page' => $this->App->getSecretDetailBreadcrumbPage($CareerOpportunity),
						'parent' => array(array('title'=>'コールセンター オリジナル求人', 'link' => '/secret'))
				   );

$href = $this->Html->url(array('controller' => 'secret', 'action' => 'apply-step1?id='.$CareerOpportunity['id']), true);
$this->set('href', $href);

$this->start('css'); 
	echo $this->Html->css('secret2');
$this->end('css');

$this->start('script'); 
	echo $this->Html->script('create-company.js');
	$this->app->echoScriptScrollColumnJs();
	echo $this->Html->script('init-googlemap');
	echo '<script src="https://maps.google.com/maps/api/js?libraries=geometry&key='.Configure::read('webconfig.google_map_api_key').'"></script>';
?>
<?php $this->end('script');?>

<?php $this->start('header_breadcrumb'); ?>
<?php echo $this->element('Item/breadcrumb', $breadcrumb); ?>
<?php $this->end('header_breadcrumb'); ?>

<?php $this->start('sub_header'); ?>
<?php //echo $this->element('../Top/2017/_top_banner');?>

<div class="secret-v3 padding-t-170" id="secret_content">
	<div class="container">
		<?php echo $this->element('../Secret/detail/detail3/_time'); ?>
		
		<?php echo $this->element('../Secret/detail/detail3/index-tab1'); ?>
		
		<?php echo $this->element('../Secret/detail/detail3/index-tab2'); ?>
		
		<?php echo $this->element('../Secret/detail/detail3/index-tabslider'); ?>
		
		<?php echo $this->element('../Secret/detail/detail3/index-tab3'); ?>
	
		<div class="mg-top-60"></div>
	</div>
</div>
<?php $this->end('sub_header'); ?>


<script>
	window.onload = function() {
		// short timeout
		setTimeout(function() {
			$('body').scrollTop($('#secret_content').offset().top);
		}, 15);
	};

	$( document ).ready(function() {
		var direction = '<a style="font-size:12px" href="https://maps.google.com/maps?ll=<?php echo $CareerOpportunity['lat'];?>,<?php echo $CareerOpportunity['lng'];?>&z=16&t=m&hl=ja-JP&gl=JP&mapclient=embed&daddr=<?php echo $CareerOpportunity['map_location']; ?>" target="_blank"> ルート検索 ▶︎</a>';
		// set google map
		// input lat, lng , inforwindow, zoom
		<?php $latLng = !empty($CareerOpportunity['lat']) && !empty($CareerOpportunity['lng']) ? $CareerOpportunity['lat'].' , '.$CareerOpportunity['lng'] : '0,0'; ?>
		var contentString = "<h4 style='font-weight: bold;'><?php echo $CareerOpportunity['branch_name']; ?> "+direction+"</h4>"+
							"<div style='height: 24px;'><?php echo $CareerOpportunity['work_location']; ?></div>";
		initMap(<?php echo $latLng;?>,contentString, 13);
	});
</script>