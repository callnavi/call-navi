<div class="common_secret-box">
	<div class="secret-title">
		仕事内容
	</div>
	<p>
		<?php echo str_replace("\n", "<br>",$CareerOpportunity['specific_job']); ?>
	</p>	
				
	<?php if (!empty($CareerOpportunity['what_kind_person'])): ?>
	<div class="secret-title mg-top-50">
		どんな人が働いているか
	</div>
	<p>
		<?php echo str_replace("\n", "<br>",$CareerOpportunity['what_kind_person']); ?>
	</p>
	<?php endif; ?>
	
	<?php if (
				!empty($CareerOpportunity['senior_avata'])||
				!empty($CareerOpportunity['senior_name_of_staff'])||
				!empty($CareerOpportunity['senior_employee_interview'])
			 ): ?>
	<div class="secret-title mg-top-50">
		スタッフからのメッセージ
	</div>
	<?php endif; ?>
	<div class="person-present clearfix">
		<?php if (!empty($CareerOpportunity['senior_avata'])): ?>
		<div class="cell-img">
			<img src="<?php echo $this->webroot."upload/secret/jobs/".$CareerOpportunity['senior_avata']; ?>" alt="">
		</div>
		<?php endif; ?>
		<div class="cell-content">
			<strong class="cell-header">
				<?php echo strip_tags($CareerOpportunity['senior_joining_history']); ?>
				<?php echo strip_tags($CareerOpportunity['senior_name_of_staff']); ?>
			</strong>
			<?php if (!empty($CareerOpportunity['senior_employee_interview'])): ?>
				<p><?php echo str_replace("\n", "<br>",$CareerOpportunity['senior_employee_interview']); ?></p>
			<?php endif; ?>
		</div>
	</div>	
</div>
