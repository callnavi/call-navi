<div class="gray-area secret-info-banner">
	<div class="container-980">
			<div class="clearfix">
				<div class="pull-left">
					<h1><?php echo $CorporatePrs['company_name']; ?><small>のシークレット求人</small></h1>
					<time datetime="<?php echo $CareerOpportunity['release_date']; ?>">公開日： <?php echo date('Y年m月d日', strtotime($CareerOpportunity['release_date'])); ?></time>
				</div>
				<div class="pull-right">
					<button class="brick-btn brick-green brick-apply" onclick="location.href='<?php echo $href ?>'">
						<span>シークレット求人に</span>
						<br>
						簡単エントリー
					</button>
				</div>
			</div>
	</div>
</div>
<div class="container-980">
	<div class="search-box secret-search-box mg-top-40">
		<div class="display-table">
			<div class="search-image">
				<a href="">
					<img src="<?php echo $this->webroot."upload/secret/companies/". $CorporatePrs['corporate_logo']; ?>" alt="">
				</a>
			</div>
			
			<div class="search-content secret-content">
				<?php if (!empty($CareerOpportunity['benefit'])): ?>
				<div class="search-salary">
					<span>入社特典</span> <p><?php echo strip_tags($CareerOpportunity['benefit']);?></p>
				</div>
				<?php endif; ?>
				
				<div class="company-info">
					<?php if(!empty($CareerOpportunity['employment'])){ ?>
						<p>雇用形態： <?php echo $this->app->createLabel($CareerOpportunity['employment'], 'i-cat-secret-label'); ?></p>
					<?php } ?>

					<?php if(!empty($CareerOpportunity['hourly_wage'])){ ?>
						<p>給与：<?php echo $CareerOpportunity['hourly_wage']; ?></p>
					<?php } ?>
					<?php if(!empty($CareerOpportunity['job'])){ ?>
						<p>職種：<?php echo $CareerOpportunity['job']; ?></p>
					<?php } ?>
					<?php if(!empty($CareerOpportunity['business'])){ ?>
						<p>業種：<?php echo $CareerOpportunity['business']; ?></p>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
	
	<div class="location mg-top-60">
		勤務地 <span>|</span> <?php echo str_replace(array("\n",'　',' '), "　", strip_tags($CorporatePrs['office_location'])); ?>
	</div>
</div>