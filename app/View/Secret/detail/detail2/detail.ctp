<?php
$this->set('title', $this->App->getSecretDetailTitle($CareerOpportunity));
$breadcrumb = array(
						'page' => $this->App->getSecretDetailBreadcrumbPage($CareerOpportunity),
						'parent' => array(array('title'=>'コールセンター シークレット求人まとめ', 'link' => '/secret'))
				   );

$href = $this->Html->url(array('controller' => 'secret', 'action' => 'apply-step1?id='.$CareerOpportunity['id']), true);
$this->set('href', $href);

$this->start('css'); 
	echo $this->Html->css('secret2');
$this->end('css');

$this->start('script'); 
	echo $this->Html->script('create-company.js');
	$this->app->echoScriptScrollColumnJs();
	echo $this->Html->script('init-googlemap');
	echo '<script src="https://maps.google.com/maps/api/js?libraries=geometry&key='.Configure::read('webconfig.google_map_api_key').'"></script>';
?>
<script>
	$(function(){
		$('#tab-bottom').click(function(){
			$("html, body").animate({ scrollTop: 0 }, "slow");
		});
	});
</script>
<?php $this->end('script');?>

<?php $this->start('header_breadcrumb'); ?>
<?php echo $this->element('Item/breadcrumb', $breadcrumb); ?>
<?php $this->end('header_breadcrumb'); ?>


<?php $this->start('sub_header'); ?>
<div class="secret-layout">
<?php echo $this->element('../Secret/detail/detail2/_index-header'); ?>
<?php echo $this->element('../Secret/detail/detail2/_index-map'); ?>
<?php echo $this->element('../Secret/detail/detail2/_index-tab'); ?>
<?php echo $this->element('../Secret/detail/detail2/_index-footer'); ?>
</div>
<?php $this->end('sub_header'); ?>


<?php $this->start('sub_footer'); ?>
<?php echo $this->element('../Secret/detail/detail2/_index-fixed-box'); ?>
<?php $this->end('sub_footer'); ?>


<script>
	$( document ).ready(function() {
		var direction = '<a style="font-size:12px" href="https://maps.google.com/maps?ll=<?php echo $CorporatePrs['lat'];?>,<?php echo $CorporatePrs['lng'];?>&z=16&t=m&hl=ja-JP&gl=JP&mapclient=embed&daddr=<?php echo $CorporatePrs['map_location']; ?>" target="_blank"> ルート検索 ▶︎</a>';
		// set google map
		// input lat, lng , inforwindow, zoom
		<?php $latLng = !empty($CorporatePrs['lat']) && !empty($CorporatePrs['lng']) ? $CorporatePrs['lat'].' , '.$CorporatePrs['lng'] : '0,0'; ?>
		var contentString = "<h4 style='font-weight: bold;'><?php echo $CorporatePrs['company_name']; ?> "+direction+"</h4>"+
							"<p style='line-height:1;'><a href='<?php echo $CorporatePrs['website']; ?>' ><?php echo $CorporatePrs['website']; ?></a></p>"+
							"<div style='height: 24px;'><?php echo $CorporatePrs['office_location']; ?></div>";
		initMap(<?php echo $latLng;?>,contentString, 13);

	});
</script>