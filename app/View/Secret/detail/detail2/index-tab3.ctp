<div class="common_secret-box">

    <div class="secret-table">

        <div class="display-table">
            <div>企業メッセージ/div>
            <div>
                <?php echo str_replace("\n", "<br>", $CorporatePrs['corporate']); ?>
            </div>
        </div>

        <?php if (
            !empty($CorporatePrs['president_avata']) ||
            !empty($CorporatePrs['president_name']) ||
            !empty($CorporatePrs['president_description'])
        ): ?>
            <div class="display-table">
                <div>社長はこんな人</div>
                <div>
                    <div class="person-present clearfix">
                        <?php if (!empty($CorporatePrs['president_avata'])): ?>
                            <div class="cell-img">
                                <img
                                    src="<?php echo $this->webroot . "upload/secret/companies/" . $CorporatePrs['president_avata']; ?>"
                                    alt="">
                            </div>
                        <?php endif; ?>

                        <?php if (!empty($CorporatePrs['president_name'])): ?>
                            <div class="cell-content">
                                <strong
                                    class="cell-header">代表取締役社長：<?php echo str_replace("\n", "<br>", $CorporatePrs['president_name']); ?></strong>
                            </div>
                        <?php endif; ?>
                    </div>
                    <?php if (!empty($CorporatePrs['president_description'])): ?>
                        <div class="mg-top-30 clearfix">
                            <?php if (!empty($CareerOpportunity['senior_employee_interview'])): ?>
                                <p><?php echo str_replace("\n", "<br>", $CorporatePrs['president_description']); ?></p>
                            <?php endif; ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>
        <?php endif; ?>

    </div>

<!--    <div class="secret-title-comment mg-top-50">-->
<!--        コールナビ編集部コメント-->
<!--    </div>-->
<!--    <p>--><?php //echo str_replace("\n", "<br>", $CorporatePrs['comment']); ?><!--</p>-->

    <hr class="secret-hr">

    <?php echo $this->element('../Secret/detail/detail2/tabs/index-tab-company-info'); ?>
</div>