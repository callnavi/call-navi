<div id="secretFixedBox" class="secret-fixed-box isBottom">
	<div class="secret-fixed-box-wp">
		<div class="secret-fixed-box-company">
			<p>
				<big><?php echo $CorporatePrs['company_name']; ?></big>
				<small>のシークレット求人</small>
			</p>
			
			<p>	<?php if(!empty($CareerOpportunity['hourly_wage'])){ ?>
				給与：
				<?php echo $CareerOpportunity['hourly_wage']; ?>
				<?php } ?>
			</p>
		</div>
		<button class="brick-btn brick-green brick-apply" onclick="location.href='<?php echo $href ?>'">
				<span>シークレット求人に</span>
				簡単エントリー
		</button>
	</div>
</div>


<?php $this->append('script'); ?>
<script type="text/javascript">
$(function() {
	//defined
	var secretBox = $('#secretFixedBox');
	var maxY = $(document).height() - 300;
	var state = 0;
	var enableScroll = true;
	var windowHeight = $(window).height();
	var absoluteRight = $('body').width() - $(window).width() + 20;
	var changeState = function(scrollY){
		if(scrollY >= maxY)
		{
			if(state != 1)
			{
				secretBox.removeClass('isFixed').addClass('isBottom').css({
					right: absoluteRight + 'px'
				});
				state = 1;
			}
		}
		else
		{
			if(state != 2)
			{
				secretBox.removeClass('isBottom').addClass('isFixed').removeAttr('style');
				state = 2;
			}
		}
	}
	
	//event
	$(window).scroll(function (evt) {
		if(!enableScroll) return;
		changeState($(this).scrollTop()+windowHeight);
	});
	
	//event
	$(window).resize(function () {
		windowHeight = $(window).height();
		absoluteRight = $('body').width() - $(window).width() + 20;
	});
	

	/*[TAB]*/
	$('a[data-toggle="tab"]').on('shown.bs.tab',
		function(event) {
			var $relatedTarget, $target;
			maxY = $(document).height() - 331;
		}
	);
	/*[TAB/]*/
	
	//load
	changeState($(window).scrollTop());
});
</script>
<?php $this->end('script'); ?>