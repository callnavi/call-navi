<div class="tab-company-info">

    <div class="secret-table">
        <div class="display-table">
            <div>会社名</div>
            <div><?php echo $CorporatePrs['company_name']; ?></div>
        </div>

        <div class="display-table">
            <div>代表者</div>
            <div><?php echo $CorporatePrs['representative']; ?></div>
        </div>

        <div class="display-table">
            <div>設立日</div>
            <div><?php echo date('Y年m月d日', strtotime($CorporatePrs['establishment_date'])); ?></div>
        </div>

        <div class="display-table">
            <div>資本金</div>
            <div><?php echo $CorporatePrs['capital']; ?></div>
        </div>

        <?php if (!empty($CorporatePrs['employee_number'])): ?>
            <div class="display-table">
                <div>従業員</div>
                <div><?php echo $CorporatePrs['employee_number']; ?></div>
            </div>
        <?php endif; ?>

        <?php if (!empty($CorporatePrs['website'])): ?>
            <div class="display-table">
                <div>Web</div>
                <div><a class="website-company"
                        href="<?php echo $CorporatePrs['website']; ?>"><?php echo $CorporatePrs['website']; ?></a></div>
            </div>
        <?php endif; ?>


        <div class="display-table">
            <div>本社・支社</div>
            <div>
                <?php if (!empty($CorporatePrs['other_location'])): ?>
                    <?php echo str_replace(array("\n", '　', ' '), "<br>", $CorporatePrs['other_location']); ?>
                <?php endif; ?>
            </div>
        </div>


        <div class="display-table">
            <div>事業内容</div>
            <div><?php echo $CorporatePrs['business']; ?></div>
        </div>
    </div>
</div>
