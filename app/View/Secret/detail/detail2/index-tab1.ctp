<div class="common_secret-box">

    <div class="secret-table">
        <div class="display-table">
            <div>仕事内容</div>
            <div>
                <?php echo str_replace("\n", "<br>", $CareerOpportunity['specific_job']); ?>
            </div>
        </div>

        <div class="display-table">
            <div>どんな人が働いているか</div>
            <div>
                <?php echo str_replace("\n", "<br>", $CareerOpportunity['what_kind_person']); ?>

                <?php if (
                    !empty($CareerOpportunity['senior_avata']) ||
                    !empty($CareerOpportunity['senior_name_of_staff']) ||
                    !empty($CareerOpportunity['senior_employee_interview'])
                ): ?>
                    <div class="secret-title mg-top-50">
                        スタッフからのメッセージ
                    </div>
                <?php endif; ?>
                <div class="person-present clearfix">
                    <?php if (!empty($CareerOpportunity['senior_avata'])): ?>
                        <div class="cell-img">
                            <img
                                src="<?php echo $this->webroot . "upload/secret/jobs/" . $CareerOpportunity['senior_avata']; ?>"
                                alt="">
                        </div>
                    <?php endif; ?>
                    <div class="cell-content">
                        <strong class="cell-header">
                            <?php echo strip_tags($CareerOpportunity['senior_joining_history']); ?>
                        </strong>
                        <p><?php echo strip_tags($CareerOpportunity['senior_name_of_staff']); ?></p>
                    </div>
                </div>
                <div class="mg-top-30 clearfix">
                    <?php if (!empty($CareerOpportunity['senior_employee_interview'])): ?>
                        <p><?php echo str_replace("\n", "<br>", $CareerOpportunity['senior_employee_interview']); ?></p>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
