<div class="gray-area secret-info-footer">
	<div class="container-980">
		<div class="display-table">
			<div class="table-cell">
				<button class="brick-btn brick-green brick-apply" onclick="location.href='<?php echo $href ?>'">
						<span>シークレット求人に</span>
						<br>
						簡単エントリー
					</button>
			</div>
			<div class="table-cell secret-info-footer-company">
				<p><?php echo $CorporatePrs['company_name']; ?><small>のシークレット求人</small></p>
				
				<p> <?php if(!empty($CareerOpportunity['employment'])){ ?>
						<span>雇用形態： <?php echo $this->app->createLabel($CareerOpportunity['employment'], 'i-cat-secret-label'); ?></span>
						&#160;&#160;&#160;&#160;
					<?php } ?>
					
					<?php if(!empty($CareerOpportunity['hourly_wage'])){ ?>
						給与：<?php echo $CareerOpportunity['hourly_wage']; ?>
					<?php } ?>
				</p>
			</div>
		</div>
	</div>
</div>