<?php $this->layout = 'single_column_v2_sp'; ?>
<?php
$this->set('title', 'エントリーフォーム');
$this->start('css');
echo $this->Html->css('jquery.steps-sp');
echo $this->Html->css('apply-v2-sp');
$this->end('css');
?>
<div class="banner">
	<div class="background clip-bottom-arrow">
		<img src="/img/banner/apply_banner_sp.jpg">
	</div>
</div>

<div id="contact-main">
	<div class="company-title mg-top-60">
		<h1><?php echo $company['company_name']; ?>
		<small>のオリジナル求人</small></h1>
	</div>
	<div class="header-title df-padding mg-top-60">
		エントリーフォーム
	</div>
	<div class="div-contact-form mg-top-20">
			<form id="frm-contact-form1" action="" method="post" accept-charset="utf-8" enctype="multipart/form-data"
				  novalidate="novalidate">
				<div id="contact-form">
					<h2>STEP1</h2>
					<section class="step1">
						<div class="form-group" id="step1FullName">
							<div id="st1-fullname">
								<label for="step1FullName" class="contact-lablel control-label">
									氏名<span class="require">必須</span>
								</label>

								<div class="contact-input">
									<input type="text" name="step1FullName" id="step1-full-name" value=""
										   class="form-control" aria-required="true" aria-invalid="false"
										   placeholder="お名前を入力">
								</div>
							</div>
						</div>
						<div class="form-group" id="step1Phonetic">
							<div id="st1-fullname">
								<label for="step1FullName" class="contact-lablel control-label">
									ふりがな<span class="require">必須</span>
								</label>

								<div class="contact-input">
									<input type="text" name="step1Phonetic" id="step1Phonetic" value=""
										   class="form-control" aria-required="true" aria-invalid="false"
										   placeholder="ふりがなを入力">
								</div>
							</div>
						</div>

						<div class="form-group" id="step1Birth">
							<div id="st1-fullname">
								<label for="step1FullName" class="contact-lablel control-label">
									年齢<span class="require">必須</span>
								</label>

								<div class="contact-input">
									<input type="text" name="step1Birth" id="step1Birth" value="" class="form-control"
										   aria-required="true" aria-invalid="false" placeholder="例）20160912">
								</div>
							</div>
						</div>

						<div class="form-group" id="step1gender">
							<div id="st1-fullname">
								<label for="step1FullName" class="contact-lablel control-label">
									性別<span class="require">必須</span>
								</label>

								<div class="contact-input div_gender">
									<div class="custom-checkbox">
										<label>
											<input checked type="radio" value="男性" name="step1gender" id="Male" checked="true" aria-required="true">

											<div class="vir-label circle-style">
												<span class="vir-content">男性</span>
												<span class="vir-checkbox"></span>
											</div>
										</label>
									</div>

									<div class="custom-checkbox">
										<label>
											<input type="radio" value="女性" name="step1gender" id="Female" aria-required="true">

											<div class="vir-label circle-style">
												<span class="vir-content">女性</span>
												<span class="vir-checkbox"></span>
											</div>
										</label>
									</div>
								</div>
							</div>
						</div>


						<div class="form-group" id="step1Phone">
							<div id="st1-fullname">
								<label for="step1FullName" class="contact-lablel control-label">
									電話番号<span class="require">必須</span>
								</label>

								<div class="contact-input">
									<input type="text" name="step1Phone" id="step1Phone" value="" class="form-control"
										   aria-required="true" aria-invalid="false" placeholder="電話番号を入力">
								</div>
							</div>
						</div>

						<div class="form-group" id="step1Email">
							<div id="st1-email">
								<label for="step1Email" class="contact-lablel control-label">
									E-mail<span class="require">必須</span>
								</label>

								<div class="contact-input">
									<input type="email" name="step1Email" id="step1-email" value="" class="form-control"
										   aria-required="true" aria-invalid="false" placeholder="メールアドレスを入力">
								</div>
							</div>
						</div>
						<div class="form-group" id="step1Content">
							<div id="st1-content">
								<label for="step1Content" class="contact-lablel control-label">
									備考欄<!-- <span class="require">必須</span> -->
								</label>

								<div class="contact-input">
									<textarea name="step1Content" id="step1-content" value="" class="form-control"
											  aria-required="true" aria-invalid="false"
											  placeholder="ご質問等あればご入力ください。"></textarea>
								</div>
							</div>
						</div>
					</section>

					<h2>STEP2</h2>
					<section class="step2">
						<div class="form-group" id="step2FullName">
							<div id="st2-fullname">
								<label for="step2FullName" class="contact-lablel control-label">
									氏名
								</label>

								<div class="contact-input">
									<input type="text" name="step2FullName" disabled id="step2-full-name"
										   class="form-control" aria-required="true" aria-invalid="false"
										   placeholder="お名前を入力">

								</div>
							</div>
						</div>


						<div class="form-group" id="step2Phonetic">
							<div id="st2-Phonetic">
								<label for="step2Phonetic" class="contact-lablel control-label">
									ふりがな
								</label>

								<div class="contact-input">
									<input type="text" name="step2Phonetic" disabled id="step2Phonetic" value=""
										   class="form-control" aria-required="true" aria-invalid="false"
										   placeholder="ふりがなを入力">
								</div>
							</div>
						</div>

						<div class="form-group" id="step2Birth">
							<div id="st2-Birth">
								<label for="step2Birth" class="contact-lablel control-label">
									年齢
								</label>

								<div class="contact-input">
									<input type="text" name="step2Birth" id="step2Birth" value="" class="form-control"
										   aria-required="true" aria-invalid="false" disabled placeholder="20160518">
								</div>
							</div>
						</div>

						<div class="form-group" id="step2gender">
							<div id="st1-gender">
								<label for="step2FullName" class="contact-lablel control-label">
									性別
								</label>

								<div class="contact-input div_gender">
									<div class="custom-checkbox">
										<label>
											<input disabled type="radio" value="男性" name="step2gender" id="Malestep2" checked="true" aria-required="true">

											<div class="vir-label circle-style">
												<span class="vir-content">男性</span>
												<span class="vir-checkbox"></span>
											</div>
										</label>
									</div>

									<div class="custom-checkbox">
										<label>
											<input disabled type="radio" value="女性" name="step2gender" id="Femalestep2" aria-required="true">

											<div class="vir-label circle-style">
												<span class="vir-content">女性</span>
												<span class="vir-checkbox"></span>
											</div>
										</label>
									</div>
								</div>
							</div>
						</div>


						<div class="form-group" id="step2Phone">
							<div id="st1-Phone">
								<label for="step2Phone" class="contact-lablel control-label">
									電話番号
								</label>

								<div class="contact-input">
									<input type="text" name="step2Phone" id="step2Phone" value="" class="form-control"
										   aria-required="true" disabled aria-invalid="false">
								</div>
							</div>
						</div>

						<div class="form-group" id="step2Email">
							<div id="st2-email">
								<label for="step2Email" class="contact-lablel control-label">
									E-mail
								</label>

								<div class="contact-input">
									<input type="email" name="step2Email" disabled id="step2-email" class="form-control"
										   aria-required="true" aria-invalid="false" placeholder="メールアドレス">
								</div>
							</div>
						</div>

						<div class="form-group" id="step2Content">
							<div id="st2-content">
								<label for="step2Content" class="contact-lablel control-label">
									備考欄
								</label>

								<div class="contact-input">
									<textarea name="step2Content" disabled id="step2-content" class="form-control"
											  aria-required="true" aria-invalid="false" placeholder="質問等ございましたらご入力ください。">質問等ございましたらご入力ください。</textarea>

								</div>
							</div>
						</div>
					</section>

					<h2>完了</h2>
					<section class="finish">
						<p>
							この度はお問い合せ頂き誠にありがとうございました。
							<br>
							改めて担当者よりご連絡をさせていただきます。
						</p>
						<div class="finish-link">
							<a href="/">ページトップに戻る</a>
						</div>
					</section>
				</div>

				<input type="hidden" name="job_id" id="job_id" value="<?php echo $job_id ?>"/>
			</form>

	</div>
</div>


<?php echo $this->element('Item/script_google_yahoo'); ?>

<?php
$this->start('script');
	echo $this->Html->script('jquery.cookie-1.3.1.js');
	echo $this->Html->script('jquery.steps.min.js');
	echo $this->Html->script('jquery.validate.min.js');
	echo $this->Html->script('apply/apply-v2-sp.js');
$this->end('script');
?>


