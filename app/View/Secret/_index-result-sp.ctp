<?php 
	$secretJobsHasInterview = $this->app->getListSecretJobsHasInterview();
?>
<div class="secret-banner-benefits df-padding mg-bottom-40"><a href="/oiwaikin"><img width="100%" src="/img/oiwaikin/bnr_top_sp.png" alt="【お祝い金】最大90,000円 採用が決まればプレゼント！"></a></div >
<div class="search-layout">
	<div class="search-result">
		<?php if ($data): ?>
			<ul class="clearfix">
				<?php $stt = 0; ?>
				<?php foreach ($data as $job): $stt ++;


					//check secret job has interview
					$hasInterview = false;
                    $interviewlink = "";
                    $benefit =  $job['CareerOpportunity']['celebration'];
					$company_id = $job['corporate_prs']['id'];
					$secret_job_id = $job['CareerOpportunity']['id'];
					foreach ($secretJobsHasInterview as $secret) {
						if($secret['Contents']['company_id'] == $company_id && $secret['Contents']['secret_job_id'] == $secret_job_id)
						{
							$hasInterview = true;
                            $interviewlink =  "/secret/".$secret['Contents']['title'];
							break;
						}
					}


					$value = $job['CareerOpportunity'];
                    if($hasInterview == ""){
                        $href = $this->App->createSecretDetailHref($value);
                    }else{
                        $href = $interviewlink;   
                    }
				?>
					<?php $company = $job['CareerOpportunity']['branch_name']; ?>
					<?php $clip_title = strlen($value['job'])> 60 ? mb_substr(   $value['job'],0,60,'UTF-8').'...' :  $value['job'];?>
					<?php 
						  $value['salary'] = strip_tags($value['salary']);
							$salary = mb_substr(strip_tags($value['hourly_wage']),0,35,'utf-8');
					?>
					<?php if($stt == 6): ?>
						<?php echo $this->element('../Secret/_index-secret-slider-sp'); ?>
						<li class="y_secret_banner_bottom"></li>
					<?php endif; ?>

					<li>						
						<div class="df-padding y_search_sp">
							<div class="article-box">

								<?php if($hasInterview): ?>
								<span class="interview-tag">企業インタビュー</span>
								<?php endif; ?>
                                <?php if($benefit): ?>
                                <span class="benefit-tag">お祝い金あり</span>
                                <?php endif; ?>
								<p class="title"><?php echo $value['job']; ?></p>
								<div class="display-table">
									<div class="search-image">
										<a target="_blank" href="<?php echo $href; ?>">
											<img src="<?php echo $this->webroot."upload/secret/jobs/".$value['corporate_logo'] ;?>" alt="<?php echo $value['job']; ?>">
										</a>
									</div>
                                    
                                    <div class="search-content">
                                      <ul>
                                        <li class="search-content__salary"><span class="label-brick-inline">給与</span><span class="salary"><?php echo $salary; ?></span></li>
                                        <?php if($benefit): ?>
                                            <li class="mg-top"><span class="label-brick-inline--secondary">お祝い金</span> 
                                            <span class="benefit"><?php echo (strip_tags($benefit)); ?></span></li><span class="salary"></span>
                                        <?php endif; ?>

                                      </ul>
                                      <hr class="secret-hr"><span class="company text-ellipsis-100"><?php echo( $value['branch_name']); ?></span>
                                    </div>                                    
								</div>
								<br>
								<a target="_blank" class="y_search_sp_btn" href="<?php echo $href; ?>">詳細を確認する</a>

								<div class="site_name"></div>


							</div>
						</div>
					</li>
				<?php endforeach; ?>
				
				<?php if (count($data) < 6): ?>
					<?php echo $this->element('../Secret/_index-secret-slider-sp'); ?>
				<?php endif; ?>
			</ul>
			<?php else: ?>
			<div style="padding:3rem;background-color: #eeeeee;">
				<span style="font-size:14px"><b>データなし</b></span>
				<div style="margin-top:3rem;">
					<p>検索のヒント:</p>
					<div style="padding-left:2rem">
						<p>キーワードに誤字・脱字がないか確認します。</p>
						<p>別のキーワードを試してみます。</p>
						<p>もっと一般的なキーワードに変えてみます。</p>
					</div>
				</div>
			</div>
		<?php endif; ?>
	</div>
	<?php 
	$pageCount = ceil($search_total / $size);
	if($pageCount > 1): ?>
		<?php if (count($data) < 6): ?>
		<div class="mg-top-30"></div>
		<?php endif; ?>
	<div class="y_pagination_sp pagination">
	<?php 
		echo $this->element('Item/y_pagination_sp', ['currentPage'=>$currentPage, 'pageCount'=>$pageCount]); 
	?>
	</div>
	<?php endif; ?>
	
		
</div>