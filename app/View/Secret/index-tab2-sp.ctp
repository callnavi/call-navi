<?php

$this->start('css');
echo $this->Html->css('owl.carousel/assets/owl.carousel.min.css');
$this->end('css');

$this->start('script');
echo $this->Html->script('owl.carousel/owl.carousel.min.js');
?>
<script>
	$(document).ready(function() {
		$('#slide_images').owlCarousel({
			navigation : false, // Show next and prev buttons
			slideSpeed : 300,
			paginationSpeed : 400,
			pagination:false,
			autoPlay:true,
			items : 2,
			itemsDesktop : [1199,4],
			itemsDesktopSmall : [979,4],
			itemsTablet: [768,3],
			itemsTabletSmall: false,
			itemsMobile : [479,2]

		});
	});


</script>
<?php
$this->end('script');
?>

<!-- myTabContent-->

<div class="slidertop contenttab crontop">
	<div class="secret-pr-title">
		<hr>
		<span>企業メッセージ</span>
	</div>
	<?php 
			
				$corporate =  explode("\n",$CorporatePrs['corporate']); 
				foreach( $corporate as $val)
				{
					echo '<p>'.$val.'</p>';
				}
			
			?>
			
			<?php if (
						!empty($CorporatePrs['president_avata'])||
						!empty($CorporatePrs['president_name'])||
						!empty($CorporatePrs['president_description'])
					 ): ?>
			<p class="title">社長はこんな人</p>
			<?php endif; ?>
			<?php if (!empty($CorporatePrs['president_avata'])): ?>
				<img src="<?php echo $this->webroot."upload/secret/companies/".$CorporatePrs['president_avata']; ?>" alt="">
			<?php endif; ?>
			<?php if (!empty($CorporatePrs['president_name'])): ?>
				<h4>代表取締役社長：<?php echo str_replace("\n", "<br>",$CorporatePrs['president_name']); ?></h4>
			<?php endif; ?>
			<?php
				if (!empty($CorporatePrs['president_description'])) {
					$corporate = explode("\n", $CorporatePrs['president_description']);
					foreach ($corporate as $val) {
						echo '<p>' . $val . '</p>';
					}
				}
			?>
		
		<p class="title">コールナビ編集部コメント</p>
		<?php
			
			$corporate =  explode("\n",$CorporatePrs['comment']);
			foreach( $corporate as $val)
			{
				echo '<p>'.$val.'</p>';
			}
			
		?>

		<div class="secret-table">
			<ul>
				<li class="secret-ulfist">会社名：</li>
				<li>
					<?php echo $CorporatePrs['company_name']; ?>
				</li>
			</ul>
		</div>
		<?php if(!empty($CorporatePrs['website'])){ ?>
			<div class="secret-table">
				<ul class="secret-ulfist">WEBサイト：</ul>
				<a href="<?php echo $CorporatePrs['website']; ?>">
					<ul>
						<?php echo $CorporatePrs['website']; ?>
					</ul>
				</a>
			</div>
		<?php } ?>
						<div class="secret-table border-secret spec">
							<ul>
								<li class="secret-ulfist">募集事業所：</li>
					<?php
			
						$office_location =  explode("\n",$CorporatePrs['office_location']); 
						foreach( $office_location as $val)
						{
							echo '<li>'.$val.'</li>';
						}
			
					?>

					<?php
						if(!empty($CorporatePrs['other_location'])) {
							echo '<li class="cus-table-top secret-ulfist">本社・その他事業所：</li>';
							$other_location = explode("\n", $CorporatePrs['other_location']);
							foreach ($other_location as $val) {
								echo '<li class="cus-ul-icon-tab">' . $val . '</li>';
							}
						}
					?>
							</ul>
						</div>
						<br>
						<div class="secret-table cell">
							<table>
								<tr>
									<td class="colum fist">代表者</td>
									<td class="colum-right fist-right">
										<?php echo $CorporatePrs['representative']; ?>
									</td>
								</tr>
								<tr>

									<td class="colum">設立日</td>
									<td class="colum-right">
										<p><?php echo date('Y年m月d日', strtotime($CorporatePrs['establishment_date'])); ?></p>
									</td>
								</tr>
								<?php if(!empty($CorporatePrs['capital'])){ ?>
									<tr>
										<td class="colum">資本金</td>
										<td class="colum-right">
											<?php echo $CorporatePrs['capital']; ?>
										</td>
									</tr>
								<?php } ?>
								<?php if(!empty($CorporatePrs['employee_number'])){ ?>
									<tr>
										<td class="colum">従業員</td>
										<td class="colum-right">
											<?php echo $CorporatePrs['employee_number']; ?>
										</td>
									</tr>
								<?php } ?>
							</table>
							<ul>
								<li class="cus-icon-tab2 secret-ulfist">事業内容</li>
								<?php
							$icontab =[];
							$tab2right = explode("\n" ,$CorporatePrs['business']);
							foreach( $tab2right as $index=>$value)
							{
								if(strpos($value,'◆')!==false)
								{
									$icontab[] = str_replace('◆','',$value);
									unset($tab2right[$index]);
								}
							}
						
						
							foreach( $tab2right as $tab2right_val)
							{
								echo '<li class="cus-icon-tab2right">'.$tab2right_val.'</li>';
							}
						
							foreach( $icontab as $icontab_val)
							{
								echo '<li class="ul-icontab">'.$icontab_val.'</li>';
							}
						?>
							</ul>
						</div>
</div>


<!-- <div class="cus-slideshow">
			<div id="jssor_1" class="secret-slideshow">
        <div data-u="slides" class="crus-slide">
            <div style="display: none;">
                <img data-u="image" src="img/005.jpg" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="img/006.jpg" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="img/011.jpg" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="img/013.jpg" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="img/014.jpg" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="img/019.jpg" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="img/020.jpg" />
            </div>
            <div style="display: none;">
                <img data-u="image" src="img/021.jpg" />
            </div>
        
        </div>

		</div> 
		</div>-->

	<div class="box_slider">
		<div id="slide_images" class="owl-carousel">
			<?php
			echo (!empty($CareerOpportunity['thumb_1'])) ? "<div class='item'><img src='".$this->webroot."upload/secret/jobs/".$CareerOpportunity['thumb_1']."' /> </div>" : "";
			echo (!empty($CareerOpportunity['thumb_2'])) ? "<div class='item'><img src='".$this->webroot."upload/secret/jobs/".$CareerOpportunity['thumb_2']."' /> </div>" : "";
			echo (!empty($CareerOpportunity['thumb_3'])) ? "<div class='item'><img src='".$this->webroot."upload/secret/jobs/".$CareerOpportunity['thumb_3']."' /> </div>" : "";
			?>
		</div>

	</div>

<div class="secret-button">
	<button class="brick-btn" onclick="location.href='<?php echo $this->Html->url(array('controller' => 'secret', 'action' => 'apply?id='.$CareerOpportunity['id']), true)?>'">この求人にエントリーする</button>
</div>



<div class="slidertop contenttab secret-box boxtab">
	<p class="title">仕事内容</p>

	<?php 
			
						$specific_job =  explode("\n",$CareerOpportunity['specific_job']); 
						foreach( $specific_job as $val)
						{
							echo '<p>'.$val.'</p>';
						}
			
			?>

		<div class="secret-table">
			<ul>
				<li class="secret-ulfist">対象となる方（応募資格）：</li>
				<?php 
						$person_of_interest =  explode("\n",$CareerOpportunity['person_of_interest']); 
						foreach( $person_of_interest as $val)
						{
							echo '<li>'.$val.'</li>';
						}
					?>
			</ul>
		</div>
		<div class="secret-table">
			<ul>
				<li class="secret-ulfist">勤務時間：</li>
				<li>
					<?php echo str_replace("\n", "<br>",$CareerOpportunity['working_hours']); ?>
				</li>
			</ul>
		</div>
		<div class="secret-table border-secret">
			<ul>
				<li class="secret-ulfist">給与：</li>
				<?php 
						$working_hours =  explode("\n",$CareerOpportunity['salary']); 
						foreach( $working_hours as $val)
						{
							echo '<li>'.$val.'</li>';
						}
					?>
			</ul>
		</div>
		<p class="title">こんな経験・スキルが活かせます</p>
			<p>
				<?php
					if(!empty($CareerOpportunity['skill_experience'])) {
						$skill_experience = str_replace('○', '<span class="icon-bol"></span>', $CareerOpportunity['skill_experience']);
						echo str_replace("\n", '<br>', $skill_experience);
					}
				?>
			</p>
			<div class="secret-table">
			<ul>
				<li class="secret-ulfist">休日・休暇：</li>
				<ul>
					<?php 
					$holiday_vacation = str_replace('◆', '<li class="ul-icon"></li>', $CareerOpportunity['holiday_vacation']);
					echo str_replace("\n",'<br>',$holiday_vacation);
				?>
				</ul>
			</ul>
			</div>
			<div class="secret-table">
			<ul>
				<li class="secret-ulfist">待遇・福利厚生：</li>

				<ul>
					<?php

						$string = str_replace('■', '', $CareerOpportunity['health_welfare']);
						$string = str_replace('◆', '', $string);
						$string = explode("\n", $string);
						foreach( $string as $val)
						{
							if(!empty($val))
							echo '<li class="ul-icon">'.$val.'</li>';
						}
					?>
				</ul>
			</ul>
			</div>
			<div class="secret-table border-secret">
			<ul>
				<li class="secret-ulfist">応募方法：</li>
				<ul>
					<?php
						$string = str_replace('▼', '<span class="cus-ul-icon"></span>', $CareerOpportunity['application_method']);
						$string = str_replace('◆', '<span class="ul-icon"></span>', $string);
						$string = explode("\n", $string);
						foreach( $string as $val)
						{
							if(!empty($val))
							echo '<li>'.$val.'</li>';
						}
					?>
				</ul>
			</ul>
			</div>
			<?php if (!empty($CareerOpportunity['other_remark'])): ?>
			<p class="title">備考</p>
			<ul class="secret-tel">
				<?php 
					$remark =  explode("\n",$CareerOpportunity['other_remark']); 
					foreach( $remark as $val)
					{
						echo '<li>'.$val.'</li>';
					}
				?>
			</ul>
			<?php endif; ?>
			
			<p class="title">お問い合わせ</p>

			<ul class="secret-tel">
				<?php 
					$contact =  explode("\n",$CareerOpportunity['contact']); 
					foreach( $contact as $val)
					{
						echo '<li>'.$val.'</li>';
					}
				?>
			</ul>
</div>

<!-- /myTabContent-->