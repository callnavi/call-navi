<?php $this->layout = 'single_column_v2_sp'; ?>
<?php
$this->set('title', 'コールセンター オリジナル求人情報｜ コールセンター特化型求人サイト「コールナビ」');
$this->start('css');
echo $this->Html->css('search-sp');
$this->end('css');
?>
<?php
$this->start('script');
echo $this->Html->script('secret-sp.js');
$this->end('script');

$this->start('meta');
    //meta description keywords image
    echo Configure::read('webconfig.meta_description');
    echo Configure::read('webconfig.meta_keywords');
    echo Configure::read('webconfig.apple_touch_icon_precomposed');
    echo Configure::read('webconfig.ogp');  
$this->end('meta');
?>

<?php echo $this->element('../Secret/_index-search-sp'); ?>
<?php echo $this->element('../Secret/_index-result-sp'); ?>
<?php echo $this->element('../Top/_social-sp'); ?>

<?php echo $this->element('Item/concierge_link_banner_sp');?>

