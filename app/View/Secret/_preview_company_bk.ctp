<div class="common_secret-box">
	<div class="secret-title">企業メッセージ</div>
	<p>
		<?php echo str_replace("\n", "<br>",$CorporatePrs['corporate']); ?>
		<br>
		<br>
	</p>
	
	<?php if (
				!empty($CorporatePrs['president_avata'])||
				!empty($CorporatePrs['president_name'])||
				!empty($CorporatePrs['president_description'])
			 ): ?>
	<div class="secret-title">
		社長はこんな人
	</div>
	<?php endif; ?>
	<div class="person-present clearfix president-present">
		<?php if (!empty($CorporatePrs['president_avata'])): ?>
		<div class="cell-img">
			<img src="<?php echo $this->webroot."upload/secret/companies/".$CorporatePrs['president_avata']; ?>" alt="">
		</div>
		<?php endif; ?>

		<?php if (!empty($CorporatePrs['president_name'])): ?>
		<div class="cell-content">
			<strong class="cell-header">代表取締役社長：<?php echo str_replace("\n", "<br>",$CorporatePrs['president_name']); ?></strong>
			<?php if (!empty($CorporatePrs['president_description'])): ?>
			<p><?php echo str_replace("\n", "<br>",$CorporatePrs['president_description']); ?></p>
	<?php endif; ?>
		</div>
		<?php endif; ?>
	</div>

	<div class="secret-title-comment">
		コールナビ編集部コメント
	</div>
	<p>
		<?php echo str_replace("\n", "<br>",$CorporatePrs['comment']); ?>
		<br>
	</p>
</div>