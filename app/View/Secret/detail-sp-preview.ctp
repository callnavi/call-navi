<?php $this->layout = 'single_column_v2_sp'; ?>
<?php
$this->set('title', $this->App->SecretDetailTitle1($CareerOpportunity));

$this->start('css');
echo $this->Html->css('secret-sp');
    // style css add for preview error in button styling facebook
echo $this->Html->css('preview');
    
$this->end('css');

$this->start('script');
echo $this->Html->script('secret-sp.js');
echo $this->Html->script('init-googlemap');
echo '<script src="https://maps.google.com/maps/api/js?libraries=geometry&key='.Configure::read('webconfig.google_map_api_key').'"></script>';
$this->end('script');

?>

<?php echo $this->element('../Secret/detail-sp/_detail-banner-sp'); ?>
<?php //chenge to company_info_v2 for issues #337 09 september 2017
//echo $this->element('../Secret/detail-sp/_detail-head-info-sp'); ?>
<?php echo $this->element('../Secret/detail-sp/detail-sp2/company_info_v2'); ?>
<?php echo $this->element('../Secret/detail-sp/_detail-button-sp'); ?>
<?php echo $this->element('../Secret/detail-sp/detail-sp2/_detail-tab-sp'); ?>
<?php echo $this->element('../Secret/detail-sp/_detail-button-sp'); ?>
<?php echo $this->element('../Top/_social-sp'); ?>
<?php echo $this->element('../Secret/detail-sp/_detail-fixed-button-sp'); ?>

<?php $this->start('script'); ?>
<script type="text/javascript">
	$(document).ready(function() {
		
		<?php if(!empty($zoom) && $zoom): ?>
			$("html").css("zoom", "0.5");
		<?php endif; ?>


		var direction = '<a style="font-size:12px" href="https://maps.google.com/maps?ll=<?php echo $CorporatePrs['lat'];?>,<?php echo $CorporatePrs['lng'];?>&z=16&t=m&hl=ja-JP&gl=JP&mapclient=embed&daddr=<?php echo $CorporatePrs['map_location']; ?>" target="_blank"> ルート検索 ▶︎</a>';
		// set google map
		// input lat, lng , inforwindow, zoom
		<?php $latLng = !empty($CorporatePrs['lat']) && !empty($CorporatePrs['lng']) ? $CorporatePrs['lat'].' , '.$CorporatePrs['lng'] : '0,0'; ?>
		var contentString = "<h4 style='font-weight: bold;'><?php echo $CorporatePrs['company_name']; ?>";
		// var contentString = "<h4 style='font-weight: bold;'><?php echo $CorporatePrs['company_name']; ?> "+direction+"</h4>"+
		// 					"<p style='line-height:1;'><a href='<?php echo $CorporatePrs['website']; ?>' ><?php echo $CorporatePrs['website']; ?></a></p>"+
		// 					"<div style='height: 24px;'><?php echo $CorporatePrs['office_location']; ?></div>";

		var flagLoadedMap = false; 
		$(function() {
			if(flagLoadedMap == false)
			{
				initMap(<?php echo $latLng;?>, contentString, 13);
				flagLoadedMap = true;
			}
		});
	});
</script>
<?php $this->end('script'); ?>