<?php
    $this->set('title', 'コールナビ｜お問い合わせ');
    $this->start('css');
        echo $this->Html->css('jquery.steps-sp');
        echo $this->Html->css('pages-sp/contact-page-sp');
    $this->end('css');

    $this->start('script');
        echo $this->Html->script('jquery.cookie-1.3.1');
        echo $this->Html->script('jquery.steps.min');
        echo $this->Html->script('jquery.validate.min');
        echo $this->Html->script('ajax-send-contact-mail');
    $this->end('script');

$this->start('meta');
    //meta description keywords image
    echo Configure::read('webconfig.meta_description');
    echo Configure::read('webconfig.meta_keywords');
    echo Configure::read('webconfig.apple_touch_icon_precomposed');
    echo Configure::read('webconfig.ogp');  
$this->end('meta');

?>

<div  class=" no-padding container">

    <div id="contact-main">
        <div class="header-title">
            <h4>お問い合わせ</h4>
        </div>

        <div class="contact-subtitle">
            <p>
                担当者から料金やサービスのご案内を差し上げます。<br>
                通常2営業日以内に対応致しますが、内容によっては返信にお時間をいただく場合がございます。
            </p>
        </div>

        <div class="custom-radio">
            <label>
                <input type="radio" value="0" name="choice" id="select-form-contact1" checked="true"> 
                <div class="vir-label">
                    <span class="vir-radio"></span>
                    <span class="vir-content" id="color-disable1">サイトに関する問い合わせ</span>
                </div>
            </label>
        </div>
        <div class="custom-radio" >
            <label>
                <input type="radio" value="1" name="choice" id="select-form-contact2">
                <div class="vir-label">
                    <span class="vir-radio"></span>
                    <span class="vir-content" id="color-disable2">求人掲載に関するお問い合わせ</span>
                </div>
            </label>
        </div>

        <div class="sp-div-contact-form">
            <!-- Form 1 -->
            <form id="frm-contact-form1" action="#" method="post" accept-charset="utf-8" enctype="multipart/form-data" novalidate="novalidate">
                <div id="contact-form">
                    <h2>STEP1</h2>
                    <section class="step1">
                        <div class="form-group" id="step1FullName">
                            <div id="st1-fullname">
                                <label for="step1FullName" class="contact-lablel control-label">
                                    氏名<span class="require">必須</span>
                                </label>

                                <div class="contact-input">
                                    <input type="text" name="step1FullName" id="step1-full-name" value=""  class="form-control" aria-required="true" aria-invalid="false" placeholder="お名前を入力">
                                </div>
                            </div>
                        </div>

                        <div class="form-group" id="step1Email">
                            <div id="st1-email">
                                <label for="step1Email" class="contact-lablel control-label">
                                    メールアドレス<span class="require">必須</span>
                                </label>

                                <div class="contact-input">
                                    <input type="email" name="step1Email" id="step1-email" value=""   class="form-control" aria-required="true" aria-invalid="false" placeholder="メールアドレス">
                                    <p>
                                        ※こちらのメールアドレスにご返信させていただきます。<br>
                                        お手数ですがメールが受信可能かご確認ください。
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="form-group" id="step1Content">
                            <div id="st1-content">
                                <label for="step1Content" class="contact-lablel control-label">
                                    お問い合わせ内容<span class="require">必須</span>
                                </label>

                                <div class="contact-input">
                                    <textarea name="step1Content" id="step1-content" value=""  class="form-control" aria-required="true" aria-invalid="false" placeholder="お問い合わせ内容"></textarea>
                                </div>
                            </div>
                        </div>
                    </section>

                    <h2>STEP2</h2>
                    <section class="step2">
                        <div class="form-group" id="step2FullName">
                            <div id="st2-fullname">
                                <label for="step2FullName" class="contact-lablel control-label">
                                    氏名
                                </label>

                                <div class="contact-input">
                                    <input type="text" name="step2FullName" disabled id="step2-full-name"  class="form-control" aria-required="true" aria-invalid="false" placeholder="お名前を入力">

                                </div>
                            </div>
                        </div>

                        <div class="form-group" id="step2Email">
                            <div id="st2-email">
                                <label for="step2Email" class="contact-lablel control-label">
                                    メールアドレス
                                </label>

                                <div class="contact-input">
                                    <input type="email" name="step2Email" disabled id="step2-email"  class="form-control" aria-required="true" aria-invalid="false" placeholder="メールアドレス">
                                    <p>
                                        ※こちらのメールアドレスにご返信させていただきます。<br>
                                        お手数ですがメールが受信可能かご確認ください。
                                    </p>
                                </div>
                            </div>
                        </div>

                        <div class="form-group" id="step2Content">
                            <div id="st2-content">
                                <label for="step2Content" class="contact-lablel control-label">
                                    お問い合わせ内容
                                </label>

                                <div class="contact-input">
                                    <textarea name="step2Content" disabled id="step2-content" class="form-control" aria-required="true" aria-invalid="false" placeholder="お問い合わせ内容">お問い合わせ内容を入力</textarea>

                                </div>
                            </div>
                        </div>
                    </section>

                    <h2>完了</h2>
                    <section class="finish">
                        <p>
                            この度はお問い合せ頂き誠にありがとうございました。<br>
                            改めて担当者よりご連絡をさせていただきます。
                        </p>
                    </section>
                </div>

            </form>


            <!-- Form 2 -->
            <form id="frm-contact-form2" action="#" method="post" accept-charset="utf-8" enctype="multipart/form-data" novalidate="novalidate">
                <div id="contact-form2">
                    <h2>STEP1</h2>
                    <section class="step1">

                        <div class="form-group" id="content-step1Content">
                        </div>

                        <div class="form-group">
                            <label for="step1CompanyName" class="contact-lablel control-label">
                                会社名<span class="require">必須</span>
                            </label>

                            <div class="contact-input">
                                <input type="text" name="step1CompanyName" id="step1-company-name" value=""  class="form-control" aria-required="true" aria-invalid="false" placeholder="会社名を入力">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="step1DepartmentNamePosition" class="contact-lablel control-label">
                                部署名・役職<span class="require">必須</span>
                            </label>

                            <div class="contact-input">
                                <input type="text" name="step1DepartmentNamePosition" id="step1-department-name-position" value=""  class="form-control" aria-required="true" aria-invalid="false" placeholder="部署名・役職を入力">
                            </div>
                        </div>

                        <div class="form-group" id="content-step1FullName">
                        </div>

                        <div class="form-group" id="content-step1Email">
                        </div>

                        <div class="form-group">
                            <label for="step1PhoneNumber" class="contact-lablel control-label">
                                電話番号<span class="require">必須</span>
                            </label>

                            <div class="contact-input">
                                <input type="text" name="step1PhoneNumber" id="step1-phone-number" value=""  class="form-control" aria-required="true" aria-invalid="false" placeholder="電話番号を入力">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="step1HowToAnswer" class="contact-lablel control-label">
                                お問い合わせ解答方法
                            </label>

                            <div class="contact-input">
                                <select name="step1HowToAnswer" id="step1-how-to-answer" class="form-control">
                                    <option value="メールでの解答を希望する。">メールでの解答を希望する。</option>
                                    <option value="電話での回答を希望する。">電話での回答を希望する。</option>
                                </select>
                            </div>
                        </div>

                        <div class="custom-checkbox-privacy" >
                            <label>
                                <input type="checkbox" id="step1Agree" name="step1Agree" value="agree">
                                <div class="vir-label">
                                    <span class="vir-checkbox"></span>
                                    <span class="vir-content">利用規約・プライバシポリシーに同意する</span>
                                </div>
                            </label>
                        </div>
                        
                    </section>

                    <h2>STEP2</h2>
                    <section class="step2">

                        <div class="form-group" id="content-step2Content">
                        </div>

                        <div class="form-group">
                            <label for="step2CompanyName" class="contact-lablel control-label">
                                会社名
                            </label>

                            <div class="contact-input">
                                <input type="text" name="step2CompanyName" disabled id="step2-company-name" class="form-control" aria-required="true" aria-invalid="false" placeholder="会社名を入力">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="step2DepartmentNamePosition" class="contact-lablel control-label">
                                部署名・役職
                            </label>

                            <div class="contact-input">
                                <input type="text" name="step2DepartmentNamePosition" disabled id="step2-department-name-position" class="form-control" aria-required="true" aria-invalid="false" placeholder="部署名・役職を入力">
                            </div>
                        </div>

                        <div class="form-group" id="content-step2FullName">
                        </div>

                        <div class="form-group" id="content-step2Email">
                        </div>

                        <div class="form-group">
                            <label for="step2PhoneNumber" class="contact-lablel control-label">
                                電話番号
                            </label>

                            <div class="contact-input">
                                <input type="text" name="step2PhoneNumber" disabled id="step2-phone-number" class="form-control" aria-required="true" aria-invalid="false" placeholder="電話番号を入力">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="step2HowToAnswer" class="contact-lablel control-label">
                                お問い合わせ解答方法
                            </label>

                            <div class="contact-input">
                                <select name="step2HowToAnswer" id="step2-how-to-answer" disabled class="form-control">
                                    <option value="メールでの解答を希望する。">メールでの解答を希望する。</option>
                                    <option value="電話での回答を希望する。">電話での回答を希望する。</option>
                                </select>
                            </div>
                        </div>

                        <div class="custom-checkbox-privacy" >
                            <label>
                                <input type="checkbox" name="step2Agree" id="step2Agree" value="agree" disabled>
                                <div class="vir-label">
                                    <span class="vir-checkbox"></span>
                                    <span class="vir-content">利用規約・プライバシポリシーに同意する</span>
                                </div>
                            </label>
                        </div>
                        
                    </section>

                    <h2>完了</h2>
                    <section class="finish">
                        <p>
                            この度はお問い合せ頂き誠にありがとうございました。<br>
                            改めて担当者よりご連絡をさせていただきます。
                        </p>
                    </section>
                </div>

            </form>
        </div>


    </div>

</div>
<script>
    $(function ()
    {
        $("#frm-contact-form2").hide();

        $('#select-form-contact1').change(function() {
            if($("#select-form-contact1").is(':checked'))
            {
                $("#frm-contact-form2").hide();
                $("#frm-contact-form1").show();

                $("#frm-contact-form2").validate().resetForm();

                $("#step1FullName").append($("#st1-fullname"));
                $("#step1Email").append($("#st1-email"));
                $("#step1Content").append($("#st1-content"));

                $("#step2FullName").append($("#st2-fullname"));
                $("#step2Email").append($("#st2-email"));
                $("#step2Content").append($("#st2-content"));
            }
        });

        $('#select-form-contact2').change(function() {
            if($("#select-form-contact2").is(':checked'))
            {
                $("#frm-contact-form1").hide();
                $("#frm-contact-form2").show();

                $("#frm-contact-form1").validate().resetForm();

                $("#content-step1FullName").append($("#st1-fullname"));
                $("#content-step1Email").append($("#st1-email"));
                $("#content-step1Content").append($("#st1-content"));

                $("#content-step2FullName").append($("#st2-fullname"));
                $("#content-step2Email").append($("#st2-email"));
                $("#content-step2Content").append($("#st2-content"));
            }
        });

//        Check validate form

        var form1 = $("#frm-contact-form1");
        form1.validate({
            errorPlacement: function errorPlacement(error, element) { element.before(error); },
            onkeyup: false,
            rules: {
                step1FullName: {
                    required:true,
                },
                step1Email: {
                    required: true,
                    email: true
                },
                step1Content:  {
                    required:true,
                },

            },

        });

//        contact form step by step
        $("#contact-form").steps({
            headerTag: "h2",
            bodyTag: "section",
            transitionEffect: "fade",
            cssClass: "tabcontrol",
            onStepChanging: function (event, currentIndex, newIndex)
            {

                if(newIndex ==1){
                    form1.validate().settings.ignore = ":disabled,:hidden";
                    if(form1.valid()) {
                        //console.log(form.valid());
                        $("#contact-form .actions  ul li a[href=#next]").html("上記内容で送信");
                        $("#contact-main .sp-div-contact-form .step2 .form-group #step2-full-name").attr("value", $("#contact-main .sp-div-contact-form .step1 .form-group #step1-full-name").val());
                        $("#contact-main .sp-div-contact-form .step2 .form-group #step2-email").attr("value", $("#contact-main .sp-div-contact-form .step1 .form-group #step1-email").val());
                        $("#contact-main .sp-div-contact-form .step2 .form-group #step2-content").html($("#contact-main .sp-div-contact-form .step1 .form-group #step1-content").val());

                        disableRadioButton();
                    }else{
                        $("#contact-form .content").css("height","460px");
                        return form1.valid();
                    }
                }
                
//                go to step 3
                if(newIndex ==2){
                    var arr =  [];
                    arr[0]= $("#contact-main .sp-div-contact-form .step1 .form-group #step1-full-name").val();
                    arr[1]= $("#contact-main .sp-div-contact-form .step1 .form-group #step1-email").val();
                    arr[2]= $("#contact-main .sp-div-contact-form .step1 .form-group #step1-content").val();

                    sendContactMail("<?php echo $this->Html->url(array('controller' => 'contact', 'action' => 'SendContactMail'), true);?>",arr);
                    $("#contact-form .actions  ul li a[href=#finish]").html("トップページへ戻る");

                    disableButtonStep();
                }
                return true;
            },
            onFinishing: function (event, currentIndex)
            {
                return true;
            },
            onFinished: function (event, currentIndex)
            {
                window.top.location.href = "http://"+document.location.hostname;
            }
        });

        $("#contact-form .actions  ul li a[href=#next]").html("確認画面へ進む");
        $("#contact-form .actions  ul li a[href=#previous]").parent().hide();

        jQuery.validator.addMethod("phone_valid", function(phone_number, element) {
            phone_number = phone_number.replace(/\s+/g, "");
            return this.optional(element) || phone_number.length > 9 && 
                // phone_number.match(/([0-9]{3})([-])?([0-9]{4})([-])?([0-9]{4})/);
                phone_number.match(/^[0-9]{3}[-\s]{0,1}[0-9]{4}[-\s]{0,1}[0-9]{4}$/);
        }, "携帯電話番号は半角数字11桁で入力してください。");

        //Check validate form 2
        var form2 = $("#frm-contact-form2");
        form2.validate({
            errorPlacement: function errorPlacement(error, element) { element.before(error); },
            onkeyup: false,
            rules: {
                step1FullName: {
                    required:true,
                },
                step1Email: {
                    required: true,
                    email: true
                },
                step1Content:  {
                    required:true,
                },
                step1CompanyName: {
                    required:true,
                },
                step1DepartmentNamePosition: {
                    required:true,
                },
                step1PhoneNumber: {
                    required:true,
                    phone_valid: true
                },
                step1Agree: {
                    required:true
                }
            },

        });

        jQuery.extend(jQuery.validator.messages, {
            required: "この項目は必須です。",
            email: "メールアドレスが不正です。",
        });
        //contact form step by step
        $("#contact-form2").steps({
            headerTag: "h2",
            bodyTag: "section",
            transitionEffect: "fade",
            cssClass: "tabcontrol tab2",
            onStepChanging: function (event, currentIndex, newIndex)
            {
                //go to step 2
                if(newIndex ==1){
                    form2.validate().settings.ignore = ":disabled,:hidden";
                    if(form2.valid()) {
                        //console.log(form.valid());
                        $("#contact-form .actions  ul li a[href=#next]").html("上記内容で送信");
                        $("#contact-main .sp-div-contact-form .step2 .form-group #step2-full-name").attr("value", $("#contact-main .sp-div-contact-form .step1 .form-group #step1-full-name").val());
                        $("#contact-main .sp-div-contact-form .step2 .form-group #step2-email").attr("value", $("#contact-main .sp-div-contact-form .step1 .form-group #step1-email").val());
                        $("#contact-main .sp-div-contact-form .step2 .form-group #step2-content").html($("#contact-main .sp-div-contact-form .step1 .form-group #step1-content").val());
                        $("#contact-main .sp-div-contact-form .step2 .form-group #step2-company-name").attr("value", $("#contact-main .sp-div-contact-form .step1 .form-group #step1-company-name").val());
                        $("#contact-main .sp-div-contact-form .step2 .form-group #step2-department-name-position").attr("value", $("#contact-main .sp-div-contact-form .step1 .form-group #step1-department-name-position").val());
                        $("#contact-main .sp-div-contact-form .step2 .form-group #step2-phone-number").attr("value", $("#contact-main .sp-div-contact-form .step1 .form-group #step1-phone-number").val());
                        $("#contact-main .sp-div-contact-form .step2 .form-group #step2-how-to-answer").val($("#contact-main .sp-div-contact-form .step1 .form-group #step1-how-to-answer").val());

                        $("#step2Agree").prop('checked', true);

                        disableRadioButton();

                    }else{
                        $("#contact-form2 .content").css('cssText', 'height: 860px !important');
                        return form2.valid();
                    }
                }
                //go to step 3
                if(newIndex ==2){
                    var arr = [];
                    arr[0]= $("#contact-main .sp-div-contact-form .step1 .form-group #step1-full-name").val();
                    arr[1]= $("#contact-main .sp-div-contact-form .step1 .form-group #step1-email").val();
                    arr[2]= $("#contact-main .sp-div-contact-form .step1 .form-group #step1-content").val();
                    arr[3]= $("#contact-main .sp-div-contact-form .step1 .form-group #step1-company-name").val();
                    arr[4]= $("#contact-main .sp-div-contact-form .step1 .form-group #step1-department-name-position").val();
                    arr[5]= $("#contact-main .sp-div-contact-form .step1 .form-group #step1-phone-number").val();
                    arr[6]= $("#contact-main .sp-div-contact-form .step1 .form-group #step1-how-to-answer").val();
                    sendContactMail("<?php echo $this->Html->url(array('controller' => 'contact', 'action' => 'SendContactMail'), true);?>",arr);
                    $("#contact-form2 .actions  ul li a[href=#finish]").html("トップページへ戻る");

                    disableButtonStep();
                    $("#contact-form2 .content").css('cssText', 'height: 350px !important');
                }
                return true;
            },
            onFinishing: function (event, currentIndex)
            {
                return true;
            },
            onFinished: function (event, currentIndex)
            {
                //finish
                window.top.location.href = "http://"+document.location.hostname;
            }
        });

        $("#contact-form2 .actions  ul li a[href=#next]").html("確認画面へ進む");
        $("#contact-form2 .actions  ul li a[href=#previous]").parent().hide();

        function disableRadioButton()
        {
            if($("#select-form-contact1").is(':checked'))
            {
                $("#select-form-contact2").attr('disabled',true);
                $("#color-disable2").addClass("color-disable");
            }
            else
            {
                $("#select-form-contact1").attr('disabled',true);
                $("#color-disable1").addClass("color-disable");
            }
        }

        function disableButtonStep()
        {
            $("li.first.done").addClass('disabled');
            $("li.current").addClass('disabled');
        }

    });
</script>


