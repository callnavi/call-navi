<form id="frm-contact-form1"
      action="<?php echo $this->Html->url(array('controller' => 'contact', 'action' => 'SendContactMail'), true); ?>"
      method="post" accept-charset="utf-8" enctype="multipart/form-data" novalidate="novalidate">
    <div id="contact-form">
        <h2>STEP1</h2>
        <section class="step1">
            <div class="form-group">
                <div>
                    <label for="step1FullName" class="contact-lablel control-label">
                        種別<span class="require border-blue">必須</span>
                    </label>

                    <div class="contact-input div_gender">
                        <div class="custom-checkbox">
                            <label>
                                <input type="radio" checked="checked">

                                <div class="vir-label circle-style">
                                    <span class="vir-content">個人</span>
                                    <span class="vir-checkbox"></span>
                                </div>
                            </label>
                        </div>

                        <div class="custom-checkbox" id="open-form-contact2">
                            <label>
                                <input type="radio" disabled>
                                <div class="vir-label circle-style">
                                    <span class="vir-content">法人</span>
                                    <span class="vir-checkbox"></span>
                                </div>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group" id="step1FullName">
                <div id="st1-fullname">
                    <label for="step1FullName" class="contact-lablel control-label">
                        氏名<span class="require border-blue">必須</span>
                    </label>

                    <div class="contact-input">
                        <input type="text" name="step1FullName" id="step1-full-name" value=""
                               class="form-control" aria-required="true" aria-invalid="false"
                               placeholder="お名前を入力">
                    </div>
                </div>
            </div>


            <div class="form-group" id="step1Email">
                <div id="st1-email">
                    <label for="step1Email" class="contact-lablel control-label">
                        E-mail<span class="require border-blue">必須</span>
                    </label>

                    <div class="contact-input">
                        <input type="email" name="step1Email" id="step1-email" value="" class="form-control"
                               aria-required="true" aria-invalid="false" placeholder="メールアドレスを入力">
                    </div>
                </div>
            </div>
            <div class="form-group" id="step1Content">
                <div id="st1-content">
                    <label for="step1Content" class="contact-lablel control-label contents-label">
                        お問い合わせ内容
                    </label>

                    <div class="contact-input">
                                    <textarea name="step1Content" id="step1-content" value="" class="form-control"
                                              aria-required="true" aria-invalid="false"
                                              placeholder="お問い合わせ内容を入力"></textarea>
                    </div>
                </div>
            </div>
            <section class="contact-custom-text">
                <p class="text-center">【お問い合わせフォームに関する注意事項】</p>
                <p>「@callnavi.jp」からのメール受信が可能な設定にしていただきますようお願いいたします。<br>15分以上以内に返信メールが届かない場合は、迷惑メールフォルダをご確認ください。</p>
            </section>
        </section>

        <h2>STEP2</h2>
        <section class="step2">
            <div class="form-group">
                <div>
                    <label for="step1FullName" class="contact-lablel control-label">
                        種別<span class="require border-blue">必須</span>
                    </label>

                    <div class="contact-input div_gender">
                        <div class="custom-checkbox">
                            <label>
                                <input type="radio" checked="checked">

                                <div class="vir-label circle-style">
                                    <span class="vir-content">個人</span>
                                    <span class="vir-checkbox"></span>
                                </div>
                            </label>
                        </div>

                        <div class="custom-checkbox" id="open-form-contact2">
                            <label>
                                <input type="radio" disabled>
                                <div class="vir-label circle-style">
                                    <span class="vir-content">法人</span>
                                    <span class="vir-checkbox"></span>
                                </div>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group" id="step2FullName">
                <div id="st2-fullname">
                    <label for="step2FullName" class="contact-lablel control-label">
                        氏名<span class="require border-blue">必須</span>
                    </label>

                    <div class="contact-input">
                        <input type="text" name="step2FullName" disabled id="step2-full-name"
                               class="form-control" aria-required="true" aria-invalid="false"
                               placeholder="お名前を入力">

                    </div>
                </div>
            </div>

            <div class="form-group" id="step2Email">
                <div id="st2-email">
                    <label for="step2Email" class="contact-lablel control-label">
                        E-mail<span class="require border-blue">必須</span>
                    </label>

                    <div class="contact-input">
                        <input type="email" name="step2Email" disabled id="step2-email" class="form-control"
                               aria-required="true" aria-invalid="false" placeholder="メールアドレス">
                    </div>
                </div>
            </div>

            <div class="form-group" id="step2Content">
                <div id="st2-content">
                    <label for="step2Content" class="contact-lablel control-label contents-label">
                        お問い合わせ内容
                    </label>

                    <div class="contact-input">
                                    <textarea name="step2Content" disabled id="step2-content" class="form-control"
                                              aria-required="true" aria-invalid="false"
                                              placeholder="質問等ございましたらご入力ください。">質問等ございましたらご入力ください。</textarea>
                    </div>
                </div>
            </div>
            <section class="contact-custom-text">
                <p class="text-center">【お問い合わせフォームに関する注意事項】</p>
                <p>「@callnavi.jp」からのメール受信が可能な設定にしていただきますようお願いいたします。<br>15分以上以内に返信メールが届かない場合は、迷惑メールフォルダをご確認ください。</p>
            </section>
        </section>

        <h2>完了</h2>
        <section class="finish">
            <p>
                この度はお問い合せ頂き誠にありがとうございました。
                <br>
                改めて担当者よりご連絡をさせていただきます。
            </p>
            <div class="contact-custom-text">
                <p class="text-center">【お問い合わせフォームに関する注意事項】</p>
                <p>「@callnavi.jp」からのメール受信が可能な設定にしていただきますようお願いいたします。<br>15分以上以内に返信メールが届かない場合は、迷惑メールフォルダをご確認ください。</p>
            </div>
            <div class="finish-link">
                <a href="/">ページトップに戻る</a>
            </div>
        </section>
    </div>

    <input type="hidden" name="job_id" id="job_id" value=""/>
</form>