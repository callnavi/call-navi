<?php
$this->set('title', $this->app->getWebTitle('contact'));
$breadcrumb = array('page' => 'お問い合わせ');

$this->start('css');
	echo $this->Html->css('contact-page');
	echo $this->Html->css('jquery.steps.css');
$this->end('css');

$this->start('script');
	echo $this->Html->script('jquery.cookie-1.3.1');
	echo $this->Html->script('jquery.steps.min');
	echo $this->Html->script('jquery.validate.min');
	echo $this->Html->script('ajax-send-contact-mail');
$this->end('script');

$this->start('meta');
    //meta description keywords image
    echo Configure::read('webconfig.meta_description');
    echo Configure::read('webconfig.meta_keywords');
    echo Configure::read('webconfig.apple_touch_icon_precomposed');
    echo Configure::read('webconfig.ogp');  
$this->end('meta');

$this->start('sub_footer');
	echo $this->element('Item/bottom_banner');
$this->end('sub_footer');

$this->start('header_breadcrumb');
	echo $this->element('Item/breadcrumb', $breadcrumb);
$this->end('header_breadcrumb');
?>

<?php $this->start('sub_header'); ?>
<?php //header change in #345 echo $this->element('../Top/2017/_top_banner');?>
<?php $this->end('sub_header'); ?> 

<div class="single-form-layout mg-top-40 mg-bottom-50 no-padding container ipad-padding padding-t-170">
	<div class="content">
		<div class="no-padding container-contact">
			<div id="contact-main">
				<div class="list-header non-type">
					<h1>お問い合わせ</h1>
				</div>
				
				<div class="contact-subtitle">
					<p>
						担当者から料金やサービスのご案内を差し上げます。<br>
						通常2営業日以内に対応致しますが、内容によっては返信にお時間をいただく場合がございます。
					</p>
				</div>
				
				<div class="div-contact-form">
					<?php echo $this->element('../Contact/index_form_1'); ?>
					<?php echo $this->element('../Contact/index_form_2'); ?>
				</div>

			</div>

		</div>
		
	</div>
</div>






<script>
    $( document ).ready(function() {
        $("#frm-contact-form2").hide();
		
		
		
//        $('#select-form-contact1').change(function() {
//            if($("#select-form-contact1").is(':checked'))
//            {
//                $("#frm-contact-form2").hide();
//                $("#frm-contact-form1").show();
//                
//                $("#frm-contact-form2").validate().resetForm();
//
//                $("#step1FullName").append($("#st1-fullname"));
//                $("#step1Email").append($("#st1-email"));
//                $("#step1Content").append($("#st1-content"));
//
//                $("#step2FullName").append($("#st2-fullname"));
//                $("#step2Email").append($("#st2-email"));
//                $("#step2Content").append($("#st2-content"));
//            }
//        });

//        $('#select-form-contact2').change(function() {
//            if($("#select-form-contact2").is(':checked'))
//            {
//                $("#frm-contact-form1").hide();
//                $("#frm-contact-form2").show();
//
//                $("#frm-contact-form1").validate().resetForm();
//
//                $("#content-step1FullName").append($("#st1-fullname"));
//                $("#content-step1Email").append($("#st1-email"));
//                $("#content-step1Content").append($("#st1-content"));
//
//                $("#content-step2FullName").append($("#st2-fullname"));
//                $("#content-step2Email").append($("#st2-email"));
//                $("#content-step2Content").append($("#st2-content"));
//            }
//        });

        

        //Check validate form 1
        var form1 = $("#frm-contact-form1");
        form1.validate({
            errorPlacement: function errorPlacement(error, element) { element.before(error); },
            onkeyup: false,
            rules: {
                step1FullName: {
                    required:true,
                },
                step1Email: {
                    required: true,
                    email: true
                },
                step1Content:  {
                    required:true,
                },

            },

        });

        //contact form step by step
        $("#contact-form").steps({
            headerTag: "h2",
            bodyTag: "section",
            transitionEffect: "fade",
            cssClass: "tabcontrol",
            onStepChanging: function (event, currentIndex, newIndex)
            {
                //go to step 2
                if(newIndex ==1){
                    form1.validate().settings.ignore = ":disabled,:hidden";
                    if(form1.valid()) {
                        //console.log(form.valid());
                        $("#contact-form .actions  ul li a[href=#next]").html("上記内容で送信");
                        $("#contact-main .div-contact-form .step2 .form-group #step2-full-name").attr("value", $("#contact-main .div-contact-form .step1 .form-group #step1-full-name").val());
                        $("#contact-main .div-contact-form .step2 .form-group #step2-email").attr("value", $("#contact-main .div-contact-form .step1 .form-group #step1-email").val());
                        $("#contact-main .div-contact-form .step2 .form-group #step2-content").html($("#contact-main .div-contact-form .step1 .form-group #step1-content").val());

                        disableRadioButton();
                    }else{
                        return form1.valid();
                    }
                }
                //go to step 3
                if(newIndex ==2){
                    var arr =  [];
                    arr[0]= $("#contact-main .div-contact-form .step1 .form-group #step1-full-name").val();
                    arr[1]= $("#contact-main .div-contact-form .step1 .form-group #step1-email").val();
                    arr[2]= $("#contact-main .div-contact-form .step1 .form-group #step1-content").val();

                    sendContactMail("<?php echo $this->Html->url(array('controller' => 'contact', 'action' => 'SendContactMail'), true);?>",arr);
                    $("#contact-form .actions  ul li a[href=#finish]").addClass("blue-style").html("ページトップに戻る");

                    disableButtonStep();
                }

                return true;
            },
            onFinishing: function (event, currentIndex)
            {
                return true;
            },
            onFinished: function (event, currentIndex)
            {
                //finish
                window.top.location.href = "http://"+document.location.hostname;
            }
        });

        $("#contact-form .actions  ul li a[href=#next]").html("確認画面へ進む");
        $("#contact-form .actions  ul li a[href=#previous]").parent().hide();


        jQuery.validator.addMethod("phone_valid", function(phone_number, element) {
            phone_number = phone_number.replace(/\s+/g, "");
            return this.optional(element) || phone_number.length > 9 && 
                // phone_number.match(/([0-9]{3})([-])?([0-9]{4})([-])?([0-9]{4})/);
                phone_number.match(/^[0-9]{3}[-\s]{0,1}[0-9]{4}[-\s]{0,1}[0-9]{3,4}$/);
        }, "携帯電話番号は半角数字11桁で入力してください。");

        //Check validate form 2
        var form2 = $("#frm-contact-form2");
        form2.validate({
            errorPlacement: function errorPlacement(error, element) { element.before(error); },
            onkeyup: false,
            rules: {
                step1FullName: {
                    required:true,
                },
                step1Email: {
                    required: true,
                    email: true
                },
                step1Content:  {
                    required:true,
                },
                step1CompanyName: {
                    required:true,
                },
                step1DepartmentNamePosition: {
                    required:true,
                },
                step1PhoneNumber: {
                    required:true,
                    phone_valid: true
                },
                step1Agree: {
                    required:true
                }
            },

        });

        jQuery.extend(jQuery.validator.messages, {
            required: "この項目は必須です。",
            email: "メールアドレスが不正です。",
        });
        //contact form step by step
        $("#contact-form2").steps({
            headerTag: "h2",
            bodyTag: "section",
            transitionEffect: "fade",
            cssClass: "tabcontrol tab2",
            onStepChanging: function (event, currentIndex, newIndex)
            {
                //go to step 2
                if(newIndex ==1){
                    form2.validate().settings.ignore = ":disabled,:hidden";
                    if(form2.valid()) {
                        //console.log(form.valid());
                        $("#contact-form .actions  ul li a[href=#next]").html("上記内容で送信");
                        $("#contact-main .div-contact-form .step2 .form-group #step2-full-name").attr("value", $("#contact-main .div-contact-form .step1 .form-group #step1-full-name").val());
                        $("#contact-main .div-contact-form .step2 .form-group #step2-email").attr("value", $("#contact-main .div-contact-form .step1 .form-group #step1-email").val());
                        $("#contact-main .div-contact-form .step2 .form-group #step2-content").html($("#contact-main .div-contact-form .step1 .form-group #step1-content").val());
                        $("#contact-main .div-contact-form .step2 .form-group #step2-company-name").attr("value", $("#contact-main .div-contact-form .step1 .form-group #step1-company-name").val());
                        $("#contact-main .div-contact-form .step2 .form-group #step2-department-name-position").attr("value", $("#contact-main .div-contact-form .step1 .form-group #step1-department-name-position").val());
                        $("#contact-main .div-contact-form .step2 .form-group #step2-phone-number").attr("value", $("#contact-main .div-contact-form .step1 .form-group #step1-phone-number").val());
                        $("#contact-main .div-contact-form .step2 .form-group #step2-how-to-answer").val($("#contact-main .div-contact-form .step1 .form-group #step1-how-to-answer").val());

                        $("#step2Agree").prop('checked', true);

                        disableRadioButton();
                    }else{
                        //$("#contact-form2 .content").css('cssText', 'height: 740px !important');
                        return form2.valid();

                    }
                }
                //go to step 3
                if(newIndex ==2){
                    var arr =  [];
                    arr[0]= $("#contact-main .div-contact-form .step1 .form-group #step1-full-name").val();
                    arr[1]= $("#contact-main .div-contact-form .step1 .form-group #step1-email").val();
                    arr[2]= $("#contact-main .div-contact-form .step1 .form-group #step1-content").val();
                    arr[3]= $("#contact-main .div-contact-form .step1 .form-group #step1-company-name").val();
                    arr[4]= $("#contact-main .div-contact-form .step1 .form-group #step1-department-name-position").val();
                    arr[5]= $("#contact-main .div-contact-form .step1 .form-group #step1-phone-number").val();
                    arr[6]= $("#contact-main .div-contact-form .step1 .form-group #step1-how-to-answer").val();
                    sendContactMail("<?php echo $this->Html->url(array('controller' => 'contact', 'action' => 'SendContactMail'), true);?>",arr);
                    $("#contact-form2 .actions  ul li a[href=#finish]").addClass("blue-style").html("ページトップに戻る");

                    disableButtonStep();
                    $("#contact-form2 .content").css('cssText', 'height: 300px !important');
                }
                return true;
            },
            onFinishing: function (event, currentIndex)
            {
                return true;
            },
            onFinished: function (event, currentIndex)
            {
                //finish
                window.top.location.href = "http://"+document.location.hostname;
            }
        });

        $("#contact-form2 .actions  ul li a[href=#next]").html("確認画面へ進む");
        $("#contact-form2 .actions  ul li a[href=#previous]").parent().hide();
		
		$('#open-form-contact2').click(function () {
			
			$("#frm-contact-form1").hide();
			$("#frm-contact-form2").show();

			$("#frm-contact-form1").validate().resetForm();

			$("#content-step1FullName").append($("#st1-fullname"));
			$("#content-step1Email").append($("#st1-email"));
			$("#content-step1Content").append($("#st1-content"));

			$("#content-step2FullName").append($("#st2-fullname"));
			$("#content-step2Email").append($("#st2-email"));
			$("#content-step2Content").append($("#st2-content"));
		
		});
		
		$('#open-form-contact1').click(function () {
			
			$("#frm-contact-form2").hide();
			$("#frm-contact-form1").show();

			$("#frm-contact-form2").validate().resetForm();

			$("#step1FullName").append($("#st1-fullname"));
			$("#step1Email").append($("#st1-email"));
			$("#step1Content").append($("#st1-content"));

			$("#step2FullName").append($("#st2-fullname"));
			$("#step2Email").append($("#st2-email"));
			$("#step2Content").append($("#st2-content"));
		});
		
        function disableRadioButton()
        {
            if($("#select-form-contact1").is(':checked'))
            {
                $("#select-form-contact2").attr('disabled',true);
                $("#color-disable2").addClass("color-disable");
            }
            else
            {
                $("#select-form-contact1").attr('disabled',true);
                $("#color-disable1").addClass("color-disable");
            }
        }

        function disableButtonStep()
        {
            $("li.first.done").addClass('disabled');
            $("li.current").addClass('disabled');
        }
    });
</script>


