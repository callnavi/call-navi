<?php
$this->layout = 'single_column_v2_sp';

$this->set('title', $this->app->getWebTitle('contact'));

$this->start('css');
    echo $this->Html->css('jquery.steps-sp');
    echo $this->Html->css('pages-sp/contact-page-sp');
    echo $this->Html->css('contact-v2-sp');
$this->end('css');

$this->start('script');
    echo $this->Html->script('jquery.cookie-1.3.1');
    echo $this->Html->script('jquery.steps.min');
    echo $this->Html->script('jquery.validate.min');
    echo $this->Html->script('contact.js?v=1.0');
    echo $this->Html->script('ajax-send-contact-mail');
$this->end('script');

$this->start('meta');
//meta description keywords image
    echo Configure::read('webconfig.meta_description');
    echo Configure::read('webconfig.meta_keywords');
    echo Configure::read('webconfig.apple_touch_icon_precomposed');
    echo Configure::read('webconfig.ogp');
$this->end('meta');

?>

<div class="banner">
    <div class="background clip-bottom-arrow">
        <img src="/img/banner/top_contact_sp_banner.jpg">
    </div>
    <div class="middle-content banner-title">
        <div class="banner-title-wp">
            <h1>お問い合わせ</h1>
            <h2>担当者から料金やサービスのご案内を差し上げます。</h2>
            <h2>通常2営業日以内に対応致しますが、</h2>
            <h2>内容によっては返信にお時間をいただく場合がございます。</h2>
        </div>
    </div>
</div>

<div id="contact-main">
    <div class="header-title df-padding mg-top-60">
    </div>
    <div class="div-contact-form mg-top-20">
        <?php
        echo $this->element('../Contact/index_form_1_sp');
        echo $this->element('../Contact/index_form_2_sp');
        ?>
    </div>
</div>


<?php echo $this->element('Item/script_google_yahoo'); ?>




