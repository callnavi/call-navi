<form id="frm-contact-form2" action="<?php echo $this->Html->url(array('controller' => 'contact', 'action' => 'SendContactMail'), true);?>" method="post" accept-charset="utf-8" enctype="multipart/form-data" novalidate="novalidate">
	<div id="contact-form2">
		<h2>STEP1</h2>
		<section class="step1">			
			<div class="form-group">
				<div class="wp-st">
					<label for="step1Email" class="contact-lablel control-label">
						種別<span class="require">必須</span>
					</label>

					<div class="contact-input">
						
						<div class="switch-form">
							
							<div class="custom-checkbox" id="open-form-contact1">
								<label>
									<input type="radio" disabled> 
									<div class="vir-label">
										<span class="vir-checkbox"></span>
										<span class="vir-content">サイトに関する問い合わせ</span>
									</div>
								</label>
							</div>
							
							<div class="custom-checkbox">
								<label>
									<input type="radio" checked="checked">
									<div class="vir-label">
										<span class="vir-checkbox"></span>
										<span class="vir-content">求人掲載に関するお問い合わせ</span>
									</div>
								</label>
							</div>
							
						</div>
						
					</div>
				</div>
			</div>
			
			<div class="form-group" id="content-step1Content">
			</div>

			<div class="form-group">
				<label for="step1CompanyName" class="contact-lablel control-label">
					会社名<span class="require">必須</span>
				</label>

				<div class="contact-input">
					<input type="text" name="step1CompanyName" id="step1-company-name" value="" class="form-control" aria-required="true" aria-invalid="false" placeholder="会社名を入力">
				</div>
			</div>

			<div class="form-group">
				<label for="step1DepartmentNamePosition" class="contact-lablel control-label">
					部署名役職<span class="require">必須</span>
				</label>

				<div class="contact-input">
					<input type="text" name="step1DepartmentNamePosition" id="step1-department-name-position" value="" class="form-control" aria-required="true" aria-invalid="false" placeholder="部署名役職を入力">
				</div>
			</div>

			<div class="form-group" id="content-step1FullName">
			</div>

			<div class="form-group" id="content-step1Email">
			</div>
			<div class="form-group">
				<label for="step1PhoneNumber" class="contact-lablel control-label">
					電話番号<span class="require">必須</span>
				</label>

				<div class="contact-input">
					<input type="text" name="step1PhoneNumber" id="step1-phone-number" value="" class="form-control" aria-required="true" aria-invalid="false" placeholder="電話番号を入力">
				</div>
			</div>

			<div class="form-group">
				<label for="step1HowToAnswer" class="contact-lablel control-label">
					お問い合わせ解答方法
				</label>

				<div class="contact-input">
					<select name="step1HowToAnswer" id="step1-how-to-answer" class="form-control">
						<option value="メールでの解答を希望する。">メールでの解答を希望する。</option>
						<option value="電話での回答を希望する。">電話での回答を希望する。</option>
					</select>
				</div>
			</div>
			
			<div class="custom-checkbox">
				<label>
					<input type="checkbox" id="step1Agree" name="step1Agree" value="agree">
					<div class="vir-label">
						<span class="vir-checkbox"></span>
						<span class="vir-content">利用規約・プライバシポリシーに同意する</span>
					</div>
				</label>
			</div>
			<div class="custom-text">
				<p class="text-center">【お問い合わせフォームに関する注意事項】</p>
				<p>「@callnavi.jp」からのメール受信が可能な設定にしていただきますようお願いいたします。<br>15分以上以内に返信メールが届かない場合は、迷惑メールフォルダをご確認ください。</p>
			</div>
		</section>

		<h2>STEP2</h2>
		<section class="step2">
			
			<div class="form-group">
				<div class="wp-st">
					<label for="step1Email" class="contact-lablel control-label">
						種別
					</label>

					<div class="contact-input">
						
						<div class="switch-form">
							
							<div class="custom-checkbox" id="open-form-contact1">
								<label>
									<input type="radio" disabled> 
									<div class="vir-label">
										<span class="vir-checkbox"></span>
										<span class="vir-content color-disable">サイトに関する問い合わせ</span>
									</div>
								</label>
							</div>
							
							<div class="custom-checkbox">
								<label>
									<input type="radio" checked="checked">
									<div class="vir-label">
										<span class="vir-checkbox"></span>
										<span class="vir-content">求人掲載に関するお問い合わせ</span>
									</div>
								</label>
							</div>
							
						</div>
						
					</div>
				</div>
			</div>
			
			<div class="form-group" id="content-step2Content">
			</div>

			<div class="form-group">
				<label for="step2CompanyName" class="contact-lablel control-label">
					会社名
				</label>

				<div class="contact-input">
					<input type="text" name="step2CompanyName" disabled id="step2-company-name" class="form-control" aria-required="true" aria-invalid="false" placeholder="会社名を入力">
				</div>
			</div>

			<div class="form-group">
				<label for="step2DepartmentNamePosition" class="contact-lablel control-label">
					部署役職名
				</label>

				<div class="contact-input">
					<input type="text" name="step2DepartmentNamePosition" disabled id="step2-department-name-position" class="form-control" aria-required="true" aria-invalid="false" placeholder="部署名役職を入力">
				</div>
			</div>

			<div class="form-group" id="content-step2FullName">
			</div>

			<div class="form-group" id="content-step2Email">
			</div>

			<div class="form-group">
				<label for="step2PhoneNumber" class="contact-lablel control-label">
					電話番号
				</label>

				<div class="contact-input">
					<input type="text" name="step2PhoneNumber" disabled id="step2-phone-number" class="form-control" aria-required="true" aria-invalid="false" placeholder="電話番号を入力">
				</div>
			</div>

			<div class="form-group">
				<label for="step2HowToAnswer" class="contact-lablel control-label">
					お問い合わせ解答方法
				</label>

				<div class="contact-input">
					<select name="step2HowToAnswer" id="step2-how-to-answer" disabled class="form-control">
						<option value="メールでの解答を希望する。">メールでの解答を希望する。</option>
						<option value="電話での回答を希望する。">電話での回答を希望する。</option>
					</select>
				</div>
			</div>
			
			<div class="custom-checkbox" >
						<label>
							<input type="checkbox" value="1" name="step2Agree" id="step2Agree">
							<div class="vir-label">
								<span class="vir-checkbox"></span>
								<span class="vir-content" id="color-disable2">利用規約・プライバシポリシーに同意する</span>
							</div>
						</label>
			</div>
			<div class="custom-text">
				<p class="text-center">【お問い合わせフォームに関する注意事項】</p>
				<p>「@callnavi.jp」からのメール受信が可能な設定にしていただきますようお願いいたします。<br>15分以上以内に返信メールが届かない場合は、迷惑メールフォルダをご確認ください。</p>
			</div>
		</section>

		<h2>完了</h2>
		<section class="finish">
			<p>
				この度はお問い合せ頂き誠にありがとうございました。
				<br> 改めて担当者よりご連絡をさせていただきます。
			</p>
			<div class="finish-tr">
				<div class="finish-td">
					<p class="text-center">【お問い合わせフォームに関する注意事項】</p>
					<p class="text-center">「@callnavi.jp」からのメール受信が可能な設定にしていただきますようお願いいたします。<br>15分以上以内に返信メールが届かない場合は、迷惑メールフォルダをご確認ください。</p>
				</div>
			</div>
		</section>
	</div>

</form>