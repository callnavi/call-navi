<form id="frm-contact-form2"
      action="<?php echo $this->Html->url(array('controller' => 'contact', 'action' => 'SendContactMail'), true); ?>"
      method="post" accept-charset="utf-8" enctype="multipart/form-data" novalidate="novalidate">
    <div id="contact-form2">
        <h2>STEP1</h2>
        <section class="step1">
            <div class="form-group">
                <div >
                    <label for="step1FullName" class="contact-lablel control-label">
                        種別<span class="require border-blue">必須</span>
                    </label>

                    <div class="contact-input">
                        <div class="custom-checkbox" id="open-form-contact1">
                            <label>
                                <input type="radio" disabled>
                                <div class="vir-label circle-style">
                                    <span class="vir-content">個人</span>
                                    <span class="vir-checkbox"></span>
                                </div>
                            </label>
                        </div>

                        <div class="custom-checkbox">
                            <label>
                                <input type="radio" checked="checked">
                                <div class="vir-label circle-style">
                                    <span class="vir-content">法人</span>
                                    <span class="vir-checkbox"></span>
                                </div>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group" id="step1CompanyName">
                <div id="st1-companyname">
                    <label for="step1CompanyName" class="contact-lablel control-label">
                        会社名<span class="require border-blue">必須</span>
                    </label>

                    <div class="contact-input">
                        <input type="text" name="step1CompanyName" id="step1-company-name" value="" class="form-control"
                               aria-required="true" aria-invalid="false" placeholder="会社名を入力">
                        <input type="text" name="step1DepartmentNamePosition" id="step1-department-name-position" value=""
                               class="form-control mg-top-30" aria-required="true" aria-invalid="false"
                               placeholder="部署名・役職を入力">
                    </div>
                </div>
            </div>
            <div class="form-group" id="content-step1FullName">
            </div>

            <div class="form-group" id="content-step1Email">
            </div>

            <div class="form-group">
                <div id="st1-fullname">
                    <label for="step1PhoneNumber" class="contact-lablel control-label">
                        電話番号<span class="require border-blue">必須</span>
                    </label>

                    <div class="contact-input">
                        <input type="text" name="step1PhoneNumber" id="step1-phone-number" value="" class="form-control"
                               aria-required="true" aria-invalid="false" placeholder="電話番号を入力">
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div id="st1-fullname">
                    <label for="step1HowToAnswer" class="contact-lablel control-label">
                        解答方法<span class="require border-blue">必須</span>
                    </label>

                    <div class="contact-input left-arrow-select">
                        <select name="step1HowToAnswer" id="step1-how-to-answer" class="form-control">
                            <option value="メールでの解答を希望する。">メールでの解答を希望する。</option>
                            <option value="電話での回答を希望する。">電話での回答を希望する。</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group" id="content-step1Content">

                <div class="custom-checkbox mg-top-40 mg-bottom-10">
                    <label>
                        <input type="checkbox" id="step1Agree" name="step1Agree" value="agree">
                        <div class="vir-label circle-style">
                            <span class="vir-checkbox"></span>
                            <span class="vir-content">利用規約・プライバシポリシーに同意する</span>
                        </div>
                    </label>
                </div>
            </div>
            <section class="contact-custom-text">
                <p class="text-center">【お問い合わせフォームに関する注意事項】</p>
                <p>「@callnavi.jp」からのメール受信が可能な設定にしていただきますようお願いいたします。<br>15分以上以内に返信メールが届かない場合は、迷惑メールフォルダをご確認ください。</p>
            </section>

        </section>

        <h2>STEP2</h2>
        <section class="step2">
            <div class="form-group">
                <div id="st1-fullname">
                    <label for="step1FullName" class="contact-lablel control-label">
                        種別<span class="require border-blue">必須</span>
                    </label>

                    <div class="contact-input">
                        <div class="custom-checkbox" id="open-form-contact1">
                            <label>
                                <input type="radio" disabled>
                                <div class="vir-label circle-style">
                                    <span class="vir-content">個人</span>
                                    <span class="vir-checkbox"></span>
                                </div>
                            </label>
                        </div>

                        <div class="custom-checkbox">
                            <label>
                                <input type="radio" checked="checked">
                                <div class="vir-label circle-style">
                                    <span class="vir-content">法人</span>
                                    <span class="vir-checkbox"></span>
                                </div>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group" id="step2CompanyName">
                <div id="st1-fullname">
                    <label for="step2CompanyName" class="contact-lablel control-label">
                        会社名<span class="require border-blue">必須</span>
                    </label>

                    <div class="contact-input">
                        <input type="text" name="step2CompanyName" id="step2-company-name" value="" class="form-control" disabled
                               aria-required="true" aria-invalid="false" placeholder="会社名を入力">
                        <input type="text" name="step2DepartmentNamePosition" id="step2-department-name-position" value="" disabled
                               class="form-control mg-top-30" aria-required="true" aria-invalid="false"
                               placeholder="部署名・役職を入力">
                    </div>
                </div>
            </div>
            <div class="form-group" id="content-step2FullName">
            </div>

            <div class="form-group" id="content-step2Email">
            </div>

            <div class="form-group">
                <div id="st1-fullname">
                    <label for="step2PhoneNumber" class="contact-lablel control-label">
                        電話番号<span class="require border-blue">必須</span>
                    </label>

                    <div class="contact-input">
                        <input type="text" name="step2PhoneNumber" id="step2-phone-number" value="" class="form-control"
                               aria-required="true" aria-invalid="false" placeholder="電話番号を入力" disabled>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div id="st1-fullname">
                    <label for="step2HowToAnswer" class="contact-lablel control-label">
                        解答方法<span class="require border-blue">必須</span>
                    </label>

                    <div class="contact-input left-arrow-select">
                        <select name="step2HowToAnswer" id="step2-how-to-answer" class="form-control" disabled>
                            <option value="メールでの解答を希望する。">メールでの解答を希望する。</option>
                            <option value="電話での回答を希望する。">電話での回答を希望する。</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="form-group" id="content-step2Content">

                <div class="custom-checkbox mg-top-40 mg-bottom-10">
                    <label>
                        <input type="checkbox" id="step2Agree" name="step2Agree" value="agree">
                        <div class="vir-label circle-style">
                            <span class="vir-checkbox"></span>
                            <span class="vir-content">利用規約・プライバシポリシーに同意する</span>
                        </div>
                    </label>
                </div>
            </div>
            <section class="contact-custom-text">
                <p class="text-center">【お問い合わせフォームに関する注意事項】</p>
                <p>「@callnavi.jp」からのメール受信が可能な設定にしていただきますようお願いいたします。<br>15分以上以内に返信メールが届かない場合は、迷惑メールフォルダをご確認ください。</p>
            </section>
        </section>

        <h2>完了</h2>
        <section class="finish">
            <p>
                この度はお問い合せ頂き誠にありがとうございました。
                <br>
                改めて担当者よりご連絡をさせていただきます。
            </p>
            <div class="contact-custom-text">
                <p class="text-center">【お問い合わせフォームに関する注意事項】</p>
                <p>「@callnavi.jp」からのメール受信が可能な設定にしていただきますようお願いいたします。<br>15分以上以内に返信メールが届かない場合は、迷惑メールフォルダをご確認ください。</p>
            </div>
            <div class="finish-link">
                <a href="/">ページトップに戻る</a>
            </div>
        </section>
    </div>

    <input type="hidden" name="job_id" id="job_id" value=""/>
</form>