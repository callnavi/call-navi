<?php $this->layout = 'single_column_v2_sp'; ?>
<?php
$this->set('title', '美男美女図鑑 ｜ コールセンター特化型求人サイト「コールナビ」');
$this->start('css');
echo $this->Html->css('pretty-sp.css');
$this->end('css');

//ogp meta
$this->app->meta_ogp($this);

$tabs = array(
  '新着順' => '/pretty',
  '美女' => '/pretty/美女',
  '美男' => '/pretty/美男',
);
$active = !empty(explode('/',$_SERVER["REQUEST_URI"])[2]) ? explode('/',$_SERVER["REQUEST_URI"])[2] : '新着順';
$active = urldecode($active);

$this->append('script');
  $this->app->echoScriptScrollSidebarJs();
  echo $this->Html->script('pretty-sp.js');
$this->end('script');

?>

<div class="banner">
  <img src="/img/banner/top_pretty_sp_banner.jpg">
</div>

<div class="pretty-content<?php echo ($show_num) ? " box_cat" : "";?>">
  <div class="pretty-content-header gb-gray">
    <h1 class="pretty-title"><strong>総合ランキング</strong><p><?php echo date('Y年m月01日').'〜'.date('m月t日'); ?></p></h1>
  </div>
  <div class="pretty-content-header mg-top-5">
    <p class="fz-xxs">ランキングは記事のPV数（ページビュー数）に基づいて表示しています。</p>
  </div>
  <div class="pretty-tab-header mg-top-20">
    <?php foreach ($tabs as $key=>$link): ?>
    <?php if ($key == $active): ?>
    <div class="tab-item active">
      <?php else: ?>
      <div class="tab-item">
        <?php endif; ?>
        <a href="<?php echo $link; ?>">
          <span><?php echo $key; ?></span>
        </a>
      </div>
      <?php endforeach; ?>
    </div>
    <div class="pretty-tab-content gb-gray">
      <?php
      if($show_num){
        echo '<div class="title_stt">TOP 1 〜 6</div>';
      }
      ?>
      <ul class="list-col list-col-2 gb-gray">
        <?php
        if(!empty($data)){
          $i = 0;
          foreach ($data as $x){
            $i++;
            $row = $x['Pretty'];
            if($i < 7){
				  $age = date("Y") - date("Y",strtotime($row['birthday']));
				  $row['show_num'] = $show_num;
				  if($show_num){
					$is_new = '<span class="i-count"><span>'.$i.'</span></span>' ;
					$pv = '<span class="s-pv"><big class="pv_'.$row['id'].'">'.$row['point_pv'].'</big>PV</span>';
					$class = ' had_cat';
					$row['level'] = $i;
				  }else{
					$is_new = ($row['is_new']) ? '<span class="i-new">NEW</span>' : "";
					$pv = !$row['is_new'] ? '<span class="s-pv"><big class="pv_'.$row['id'].'">'.$row['point_pv'].'</big>PV</span><br>' : "";
					$class = "";
					$row['level'] = "NEW";
				  }

              ?>
              <li>
                <div class="pretty-item">

                    <div class="search-image">
                      <img src="<?php echo $this->webroot?>upload/pretty/<?php echo $row['pic_1']?>" alt="<?php echo $row['nick_name']?>">
                    </div>

                    <div class="search-content<?php echo $class?>">
                      <div class="padding">
                        <div>
                          <?php echo $is_new?>
                          <span class="i-nick">
                            <?php echo $pv?>
                            <big><?php echo $row['nick_name']?></big><b>さん</b> <?php echo $age?>才
                          </span>
                        </div>

                        <div class="div_button">
                          <button data-pretty="open"
                                  data-id = "<?php echo $row['id']?>"
                                  data-value='<?php echo json_encode($row); ?>'
                                  class="pretty-btn">詳しい情報をみる</button>
                        </div>
                      </div>

                    </div>
                </div>
              </li>
              <?php

            } }
        }
        ?>
      </ul>
    </div>
    <?php if(!$show_num){?>
    <div class="gb-gray">
      <div class="pretty-div"></div>
    </div>
    <?php }?>
    <div class="pretty-list gb-gray">
      <?php
      if($show_num){
        echo '<div class="title_stt">TOP 7 〜 '.$total.'</div>';
      }
      ?>
      <ul class="list-col list-col-4">
        <?php
        if(!empty($data)) {
          $j = 0;
          foreach ($data as $v):
            $j ++;
            if($j > 6){
              $row_ex = $v['Pretty'];
              $row_ex['show_num'] = $show_num;
              if($show_num){
                $row_ex['level'] = $j;
              }else{
                $row_ex['level'] = 'NEW';
              }
              $age_ = date("Y") - date("Y",strtotime($row_ex['birthday']));
              $count = ($show_num) ? '<span class="s-count">'.$j.'</span>' : "";
              ?>
              <li>
                <div class="pretty-simple" >

                  <div class="image_left"><img src="<?php echo $this->webroot?>upload/pretty/<?php echo $row_ex['pic_1']?>" alt="<?php echo $row_ex['name']?>"></div>

                  <div class="info_right">
                    <div class="padding">

                      <div <?php echo ($show_num) ? : 'class="text-center"'?>>
                        <?php echo $count?><span class="s-pv"><big class="pv_<?php echo $row_ex['id']?>"><?php echo $row_ex['point_pv']?></big>PV</span>
                        <span class="i-nick">
                            <big><?php echo $row_ex['nick_name']?></big><b>さん</b> <?php echo $age_?>才
                          </span>
                      </div>

                      <div class="div_button">
                        <button data-pretty="open"
                                data-id = "<?php echo $row_ex['id']?>"
                                data-value='<?php echo json_encode($row_ex); ?>'
                                class="pretty-btn">詳しい情報をみる</button>
                      </div>

                    </div>

                  </div>

                </div>
              </li>
              <?php
            }
          endforeach;

        }?>
      </ul>
    </div>
  </div>
</div>



<div id="popup_pretty_modal" class="top-popup-search" style="display: none;" data-pretty="close">

  <div class="popup-container popup-pretty">
    <div class="clearfix">
			<span class="close-button" data-pretty="close">
					<i></i>
					<i></i>
					close
				</span>
    </div>
    <div id="popup_pretty_content" class="banner-search-wrap-box mg-top-10">
      <div class="popup-pretty-item">
        <div class="display-table">
          <div class="search-image">
            <img src="" alt="はじめまして！">
            <div class="info">
              <div class="text-bold">
                <span id="level"></span><span class="p-pv"></span><big class="p-nick"></big>さん <span id="age"></span>才
              </div>
              <div class="gray-hr mg-top-30"></div>
             <div class="mg-top-20">
                <div>
                  生年月日：<span><span id="year"></span>年<span id="month"></span>月<span id="day"></span>日</span>
                </div>
                <div class="mg-top-5">
                  出身地：<span id="address"></span>
                </div>
                <div class="mg-top-5">
                  憧れの人：<span id="name"></span>
                </div>
              </div>

            </div>

          </div>

          <div class="search-content">


            <div class="mg-top-15">
              <span class="black-brick">勤務地</span> <span class="company_name"></span>
            </div>
            <div class="mg-top-10">
              <a target="_blank" class="pretty-full-btn" href="" id="link_secret">求人詳細を今すぐ確認する</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

</div>


<?php echo $this->element('../Top/_social-sp'); ?>



