<?php
$title = '美男美女図鑑';
$this->set('title', $this->App->getWebTitle('pretty'));
$breadcrumb = array('page' => $title);

$tabs = array(
    '新着順' => '/pretty',
    '美女' => '/pretty/美女',
    '美男' => '/pretty/美男',
);
$active = !empty(explode('/', $_SERVER["REQUEST_URI"])[2]) ? explode('/', $_SERVER["REQUEST_URI"])[2] : '新着順';
$active = urldecode($active);
?>

<?php $this->start('sub_footer'); ?>
<?php echo $this->element('Item/bottom_banner'); ?>
<?php $this->end('sub_footer'); ?>

<?php $this->start('header_breadcrumb'); ?>
<?php echo $this->element('Item/breadcrumb', $breadcrumb); ?>
<?php $this->end('header_breadcrumb'); ?>

<?php $this->start('sub_header'); ?>
<?php 
// delete in issues#345
//echo $this->element('../Top/2017/_top_banner');?>
<?php $this->end('sub_header'); ?>

<?php $this->start('css'); ?>
<?php echo $this->Html->css('pretty.css'); ?>
<?php $this->end('css'); ?>

<?php $this->append('script'); ?>
<?php
$this->app->echoScriptScrollSidebarJs();
echo $this->Html->script('top.js');
echo $this->Html->script('pretty.js');
?>
<?php $this->end('script'); ?>


<div class="no-padding container ipad-padding padding-t-170">
    <div class="clearfix">
        <div class="pull-left content">
            <div id="left-column" class="pretty-layout mg-top-50">
                <div class="pretty-topic">
                    <img src="/img/pretty/header_pc.png">
                </div>

                <div class="pretty-content mg-top-10">
                    <div class="pretty-content-header gb-gray">
                        <h1 class="pretty-title"><strong>総合ランキング</strong><strong>
                                <small><?php echo date('Y年m月01日').'〜'.date('m月t日'); ?></small>
                            </strong></h1>
                    </div>
                    <div class="pretty-content-header mg-top-5">
                        <p class="fz-xxs">ランキングは記事のPV数（ページビュー数）に基づいて表示しています。</p>
                    </div>
                    <div class="pretty-tab-header mg-top-20">
                        <?php foreach ($tabs as $key => $link): ?>
                        <?php if ($key == $active): ?>
                        <div class="tab-item active">
                            <?php else: ?>
                            <div class="tab-item">
                                <?php endif; ?>
                                <a href="<?php echo $link; ?>">
                                    <span><?php echo $key; ?></span>
                                </a>
                            </div>
                            <?php endforeach; ?>
                        </div>

                        <div class="pretty-tab-content gb-gray">
                            <ul class="list-col list-col-2 gb-gray">
                                <?php
                                if (!empty($data)) {
                                    $i = 0;
                                    $pv = '';
                                    foreach ($data as $x):
                                        $i++;
                                        $row = $x['Pretty'];
                                        if ($i < 7) {
                                            $age = date("Y") - date("Y", strtotime($row['birthday']));
                                            if ($show_num) {
                                                if ($i < 4) {
                                                    $is_new = '<div class="i-top">TOP</div><span class="i-count">' . $i . '</span>';
                                                } else {
                                                    $is_new = '<span class="i-count">' . $i . '</span>';
                                                }

                                            } else {
                                                $is_new = ($row['is_new']) ? '<span class="i-new">NEW</span>' : "";
                                            }
                                            $pv = '<span class="i-pv"><span class="pv_'.$row['id'].'">'.$row['point_pv'].'</span> <i class="i-unit">pv</i></span>' ;
                                            ?>
                                            <li>
                                                <div class="article-box pretty-item">
                                                    <div class="display-table">
                                                        <div class="search-image">
                                                            <img
                                                                src="<?php echo $this->webroot ?>upload/pretty/<?php echo $row['pic_1'] ?>"
                                                                alt="<?php echo $row['nick_name'] ?>">
                                                        </div>

                                                        <div class="search-content">
                                                            <div>
                                                                <?php echo $is_new . $pv ?>
                                                            </div>
                                                            <div class="mg-top-20 i-nick">
                                                                <big><?php echo $row['nick_name'] ?></big>
                                                                <small>さん <?php echo $age ?>才</small>
                                                            </div>
                                                            <div class="mg-top-5">
                                                                <button data-pretty="open"
                                                                        data-id="<?php echo $row['id'] ?>"
                                                                        data-value='<?php echo json_encode($row); ?>'
                                                                        class="pretty-btn">詳しい情報をみる
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <?php

                                        } endforeach;
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="gb-gray">
                            <div class="pretty-div"></div>
                        </div>
                        <div class="pretty-list gb-gray">
                            <ul class="list-col list-col-4">
                                <?php
                                if (!empty($data)) {
                                    $j = 0;
                                    foreach ($data as $v):
                                        $j++;
                                        if ($j > 6) {
                                            $row_ex = $v['Pretty'];
                                            $age_ = date("Y") - date("Y", strtotime($row_ex['birthday']));
                                            $count = ($show_num) ? '<span class="s-count">' . $j . '</span>' : "";

                                            ?>
                                            <li>
                                                <div class="pretty-simple" data-id="<?php echo $row_ex['id'] ?>"
                                                     data-value='<?php echo json_encode($row_ex); ?>'
                                                     data-pretty="open">

                                                    <img
                                                        src="<?php echo $this->webroot ?>upload/pretty/<?php echo $row_ex['pic_1'] ?>"
                                                        alt="<?php echo $row_ex['name'] ?>">
                                                    <span class="s-pv"><big
                                                            class="pv_<?php echo $row_ex['id'] ?>"><?php echo $row_ex['point_pv'] ?></big>PV</span>

                                                    <span class="s-nick">
														<big><?php echo $row_ex['nick_name'] ?></big>
														<small>さん <?php echo $age_ ?>才</small>
													</span>
                                                    <?php echo $count ?>
                                                </div>
                                            </li>
                                            <?php
                                        }
                                    endforeach;

                                } ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <div style="position: absolute;bottom: 0;" id="stopScrollPoint"></div>
            </div>
            <div class="pull-right slider">
                <div id="right-column">
                    <?php echo $this->element('../Top/_pickup'); ?>
                    <?php echo $this->element('slider_top'); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="mg-bottom-120"></div>


    <div id="popup_pretty_modal" class="top-popup-search" style="display: none;" data-pretty="close">

        <div class="popup-container popup-pretty">
            <div class="clearfix">
			<span class="close-button" data-pretty="close">
					<i></i>
					<i></i>
					close
				</span>
            </div>

            <div id="popup_pretty_content" class="banner-search-wrap-box mg-top-10">
                <div class="article-box popup-pretty-item">
                    <div class="display-table">
                        <div class="search-image">
                            <img src="" alt="はじめまして！">
                        </div>

                        <div class="search-content">
                            <div class="text-bold">
                                <big class="p-nick"></big>さん <span id="age"></span>才
                                <span class="p-pv"></span> pv
                            </div>
                            <div class="gray-hr mg-top-30"></div>
                            <div class="mg-top-15">
                                <span class="black-brick">勤務地</span> <span class="company_name"></span>
                            </div>

                            <div class="mg-top-20">
                                <div>
                                    生年月日：<span><span id="year"></span>年<span id="month"></span>月<span id="day"></span>日</span>
                                </div>
                                <div class="mg-top-5">
                                    出身地：<span id="address"></span>
                                </div>
                                <div class="mg-top-5">
                                    憧れの人：<span id="name"></span>
                                </div>
                            </div>

                            <div class="mg-top-50">
                                <span class="company_name"></span>
                            </div>
                            <div class="mg-top-10">
                                <a target="_blank" class="pretty-full-btn" href="" id="link_secret">求人詳細を今すぐ確認する</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

