<?php
	$link = $this->Html->url(array('controller' => 'blog', 'action' => 'index','category_alias'=>$item['BlogsCategory']['category_alias']), true);
	$title = $item['BlogsCategory']['title'];
?>
	
<div class="person-item-box">
	<div class="item">
		<div class="top-part">
			<img src="<?php echo $this->webroot."upload/blogs-category/avatar/".$item['BlogsCategory']['avatar'] ;?>" />
		</div>
		<div class="content-part">
			<a class="btn-enter" href="<?php echo $link;?>">ENTER</a>
		</div>
		<div class="bottom-part">
			<h3 class="i-title"><?php echo $title; ?></h3>
			<div class="short"><?php echo $item['BlogsCategory']['short']?></div>
		</div>
	</div>
</div>