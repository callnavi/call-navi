<?php $this->layout = 'single_column_v2_sp'; ?>
<?php
$title = ($info_cat['BlogsCategory']['friendly_title']) ? $info_cat['BlogsCategory']['friendly_title'] : $info_cat['BlogsCategory']['title'];
$this->set('title', 'コールナビ｜'.$title);
$this->start('script');
echo $this->Html->script('news-sp.js');
$this->end('script');
$this->start('css');
echo $this->Html->css('blog-sp.css');
$this->end('css');


$courseimage_fb = $this->Html->url('/', true)."upload/blogs-category/".$info_cat['BlogsCategory']['pic'];

$description = !empty($info_cat['BlogsCategory']['metadesc']) ? $info_cat['BlogsCategory']['metadesc'] : $info_cat['BlogsCategory']['short'];
//ogp meta
$this->app->meta_ogp(
	$this,
	//title
	$title,
	//decription
	$description,
	//keywords
	$info_cat['BlogsCategory']['metakey'],
	//url
	$this->Html->url('/', true).'blog/'.$info_cat['BlogsCategory']['category_alias'],
	//image
	$courseimage_fb,
	true
); 

?>

<?php echo $this->element('blog_info_v2_sp', array("category_alias" =>$info_cat['BlogsCategory']['category_alias']) );?>

<?php echo $this->element('../Blog/_mainList-sp'); ?>

<div class="show-more df-padding mg-top-40 mg-bottom-60">
	<a href="/blog" class="btn-blue-3d">ブログ＆コラム一覧に戻る</a>
</div>

<?php echo $this->element('../Top/_social-sp'); ?>




	
