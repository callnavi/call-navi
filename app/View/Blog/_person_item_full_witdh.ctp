<?php
	$link = $this->Html->url(array('controller' => 'blog', 'action' => 'index','category_alias'=>$item['BlogsCategory']['category_alias']), true);
	$date_post = ($item['blogs']['created']) ? $CreateDate = new DateTime($item['blogs']['created']) : "";
	$title = $item['BlogsCategory']['title'];
	if($date_post){
		$date = $date_post->format('Y年m月d日');
	}else{
		$date = "";
	}
	$amount =  $item['blogs']['cnt'];
?>

	<div class="person-item">
		<div class="avatar">
			<img src="<?php echo $this->webroot."upload/blogs-category/avatar/".$item['BlogsCategory']['avatar'] ;?>" />
		</div>
		<div class="div_img">
			 <img src="<?php echo $this->webroot."upload/blogs-category/".$item['BlogsCategory']['pic'] ;?>" />
		</div>
		<div class="info">
			<h3 class="i-title"><?php echo $title; ?></h3>
			<div class="short"><?php echo $item['BlogsCategory']['short']?></div>
			<hr />
			<div class="attribute">
				<div>好きな色：<span><?php echo $item['BlogsCategory']['color']?></span>興味があるもの：<span><?php echo $item['BlogsCategory']['interested']?></span></div>
				<div>座右の銘：<span><?php echo $item['BlogsCategory']['motto']?></span></div>
			</div>
			<div class="bottom-part">
				<a class="bookmark-this btn-gray" title="<?php echo $title; ?>" href="<?php echo $link;?>">▷ ブックマーク</a>
				<a title="<?php echo $title; ?>" class="btn-gray" href="<?php echo $link;?>">▷ ブログ一覧を見る</a>
				<span>ブログ数：<?php echo $amount ?>記事</span>
				<span class="i-date">最終更新日： <?php echo $date; ?></span>
			</div>
		</div>
	</div>