<?php $this->layout = 'single_column_v2_sp'; ?>
<?php 
$this->set('title', 'コールナビ｜コールセンターのプロがブログを配信');
$this->start('css');
echo $this->Html->css('blog-sp.css');
$this->end('css');

//ogp meta
$this->app->meta_ogp($this);
?>

<div class="banner">
	<div class="background clip-bottom-arrow">
		<img src="/img/banner/top_blog_sp_banner.jpg">
	</div>
	<div class="middle-content banner-title">
		<div class="banner-title-wp">
			<h1>Blog & Column</h1>
			<h2>コールセンターで働くメンバーがブログを配信。</h2>
		</div>
	</div>
</div>


<?php echo $this->element('../Blog/_index_list_sp'); ?>
<?php echo $this->element('../Top/_social-sp'); ?>
<?php echo $this->element('Item/concierge_link_banner_sp');?>




