<?php
$title = ($list['Blogs']['friendly_title']) ? $list['Blogs']['friendly_title'] : $list['Blogs']['title'];
$this->set('title', 'コールナビ｜'.$list['Blogs']['title']);
$breadcrumb =
	array('page' => $info['BlogsCategory']['title'],
		'parent' => array(
			array('link' => '/blog', 'title' => 'ブログ＆コラム'),
		)
	);


$courseimage_fb = $this->Html->url('/', true)."upload/blogs/".$list['Blogs']['pic'];

$description = !empty($list['Blogs']['metadesc']) ? $list['Blogs']['metadesc'] : $list['Blogs']['short'];
//ogp meta
$this->app->meta_ogp(
	$this,
	//title
	$title,
	//decription
	$description,
	//keywords
	$list['Blogs']['metakey'],
	//url
	$this->Html->url(null, true),
	//image
	$courseimage_fb,
	true
); 


$this->start('css');
echo $this->Html->css('blog.css');
$this->end('css');

$this->start('sub_footer');
echo $this->element('Item/bottom_banner');
$this->end('sub_footer');


$this->start('header_breadcrumb');
echo $this->element('Item/breadcrumb', $breadcrumb);
$this->end('header_breadcrumb');

$this->start('sub_header');
echo $this->element('blog_info_full_width',array('category_alias' => $info['BlogsCategory']['category_alias']));
$this->end('sub_header');



$CreateDate = new DateTime($list['Blogs']['created']);
?>
<?php $blogDetailHref = $this->Html->url(array('controller' => 'blog', 'action' => 'detail',"category_alias"=>$category_alias, 'id'=>$list['Blogs']['id']), true);?>

<div class="clearfix">

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding headerlink">
	<?php if(!empty($isPreview)){ ?>
		<?php echo $this->element('Item/btn_publish', array('status' => $list['Blogs']['status'])); ?>
	<?php } ?>
</div>

	<div class="content-center-column detail_blog">
		<div class="article-title">
			<h1><?php echo $list['Blogs']['title'];?></h1>
		</div>

		<div class="article-accessory">
			<div class="clearfix">
				<div class="pull-left">
					<div class="social-buttons">
						<a  class="facebook-button-article"
							onClick="window.open('http://www.facebook.com/sharer.php?s=100&amp;p[title]=<?php echo $title;?>&amp;p[summary]=&amp;p[url]=<?php echo $blogDetailHref; ?>&amp;p[images][0]=<?php echo $courseimage_fb;?>','sharer','toolbar=0,status=0,width=548,height=325');" href="javascript: void(0)">
							<i class="fa fa-facebook" aria-hidden="true"></i>
						</a>


						<a  target="_blank" 
							href="https://twitter.com/intent/tweet?url=<?php echo $blogDetailHref ;?>&via=コールナビ&text=<?php echo $title; ?>&related=コールナビ"
							class="twitter-button-article">
							<i class="fa fa-twitter" aria-hidden="true"></i>
						</a>


					</div>

				</div>
				<div class="pull-right mg-top-10">
					<time class="datetime"><?php echo $CreateDate->format('Y.m.d');?></time>
					<span class="view"><big><?php echo $list['Blogs']['view'];?></big>view</span>
				</div>
			</div>
		</div>

		<div class="middle-content mg-bottom-40">
			<?php echo $list['Blogs']['description'];?>
		</div>
		
		<?php if($other_list){?>
		<div class="article-relation content-center-column">
			<div class="relation-header">関連するブログ＆コラム</div>
			<div class="relation-list">
				<div class="main-list">
					<ol>
					<?php
					$i=0;

						foreach($other_list as $item){
							$i++;

							$link = $this->Html->url(array('controller' => 'blog', 'action' => 'detail',"category_alias"=>$info['BlogsCategory']['category_alias'], 'id'=>$item['Blogs']['id']), true);

							$CreateDate = new DateTime($item['Blogs']['created']);
							?>

							<li>
								<div class="item">
									<div class="top-part">
										<a href="<?php echo $link;?>">
											<?php if ($item['Blogs']['pic']): ?>
												<img src="<?php echo $this->webroot."upload/blogs/".$item['Blogs']['pic'] ;?>" />
											<?php else: ?>
												<img src="" alt="">
											<?php endif; ?>
											<span class="i-view"><?php echo $item['Blogs']['view']?> <small>view</small></span>
										</a>
									</div>
									<div class="content-part">
										<div class="clearfix">
											<div class="pull-left">
												<span class="i-date"><?php echo $CreateDate->format("Y.m.d")?></span>
											</div>
											<div class="pull-right">
												<a class="i-cat" href="#">コールセンター</a>
											</div>
										</div>

										<p class="i-title">
											<a href="<?php echo $link;?>"><?php echo mb_strlen($item['Blogs']['title'])> 65 ? mb_substr(  $item['Blogs']['title'],0,65,'UTF-8')."..." : $item['Blogs']['title'] ;?></a>
										</p>
									</div>
								</div>
							</li>
						<?php }?>
					</ol>
				</div>
			</div>
		</div>
		<?php }?>
		
		<div class="article-more mg-top-40 mg-bottom-60">
			<a href="/blog/<?php echo $info['BlogsCategory']['category_alias'] ?>">
				一覧に戻る
				<br>
				<span class="icon-arrow-down"></span>
			</a>
		</div>


	</div>


</div>
