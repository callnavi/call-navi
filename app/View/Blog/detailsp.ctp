<?php $this->layout = 'single_column_v2_sp'; ?>
<?php
$title = ($list['Blogs']['friendly_title']) ? $list['Blogs']['friendly_title'] : $list['Blogs']['title'];
$this->set('title', 'コールナビ｜'.$title);

$this->start('css');
	echo $this->Html->css('blog-sp.css');
$this->end('css');


$courseimage_fb = $this->Html->url('/', true)."upload/blogs/".$list['Blogs']['pic'];

$description = !empty($list['Blogs']['metadesc']) ? $list['Blogs']['metadesc'] : $list['Blogs']['short'];
//ogp meta
$this->app->meta_ogp(
	$this,
	//title
	$title,
	//decription
	$description,
	//keywords
	$list['Blogs']['metakey'],
	//url
	$this->Html->url(null, true),
	//image
	$courseimage_fb,
	true
); 


$CreateDate = new DateTime($list['Blogs']['created']);
?>

<?php $this->start('script'); ?>
 	<script type="text/javascript">
	 	$(document).ready(function() {
	 		<?php if(!empty($zoom) && $zoom): ?>
				$("html").css("zoom", "0.5");
			<?php endif; ?>
		});
	</script>
<?php $this->end('script'); ?>


<?php echo $this->element('blog_info_detail_v2_sp', array(
															"category_alias" =>$info['BlogsCategory']['category_alias'],
															"view_number" => $list['Blogs']['view']
														 ) );?>

<div class="blog-info df-padding blog-title">
	<h1 class="mg-top-30"><?php echo $list['Blogs']['title'];?></h1>
</div>

<div class="detail-datetime df-padding mg-top-20">
	<datetime><?php echo $CreateDate->format("Y年m月d日"); ?></datetime>
	<?php 
	if($this->App->isNew($list['Blogs']['created']))
	{
		echo '<span class="new-label">NEW</span>';
	}
	?>
</div>

<div class="detail-social df-padding mg-top-20">
	<div class="link">
        <?php $blogDetailHref = $this->Html->url(array('controller' => 'blog', 'action' => 'detail',"category_alias"=>$category_alias, 'id'=>$list['Blogs']['id']), true);?>
        
        <a class="icon-facebook" href="http://www.facebook.com/sharer.php?u=<?php echo $blogDetailHref; ?>" target="_blank">
             <img src="/img/icons/icon-facebook.png" alt="Facebookでシェアする">
        </a>
        
        <a class="icon-twitter" href="https://twitter.com/share?url=<?php echo $blogDetailHref; ?>" target="_blank">
             <img src="/img/icons/icon-twitter.png" alt="Tweetする">
        </a>

    </div>
</div>

<div class="detail-content detail-blog-design df-padding mg-top-50 image-rmp-content">
   <?php echo $list['Blogs']['description'];?>
</div>

<div class="show-more df-padding mg-top-40 mg-bottom-60">
	<a href="/blog/<?php echo $info['BlogsCategory']['category_alias']; ?>" class="btn-blue-3d">一覧に戻る</a>
</div>

<?php echo $this->element('../Top/_social-sp'); ?>
<?php echo $this->element('Item/concierge_link_banner_sp');?>
