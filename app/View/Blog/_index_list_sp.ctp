<div id="wrapper-content">
   <div class="contentBlog df-small-padding mg-bottom-40">
            <?php
            $i=0;
            foreach($list as $item){
                $i++;
                $class= "";
                if($i%2 ==0){
                    $class= " right";
                }else{
                    $class = " left";
                }
                $link = $this->Html->url(array('controller' => 'blog', 'action' => 'index','category_alias'=>$item['BlogsCategory']['category_alias']), true);

                $date_post = ($item['blogs']['created']) ? $CreateDate = new DateTime($item['blogs']['created']) : "";

                if($date_post){
                    $date = '	<span class="date_post">'.$date_post->format('Y.m.d').'</span>';
                }else{
                    $date = "";
                }


                ?>
                
                <div class="article-box">
                	<a href="<?php echo $link;?>">
						<div class="display-table">
							<div class="search-image">

									<img src="<?php echo $this->webroot."upload/blogs-category/avatar/".$item['BlogsCategory']['avatar'] ;?>" />

							</div>

							<div class="search-content">
								<h3><?php echo $item['BlogsCategory']['title']?></h3>
								<div class="short">
									<?php echo $item['BlogsCategory']['short']?>
								</div>
							</div>

							<div class="reference">
								<span class="icon-big-arrow"></span>
							</div>
					</div>
				</a>
			</div>
            <?php }?>
            <div class="clearfix"></div>
        </div>
</div>