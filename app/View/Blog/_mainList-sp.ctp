<div class="article-list">
	<ul class="fullcolor-list">
<?php foreach ($list as $item): ?>
	<?php
		$href = $this->Html->url(array('controller' => 'blog', 'action' => 'detail',"category_alias"=>$info_cat['BlogsCategory']['category_alias'], 'id'=>$item['Blogs']['id']), true);
		$createDate = new DateTime($item['Blogs']['created']);
		$createDate = $createDate->format("Y年m月d日");
		$view = $item['Blogs']['view'];
		$image = '';
		$isNew = $this->App->isNew($item['Blogs']['created']);
		$title = $item['Blogs']['title'];
		
		if ($item['Blogs']['pic'])
		{
			$image=$this->webroot."upload/blogs/".$item['Blogs']['pic'] ;
		}
		
		$title = str_replace("\r\n","", $title);
		if(mb_strlen($title) > 35)
		{
			$title = mb_substr($title,0,35,'UTF-8').'...';
		}
	?>
	<li class="df-padding">
			<div class="article-box">
				<div class="display-table">
					<div class="search-image">
						<a href="<?php echo $href; ?>">
							<img src="<?php echo $image; ?>">
						</a>
					</div>

					<div class="search-content">
						<div class="top-part clearfix">
							<div class="pull-left">
								<time><?php echo $createDate; ?></time>
								<?php if($isNew){?>
									<span class="new-label">NEW</span>
								<?php } ?>
							</div>
							<div class="pull-right">
								<big><?php echo $view; ?></big> view
							</div>
						</div>
						<div class="middle-part">
							<a href="<?php echo $href; ?>"><?php echo $title; ?></a>
						</div>
						<div class="bottom-part text-right">
						</div>
					</div>
				</div>
			</div>
		</li>
<?php endforeach; ?>
	</ul>
</div>