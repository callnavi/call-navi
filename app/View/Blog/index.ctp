<?php 
$this->set('title', 'コールナビ｜コールセンターのプロがブログを配信');
$breadcrumb = array('page' => 'ブログ＆コラム');
$this->start('script');
		echo $this->Html->script('create-company.js');
    // echo $this->Html->script('scroll-column.js');
    // echo $this->Html->script('blog.js');
	echo $this->Html->script('bookmark-this.js');
$this->end('script');

//ogp meta
$this->app->meta_ogp($this);

$this->start('css');
	echo $this->Html->css('blog.css');
$this->end('css');

$this->start('sub_footer');
	echo $this->element('Item/bottom_banner');
$this->end('sub_footer');
?>

<?php $this->start('sub_header'); ?>
<?php echo $this->element('../Top/2017/_top_banner');?>
<?php $this->end('sub_header'); ?>

<?php $this->start('header_breadcrumb'); ?>
<?php echo $this->element('Item/breadcrumb', $breadcrumb);?>
<?php $this->end('header_breadcrumb'); ?>


		<div class="blog-content">
			<div class="article blog-layout mg-top-40 mg-bottom-120">
				<div class="list-header non-type">
					<span>コールセンターで働くさまざまなメンバーがブログを配信。</span>
				</div>
				<div class="ContentBlog person-list" >
					<?php
					foreach($list as $item){
						echo $this->element('../Blog/_person_item_grid_box', array('item' => $item));
					}
					?>					
					<div class="clearfix"></div>
				</div>
				<?php echo $this->element('Item/pagination', ['currentPage' => $currentPage, 'pageCount' => $pageCount]); ?>
			</div>
		</div>

