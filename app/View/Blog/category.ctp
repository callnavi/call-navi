<?php
$title = ($info_cat['BlogsCategory']['friendly_title']) ? $info_cat['BlogsCategory']['friendly_title'] : $info_cat['BlogsCategory']['title'];
$breadcrumb =
	array('page' => $title_cat,
		'parent' => array(
			array('link' => '/blog', 'title' => 'ブログ＆コラム'),
		)
 		);
$this->set('title', 'コールナビ｜'.$title);
$this->start('script');
		echo $this->Html->script('create-company.js');
    	echo $this->Html->script('lazyload/jquery.lazyload.js');
		echo $this->Html->script('lazyload/lazyload_setup.js');
echo $this->Html->script('bookmark-this.js');
$this->end('script');


$courseimage_fb = $this->Html->url('/', true)."upload/blogs-category/".$info_cat['BlogsCategory']['pic'];

$description = !empty($info_cat['BlogsCategory']['metadesc']) ? $info_cat['BlogsCategory']['metadesc'] : $info_cat['BlogsCategory']['short'];
//ogp meta
$this->app->meta_ogp(
	$this,
	//title
	$title,
	//decription
	$description,
	//keywords
	$info_cat['BlogsCategory']['metakey'],
	//url
	$this->Html->url('/', true).'blog/'.$info_cat['BlogsCategory']['category_alias'],
	//image
	$courseimage_fb,
	true
); 


$this->start('css');
	echo $this->Html->css('blog.css');
$this->end('css');

$this->start('sub_footer');
echo $this->element('Item/bottom_banner');
$this->end('sub_footer');

$this->start('header_breadcrumb');
echo $this->element('Item/breadcrumb', $breadcrumb);
$this->end('header_breadcrumb');

$this->start('sub_header');
echo $this->element('blog_info_full_width',array('category_alias' => $this->params->pass[0]));
$this->end('sub_header');
?>

<div class="container blog-category-layout mg-top-40">
	<div class="contentBlog_bg" >
		<div class="article-list list_blog">
		<?php if($list){ ?>
		<div class="main-list">
			<ol>
			<?php
			foreach($list as $item){
				$link = $this->Html->url(array('controller' => 'blog', 'action' => 'detail',"category_alias"=>$info_cat['BlogsCategory']['category_alias'], 'id'=>$item['Blogs']['id']), true);
				$CreateDate = new DateTime($item['Blogs']['created']);
				if($item['Blogs']['pic'])
				{
					$image = $this->webroot."upload/blogs/".$item['Blogs']['pic'];
				}
				else
				{
					$image = '';
				}
				$view = $item['Blogs']['view'];

				$org_title = $item['Blogs']['title'];
				if(mb_strlen($item['Blogs']['title'])> 28) {
					$title = mb_substr(  $item['Blogs']['title'],0,28,'UTF-8')."...";
				} 
				else
				{
					$title = $item['Blogs']['title'];
				}

				$cateList = array(array(
					'name' => $item['Blogs']['tag'],
					'href' => '#'
				));
				
				
				$isNew = false;
				$diff = microtime(true) - strtotime($item['Blogs']['created']); 
				if($diff < 3)
				{
					$isNew = true;
				}
			?>

			<li>
				<?php echo $this->element('Item/item_dark_lazy_load', array(
					'href' => $link,
					'org_title' => $org_title,
					'image' => $image,
					'view' => $view,
					'createDate' => $CreateDate->format("Y.m.d"),
					'isNew' => false,
					'cateList' => $cateList,
					'title' => $title,
				)); ?>
			</li>

		<?php } ?>
				</ol>
		</div>
		<?php }?>

		</div>
	</div>
</div>

<div class="article-more mg-top-60 mg-bottom-60">
	<a href="/blog">
		ブログ＆コラム 一覧に戻る
		<br>
		<span class="icon-arrow-down"></span>
	</a>
</div>
