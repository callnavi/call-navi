<?php
/**
 */
?>

<div class="concierge-popup">
    <form class="contact-form" action="/concierge/thanks" method="post">
        <h4>下記の内容でコンシェルジュに依頼をする</h4>
        <table class="table-form" style="width:100%;">
            <tbody>
            <!--name-->
            <tr>
                <td>
                    <label for="name"><span class="require require-green">必須</span>お名前</label>
                </td>
                <td>
                    <input required type="text" class="" readonly
                           value="<?php echo !empty($data['name']) ? $data['name'] : ''; ?>" name="name"
                           placeholder="お名前をご入力ください">
                </td>
            </tr>
            <!-- end name-->

            <!--phone-->
            <tr>
                <td>
                    <label for="work_location"><span class="require require-green">必須</span>電話番号</label>
                </td>
                <td>
                    <input required type="text" class="" readonly
                           value="<?php echo !empty($data['phone']) ? $data['phone'] : ''; ?>" name="phone"
                           placeholder="電話番号をご入力ください">
                </td>
            </tr>
            <!--end phone-->


            <!--work location-->
            <tr>
                <td>
                    <label for="work_condition">備考</label>
                </td>
                <td>
                    <textarea name="work_condition" rows="4" readonly
                              placeholder="コンシェルジュにご相談したいことがございましたら、
ご記入ください
例)希望勤務地/雇用形態 等"><?php echo !empty($data['work_condition']) ? $data['work_condition'] : ''; ?></textarea>
                </td>
            </tr>
            <!--end work location-->
            </tbody>
        </table>
        <div class="popup-btn">
            <input type="button" onclick="history.go(-1);" name="return" class="btn_submit btn_gray" value="入力内容のリセット">
            <input type="submit" class="btn_submit" value="上記内容で送信する">
        </div>
    </form>
</div>
