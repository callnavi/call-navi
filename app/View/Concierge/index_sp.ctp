<?php
$this->layout = 'single_column_v2_sp';

$this->set('title', $this->App->getWebTitle('concierge'));

$this->start('css');
echo $this->Html->css('concierge-sp');
$this->end('css');

$this->start('script');
echo $this->Html->script('concierge-sp');
//echo "<script type='text/javascript' src='https://ws1.sinclo.jp/client/wiz.js' data-form='1'></script>";
$this->end('script');

$this->start('meta');
//meta description keywords image
echo Configure::read('webconfig.meta_description');
echo Configure::read('webconfig.meta_keywords');
echo Configure::read('webconfig.apple_touch_icon_precomposed');
echo Configure::read('webconfig.ogp');
$this->end('meta');
?>
<?php
echo $this->element('Item/top_banner_concierge_sp');
echo $this->element('Item/concierge_box_contact_sp');
?>
<div class="section point-title">
    <div class="container">
        <img src="/img/concierge/concierge_banner_full_2_sp.png">
    </div>
</div>




<div class="section">
    <div class="container">
        <div class="header-member-list">
            <h4>活躍中の求人コンサルタントをご紹介</h4>
            <img src="/img/concierge/concierge_member_heaser_sp.png">
        </div>

        <div class="banner-list mg-top-40">
            <ul>
                <li>
                    <div class="li-item">
                        <img src="/img/concierge/concierge_member_1_sp.png">
                        <p class="member-info">求人コンサルタント<br><span></span></p>
                    </div>
                </li>

                <li>
                    <div class="li-item">
                        <img src="/img/concierge/concierge_member_2_sp.png">
                        <p class="member-info">求人コンサルタント<br><span></span></p>
                    </div>
                </li>

                <li>
                    <div class="li-item">
                        <img src="/img/concierge/concierge_member_3_sp.png">
                        <p class="member-info">求人コンサルタント<br><span></span></p>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</div>

<?php echo $this->element('Item/concierge_box_contact_sp'); ?>

<div class="section concierge_banner_2 ">
    <div class="container ">
        <img width="100%" src="/img/concierge/concierge_step_by_step_sp_v2.png">
    </div>
</div>

<div class="section" id="contact_area">
    <?php
    if (!empty($data['type'])) {
        switch ($data['type']) {
            case 'confirm':
                echo $this->element('../Concierge/_confirm_modal_sp');
                break;
            case 'thanks':
                echo $this->element('../Concierge/_thanks_modal_sp');
                break;
        }
    } else {
        echo $this->element('../Concierge/_contact_modal_sp');
    }
    ?>
</div>
