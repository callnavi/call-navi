<div class="container contact-form">
    <h4 class="title text-center">お気軽にお問い合わせください</h4>
</div>
<div class="">
    <form class="contact-form" action="/concierge/thanks#contact_area" method="post">
        <dl class="mailform-01">

            <dt><label for="name"><span class="require require-green">必須</span>お名前</label></dt>
            <dd><input readonly type="text" class="" value="<?php echo !empty($data['name']) ? $data['name'] : ''; ?>"
                       name="name" placeholder="お名前を入力"></dd>


            <dt><label for="phone"><span class="require require-green">必須</span>電話番号</label></dt>
            <dd>

                <input readonly type="text" class="" value="<?php echo !empty($data['phone']) ? $data['phone'] : ''; ?>"
                         name="phone" placeholder="電話番号を入力">
            </dd>


            <dt><label for="work_condition">備考</label></dt>
            <dd><textarea name="work_condition" rows="6" readonly
                          placeholder="コンシェルジュにご相談したいことがございましたら、
ご記入ください
例)希望勤務地/雇用形態 等。"><?php echo !empty($data['work_condition']) ? $data['work_condition'] : ''; ?></textarea>
            </dd>

        </dl>

        <div class="popup-btn">
            <input type="submit" class="btn_submit" value="上記内容で送信する">
            <input type="button" onclick="history.go(-1);" name="return" class="btn_submit btn_gray" value="入力内容のリセット">
        </div>
    </form>
</div>