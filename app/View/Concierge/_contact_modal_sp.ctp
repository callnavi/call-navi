<?php
/**
 */
?>

<div class="container contact-form">
    <h4 class="title text-center">お気軽にお問い合わせください</h4>
    <p class="title_p">コールナビ求人コンサルタントがあなたのご要望に
            合わせてぴったりな求人をご案内いたします。<br>
      下記、必須項目をご記入ください。</p>
</div>
<div class="">
    <form novalidate id="concierge_form" class="contact-form" action="/concierge/confirm#contact_area" method="post">
        <dl class="mailform-01">

            <dt><label for="name"><span class="require require-green">必須</span>お名前</label></dt>
            <dd><input required type="text" class="" value="" name="name" placeholder="お名前をご入力ください"></dd>

            <dt><label for="work_location"><span class="require require-green">必須</span>電話番号</label></dt>
            <dd> <input required type="text" class="" value="" name="phone" placeholder="電話番号をご入力ください">
            </dd>


            <dt><label for="work_condition">備考</label></dt>
            <dd><textarea name="work_condition" rows="6" required placeholder="求人コンサルタントにご相談したいことがございましたら、ご記入ください。例)希望勤務地/雇用形態 等。"></textarea></dd>
			
       		<dd>
				<div class="privacy">
					<div class="custom-checkbox text-center df-lh custom-checkbox__except">
						<label class="mg-bottom-0">
							<input id="input_confirm" type="checkbox" name="concierge_confirm">
							<div class="vir-label">
								<span class="vir-checkbox green-dot"></span>
								<span class="vir-content color-disable1">
															当社<a target="_blank" href="/company">「個人情報の取り扱いについて」</a>同意する
														</span>
							</div>
						</label>
						<div id="input_confirm_error" class="error mg-top-5" style="display:none">この項目は必須です。</div>
					</div>
				</div>
			</dd>
        </dl>
		
       
        <input id="btn_submit" class="btn_submit" type="submit" value="確認画面へ進む">
    </form>
</div>
