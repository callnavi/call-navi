<?php
$this->set('title', $this->App->getWebTitle('concierge'));
$breadcrumb = array('page' => 'コールセンター・求人コンサルタント');
$this->start('meta');
//meta description keywords image
echo Configure::read('webconfig.meta_description');
echo Configure::read('webconfig.meta_keywords');
echo Configure::read('webconfig.apple_touch_icon_precomposed');
echo Configure::read('webconfig.ogp');
$this->end('meta');

$this->start('css');
echo $this->Html->css('concierge');
$this->end('css');

$this->start('header_breadcrumb');
echo $this->element('Item/breadcrumb', $breadcrumb);
$this->end('header_breadcrumb');
?>

<?php $this->start('sub_header'); ?>
    <div class="no-padding container">
        <div class="row">
            <?php echo $this->element('Item/top_banner_concierge'); ?>
        </div>
    </div>
<?php $this->end('sub_header'); ?>


    <div class="cocierge">
        <?php echo $this->element('Item/concierge_box_contact'); ?>

        <div class="section concierge_banner">
            <div class="no-padding container ">
                <div class="text-banner">
                    <h4 class="title text-center">コールセンター・求人コンサルタントとは<span class="text-green">？</span></h4>
                    <p class="text-center line-height-plus">コールセンター採用のエキスパートが、<br/>
                        あなたにぴったりのコールセンター求人をご紹介します。</p>
                </div>

            </div>
            <div class="concierge_banner__point">
                <ul class="point-box-list">
                  <li class="point-box-list__item">
                    <div class="point-box">
                      <div class="point-box__ttl">Point<span>1</span></div>
                      <p class="point-box__txt">コールセンター採用のエキスパートがあなたの要望をヒアリング。<br>面接設定までサポート！</p>
                    </div>
                  </li>
                  <li class="point-box-list__item">
                    <div class="point-box">
                      <div class="point-box__ttl">Point<span>2</span></div>
                      <p class="point-box__txt">コールナビだけのオリジナル求人も紹介できるから、あなたにピッタリな求人をご紹介できます！</p>
                    </div>
                  </li>
                  <li class="point-box-list__item">
                    <div class="point-box">
                      <div class="point-box__ttl">Point<span>3</span></div>
                      <p class="point-box__txt">希望者には研修などの教育メニューもご用意するなど、コールセンター未経験でもしっかりサポート！</p>
                    </div>
                  </li>
                </ul>
              </div>

        </div>
        <div class="section">
            <div class="no-padding container ">
                <h4 class="list-member-title">活躍中の求人コンサルタントをご紹介</h4>
                <p class="list-member-sub-title">日本全国で求人紹介が可能！<br>
                    地域ごとに専門の求人コンサルタントがご対応します。</p>

                <div class="banner-list text-center mg-top-40 mg-bottom-90">
                    <ul>
                        <li>
                            <div class="li-item">
                                <img src="/img/concierge/concierge_member_1.png">
                                <p class="member-info">求人コンサルタント<br><span></span></p>
                            </div>
                        </li>

                        <li>
                            <div class="li-item">
                                <img src="/img/concierge/concierge_member_2.png">
                                <p class="member-info">求人コンサルタント<br><span></span></p>
                            </div>
                        </li>

                        <li>
                            <div class="li-item">
                                <img src="/img/concierge/concierge_member_3.png">
                                <p class="member-info">求人コンサルタント<br><span></span></p>
                            </div>
                        </li>
                    </ul>
                    <ul class="list-member-2">
                        <li>
                            <div class="li-item">
                                <img src="/img/concierge/concierge_member_4.png">
                                <p class="member-info">求人コンサルタント<br><span></span></p>
                            </div>
                        </li>

                        <li>
                            <div class="li-item">
                                <img src="/img/concierge/concierge_member_5.png">
                                <p class="member-info">求人コンサルタント<br><span></span></p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="section bg-gray">
            <?php echo $this->element('Item/concierge_box_contact'); ?>
        </div>

        <div class="section concierge_banner_2">
            <div class="no-padding container ">
                <img src="/img/concierge/concierge_step_by_step_v2.png">
            </div>
        </div>

        <div class="section" id="contact_area">
            <div class="no-padding container contact-form">
                <h4 class="title text-center">お気軽にご依頼・ご相談ください</h4>
                <p class="p_title">コールナビ求人コンサルタントがあなたのご要望に合わせてぴったりな求人をご案内いたします。<br>
                    下記、必須項目をご記入ください。</p>
                <form novalidate id="concierge_form" class="contact-form" action="/concierge/confirm" method="post">
                    <table class="table-form" style="width: 100%">
                        <tbody>
                        <!--name-->
                        <tr>
                            <td>
                                <label for="name">
                                    <span class="require require-green">必須</span>
                                    <span>お名前</span></label>
                            </td>
                            <td>
                                <input required type="text" class=""
                                       value="<?php echo !empty($data['name']) ? $data['name'] : ''; ?>" name="name"
                                       placeholder="お名前をご入力ください">
                            </td>
                        </tr>
                        <!-- end name-->

                        <!--phone-->
                        <tr>
                            <td>
                                <label for="work_location">
                                    <span class="require require-green">必須</span>
                                    <span>電話番号</span>
                                </label>
                            </td>
                            <td>
                                <input required type="text" class=""
                                       value="<?php echo !empty($data['phone']) ? $data['phone'] : ''; ?>" name="phone"
                                       placeholder="電話番号をご入力ください">
                            </td>
                        </tr>
                        <!--end phone-->


                        <!--work location-->
                        <tr>
                            <td>
                                <label for="work_condition">備考</label>
                            </td>
                            <td>
                                <textarea name="work_condition" rows="6" required
                                          placeholder="求人コンサルタントにご相談したいことがございましたら、
ご記入ください。例)希望勤務地/雇用形態 等"><?php echo !empty($data['work_condition']) ? $data['work_condition'] : ''; ?></textarea>
                            </td>
                        </tr>
                        <!--end work location-->
                        </tbody>
                    </table>
					
                  	<div class="privacy">
                  		<div class="custom-checkbox text-center df-lh">
							<label class="mg-bottom-0">
								<input id="input_confirm" type="checkbox" name="concierge_confirm"> 
								<div class="vir-label">
									<span class="vir-checkbox green-dot"></span>
									<span class="vir-content color-disable1">
										当社<a target="_blank" href="/company">「個人情報の取り扱いについて」</a>同意する
									</span>
								</div>
							</label>
							<div id="input_confirm_error" class="error mg-top-5" style="display:none">この項目は必須です。</div>
						</div>
                  	</div>
                   
                    <input id="btn_submit" class="btn_submit" type="submit" value="求人コンサルタントに相談する">

                    <div class="sub-footer mg-bottom-20">
                        <img src="/img/concierge/concierge_sub_footer.png">
                    </div>
                     
                </form>
            	<div class="sub-footer mg-bottom-90 text-center">
                     	<a href="https://line.me/R/ti/p/%40zgv0276t">
                     	<img src="/img/concierge/concierge_contact_long.jpg">
                     	</a>
				</div>
            </div>
        </div>


    </div>

<?php

if (!empty($data['type'])) {
    switch ($data['type']) {
        case 'confirm':
            echo $this->element('../Concierge/_confirm_modal_pc');
            break;
        case 'thanks':
            echo $this->element('../Concierge/_thanks_modal_pc');
            break;
    }
} else {
    $this->start('script');
    echo $this->Html->script('concierge');
    $this->end('script');
}
?>