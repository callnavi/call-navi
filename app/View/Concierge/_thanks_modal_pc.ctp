<?php
/**
 */
?>

<div class="concierge-popup">
    <div class="dv-thanks">
        <h4>ご依頼を承りました。</h4>
        <p>この度はお問い合せ頂き誠にありがとうございました。<br>
            コールナビスタッフがお申し込み順に提携企業より<br>
            お客様に適任のキャリアコンシェルジュを手配いたします。<br>
            追って担当のキャリアコンシェルジュより<br>
            連絡がありますので、お待ちください。</p>
        <a href="/">コールナビ トップページへ</a>
    </div>
</div>
