        <section class="section" id="flow">
          <div class="section__inner">
            <h2 class="heading">
              <div class="heading__main-txt">FLOW</div>
              <div class="heading__sub-txt">ご契約までの流れ</div>
            </h2>
            <ol class="flow-box u-mt40">
              <li class="flow-box__item">
                <svg class="flow-box__icon" role="image">
                  <use xlink:href="../img/svg/symbols.svg#icon_mail"></use>
                </svg>
                <h3 class="u-mt20 u-fwb u-tac">お問い合わせ</h3>
                <p class="u-mt20">まずは、資料請求やお問い合わせでご連絡ください。お客様のご要望などお聞きいたします。</p>
              </li>
              <li class="flow-box__item">
                <svg class="flow-box__icon--l" role="image">
                  <use xlink:href="../img/svg/symbols.svg#icon_light"></use>
                </svg>
                <h3 class="u-mt20 u-fwb u-tac">ご提案</h3>
                <p class="u-mt20">ご要望に合わせて最適なプランをご提案いたします。</p>
              </li>
              <li class="flow-box__item">
                <svg class="flow-box__icon" role="image">
                  <use xlink:href="../img/svg/symbols.svg#icon_memo"></use>
                </svg>
                <h3 class="u-mt20 u-fwb u-tac">お申し込み</h3>
                <p class="u-mt20">申込手続き完了後、掲載する情報を作成する求人原稿作成フォーマットをお送りし、ご記入いただきます。</p>
              </li>
              <li class="flow-box__item">
                <svg class="flow-box__icon" role="image">
                  <use xlink:href="../img/svg/symbols.svg#icon_memo02"></use>
                </svg>
                <h3 class="u-mt20 u-fwb u-tac">ご掲載</h3>
                <p class="u-mt20">インタビュー記事や募集要項など最終確認後、掲載スタートとなります。</p>
              </li>
            </ol><a class="btn--contact u-mt40" href="/keisaiguide/contact"><img src="../img/keisaiguide/txt_btn_contact01.svg" alt="資料請求・お問い合わせはこちら"></a>
          </div>
        </section>