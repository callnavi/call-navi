      <header class="header-area">
        <figure>
          <svg class="header__logo" role="image">
            <title>コールナビ</title>
            <use xlink:href="../../img/svg/symbols.svg#logo"></use>
          </svg>
        </figure>
        <p class="header__txt">求人広告掲載ガイド</p>
      </header>