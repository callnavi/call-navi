      <h1 class="heading u-mt60">
        <div class="heading__main-txt">CONTACT</div>
        <div class="heading__sub-txt">資料請求・お問い合わせ</div>
      </h1>
      <article class="article u-fc--txt--contact">
        <section class="section">
          <div class="section__inner">
            <form class="form-area" name="" action="" method="post" accept-charset="utf-8" data-form="keisaiguide">
              <dl class="form-block">
                <dt class="form-block__ttl">
                  <div class="c-tag--require">必須</div>貴社名
                </dt>
                <dd class="form-block__content">
                  <div class="form--text--confirm"><?php echo($data['company']); ?></div>
                </dd>
              </dl>
              <dl class="form-block">
                <dt class="form-block__ttl">
                  <div class="c-tag--require">必須</div>ご担当者様
                </dt>
                <dd class="form-block__content">
                  <div class="form--text--confirm"><?php echo($data['name']); ?></div>
                </dd>
              </dl>
              <dl class="form-block">
                <dt class="form-block__ttl">
                  <div class="c-tag--require">必須</div>メールアドレス
                </dt>
                <dd class="form-block__content">
                  <div class="form--text--confirm"><?php echo($data['mail']); ?></div>
                </dd>
              </dl>
              <dl class="form-block">
                <dt class="form-block__ttl">
                  <div class="c-tag--require">必須</div>ご住所
                </dt>
                <dd class="form-block__content">
                  <div class="form--text--confirm"><?php echo($data['address']); ?></div>
                </dd>
              </dl>
              <dl class="form-block">
                <dt class="form-block__ttl">
                  <div class="c-tag--require">必須</div>電話番号
                </dt>
                <dd class="form-block__content">
                  <div class="form--text--confirm"><?php echo($data['phone']); ?></div>
                </dd>
              </dl>
              <dl class="form-block">
                <dt class="form-block__ttl">
                  <div class="c-tag--require">必須</div>お問い合わせ内容
                </dt>
                <dd class="form-block__content">
                  <div class="form--text--confirm"><?php echo($data['inquiry']); ?></div>
                </dd>
              </dl>
              <dl class="form-block">
                <dt class="form-block__ttl">お問い合わせ内容詳細</dt>
                <dd class="form-block__content">
                  <div class="form--textarea--confirm"><?php echo($data['remarks']); ?></div>
                </dd>
              </dl>
            </form>
            <div class="u-mt60"><a class="btn--primary" href="/keisaiguide/contact/thanks">送信する</a></div>
          </div>
        </section>
      </article>