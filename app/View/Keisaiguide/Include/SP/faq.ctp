        <section class="section--bg-beige" id="faq">
          <div class="section__inner">
            <h2 class="heading">
              <div class="heading__main-txt">FAQ</div>
              <div class="heading__sub-txt">よくある質問</div>
            </h2>
            <dl class="faq-block">
              <dt class="faq-block__que"><span class="faq-block__mark">Q</span>求人の掲載について質問です。費用が発生するのは、本当に採用できた時のみですか？</dt>
              <dd class="faq-block__ans"><span class="faq-block__mark">A</span>完全成果報酬ですので、採用されるまでは一切費用はかかりません。</dd>
            </dl>
            <dl class="faq-block">
              <dt class="faq-block__que"><span class="faq-block__mark">Q</span>掲載の職種に制限はありますか？</dt>
              <dd class="faq-block__ans"><span class="faq-block__mark">A</span>コールセンター特化型の求人サイトになりますので、コールセンター業務のご掲載をお願いしております。</dd>
            </dl>
            <dl class="faq-block">
              <dt class="faq-block__que"><span class="faq-block__mark">Q</span>申し込みからどのくらいで掲載できますか？</dt>
              <dd class="faq-block__ans"><span class="faq-block__mark">A</span>必要書類のご提出後、5営業日程度で原稿作成させていただきます。内容のご確認後、掲載開始となります。</dd>
            </dl>
            <dl class="faq-block">
              <dt class="faq-block__que"><span class="faq-block__mark">Q</span>求人の掲載期間はどれくらいですか？</dt>
              <dd class="faq-block__ans"><span class="faq-block__mark">A</span>完全成果報酬型ですので、掲載期間に定めはございません。ご希望されている期間分、掲載が可能です。求人の取り下げもご希望に合わせて切替可能です。</dd>
            </dl>
            <dl class="faq-block">
              <dt class="faq-block__que"><span class="faq-block__mark">Q</span>求人の掲載数に制限はありますか？</dt>
              <dd class="faq-block__ans"><span class="faq-block__mark">A</span>掲載数の制限はありません。ご希望される分、ご掲載が可能です。</dd>
            </dl>
          </div>
        </section>