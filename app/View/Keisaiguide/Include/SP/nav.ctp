      <nav class="contents-menu-wrap" data-height="nav">
        <ul class="contents-menu" data-target-menu="keisaiguide">
          <li class="contents-menu__item" data-target-nav="1"><a href="#feature" data-trigger-scroll="anchor"><span class="u-ff--lato u-fs--l">FEATURE</span><span class="u-fs--s">コールナビの特徴</span></a></li>
          <li class="contents-menu__item" data-target-nav="2"><a href="#flow" data-trigger-scroll="anchor"><span class="u-ff--lato u-fs--l">FLOW</span><span class="u-fs--s">ご契約までの流れ</span></a></li>
          <li class="contents-menu__item" data-target-nav="3"><a href="#faq" data-trigger-scroll="anchor"><span class="u-ff--lato u-fs--l">FAQ</span><span class="u-fs--s">よくある質問</span></a></li>
        </ul>
      </nav>