      <footer class="footer-area">
        <ul class="footer-link-list">
          <li class="footer-link-list__item"><a href="/">HOME</a></li>
          <li class="footer-link-list__item"><a href="/company">会社概要</a></li>
          <li class="footer-link-list__item"><a href="/company">プライバシーポリシー</a></li>
        </ul>
        <p class="footer__copy">&copy; 2015 CALL Navi Inc.</p>
      </footer>