      <h1 class="heading u-mt60">
        <div class="heading__main-txt">CONTACT</div>
        <div class="heading__sub-txt">資料請求・お問い合わせ</div>
      </h1>
      <article class="article u-fc--txt--contact">
        <section class="section">
          <div class="section__inner">
            <form class="form-area" name="" action="/keisaiguide/contact/confirm" method="post" accept-charset="utf-8" data-form="keisaiguide">
              <dl class="form-block">
                <dt class="form-block__ttl">
                  <div class="c-tag--require">必須</div>
                  <label for="company">貴社名</label>
                </dt>
                <dd class="form-block__content">
                  <input class="form--text" id="company" type="text" name="company" placeholder="貴社名を入力ください">
                  <div class="error-msg" data-target-msg="error"></div>
                </dd>
              </dl>
              <dl class="form-block">
                <dt class="form-block__ttl">
                  <div class="c-tag--require">必須</div>
                  <label for="name">ご担当者様</label>
                </dt>
                <dd class="form-block__content">
                  <input class="form--text" id="name" type="text" name="name" placeholder="ご担当者様のお名前を入力ください">
                  <div class="error-msg" data-target-msg="error"></div>
                </dd>
              </dl>
              <dl class="form-block">
                <dt class="form-block__ttl">
                  <div class="c-tag--require">必須</div>
                  <label for="email">メールアドレス</label>
                </dt>
                <dd class="form-block__content">
                  <input class="form--text" id="email" type="email" name="mail" placeholder="例）example@callnavi.jp">
                  <div class="error-msg" data-target-msg="error"></div>
                </dd>
              </dl>
              <dl class="form-block">
                <dt class="form-block__ttl">
                  <div class="c-tag--require">必須</div>
                  <label for="address">ご住所</label>
                </dt>
                <dd class="form-block__content">
                  <input class="form--text" id="address" type="text" name="address">
                  <div class="error-msg" data-target-msg="error"></div>
                </dd>
              </dl>
              <dl class="form-block">
                <dt class="form-block__ttl">
                  <div class="c-tag--require">必須</div>
                  <label for="phone">電話番号</label>
                </dt>
                <dd class="form-block__content">
                  <input class="form--text" id="phone" type="phone" name="phone" placeholder="半角数字（ハイフンなし）">
                  <div class="error-msg" data-target-msg="error"></div>
                </dd>
              </dl>
              <dl class="form-block">
                <dt class="form-block__ttl">
                  <div class="c-tag--require">必須</div>
                  <label for="inquiry">お問い合わせ内容</label>
                </dt>
                <dd class="form-block__content">
                  <div class="form-select-triangle">
                    <select class="form--select" id="inquiry" name="inquiry" data-trigger-selectbox="select">
                      <option class="form--select__placeholder" value="">お問い合わせ内容を選択してください</option>
                      <option value="資料請求">資料請求</option>
                      <option value="申込希望">申込希望</option>
                      <option value="その他お問い合わせ">その他お問い合わせ</option>
                    </select>
                  </div>
                  <div class="error-msg" data-target-msg="error"></div>
                </dd>
              </dl>
              <dl class="form-block">
                <dt class="form-block__ttl">
                  <label for="remarks">お問い合わせ内容詳細</label>
                </dt>
                <dd class="form-block__content">
                  <p>何か追記事項等ございましたらご記入ください</p>
                  <textarea class="form--textarea u-mt10" id="remarks" name="remarks" rows="4"></textarea>
                </dd>
              </dl>
              <div class="u-tac u-mt40">
                <p class="u-tal">お預かりするお客様の個人情報は、弊社の『個人情報保護管理規定』に基いて厳重に管理し、お預かりするお客様の個人情報は、弊社の『個人情報保護管理規定』に基いて厳重に管理し、お客様の同意がない限りお問い合わせ・ご相談への対応以外には使用いたしません。お問い合わせ・ご相談内容に応じて、後日、各担当者より電話またはメールにてご回答させていただきます。尚、上記フォームのご利用に際しまして「<a class="txt-link" href="/company">個人情報保護方針</a>」にご同意いただく必要がございますことを予めご了承ください。</p>
                <label class="agreement-checkbox-label u-mt40" for="agreement">
                  <input class="agreement-checkbox" id="agreement" type="checkbox" name="agreement" value=""><span>個人情報の取り扱いに同意する</SPan>
                </label>
                <div class="error-msg" id="agreementError" data-target-msg="error"></div>
              </div>
              <div class="u-mt40">
                <a class="btn--primary" type="submit" data-form-submit="confirm">確認画面へ進む</a>
              </div>
            </form>
          </div>
        </section>
      </article>