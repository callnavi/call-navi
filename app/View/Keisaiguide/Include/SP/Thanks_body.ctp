      <h1 class="heading u-mt60">
        <div class="heading__main-txt">CONTACT</div>
        <div class="heading__sub-txt">資料請求・お問い合わせ</div>
      </h1>
      <article class="article u-fc--txt--contact">
        <section class="section">
          <div class="section__inner u-mt20">
            <h2 class="u-fs--xl u-tac">送信完了</h2>
            <p class="u-fs--m u-mt40 u-tac">お問い合わせありがとうございました。<br>担当者から２営業日以内で対応させていただきますが、<br>内容によってはお時間いただく場合もございます。<br>ご了承頂けますよう、宜しくお願いいたします。</p>
            <div class="u-mt180"><a class="btn--primary" href="/">TOPへ戻る</a></div>
          </div>
        </section>
      </article>