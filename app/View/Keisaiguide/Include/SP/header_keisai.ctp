        <header class="header-area">
          <figure>
            <svg class="header__logo" role="image">
              <title>コールナビ</title>
              <use xlink:href="../img/svg/symbols.svg#logo"></use>
            </svg>
          </figure>
          <h1 class="header__txt">求人広告掲載ガイド</h1>
        </header>
        <div class="main-visual">
          <figure class="main-visual__img"><img src="../img/keisaiguide/img_mv_sp.png" alt="コールセンター特化型求人サイト『コールナビ』。完全成果報酬型だから採用にいたるまで完全無料！"></figure>
        </div>