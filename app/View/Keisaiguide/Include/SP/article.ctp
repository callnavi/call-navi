        <section class="section">
          <div class="section__inner">
            <h2 class="u-fs--xxl u-tac">コールセンター特化型求人サイト<br>「コールナビ」は、完全成果報酬型求人サイトです。採用されるまで費用は一切かかりません</h2>
            <p class="u-mt30">採用決定して初めて料金が発生する料金メニューとなっていますので、求人広告費用の掛け捨てリスクもナシ。採用コストがかさんでお困りの企業様、この機会にぜひ「コールナビ」のオリジナル求人をお試しください！</p><a class="btn--contact u-mt30" href="/keisaiguide/contact"><img src="../img/keisaiguide/txt_btn_contact01.svg" alt="資料請求・お問い合わせはこちら"></a><a class="btn--telephone u-mt35" href="tel:0363672669"><img src="../img/keisaiguide/txt_btn_contact02.svg" alt="お電話でのお問い合わせはこちら 営業時間 9:00〜18:00 (定休日：土日祝日)"></a>
          </div>
        </section>