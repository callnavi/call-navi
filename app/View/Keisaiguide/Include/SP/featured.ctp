        <section class="section--bg-beige" id="feature">
          <div class="section__inner">
            <h2 class="heading">
              <div class="heading__main-txt">FEATURE</div>
              <div class="heading__sub-txt">コールナビの特徴</div>
            </h2>
            <div class="feature-item">
              <h3 class="heading--flex u-pr40">
                <figure class="heading--flex__icon"><img src="../img/keisaiguide/img_feature_01_sp.png" alt="特徴01"></figure><span class="heading--flex__txt">リスクの少ない完全報酬プラン！採用が決まるまで0円！</span>
              </h3>
              <div class="feature-item__contents u-mt30">
                <p class="feature-item__txt">広告費用の掛け捨てリスクなし！初期コスト0円で確実に採用されたいお客様にピッタリな「完全成果報酬プラン」をご用意しています。採用が決まるまで費用は一切かかりません。</p>
                <figure class="feature-item__img u-mt30"><img src="../img/keisaiguide/img_feature_01_01_sp.png" alt="コールナビは安心の定額制 採用が決まるまで完全無料！0円で求人掲載可能！ ※一部例外あり"></figure>
                <table class="feature-tbl u-mt40">
                  <colgroup>
                    <col width="33.33%">
                    <col width="33.33%">
                    <col width="33.33%">
                  </colgroup>
                  <thead>
                    <tr>
                      <th class="feature-tbl__th--vbottom">
                        <div class="feature-tbl__th__inner--left">掲載課金型</div>
                      </th>
                      <th class="feature-tbl__th--vbottom">
                        <div class="feature-tbl__th__inner--em">
                          <svg role="image">
                            <title>コールナビ</title>
                            <use xlink:href="../img/svg/symbols.svg#logo"></use>
                          </svg>オリジナル求人
                        </div>
                      </th>
                      <th class="feature-tbl__th--vbottom">
                        <div class="feature-tbl__th__inner--right">人材紹介型</div>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <th class="feature-tbl__th">
                        <div class="feature-tbl__th__inner">掛け捨てリスク</div>
                      </th>
                      <th class="feature-tbl__th">
                        <div class="feature-tbl__th__inner--em">掛け捨てリスク</div>
                      </th>
                      <th class="feature-tbl__th">
                        <div class="feature-tbl__th__inner">掛け捨てリスク</div>
                      </th>
                    </tr>
                    <tr>
                      <td class="feature-tbl__td">
                        <div class="feature-tbl__td__inner">
                          <div class="u-fwb u-tac u-fs--m">×</div>
                          <p class="u-fs--s">採用に至らなくても広告費用が発生</p>
                        </div>
                      </td>
                      <td class="feature-tbl__td">
                        <div class="feature-tbl__td__inner--em">
                          <div class="u-fwb u-tac">◯</div>
                          <p class="u-fs--s">採用決定まで無料</p>
                        </div>
                      </td>
                      <td class="feature-tbl__td">
                        <div class="feature-tbl__td__inner">
                          <div class="u-fwb u-tac">◯</div>
                          <p class="u-fs--s">採用決定まで無料</p>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <th class="feature-tbl__th">
                        <div class="feature-tbl__th__inner">採用コスト</div>
                      </th>
                      <th class="feature-tbl__th">
                        <div class="feature-tbl__th__inner--em">採用コスト</div>
                      </th>
                      <th class="feature-tbl__th">
                        <div class="feature-tbl__th__inner">採用コスト</div>
                      </th>
                    </tr>
                    <tr>
                      <td class="feature-tbl__td">
                        <div class="_td--bg-none__inner">
                          <div class="u-fwb u-tac">◯</div>
                          <p class="u-fs--s">広告費のみでOKのため、複数名の採用時には低コストで採用が可能。</p>
                        </div>
                      </td>
                      <td class="feature-tbl__td--bg-none" rowspan="2">
                        <div class="_td--bg-none__inner--em--last">
                          <div class="u-fwb u-tac">◯</div>
                          <p class="u-fs--s">成果報酬で掛け捨てリスクも少ないのに、採用費用も安価。プランによっては正社員でも年収の10％程度で採用可能。</p>
                        </div>
                      </td>
                      <td class="feature-tbl__td">
                        <div class="_td--bg-none__inner">
                          <div class="u-fwb u-tac u-fs--m">×</div>
                          <p class="u-fs--s">費用は採用者の年収によって左右されるため、優秀な人材になるほど高額になりやすい。</p>
                        </div>
                      </td>
                    </tr>
                    <tr>
                      <td>&nbsp;</td>
                      <td>&nbsp;</td>
                    </tr>
                  </tbody>
                </table>
                <p class="u-mt40 u-fs--xl u-fwb">「成果報酬型求人は高くてなかなか手が出せない…」という企業様もコールナビなら低コストで採用が可能！</p>
              </div>
            </div>
            <div class="feature-item">
              <h3 class="heading--flex u-pr40">
                <figure class="heading--flex__icon"><img src="../img/keisaiguide/img_feature_02_sp.png" alt="特徴02"></figure><span class="heading--flex__txt">ユーザーは「コールセンターに興味を持っている層」ばかり！</span>
              </h3>
              <div class="feature-item__contents u-mt30">
                <p class="feature-item__txt">コールナビはコールセンター特化型の求人サイトであるため、訪問しているサイトユーザーもコールセンターに興味を持っている層が多くを占めます。読み物コンテンツも多数揃えており、企業インタビューコンテンツも無料で作成・公開が可能です。</p>
                <figure class="feature-item__img u-mt30"><img src="../img/keisaiguide/img_feature_02_01_sp.png" alt="相互リンクで採用率・集客効果UP!"></figure>
                <p class="u-mt60 u-fs--xl u-fwb">求人ページからの訴求のみではなく、多角的にユーザーへ訴求が可能になっているのも特徴です！</p>
              </div>
            </div>
            <div class="feature-item">
              <h3 class="heading--flex u-pr40">
                <figure class="heading--flex__icon"><img src="../img/keisaiguide/img_feature_03_sp.png" alt="特徴03"></figure><span class="heading--flex__txt">オプションなしで求人の上位表示OK！</span>
              </h3>
              <div class="feature-item__contents u-mt30">
                <p class="feature-item__txt">クローリングでコールセンター求人の一括検索ができる当サイト。現在、約1万件の求人が掲載されていますが、「オリジナル求人」のお申し込みで優先的に上位に表示が可能です！</p>
                <div class="feature-item__img u-mt30"><img src="../img/keisaiguide/img_feature_03_01_sp.png" alt="求人検索結果の画面"></div>
              </div>
            </div><a class="btn--contact u-mt40" href="/keisaiguide/contact"><img src="../img/keisaiguide/txt_btn_contact01.svg" alt="資料請求・お問い合わせはこちら"></a>
          </div>
        </section>