        <header class="heading">
          <div class="heading__inner">
            <div class="heading--left">
              <h1 class="heading__ttl u-fwb">
                <svg class="heading__logo" role="image">
                  <title>コールナビ</title>
                  <use xlink:href="../img/svg/symbols.svg#logo"></use>
                </svg>求人広告掲載ガイド
              </h1>
            </div>
            <div class="heading__right">
              <figure class="heading__tell"><img src="../img/keisaiguide/tellnumber.png" alt="03-6367-2669 9:00~18:00 (定休日:土日祝日)"></figure>
            </div>
          </div>
        </header>
        <div class="top">
          <figure class="top_main_img"><img src="../img/keisaiguide/top_main.png" alt="コールセンター特化型求人サイト 「コールナビ」完全成果報酬型だから採用できるまで完全無料！"></figure>
        </div>