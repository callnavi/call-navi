      <h1 class="section__heading u-tac u-mt90"><span class="section__heading__lead-txt font-lato">CONTACT</SPan>資料請求・お問い合わせ</h1>
      <article class="article u-fc--txt--contact">
        <section class="section">
          <div class="section__inner">
            <h2 class="u-fs--xl u-mt10 u-tac u-fwb">送信完了</h2>
            <p class="u-fs--l u-mt40 u-tac">お問い合わせありがとうございました。<br>担当者から２営業日以内で対応させていただきますが、<br>内容によってはお時間いただく場合もございます。<br>ご了承頂けますよう、宜しくお願いいたします。</p>
            <div class="btn-wrap u-mt90"><a class="btn--secondary" href="/">TOPへ戻る</a></div>
          </div>
        </section>
      </article>