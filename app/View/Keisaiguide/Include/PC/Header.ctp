      <header class="heading--contact">
        <div class="heading__inner">
          <div class="heading--left">
            <div class="heading__ttl u-fwb">
              <svg class="heading__logo" role="image">
                <title>コールナビ</title>
                <use xlink:href="../../img/svg/symbols.svg#logo"></use>
              </svg>求人広告掲載ガイド
            </div>
          </div>
          <div class="heading__right">
            <figure class="heading__tell"><img src="../../img/keisaiguide/tellnumber.png" alt="03-6367-2669 9:00~18:00 (定休日:土日祝日)"></figure>
          </div>
        </div>
      </header>