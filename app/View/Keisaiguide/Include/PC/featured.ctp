        <section class="section--bg-beige u-tac" id="feature">
          <div class="section__inner">
            <h3 class="section__heading u-tac"><span class="section__heading__lead-txt font-lato">FEATURE</span>コールナビの特徴</h3>
            <div class="feature-column">
              <h4 class="feature-column__heading u-tal u-fwb"><img class="feature-column__heading-icon" src="../img/keisaiguide/feature_01.png" alt="特徴1">リスクの少ない完全報酬プラン！採用が決まるまで0円！</h4>
              <p class="feature-column__description u-tal u-fs--s">広告費用の掛け捨てリスクなし！初期コスト0円で確実に採用されたいお客様にピッタリな「完全成果報酬プラン」をご用意しています。採用が決まる<br>まで費用は一切かかりません。</p>
              <figure class="feature01-img"><img src="../img/keisaiguide/feature01_top_img.png" alt="初期費用・掲載料・デポジットなし！ 採用が決まるまで完全無料！0円で求人掲載可能！ コールナビは安心の定額制"></figure>
              <div class="feature-tbl u-tac">
                <table>
                  <colgroup>
                    <col class="col-1" span="1" width="200px">
                    <col class="col-2" span="3" width="300px">
                  </colgroup>
                  <thead>
                    <tr class="feature__tr--primary">
                      <th class="empty"></th>
                      <th>
                        <div class="feature__th--secondary radius--left-top u-tac">掲載課金型</div>
                      </th>
                      <th>
                        <div class="feature__th--primary u-tac">
                          <svg class="feature__logo" role="image">
                            <title>コールナビ</title>
                            <use xlink:href="../img/svg/symbols.svg#logo"></use>
                          </svg>オリジナル求人
                        </div>
                      </th>
                      <th>
                        <div class="feature__th--secondary radius--right-top u-tac">人材紹介型</div>
                      </th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr class="feature__tr--secondary">
                      <th class="radius--left-top u-fs--l u-tac">掛け捨てリスク<span class="drawing-line"></span></th>
                      <td class="feature__td u-fs--s u-pt30">×
                        <div>採用に至らなくても広告費用が発生。<span class="drawing-line"></span></div>
                      </td>
                      <td class="feature__td u-fs--s color--primary side-border--primary u-pt30">○
                        <div>採用決定まで無料。<span class="drawing-line"></span></div>
                      </td>
                      <td class="feature__td u-fs--s u-pt30">○
                        <div>採用決定まで無料。<span class="drawing-line"></span></div>
                      </td>
                    </tr>
                    <tr class="feature__tr--secondary">
                      <th class="feature__th u-fs--l u-tac radius--left-bottom">採用コスト</th>
                      <td class="feature__td u-fs--s u-pt20">○
                        <div class="u-tal">広告費のみでOKのため、複数名の採用時には低コストで採用が可能。</div>
                      </td>
                      <td class="feature__td u-fs--s color--primary sidedowm-border--primary u-pt20"> ○
                        <div class="u-tal">成果報酬で掛け捨てリスクも少ないのに、採用費用も安価。プランによっては正社員でも年収の10%程度で採用可能。</div>
                      </td>
                      <td class="feature__td u-fs--s radius--right-bottom u-pt20">×
                        <div class="u-tal">費用は採用者の年収によって左右されるため、優秀な人材になるほど高額になりやすい。</div>
                      </td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <p class="section__concept u-fwb u-fs--xxl">「成果報酬型求人は高くてなかなか手が出せない…」という<br>企業様もコールナビなら低コストで採用が可能！</p>
            </div>
            <div class="feature-column">
              <h4 class="feature-column__heading u-tal u-fwb"><img class="feature-column__heading-icon" src="../img/keisaiguide/feature_02.png" alt="特徴2">ユーザーは「コールセンターに興味を持っている層」ばかり！</h4>
              <p class="feature-column__description u-tal u-fs--s">コールナビはコールセンター特化型の求人サイトであるため、訪問しているサイトユーザーもコールセンターに興味を持っている層が多くを占めます。<br>読み物コンテンツも多数揃えており、企業インタビューコンテンツも無料で作成・公開が可能です。</p>
              <figure class="feature02-img u-mt55"><img src="../img/keisaiguide/feature02_column_img.png" alt="相互リンクで採用率・集客効果UP!" width="908" height="498"></figure>
              <p class="section__concept u-fwb u-fs--xxl">求人ページからの訴求のみではなく、<br>多角的にユーザーへ訴求が可能になっているのも特徴です！</p>
            </div>
            <div class="feature-column">
              <h4 class="feature-column__heading u-tal u-fwb"><img class="feature-column__heading-icon" src="../img/keisaiguide/feature_03.png" alt="特徴3">オプションなしで求人の上位表示OK！</h4>
              <p class="feature-column__description u-tal u-fs--s">クローリングでコールセンター求人の一括検索ができる当サイト。現在、約1万件の求人が掲載されていますが、「オリジナル求人」のお申し込みで優先<br>的に上位に表示が可能です！</p>
              <figure class="feature02-img u-mt55 u-mb55"><img src="../img/keisaiguide/feature03_column_img.png" alt="求人の上位表示の例" width="907" height="406"></figure>
              <p class="contact-btn--primary"><a class="contact-btn--primary__inner" href="/keisaiguide/contact"><img class="btn--primary__img" src="../img/keisaiguide/txt_btn_contact01.svg" alt="資料請求・お問い合わせはこちら"></a></p>
            </div>
          </div>
        </section>