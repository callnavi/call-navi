        <section class="section u-tac" id="flow">
          <div class="section__inner">
            <h3 class="section__heading u-tac"><span class="section__heading__lead-txt font-lato">FLOW</span>ご契約までの流れ</h3>
            <div class="flow-box u-tac u-mt50">
              <div class="flow-box__inner"><span class="fa fa-caret-right"></span>
                <div class="flow-box__icon u-mt30">
                  <div class="flow-box__icon__inner">
                    <svg class="flow-icon" role="image" width="58px" height="45px">
                      <use xlink:href="../img/svg/symbols.svg#icon_mail"></use>
                    </svg>
                  </div>
                </div>
                <div class="flow-box__txt u-fwb">お問い合わせ
                  <p class="u-fs--s u-tal u-mt15">まずは、資料請求やお問い合わせでご連絡ください。お客様のご要望などをお聞きいたします。</p>
                </div>
              </div>
              <div class="flow-box__inner"><span class="fa fa-caret-right"></span>
                <div class="flow-box__icon u-mt30">
                  <div class="flow-box__icon__inner">
                    <svg class="flow-icon" role="image" width="70px" height="70px">
                      <use xlink:href="../img/svg/symbols.svg#icon_light"></use>
                    </svg>
                  </div>
                </div>
                <div class="flow-box__txt u-fwb">ご提案
                  <p class="u-fs--s u-tal u-mt15">ご要望に合わせて最適なプランをご提案いたします。</p>
                </div>
              </div>
              <div class="flow-box__inner"><span class="fa fa-caret-right"></span>
                <div class="flow-box__icon u-mt30">
                  <div class="flow-box__icon__inner">
                    <svg class="flow-icon" role="image" width="45px" height="51px">
                      <use xlink:href="../img/svg/symbols.svg#icon_memo"></use>
                    </svg>
                  </div>
                </div>
                <div class="flow-box__txt u-fwb">お申し込み
                  <p class="u-fs--s u-tal u-mt15">申し込み手続き完了後、掲載する情報を作成する求人原稿作成フォーマットをお送りし、ご記入いただきます。</p>
                </div>
              </div>
              <div class="flow-box__inner">
                <div class="flow-box__icon u-mt30">
                  <div class="flow-box__icon__inner">
                    <svg class="flow-icon" role="image" width="51px" height="45px">
                      <use xlink:href="../img/svg/symbols.svg#icon_memo02"></use>
                    </svg>
                  </div>
                </div>
                <div class="flow-box__txt u-fwb">ご掲載
                  <p class="u-fs--s u-tal u-mt15">インタビュー記事や募集要項など最終確認後、掲載スタートとなります。</p>
                </div>
              </div>
            </div>
          </div>
        </section>