        <section class="section--bg-beige u-tac" id="faq">
          <div class="section__inner">
            <h3 class="section__heading u-tac"><span class="section__heading__lead-txt font-lato">FAQ</span>よくある質問</h3>
            <dl class="faq-list u-tal">
              <dt class="question u-fwb u-mt45">求人の掲載について質問です。費用が発生するのは、本当に採用できた時のみですか？</dt>
              <dd class="answer u-fs--s u-mb45">
                <div class="answer-txt">完全成果報酬ですので、採用されるまでは一切費用はかかりません。</div>
              </dd>
            </dl>
            <dl class="faq-list u-tal">
              <dt class="question u-fwb u-mt45">掲載の職種に制限はありますか？</dt>
              <dd class="answer u-fs--s u-mb45">
                <div class="answer-txt">コールセンター特化型の求人サイトになりますので、コールセンター業務のご掲載をお願いしております。</div>
              </dd>
            </dl>
            <dl class="faq-list u-tal">
              <dt class="question u-fwb u-mt45">申し込みからどのくらいで掲載できますか？</dt>
              <dd class="answer u-fs--s u-mb45">
                <div class="answer-txt">必要書類のご提出後、5営業日程度で原稿作成させていただきます。内容のご確認後、掲載開始となります。</div>
              </dd>
            </dl>
            <dl class="faq-list u-tal">
              <dt class="question u-fwb u-mt45">求人の掲載期間はどれくらいですか？</dt>
              <dd class="answer u-fs--s u-mb45">
                <div class="answer-txt">完全成果報酬型ですので、掲載期間に定めはございません。ご希望されている期間分、掲載が可能です。求人の取り下げもご希望に合わせて切替可能です。</div>
              </dd>
            </dl>
            <dl class="faq-list u-tal">
              <dt class="question u-fwb u-mt45">求人の掲載数に制限はありますか？</dt>
              <dd class="answer u-fs--s">
                <div class="answer-txt">掲載数の制限はありません。ご希望される分、ご掲載が可能です。</div>
              </dd>
            </dl>
          </div>
        </section>