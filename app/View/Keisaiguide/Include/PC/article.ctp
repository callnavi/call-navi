        <section class="section u-tac">
          <div class="section__inner">
            <h2 class="u-fwb u-fs--xxl">コールセンター特化型求人サイト「コールナビ」は、<br>完全成果報酬型求人サイトです。採用されるまで費用は一切かかりません。</h2>
            <P class="u-fs--m u-mt40 u-mb60">採用決定して初めて料金が発生する料金メニューとなっていますので、求人広告費用の掛け捨てリスクもナシ。<br>採用コストがかさんでお困りの企業様、この機会にぜひ「コールナビ」のオリジナル求人をお試しください！</P>
            <p class="contact-btn--primary"><a class="contact-btn--primary__inner" href="/keisaiguide/contact"><img class="btn--primary__img" src="../img/keisaiguide/txt_btn_contact01.svg" alt="資料請求・お問い合わせはこちら"></a></p>
          </div>
        </section>