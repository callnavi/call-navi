      <nav class="nav" data-height="nav">
        <ul class="nav__inner" data-target-menu="keisaiguide">
          <li class="nav__item u-tac" data-target-nav="1"><a class="nav__item__inner" href="#feature" data-trigger-scroll="anchor">
              <p class="font-lato">FEATURE</p><span class="u-fs--s">コールナビの特徴</span></a></li>
          <li class="nav__item u-tac" data-target-nav="2"><a class="nav__item__inner" href="#flow" data-trigger-scroll="anchor">
              <p class="font-lato">FLOW</p><span class="u-fs--s">ご契約までの流れ</span></a></li>
          <li class="nav__item u-tac" data-target-nav="3"><a class="nav__item__inner" href="#faq" data-trigger-scroll="anchor">
              <p class="font-lato">FAQ</p><span class="u-fs--s">よくある質問</span></a></li>
          <li class="nav__item--black u-tac"><a class="nav__item__inner nav__contact nev__contact__svg font-lato" href="/keisaiguide/contact">
              <svg class="nav__contact__img" role="image">
                <use xlink:href="../img/svg/symbols.svg#icon_mail"></use>
              </svg>資料請求・お問い合わせ</a></li>
        </ul>
      </nav>