<?php
	Configure::load('contact', 'default');
    //Set load config/contact data
    $this->set('title', Configure::read('contact.title'));
	
	$this->start('meta');
		//meta description keywords image
        echo '<meta name="description" content="'. Configure::read('contact.keisaiguide_content') .'">';
        echo '<meta name="keyword" content="'. Configure::read('contact.keyword') .'">';         
		echo Configure::read('webconfig.apple_touch_icon_precomposed');
		echo Configure::read('webconfig.ogp');  
	$this->end('meta');
?>
<?php
$this->start('css');
echo $this->Html->css('appkeisai_new-sp');
echo $this->Html->css('commonkeisai_v2_sp');
$this->end('css');
?>
<?php  $this->start('script');?>
<?php echo $this->Html->script('app_new_contact'); ?>
<?php echo $this->Html->script('commonkeisai_v2_sp'); ?>
<?php $this->end('script'); ?>
    <div data-trigger-menu="keisaiguide">

        <?php echo $this->element('../Keisaiguide/Include/SP/header_keisai'); ?>
    </div>
        <?php echo $this->element('../Keisaiguide/Include/SP/nav'); ?>
      <article class="article u-fc--txt">
        <?php echo $this->element('../Keisaiguide/Include/SP/article'); ?>
        <?php echo $this->element('../Keisaiguide/Include/SP/featured'); ?>
        <?php echo $this->element('../Keisaiguide/Include/SP/flow'); ?>
        <?php echo $this->element('../Keisaiguide/Include/SP/faq'); ?>
        <?php echo $this->element('../Keisaiguide/Include/SP/btn_bottom'); ?>


      </article>
        <?php echo $this->element('../Keisaiguide/Include/SP/Footer'); ?>
