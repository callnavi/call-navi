<?php
	
    Configure::load('contact', 'default');
    //Set load config/Contact data
    $this->set('title', Configure::read('contact.title_contact'));
    
    $this->start('meta');
        //meta description keywords image
        echo '<meta name="description" content="'. Configure::read('contact.des_contact') .'">';
        echo '<meta name="keyword" content="'. Configure::read('contact.keyword_contact') .'">';         
        echo Configure::read('webconfig.apple_touch_icon_precomposed');
        echo Configure::read('webconfig.ogp');  
    $this->end('meta');
?>
<?php
$this->start('css');
echo $this->Html->css('app_new');
$this->end('css');
?>
<?php  $this->start('script');?>
<?php echo $this->Html->script('app_new_contact'); ?>

<?php $this->end('script'); ?>
        <?php echo $this->element('../Keisaiguide/Include/PC/Header'); ?>
        <?php echo $this->element('../Keisaiguide/Include/PC/Form'); ?>
        <?php echo $this->element('../Keisaiguide/Include/PC/Footer'); ?>