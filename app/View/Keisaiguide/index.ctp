<?php
	Configure::load('contact', 'default');
    //Set load config/contact data

	$this->set('title', Configure::read('contact.title'));
	
	$this->start('meta');
		//meta description keywords image
		echo '<meta name="description" content="'. Configure::read('contact.keisaiguide_content') .'">';
        echo '<meta name="keyword" content="'. Configure::read('contact.keyword') .'">';        
		echo Configure::read('webconfig.apple_touch_icon_precomposed');
		echo Configure::read('webconfig.ogp');  
	$this->end('meta');
?>
<?php
$this->start('css');
echo $this->Html->css('app_new');
$this->end('css');
?>
<?php  $this->start('script');?>
<?php echo $this->Html->script('app_new_contact'); ?>

<?php $this->end('script'); ?>
    <div data-trigger-menu="keisaiguide">
        <?php echo $this->element('../Keisaiguide/Include/PC/header_keisai'); ?>
    </div>
        <?php echo $this->element('../Keisaiguide/Include/PC/nav'); ?>
      <article class="article">
        <?php echo $this->element('../Keisaiguide/Include/PC/article'); ?>
        <?php echo $this->element('../Keisaiguide/Include/PC/featured'); ?>
        <?php echo $this->element('../Keisaiguide/Include/PC/flow'); ?>
        <?php echo $this->element('../Keisaiguide/Include/PC/faq'); ?>
        <?php echo $this->element('../Keisaiguide/Include/PC/btn_bottom'); ?>


      </article>
        <?php echo $this->element('../Keisaiguide/Include/PC/Footer'); ?>