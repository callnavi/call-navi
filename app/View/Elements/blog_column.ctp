<?php $data = $this->requestAction('elements/list_blog_view'); ?>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding sliderbottom">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding slider-header" >
        <h1>新着 ブログ＆コラム</span></h1>
<!--         <h2>ブログ＆コラム</h2> -->
    </div>

    <?php
    if($data){
    foreach($data as $item){
        $CreateDate = new DateTime($item['Blogs']['date_post']);
        $link = $this->Html->url(array('controller' => 'blog', 'action' => 'detail',"category_alias"=>$item['blogs_category']['category_alias'], 'id'=>$item['Blogs']['id']), true);

        ?>
        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding sliderBlogimg">
            <a href="<?php echo $link;?>">
            	<?php if ($item['Blogs']['pic']): ?>
            	<img src="<?php echo $this->webroot."upload/blogs/".$item['Blogs']['pic'] ;?>" />
            	<?php else: ?>
            	<img src="" />
            	<?php endif; ?>
            </a>
        </div>
        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-padding sliderBlogtext blog">
            <a href="<?php echo $link;?>"><h4><?php echo mb_strlen($item['Blogs']['title'])> 30 ? mb_substr(  $item['Blogs']['title'],0,30,'UTF-8')."..." : $item['Blogs']['title'] ;?></h4></a>
        </div>
        <!-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding sliderborder"></div> -->
    <?php }    }?>
    <!-- <button class="buttonslider"><a href="<?php echo  $this->Html->url(array('controller' => 'blog', 'action' => 'index'));?>">+ more</a> </button> -->

</div>