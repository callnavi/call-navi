<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.7&appId=1579537585628485";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<footer class="<?php echo $this->params['controller'];?>">
<div class="footer__adv--textArea">
	<div class="container ipad-padding-sub">
	<h2 class="footer__adv--p">コールセンター求人数日本No.1サイト「コールナビ」<br />
最新の求人情報とお役立ち記事が満載のコールセンター特化型求人サイトです。勤務地、職種、雇用形態からあなたにあったワークスタイルの求人検索が可能です。また、在宅ワークや様々な条件、不安や悩み解消に役立つ情報は充実！コールセンター求人を探すならコールナビ！</h2>
	</div>
</div>
<div class="footer__gray-bg">
	<div class="no-padding container ipad-padding-sub">
		<div class="footer__left2">
			<div class="footer-grid mg-top-30 mg-bottom-30">
				<div class="footer__menu">
					<div><a class="foot-logo" href="/"><img src="/img/footer_logo.png" alt=""></a></div>
				</div>
				<div class="footer__menu">
					<div>
						<a class="fa-icon fa-facebook-icon" href="https://www.facebook.com/login.php?next=https%3A%2F%2Fwww.facebook.com%2Fshare.php%3Fu%3Dhttp%253A%252F%252Fcallnavi.jp%252F&amp;display=popup">
							<i class="fa fa-facebook" aria-hidden="true"></i>
						</a>
						<a class="fa-icon fa-twitter-icon" href="https://twitter.com/intent/tweet?url=http%3A%2F%2Fcallnavi.jp%2F&amp;text=%E3%82%B3%E3%83%BC%E3%83%AB%E3%82%BB%E3%83%B3%E3%82%BF%E3%83%BC%E6%B1%82%E4%BA%BA%E3%81%BE%E3%81%A8%E3%82%81%E3%83%A1%E3%83%87%E3%82%A3%E3%82%A2%EF%BD%9C%E3%82%B3%E3%83%BC%E3%83%AB%E3%83%8A%E3%83%93&amp;original_referer=">
							<i class="fa fa-twitter" aria-hidden="true"></i>
						</a>
					</div>
				</div>
			</div>
			<div class="border-gray-divide"></div>
		</div>
		<div class="footer__left2">
			<div class="footer-grid mg-top-25 mg-bottom-25">
				<div class="footer-search">
					<div class="footer-left-search">
						人気エリアから探す
					</div>
					<div class="footer-right-search">
						<ul class="footer__menu__list ">
							<li><a href="/search/関西">関西</a></li>
							<li><a href="/search/関東">関東</a></li>
							<li><a href="/search/北海道">北海道</a></li>
							<li><a href="/search/宮城県">宮城県</a></li>
							<li><a href="/search/新潟県">新潟県</a></li>
							<li><a href="/search/東京都">東京都</a></li>
							<li><a href="/search/埼玉県">埼玉県</a></li>
							<li><a href="/search/神奈川県">神奈川県</a></li>
							<li><a href="/search/千葉県">千葉県</a></li>
							<li><a href="/search/愛知県">愛知県</a></li>
							<li><a href="/search/大阪府">大阪府</a></li>
							<li><a href="/search/兵庫県">兵庫県</a></li>
							<li><a href="/search/京都府">京都府</a></li>
							<li><a href="/search/広島県">広島県</a></li>
							<li><a href="/search/高知県">高知県</a></li>
							<li><a href="/search/徳島県">徳島県</a></li>
							<li><a href="/search/福岡県">福岡県</a></li>
							<li><a href="/search/熊本県">熊本県</a></li>
							<li><a href="/search/沖縄県">沖縄県</a></li>
						</ul>
					</div>
				</div>
				<div class="footer-search">
					<div class="footer-left-search">
						人気キーワードから探す
					</div>
					<div class="footer-right-search">
						<ul class="footer__menu__list ">
							<li><a href="/search/在宅ワーク">在宅ワーク</a></li>
							<li><a href="/search/服装髪型自由">服装髪型自由</a></li>
							<li><a href="/search/フリーター歓迎">フリーター歓迎</a></li>
							<li><a href="/search/即日勤務OK">即日勤務OK</a></li>
							<li><a href="/search/駅近">駅近</a></li>
							<li><a href="/search/日払い">日払い</a></li>
							<li><a href="/search/主婦歓迎">主婦歓迎</a></li>
							<li><a href="/search/転勤なし">転勤なし</a></li>
							<li><a href="/search/短期">短期</a></li>
							<li><a href="/search/登録制">登録制</a></li>
							<li><a href="/search/入社祝い金">入社祝い金</a></li>
							<li><a href="/search/交通費支給">交通費支給</a></li>
						</ul>
					</div>
				</div>
				<div class="footer-search">
					<div class="footer-left-search">
						雇用形態
					</div>
					<div class="footer-right-search">
						<ul class="footer__menu__list ">
							<li><a href="/search/正社員">正社員</a></li>
							<li><a href="/search/アルバイト">アルバイト</a></li>
							<li><a href="/search/パート">パート</a></li>
							<li><a href="/search/契約社員">契約社員</a></li>
							<li><a href="/search/派遣">派遣</a></li>
						</ul>
					</div>
				</div>
				<div class="footer__advertisement">
					<div class="footer__advertisement__item"><a href="/pretty"><img src="/img/pretty.png"/></a></div>
					<div class="footer__advertisement__item"><a href="/callcenter_matome"><img src="/img/map.png"/></a></div>
					<div class="footer__advertisement__item">
						<img src="/img/sns.png"/>
						<div class="fb-like" data-href="https://callnavi.jp" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
					</div>
				</div>
			</div>
			<div class="border-gray-divide"></div>	
		</div>

		
		<div class="footer__left2">
			<div class="footer-grid mg-top-25 mg-bottom-10">
				<div class="footer__menu">
					<div class="footer__menu__header">
						Top Menu
					</div>
					<ul class="footer__menu__list">
						<li><a href="/search">コールセンター求人検索</a></li>
						<li><a href="/secret">オリジナル求人</a></li>
                        <li><a href="/oiwaikin">お祝い金について</a></li>
						<li><a href="/callcenter_matome">日本コールセンターまとめ</a></li>
						<li><a href="/concierge">コンシェルジュ</a></li>
						<li><a href="/pretty">コールセンター美男美女図鑑</a></li>
					</ul>
				</div>

				<div class="footer__menu">
					<div class="footer__menu__header">
						Contents
					</div>
					<ul class="footer__menu__list">
						<li><a href="/contents/働く人の声">働く人の声</a></li>
						<li><a href="/contents/スキル・テクニック">スキル・テクニック</a></li>
						<li><a href="/contents/ビジネス・キャリア">ビジネス・キャリア</a></li>
						<li><a href="/contents/採用・面接">採用・面接</a></li>
						<li><a href="/contents/注目トピックス">注目トピックス</a></li>
						<li><a href="/contents/エリア特集">エリア特集</a></li>
					</ul>
				</div>	

				<div class="footer__menu">
					<div class="footer__menu__header">
						Blog&Column
					</div>
					<ul class="footer__menu__list">
						<li><a href="/blog/jinji">美人事タニたん秘密の人事録</a></li>
						<li><a href="/blog/edit">コールナビ編集部のブログ</a></li>
						<li><a href="/blog/supervisor">SV Naomiの日記</a></li>
						<li><a href="/blog/kikikanri">SNS時代のリスク対策術</a></li>
						<li><a href="/blog/trainer">トップ営業マンの育て方</a></li>
						<li><a href="/blog/se">注目！コールシステム最前線</a></li>
					</ul>
				</div>		

				<div class="footer__menu">
					<div class="footer__menu__header">
						Company
					</div>
					<ul class="footer__menu__list">
						<li><a href="/about">コールナビ</a></li>
						<li><a href="/company">運営会社</a></li>
						<li><a href="/company">プライバシーポリシー</a></li>
						<li><a href="/contact">お問い合わせ</a></li>
						<li><a href="/keisaiguide">求人広告をお考えの企業様へ</a></li>
					</ul>
				</div>
			</div>		
		</div>
	</div>
	<div class="border-gray-divide"></div>
</div>
<div class="text-center mg-top-30 padding-bottom-30">
	<span>© 2015 CALL Navi Inc.</span>
</div>
</footer>