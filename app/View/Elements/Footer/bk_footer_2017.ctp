<footer class="padding-tb-1 <?php echo $this->params['controller'];?>">
<div class="footer__adv--textArea">
	<h2 class="footer__adv--p">コールセンター求人数日本No.1サイト「コールナビ」<br />
最新の求人情報とお役立ち記事が満載のコールセンター特化型求人サイトです。勤務地、職種、雇用形態からあなたにあったワークスタイルの求人検索が可能です。また、在宅ワークや様々な条件、不安や悩み解消に役立つ情報は充実！コールセンター求人を探すならコールナビ！</h2>
</div>
<div class="footer__gray-bg">
	<div class="no-padding container ipad-padding-sub">
		
		<div class="pull-left footer__left1 text-center">
			<div class="top-part mg-top-70">
				<a class="foot-logo" href="/"><img src="/img/logo_trans.png" alt=""></a>
			</div>
			<div class="bottom-part mg-top-20 mg-bottom-70">
				<a class="fa-icon" href="https://www.facebook.com/login.php?next=https%3A%2F%2Fwww.facebook.com%2Fshare.php%3Fu%3Dhttp%253A%252F%252Fcallnavi.jp%252F&amp;display=popup">
					<i class="fa fa-facebook" aria-hidden="true"></i>
				</a>
				<a class="fa-icon" href="https://twitter.com/intent/tweet?url=http%3A%2F%2Fcallnavi.jp%2F&amp;text=%E3%82%B3%E3%83%BC%E3%83%AB%E3%82%BB%E3%83%B3%E3%82%BF%E3%83%BC%E6%B1%82%E4%BA%BA%E3%81%BE%E3%81%A8%E3%82%81%E3%83%A1%E3%83%87%E3%82%A3%E3%82%A2%EF%BD%9C%E3%82%B3%E3%83%BC%E3%83%AB%E3%83%8A%E3%83%93&amp;original_referer=">
					<i class="fa fa-twitter" aria-hidden="true"></i>
				</a>
			</div>
		</div>
		
		
		<div class="pull-left footer__left2">
				<div class="footer-grid mg-top-20">
					<div class="footer__menu">
						<div class="footer__menu__header">
							Top Menu
						</div>
						<ul class="footer__menu__list">
							<li><a href="/search">コールセンター求人検索</a></li>
							<li><a href="/secret">シークレット求人</a></li>
							<li><a href="/callcenter_matome">日本コールセンターまとめ</a></li>
							<li><a href="/concierge">コンシェルジュ</a></li>
							<li><a href="/pretty">コールセンター美男美女図鑑</a></li>
						</ul>
					</div>

					<div class="footer__menu">
						<div class="footer__menu__header">
							Contents
						</div>
						<ul class="footer__menu__list">
							<li><a href="/contents/働く人の声">働く人の声</a></li>
							<li><a href="/contents/スキル・テクニック">スキル・テクニック</a></li>
							<li><a href="/contents/ビジネス・キャリア">ビジネス・キャリア</a></li>
							<li><a href="/contents/採用・面接">採用・面接</a></li>
							<li><a href="/contents/注目トピックス">注目トピックス</a></li>
							<li><a href="/contents/エリア特集">エリア特集</a></li>
						</ul>
					</div>	

					<div class="footer__menu">
						<div class="footer__menu__header">
							Blog&Column
						</div>
						<ul class="footer__menu__list">
							<li><a href="/blog/jinji">美人事タニたん秘密の人事録</a></li>
							<li><a href="/blog/edit">コールナビ編集部のブログ</a></li>
							<li><a href="/blog/supervisor">SV Naomiの日記</a></li>
							<li><a href="/blog/kikikanri">SNS時代のリスク対策術</a></li>
							<li><a href="/blog/trainer">トップ営業マンの育て方</a></li>
							<li><a href="/blog/se">注目！コールシステム最前線</a></li>
						</ul>
					</div>		

					<div class="footer__menu">
						<div class="footer__menu__header">
							Company
						</div>
						<ul class="footer__menu__list">
							<li><a href="/about">コールナビ</a></li>
							<li><a href="/company">運営会社</a></li>
							<li><a href="/company">プライバシーポリシー</a></li>
							<li><a href="/keisaiguide">お問い合わせ</a></li>
						</ul>
					</div>
				</div>		
		</div>
	</div>
</div>
<div class="text-center mg-top-30 mg-bottom-30">
	<span>© 2015 CALL Navi Inc.</span>
</div>
</footer>