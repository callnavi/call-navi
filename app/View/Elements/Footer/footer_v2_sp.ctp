<div class="f-wp df-padding">
	<div class="go-to-top">
		<a href="#" class="btn-go-top-top func-go-to-top"><span class="btn-arrow-up"></span></a>
	</div>
	<ul class="f-menu mg-top-15">
		<li>
			<span class="icon-mail"></span>
			<a href="/contact">お問い合わせ</a> <a>|</a> <a href="/keisaiguide">企業の方はこちら</a>
		</li>
		<li class="mg-top-30">
			<span class="icon-logo"></span>
			<a href="/about">コールセンター総合情報サイトについて</a>
		</li>
		<li class="mg-top-30">
			<span class="icon-company"></span>
			<a href="/company">運営会社 <small>|</small> プライバシーポリシー</a>
		</li>
	</ul>
	<div class="copy-right text-center">
		<span>© 2015 CALL Navi Inc.</span>
	</div>
</div>