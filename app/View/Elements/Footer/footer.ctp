
<div id="footer" class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding <?php echo $this->params['controller'];?>">
	<div id="" class="bg-white">
		<div class="container no-padding">
			<div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 footerleft">
				<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 footerlink">
					<h3>メニュー</h3>
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 footerlinkleft no-padding">
						<a href="/search"><p>求人まとめ</p></a>
						<a href="/callcenter_matome"><p>日本コールセンターまとめ</p></a>
						<a href="/blog"><p>ブログ&コラム</p></a>
					</div>
					<div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 padding-10 footerlinkright">
						<a href="/contents"><p>特集記事</p></a>
						<a href="/news"><p>ニュース</p></a>
						<a href="#"><p>ランキング</p></a>
					</div>
				</div>
				<div class="col-xs-12 col-sm-6 col-md-8 col-lg-8 footerbut">
					<h3>話題のキーワード</h3>
					<p>
						<a href="/news/callcenter">コールセンター</a>
						<a href="/news/it">IT通信</a>
						<!-- <a href="/news/smartphone">スマホ</a> -->
						<a href="/news/insurance">保険</a>
						<a href="/news/electric">電力</a>
						<a href="/contents/work">働く人の声</a>
					</p>
					<p>
						<!-- <a href="/news/estate">不動産</a> -->
						<a href="/contents/skill">スキル・テクニック</a>
						<a href="/contents/saiyou">採用・面接</a>
						<a href="/contents/business">ビジネス・キャリア</a>
					</p>
					<p>
						<a href="/contents/topics">注目トピック</a>
						<a href="/contents/japanesecallcenter">エリア特集</a>
					</p>
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 footerborder">
				</div>
				<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 footertem">
					<p><a href="/keisaiguide"><img src="/img/tem1.jpg" /></a><a href="/company" style="color:#636363;">運営会社 </a><span class="gach">| </span><a href="/about" style="color:#636363;">コールセンター総合情報サイトについて　</a><span class="gach">| </span><a href="/company" style="color:#636363;">プライバシーポリシー</a></p>
				</div>
			</div>
			<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 footerright">
				<a href="/"><img src="/img/logo_footer.png" /></a>
			</div>
		</div>
	</div>
</div>