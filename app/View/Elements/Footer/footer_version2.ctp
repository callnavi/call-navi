<footer class="_footer <?php echo $this->params['controller'];?>">
	

	<div class="no-padding container ipad-padding-sub">
		<div class="pull-left left-part">



						<div class="part-header">MENU</div>
						<div class="part-content">
							<ul class="foot-menu">
								<li><a href="/search">求人まとめ</a></li>
								<li><a href="/callcenter_matome">日本コールセンターまとめ</a></li>
								<li><a href="/blog">ブログ&コラム</a></li>
							</ul>
							<ul class="foot-menu">
								<li><a href="/contents">特集記事</a></li>
								<li><a href="/secret">シークレット求人</a></li>
								<li><a href="" class="empty">&#160;</a></li>
							</ul>
							<ul class="foot-menu">
								<li><a href="/company">運営会社｜プライバシーポリシー</a></li>
								<li><a href="/about">コールナビについて</a></li>
								<li><a href="/keisaiguide">お問い合わせ</a></li>
							</ul>
						</div>
					</div>
		<div class="pull-right right-part">
			<div class="top-part">
				<a class="foot-logo" href="/"><img src="/img/logo_trans.png" alt=""></a>
				<a class="fa-icon" href="https://www.facebook.com/login.php?next=https%3A%2F%2Fwww.facebook.com%2Fshare.php%3Fu%3Dhttp%253A%252F%252Fcallnavi.jp%252F&amp;display=popup">
					<i class="fa fa-facebook" aria-hidden="true"></i>
				</a>
				<a class="fa-icon" href="https://twitter.com/intent/tweet?url=http%3A%2F%2Fcallnavi.jp%2F&amp;text=%E3%82%B3%E3%83%BC%E3%83%AB%E3%82%BB%E3%83%B3%E3%82%BF%E3%83%BC%E6%B1%82%E4%BA%BA%E3%81%BE%E3%81%A8%E3%82%81%E3%83%A1%E3%83%87%E3%82%A3%E3%82%A2%EF%BD%9C%E3%82%B3%E3%83%BC%E3%83%AB%E3%83%8A%E3%83%93&amp;original_referer=">
					<i class="fa fa-twitter" aria-hidden="true"></i>
				</a>
			</div>
			<div class="bottom-part">
				<span class="copy-right"><small>©</small> 2015 CALL Navi Inc.</span>
			</div>
		</div>
	</div>
</footer>