<!--
  Created date : 06-10-2017
  Created By : andy william
  Issues : #345 change New Header & Footer
  Use in : Domain/
-->
    <a class="common-footer__page-top func-go-to-top" href="#"></a>
      <ul class="common-footer__list">
        <li class="common-footer__list__item"><a class="footer-link" href="/about">
            <div class="footer-link__icon">
              <svg role="image" width="31" height="39">
                <use xlink:href="/img/svg/symbols.svg#icon_logoMark"></use>
              </svg>
            </div>コールナビとは</a></li>
        <li class="common-footer__list__item"><a class="footer-link" href="/company">
            <div class="footer-link__icon">
              <svg role="image" width="30" height="30">
                <use xlink:href="/img/svg/symbols.svg#icon_company"></use>
              </svg>
            </div>会社概要 ｜ プライバシーポリシー</a></li>
        <li class="common-footer__list__item"><a class="footer-link" href="/contact">
            <div class="footer-link__icon">
              <svg role="image" width="40" height="30">
                <use xlink:href="/img/svg/symbols.svg#icon_contact"></use>
              </svg>
            </div>お問い合わせ</a></li>
      </ul><small class="common-footer__copyright">&copy; 2015 CALL Navi Inc.</small>
