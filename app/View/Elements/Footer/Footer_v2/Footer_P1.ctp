<!--
// file created : 05-10-2017
// created by : andy william
// patern header 1
// use in PC -> domain/ (layout/top_2017), domain/secret (layout/single_column), domain/secret/title (layout/single_column) / , domain/oiwaikin(layout/oiwaikin_layout), domain/callcenter_matome (layout/callcenter_matome.ctp)
-->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/sdk.js#xfbml=1&version=v2.7&appId=1579537585628485";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

    <footer class="common-footer <?php echo $this->params['controller'];?>">
      <div class="common-footer__adv-txt">
        <p class="common-footer__adv-txt__inner">コールセンター求人数日本No.1サイト「コールナビ」<br>最新の求人情報とお役立ち記事が満載のコールセンター特化型求人サイトです。勤務地、職種、雇用形態からあなたにあったワークスタイルの求人検索が可能です。また、在宅ワークや様々な条件、不安や悩み解消に役立つ情報は充実！コールセンター求人を探すならコールナビ！</p>
      </div>
      <div class="common-footer__inner">
        <dl class="footer-search-box">
          <dt class="footer-search-box__ttl">人気エリアから探す</dt>
          <dd class="footer-search-box__content">
            <ul class="footer-link-list">
              <li><a href="/search/関西">関西</a></li>
              <li><a href="/search/関東">関東</a></li>
              <li><a href="/search/北海道">北海道</a></li>
              <li><a href="/search/宮城県">宮城県</a></li>
              <li><a href="/search/新潟県">新潟県</a></li>
              <li><a href="/search/東京都">東京都</a></li>
              <li><a href="/search/埼玉県">埼玉県</a></li>
              <li><a href="/search/神奈川県">神奈川県</a></li>
              <li><a href="/search/千葉県">千葉県</a></li>
              <li><a href="/search/愛知県">愛知県</a></li>
              <li><a href="/search/大阪府">大阪府</a></li>
              <li><a href="/search/兵庫県">兵庫県</a></li>
              <li><a href="/search/京都府">京都府</a></li>
              <li><a href="/search/広島県">広島県</a></li>
              <li><a href="/search/高知県">高知県</a></li>
              <li><a href="/search/徳島県">徳島県</a></li>
              <li><a href="/search/福岡県">福岡県</a></li>
              <li><a href="/search/熊本県">熊本県</a></li>
              <li><a href="/search/沖縄県">沖縄県</a></li>
            </ul>
          </dd>
        </dl>
        <dl class="footer-search-box">
          <dt class="footer-search-box__ttl">人気キーワードから探す</dt>
          <dd class="footer-search-box__content">
            <ul class="footer-link-list">
              <li><a href="/search/在宅ワーク">在宅ワーク</a></li>
              <li><a href="/search/服装髪型自由">服装髪型自由</a></li>
              <li><a href="/search/フリーター歓迎">フリーター歓迎</a></li>
              <li><a href="/search/即日勤務OK">即日勤務OK</a></li>
              <li><a href="/search/駅近">駅近</a></li>
              <li><a href="/search/日払い">日払い</a></li>
              <li><a href="/search/主婦歓迎">主婦歓迎</a></li>
              <li><a href="/search/転勤なし">転勤なし</a></li>
              <li><a href="/search/短期">短期</a></li>
              <li><a href="/search/登録制">登録制</a></li>
              <li><a href="/search/入社祝い金">入社祝い金</a></li>
              <li><a href="/search/交通費支給">交通費支給</a></li>
            </ul>
          </dd>
        </dl>
        <dl class="footer-search-box">
          <dt class="footer-search-box__ttl">雇用形態</dt>
          <dd class="footer-search-box__content">
            <ul class="footer-link-list">
              <li><a href="/search/正社員">正社員</a></li>
              <li><a href="/search/アルバイト">アルバイト</a></li>
              <li><a href="/search/パート">パート</a></li>
              <li><a href="/search/契約社員">契約社員</a></li>
              <li><a href="/search/派遣">派遣</a></li>
            </ul>
          </dd>
        </dl>
        <div class="common-footer__box--secondary">
          <div class="_box--secondary__item">
            <ul class="footer-link-list--secondary">
              <li><a href="/about">コールナビとは</a></li>
              <li><a href="/company">会社概要</a></li>
              <li><a href="/company">プライバシーポリシー</a></li>
              <li><a href="/contact">お問い合わせ</a></li>
            </ul>
            <ul class="social-list">
              <li class="social-list__item"><a class="c-icon-btn__footer" href="https://www.facebook.com/callnavi/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
              <li class="social-list__item"><a class="c-icon-btn__footer" href="https://twitter.com/callnavi" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
            </ul>
          </div><small class="_box--secondary__item--copyright">© 2015 CALL Navi Inc.</small>
        </div>
      </div>
    </footer>
