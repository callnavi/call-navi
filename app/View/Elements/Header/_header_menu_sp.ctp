<div class="nav-menu" id="nav-slide-menu">
	<div class="bg-overlay"></div>
	<div class="nav-menu-contain">
		<div class="display-table">
			<div class="table-cell nav-menu-close">
				<button id="button-slide-close" type="button" class="btn-square btn-close">
					<i></i>
					<i></i>
				</button>
			</div>
			<div class="table-cell nav-menu-list">
				<ul id="touch-menu" class="menu-list">
					<li>
						<a href="/">
							<span class="icon-home"></span>
							<span>コールナビ / HOME</span>
						</a>
					</li>
					<li>
						<a href="/search">
							<span>コールセンター求人検索</span>
							<span class="right-arrow-gray"></span>
						</a>
					</li>
					<li>
						<a href="/secret">
							<span>オリジナル求人</span>
							<span class="right-arrow-gray"></span>
						</a>
					</li>
					<li>
						<a href="/callcenter_matome">
							<span class="icon-callcenter"></span>
							<span>日本コールセンターまとめ</span>
							<span class="right-arrow-gray"></span>
						</a>
					</li>
					<li>
						<a href="/contents">
							<span class="icon-content"></span>
							<span>特集記事</span>
							<span class="right-arrow-gray"></span>
						</a>
					</li>
					<li>
						<a href="/blog">
							<span class="icon-blog"></span>
							<span>ブログ＆コラム</span>
							<span class="right-arrow-gray"></span>
						</a>
					</li>
					<li class="gray-padding-top"></li>
					<li class="gray">
						<a href="/keisaiguide">
							<span class="icon-mail"></span>
							<span>お問い合わせ</span>
						</a>
					</li>
                    
					<li class="gray">
						<a href="/about">
							<span class="icon-logo"></span>
							<span>コールセンター総合情報サイトについて</span>
						</a>
					</li>
					<li class="gray">
						<a href="/company">
							<span class="icon-company"></span>
							<span>運営会社 ｜ プライバシーポリシー</span>
						</a>
					</li>
					<li class="gray-padding-bottom"></li>
				</ul>
			</div>
		</div>
	</div>
</div>