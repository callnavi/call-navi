<div  class=" hedader-fix">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button id="menu-toggle" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar top-bar"></span>
                            <span class="icon-bar middle-bar"></span>
                            <span class="icon-bar bottom-bar"></span>
                        </button>
                        <div id="logo-img">
                            <a href="<?php echo $this->webroot; ?>" title="コールナビ｜コールセンターの総合情報サイト">
                             	<img src="/img/logo_sp.png" alt="コールナビ｜コールセンターの総合情報サイト" />
                            </a>
                        </div>
                    </div>
                    <div id="sidebar-wrapper" class="navbar-collapse no-padding" >
                        <ul class="nav navbar-nav sidebar-nav">
                            <li class=""><a href="/" title="コールナビ｜コールセンターの総合情報サイト">コールナビ</a>
                            <li><a href="/search">求人まとめ</a>
                            </li>
                            <li><a href="/secret">シークレット求人</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">ブログ&コラム<span class="glyphicon glyphicon-plus float-right"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="/blog">ブログ＆コラム 一覧</a></li>
                                    <li><a href="/blog/kikikanri">危機管理広報</a></li>
                                    <li><a href="/blog/edit">コールナビ編集部</a></li>
                                    <li><a href="/blog/trainer">トレーナーYUMI</a></li>
                                    <li><a href="/blog/supervisor">スーパーバイザーNaomi</a></li>
                                    <li><a href="/blog/se">コールセンターSE</a></li>
                                    <li><a href="/blog/jinji">人事の本音</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">特集記事<span class="glyphicon glyphicon-plus float-right"></span></a>
                                <ul class="dropdown-menu" role="menu">
                                    <li><a href="/contents">特集記事 一覧</a></li>
                                    <li><a href="/contents/work/">働く人の声</a></li>
                                    <li><a href="/contents/skill/">スキル・テクニック</a></li>
                                    <li><a href="/contents/saiyou/">採用・面接</a></li>
                                    <li><a href="/contents/business/">ビジネス・キャリア</a></li>
                                    <li><a href="/contents/topics/">注目トピックス</a></li>
                                    <li><a href="/contents/japanesecallcenter/">エリア特集</a></li>
                                </ul>
                            </li>
                            <li><a href="/callcenter_matome">日本コールセンターまとめ</a>
                            </li>
                        </ul>
                    </div>

                    <div id="overlay" class=""></div>
                    <!--/.nav-collapse -->
                </div>
                
            </nav>
            <div  class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding header">
            </div>
        </div>