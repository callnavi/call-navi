<?php 
if(strtolower($this->params['controller']) == 'secret' 
   &&
   strtolower($this->params['action']) == 'index'
  )
{
	$action_search = '/secret';
}
else
{
	$action_search = '/search';
}

$area_data = isset($area_data) ? $area_data : Configure::read('webconfig.area_data');
$_selectedArea = isset($_selectedArea) ? $_selectedArea : "";

$employment_data = isset($employment_data) ? $employment_data : Configure::read('webconfig.employment_data');
$_selectedEm  = isset($_selectedEm) ? $_selectedEm : "";

$_keyword = isset($_keyword) ? $_keyword : "";

$hotkeyList = isset($hotkeyList) ? $hotkeyList : $this->requestAction('elements/getHotKeys');

$_checkedHotkey = isset($_checkedHotkey) ? $_checkedHotkey : array();

?>

<div class="nav-search" id="nav-search">
	<div class="nav-search-wp top-search df-padding">
		<form data-seo="submit" action="<?php echo $action_search; ?>" method="get">
			<p class="mg-title-search mg-bottom-20 mg-top-120">SEARCH - コールセンター求人を再検索</p>
			<div class="display-table">
				<div class="table-cell">
					<div class="custom-text-box">
						<input data-search="area" readonly type="text" id="selectArea" placeholder="地域を選択" class="text_field select-area" value="<?php echo isset($params['area'])?$params['area']:''; ?>" data-popup="SP">
					</div>
					<input id="" class="result-area" type="hidden" name="area" value="<?php echo isset($params['list_key_area'])?$params['list_key_area']:''; ?>">
					<input type="hidden" class="data-em" name="data_em" value="<?php echo isset($params['data_em'])?$params['data_em']:0 ?>">
					<input type="hidden" class="data-step" name="data_step" value="0">
					<input type="hidden" class="data-group" name="data_group" value="0">
					<input type="hidden" class="data-province" name="data_province" value="0">
					<input type="hidden" class="data-city" name="data_city" value="0">
					<input type="hidden" class="data-town" name="data_town" value="0">
					<input type="hidden" class="event-before-open-popup" value="">
					<input type="hidden" class="event-after-close-popup" value="">
				</div>
				<div class="table-cell multiply-wp">
					<span class="multiply"></span>
				</div>
				<div class="table-cell">
					<div class="custom-text-box">
						<input data-search="em" readonly type="text" id="selectEm" name="" placeholder="雇用形態を選択" class="text_field select-em" value="<?php echo isset($params['em'])?$params['em']:''; ?>" data-popup="SP">
					</div>
				</div>
			</div>
			
			<div class="full-control mg-top-20">
				<div class="custom-text-box">
							<input data-search="keyword" type="text" name="keyword" value="<?php echo $_keyword; ?>" class="text_field" placeholder="キーワードを入力" id="text-search">
				</div>
			</div>
			
			<hr>
			
			<p class="mg-title-check mg-bottom-40">こだわり条件を選択</p>

			<div class="content_listbox">
							<?php
								$__checkedHotkey = $_checkedHotkey;
								foreach ($hotkeyList as $k=>$v){
									$id = $v['Keywords']['id'];
									$keyword = $v['Keywords']['keyword'];
									$checked = 0;
									if(in_array($id, $__checkedHotkey))
									{
										$checked = 1;
									}
									echo $this->element('Item/custom_radius_checkbox', [
										'id'		=> 'cschk_'.$id,
										'name' 		=> 'hotkey[]',
										'value'		=> $id,
										'text' 		=> $keyword,
										'checked'	=> $checked,
										'attr'		=> 'data-search="hotkey" data-value="'.$keyword.'"',
									]);
								} 
							?>
			</div>
			
			<div class="buttons-group">
				<button type="submit" class="btn-eclipse btn-eclipse-left">
					<span class="icon-search"></span>
					<br>	
					search
				</button>
				<button type="button" class="btn-eclipse btn-eclipse-right" id="btn-close-x">
					<span class="icon-close"></span>
					<br>
					close
				</button>
			</div>
		</form>
	</div>
</div>
