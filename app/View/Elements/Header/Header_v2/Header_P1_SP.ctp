<!--
  Created date : 06-10-2017
  Created By : andy william
  Issues : #345 change New Header & Footer
  Use in : use in PC -> domain/ (layout/single_column_sp), domain/secret (layout/single_column), domain/secret/title (layout/single_column_v2_sp) / , domain/oiwaikin(layout/oiwaikin_sp_layout), domain/callcenter_matome (layout/single_column_v2_callcenter.ctp)
-->
    <header class="common-header">
      <div class="common-header__inner">
        <button class="common-header__btn" id="btn-search-open" type="button"><span class="common-header__btn__icon">
            <svg role="image" width="50" height="50">
              <use xlink:href="/img/svg/symbols.svg#menu_search"></use>
            </svg></span><span class="common-header__btn__txt">search</span></button>
        <h1 class="common-header__logo"><a href="/">
            <svg role="image">
              <title>コールナビ</title>
              <use xlink:href="/img/svg/symbols.svg#logo"></use>
            </svg></a></h1>
        <button class="common-header__btn--menu" type="button" data-navi-trigger="sp-menu"><span class="common-header__btn__icon"><i></i><i></i><i></i></span><span class="common-header__btn__txt" data-navi-target="sp-menu-txt">menu</span></button>
      </div>
        <?php echo $this->element('Header/Header_v2/Header_P1_SP_SideNav');?>
    </header>