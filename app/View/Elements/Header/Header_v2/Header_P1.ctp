<?php
// file created : 05-10-2017
// created by : andy william
// patern header 1
// use in PC -> domain/ (layout/top_2017), domain/secret (layout/single_column), domain/secret/title (layout/single_column) / , domain/oiwaikin(layout/oiwaikin_layout), domain/callcenter_matome (layout/callcenter_matome.ctp)
	$area_data =  Configure::read('webconfig.area_data');
	$employment_data = Configure::read('webconfig.employment_data');
	$web_title = Configure::read('webconfig.web_title_default');
  $controller_page = $this->params['controller']; 
?>
<div id="goTop" class="header">
      <header class="common-header">
        <div class="common-header__inner">
          <h1 class="common-header__logo"><a href="/">
              <svg role="image">
                <title>コールナビ</title>
                <use xlink:href="/img/svg/symbols.svg#logo"></use>
              </svg></a></h1>
          <div class="common-header__primary-menu">
            <ul class="common-header__primary-list">
              <li class="common-header__primary-list__item"><a class="txt-link__header" href="/oiwaikin"><i class="fa fa-play-circle"></i>お祝い金について</a></li>
              <li class="common-header__primary-list__item"><a class="txt-link__header" href="/keisaiguide"><i class="fa fa-play-circle"></i>求人掲載について</a></li>
            </ul>
            <ul class="social-list--header">
              <li class="social-list__item"><a class="c-icon-btn__header" href="https://www.facebook.com/callnavi/" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
              <li class="social-list__item"><a class="c-icon-btn__header" href="https://twitter.com/callnavi" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
            </ul>
          </div>
        </div>
        <nav class="common-header__nav">
          <ul class="common-header__nav__inner">
            <li class="common-header__nav__item <?php if($controller_page == 'search') { echo('is-active'); } ?> "><a class="nav-menu" href="/search"><span class="nav-menu__txt">求人検索</span><span class="nav-menu__txt--sub">Search</span></a></li>
            <li class="common-header__nav__item <?php if($controller_page == 'secret' || $controller_page == 'apply') { echo('is-active'); } ?>"><a class="nav-menu" href="/secret"><span class="nav-menu__txt">オリジナル求人</span><span class="nav-menu__txt--sub">Original</span></a></li>
            <li class="common-header__nav__item <?php if($controller_page == 'concierge') { echo('is-active'); } ?>"><a class="nav-menu" href="/concierge"><span class="nav-menu__txt">求人コンサルタント</span><span class="nav-menu__txt--sub">Consultant</span></a></li>
            <li class="common-header__nav__item <?php if($controller_page == 'Callcenter') { echo('is-active'); } ?>"><a class="nav-menu" href="/callcenter_matome"><span class="nav-menu__txt">日本コールセンターまとめ</span><span class="nav-menu__txt--sub">Japan Call Center Summary</span></a></li>
            <li class="common-header__nav__item <?php if($controller_page == 'contents') { echo('is-active'); } ?>"><a class="nav-menu" href="#" data-navi-trigger="contents"><span class="nav-menu__txt">お役立ちコンテンツ</span><span class="nav-menu__txt--sub">Contents</span></a>
              <div class="common-header__nav__sub-menu" data-navi-target="contents">
                <ul>
                  <li><a href="/contents/企業インタビュー">企業インタビュー</a></li>
                  <li><a href="/contents/働く人の声">働く人の声</a></li>
                  <li><a href="/contents/スキル・テクニック">スキル・テクニック</a></li>
                  <li><a href="/contents/ビジネス・キャリア">ビジネス・キャリア</a></li>
                  <li><a href="/contents/採用・面接">採用・面接</a></li>
                  <li><a href="/contents/注目トピックス">注目トピックス</a></li>
                  <li><a href="/contents/エリア特集">エリア特集</a></li>
                </ul>
              </div>
            </li>
          </ul>
        </nav>
      </header>
      <?php echo $this->fetch('header_breadcrumb'); ?>
</div>

