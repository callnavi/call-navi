<!--
  Created date : 06-10-2017
  Created By : andy william
  Issues : Element/Header_v2/Header_P1_SP
  Use in : Domain/
-->

<?php 
if(strtolower($this->params['controller']) == 'secret' 
   &&
   strtolower($this->params['action']) == 'index'
  )
{
  $action_search = '/secret';
}
else
{
  $action_search = '/search';
}

$area_data = isset($area_data) ? $area_data : Configure::read('webconfig.area_data');
$_selectedArea = isset($_selectedArea) ? $_selectedArea : "";

$employment_data = isset($employment_data) ? $employment_data : Configure::read('webconfig.employment_data');
$_selectedEm  = isset($_selectedEm) ? $_selectedEm : "";

$_keyword = isset($_keyword) ? $_keyword : "";

$hotkeyList = isset($hotkeyList) ? $hotkeyList : $this->requestAction('elements/getHotKeys');

$_checkedHotkey = isset($_checkedHotkey) ? $_checkedHotkey : array();

?>
      <div class="common-header__nav-wrap" data-navi-target="sp-menu">
        <div class="bg--overlay"></div>
        <nav class="common-header__nav">
          <div class="common-header__nav__inner">
            <ul class="nav-menu-list">
              <li class="nav-menu-list__item"><a class="nav-link" href="/search"><span class="nav-link__icon">
                    <svg role="image" width="30" height="30">
                      <use xlink:href="/img/svg/symbols.svg#icon_search"></use>
                    </svg></span>求人検索</a></li>
              <li class="nav-menu-list__item"><a class="nav-link" href="/secret"><span class="nav-link__icon">
                    <svg role="image" width="24" height="32">
                      <use xlink:href="/img/svg/symbols.svg#icon_lock"></use>
                    </svg></span>オリジナル求人</a></li>
              <li class="nav-menu-list__item"><a class="nav-link" href="/concierge"><span class="nav-link__icon">
                    <svg role="image" width="24" height="34">
                      <use xlink:href="/img/svg/symbols.svg#icon_concierge"></use>
                    </svg></span>求人コンサルタント</a></li>
              <li class="nav-menu-list__item"><a class="nav-link" href="/callcenter_matome"><span class="nav-link__icon">
                    <svg role="image" width="30" height="30">
                      <use xlink:href="/img/svg/symbols.svg#icon_map"></use>
                    </svg></span>日本コールセンターまとめ</a></li>
              <li class="nav-menu-list__item"><a class="nav-link--toggle" href="#" data-navi-trigger="contents"><span class="nav-link__icon">
                    <svg role="image" width="28" height="28">
                      <use xlink:href="/img/svg/symbols.svg#icon_contents"></use>
                    </svg></span>お役立ちコンテンツ</a>
                <div class="nav-sub-menu" data-navi-target="contents">
                  <ul class="nav-sub-menu__inner">
                    <li><a class="nav-link--sub" href="/contents/企業インタビュー">企業インタビュー</a></li>
                    <li><a class="nav-link--sub" href="/contents/働く人の声/">働く人の声</a></li>
                    <li><a class="nav-link--sub" href="/contents/スキル・テクニック/">スキル・テクニック</a></li>
                    <li><a class="nav-link--sub" href="/contents/ビジネス・キャリア/">ビジネス・キャリア</a></li>
                    <li><a class="nav-link--sub" href="/contents/採用・面接/">採用・面接</a></li>
                    <li><a class="nav-link--sub" href="/contents/注目トピックス/">注目トピックス</a></li>
                    <li><a class="nav-link--sub" href="/contents/エリア特集/">エリア特集</a></li>
                  </ul>
                </div>
              </li>
            </ul>
            <div class="nav-menu--secondary">
              <ul class="nav-menu-list--secondary">
                <li class="nav-menu-list--secondary__item"><a class="nav-link--secondary" href="/oiwaikin"><span class="nav-link--secondary__icon">
                      <svg role="image" width="24" height="30">
                        <use xlink:href="/img/svg/symbols.svg#icon_present"></use>
                      </svg></span>お祝い金について</a></li>
                <li class="nav-menu-list--secondary__item"><a class="nav-link--secondary" href="/keisaiguide"><span class="nav-link--secondary__icon">
                      <svg role="image" width="24" height="28">
                        <use xlink:href="/img/svg/symbols.svg#icon_person"></use>
                      </svg></span>求人掲載について</a></li>
                <li class="nav-menu-list--secondary__item"><a class="nav-link--secondary" href="/contact"><span class="nav-link--secondary__icon">
                      <svg role="image" width="30" height="22">
                        <use xlink:href="/img/svg/symbols.svg#icon_contact"></use>
                      </svg></span>お問い合わせ</a></li>
                <li class="nav-menu-list--secondary__item"><a class="nav-link--secondary" href="/company"><span class="nav-link--secondary__icon">
                      <svg role="image" width="24" height="30">
                        <use xlink:href="/img/svg/symbols.svg#icon_logoMark"></use>
                      </svg></span>コールナビとは</a></li>
                <li class="nav-menu-list--secondary__item"><a class="nav-link--secondary" href="/company"><span class="nav-link--secondary__icon">
                      <svg role="image" width="24" height="24">
                        <use xlink:href="/img/svg/symbols.svg#icon_company"></use>
                      </svg></span>会社概要 ｜ プライバシーポリシー</a></li>
              </ul>
              <ul class="social-area">
                <li class="social-area__item"><a class="social-link" href="https://www.facebook.com/callnavi/" target="_blank"><span><i class="fa fa-facebook" aria-hidden="true"></i>facebook</span></a></li>
                <li class="social-area__item"><a class="social-link" href="https://twitter.com/callnavi" target="_blank"><span><i class="fa fa-twitter" aria-hidden="true"></i>twitter</span></a></li>
              </ul>
            </div>
          </div>
        </nav>
      </div>
      <div class="nav-search" id="nav-search">
        <div class="nav-search-wp top-search df-padding">
          <form data-seo="submit" action="<?php echo $action_search; ?>" method="get">
            <p class="mg-title-search mg-bottom-20 mg-top-120">SEARCH - コールセンター求人を再検索</p>
            <div class="display-table">
              <div class="table-cell">
                <div class="custom-text-box">
                  <input class="text_field select-area" id="selectArea" data-search="area" readonly="" type="text" placeholder="地域を選択" value="<?php echo isset($params['area'])?$params['area']:''; ?>" data-popup="SP">
                </div>
                <input class="result-area" id="" type="hidden" name="area" value="?php echo isset($params['list_key_area'])?$params['list_key_area']:''; ?>">
                <input class="data-em" type="hidden" name="data_em" value="<?php echo isset($params['data_em'])?$params['data_em']:0 ?>">
                <input class="data-step" type="hidden" name="data_step" value="0">
                <input class="data-group" type="hidden" name="data_group" value="0">
                <input class="data-province" type="hidden" name="data_province" value="0">
                <input class="data-city" type="hidden" name="data_city" value="0">
                <input class="data-town" type="hidden" name="data_town" value="0">
                <input class="event-before-open-popup" type="hidden" value="">
                <input class="event-after-close-popup" type="hidden" value="">
              </div>
              <div class="table-cell multiply-wp"><span class="multiply"></span></div>
              <div class="table-cell">
                <div class="custom-text-box">
                  <input class="text_field select-em" id="selectEm" data-search="em" readonly="" type="text" name="" placeholder="雇用形態を選択" value="<?php echo isset($params['em'])?$params['em']:''; ?>" data-popup="SP">
                </div>
              </div>
            </div>
            <div class="full-control mg-top-20">
              <div class="custom-text-box">
                <input class="text_field" id="text-search" data-search="keyword" type="text" name="keyword" value="<?php echo $_keyword; ?>" placeholder="キーワードを入力">
              </div>
            </div>
            <hr>
            <p class="mg-title-check mg-bottom-40">こだわり条件を選択</p>
            <div class="content_listbox">
              <?php
                $__checkedHotkey = $_checkedHotkey;
                foreach ($hotkeyList as $k=>$v){
                  $id = $v['Keywords']['id'];
                  $keyword = $v['Keywords']['keyword'];
                  $checked = 0;
                  if(in_array($id, $__checkedHotkey))
                  {
                    $checked = 1;
                  }
                  echo $this->element('Item/custom_radius_checkbox_v2', [
                    'id'    => 'cschk_'.$id,
                    'name'    => 'hotkey[]',
                    'value'   => $id,
                    'text'    => $keyword,
                    'checked' => $checked,
                    'attr'    => 'data-search="hotkey" data-value="'.$keyword.'"',
                  ]);
                } 
              ?>              
            </div>
            <div class="buttons-group">
              <button class="btn-eclipse btn-eclipse-left" type="submit"><span class="icon-search"></span><br>                   search</button>
              <button class="btn-eclipse btn-eclipse-right" id="btn-close-x" type="button"><span class="icon-close"></span><br>                   close</button>
            </div>
          </form>
        </div>
      </div>
