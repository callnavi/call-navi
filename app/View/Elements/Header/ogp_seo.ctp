<?php
	if($canonical)
	{
		echo $this->Html->meta('canonical', $url,
					   array('rel'=>'canonical', 'type'=>null, 'title'=>null));
	}
?>
<meta name='description' content= '<?php echo $description?>' >
<meta name='Keywords' content='<?php echo $keywords?>'>
<meta property='og:image' content='<?php echo $image?>'>

<meta property='og:type' content='article'/>
<meta property='og:site_name' content='コールナビ'/>
<meta property='og:title' content='<?php echo $title;?>'/>
<meta property='og:url' content='<?php echo $url ?>'/>
<meta property='og:description' content='<?php echo $description?>'/>

<meta name="twitter:card" content="summary" />
<meta name="twitter:site" content="<?php echo $title?>" />
<meta name="twitter:title" content="<?php echo $title?>" />
<meta name="twitter:description" content="<?php echo $description?>" />
<meta name="twitter:image" content="<?php echo $image?>" />