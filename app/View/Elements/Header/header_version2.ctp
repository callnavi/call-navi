<?php
	$area_data =  Configure::read('webconfig.area_data');
	$employment_data = Configure::read('webconfig.employment_data');
	$web_title = Configure::read('webconfig.web_title_default');
?>
<div id="goTop" class="header">
	<div class="container">
		<div class="clearfix">
			<div class="pull-left">
				<h1 class="heading-logo">
					<a href="<?php echo FULL_BASE_URL; ?>" title="<?php echo $web_title; ?>">
						<img src="/img/logo-white-shadow.png" title="<?php echo $web_title; ?>" />
					</a>
					<p>コールセンター特化型求人サイト</p>
				</h1>
				<?php echo $this->fetch('header_breadcrumb'); ?>
			</div>
			<div class="pull-right">
				<div class="head-menu text-right">
					<div id="btnHeadMenu" class="head-menu noselect">
						<span class="btn-menu"></span>メニュー
					</div>
					
					<div id="hiddenMenu" class="hidden-gray-bar hidden-gray-menu">
								<ul>
									<li><a href="/">HOME</a></li>
									<li><a href="/search">コールセンター求人検索</a></li>
									<li><a href="/secret">オリジナル求人</a></li>
									<li><a href="/contents">特集記事</a></li>
									<li><a href="/callcenter_matome">日本コールセンターまとめ</a></li>
									<li><a href="/blog">ブログ&amp;コラム</a></li>
									<li><a href="/pretty">美男美女図鑑</a></li>
                                    <li><a href="/oiwaikin">お祝い金について</a></li>
								</ul>
					</div>
					
				</div>
			</div>
		</div>
	</div>	
</div>

