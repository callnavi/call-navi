<?php
	$area_data =  Configure::read('webconfig.area_data');
	$employment_data = Configure::read('webconfig.employment_data');
	$web_title = Configure::read('webconfig.web_title_default');
?>
<div id="goTop" class="header">
	<div class="container">
		<div class="clearfix">
			<div class="pull-right">
				<div class="head-menu text-right">
					<div id="btnHeadMenu" class="head-menu noselect">
						<span class="btn-menu"></span>メニュー
					</div>
					
					<div id="hiddenMenu" class="hidden-gray-bar hidden-gray-menu">
								<ul>
									<li><a href="/">HOME</a></li>
									<li><a href="/search">コールセンター求人検索</a></li>
									<li><a href="/secret">オリジナル求人</a></li>
									<li><a href="/contents">特集記事</a></li>
									<li><a href="/callcenter_matome">日本コールセンターまとめ</a></li>
									<li><a href="/blog">ブログ&amp;コラム</a></li>
									<li><a href="/pretty">美男美女図鑑</a></li>
								</ul>
					</div>
					
				</div>
			</div>
		</div>
	</div>	
</div>

