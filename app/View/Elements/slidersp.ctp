<?php $data = $this->requestAction('elements/rankingSP'); ?>
<h1>アクセスランキング</h1>
<div id="ranking-sp" class="col-xs-12 col-sm-12 col-md-4 col-lg-4 sliderbottom slidertop">
<!--     <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding slider-header">
        <h1>ランキング</h1>
    </div> -->
    <?php 
		$count =0;
		foreach($data as $item){
			$count++;
			$type 	= $item['list']['type'];
			$title 	= $item['list']['title'];
			$image 	= $item['list']['image'];
			
			if(mb_strlen($title) > 20)
			{
				$title = mb_substr($title,0,20,'UTF-8').'...';
			}

			$cateList = [];
			$cateAias  = explode(',', $item['list']['cat_alias']);
			$cateNames = explode(',', $item['list']['cat_names']);
			$firstCateAlias = $cateAias[0];

			$idUrl = $item['list']['id'];
			$href = '#';
			switch($type)
			{
				case 'news':
					//MAKE IMAGE LINK
					if (!empty($image)){
						$pos = strpos($image, "http://");
						if($pos === false)
						{
							$image = '/upload/news/'.$image;
						}
						else
						{
							$image = $this->app->proxy_image($image);
						}
					}

					//MAKE CATEGORY
					foreach( $cateNames as $i=>$cate)
					{
						if(isset($cateAias[$i]))
						{
							$cateList[] = array(
								'href' => "/$type/".$cateAias[$i],
								'name' => $cate
							);
						}
					}
					break;
				case 'blog':
		
					//MAKE IMAGE LINK
					if (!empty($image)){
						$image = $this->webroot."upload/blogs/".$image;
					}
					
					
					//Make category
					$cateList[] = array(
						'href' => "/$type/",
						'name' => 'CCブログ'
					);
					break;
				case 'contents':
					//MAKE IMAGE LINK
					if (!empty($image)){
						$pos = strpos($image, "ccwork");
						if($pos === false)
						{
							$image= '/upload/contents/'.$image;
						}							
                    }
					
					$idUrl = $item['list']['title_alias'];

					//MAKE CATEGORY
					foreach( $cateNames as $i=>$cate)
					{
						if(isset($cateAias[$i]))
						{
							$cateList[] = array(
								'href' => "/$type/".$cateAias[$i],
								'name' => $cate
							);
						}
					}
					break;
			}

			//MAKE HREF
			$href = "/$type/$firstCateAlias/$idUrl";
	?>
        <div class="col-xs-3 col-sm-3 col-md-4 col-lg-4 no-padding sliderimg">
            <a href="<?php echo $href; ?>">
                <img src="<?php echo $image ;?>"/>
            </a>
        </div>
        <div class="col-xs-9 col-sm-9 col-md-8 col-lg-8 no-padding slidertext">
            <div class="new-label"><span><?php echo $count ;?></span></div>
            <a href="<?php echo $href; ?>">
            	<h4><?php echo $title; ?> </h4>
            </a>

           	<?php foreach ($cateList as $cat): ?>
				<a href="<?php echo $cat['href']; ?>" style="float:left;">
					<p><?php echo $cat['name']; ?></p>
				</a>
			<?php endforeach; ?>
        </div>
    <?php }?>

<!--     <button  class="col-xs-12 col-sm-12 col-md-12 col-lg-12 buttonslidermore">
        <a href="<?php echo $this->Html->url(array('controller' => 'news', 'action' => 'index'), true);?>" >READ MORE <span>もっと読む</span></a>
    </button> -->
</div>