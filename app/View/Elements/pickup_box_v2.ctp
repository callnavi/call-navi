<div class="pick-up-box_v2">
	<div class="main-list">
		<div class="item">
			<span class="i-pick-up">pick-up</span>
			<span class="i-view"><?php echo $view; ?> view</span>
			<div class="top-part">
				<a href="<?php echo $url; ?>" title="<?php echo $title; ?>">
					<img src="<?php echo $image; ?>" alt="<?php echo $title; ?>">
				</a>

			</div>
			
			<div class="i-title">
				<a href="<?php echo $url; ?>" title="<?php echo $title; ?>">
					<?php 
						if(mb_strlen($title) > 43)
						{
							echo mb_substr($title,0,43,'UTF-8').'...';
						}
						else
						{
							echo $title; 
						}
					?>
				</a>
			</div>

		</div>
	</div>
</div>