<?php
$data = $this->requestAction('elements/Blogs_info/'.$category_alias);
$data = $data[0];

$link = $this->Html->url(array('controller' => 'blog', 'action' => 'index','category_alias'=>$data['BlogsCategory']['category_alias']), true);
?>

<div class="slidertop no-padding box_blog">

  <div class="items">
    <div class="div_img">
      <a href="<?php echo $link;?>"> <img width="260" class="img_radius" src="<?php echo $this->webroot."upload/blogs-category/".$data['BlogsCategory']['pic'] ;?>" /></a>
    </div>
    <div class="info">
      <div class="avatar"><img src="<?php echo $this->webroot."upload/blogs-category/avatar/".$data['BlogsCategory']['avatar'] ;?>" /></div>
      <h3><a href="<?php echo $link;?>"><?php echo $data['BlogsCategory']['title']?></a></h3>
      <div class="short"><?php echo $data['BlogsCategory']['description']?></div>
    </div>
  </div>

</div>