<?php $data = $this->requestAction('elements/ranking'); ?>

<div class="slider-column slider-ranking">
    <div class="slider-header">
    	<div class="mg-bottom-0 list-header mg-top-30">
		<span>人気コンテンツ</span>
		</div>
    </div>
    <div class="slider-content">
    <ol>
    <?php
		$count =0;
		foreach($data as $item){
			$count++;
			$type 	= $item['list']['type'];
			$title 	= $item['list']['title'];
			$image 	= $item['list']['image'];
			
			if(mb_strlen($title) > 47)
			{
				$title = mb_substr($title,0,47,'UTF-8').'...';
			}

			$cateList = [];
			$cateAias  = explode(',', $item['list']['cat_alias']);
			$cateNames = explode(',', $item['list']['cat_names']);
			$firstCateAlias = $cateAias[0];

			$idUrl = $item['list']['id'];
			$href = '#';
			switch($type)
			{
				case 'news':
					//MAKE IMAGE LINK
					if (!empty($image)){
						$pos = strpos($image, "http://");
						if($pos === false)
						{
							$image = '/upload/news/'.$image;
						}
						else
						{
							$image = $this->app->proxy_image($image);
						}
					}

					//MAKE CATEGORY
					foreach( $cateNames as $i=>$cate)
					{
						if(isset($cateAias[$i]))
						{
							$cateList[] = array(
								'href' => "/$type/".$cateAias[$i],
								'name' => $cate
							);
						}
					}

					//MAKE HREF
					$href = "/$type/$firstCateAlias/$idUrl";
					break;
				case 'blog':
		
					//MAKE IMAGE LINK
					if (!empty($image)){
						$image = $this->webroot."upload/blogs/".$image;
					}
					
					
					//Make category
					$cateList[] = array(
						'href' => "/$type/",
						'name' => 'CCブログ'
					);

					//MAKE HREF
					$href = "/$type/$firstCateAlias/$idUrl";
					break;
				case 'contents':
					//MAKE IMAGE LINK
					if (!empty($image)){
						$pos = strpos($image, "ccwork");
						if($pos === false)
						{
							$image= '/upload/contents/'.$image;
						}							
                    }
					
					$idUrl = $item['list']['title_alias'];

					//Make category
					$cateList[] = array(
								'href' => "/$type/",
								'name' => '特集'
					);

					//MAKE HREF
					$href = $this->app->createContentDetailHref($item['list']);
					break;
			}

			
	?>
           
        <li>
			<?php echo $this->element('Item/item_dark_lazy_load_v2', array(
				'href' => $href,
				'org_title' => $title,
				'image' => $image,
				'view' => '',
				'createDate' => '',
				'isNew' => '',
				'cateList' => '',
				'title' => $title,
			)); ?>
    	</li>          
    <?php }//End foreach ?>
       </ol>
    </div>
</div>