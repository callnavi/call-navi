<?php
$data = $this->requestAction('elements/Blogs_info/'.$category_alias);
$item = $data[0];

$link = $this->Html->url(array('controller' => 'blog', 'action' => 'index','category_alias'=>$category_alias), true);

$title = $item['BlogsCategory']['title'];
$date_post = ($item['blogs']['created']) ? $CreateDate = new DateTime($item['blogs']['created']) : "";
if($date_post){
  $date = $date_post->format('Y年m月d日');
}else{
  $date = "";
}
$amount =  $item['blogs']['cnt'];

?>

<div class="person-item-header">
 	<div class="item">
 		<div class="top-part">
 			 <img src="<?php echo $this->webroot."upload/blogs-category/".$item['BlogsCategory']['pic'] ;?>" />
 		</div>
 		<div class="container">
			<div class="content-part">

					<div class="content-part-wp">
						<div>
							<div><span>ブログ数：<?php echo $amount ?>記事</span></div>
							<div><span class="i-date">最終更新日： <?php echo $date; ?></span></div>
						</div>
						<div class="attribute">
						  <div>好きな色：<span><?php echo $item['BlogsCategory']['color']?></span></div>
						  <div>座右の銘：<span><?php echo $item['BlogsCategory']['motto']?></span></div>
						  <div>興味があるもの：<span><?php echo $item['BlogsCategory']['interested']?></span></div>
						</div>
					</div>
				</div>

			<div class="bottom-part text-center">
					 <div class="avatar">
						<img src="<?php echo $this->webroot."upload/blogs-category/avatar/".$item['BlogsCategory']['avatar'] ;?>" />
					</div>
					<h1 class="i-title"><?php echo $title; ?></h1>
					<div class="short"><?php echo $item['BlogsCategory']['short']?></div>
			</div>
 		</div>
 	</div>
</div>
 
 
