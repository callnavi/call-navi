<?php 
	$here =  $this->here;
	$chat_1 = Configure::read('chatconfig.chat_1');
	$chat_2 = Configure::read('chatconfig.chat_2');
	$chat_4 = Configure::read('chatconfig.chat_4');
	
	$list_urls = array(
		//0 - nothing
		0 => array(
			'/secret/apply-step2',
			'/secret/apply-thanks',
			'/contents/preview',
			'/secret/preview',
			'/secret/previewCompany',
			'/blog/preview',
		),
		
		//4
		4 => array('/secret/apply-step1',),

		
		//2
		2 => array(
			'/callcenter_matome',
			'/pretty',
			'/contents',
			'/blog',
			'/about',
			'/company',
			'/contact',
		),
		
		//1
		1 => array(
			'/search',
			'/secret',
			'/',
		),
	);
	
	$rsChat_id = 0;
	foreach( $list_urls as $chat_id => $urls)
	{
		$gotIt = false;
		foreach( $urls as $url)
		{
			if(strpos($here,$url) === 0)
			{
				$rsChat_id = $chat_id;
				$gotIt = true;
				break;
			}
		}
		if($gotIt)
		{
			break;
		}
	}
	
	echo '<!--chat:'.$rsChat_id.'-->';
	switch($rsChat_id)
	{
		case 1:
			echo $chat_1;
			break;
		case 2:
			echo $chat_2;
			break;
		case 4:
			echo $chat_4;
			break;
		default:
		break;
	}
?>