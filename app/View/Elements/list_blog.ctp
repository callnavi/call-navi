<?php
	$ext = (!empty($alias)) ? $alias : "";
$data = $this->requestAction('elements/list_blog_view/'.$ext); ?>

<div class="slider-column slider-blog">
    <div class="slider-header">ブログ＆コラム</div>
    <div class="slider-blog-content">
		<ol>
		<?php
		if($data){
			foreach($data as $item){

				$link = $this->Html->url(array('controller' => 'blog', 'action' => 'index','category_alias' =>$item['BlogsCategory']['category_alias']), true);

				$title = $item['BlogsCategory']['title'];
				$description = mb_strlen($item['BlogsCategory']['short'])> 45 ?
							mb_substr(  $item['BlogsCategory']['short'],0, 45,'UTF-8')."..."
							: 
							$item['BlogsCategory']['short'];
		 ?>
			<li>
				<div class="item">
					<div class="div_images">
						<a href="<?php echo $link;?>">
							<?php if ($item['BlogsCategory']['pic']): ?>
							<img src="<?php echo $this->webroot."upload/blogs-category/".$item['BlogsCategory']['pic'] ;?>" />
							<?php else: ?>
							<img src="" />
							<?php endif; ?>

						<div class="avatar">
							<img src="<?php echo $this->webroot."upload/blogs-category/avatar/".$item['BlogsCategory']['avatar'] ;?>" />
						</div>
						</a>
					</div>
					<div class="content-part">
						<div class="z-index">
							<p class="i-title"><a href="<?php echo $link;?>"><?php echo $title; ?></a></p>
							<p class="i-des"><?php echo $description; ?></p>
						</div>

					</div>
				</div>
			</li>
		<?php } }?>
		</ol>
	</div>
</div>