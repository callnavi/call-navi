<?php $data = $this->requestAction('elements/ranking'); ?>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 slidertop">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding slider-header" >
        <h1>アクセスランキング</h1>
			<!--<h2>アクセスランキング</h2> -->
    </div>
    <?php
		$count =0;
		foreach($data as $item){
			$count++;
			$type 	= $item['list']['type'];
			$title 	= $item['list']['title'];
			$image 	= $item['list']['image'];
			
			if(mb_strlen($title) > 20)
			{
				$title = mb_substr($title,0,20,'UTF-8').'...';
			}

			$cateList = [];
			$cateAias  = explode(',', $item['list']['cat_alias']);
			$cateNames = explode(',', $item['list']['cat_names']);
			$firstCateAlias = $cateAias[0];

			$idUrl = $item['list']['id'];
			$href = '#';
			switch($type)
			{
				case 'news':
					//MAKE IMAGE LINK
					if (!empty($image)){
						$pos = strpos($image, "http://");
						if($pos === false)
						{
							$image = '/upload/news/'.$image;
						}
						else
						{
							$image = $this->app->proxy_image($image);
						}
					}

					//MAKE CATEGORY
					foreach( $cateNames as $i=>$cate)
					{
						if(isset($cateAias[$i]))
						{
							$cateList[] = array(
								'href' => "/$type/".$cateAias[$i],
								'name' => $cate
							);
						}
					}
					break;
				case 'blog':
		
					//MAKE IMAGE LINK
					if (!empty($image)){
						$image = $this->webroot."upload/blogs/".$image;
					}
					
					
					//Make category
					$cateList[] = array(
						'href' => "/$type/",
						'name' => 'CCブログ'
					);
					break;
				case 'contents':
					//MAKE IMAGE LINK
					if (!empty($image)){
						$pos = strpos($image, "ccwork");
						if($pos === false)
						{
							$image= '/upload/contents/'.$image;
						}							
                    }
					
					$idUrl = $item['list']['title_alias'];

					//Make category
					$cateList[] = array(
								'href' => "/$type/",
								'name' => '特集'
					);
					break;
			}

			//MAKE HREF
			$href = "/$type/$firstCateAlias/$idUrl";
	?>
            <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding sliderimg">
                <a href="<?php echo $href; ?>">
    				<img src="<?php echo $image ;?>"/>
                </a>
            </div>
            <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-padding slidertext">
        		<div class="rank-label"><span><?php echo $count ;?></span></div>
                <a href="<?php echo $href; ?>">
                	<h4><?php echo $title; ?></h4>
                </a>
				
              	<!-- <?php foreach ($cateList as $cat): ?> -->
				<!-- <a href="<?php echo $cat['href']; ?>"> -->
					<!-- <p><?php echo $cat['name']; ?></p> -->
				<!-- </a> -->
				<!-- <?php endforeach; ?> -->
            </div>
            <!-- <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding sliderborder"></div> -->
    <?php }//End foreach ?>
</div>


<?php echo $this->element('blog_column'); ?>