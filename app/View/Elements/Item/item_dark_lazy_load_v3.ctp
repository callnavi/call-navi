<div class="article-box">
    <div class="display-table">
        <div class="search-image">
            <a href="<?php echo $href; ?>" title="<?php echo $org_title; ?>">
                <img src="<?php echo $image; ?>" alt="<?php echo $org_title; ?>">
            </a>
        </div>

        <div class="search-content">
            <!--Contents-->
            <a href="<?php echo $href; ?>">
                <h4 class="middle-part title">
                    <strong><?php echo $title; ?></strong>
                </h4>
                <div class="middle-part desciption">
                    <?php echo $content; ?>
                </div>
            </a>
            <!--End Contents-->

            <!--Category-->
            <?php if (!empty($cateList)) { ?>
                <div class="bottom-part">
                    <?php foreach ($cateList as $cat): ?>
                        <?php
                        $cat['category'] = $cat['name'];
                        $href = $this->app->createContentCategoryHref($cat);
                        ?>
                        <a class="i-cat" href="<?php echo $href; ?>"><strong><?php echo $cat['name']; ?></strong></a>
                    <?php endforeach; ?>
                </div>
            <?php } ?>
            <!--End Category-->

            <!--Create Date-->
            <a href="<?php echo $href; ?>">
                <div class="top-part clearfix mg-top-10 _sss">
                    <?php if (!empty($createDate)) { ?>
                        <div class="pull-left">
                            <time><?php echo $createDate; ?></time>
                        </div>
                    <?php } ?>
                </div>

            </a>
            <!--End Create Date-->
        </div>
    </div>
</div>