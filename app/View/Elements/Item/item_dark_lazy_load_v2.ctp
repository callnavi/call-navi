<div class="article-box">
    <div class="display-table">
        <div class="search-image">
            <a href="<?php echo $href; ?>" title="<?php echo $org_title; ?>">
                <img src="<?php echo  $image; ?>" alt="<?php echo $org_title; ?>">
            </a>
        </div>

        <div class="search-content">
           <a href="<?php echo $href; ?>">
				<div class="top-part clearfix">
					<?php if (!empty($createDate)) { ?>
						<div class="pull-left">
							<time><?php echo $createDate; ?></time>
						</div>
					<?php } ?>
					<?php if (!empty($view)) { ?>
						<div class="pull-right" style="
                            <?php
                                $catvalidate = false;    
                            ?>
                            <?php foreach ($cateList as $cat): ?>
                            <?php
                                if($cat['name'] == '企業インタビュー')
                                {
                                    $catvalidate = true;
                                }                          
                            
                            ?>
                            <?php endforeach; ?>
                            <?php
                            if($catvalidate == true )
                            {
                                echo('display:none;');
                            }
                            ?>
                        
                        ">
							<?php echo $view; ?> view
						</div>
					<?php } ?>
				</div>
				<div class="middle-part">
					<?php echo $title; ?>
				</div>
				</a>
				<?php if (!empty($cateList)) { ?>
					<div class="bottom-part text-right">
						<?php foreach ($cateList as $cat): ?>
							<?php 
								$cat['category'] = $cat['name'];
								$href = $this->app->createContentCategoryHref($cat);
							?>
							<a class="i-cat" href="<?php echo $href; ?>"><?php echo $cat['name']; ?></a>
						<?php endforeach; ?>
					</div>
				<?php } ?>
        </div>
    </div>
</div>