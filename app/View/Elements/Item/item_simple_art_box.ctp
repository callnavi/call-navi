<div class="verticle_art_box">
	<a href="<?php echo $href; ?>" title="<?php echo $org_title; ?>">
		<div class="mark-image">
			<div class="mark-image--wp fix-height-160">
				<img src="<?php echo $image; ?>" alt="<?php echo $org_title; ?>">
			</div>
		</div>
    </a>
</div>