<?php  
if(empty($pic_url))
{
	$pic_url = '/img/instead/img_no-image_' . sprintf("%02d", rand(1, 21)) . '.jpg';
}
else
{
	if($pic_url[0] != "/")
	{
		$pic_url = $this->app->proxy_image($pic_url);
	}
}

$salary = mb_substr(strip_tags($salary),0,35,'utf-8');
if(mb_strlen($salary)>25)
{
	$salary = mb_substr($salary,0,25,'UTF-8').'...';
}

$clip_title = mb_substr(strip_tags($title),0,65,'utf-8');

if(mb_strlen($company)>15)
{
	$company = mb_substr($company,0,15,'UTF-8').'...';
}


$isNew = 0;
//604800 = 7 days
$timeInAWeek = time()-604800;
if($updatetime > $timeInAWeek)
{
	$isNew = 1;
}

//Sort category follows length
usort($labels, function($a, $b) {
    return strlen($a) - strlen($b);
});

$catLengCount = 0;
$maxCatLeng = 30;

$encodeId = $this->app->jobUrlEncode($unique_id);
$href = '/search/job/'.$encodeId;
?>

<div class="clearfix search-box">
	<div class="pull-left search-image">
		<a rel="nofollow" href="<?php echo $href; ?>" target="_blank">
			<img src="<?php echo $pic_url; ?>" alt="<?php echo $title; ?>">
		</a>
	</div>
	<div class="pull-right search-content">
		<div class="clearfix search-head">
			<div class="pull-left">
				<?php echo $company; ?>
				
				<?php if ($isNew): ?>
					<span class="new-label">NEW</span>
				<?php endif; ?>
			</div>
			<div class="pull-right">
				<div class="search-work">提供元：<?php echo $site_name; ?></div>
			</div>
		</div>
		
		<div class="search-keyword">
			<?php
				foreach ($labels as $i => $label): 
					$catLengCount += mb_strlen($label);
					if($catLengCount >= $maxCatLeng) { break; } 
			?>
				<span class="i-cat"><?php echo $label; ?></span>
			<?php endforeach; ?>
		</div>
		<div class="search-title">
			<a rel="nofollow" href="<?php echo $href; ?>"target="_blank" ><?php echo $clip_title; ?></a>
		</div>
		<div class="clearfix search-foot">
			<div class="pull-left search-salary">
				<?php echo $salary; ?>
			</div>
			<div class="pull-right search-ref">
				<a rel="nofollow" class="brick-btn brick-gray" href="<?php echo $href; ?>"target="_blank" >詳細を確認</a>
			</div>
		</div>
	</div>
</div>