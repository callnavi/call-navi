<?php
	/*
		requestAction('elements/search_total')
		-> go: app/Controller/ElementsController.php -> call: search_total()
		
		Configure::read('webconfig.area_data')
		-> go: app/Config/webconfig.php -> get: $config['webconfig']['area_data']
	*/
	$search_total = $this->requestAction('elements/search_total');
	$secret_total = $this->requestAction('elements/secret_total');
	$area_data =  Configure::read('webconfig.area_data');
	$employment_data = Configure::read('webconfig.employment_data');
	$hotkey_list = Configure::read('webconfig.hotkey_data');


	/*
		'keyword' => $_keyword,
		'area'	  => $_selectedArea,
		'em'	  => $_selectedEm,
		'hotkey'  => $_checkedHotkey,
		'total'	  => $search_total
	*/
?>

<script type="text/javascript">
var request = {};
request.area  		= '<?php echo $area; ?>';
request.em  		= "<?php echo isset($params['data_em']) ? $params['data_em'] : 0; ?>";
request.keyword  	= '<?php echo $keyword; ?>';
request.countUrl 	= '<?php echo $countUrl; ?>';
request.hotkey 		= <?php echo json_encode($hotkey); ?>;
request.total 		= <?php echo $total; ?>;
</script>

<div class="top-banner">
	<div class="background">
		<div class="container">
			<div class="clearfix">
				<img src="/img/banner/top_02.jpg">
				<img src="/img/banner/top_03.jpg">
			</div>
		</div>
	</div>
	
	<div class="top-banner-search">
		<div class="container">
			<div class="top-list-header">
				<span class="label-header">JOB</span>
				<?php if ($_keyResult): ?>
				<big><?php echo $_keyResult; ?></big>
				<?php else: ?>
				<big><!-- すべて --></big>
				<?php endif; ?>
				<small>コールセンター求人・仕事が</small> <big><?php echo $total; ?></big><small class="unit">件見つかりました</small>
			</div>
			<div class="banner-search-wrap-box">
				<div class="column-left">
					<div class="search-box-metal">
						<div class="top-part">
							<div class="metal-item">
								<span class="i-label">地域：</span>
								<div class="metal-text-justify"><?php echo isset($area)?$area:''; ?></div>
								<span class="i-change">変更</span>
								
							</div>
							<div class="metal-item">
								<span class="i-label">雇用形態：</span>
								<div><?php echo isset($em)?$em:''; ?></div>
								<span class="i-change">変更</span>
								
							</div>
							
						</div>
						<div class="content-part">
							<div class="metal-item">
								<span class="i-label">フリーワード：</span>
								<?php if (empty($keyword)): ?>
								<div>未入力</div>
								<?php else: ?>
								<div><?php echo $keyword; ?></div>
								<?php endif; ?>
								<span class="i-change">変更</span>
							</div>
						</div>
						
						<div class="bottom-part">
							<div class="metal-item">
								<span class="i-label">条件：</span>
								<div>
									<?php
										foreach( $hotkey as $checkedId)
										{
											$checkedName = $hotkey_list[$checkedId];
											echo '<span class="i-hotkey">'.$checkedName.'</span> ';
										}
									?>
								</div>
								<span class="i-change">変更</span>
							</div>
						</div>
					</div>
				</div>
				<div class="column-right" id="wrapper_history_box">
					<div class="history-box-metal" id="history_box">
						<div class="top-part">
							<span class="icon-search"></span>
							<a href="#" class="go-to-search-history" data-history="url">前回検索した条件から探す</a><span data-history="total"></span>
							<a href="#" class="clear-search-history" data-history="remove">履歴を削除</a>
						</div>
						<div class="content-part">
							<?php echo $currentKeyWord; ?>
						</div>
						<div class="bottom-part">
							<button type="submit" class="brick-btn brick-green search-btn search-horizon-result">検索条件を再設定</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>


<!--POP-UP-->

<div class="top-popup-search">
		<form method="get" data-seo="submit">
			<div class="popup-container">
				<div class="top-list-header">
					<span class="label-header">JOB</span>
					<big class="callcenterjob">コールセンター求人</big><big data-total="result"><?php echo $total; ?></big><small class="unit">件</small>
				
					<span class="close-button">
						<i></i>
						<i></i>
						閉じる
					</span>
				</div>

				<div class="banner-search-wrap-box">
						<div class="search-box-metal">
							<div class="top-part">
								<div class="metal-item">
									<span class="i-label">地域：</span>
									<div>
											<input id="selectArea" data-search="area" class="textbox-metal select-area" type="text" placeholder="地域を選択する" value="<?php echo isset($area)?$area:''; ?>" data-popup="PC">
											<input id="" class="result-area" type="hidden" name="area" value="<?php echo isset($params['list_key_area'])?$params['list_key_area']:''; ?>">
											<input type="hidden" class="data-em" name="data_em" value="0">
											<input type="hidden" class="data-step" name="data_step" value="0">
											<input type="hidden" class="data-group" name="data_group" value="0">
											<input type="hidden" class="data-province" name="data_province" value="0">
											<input type="hidden" class="data-city" name="data_city" value="0">
											<input type="hidden" class="data-town" name="data_town" value="0">
											<input type="hidden" class="event-before-open-popup" value="">
											<input type="hidden" class="event-after-close-popup" value="">
									</div>
								</div>
								<div class="metal-item">
									<span class="i-label">雇用形態：</span>
									<div>
										<div class="select-metal">
											<select name="em" id="selectEm" data-search="em">
												<option value="">雇用形態を選択する</option>
												<?php foreach ($employment_data as $key=>$name): ?>
												<option value="<?php echo $key; ?>"><?php echo $name; ?></option>
												<?php endforeach; ?>
											</select>
										</div>
									</div>

								</div>

							</div>
							<div class="content-part">
								<div class="metal-item">
									<span class="i-label">フリーワード：</span>
									<div><input data-search="keyword" id="txtKeyword" name="keyword" class="textbox-metal" type="text" placeholder="フリーワードを入力する"></div>
								</div>
							</div>

							<div class="bottom-part">
								<div class="metal-item">
									<span class="i-label">条件：</span>
									<div>
										<?php foreach ($hotkey_list as $key=>$name): ?>
										<label class="checkbox-i-button">
											<input data-search="hotkey" data-value="<?php echo $name; ?>" name="hotkey[]" type="checkbox" value="<?php echo $key; ?>">
											<span class="i-hotkey"><?php echo $name; ?></span>
										</label>
										<?php endforeach; ?>
									</div>
								</div>
							</div>
					</div>
				</div>

				<div class="bottom-submit">
					<button type="submit" class="brick-btn brick-green">検索開始</button>
				</div>
			</div>
		</form>
	</div>

<script type="text/javascript">
$('.data-step').val(0);
$('.data-group').val(<?php echo isset($params['data_group'])?$params['data_group']:0; ?>);
$('.data-province').val(<?php echo isset($params['data_province'])?$params['data_province']:0; ?>);
$('.data-city').val(<?php echo isset($params['data_city'])?$params['data_city']:0; ?>);
$('.data-town').val(<?php echo isset($params['data_town'])?$params['data_town']:0; ?>);
$('.data-em').val(<?php echo isset($params['data_em'])?$params['data_em']:0; ?>);
</script>