<div class="top-banner">
	<div class="background-full">
		<div class="container">
			<div class="clearfix">
				<img src="/img/banner/top_concierge.png">
			</div>

            <div class="text text-center">
                <h1>コールセンターのことなら、私たちにお任せください！</h1>
                <p>私たち、求人コンサルタントが、あなたのご要望に合わせて、<br>
                    親身にご提案させていただきます。</p>
            </div>
            <div class="cirle-green">
                相談<br>無料
            </div>
		</div>
	</div>
</div>
