<?php

	$modulus = 2;
	$haft  = (int)$modulus/2;
 
	if(($modulus+1) >= $pageCount)
	{
		$start = 1;
		$end = $pageCount;
	}
	else
	{
		if(($currentPage - $haft) <= 1)
		{
			$start = 1;
			$end = $start + $modulus;
		}
		else
		{
			if($currentPage > ($pageCount - $haft))
			{
				$end = $pageCount;
				$start = $end - $modulus;
			}
			else
			{
				$start = $currentPage - $haft;
				$end = $currentPage + $haft; 
			}
		}
	}
?>
<div class="clearfix"></div>
	<?php if($pageCount > 1): ?>
<div class="prev">
		<div class="next-prev-block">
			<?php if($currentPage <= 1): ?>
				<span class="button1">PREV</span>
			<?php else: ?>
				<span class="prev"><a href="<?php echo $this->Html->general_url($currentPage-1); ?>" rel="prev">PREV</a></span>&nbsp;
			<?php endif; ?>

			<?php if($currentPage >= $pageCount): ?>
				 <span class="button3">NEXT</span>
			<?php else: ?>
				<span class="next"><a href="<?php echo $this->Html->general_url($currentPage+1); ?>" rel="next">NEXT</a></span>
			<?php endif; ?>

		</div>
		<div class="pagination-number" >
			<?php if($currentPage >= $modulus): ?>
				&nbsp;<span class="current button2"><a href="<?php echo $this->Html->general_url(1); ?>" rel="first"> 1 </a></span> &nbsp;
				•••&nbsp;
			<?php endif; ?>


			<?php for($i=$start; $i<=$end; $i++): ?>
				<?php if ($currentPage == $i): ?>
					<span class="current button2"><?php echo $i; ?></span>&nbsp;
				<?php else: ?>
					<span class="button2"><a href="<?php echo $this->Html->general_url($i); ?>"><?php echo $i; ?></a></span>&nbsp;
				<?php endif; ?>
			<?php endfor; ?>


			<?php if($pageCount >= $currentPage+$modulus): ?>
				  ••• &nbsp; 
				  <span class="button2"><a href="<?php echo $this->Html->general_url($pageCount); ?>" rel="last"> <?php echo $pageCount; ?> </a></span>&nbsp;
			<?php endif; ?>

		</div>
</div>
	<?php endif; ?>
