<div class="custom-checkbox" 
<?php if (isset($id)): ?>
id="<?php echo $id; ?>"
<?php endif; ?>>
	<label>
		<input type="checkbox" value="<?php echo $value; ?>" name="<?php echo $name; ?>" <?php echo isset($checked)&&$checked?'checked':''; ?>>
		<div class="vir-label">
			<span class="vir-checkbox"></span>
			<span class="vir-content"><?php echo $text; ?></span>
		</div>
	</label>
</div>