<?php
	$modulus = 4;
	$haft  = (int)$modulus/2;
 
	if(($modulus+1) >= $pageCount)
	{
		$start = 1;
		$end = $pageCount;
	}
	else
	{
		if(($currentPage - $haft) <= 1)
		{
			$start = 1;
			$end = $start + $modulus;
		}
		else
		{
			if($currentPage > ($pageCount - $haft))
			{
				$end = $pageCount;
				$start = $end - $modulus;
			}
			else
			{
				$start = $currentPage - $haft;
				$end = $currentPage + $haft; 
			}
		}
	}
?>


<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 prev">
	<?php if($pageCount > 1): ?>

		<?php if($currentPage <= 1): ?>
			<span class="button1">&#60; PREV</span>&nbsp;			
		<?php else: ?>
			<a href="<?php echo $this->Html->general_url($currentPage-1); ?>" rel="prev">&#60; PREV</a>&nbsp;
		<?php endif; ?>
		
		<?php if($currentPage >= $modulus): ?>
			<span class="button2"><a href="<?php echo $this->Html->general_url(1); ?>" rel="first"> 1 </a></span>&nbsp;
			・・・&nbsp;
		<?php endif; ?>
		
		
		<?php for($i=$start; $i<=$end; $i++): ?>
			<?php if ($currentPage == $i): ?>
				<span class="current button2"><?php echo $i; ?></span>&nbsp;
			<?php else: ?>
				<span class="button2"><a href="<?php echo $this->Html->general_url($i); ?>"><?php echo $i; ?></a></span>&nbsp;
			<?php endif; ?>
		<?php endfor; ?>
		
		
		<?php if($pageCount >= $currentPage+$modulus): ?>
			  ・・・ &nbsp; 
			  <span class="button2"><a href="<?php echo $this->Html->general_url($pageCount); ?>" rel="last"> <?php echo $pageCount; ?> </a></span>&nbsp;
		<?php endif; ?>

		
		<?php if($currentPage >= $pageCount): ?>
			<span class="button3">NEXT &#62;</span>			
		<?php else: ?>
			<a href="<?php echo $this->Html->general_url($currentPage+1); ?>" rel="next">NEXT &#62;</a>
		<?php endif; ?>
	<?php endif; ?>
</div>