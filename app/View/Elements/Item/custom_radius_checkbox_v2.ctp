<div class="custom-radius-checkbox" 
<?php if (isset($id)): ?>
id="<?php echo $id; ?>"
<?php endif; ?>>
    <label>
        <input <?php echo isset($attr)?$attr:''; ?> type="checkbox" value="<?php echo $value; ?>" name="<?php echo $name; ?>" <?php echo isset($checked)&&$checked?'checked':''; ?>>
                  <div class="vir-label"><span class="vir-content"><?php echo $text; ?></span><span class="vir-checkbox"></span></div>
                </label>
              </div>