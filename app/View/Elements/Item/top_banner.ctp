<?php
?>
<div class="banner common-panel">
	<div id="ads-panel" class="ads-panel">
		<a href="#banner" title="">
			<img style="width:1100px;" src="/img/callnavi_topPc.png" alt="「働きやすいコールセンター」が探せる求人サイト">
		</a>
	</div>
	<div class="clearfix search_wrap_box">
		<div class="top-search pull-left">
			<form action="/search" method="GET">
			<div class="custom-select">
				<select name="area" id="area">
						<option value="">都道府県選択</option>
						<?php foreach ($area_data as $k=>$v): ?>
							<option value="<?php echo $k; ?>"><?php echo $v; ?></option>
						<?php endforeach; ?>
					</select>
			</div>
			<span class="multiply"></span>
			<div class="custom-select">
				<select name="em" id="em">
						<option value="">雇用形態を選択</option>
						<?php foreach ($employment_data as $k=>$v): ?>
							<option value="<?php echo $k; ?>"><?php echo $v; ?></option>
						<?php endforeach; ?>
				</select>
			</div>
			<button type="submit" class="brick-btn brick-green">コールセンター求人検索開始</button>
			</form>
		</div>

		<div class="number-jobs text-right pull-right">
			<p>CC求人数 <span><?php echo number_format($search_total,0); ?></span> 件</p>
			<p>シークレット求人数 <span><?php echo number_format($secret_total,0); ?></span> 件</p>
		</div>
	</div>
</div>

<!-- <script>
	$( document ).ready(function() {
		// var rand = <?php echo rand(1,3) ;?>;
		switch (rand){
			case 1:
				$("#ads-panel a img ").attr('src','/img/pc_top_banner_wiz.png');
				$("#ads-panel a img ").attr('alt','株式会社Wiz（ワイズ） 「シークレット求人」');
				$("#ads-panel a").attr('href','/secret/detail/1');
				break;
			case 2:
				$("#ads-panel a img ").attr("src","/img/pc_top_banner_funride.png");
				$("#ads-panel a img ").attr('alt','株式会社FunRid(ファンライド) 「シークレット求人」');
				$("#ads-panel a").attr('href','/secret/detail/2');
				break;
			case 3:
				$("#ads-panel a img ").attr('src','/img/pc_top_banner_besteffort.png');
				$("#ads-panel a img ").attr('alt','株式会社Bestエフォート「シークレット求人」');
				$("#ads-panel a").attr('href','/secret/detail/22');
				break;
		}
	});
</script> -->
