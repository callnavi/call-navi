<?php 
$interviewlink = "";
$hasInterview = "";

foreach ($secretJobsHasInterview as $secret) {
	if($secret['Contents']['secret_job_id'] == $item['CareerOpportunity']['id'])
	{
		$hasInterview = true;
        $interviewlink = "/secret/".$secret['Contents']['title'];
		break;
	}
}   
    
	$value = $item['CareerOpportunity'];

    $benefit =  $value['celebration'];
    if($hasInterview == ""){
        $href = $this->App->createSecretDetailHref($value);
    }else{
        $href = $interviewlink;   
    }
	$company = $item['CareerOpportunity']['branch_name'];
	$clip_title = strlen($value['job'])> 60 ? mb_substr(   $value['job'],0,60,'UTF-8').'...' :  $value['job'];
	$value['salary'] = strip_tags($value['salary']);
	$salary = strlen($value['salary'])> 40 ? mb_substr(  $value['salary'],0,40,'UTF-8').'...' : $value['salary'];
	$pic_error_url = '/img/instead/img_no-image_' . sprintf("%02d", rand(1, 10)) . '.jpg';


	$hasInterview = false;
	$company_id = $item['corporate_prs']['id'];
	$secret_job_id = $item['CareerOpportunity']['id'];
	foreach ($secretJobsHasInterview as $secret) {
		if($secret['Contents']['company_id'] == $company_id && $secret['Contents']['secret_job_id'] == $secret_job_id)
		{
			$hasInterview = true;
			break;
		}
	}
?>					
<div class="df-padding y_search_sp y_search_sp">
	<div class="article-box">
		<?php if($hasInterview): ?>
		<span class="interview-tag">企業インタビュー</span>
		<?php endif; ?>
		<span class="secret-tag">オリジナル求人</span>
        <?php if($benefit): ?>
		<span class="benefit-tag">お祝い金あり</span>
		<?php endif; ?>
        <p class="title"><?php echo $value['job']; ?></p>
        <div class="display-table">
			<div class="search-image">
				<a target="_blank" href="<?php echo $href; ?>">
					<img src="<?php echo $this->webroot."upload/secret/jobs/".$value['corporate_logo'] ;?>" alt="<?php echo $value['job']; ?>" onerror="this.onerror=null;this.src='<?php echo $pic_error_url; ?>';">
				</a>
			</div>
                    <div class="search-content">
                      <ul>
                        <li class="search-content__salary"><span class="label-brick-inline">給与</span><span class="salary"><?php echo $salary; ?></span></li>
                        <?php if($benefit): ?>
                            <li class="mg-top"><span class="label-brick-inline--secondary">お祝い金</span> 
                            <span class="benefit"><?php echo (strip_tags($benefit)); ?></span></li><span class="salary"></span>
                        <?php endif; ?>
                        
                      </ul>
                      <hr class="secret-hr"><span class="company text-ellipsis-100"><?php echo( $item['CareerOpportunity']['branch_name']); ?></span>
                    </div>
		</div>
		<br>
		<a target="_blank" class="y_search_sp_btn" href="<?php echo $href; ?>">詳細を確認する</a>

		<div class="site_name"></div>
	</div>
</div>