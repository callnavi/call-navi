<div class="clearfix">
		<div class="top-search search-sp">
			<form action="/search" method="GET">
				<div class="custom-select custom-aboutsp">
					<select name="area" id="area">
						<option value="">都道府県選択</option>
						<?php foreach ($area_data as $k=>$v): ?>
							<option value="<?php echo $k; ?>"><?php echo $v; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<span class="multiply"></span>
				<div class="custom-select custom-aboutsp">
					<select name="em" id="em">
						<option value="">雇用形態を選択</option>
						<?php foreach ($employment_data as $k=>$v): ?>
							<option value="<?php echo $k; ?>"><?php echo $v; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<div class="button-select">
					<button type="submit" class="brick-btn brick-green">CC求人検索開始</button>
				</div>
			</form>
		</div>
	</div>