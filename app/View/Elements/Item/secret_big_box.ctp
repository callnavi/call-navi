<?php

$career = $item['CareerOpportunity'];
$corporate = $item['corporate_prs'];

$hasInterview = false;
$company_id = $corporate['id'];
$secret_job_id = $career['id'];
$interviewlink = "";


$benefit =  $career['benefit'];

foreach ($secretJobsHasInterview as $secret) {
	if($secret['Contents']['company_id'] == $company_id && $secret['Contents']['secret_job_id'] == $secret_job_id)
	{
		$hasInterview = true;
        $interviewlink = "/secret/".$secret['Contents']['title'];
		break;
	}
}
$title = $career['job'];
$company = $career['branch_name'];
$labels = explode(',',$career['work_features']);
$benefit = strip_tags($career['celebration']);
$business = $career['business'];
if(empty($career['corporate_logo']))
{
	$pic_url = '/img/instead/img_no-image_' . sprintf("%02d", rand(1, 10)) . '.jpg';
}
else
{
	$pic_url = $this->webroot."upload/secret/jobs/".$career['corporate_logo'];
}
$pic_error_url = '/img/instead/img_no-image_' . sprintf("%02d", rand(1, 10)) . '.jpg';

$salary = mb_substr(strip_tags($career['hourly_wage']),0,35,'utf-8');
if(mb_strlen($salary)>50)
{
	$salary = mb_substr($salary,0,50,'UTF-8').'...';
}

$clip_title = $title;
if(mb_strlen($clip_title)>35)
{
	$clip_title = mb_substr(strip_tags($clip_title),0,35,'utf-8').'...';
}

if(mb_strlen($company)>25)
{
	$company = mb_substr($company,0,25,'UTF-8').'...';
}


$isNew = 0;
//604800 = 7 days

//Sort category follows length
usort($labels, function($a, $b) {
    return strlen($a) - strlen($b);
});

$catLengCount = 0;
$maxCatLeng = 30;
if($interviewlink == ""){
    $href = $this->App->createSecretDetailHref($career);
}else{
    $href = $interviewlink;
}
?>

<div class="search-box">
	<div class="search-title">
		<?php if($hasInterview): ?>
		<span class="interview-tag">企業インタビュー</span>
		<?php endif; ?>
		<?php if(isset($isSearch)): ?>
		<span class="secret-tag">オリジナル求人</span>
		<?php endif; ?>
        <?php if($benefit): ?>
		<span class="benefit-tag">お祝い金あり</span>
		<?php endif; ?>
		<h3><?php echo $title; ?></h3>
        
	</div>
	<div class="display-table">
		<div class="search-image">
			<a href="<?php echo $href; ?>" target="_blank">
				<img src="<?php echo $pic_url; ?>" alt="<?php echo $title; ?>" onerror="this.onerror=null;this.src='<?php echo $pic_error_url; ?>';">
			</a>
		</div>

		<div class="search-content secret-content">
			<div class="search-head clearfix">
				<div class="pull-left">
					<p class="search-company"><?php echo $company; ?></p>
				</div>
				<div class="pull-right">
					<div class="new-label">
					<?php if ($isNew): ?>
						NEW
					<?php endif; ?>
					</div>
					<div class="search-work"></div>
				</div>

			</div>

			<div class="search-salary">
				<span>給与</span> <p><?php echo $salary; ?></p>
			</div>
            <?php if($benefit): ?> 
            
                <div class="search-benefit">
                    <span>お祝い金</span> <p><?php echo $benefit; ?></p>
                </div>
            <?php endif; ?>   
            
			<div class="search-keyword">
				条件：
				<?php
					foreach ($labels as $i => $label): 
						$catLengCount += mb_strlen($label);
						if($catLengCount >= $maxCatLeng) { break; } 
				?>
					<span class="i-condi"><?php echo $label; ?></span>
				<?php endforeach; ?>
			</div>

			<div class="clearfix search-foot">
				<a class="brick-btn brick-green" href="<?php echo $href; ?>"target="_blank" >詳細を確認</a>
			</div>
		</div>
	</div>
</div>