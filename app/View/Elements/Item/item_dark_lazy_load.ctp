<div class="item">
	<div class="top-part">
		<a href="<?php echo $href; ?>" title="<?php echo $org_title; ?>">
			<img class="lazy" data-original="<?php echo $image; ?>" alt="<?php echo $org_title; ?>">
			<noscript>
			<img src="<?php echo $image; ?>" alt="<?php echo $org_title; ?>">
			</noscript>
		</a>
		<span class="i-view"><?php echo $view; ?>
			<small>view</small></span>
	</div>
	<div class="content-part">
		<div class="clearfix">
			<div class="pull-left">
				<span class="i-date"><?php echo $createDate; ?></span>
				<?php if ($isNew) { ?>
					<span class="i-new">NEW</span>
				<?php } ?>
			</div>
			<div class="pull-right">
				<?php foreach ($cateList as $cat): ?>
					<a class="i-cat" href="<?php echo $cat['href']; ?>"><?php echo $cat['name']; ?></a>
				<?php endforeach; ?>
			</div>
		</div>
		<p class="i-title">
			<a href="<?php echo $href; ?>" title="<?php echo $org_title; ?>">
				<?php echo $title; ?>
			</a>
		</p>
	</div>
</div>