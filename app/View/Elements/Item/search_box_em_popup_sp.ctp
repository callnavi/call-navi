<div id="popup_search_box_em" class="popup-search-box">
	<div class="popup-content box-search">
		<div class="close-button" id="close_popup_em">
				<span class="icon-close"></span>
				<span class="title-close">close</span>
		</div>
		<div class="top-box-header">
			<div class="logo">
				<div class="image"><img src="/img/only-logo-white.png"></div>
				<div class="title">雇用形態を選択する</div>
			</div>
		</div>
		<div class="top-box-title">
			<span class="title">雇用形態を選択</span>
		</div>
		<div class="search-box-metal" id="box_search_em">
			<div class="metal-item">
				<span class="i-label">正社員</span>
				<span class="i-change"></span>
			</div>
			<div class="metal-item">
				<span class="i-label">アルバイト</span>
				<span class="i-change"></span>
			</div>
			<div class="metal-item">
				<span class="i-label">パート</span>
				<span class="i-change"></span>
			</div>
			<div class="metal-item">
				<span class="i-label">契約社員</span>
				<span class="i-change"></span>
			</div>
			<div class="metal-item">
				<span class="i-label">派遣</span>
				<span class="i-change">選択しない</span>
			</div>
			<div>
			</div>
		</div>
	</div>
</div>