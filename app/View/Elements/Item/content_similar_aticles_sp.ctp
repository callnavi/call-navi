<?php
    $data = $this->requestAction('elements/content_similar_aticles/'.$category);
?>
<div class="article-list">
	<ul class="fullcolor-list">
		<?php foreach($data as $item):?>
		<?php 
			if($item[0]['totalHour']<= 3)
				$isNew = true;
			else
				$isNew = false;
		
			$CreateDate = new DateTime($item['Contents']['created']);
			$createDate = $CreateDate->format('Y年m月d日');
        if($item['Contents']['secret_job_id']){
            $href = "/secret/". $item['Contents']['title']; ;
        }else{
			$href = "/contents/".$item['Contents']['category_alias'].'/'.$item['Contents']['title_alias'];            
        }
		?>
		<li class="df-padding">
			<div class="article-box">
				<div class="display-table">
					<div class="search-image">
						<a href="<?php echo $href ;?>">
                            <img src="<?php echo $this->app->getContentImageSrc($item['Contents']['image']); ?>"/>
						</a>
					</div>

					<div class="search-content">
						<div class="top-part clearfix">
							<div class="pull-left">
								<time><?php echo $createDate; ?></time>
								<?php if($isNew){?>
									<span class="new-label">NEW</span>
								<?php } ?>
							</div>
							<div class="pull-right">
								<big><?php echo $item['Contents']['view']; ?></big> view
							</div>
						</div>
						<div class="middle-part">
							<a href="<?php echo $href; ?>"><?php echo $item['Contents']['title']; ?></a>
						</div>
						<div class="bottom-part text-right">
							<a href="<?php echo $this->Html->url(array('controller' => 'contents', 'action' => 'index', 'category_alias'=>$item['Contents']['category_alias']), true);?>"><?php echo $item['category']['category'] ;?></a>
						</div>
					</div>
				</div>
			</div>
		</li>
		<?php endforeach;?>
	</ul>
</div>