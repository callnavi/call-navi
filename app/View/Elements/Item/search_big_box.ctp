<?php 
//---------------------------------------------------
if(empty($pic_url))
{
	$pic_url = '/img/instead/img_no-image_' . sprintf("%02d", rand(1, 21)) . '.jpg';
}
else
{
	if($pic_url[0] != "/")
	{
		$pic_url = $this->app->proxy_image($pic_url);
	}
}

$pic_error_url = '/img/instead/img_no-image_' . sprintf("%02d", rand(1, 10)) . '.jpg';

$salary = strip_tags($salary);
if(mb_strlen($salary)>50)
{
	$salary = mb_substr($salary,0,50,'UTF-8').'...';
}

$clip_title = mb_substr(strip_tags($title),0,65,'utf-8');

if(mb_strlen($company)>25)
{
	$company = mb_substr($company,0,25,'UTF-8').'...';
}


$isNew = 0;
//604800 = 7 days
$timeInAWeek = time()-604800;
if($updatetime > $timeInAWeek)
{
	$isNew = 1;
}

//Sort category follows length
usort($labels, function($a, $b) {
    return strlen($a) - strlen($b);
});

$catLengCount = 0;
$maxCatLeng = 30;

$encodeId = $this->app->jobUrlEncode($unique_id);
$href = '/search/job/'.$encodeId;
?>

<div class="search-box">
	<div class="search-title">
		<h3><?php echo $title; ?></h3>
	
		<?php 
			if(isset($_GET['query_log']))
			{
				echo $workplace; 
			}
		?>
	</div>
	<div class="display-table">
		<div class="search-image">
			<a rel="nofollow" href="<?php echo $href; ?>" target="_blank">
				<img src="<?php echo $pic_url; ?>" alt="<?php echo $title; ?>" onerror="this.onerror=null;this.src='<?php echo $pic_error_url; ?>';">
			</a>
		</div>

		<div class="search-content secret-content">
			<div class="search-head clearfix">
				<div class="pull-left">
					<p class="search-company"><?php echo $company; ?></p>
				</div>
				<div class="pull-right">
					<div class="new-label">
					<?php if ($isNew): ?>
						NEW
					<?php endif; ?>
					</div>
					<div class="search-work">提供元：<?php echo $site_name; ?></div>
				</div>

			</div>

			<div class="search-salary">
				<span>給与</span> <p><?php echo $salary; ?></p>
			</div>

			<div class="search-keyword">
				条件：
				<?php
					foreach ($labels as $i => $label): 
						$catLengCount += mb_strlen($label);
						if($catLengCount >= $maxCatLeng) { break; } 
				?>
					<span class="i-condi"><?php echo $label; ?></span>
				<?php endforeach; ?>
			</div>

			<div class="clearfix search-foot">
				<a rel="nofollow" class="brick-btn brick-green" href="<?php echo $href; ?>"target="_blank" >詳細を確認</a>
			</div>
		</div>
	</div>
</div>