<script type="text/javascript" src="/js/new-search.js"></script>
<script>
	$(document).ready(function() {
	    windowHeight = $(window).height();
	    boxSeachHeight = windowHeight*57.3/100;
	    $('#box_search').css('max-height', boxSeachHeight + 'px');
	});
</script>
<div id="popup_search_box" class="popup-search-box">
	<div class="popup-content box-search">
		<div class="top-box-header">
			<span><img src="/img/only-logo-white.png"> 地域を選択する</span>
			<span class="close-button" id="close_popup">
				<i></i>
				<i></i>
				close
			</span>
		</div>
		<div class="top-box-title">
			<span class="back-to-previous-step"><img src="/img/arrow_back.png" id="back_to_previous_step"></span>
			<span class="title">地域を選択</span>
		</div>
		<div class="search-box-metal" id="box_search">
			<div class="metal-item">
				<span class="i-label">北海道・東北</span>
				<span class="i-change"></span>
			</div>
			<div class="metal-item">
				<span class="i-label">関東</span>
				<span class="i-change"></span>
			</div>
			<div class="metal-item">
				<span class="i-label">北陸・甲信越</span>
				<span class="i-change"></span>
			</div>
			<div class="metal-item">
				<span class="i-label">東海</span>
				<span class="i-change"></span>
			</div>
		</div>
	</div>
</div>