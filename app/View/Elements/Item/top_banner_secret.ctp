<?php 
	$secret_result = $this->requestAction('elements/secret_top_banner');
?>
<div class="top-banner-secrect">
	<div class="no-padding container">
		<div class="banner-secrect-header ipad-padding">
		<div class="clearfix">
			<div class="pull-left">
				<img src="/img/logo-obi.png" alt="">
				<div class="banner-slogan">
					<strong>シークレット求人は応募特典付き！</strong>
					<span>シークレット求人はコールナビでしか見られない、採用特典付きお得な求人が満載☆</span>
				</div>
			</div>
			<div class="pull-right">
				<a href="/secret" class="btn-green-gardient">シークレット求人をもっと見る ＞</a>
			</div>
		</div>
	</div>
		<div class="banner-secrect-list">
		<ul>
			<?php foreach ($secret_result as $secret): ?>
					<?php 
						$value 			= $secret['CareerOpportunity'];
						//$link 			= $this->Html->url(array('controller' => 'secret', 'action' => 'detail',$value['id']), true);
						$link 			= $this->app->createSecretDetailHref($value);
						$image 			= $this->webroot."upload/secret/companies/".$secret['corporate_prs']['corporate_logo'];
						
						$companyName 	= $secret['corporate_prs']['company_name'];
						$employment 	= $value['employment'];
						$salary 		= strip_tags($value['salary']);
						$job			= $value['job'];
						
						if(mb_strlen($job)> 40) 
						{	
							$job = mb_substr($job,0,40,'UTF-8').'...';
						}
					
						if(mb_strlen($salary)> 28) 
						{	
							$salary = mb_substr($salary,0,28,'UTF-8').'...';
						}
					
						if(mb_strlen($companyName)> 20) 
						{	
							$companyName = mb_substr($companyName,0,12,'UTF-8').'...';
						}
					
						$employmentArr = explode(',',$employment);
			?>
		
			<li>
				<div class="item-secret-banner">
					<div class="item-wp">
						<!-- <div class="top-part"><?php echo $salary; ?></div> -->
						<div class="content-part">
							<a href="<?php echo $link; ?>">
								<img src="<?php echo $image; ?>" />
							</a>
							<div class="i-cat-group">
								<?php foreach ($employmentArr as $label): ?>
									<span class="i-cat"><?php echo $label; ?></span>
								<?php endforeach; ?>
							</div>
						</div>
						<div class="bottom-part">
							<a href="<?php echo $link; ?>">
							<p><?php echo $job; ?></p>
							<h3><?php echo $companyName; ?></h3>
							</a>
						</div>
						<div class="top-part"><?php echo $salary; ?></div>
					</div>
				</div>
			</li>
			<?php endforeach; ?>
		</ul>
	</div>
	</div>
	<div class="banner-secret-bottom"></div>
</div>