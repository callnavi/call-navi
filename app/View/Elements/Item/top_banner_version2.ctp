<?php
	/*
		requestAction('elements/search_total')
		-> go: app/Controller/ElementsController.php -> call: search_total()
		
		Configure::read('webconfig.area_data')
		-> go: app/Config/webconfig.php -> get: $config['webconfig']['area_data']
	*/
	$search_total = $this->requestAction('elements/search_total');
	$secret_total = $this->requestAction('elements/secret_total');
	$area_data =  Configure::read('webconfig.area_data');
	$employment_data = Configure::read('webconfig.employment_data');
?>


<div class="top-banner">
	<div class="background">
		<div class="container">
			<div class="clearfix">
				<img src="/img/banner/top_02.jpg">
				<img src="/img/banner/top_03.jpg">
			</div>
		</div>
	</div>

	<div class="focus-search-bar">
		<div class="container">
			<h2 class="present"><strong>“働きやすいコールセンター”</strong>が探せる求人サイト</h2>
			<div class="search-wrap-box">
				<form action="/search" method="GET" data-seo="submit">
					<div class="custom-textbox area">
						<input data-search="area" id="selectArea" class="select-area" placeholder="地域を選択" type="text" data-popup="">
					</div>
					<div class="custom-select">
						<select name="em" id="em" data-search="em">
							<option value="">雇用形態を選択</option>
							<?php foreach ($employment_data as $k=>$v): ?>
								<option value="<?php echo $k; ?>">
									<?php echo $v; ?>
								</option>
								<?php endforeach; ?>
						</select>
					</div>
					<span class="multiply"></span>
					<div class="custom-textbox">
						<input data-search="keyword" placeholder="フリーキーワード" type="text" name="keyword">
					</div>
					<span class="equal-symbol">=</span>
					<button type="submit" class="btn-focus-search"></button>
					<input id="" class="result-area" type="hidden" name="area">
					<input type="hidden" class="data-em" name="data_em" value="0">
					<input type="hidden" class="data-step" name="data_step" value="0">
					<input type="hidden" class="data-group" name="data_group" value="0">
					<input type="hidden" class="data-province" name="data_province" value="0">
					<input type="hidden" class="data-city" name="data_city" value="0">
					<input type="hidden" class="data-town" name="data_town" value="0">
					<input type="hidden" class="event-before-open-popup" value="">
					<input type="hidden" class="event-after-close-popup" value="">
				</form>
			</div>

			<p class="statistical">現在のコールセンター求人数<strong><?php echo number_format($search_total,0); ?></strong>件</p>
		</div>
	</div>
</div>

<?php echo $this->element('Item/search_box_area_popup'); ?>