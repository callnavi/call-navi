    <section class="advertisement--concierge">
      <div class="advertisement__inner">
        <div class="advertisement__item--left">
          <h4><img src="/img/concierge/banner_link/title_ads_concierge.png" alt="コールセンターのお仕事探しは、求人コンサルタントにお任せください！！"></h4>
          <p class="advertisement--concierge__txt01">私たち、求人コンサルタントが、あなたのご要望に合わせて、ぴったりな求人をご案内します。</p><img class="advertisement--concierge__img01" src="/img/concierge/banner_link/concierge_link_chat_box.png">
          <div class="advertisement--concierge__cv"><a class="link-to-concierge" href="/concierge"><img src="/img/concierge/banner_link/concierge_link_btn.png"></a>
            <div class="advertisement--concierge__txt02">24時間<br><span>相談無料</span></div>
          </div>
        </div>
        <div class="advertisement__item--right"><img src="/img/concierge/banner_link/concierge_link_right.png"></div>
      </div>
    </section>