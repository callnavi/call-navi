<div class="top-banner">
	<div class="background-full">
		<div class="container">
			<div class="clearfix">
				<img src="/img/banner/top_concierge_sp.png">
			</div>

            <div class="text text-center">
                <h1>コールセンターのことなら、<br>私たちにお任せください！</h1>
                <p>私たち、求人コンサルタントが、<br>あなたのご要望に合わせて、<br>
                    親身にご提案させていただきます。</p>
            </div>
            <div class="cirle-green">
                相談<br>無料
            </div>
		</div>
	</div>
</div>
