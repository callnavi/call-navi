<?php 
$career = $CareerOpportunity;
$corporate = $corporate_prs;

$title = $career['job'];
$company = $corporate['company_name'];
$labels = explode(',',$career['employment']);
$benefit = strip_tags($career['benefit']);
$business = $career['business'];
if(empty($corporate['corporate_logo']))
{
	$pic_url = '/img/instead/img_no-image_' . sprintf("%02d", rand(1, 10)) . '.jpg';
}
else
{
	$pic_url = $this->webroot."upload/secret/companies/".$corporate['corporate_logo'];
}

$salary = mb_substr(strip_tags($career['hourly_wage']),0,35,'utf-8');
if(mb_strlen($salary)>25)
{
	$salary = mb_substr($salary,0,25,'UTF-8').'...';
}

$clip_title = $title;
if(mb_strlen($clip_title)>35)
{
	$clip_title = mb_substr(strip_tags($clip_title),0,35,'utf-8').'...';
}

if(mb_strlen($company)>25)
{
	$company = mb_substr($company,0,25,'UTF-8').'...';
}


$isNew = 0;
//604800 = 7 days

//Sort category follows length
usort($labels, function($a, $b) {
    return strlen($a) - strlen($b);
});

$catLengCount = 0;
$maxCatLeng = 30;

$href = $this->Html->url(array('controller' => 'secret', 
							   'action' => 'detail',$career['id']), true);
?>

<div class="clearfix search-box">
	<div class="pull-left search-image">
		<a href="<?php echo $href; ?>" target="_blank">
			<img src="<?php echo $pic_url; ?>" alt="<?php echo $title; ?>">
		</a>
	</div>
	<div class="pull-right search-content secret-content">
		<div class="search-head">
			<?php echo $company; ?>
			<?php if ($isNew): ?>
				<span class="new-label">NEW</span>
			<?php endif; ?>
		</div>
		<div class="benefit"><?php echo $benefit; ?></div>
		<div class="search-title">
			<a href="<?php echo $href; ?>"target="_blank" ><?php echo $clip_title; ?></a>
		</div>
		
		
		<div class="search-keyword">
			雇用形態：
			<?php
				foreach ($labels as $i => $label): 
					$catLengCount += mb_strlen($label);
					if($catLengCount >= $maxCatLeng) { break; } 
			?>
				<span class="i-cat"><?php echo $label; ?></span>
			<?php endforeach; ?>
		</div>
		<div class="search-salary">給与：<?php echo $salary; ?></div>
		<div class="search-salary">業種：<?php echo $business; ?></div>
		
		<div class="clearfix search-foot">
			<div class="pull-right search-ref">
				<a class="brick-btn brick-gray" href="<?php echo $href; ?>"target="_blank" >詳細を確認</a>
			</div>
		</div>
	</div>
</div>