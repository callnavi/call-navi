<?php 
/*
	array(
		'page' => STRING_T,
		'parent' => array(
			array('link' => '/', 'title' => STRING_T),
			array('link' => '/', 'title' => STRING_T),
			array('link' => '/', 'title' => STRING_T)
		)
	)
*/

//set class breadcrumb #345
$controller_page = strtolower($this->params['controller']); 
$header_class = "";


if ($controller_page == 'contents' || $controller_page == 'pretty' || $controller_page	== 'about' || $controller_page	== 'contact' || $controller_page == 'company'){
		$header_class = "headerlink--secondary";
}else if($controller_page == 'secret'  ){
	$route_data = explode("/", $_SERVER['REQUEST_URI']);
	if(isset($route_data[2])){
		if($route_data[2] == ""){
			$header_class = "headerlink";	
		}else{
			$header_class = "headerlink--secondary";
		}
	}else{
		$header_class = "headerlink";	
	}
}else{
	$header_class = "headerlink";
}
?>

<div class="no-padding <?php echo($header_class); ?>">
	<a href="<?php echo $this->webroot; ?>" rel="nofollow">コールセンター特化型求人サイト「コールナビ」</a><span>&gt;</span>
	<?php if (isset($parent)): ?>
		<?php foreach($parent as $item):?>
		<a href="<?php echo $item['link']; ?>"><?php echo $item['title']; ?></a><span>&gt;</span>
		<?php endforeach; ?>
	<?php endif; ?>
	<?php if (isset($page)): ?>
	<p><?php echo $page; ?></p>
	<?php endif; ?>
</div>