<?php

	$modulus = 2;
	$haft  = (int)$modulus/2;
 
	if(($modulus+1) >= $pageCount)
	{
		$start = 1;
		$end = $pageCount;
	}
	else
	{
		if(($currentPage - $haft) <= 1)
		{
			$start = 1;
			$end = $start + $modulus;
		}
		else
		{
			if($currentPage > ($pageCount - $haft))
			{
				$end = $pageCount;
				$start = $end - $modulus;
			}
			else
			{
				$start = $currentPage - $haft;
				$end = $currentPage + $haft; 
			}
		}
	}
?>
<div class="clearfix"></div>

<div class="df-padding">
	<div class="article-box">
		<div class="display-table text-center">
			
			<div class="owl-nav">
				<?php if($currentPage <= 1): ?>
					<div class="owl-prev owl-prev-cus opacity"></div>
				<?php else: ?>
					<a href="<?php echo $this->Html->general_url($currentPage-1); ?>">
						<div class="owl-prev owl-prev-cus"></div></a>
				<?php endif; ?>

				<?php if($currentPage >= $pageCount): ?>
					<div class="owl-next owl-next-cus opacity"></div>
				<?php else: ?>
					<a href="<?php echo $this->Html->general_url($currentPage+1); ?>">
						<div class="owl-next owl-next-cus"></div></a>
				<?php endif; ?>
			</div>

			<span class="paginate_center">
				<?php echo $currentPage. ' / ' .$pageCount; ?>
			</span>


		</div>
	</div>
</div>
