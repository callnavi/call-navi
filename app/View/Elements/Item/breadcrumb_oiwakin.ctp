<?php 
/*
	array(
		'page' => STRING_T,
		'parent' => array(
			array('link' => '/', 'title' => STRING_T),
			array('link' => '/', 'title' => STRING_T),
			array('link' => '/', 'title' => STRING_T)
		)
	)
*/	$routeparam = $this->request->params; 
?>

<div class="no-padding headerlink">
	<a href="<?php echo $this->webroot; ?>" rel="nofollow">トップ</a><span>&gt;</span>
	<?php if (isset($routeparam['action'])): ?>
		<?php if ($routeparam['action'] == "index"): ?>
			<p><?php echo "お祝い金について" ?></p>
		<?php elseif ($routeparam['action'] == "confirm"): ?>
			<a href="/oiwaikin" rel="nofollow">お祝い金について	</a><span>&gt;</span>
			<p><?php echo "お祝い金申請確認" ?></p>
		<?php elseif ($routeparam['action'] == "thanks"): ?>
			<a href="/oiwaikin" rel="nofollow">お祝い金について	</a><span>&gt;</span>
			<p><?php echo "お祝い金申請完了" ?></p>
		<?php endif; ?>
	<?php endif; ?>

</div>