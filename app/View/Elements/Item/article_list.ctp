<?php
//Default
$article_table = 'econtents';
$article_title = 'Title';
$article_image = 'featured_picture';
$article_view = 'view';
$article_created = 'created';
?>
<div class="main-list">
<ol>
<?php foreach ($list as $item):

		$title 		= $item[$article_table][$article_title];
		$org_title 	= $title;
		$image 		= $item[$article_table][$article_image];
		$view 		= $item[$article_table][$article_view];
		$isNew 		= $item[0]['totalHour']<= 3;
		$idUrl 		= $item[$article_table]['id'];

		$dt = new DateTime($item[$article_table][$article_created]);
		$createDate = $dt->format('Y.m.d');

		if(mb_strlen($title) > 28)
		{
			$title = mb_substr($title,0,28,'UTF-8').'...';
		}

		$cateList = array();
		$cateNames = explode(',',$item['group_cat']['cat_names']);
		$cateAlias = explode(',',$item['group_cat']['cat_alias']);
		$firstCateAlias = $cateAlias[0];
		

		//MAKE IMAGE LINK
		if (!empty($image)){
			$pos = strpos($image, "http://");
			if($pos === false)
			{
				$image = '/upload/news/'.$image;
			}
			else
			{
				$image = $this->app->proxy_image($image);
			}
		}

		//MAKE CATEGORY
		foreach($cateNames as $i=>$cate)
		{
			if(isset($cateAlias[$i]))
			{
				$cateList[] = array(
					'href' => "news/".$cateAlias[$i],
					'name' => $cate
				);
			}
		}
		//MAKE HREF
		$href = "/news/$firstCateAlias/$idUrl";		
	?>
	<li>
		<div class="item">
			<div class="top-part">
				<a href="<?php echo $href; ?>" title="<?php echo $org_title; ?>">
					<img src="<?php echo $image; ?>" alt="<?php echo $org_title; ?>">
				</a>
				<span class="i-view"><?php echo $view; ?> <small>view</small></span>
			</div>
			<div class="content-part">
				<div class="clearfix">
					<div class="pull-left">
						<span class="i-date"><?php echo $createDate; ?></span>
						<?php if($isNew){?>
							<span class="i-new">NEW</span>
						<?php } ?>
					</div>
					<div class="pull-right">
						<?php foreach ($cateList as $cat): ?>
							<a class="i-cat" href="<?php echo $cat['href']; ?>"><?php echo $cat['name']; ?></a>
						<?php endforeach; ?>
					</div>
				</div>
				<p class="i-title">
					<a href="<?php echo $href; ?>" title="<?php echo $org_title; ?>">
						<?php echo $title; ?>
					</a>
				</p>
			</div>
		</div>
	</li>
	<?php endforeach; ?>
</ol>
</div>