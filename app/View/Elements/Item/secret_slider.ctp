<?php 
	$secret_result = $this->requestAction('elements/secret_slider');
?>
<script src="/js/slider/jssor.slider.mini.js"></script>
<script src="/js/slider/secretSlider.js"></script>

<div class="slider-column secret-slider">
	<div class="slider-header">
		シークレット求人
	</div>
	
	<div class="slider-content">
		<div id="secretSlider" class="slider-wp">
			<div id="slider-mode" data-u="slides" class="slides">
			<?php foreach ($secret_result as $secret): ?>
					<?php 
						$value 			= $secret['CareerOpportunity'];
						$link 			= $this->Html->url(array('controller' => 'secret', 'action' => 'detail',$value['id']), true);
						$image 			= $this->webroot."upload/secret/companies/".$secret['corporate_prs']['corporate_logo'];
						
						$companyName 	= $secret['corporate_prs']['company_name'];
						$employment 	= $value['employment'];
						$salary 		= strip_tags($value['salary']);
						$job			= $value['job'];
						
						if(mb_strlen($job)> 60) 
						{	
							$job = mb_substr($job,0,60,'UTF-8').'...';
						}
					
						if(mb_strlen($salary)> 35) 
						{	
							$salary = mb_substr($salary,0,35,'UTF-8').'...';
						}
					
						if(mb_strlen($companyName)> 20) 
						{	
							$companyName = mb_substr($companyName,0,12,'UTF-8').'...';
						}
					
						$employmentArr = explode(',',$employment);
				?>
			
				<div data-p="80.125" style="display: none;">
					<div class="item">
						<div class="top-part">
							<a href="<?php echo $link; ?>">
								<img data-u="image" src="<?php echo $image; ?>" />
							</a>
						</div>
						<div class="content-part">
							<p class="i-title">
								<a href="<?php echo $link; ?>">
									<?php echo $job; ?>
								</a>
							</p>
						</div>
						<div class="bottom-part">
							<div>
								<span><?php echo $companyName; ?></span>
								
								<?php foreach ($employmentArr as $label): ?>
									<span class="i-label"><?php echo $label; ?></span>
								<?php endforeach; ?>
							</div>
							<div>
								<span class="i-salary"><?php echo $salary; ?></span>
							</div>
						</div>
					</div>
				</div>
				<?php endforeach; ?>
			</div>

      		<div data-u="navigator" class="slider-nav" data-autocenter="0">
            	<div data-u="prototype"></div>
        	</div>
        	
        	<span data-u="arrowleft" class="arrow-left" data-autocenter="2"></span>
        	<span data-u="arrowright" class="arrow-right" data-autocenter="2"></span>	
       	</div>
	</div>
</div>