<div class="container no-padding">
	<div class="row contact-info">
		<div class="width-100 format-image">
			<a class="link-to-contact" href="#contact_area"><img src="/img/concierge/concierge_contact_sp.png" > </a>
		</div>
		<div class="width-100 format-image">
           	<a href="tel:<?php echo $group = Configure::read('webconfig.secret_telephone'); ?>">
            	<img src="/img/concierge/concierge_contact_info_sp.jpg" >
			</a>
        </div>
        <div class="width-100 format-image">
           	<a href="https://line.me/R/ti/p/%40zgv0276t">
            	<img src="/img/concierge/concierge_line_sp.jpg" >
			</a>
        </div>
	</div>
</div>

