<?php
    $data = $this->requestAction('elements/news_similar_aticles/'.$category);
?>
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 contentnew detaispimg">
    <?php foreach($data as $item):
        $catIds = explode(',',$item['group_cat']['cat_ids']);
        $catNames = explode(',',$item['group_cat']['cat_names']);
        $catAlias = explode(',',$item['group_cat']['cat_alias']);?>
        <div class="col-xs-3 col-sm-3 col-md-4 col-lg-4 sayimg ">
            <a  href="<?php echo $this->Html->url(array('controller' => 'news', 'action' => 'detail', 'category_alias'=>$catAlias[0], 'slug'=>$item['econtents']['id']), true);?>">
                <?php if (!empty($item['econtents']['featured_picture'])){
                    $pos = strpos($item['econtents']['featured_picture'], "http://");
                    if($pos !== false)
                        $src = $item['econtents']['featured_picture'];
                    else
                        $src= '/upload/news/'.$item['econtents']['featured_picture'];
                    ?>
                    <img src="<?php echo $src ;?>"/>
                <?php } ?>
            </a>
        </div>
        <div class="col-xs-9 col-sm-9 col-md-8 col-lg-8 saytext ">
            <a style="float: none;" href="<?php echo $this->Html->url(array('controller' => 'news', 'action' => 'detail', 'category_alias'=>$catAlias[0], 'slug'=>$item['econtents']['id']), true);?>"><h4><?php echo strlen($item['econtents']['Title'])> 50 ? mb_substr(  $item['econtents']['Title'],0,50,'UTF-8').'...' : $item['econtents']['Title'] ;?> </h4></a>
            <?php for($x = 0; $x < count($catAlias); $x++):
                $href = '/news/'.$catAlias[$x];
                ?>

                <a href="<?php echo $href; ?>" style="float: left">
                    <p>
                        <?php echo $catNames[$x];?>
                    </p>
                </a>
            <?php endfor;?>
        </div>
    <?php endforeach;?>
</div>
