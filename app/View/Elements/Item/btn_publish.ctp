<div  class="no-padding preview">
	<?php echo $this->Form->create('PublishJob', array('type' => 'file','class' => 'form-horizontal'));
	$btn = $status == 0 ? 'パブリッシュ' : '非公開' ;
	echo $this->Form->input('status',
		array(
			'type' => 'hidden',
			'id'=>'status',
			'value' =>$status == 0 ? 1 :0,
		));

	?>

	<input type="submit" class="btn btn-primary pull-right" value="<?php echo $btn ;?>"/>
	<?php echo $this->Form->end(null); ?>
</div>