<div class="verticle_art_box">
	<div class="verticle_art_box--cat">
		<?php if (!empty($cateList)) { ?>
			<?php foreach ($cateList as $cat): ?>
				<?php 
					$cat['category'] = $cat['name'];
					$cat_href = $this->app->createContentCategoryHref($cat);
                    $catvalidate = false;
                    if($cat['name'] == '企業インタビュー'){
                        $catvalidate = true;    
                        
                    }
				?>
				<a class="i-cat-green" href="<?php echo $cat_href; ?>"><?php echo $cat['name']; ?></a>
			<?php endforeach; ?>
		<?php } ?>
	</div>
	<a href="<?php echo $href; ?>" title="<?php echo $org_title; ?>">
		<div class="mark-image">
			<div class="mark-image--wp fix-height-160">
				<img src="<?php echo $image; ?>" alt="<?php echo $org_title; ?>">
			</div>
		</div>

		<div class="verticle_art_box--title mg-top-5">
			<?php echo $org_title; ?>
		</div>
    </a>
    <div class="verticle_art_box--bottom clearfix mg-top-5">
    	<?php if (!empty($createDate)) { ?>
			<div class="pull-left">
				<time class="verticle_art_box--time"><?php echo $createDate; ?></time>
			</div>
		<?php } ?>
		<?php if (!empty($view)) { ?>
			<div class="pull-right">
				<span class="verticle_art_box--view" style="<?php 
                    if($catvalidate == true){
                        echo('display:none;');    
                    } ?>"><?php echo $view; ?> view</span>
			</div>
		<?php } ?>
    </div>
</div>