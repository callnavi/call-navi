<?php  
if(empty($pic_url))
{
	$pic_url = '/img/instead/img_no-image_' . sprintf("%02d", rand(1, 21)) . '.jpg';
}
else
{
	if($pic_url[0] != "/")
	{
		$pic_url = $this->app->proxy_image($pic_url);
	}
}
$pic_error_url = '/img/instead/img_no-image_' . sprintf("%02d", rand(1, 10)) . '.jpg';
$salary = mb_substr(strip_tags($salary),0,37,'utf-8');
$clip_title = mb_substr(strip_tags($title),0,50,'utf-8');
$company = strip_tags($company);

$isNew = 0;
//604800 = 7 days
$timeInAWeek = time()-604800;
if($updatetime > $timeInAWeek)
{
	$isNew = 1;
}

//Sort category follows length
usort($labels, function($a, $b) {
    return strlen($a) - strlen($b);
});

$catLengCount = 0;
$maxCatLeng = 15;

$encodeId = $this->app->jobUrlEncode($unique_id);
$href = '/search/job/'.$encodeId;
?>

<div class="df-padding y_search_sp">
	<div class="article-box">
		<p class="title"><?php echo $title; ?></p>
		<div class="display-table">
			<div class="search-image">
				<a rel="nofollow" target="_blank" href="<?php echo $href; ?>">
					<img src="<?php echo $pic_url; ?>" alt="<?php echo $title; ?>" onerror="this.onerror=null;this.src='<?php echo $pic_error_url; ?>';">
				</a>
			</div>

			<div class="search-content">
				<span class="label-brick-inline">給与</span> 
				<span class="salary"><?php echo $salary; ?></span>
				<hr class="secret-hr">
				<span class="company text-ellipsis-100"><?php echo $company; ?></span>
			</div>
		</div>
		<br>
		<a rel="nofollow" target="_blank" class="y_search_sp_btn" href="<?php echo $href; ?>">詳細を確認する</a>

		<div class="site_name">提供元：<?php echo $site_name; ?></div>


	</div>
</div>

