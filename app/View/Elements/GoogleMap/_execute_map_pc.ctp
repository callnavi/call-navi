<?php
	echo $this->Html->script('init-googlemap');
	echo '<script src="https://maps.google.com/maps/api/js?libraries=geometry&key='.Configure::read('webconfig.google_map_api_key').'"></script>';
?>	
<script>
	$( document ).ready(function() {
		var direction = '<a style="font-size:12px" href="https://maps.google.com/maps?ll=<?php echo $CareerOpportunity['lat'];?>,<?php echo $CareerOpportunity['lng'];?>&z=16&t=m&hl=ja-JP&gl=JP&mapclient=embed&daddr=<?php echo $CareerOpportunity['map_location']; ?>" target="_blank"> ルート検索 ▶︎</a>';
		// set google map
		// input lat, lng , inforwindow, zoom
		<?php $latLng = !empty($CareerOpportunity['lat']) && !empty($CareerOpportunity['lng']) ? $CareerOpportunity['lat'].' , '.$CareerOpportunity['lng'] : '0,0'; ?>
		var contentString = "<h4 style='font-weight: bold;'><?php echo $CareerOpportunity['branch_name']; ?> "+direction+"</h4>"+
							"<div style='height: 24px;'><?php echo $CareerOpportunity['work_location']; ?></div>";
		initMap(<?php echo $latLng;?>,contentString, 13);
	});
</script>