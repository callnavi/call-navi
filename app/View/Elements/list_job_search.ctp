<?php $data = $this->requestAction('elements/list_job_search/'.$tag); ?>

<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding sliderbottom">
    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding slider-header" >
        <h1>BLOG<span>新着CCブログ</span></h1>
<!--         <h2>ブログ＆コラム</h2> -->
    </div>

    <?php
    if($data){

    foreach($data[0] as $k=>$v){
        $link = $v['url'];
        if(empty($v['pic_url']))
        {
            $pic_url = '/img/instead/img_no-image_' . sprintf("%02d", rand(1, 10)) . '.jpg';
        }else{
            $pic_url = $v['pic_url'];
        }
    ?>

        <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 no-padding sliderimg">
            <a href="<?php echo $link;?>"><img src="<?php echo $pic_url;?>" /></a>
        </div>
        <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8 no-padding slidertext">
            <a href="<?php echo $link;?>"><h4><?php echo mb_substr( $v['title'],0,50,'UTF-8');?> </h4></a>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 no-padding sliderborder">
        </div>
    <?php         }  }?>


</div>
