<?php 
	$dt = new DateTime($createDate);
	$_createDate = $dt->format('Y.m.d');
?>
<div class="pick-up-box">
	<div class="main-list">
		<div class="item">
			<div class="top-part">
				<a href="<?php echo $url; ?>" title="<?php echo $title; ?>">
					<img src="<?php echo $image; ?>" alt="<?php echo $title; ?>">
				</a>
				<span class="i-view"><?php echo $view; ?> <small>view</small></span>
			</div>
			
			<div class="i-title">
				<a href="<?php echo $url; ?>" title="<?php echo $title; ?>"><?php echo $title; ?></a>
				<div class="i-date"><?php echo $_createDate; ?></div>
			</div>
			<span class="i-pick-up">pick-up</span>
		</div>
	</div>
</div>