<?php
$data = $this->requestAction('elements/Blogs_info/'.$category_alias);
$data = $data[0];

$link = $this->Html->url(array('controller' => 'blog', 'action' => 'index','category_alias'=>$data['BlogsCategory']['category_alias']), true);
?>


<div class="banner category-banner">
	<div class="background">
		<img width="260" class="img_radius" src="<?php echo $this->webroot."upload/blogs-category/".$data['BlogsCategory']['pic'] ;?>" />
	</div>
	<div class="blog-avatar">
		<div class="avatar"><img src="<?php echo $this->webroot."upload/blogs-category/avatar/".$data['BlogsCategory']['avatar'] ;?>" /></div>
	</div>
	
	<div class="blog-view">
		<span class="clip-of-logo"></span> <big><?php echo empty($view_number)?1:$view_number; ?></big> view
	</div>
</div>