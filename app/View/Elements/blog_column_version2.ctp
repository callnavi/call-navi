<?php $data = $this->requestAction('elements/list_blog_view'); ?>

<div class="slider-column slider-blog">
    <div class="slider-header">ブログ＆コラム</div>
    <div class="slider-blog-content">
		<ol>
		<?php
		if($data){
			foreach($data as $index=>$item){
				$CreateDate = new DateTime($item['Blogs']['date_post']);
				$link = $this->Html->url(array('controller' => 'blog', 'action' => 'detail',"category_alias"=>$item['blogs_category']['category_alias'], 'id'=>$item['Blogs']['id']), true);
				$title = mb_strlen($item['Blogs']['title'])> 18 ? 
							mb_substr(  $item['Blogs']['title'],0,18,'UTF-8')."..." 
							: 
							$item['Blogs']['title'] ;

				$description = mb_strlen($item['Blogs']['metadesc'])> 45 ? 
							mb_substr(  $item['Blogs']['metadesc'],0, 45,'UTF-8')."..." 
							: 
							$item['Blogs']['metadesc'];
		 ?>
			<li>
				<div class="item">
					<div class="top-part">
						<a href="<?php echo $link;?>">
							<?php if ($item['Blogs']['pic']): ?>
							<img src="<?php echo $this->webroot."upload/blogs/".$item['Blogs']['pic'] ;?>" />
							<?php else: ?>
							<img src="" />
							<?php endif; ?>
						</a>
					</div>
					<div class="content-part">
						<p class="i-title"><a href="<?php echo $link;?>"><?php echo $title; ?></a></p>
						<p class="i-des"><?php echo $description; ?></p>
					</div>
					<div class="bottom-part">
						<span class="model person_<?php echo $index; ?>"></span>
					</div>
				</div>
			</li>
		<?php } }?>
		</ol>
	</div>
</div>