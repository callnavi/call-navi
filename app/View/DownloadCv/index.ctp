<?php
$this->set('title', 'コールナビ｜Download CV');
$this->start('css'); ?>
<link rel="stylesheet" href="<?php echo $this->base;?>/admin/css/login.css" type="text/css" media="all">
<?php $this->end('css');
?>



<div class="login-container">

	<?php echo $this->Form->create('Download', array(
		'url' => '/downloadcv/get?id='.$id,
		'class' => 'form-horizontal',
    )); ?>
		<div class="form-group">
			<label for="password" class="control-label">パスワード</label>
			<input type="password" name="password" class="form-control">
			<?php if(!empty($errorMess)){ ?>
				<label  generated="true" class="error"><?php echo $errorMess ;?></label>
			<?php } ?>
		</div>

		<div class="form-group">
			<input type="submit" class="btn btn-primary pull-right" value="ダウンロード"/>
		</div>

	<?php echo $this->Form->end(null); ?>
	<?php if (!empty($cv)): ?>
		<h5>ダウンロード:</h5>
		<?php foreach ($cv as $file): ?>
			<div>
				<h5>- <a target="_blank" href="<?php echo $file['link'] ?>"><?php echo $file['name']; ?></a></h5>
			</div>
		<?php endforeach; ?>
	<?php endif; ?>
</div>