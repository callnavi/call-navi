<?php
$this->set('title', 'コールナビ｜Download CV');
$this->start('css'); ?>
<meta name="viewport" content="width=device-width,initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
<link rel="stylesheet" href="<?php echo $this->base;?>/admin/css/login.css" type="text/css" media="all">
<style>
	html{
		width: 100% !important;
	}
	.login-container{
		width: 100%;
		height: auto;
		position: static;
		padding: 40px;
    	margin-top: 30px;
	}
</style>
<?php $this->end('css');
?>



<div class="login-container">

	<?php echo $this->Form->create('Download', array(
		'url' => '/downloadcv/get?id='.$id,
		'class' => 'form-horizontal',
    )); ?>
		<div class="form-group">
			<label for="password" class="control-label">パスワード</label>
			<input type="password" name="password" class="form-control">
			<?php if(!empty($errorMess)){ ?>
				<label  generated="true" class="error"><?php echo $errorMess ;?></label>
			<?php } ?>
		</div>

		<div class="form-group">
			<input type="submit" class="btn btn-primary pull-right" value="ダウンロード"/>
		</div>

	<?php echo $this->Form->end(null); ?>
	<?php if (!empty($cv)): ?>
		<h5>ダウンロード:</h5>
		<?php foreach ($cv as $file): ?>
			<div>
				<h5>- <a target="_blank" href="<?php echo $file['link'] ?>"><?php echo $file['name']; ?></a></h5>
			</div>
		<?php endforeach; ?>
	<?php endif; ?>
</div>