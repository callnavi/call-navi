<?php if(!empty($_GET['m'])){ ?>
			<?php 
			$errorMess = '';
			switch($_GET['m'])
			{
				case 'outofdate': 
					$errorMess = 'This link was out of date.';
					break;
				case 'invalid': 
					$errorMess = 'This link is invalid.';
					break;
				case 'nofile': 
					$errorMess = 'This file doesn\'t exit to download.';
					break;
				default:
				break;
			}
								 
			?>
<?php } ?>

<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title>
        Error
	</title>
	<link rel="stylesheet" type="text/css" href="/css/error404.css?1485250651"/></head>
<body>
	<div class="container">
		

<div class="content">
	<div class="title">
		<a href="/"><img src="/img/logo_pc.png"></a>
		<p><small><?php echo $errorMess; ?></small></p>
		<br>
		<br><a href="/" class="content-link">TOPページに戻る</a>
	</div>
</div>	</div>
</body>
</html>
