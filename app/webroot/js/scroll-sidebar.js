/*scroll following screen v1.0*/
var scrollColumn = function (_obj, _stopPoint, _plusForHeight) {
	var _this = this;
	var makeStopedPosition = function () {
		return _this.stopPoint - _this.top - _this.heightContent - _this.plusForHeight;
	}
	var makeMaxY = function () {
		return _this.stopPoint;
	}
	
	var makeEnableScroll = function () {
		return (_this.stopPoint - _this.top) > (_this.heightContent + _this.plusForHeight);
	}
	
	this.obj 				= _obj;
	
	this.fixClassName 		= 'isFixed';
	this.bottomClassName 	= 'isBottom';
	this.topClassName 		= 'isTop';
	
	//=> .isFixed { position: fixed; bottom: 30px; } <= bottom: this.bottomMarginWhenScrolling
	this.bottomMarginWhenScrolling = 30;
	
	this.borderScreen		= 100;
	this.plusForHeight 		= _plusForHeight;
	this.stopPoint 			= _stopPoint;
	this.heightContent 		= _this.obj.outerHeight(true);
	this.top 				= _this.obj.offset().top;
	this.stopedPosition 	= makeStopedPosition();
	this.maxY 				= makeMaxY();
	this.enableScroll 		= makeEnableScroll();
	this.bottomOfSidebar	= this.top + this.heightContent;
	
	this.scrollBottomEvent = function(){};
	
	this.setstopPoint = function (_newstopPoint) {
		_this.stopPoint 		= _newstopPoint;
		_this.reset();
	}
	
	this.reset = function () {
		_this.heightContent 		= _this.obj.outerHeight(true);
		//_this.top 					= _this.obj.offset().top;
		_this.stopedPosition 		= makeStopedPosition();
		_this.maxY 					= makeMaxY();
		_this.enableScroll 			= makeEnableScroll();
	}
	
	//Event
	$(window).scroll(function (evt) {
		
		if(!_this.enableScroll) return;

		var y = $(this).scrollTop() + $(window).height();
		
		if (y > _this.bottomOfSidebar) {
			
			//is bottom
			if (y >= _this.maxY) {
				_this.obj.addClass(_this.bottomClassName)
						 .removeClass(_this.fixClassName)
						 .removeClass(_this.topClassName);
				
				_this.scrollBottomEvent!==undefined&&_this.scrollBottomEvent(_this);
			//is top
			} else {	
				_this.obj.addClass(_this.fixClassName)
						 .removeClass(_this.topClassName)
						 .removeClass(_this.bottomClassName);
			}
		
		//scrolling
		} else {
			
			_this.obj.addClass(_this.topClassName)
						 .removeClass(_this.fixClassName)
						 .removeClass(_this.bottomClassName);
		}
	});

	return _this;
}