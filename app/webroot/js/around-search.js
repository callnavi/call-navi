$(document).ready(function () {
	
	var map;
	var indexNumber = 1;
	var elevator;
	var myOptions = {
		zoom: 13,
		mapTypeId: 'terrain'
	};
	var addMarkerWithNumber = function(number, markerScr, onloadCallback)
	{
		var dataURL = '';
		var img = new Image();
		img.setAttribute('crossOrigin', 'anonymous');
		img.src = markerScr;
		img.onload = function() {	
			var canvas = document.createElement("canvas");
			canvas.width = this.width;
			canvas.height = this.height;
			var ctx = canvas.getContext("2d");
			
			ctx.drawImage(this, 0, 0);
			ctx.font = "11px Arial";
			ctx.fillStyle = 'white';
			ctx.textAlign = "center"; 
			ctx.fillText(number,15,14);
			dataURL = canvas.toDataURL("image/png");
			
			onloadCallback(dataURL);
		};
	}
	var addMarkerIntoMap = function(relatedCC, indexNumber, latlng){
		var _title = "「"+relatedCC.name+"」\n"+relatedCC.address;
		
		if(relatedCC.isInCity)
		{
			var imgIcon = '/img/blue-dot-marker_2.png';
			addMarkerWithNumber(indexNumber, imgIcon, function(dataURL){
					new google.maps.Marker({
						position: latlng,
						map: map,
						title: _title,
						icon: dataURL,
						optimized:false
					});
			});
		}
		else
		{
			imgIcon = 'http://maps.google.com/mapfiles/ms/icons/green-dot.png';
			new google.maps.Marker({
						position: latlng,
						map: map,
						title: _title,
						icon: imgIcon,
						optimized:false
			});	
		}
	}
	var createFlagForAroundAddress = function (relatedCC, indexNumber) {
		var latlng = new google.maps.LatLng(relatedCC.lat, relatedCC.lng);
		if(latlng !== undefined && latlng.lat() != 0 && latlng.lng() != 0)
		{
			addMarkerIntoMap(relatedCC, indexNumber, latlng);
		}
		else
		{
			var __address = relatedCC.address.replace('　','');
			$.getJSON('http://maps.googleapis.com/maps/api/geocode/json?address=' + __address + '&sensor=false', null, function (data) {			
				if(data.results.length > 0)
				{
					var p = data.results[0].geometry.location;
					var getlatlng = new google.maps.LatLng(p.lat, p.lng);
					addMarkerIntoMap(relatedCC, indexNumber, getlatlng);
				}
			});
		}
	}
	
	var loadOriginAddress = function(_origin){
		var latlng = new google.maps.LatLng(_origin.lat, _origin.lng);
		var _title = _origin.name + "\n"+_origin.address;
		
		if(latlng !== undefined && latlng.lat() != 0 && latlng.lng() != 0)
		{
			//SET CENTER
			map.setCenter(latlng);
			new google.maps.Marker({
				position: latlng,
				map: map,
				title: _title
			});
		}
		else
		{
			var __address = _origin.address.replace('　','');
			$.getJSON('http://maps.googleapis.com/maps/api/geocode/json?address=' + __address + '&sensor=false', null, function (data) {			
				if(data.results.length > 0)
				{
					var p = data.results[0].geometry.location;
					var latlng = new google.maps.LatLng(p.lat, p.lng);
					//SET CENTER
					map.setCenter(latlng);
					new google.maps.Marker({
						position: latlng,
						map: map,
						title: _title
					});
				}
			});
		}
	}

	map = new google.maps.Map($('#map_canvas')[0], myOptions);
	var origin = __origin;
	var aroundAddress = __aroundAddress;
	
	loadOriginAddress(origin);
	
	var x = 0;
	function intervalTrigger() {
	  return window.setInterval( function() {
		if(x >= aroundAddress.length)
		{
			window.clearInterval(__id);
			return;
		}
		createFlagForAroundAddress(aroundAddress[x],x+1);
		x++;
	  }, 200 );
	};
	var __id = intervalTrigger();
	
});