$(function() {

	objSearch = {
		step: 0,
		dataAtStep4: 0,
		groupId: 0,
		groupName: '',
		provinceId: 0,
		provinceName: '',
		cityId: 0,
		cityName: '',
		townId: 0,
		townName: '',
		listDataGroup: [],
		listDataProvince: [],
		listDataCity: [],
		listDataTown: []
	}

	var list_event_after_close = {
		'show_popup_search_PC' : function(){
			$('.top-popup-search').fadeIn();
		},
		'show_popup_search_SP' : function(){
			$('#nav-search').fadeIn();
		},
		'default': function(){

		}
	};

	var list_event_before_open = {
		'close_popup_search_PC' : function(){
			$('.top-popup-search').fadeOut();
		},
		'close_popup_search_SP': function(){
			$('#nav-search').fadeOut();
		},
		'default': function(){

		}
	};

	//var selectArea = $('#selectArea');
	var lastClickedArea = '';
	var selectArea = $('.select-area');
	var closeButton = $('#close_popup');
	var popupSearch = $('#popup_search_box');
	var buttonBackToPreviousStep = $('.back-to-previous-step');
	var showPopupSearch = function(objSearch){
		$('.back-to-previous-step').hide();
		beforeOpenPopupSearch();
		//when the first time show popup search
		objSearch.dataAtStep4 = 0;
		if(objSearch.step == 0)
		{
			getDataByAjax(objSearch);
		}
		if(objSearch.step != 1)
		{
			buttonBackToPreviousStep.fadeIn();
		}
		//displayBackButton(objSearch.step);
		popupSearch.fadeIn();
		$('body').addClass('disable-scroll');
	}

	var beforeOpenPopupSearch = function()
	{
		var funcName = $('.event-before-open-popup').val()||'default';
		list_event_before_open[funcName]&&list_event_before_open[funcName]();
	}

	var afterClosePopupSearch = function(){
		var funcName = $('.event-after-close-popup').val()||'default';
		list_event_after_close[funcName]&&list_event_after_close[funcName]();
	}

	var	closePopupSearch = function(){
		setDataAfterSearch();
		popupSearch.fadeOut();
		$('body').removeClass('disable-scroll');
		afterClosePopupSearch();
		//function from search_popup.js
		if($.countcondition)
		{
			$.countcondition();
		}
	}

	//show data in view
	var setDataForPopup = function(listData, step){
		var html = '';
		switch(step)
		{
			case 2:
				html += '<div class="metal-item" data-target="close"><span class="i-label">'+ objSearch.groupName +'全域</span><span class="i-change"></span></div>';
				break;
			case 3:
				html += '<div class="metal-item" data-target="close"><span class="i-label">'+ objSearch.provinceName +'全域</span><span class="i-change"></span></div>';
				break;
			case 4:
				html += '<div class="metal-item" data-target="close"><span class="i-label">'+ objSearch.cityName +'全域</span><span class="i-change"></span></div>';
				break;
		}
		for (var key in listData) 
		{
			var area = listData[key].area;
			html += '<div class="metal-item" data-id="'+ area.id +'"><span class="i-label">'+ area.name +'</span><span class="i-change"></span></div>';
		}
		$("#box_search").html(html);
	}

	var getDataByAjax = function(objSearch){
		var url = '';
		switch(objSearch.step)
		{
			case 0:
				url = '/area/getGroup';
			break;
				
			case 1:
				url = '/area/getProvinceByGroup/'+objSearch.groupId;
			break;
			
			case 2:
				var level = 1;
				url = '/area/getAreaByParentIDAndLevel/'+objSearch.provinceId+'/'+ level;
				
			break;
				
			case 3:
				var level = 2;
				url = '/area/getAreaByParentIDAndLevel/'+objSearch.cityId+'/'+ level;
			break;
			
			case 4:
				objSearch.dataAtStep4 = objSearch.provinceId;
				closePopupSearch();
			return;
		}
		$.ajax({url: url, 
			method: 'get', 
			dataType: 'json', 
			success: function(listData){
				if(listData)
				{
					objSearch.step = parseInt(objSearch.step) + 1;
					setDataForPopup(listData, objSearch.step);
					setListDataForObject(objSearch, listData);
					displayBackButton(objSearch.step);
				}
				else
				{
					closePopupSearch();
				}
			}
		});
	}

	//set data id and data name
	var setDataIdAndDataNameForStep = function(objSearch, dataId, dataName){
		switch(objSearch.step) {
		    case 1:
		    	objSearch.groupId = dataId;
				objSearch.groupName = dataName;
		        break;
		    case 2:
		        objSearch.provinceId = dataId;
				objSearch.provinceName = dataName;
		        break;
		    case 3:
		    	objSearch.cityId = dataId;
				objSearch.cityName = dataName;
		        break;
		    case 4:
		    	objSearch.townId = dataId;
				objSearch.townName = dataName;
				break;
		    default:
		}
	}

	var displayBackButton = function(step)
	{
		if(step != 1)
		{
			$('#popup_search_box .title').addClass('title-padding-r-4');
			buttonBackToPreviousStep.show();
		}
		else
		{
			$('#popup_search_box .title').removeClass('title-padding-r-4');
			buttonBackToPreviousStep.hide();
		}
	}

	//save data for object
	var setListDataForObject = function(objSearch, listData)
	{
		switch(objSearch.step) {
		    case 1:
		    	objSearch.listDataGroup = listData;
		        break;
		    case 2:
		        objSearch.listDataProvince = listData;
		        break;
		    case 3:
		    	objSearch.listDataCity = listData;
		        break;
		    case 4:
		    	objSearch.listDataTown = listData;
				break;
		    default:
		}
	}

	var setDataAfterSearch = function(){
		//var resultArea = $('#resultArea');
		var resultArea = $('.result-area');
		var dataGroup = $('.data-group');
		var dataProvince = $('.data-province');
		var dataCity = $('.data-city');
		var dataTown = $('.data-town');
		var dataStep = $('.data-step');
		var dataEm =  $('.data-em');

		dataStep.val(objSearch.step);

		var selectedEm = $('#selectEm option:selected').val() ? $('#selectEm option:selected').val() : $('#em option:selected').val();

		if(selectedEm)
		{
			dataEm.val(selectedEm);
		}
		
		
		var __groupName = objSearch.groupName;
		var __provinceName = objSearch.provinceName;
		var __cityName = objSearch.cityName;
		var __townName = objSearch.townName;

		var __groupId = objSearch.groupId;
		var __provinceId = objSearch.provinceId;
		var __cityId = objSearch.cityId;
		var __townId = objSearch.townId;

		dataGroup.val(objSearch.groupId);
		dataProvince.val(objSearch.provinceId);
		dataCity.val(objSearch.cityId);
		dataTown.val(objSearch.townId);

		switch(objSearch.step) {
		    case 4:
		    	if(objSearch.dataAtStep4 == 0)
		    	{
		        	selectArea.val(__groupName + ', ' + __provinceName + ', ' + __cityName);
		        	resultArea.val(__groupId + '-' + __provinceId + '-' + __cityId)
		        	dataTown.val(0);
		    	}
		    	else
		    	{
		    		selectArea.val(__groupName + ', ' + __provinceName + ', ' + __cityName + ', ' + __townName);
		    		resultArea.val(__groupId + '-' + __provinceId + '-' + __cityId + '-' + __townId)
		    	}
		        break;
		    case 3:
		    	selectArea.val(__groupName + ', ' + __provinceName);
		    	resultArea.val(__groupId + '-' + __provinceId)
		        dataTown.val(0);
				dataCity.val(0);
		        break;
		    case 2:
		    	selectArea.val(__groupName);
		    	resultArea.val(__groupId)
		    	dataTown.val(0);
				dataCity.val(0);
				dataProvince.val(0);
		        break;
		    case 1:
		    	selectArea.val('');
		    	resultArea.val('');
		    	dataTown.val(0);
				dataCity.val(0);
				dataProvince.val(0);
				dataGroup.val(0);
				break;
		    default:
		}
	}

	selectArea.bind('click touchstart',function(e){
		var device = $(e.target).attr('data-popup');
		if($(e.target).attr('data-popup'))
		{
			$('.event-before-open-popup').val("close_popup_search_" + device)
			$('.event-after-close-popup').val("show_popup_search_" + device)
		}
		else
		{
			$('.event-before-open-popup').val("");
			$('.event-after-close-popup').val("");
		}
		showPopupSearch(objSearch);
		e.preventDefault();
		return false;
	});
	
	closeButton.click(function(){
		closePopupSearch();
	});
	
	//event get data-id
	$('#box_search').click(function(e){
		//element was be clicked
		var target = e.target;
				
		var targetClass = $(target).attr('class');
		if(targetClass != "metal-item")
		{
			if($(e.target).parent().data('target') == "close")
			{
				closePopupSearch();
				return;
			}
			
			if(targetClass == "i-label")
			{
				dataName = $(target).text();
			}
			else if(targetClass == "i-change")
			{
				dataName = $(target).siblings('.i-label').text();
			}
			else
			{
				return;
			}
			dataId = $(target).parent().attr('data-id');
		}
		else
		{
			if($(e.target).data('target') == "close")
			{
				closePopupSearch();
				return;
			}
			
			dataId = $(target).attr('data-id');
			dataName = $(target).children('.i-label').text();
		}

		if(dataId)
		{
			setDataIdAndDataNameForStep(objSearch, dataId, dataName);
			//call ajax load data
			getDataByAjax(objSearch);
		}
	});

	//event close popup
	$('#popup_search_box').click(function(e){
		if($(e.target).attr('class') == "popup-search-box")
		{
			closePopupSearch();
		}
	});

	//event back
	$('#back_to_previous_step').click(function(){
		objSearch.step = objSearch.step - 1;
		switch(objSearch.step) {
		    case 3:
		    	setDataForPopup(objSearch.listDataCity, objSearch.step);
		        break;
		    case 2:
		        setDataForPopup(objSearch.listDataProvince, objSearch.step);
		        break;
		    case 1:
		    	buttonBackToPreviousStep.hide();
				$('#popup_search_box .title').removeClass('title-padding-r-4');
				setDataForPopup(objSearch.listDataGroup, objSearch.step);
		        break;
		    default:
		}
	});

	$("form").submit(function () {
		setDataAfterSearch();
	});
});