/*scroll following screen v1.0*/
var scrollColumn = function (_obj, _stopPoint, _fixClassName, _plusForHeight) {
	var _this = this;
	var makeStopedPosition = function () {
		return _this.stopPoint - _this.top - _this.heightContent - _this.plusForHeight;
	}
	var makeMaxY = function () {
		return _this.stopPoint - _this.heightContent;
	}
	
	var makeEnableScroll = function () {
		return (_this.stopPoint - _this.top) > (_this.heightContent + _this.plusForHeight);
	}
	
	this.obj 				= _obj;
	this.fixClassName 		= _fixClassName;
	this.plusForHeight 		= _plusForHeight;
	this.stopPoint 			= _stopPoint;
	this.heightContent 		= _this.obj.outerHeight(true);
	this.top 				= _this.obj.offset().top;
	this.stopedPosition 	= makeStopedPosition();
	this.maxY 				= makeMaxY();
	this.enableScroll 		= makeEnableScroll();
	
	this.setstopPoint = function (_newstopPoint) {
		_this.stopPoint 		= _newstopPoint;
		_this.reset();
	}
	
	this.reset = function () {
		_this.heightContent 		= _this.obj.outerHeight(true);
		//_this.top 					= _this.obj.offset().top;
		_this.stopedPosition 		= makeStopedPosition();
		_this.maxY 					= makeMaxY();
		_this.enableScroll 			= makeEnableScroll();
	}
	
	//Event
	$(window).scroll(function (evt) {
		
		if(!_this.enableScroll) return;

		var y = $(this).scrollTop();
		if (y > _this.top) {
			if (y >= _this.maxY) {
				_this.obj.removeClass(_this.fixClassName).css({
					position: 'absolute',
					top: _this.stopedPosition + 'px'
				});
			} else {
				_this.obj.addClass(_this.fixClassName).removeAttr('style');
			}

		} else {
			_this.obj.removeClass(_this.fixClassName).attr('style','');
		}
	});

	return _this;
}