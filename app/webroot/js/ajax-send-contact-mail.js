
function sendContactMail(url, arr){
    $.ajax({
        url: url,
        type: 'POST',
        // dataType: 'json',
        data: {name: arr[0], mail: arr[1], content: arr[2], companyName: arr[3], departmentNamePosition: arr[4],
        tel: arr[5], answer: arr[6] },
        success: function(result){
            return result;
        },
        error: function(jqXHR, textStatus, errorThrown){
            return errorThrown;
        }
    });
}
