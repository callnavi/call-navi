$(function() {
	function setCookie(cname, cvalue, exdays) {
		var d = new Date();
		d.setTime(d.getTime() + (exdays*24*60*60*1000));
		var expires = "expires="+ d.toUTCString();
		document.cookie = cname + "=" + cvalue + "; " + expires;
	}

	function getCookie(cname) {
		var name = cname + "=";
		var ca = document.cookie.split(';');
		for(var i = 0; i <ca.length; i++) {
			var c = ca[i];
			while (c.charAt(0)==' ') {
				c = c.substring(1);
			}
			if (c.indexOf(name) == 0) {
				return c.substring(name.length,c.length);
			}
		}
		return "";
	}
	
	/*
		if( cookie ) 
			show cookie
			set cookie
		else
			add cookie
	*/
	function process_history (prefix) {
		console.log(prefix);
		if(getCookie(prefix+"url") != "" && getCookie(prefix+"total") != "")
		{
			$('[data-history="url"]').attr('href', getCookie(prefix+"url"));
			$('[data-history="total"]').text('（'+getCookie(prefix+"total")+'件）');
		}
		setCookie(prefix+"url", window.location.href, 1);
		setCookie(prefix+"total", $('[data-total="result"]').text(), 1);
		console.log(document.cookie);
	}
	
	function remove_history (prefix) {
		
		$('[data-history="remove"]').click(function () {
			setCookie(prefix+"url", '', 1);
			setCookie(prefix+"total", '', 1);
			
			$('[data-history="url"]').attr('href', '#');
			$('[data-history="total"]').text('');
		});
	}
	
	if(window.location.pathname.indexOf('/secret') == 0)
	{
		process_history('secret');
		remove_history('secret');
	}
	else
	{
		process_history('search');
		remove_history('search');
	}
	
});
