//trunc function to limit string
String.prototype.trunc = function(n){
          return this.substr(0,n-1)+(this.length>n?'...':'');
};

//clipText class, need to declare trunc
var clipText = function(selector){
	
	this.saveOrigin = function () {
		selector.each(function (argument) {
			var $_this = $(this);
			if($_this.attr('data-org') === undefined)
			{
				$_this.attr('data-org', $_this.text());
			}
		});
	}
	
	this.clip = function(limit){
		selector.each(function (argument) {
			var subString = $(this).attr('data-org').trunc(limit);
			console.log('-------');
			console.log(subString);
			$(this).text(subString);
		});
	};
	
	this.saveOrigin();
}