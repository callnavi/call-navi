/*
	Setting: 
		#url to load pagition
		STRING __url
		
		#number of last page
		INT __lastPage
		
		#number of current page
		INT __page
*/
$(function() {
	var _loadedContent 	= false;
	var _page 			= setting.__page || 1;
	var _lastPage 		= setting.__lastPage || 1;
	var _url 			= setting.__url || "";
	var _containContents = $('#scroll_load');
	var _distanceToScrolling = 350;
	var _lazyLoad = true;
	
	if(_url == "")
	{
		console.log("No page to scrolling");
		return false;
	}
	
	var minFadeInValueForShowBox = 500;
	var maxFadeInValueForShowBox = 3000;

	var _makeRandomFadeIn = function()
	{
		var rd = Math.floor((Math.random() * maxFadeInValueForShowBox) + minFadeInValueForShowBox);
		return rd;
	}
	
	var _recallLazyLoad = function () {
		if(_lazyLoad)
		{
			$("img.lazy:not(.done)").lazyload({
				effect : "lazyDone",
			});
		}
	}
	
	var _loadingTemplate = function () {
		var html = '<div id="loading" class="show-more">'
                   + '<span class="loading"></span>'
                   + '</div>';
		return html;
	}
	
	_containContents.after(_loadingTemplate());
	
	var _loading 		= $('#loading');
	
	if (_lastPage <= _page) {
		$('[data-action="lazy-click"]').hide();
	}
	
	$('[data-action="lazy-click"]').click(function () {
		
		if (_lastPage <= _page) {
			return false;
		}
		
		if (_loadedContent == false) {

			_page = _page + 1;
			_loadedContent = true;
			
			if (_lastPage <= _page) {
				$(this).hide();
			}
			
			_loading.show();
			
			var url = _url + "?page=" + _page;
			
			$.ajax({
				url: url, 
				dataType: "json",
				success: function (result) {
					try {
						if (result != undefined) {
							for (var i in result) {
								_containContents.append(__contentTemplate(result[i])).fadeIn(_makeRandomFadeIn());
							}
						}
					} catch (e) {}
					_loading.hide();
					_recallLazyLoad();
					_loadedContent = false;
				}
			});
		}
	});
});