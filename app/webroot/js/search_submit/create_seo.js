$(function() {
	$('form[data-seo="submit"]').submit(function (e) {

		var _this = $(this);
		var hotkey = _this.find('[data-search="hotkey"]:checked');
		
		var area = _this.find('[data-search="area"]').val();
		var arrArea = area.split(',');
		
		
		var em = _this.find('[data-search="em"]');
		var em_val = '';
		switch(em.prop('nodeName'))
		{
			case 'SELECT':
				var op = em.find('option:selected');
				if(op.val() != "")
				{
					em_val = op.text();
				}
				break;
			case 'INPUT':
				if(em.val() != "")
				{
					em_val = em.val();
				}
				break;
		}
		
		var keyword = _this.find('[data-search="keyword"]').val()||"";
		
		var URL = window.location.pathname.indexOf('/secret') == 0 ? '/secret' : '/search';
		if(arrArea.length)
		switch(arrArea.length) {
			case 1:
				var group = arrArea[0].trim();
				if(group != "")
				{
					URL += ('/'+group);
				}
				break;
			default:
				for(var i=1; i<arrArea.length; i++)
				{
					URL += ('/'+arrArea[i].trim());
				}
				break;
		}
		
		
		if(em_val != "" && em_val != "選択しない")
		{
			URL += '/'+em_val;
		}

		if(hotkey.length)
		{
			for(var i = 0; i < hotkey.length; i++)
			{
				URL += '/' + $(hotkey[i]).data('value');
			}
		}
		
		if(keyword != "")
		{
			URL += '/'+keyword;
		}
		
		window.location = URL;
//		console.log(em_val);
//		console.log(keyword);
//		console.log(hotkey);
//		console.log(URL);
		e.preventDefault();
	});
});