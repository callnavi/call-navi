$(function() {
	var clipSearchTitle = new clipText($('.search-box .search-title a'));
	var clipSearchSalary = new clipText($('.search-box .search-salary p'));

	var ctrTextLimit = function(){
		var screenSize = window.screen.width;
		if(screenSize <= 480)
		{
			clipSearchTitle.clip(22);
			clipSearchSalary.clip(16);
		}
		else
		{
			clipSearchTitle.clip(50);
			clipSearchSalary.clip(37);
		}
	}
	
	ctrTextLimit();
	window.onresize = function(){
		ctrTextLimit();
	}

	//click advance search
	$(".div_advance").hide();
	$("#btn_advance").click(function(e) {
		e.preventDefault();
		if( $('.div_advance').css('display') == 'none' ) {
			$(".div_advance").slideDown();
		} 
		else {
			$(".div_advance").slideUp();
		}

		countcondition();
	});

	var countcondition = function()
	{
		var query = '';
		$('.custom-checkbox input[type="checkbox"]:checked').each(function () {
			query += '&hotkey[]=' + $(this).val();
		})
		query += '&area='+$('#area').val();
		query += '&em='+$('#em').val();
		query += '&keyword='+$('input[name="keyword"]').val();
		
		$.get('/search/countcondition?'+query,function (data) {
			if(data === undefined)
			{
				$('.search-horizon-result strong').text(0);
			}
			else
			{
				$('.search-horizon-result strong').text(data);
			}
		})
	}
	
	var handleCheckList = function () {
		
			var checkPars = '';
			var otherPars = '';

			$('#checklist input[name="hotkey[]"]').each(function () {
				checkPars += '&hotkey[]=' + $(this).val();
			})

			//		otherPars += '&area='+$('#area').val();
			//		otherPars += '&em='+$('#em').val();
			//		otherPars += '&keyword='+$('input[name="keyword"]').val();

			$('.custom-checkbox input[type="checkbox"]:not(:checked)').each(function () {
				var thisCountQuery = checkPars + otherPars + '&hotkey[]=' + $(this).val();
				var _this = $(this);
				$.get('/search/countcondition?'+thisCountQuery,function (data) {
					var result = 0;
					if(data !== undefined)
					{
						result = data;
					}
					_this.next().find('.vir-content i').text(result);
				});
			});

			var checkedPars = '';
			$('#checklist input[name="hotkey[]"]').each(function () {
				var curVal = $(this).val();
				checkedPars += '&hotkey[]=' + curVal;
				var thisCountQuery = checkedPars + otherPars + '&hotkey[]=' + $(this).val();
				console.log(thisCountQuery);
				$.get('/search/countcondition?'+thisCountQuery,function (data) {
					var result = 0;
					if(data !== undefined)
					{
						result = data;
					}
					$('#cschk_' + curVal + ' .vir-content i').text(result);
				});
			});
	}
		
	$('#area').change(function () {
		countcondition();
	});
	$('#em').change(function () {
		countcondition();
	});
	
	$('.custom-checkbox input[type="checkbox"]').change(function() {
		var val = $(this).val();
		if(this.checked)
		{
			$('#checklist').append('<input type="hidden" id="chk_'+val+'" name="hotkey[]" value="'+val+'">');
		}
		else
		{
			$('#chk_'+val).remove();
		}
		
		countcondition();
	});
	
	$('input[name="keyword"]').focusout(function(){
		countcondition();
	});

});

