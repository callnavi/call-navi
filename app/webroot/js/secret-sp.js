$(function(){
    $('#tab-bottom').click(function(){
		$("html, body").animate({ scrollTop: 0 }, "slow");
    });

    //handle tab click
    $('#tab2').hide();
	$('#tab3').hide();

	$('li[data-select="tab1"]').click(function(){
		$('li[data-select="tab1"]').addClass('active');
		$('li[data-select="tab2"]').removeClass('active');
		$('li[data-select="tab3"]').removeClass('active');

		$('#tab1').fadeIn();
		$('#tab2').hide();
		$('#tab3').hide();
	});

	$('li[data-select="tab2"]').click(function(){
		$('li[data-select="tab2"]').addClass('active');
		$('li[data-select="tab1"]').removeClass('active');
		$('li[data-select="tab3"]').removeClass('active');

		$('#tab2').fadeIn();
		$('#tab1').hide();
		$('#tab3').hide();
	});

	$('li[data-select="tab3"]').click(function(){
		$('li[data-select="tab3"]').addClass('active');
		$('li[data-select="tab1"]').removeClass('active');
		$('li[data-select="tab2"]').removeClass('active');

		$('#tab3').fadeIn();
		$('#tab1').hide();
		$('#tab2').hide();
	});
});

