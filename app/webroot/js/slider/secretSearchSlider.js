$(function() {
	var fullWidth = $(document).width()||window.screen.width;
	
	fullWidth = Math.max(fullWidth, 1100);
	var fullHeight = 340;
	var itemWitdh = 630;
	var align = (fullWidth-itemWitdh)/2;
	
	
	var secretSliderOptions = {
	  $AutoPlay: true,
	  $SlideDuration: fullWidth,
	  $ScaleWidth: fullWidth,
	  $ScaleHeight: fullHeight,
	  $SlideWidth: itemWitdh,
	  $SlideHeight: fullHeight,
	  $Scale: false,
	  $Cols: 4,
	  $Align: align,
	  $ArrowNavigatorOptions: {
		$Class: $JssorArrowNavigator$
	  },
	  $BulletNavigatorOptions: {
		$Class: $JssorBulletNavigator$
	  }
	};

	var secretSlider = new $JssorSlider$("secretSearchSlider", secretSliderOptions);
	//window.ddd = secretSlider;
	var ScaleSlider = function () {
		var refSize = secretSlider.$Elmt.parentNode.clientWidth;
		if (refSize) {
			refSize = Math.max(refSize, 1100);
			secretSlider.$SetScaleWidth(refSize);
			
			//disable scale of jssor
			$('#secretSearchSlider').css('height', fullHeight);
			$('#secretSearchSlider > div').css({
												'transform':'scale(1)', 
												'height':fullHeight,
												'width':refSize,
											   });
		}
		else {
			window.setTimeout(ScaleSlider, 30);
		}
	}
	$(window).resize(function () {
		ScaleSlider();
	});
	
	var ua = window.navigator.userAgent;
	var msie = ua.indexOf("MSIE ");

	if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))  // If Internet Explorer
	{
		ScaleSlider();
	}
});