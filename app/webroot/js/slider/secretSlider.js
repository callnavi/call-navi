$(function() {
	var secretSliderOptions = {
	  $AutoPlay: true,
	  $SlideDuration: 1000,
	  $ScaleWidth: 700,
	  $SlideWidth: 416,
	  $Cols: 2,
	  $Align: 142,
	  $ArrowNavigatorOptions: {
		$Class: $JssorArrowNavigator$
	  },
	  $BulletNavigatorOptions: {
		$Class: $JssorBulletNavigator$
	  }
	};

	/*id: secretSlider*/
	var secretSlider = new $JssorSlider$("secretSlider", secretSliderOptions);
});