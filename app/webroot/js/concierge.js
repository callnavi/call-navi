/**
 */

$( function( $ ) {
    var inputs = $( 'input[name=email],input[name=phone]' );
    inputs.each(function( index ) {
        inputs.not( this ).prop( 'required', !$( this ).val().length );
    });
    inputs.on( 'input', function() {
        // Sets the required property of the other input to false if this input is not empty.
        inputs.not( this ).prop( 'required', !$( this ).val().length );
    });
	
	$('#concierge_form').on('submit', function(){
		
		if(!$('[name="name"]').val())
		{
			alert('お名前は必須です。');
			$('[name="name"]').focus();
			return false;
		}


		if(!$('[name="phone"]').val()){
			alert('電話番号は必須です。');
			$('[name="phone"]').focus();
			return false;
		}else{
			if(!/^\d{10,11}$/.test($('[name="phone"]').val()))
			{
				alert('電話番号が不正です。');
				$('[name="phone"]').focus();
				return false;
			}
		}



		if($('#input_confirm:checked').length < 1)
		{
			alert('「個人情報の取り扱い」に同意されていません');
			return false;
		}
	});
} );