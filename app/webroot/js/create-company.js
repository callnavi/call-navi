
$(function(){
    $('a.src').click(function(){
		$('html,body').delay(0).animate({ scrollTop: ($($(this).attr('href')).offset().top) },500, 'easeInOutQuint');
		
		$('a.src').parents("li").removeClass('active');
		$(this).parent().addClass('active');
        return false;
    });
	
	$('body').scrollspy({ target: '#menunav' });

	$(window).scroll(function(ev){
		if(window.scrollY > 200)
		{
			//hien
			$('#btnGoto').fadeIn(200);
		}
		else
		{
			//an
			$('#btnGoto').fadeOut(200);
		}
	});
});