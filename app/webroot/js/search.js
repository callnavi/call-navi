$(window).load(function () {
	var stopPoint = $('#search-result').offset().top + $('#search-result').outerHeight();
	if ($('#right-column').length > 0) {
		$keyboxHeight = 0;
		if($('#currentkey-box').length > 0)
		{
			$keyboxHeight = -$('#currentkey-box').outerHeight(true)-20;
		}
		var scrollRight = new scrollColumn($('#right-column'), stopPoint, 'right-menu-fix', $keyboxHeight);
	}
	
	
	
	var countcondition = function()
	{
		var query = '';
		$('.custom-checkbox input[type="checkbox"]:checked').each(function () {
			query += '&hotkey[]=' + $(this).val();
		})
		query += '&area='+$('#selectHorizonArea').val();
		query += '&em='+$('#selectHorizonEm').val();
		query += '&keyword='+$('#txtHorizonKeyword').val();
		
		$.get('/search/countcondition?'+query,function (data) {
			if(data === undefined)
			{
				$('.search-horizon-result strong').text(0);
			}
			else
			{
				$('.search-horizon-result strong').text(data);
			}
		})
	}

	var handleCheckList = function () {
		
			var checkPars = '';
			var otherPars = '';

			$('#checklist input[name="hotkey[]"]').each(function () {
				checkPars += '&hotkey[]=' + $(this).val();
			})

			//		otherPars += '&area='+$('#area').val();
			//		otherPars += '&em='+$('#em').val();
			//		otherPars += '&keyword='+$('input[name="keyword"]').val();

			$('.custom-checkbox input[type="checkbox"]:not(:checked)').each(function () {
				var thisCountQuery = checkPars + otherPars + '&hotkey[]=' + $(this).val();
				var _this = $(this);
				$.get('/search/countcondition?'+thisCountQuery,function (data) {
					var result = 0;
					if(data !== undefined)
					{
						result = data;
					}
					_this.next().find('.vir-content i').text(result);
				});
			});

			var checkedPars = '';
			$('#checklist input[name="hotkey[]"]').each(function () {
				var curVal = $(this).val();
				checkedPars += '&hotkey[]=' + curVal;
				var thisCountQuery = checkedPars + otherPars + '&hotkey[]=' + $(this).val();
				console.log(thisCountQuery);
				$.get('/search/countcondition?'+thisCountQuery,function (data) {
					var result = 0;
					if(data !== undefined)
					{
						result = data;
					}
					$('#cschk_' + curVal + ' .vir-content i').text(result);
				});
			});
	}
	
	$('#selectHorizonArea').change(function () {
		
		countcondition();
	});
	$('#selectHorizonEm').change(function () {
		countcondition();
	});
	
	$('.custom-checkbox input[type="checkbox"]').change(function() {
		var val = $(this).val();
		if(this.checked)
		{
			$('#checklist').append('<input type="hidden" id="chk_'+val+'" name="hotkey[]" value="'+val+'">');
		}
		else
		{
			$('#chk_'+val).remove();
		}
		countcondition();
	});

	$('#txtHorizonKeyword').focusout(function(){
		countcondition();
	});
	
});