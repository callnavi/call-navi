/*
{
	"Contents": {
		"id": "573",
		"priority": "0",
		"status": "1",
		"site_id": "0",
		"province_id": "0",
		"url": "\/contents\/skill\/karaoke",
		"category": "\u30b3\u30fc\u30eb\u30bb\u30f3\u30bf\u30fc\u306e\u4ed5\u4e8b\u3068\u306f\uff1f",
		"view": "30",
		"title_hint": "\u77e5\u3063\u5f97\u60c5\u5831",
		"title": "\u30ab\u30e9\u30aa\u30b1\u4e0a\u624b\u306f\u3001\u30c6\u30ec\u30a2\u30dd\u4e0a\u624b\uff01\uff1f\u610f\u5916\u306a\u5171\u901a\u70b9\u3068\u3001\u5fc3\u306b\u523a\u3055\u308b\u30c8\u30fc\u30af\u306e\u30b3\u30c4\u3068\u306f\uff1f",
		"image": "\/img\/ccwork\/079\/main001.jpg",
		"category_alias": "skill",
		"title_alias": "karaoke",
		"created": "2016-05-20 17:54:49",
		"isNew": false,
		"createdDate": "2016.05.20",
		"splited_title": "\u30ab\u30e9\u30aa\u30b1\u4e0a\u624b\u306f\u3001\u30c6\u30ec\u30a2\u30dd\u4e0a\u624b\uff01\uff1f\u610f\u5916\u306a\u5171\u901a\u70b9\u3068\u3001\u5fc3\u306b\u523a\u3055..."
	},
	"category": {
		"category": "\u30b9\u30ad\u30eb\u30fb\u30c6\u30af\u30cb\u30c3\u30af",
		"cateList": [{
			"href": "\/contents\/skill",
			"name": "\u30b9\u30ad\u30eb\u30fb\u30c6\u30af\u30cb\u30c3\u30af"
		}]
	},
	"0": {
		"totalHour": "2614"
	}
}
 */

var __contentTemplate = function (data) {
	var strVar = "";
		
	strVar += "<li>";
	strVar += "<div class='article-box'>";
	strVar += "<div class='display-table'>";
	strVar += "<div class='search-image'>";
	strVar += "<a href='"+ data.Contents.url +"' title='" + data.Contents.title + "'>";
	strVar += "<img src='" + data.Contents.image + "' alt='" + data.Contents.title + "'>";
	strVar += "</a>";
	strVar += "</div>";
	strVar += "<div class='search-content'>";
	strVar += "<div class='top-part clearfix'>";
	strVar += "<div class='pull-left'>";
	strVar += "<time>" + data.Contents.createdDate; + "</time>";
	strVar += "</div>";
	strVar += "<div class='pull-right'>" + data.Contents.view+" view</div>";
	strVar += "</div>"
	strVar += "<div class='middle-part'>"
	strVar += "<a href='"+ data.Contents.url +"'>" + data.Contents.splited_title + "</a>";
	strVar += "</div>";
	strVar += "<div class='bottom-part text-right'>";
	for (var i = 0; i < data.category.cateList.length; i++) {
		strVar += "<a class=\"i-cat\" href=\"" + data.category.cateList[i].href + "\">" + data.category.cateList[i].name + "<\/a>";
	}
	
	return strVar;
};

