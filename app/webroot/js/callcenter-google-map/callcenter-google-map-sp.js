$(document).ready(function () {
    //variables
    var map;
    var indexNumber = 1;
    var elevator;
    var myOptions = {
        zoom: 12,
        mapTypeId: 'terrain',
        minZoom: 4,
        //maxZoom: 9,
    };
	//Bounds to calcular zoom size
	var bounds = new google.maps.LatLngBounds();
	
    var ccDetail = $('#cc_detail');
    //outsite variable
    var aroundAddress = __aroundAddress;
	

    //TODO: METHODS
    /*TODO: modal*/
	var processKeyword = function(keyword)
	{
		return keyword.replace('会社 ','会社');
	}
	
    var showModal = function () {
        $('body').addClass('disable-scrolling');
        $('.top-popup-search').fadeIn();
    };

    var closeModal = function (className) {
        if (className == 'close-button' || className == 'top-popup-search') {
            $('body').removeClass('disable-scrolling');
            $('.top-popup-search').fadeOut();
        }
    };

    var getData = function (indexNumber) {
        return aroundAddress[indexNumber];
    };

    var addData = function (indexNumber) {
        var data = getData(indexNumber);
        setTotalOfCallCenter(data);
        setSearchHref(data);
        setDirectionMap(data);
        setHP(data['hp']);
        ccDetail.find('[data-cc="company"]').text(data['name']);
        ccDetail.find('[data-cc="address"]').text(data['address']);
        ccDetail.find('[data-cc="tel"]').text(data['tel']);
        ccDetail.find('[data-cc="cat"]').text(data['business']);
    };
    var setHP = function (hp) {
        if (hp.indexOf('http') == 0) {
            var anchor = '<a href="' + hp + '" target="_blank">' + hp + '</a>'
            ccDetail.find('[data-cc="hp"]').html(anchor);
        }
        else {
            ccDetail.find('[data-cc="hp"]').html(hp);
        }
    };

    var setDirectionMap = function (data) {
        var dir = 'https://maps.google.com/maps?ll=' + data['lat'] + ',' + data['lng'] + '&z=16&t=m&hl=ja-JP&gl=JP&mapclient=embed&daddr=' + data['name'] + '+' + data['address'];
        ccDetail.find('[data-cc="dir"]').attr('href', dir);
    };

    var setSearchHref = function (data) {
        ccDetail.find('[data-cc="keyword"]').val(processKeyword(data['name']));
    };

    var setTotalOfCallCenter = function (data) {
        ccDetail.find('[data-cc="total"]').text('...');
        var url = '/search/countcondition?keyword=' + processKeyword(data['name']);
        $.get(url, function (result) {
            ccDetail.find('[data-cc="total"]').text(result);
        });
    };

    var showCallCenterModal = function (indexNumber) {
        addData(indexNumber);
        showModal();
    };


    var addMarkerWithNumber = function (number, markerScr, onloadCallback) {
        var dataURL = '';
        var img = new Image();
        img.setAttribute('crossOrigin', 'anonymous');
        img.src = markerScr;
        img.onload = function () {
            var canvas = document.createElement("canvas");
            canvas.width = this.width;
            canvas.height = this.height;
            var ctx = canvas.getContext("2d");

            ctx.drawImage(this, 0, 0);
            ctx.font = "11px Arial";
            ctx.fillStyle = 'white';
            ctx.textAlign = "center";
            ctx.fillText(number, 16, 19);
            dataURL = canvas.toDataURL("image/png");

            onloadCallback(dataURL);
        };
    };

    window.liCC = null;
    window.__marker = null;
    window.lisMarker = [];
    var addMarkerIntoMap = function (relatedCC, indexNumber, latlng) {
        var _title = "「" + relatedCC.name + "」\n" + relatedCC.address;

        var imgIcon = '/img/green-dot-maker.png';
        addMarkerWithNumber(indexNumber, imgIcon, function (dataURL) {
            var marker = new google.maps.Marker({
                position: latlng,
                map: map,
                title: _title,
                icon: dataURL,
                callcenterId: relatedCC.id,
                optimized: false
            });
            window.lisMarker.push(marker);
            //click event
            marker.addListener('click', function () {
                showCallCenterModal(indexNumber - 1);
            });

        });
    };

    var createFlagForAroundAddress = function (relatedCC, indexNumber) {
        var latlng = new google.maps.LatLng(relatedCC.lat, relatedCC.lng);
        if (latlng !== undefined && latlng.lat() != 0 && latlng.lng() != 0) {
            addMarkerIntoMap(relatedCC, indexNumber, latlng);
        } else {
            var __address = relatedCC.address.replace('　', '');
            $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address=' + __address + '&sensor=false', null, function (data) {
                if (data.results.length > 0) {
                    var p = data.results[0].geometry.location;
                    var getlatlng = new google.maps.LatLng(p.lat, p.lng);
                    addMarkerIntoMap(relatedCC, indexNumber, getlatlng);
                }
            });
        }
		return latlng;
    };

    var loadOriginAddress = function (_origin) {
        if (_origin == undefined) {
            _origin = {'lat': 35.6732619, 'lng': 139.570306, 'name': '', 'address': ''};
        }
        var latlng = new google.maps.LatLng(_origin.lat, _origin.lng);
        var _title = _origin.name + "\n" + _origin.address;

        if (latlng !== undefined && latlng.lat() != 0 && latlng.lng() != 0) {
            //SET CENTER
            map.setCenter(latlng);
        } else {
            var __address = _origin.address.replace('　', '');
            $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address=' + __address + '&sensor=false', null, function (data) {
                if (data.results.length > 0) {
                    var p = data.results[0].geometry.location;
                    var latlng = new google.maps.LatLng(p.lat, p.lng);
                    //SET CENTER
                    map.setCenter(latlng);
                }
            });
        }
    };
	
	var loadCenterScreen = function(){
		var ct = bounds.getCenter();
		if(ct !== undefined)
		{
			map.setCenter(ct);
			return;
		}
		
		var firstC = getData(0);
		if(firstC.lat != 0)
		{
			var latlng = new google.maps.LatLng(firstC.lat, firstC.lng);
			if (latlng !== undefined && latlng.lat() != 0 && latlng.lng() != 0) {
				map.setCenter(latlng);
				return;
			}
		}
		
		var tokyoCenter = new google.maps.LatLng(35.6732619, 139.570306);
		map.setCenter(tokyoCenter);
		
	}
	
	var addExtendToBound = function(LngLat)
	{
		if(LngLat !== undefined && 
		   LngLat.lng() > 125 
		   &&
		   LngLat.lng() < 145 
		   && 
		   LngLat.lat() < 46
		   &&
		   LngLat.lat() > 24
		  )
		{
			bounds.extend(LngLat);
		}
	}
	
	var fitBounds = function()
	{
		map.fitBounds(bounds);
	}
    //TODO: END METHODS


    //Load----------------------
    map = new google.maps.Map($('#map_canvas')[0], myOptions);

    loadOriginAddress(getData(0));
	if(aroundAddress.length > 0)
	{
		var x = 0;
		function intervalTrigger() {
			return window.setInterval(function () {
				if (x >= aroundAddress.length) {
					window.clearInterval(__id);
					fitBounds();
					loadCenterScreen();
					return;
				}
				var LngLat = createFlagForAroundAddress(getData(x), x + 1);
				addExtendToBound(LngLat);
				x++;
			}, 50);
		}

		var __id = intervalTrigger();
	}
    $('.top-popup-search').click(function (e) {
        closeModal(e.target.className);
    });

    $('.ul-callcenter-list li').click(function () {
        var index = $(this).data('index');
        showCallCenterModal(index - 1);
    });

//    //check list is null
//    var keySearch = $('input[name=keySearch]').val();
//    if(keySearch != null && keySearch != ''){
//        $('.callnavi-search-panel').css('height','50rem');
//    }


});