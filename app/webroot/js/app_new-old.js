/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

	'use strict';

	var clearInputText = function clearInputText() {
	    var $trigger = $('[data-txtClear-trigger]');
	    if (!$trigger.length) return;
	    $trigger.click(function () {
	        // console.log('click');
	        $(this).prev('[data-txtClear-target]').val('');
	    });
	};

	var toggleSearchBox = function toggleSearchBox() {
	    var $trigger = $('[data-toggleSearchBox-trigger]'),
	        $target = $('[data-toggleSearchBox-target]');
	    if (!$trigger.length) return;
	    $trigger.click(function () {
	        if ($target.is(':visible')) {
	            $target.fadeOut();
	        } else {
	            $target.fadeIn();
	        }
	    });
	};

	var toggleSearchMenu = function toggleSearchMenu() {
	    var $trigger = $('[data-searchMenu-trigger]'),
	        $target = $('[data-searchMenu-target]');
	    console.log('toggleSearchMenu');
	    $trigger.click(function (e) {
	        var $triggerElm = $(this);
	        $triggerElm.toggleClass('is_close');
	        if ($triggerElm.hasClass('is_close')) {
	            e.currentTarget.innerHTML = '<span>open</span>';
	        } else {
	            e.currentTarget.innerHTML = '<span>close</span>';
	        }
	        $triggerElm.parent($target).toggleClass('is_close');
	    });
	};

	$(document).ready(function () {
	    clearInputText();
	    toggleSearchMenu();
	    toggleSearchBox();
	});

/***/ })
/******/ ]);