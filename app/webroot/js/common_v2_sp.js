jQuery.fn.getTemplate = function() {
	var o = $(this[0]);
	return o.html().replace('<!--','').replace('-->','');
};

$(function() {
	var template = {
		'#content-template': function(template, data, appendToId){
			var appendObj = $(appendToId);
			for(var i in data)
			{
				
				var item = data[i];
				appendObj.append(template
								 .replace('{image}'	, item.Contents.image)
								 .replace('{title}'	, item.Contents.title)
								 .replace(/{url}/g	, item.Contents.url)
								 .replace('{view}'	, item.Contents.view)
								 .replace('{createdDate}'	, item.Contents.createdDate)
								 .replace('{category_href}'	, item.category.cateList[0].href)
								 .replace('{category_name}'	, item.category.cateList[0].name)
								 .replace('{isNew}'			, item.Contents.isNew)
								);
			}
		}
	};
	
	$('.func-go-to-top').click(function(){ 
        $('html,body').animate({ scrollTop: 0 }, 'slow');
        return false; 
    });
	
	$('#btn-search-open').click(function () {
		$('#nav-search').fadeIn();	
		$('body').addClass('disable-scroll');
	});

	$('.i-change').click(function () {
		$('#nav-search').fadeIn();	
		$('body').addClass('disable-scroll');
	});
	
	$('#btn-close-x').click(function () {
		$('#nav-search').fadeOut();	
		$('body').removeClass('disable-scroll');
	});

	//TODO: menu action
	$('#button-slide-open').click(function () {
		$('#nav-slide-menu').addClass('active');
		$('body').addClass('disable-scroll');
	});
	$('#button-slide-close, #nav-slide-menu .bg-overlay').click(function () {
		$('#nav-slide-menu').removeClass('active');
		$('body').removeClass('disable-scroll');
	});
	
	
	//TODO: for detail of content
	$('.image-rmp-content img').each(function () {
		$(this).wrap('<div class="image-rmp-wp"></div>');
	});
	
	//TODO: lazy loading page
	$('.lazy-loading-page').click(function () {
		var $this = $(this);
		var total = $this.data('total');
		var page = $this.data('page');
		if(page >= total)
		{
			return;
		}
		page = page + 1;
		var appendToId = $this.data('append-to');
		var templateId = $this.data('template');
		var link = $this.data('link');
		var templateHTML = $(templateId).getTemplate();
		//start
		$this.addClass('loading');
		
		$.ajax({
			url: link+'?page='+page, 
			dataType: "json",
			error: function(){
				$this.removeClass('loading');
			},
			success: function (result) {
				
				if(page >= total)
				{
					$this.remove();
				}
				
				try
				{		
					template[templateId]
					&&
					template[templateId](templateHTML, result, appendToId);
				}
				catch(e){
					console.log(e);
				}
				$this.data('page', page);
				$this.removeClass('loading');
			}
		});
		//end
		
	});
	
});	