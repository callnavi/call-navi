$(function() {
	var countUrl = request.countUrl || '';
	
	var open_search_box = function (inputs) {
		var area = inputs.area || '';
		var em = inputs.em || '';
		var keyword = inputs.keyword || '';
		var hotkey = inputs.hotkey || [];
		var total = inputs.total || 0;
		
		
		var popup = $('.top-popup-search');
		
		popup.find('[name="area"]').prop('selectedIndex', area);
		popup.find('[name="em"]').prop('selectedIndex', em);
		popup.find('[name="keyword"]').val(keyword);
		popup.find('[data-total="result"]').text(total);
		
		var hotkeyList = popup.find('[name="hotkey[]"]');
		for(var i=0; i< hotkeyList.length; i++)
		{
			//var hotkeyValue = parseInt(hotkeyList[i].value);
			var hotkeyValue = hotkeyList[i].value; 
			if(hotkey.indexOf(hotkeyValue) > -1)
			{
				$(hotkeyList[i]).prop('checked', true);
			}
			else
			{
				$(hotkeyList[i]).prop('checked', false);
			}
		}
		
		$('body').addClass('disable-scrolling');
		popup.show();
	}
	
	$('.history-box-metal .brick-btn').click(function () {
		open_search_box({'total': '...'});	
	});
	
	$('.banner-search-wrap-box .i-change').click(function () {
		
		open_search_box({
			area	: request.area || '',
			em		: request.em || '',
			keyword	: request.keyword || '',
			hotkey	: request.hotkey || [],
			total	: request.total || 0
		});	
	});
	
	$('.top-popup-search').click(function (e) {
		var className = e.target.className;
		if(className == 'close-button' || className == 'top-popup-search')
		{
			$('body').removeClass('disable-scrolling');
			$(this).hide();
		}
	});
		
	/*Count job*/
	var countcondition = function()
	{
		var query = '';
		$('.checkbox-i-button input[type="checkbox"]:checked').each(function () {
			query += '&hotkey[]=' + $(this).val();
		})
		//query += '&area='+($('#selectArea').val()||"");
		query += '&area='+($('.result-area').val()||"");
		query += '&em='+($('#selectEm').val()||"");
		query += '&keyword='+($('#txtKeyword').val()||"");
		
		$.get(countUrl+'?'+query,function (data) {
			if(data === undefined)
			{
				$('[data-total="result"]').text(0);
			}
			else
			{
				$('[data-total="result"]').text(data);
			}
		})
	}
	//public function to new-search.js
	$.countcondition = countcondition;

	$('#selectArea').change(function () {
		countcondition();
	});
	
	$('#selectEm').change(function () {
		countcondition();
	});

	$('#txtKeyword').focusout(function(){
		countcondition();
	});
	
	$('.checkbox-i-button input[type="checkbox"]').change(function() {		
		countcondition();
	});
});