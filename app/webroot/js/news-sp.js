$(function() {
	var clipSearchTitle = new clipText($('#wrapper-content .saytext h4'));
	
	var ctrTextLimit = function(){
		var screenSize = window.screen.width;
		if(screenSize <= 320)
		{
			clipSearchTitle.clip(27);
		}
		else
		{
			clipSearchTitle.clip(50);
		}
	}
	
	ctrTextLimit();
	window.onresize = function(){
		ctrTextLimit();
	}
});