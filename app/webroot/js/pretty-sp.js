$(function() {
	var closeModal = function (className) {
        if (className == 'close-button' || className == 'top-popup-search') {
            $('body').removeClass('disable-scroll');
            $('#popup_pretty_modal').fadeOut();
        }
    };
	
	$('[data-pretty="open"]').click(function () {

		var id = $(this).data('id');
		var data = $(this).attr('data-value');
		var json = $.parseJSON(data);
		var birth = json.birthday.split('-');
		var d = new Date();
		var n = d.getFullYear();
		var age = n - birth[0];
		var popup = $('#popup_pretty_modal');

		popup.find('img').attr('src', '');
		popup.find('.p-pv').html("");

		popup.find('img').attr('src', '/upload/pretty/'+json.pic_2);
		popup.find('.p-nick').html(json.nick_name);

		if(json.show_num){
			popup.find('.p-pv').html(json.point_pv+ '<i> pv</i>');
		}

		popup.find('.company_name').html(json.company_name);
		popup.find('#address').html(json.address);
		popup.find('#name').html(json.name);
		popup.find('#year').html(birth[0]);
		popup.find('#month').html(birth[1]);
		popup.find('#day').html(birth[2]);
		popup.find('#age').html(age);
		popup.find('#level').html(json.level);
		if(json.is_active == 1 ) {
			popup.find('#link_secret').removeClass('disable').attr('href', '/secret/' + json.pretty_keyword);
		}else {
			popup.find('#link_secret').addClass('disable').attr('href', '');
		}
		popup.fadeIn();
		$('body').addClass('disable-scroll');

		$.ajax({
			url: '/pretty',
			cache: false,
			type: 'POST',
			data: {id: id},
			dataType: 'json',
			success: function (rs) {
					console.log(rs.point_pv);
				$('#popup_pretty_modal').find('.p-pv').html(rs.point_pv+ '<i> pv</i>');
				$('.pv_'+id).html(rs.point_pv);
			}
		});

	});
	
	$('[data-pretty="close"]').click(function (e) {
		closeModal(e.target.className);
		$('body').remove('disable-scroll');
	});



});





