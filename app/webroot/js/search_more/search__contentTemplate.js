function __contentTemplate(data) {
	var maxCatLeng = 30;
	var catLengCount = 0;	
	var li_class = data.class || "";
	var strVar="";
	var isSecret = data.secret || 0;
	var noImageIndex = Math.floor((Math.random() * 10) + 1);
	
	if(noImageIndex < 10)
	{
		noImageIndex = '0'+noImageIndex;
	}
	
	strVar += "<li class=\""+li_class+"\" style=\"display:none\">";
	strVar += "<div class=\"search-box\">";
	strVar += "	<div class=\"search-title\">";
	
	if(isSecret==1)
	{
	strVar += "	<span class=\"secret-tag\">オリジナル求人<\/span>";
	}
	if(data.isinterview == 1 )
	{
	strVar += "	<span class=\"interview-tag\">企業インタビュー<\/span>";
	}
	if(data.benefit !== '' && data.benefit !== undefined)
	{	
	strVar += "	<span class=\"benefit-tag\">お祝い金あり<\/span>";
	}
	strVar += "		<h3>"+data.title+"<\/h3>";
	strVar += "	<\/div>";
	strVar += "	<div class=\"display-table\">";
	strVar += "		<div class=\"search-image\">";
	strVar += "			<a href=\""+data.href+"\" target=\"_blank\">";
	strVar += "				<img src=\""+data.pic_url+"\" alt=\""+data.title+"\" onerror=\"this.onerror=null;this.src='/img/instead/img_no-image_"+noImageIndex+".jpg';\">";
	strVar += "			<\/a>";
	strVar += "		<\/div>";
	strVar += "		<div class=\"search-content secret-content\">";
	strVar += "			<div class=\"search-head clearfix\">";
	strVar += "				<div class=\"pull-left\">";
	strVar += "					<p class=\"search-company\">"+data.company+"<\/p>";
	strVar += "				<\/div>";
	strVar += "				<div class=\"pull-right\">";
	if(data.isNew == 1)
	{
	strVar += "					<div class=\"new-label\">NEW<\/div>";
	}
	if(isSecret != 1)
	{
	strVar += "<div class=\"search-work\">提供元："+(data.site_name||"")+"<\/div>";
	}
	strVar += "				<\/div>";
	strVar += "			<\/div>";
	strVar += "			<div class=\"search-salary\">";
	strVar += "				<span>給与<\/span> <p>"+data.salary+"<\/p>";
	strVar += "			<\/div>";
	if(data.benefit !== '' && data.benefit !== undefined)
	{
	strVar += "			<div class=\"search-benefit\">";
	strVar += "				<span>お祝い金<\/span> <p>"+data.benefit+"<\/p>";
	strVar += "			<\/div>";
	}
	strVar += "			<div class=\"search-keyword\">";
	strVar += "				条件：";
	for(var l in data.labels)
	{
		catLengCount += data.labels[l].length;
		if(catLengCount >= maxCatLeng) { break; } 
		strVar += "<span class=\"i-condi\">"+data.labels[l]+"<\/span>";
	}
	strVar += "			<\/div>";
	strVar += "			<div class=\"clearfix search-foot\">";
	strVar += "				<a class=\"brick-btn brick-green\" href=\""+data.href+"\" target=\"_blank\" >詳細を確認<\/a>";
	strVar += "			<\/div>";
	strVar += "		<\/div>";
	strVar += "	<\/div>";
	strVar += "<\/div>";
	strVar += "</li>";
	
	return strVar;
}