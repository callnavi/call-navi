/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

	'use strict';

	var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

	function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

	// メニュー表示／非表示
	var Navigation = function () {
	    function Navigation() {
	        _classCallCheck(this, Navigation);
	    }

	    _createClass(Navigation, [{
	        key: 'spMenu',
	        value: function spMenu() {
	            var $trigger = $('[data-navi-trigger=sp-menu]'),
	                $target = $('[data-navi-target=sp-menu]');
	            if (!$target.length) return;
	            $trigger.on('click', function (e) {
	                var $targetText = $(e.currentTarget).children('[data-navi-target=sp-menu-txt]');
	                $trigger.toggleClass('is-open');
	                $target.toggleClass('is-open');
	                if ($trigger.hasClass('is-open')) {
	                    $targetText.text('close');
	                } else {
	                    $targetText.text('menu');
	                }
	                $('body').toggleClass('is-fixed');
	            });
	        }
	    }, {
	        key: 'contents',
	        value: function contents() {
	            var $trigger = $('[data-navi-trigger=contents]'),
	                $target = $('[data-navi-target=contents]');
	            if (!$target.length) return;
	            $trigger.on('click', function (e) {
	                $(e.currentTarget).toggleClass('is-open');
	                $target.toggleClass('is-open');
	                return false;
	            });
	        }
	    }, {
	        key: 'map',
	        value: function map() {
	            var $trigger = $('[data-searchMenu-trigger]'),
	                $target = $('[data-searchMenu-target]');
	            $trigger.on('click', function (e) {
	                var $triggerElm = $(this);
	                $triggerElm.toggleClass('is_close');
	                if ($triggerElm.hasClass('is_close')) {
	                    e.currentTarget.innerHTML = '<span>open</span>';
	                } else {
	                    e.currentTarget.innerHTML = '<span>close</span>';
	                }
	                $triggerElm.parent($target).toggleClass('is_close');
	            });
	        }
	    }, {
	        key: 'searchBox',
	        value: function searchBox() {
	            var $trigger = $('[data-toggleSearchBox-trigger]'),
	                $target = $('[data-toggleSearchBox-target]');
	            if (!$trigger.length) return;
	            $trigger.on('click', function () {
	                if ($target.is(':visible')) {
	                    $target.fadeOut();
	                } else {
	                    $target.fadeIn();
	                }
	            });
	        }
	    }]);

	    return Navigation;
	}();

	// 入力テキストの削除


	var clearInputText = function clearInputText() {
	    var $trigger = $('[data-txtClear-trigger]');
	    if (!$trigger.length) return;
	    $trigger.on('click', function () {
	        $(this).prev('[data-txtClear-target]').val('');
	    });
	};

	// ファイル名の表示（シークレット求人応募フォームで利用）
	var displayFileName = function displayFileName() {
	    var $trigger = $('[data-trigger-file]'),
	        $target = $('[data-target-file]');
	    if (!$trigger.length) return;
	    $trigger.on('change', function (e) {
	        var filePath = $(e.target).val().split('\\'),
	            fileName = filePath[filePath.length - 1];
	        if (fileName !== '') {
	            $(e.target).siblings().filter($target).text(fileName);
	        } else {
	            $(e.target).siblings().filter($target).text('選択されていません');
	        }
	    });
	};

	// スクロール時に反応するメニューたち

	var ActionMenu = function () {
	    function ActionMenu() {
	        _classCallCheck(this, ActionMenu);
	    }

	    _createClass(ActionMenu, [{
	        key: 'oiwaikin',
	        value: function oiwaikin() {
	            var $trigger = $('[data-trigger-menu=oiwaikin]'),
	                $trigger02 = $('[data-trigger-menu02=oiwaikin]'),
	                $target = $('[data-target-menu=oiwaikin]');
	            var scrollTop = '';
	            if (!$target.length) return;
	            var triggerToTop = $trigger.offset().top - $(window).outerHeight() + 200,
	                trigger02ToTop = $trigger02.offset().top - $(window).outerHeight() + 400;
	            $(window).scroll(function () {
	                scrollTop = $(window).scrollTop();
	                if (scrollTop > trigger02ToTop || scrollTop < triggerToTop) {
	                    $target.addClass('is_hide');
	                } else {
	                    $target.removeClass('is_hide');
	                }
	            });
	        }
	    }, {
	        key: 'keisaiguide',
	        value: function keisaiguide() {
	            var $trigger = $('[data-trigger-menu=keisaiguide]'),
	                $target = $('[data-target-menu=keisaiguide]'),
	                $menu = $('[data-target-nav]');
	            var scrollTop = '';
	            if (!$target.length) return;
	            var height = $trigger.innerHeight(),
	                navHeight = $('[data-height=nav]').innerHeight(),
	                height02 = $('#feature').offset().top - navHeight,
	                height03 = $('#flow').offset().top - navHeight,
	                height04 = $('#faq').offset().top - navHeight;

	            $(window).scroll(function () {
	                scrollTop = $(window).scrollTop();
	                if (scrollTop >= height) {
	                    $target.addClass('is-fixed');
	                } else {
	                    $target.removeClass('is-fixed');
	                }

	                // nav active設定
	                if (scrollTop >= height02 && scrollTop < height03) {
	                    $menu.removeClass('is-active');
	                    $($menu[0]).addClass('is-active');
	                } else if (scrollTop >= height03 && scrollTop < height04) {
	                    $menu.removeClass('is-active');
	                    $($menu[1]).addClass('is-active');
	                } else if (scrollTop >= height04) {
	                    $menu.removeClass('is-active');
	                    $($menu[2]).addClass('is-active');
	                } else {
	                    $menu.removeClass('is-active');
	                }
	            });
	        }
	    }, {
	        key: 'bottom',
	        value: function bottom() {
	            var $target = $('[data-togglebnr-target=bottom]');
	            if (!$target.length) return;
	            var _top = $('[data-togglebnr-trigger=content]').offset().top,
	                _footer = $('[data-togglebnr-trigger=footer]').offset().top;
	            var triggerToTop = _footer - $(window).outerHeight();
	            $(window).scroll(function () {
	                var y = $(window).scrollTop();
	                if (y > _top && y < triggerToTop) {
	                    $target.addClass('is-active');
	                } else {
	                    $target.removeClass('is-active');
	                }
	            });
	        }
	    }]);

	    return ActionMenu;
	}();

	// セレクトボックスの選択時の文字色変更


	var styleFormSelect = function styleFormSelect() {
	    var $trigger = $('[data-trigger-selectbox]');
	    if (!$trigger.length) return;
	    if ($trigger.find('option:selected').hasClass('form--select__placeholder')) {
	        $trigger.css({ 'color': '#ccc' });
	    }
	    $trigger.on('change', function () {
	        if ($(this).find('option:selected').hasClass('form--select__placeholder')) {
	            $(this).css({ 'color': '#ccc' });
	        } else {
	            $(this).css({ 'color': '#7e7d7d' });
	        }
	    });
	};

	var ValidateForm = function () {
	    function ValidateForm() {
	        _classCallCheck(this, ValidateForm);
	    }

	    _createClass(ValidateForm, [{
	        key: 'oiwaikin',
	        value: function oiwaikin() {
	            var $form = $('[data-form=oiwaikin]'),
	                message = '[data-target-msg=error]';
	            if (!$form.length) return;
	            $form.validate({
	                errorPlacement: function errorPlacement(error, element) {
	                    switch (element.attr('name')) {
	                        case 'agreement':
	                            $(element).parent('label').next(message).append(error);
	                            break;
	                        case 'startDay_year':
	                        case 'startDay_month':
	                        case 'startDay_day':
	                        case 'account_lastName':
	                        case 'account_firstName':
	                            $(element).parents('dd').find(message).append(error);
	                            break;
	                        default:
	                            $(element).nextAll(message).append(error);
	                            break;
	                    }
	                },
	                onkeyup: false,
	                rules: {
	                    entryId: {
	                        required: true,
	                        entry_id_valid: true
	                    },
	                    complete: {
	                        required: true
	                    },
	                    name: {
	                        required: true
	                    },
	                    mail: {
	                        required: true,
	                        email: true
	                    },
	                    company: {
	                        required: true
	                    },
	                    startDay_year: {
	                        required: true
	                    },
	                    startDay_month: {
	                        required: true
	                    },
	                    startDay_day: {
	                        required: true
	                    },
	                    account_lastName: {
	                        required: true,
	                        account_valid: true
	                    },
	                    account_firstName: {
	                        required: true,
	                        account_valid: true
	                    },
	                    agreement: {
	                        required: true
	                    }
	                },
	                groups: {
	                    startDay: 'startDay_year startDay_month startDay_day',
	                    account: 'account_lastName account_firstName'

	                },
	                messages: {
	                    entryId: {
	                        required: '応募IDを入力してください'
	                    },
	                    complete: {
	                        required: '初出勤の完了にチェックを入れてください'
	                    },
	                    name: {
	                        required: 'お名前を入力してください'
	                    },
	                    mail: {
	                        required: 'メールアドレスを入力してください',
	                        email: 'メールアドレスの入力に誤りがあります'
	                    },
	                    company: {
	                        required: '採用企業名を入力してください'
	                    },
	                    startDay_year: {
	                        required: '勤務開始日を選択してください'
	                    },
	                    startDay_month: {
	                        required: '勤務開始日を選択してください'
	                    },
	                    startDay_day: {
	                        required: '勤務開始日を選択してください'
	                    },
	                    account_lastName: {
	                        required: '振込口座名義を入力してください'
	                    },
	                    account_firstName: {
	                        required: '振込口座名義を入力してください'
	                    },
	                    agreement: {
	                        required: '個人情報の取り扱いに同意してください'
	                    }
	                }
	            });
	            jQuery.validator.addMethod("entry_id_valid", function (entry_id, element) {
	                entry_id = entry_id.replace(/\\s+/g, "");
	                return entry_id.match(/^([A-Z][0-9][0-9][0-9][0-9]-[A-Z][A-Z][A-Z]-[A-Z][0-9][0-9][0-9][0-9]-[A-Z][0-9][0-9][0-9][0-9]-[A-Z][0-9][0-9])/);
	            }, "正しい応募IDを入力してください");
	            jQuery.validator.addMethod("account_valid", function (value, element) {
	                value = value.replace(/\\s+/g, "");
	                return (/^([ァ-ヶー]+)$/.test(value)
	                );
	            }, "振込口座名義をカナで入力してください");
	        }
	    }, {
	        key: 'keisaiguide',
	        value: function keisaiguide() {
	            var $form = $('[data-form=keisaiguide]'),
	                message = '[data-target-msg=error]';
	            if (!$form.length) return;
	            $form.validate({
	                errorPlacement: function errorPlacement(error, element) {
	                    switch (element.attr('name')) {
	                        case 'agreement':
	                            $(element).parent('label').next(message).append(error);
	                            break;
	                        case 'inquiry':
	                            $(element).parents('dd').find(message).append(error);
	                            break;
	                        default:
	                            $(element).nextAll(message).append(error);
	                            break;
	                    }
	                },
	                onkeyup: false,
	                rules: {
	                    company: {
	                        required: true
	                    },
	                    name: {
	                        required: true
	                    },
	                    mail: {
	                        required: true,
	                        email: true
	                    },
	                    address: {
	                        required: true
	                    },
	                    phone: {
	                        required: true,
	                        phone_valid: true
	                    },
	                    inquiry: {
	                        required: true
	                    },
	                    agreement: {
	                        required: true
	                    }
	                },
	                messages: {
	                    company: {
	                        required: '貴社名を入力ください'
	                    },
	                    name: {
	                        required: 'ご担当者様のお名前を入力ください'
	                    },
	                    mail: {
	                        required: 'メールアドレスを入力してください',
	                        email: 'メールアドレスの入力に誤りがあります'
	                    },
	                    address: {
	                        required: 'ご住所を入力ください'
	                    },
	                    phone: {
	                        required: '電話番号を入力ください',
	                        phone_valid: '電話番号の入力に誤りがあります'
	                    },
	                    inquiry: {
	                        required: 'お問い合わせ内容を選択してください'
	                    },
	                    agreement: {
	                        required: '個人情報の取り扱いに同意してください'
	                    }
	                }
	            });
	            jQuery.validator.addMethod("phone_valid", function (value, element) {
	                return (/^[0-9]{11}$/.test(value)
	                );
	            }, "電話番号の入力に誤りがあります");
	        }
	    }]);

	    return ValidateForm;
	}();

	var smoothScroll = function smoothScroll() {
	    var anchor = '',
	        dest = '',
	        navHeight = '';
	    if ($('[data-height=nav]').length !== 0) {
	        navHeight = $('[data-height=nav]').innerHeight();
	    } else {
	        navHeight = 0;
	    }
	    $('[data-trigger-scroll]').click(function (e) {
	        e.preventDefault();
	        anchor = $(e.currentTarget).attr('href');
	        if (anchor !== '#') {
	            dest = $(anchor).offset().top - navHeight;
	        } else {
	            dest = 0;
	        }
	        $('html, body').animate({ scrollTop: dest }, 500, 'swing');
	    });
	};

	// svg-sprite表示用JS（IE）
	var svg4 = function svg4() {
	    if (!$('use').length) return;
	    svg4everybody();
	};

	var actionMenu = new ActionMenu();
	var validateForm = new ValidateForm();
	var navigation = new Navigation();

	$(document).ready(function () {
	    svg4();
	    navigation.contents();
	    navigation.spMenu();
	    navigation.map();
	    navigation.searchBox();
	    styleFormSelect();
	    clearInputText();
	    displayFileName();
	    smoothScroll();
	    actionMenu.bottom();
	    actionMenu.oiwaikin();
	    actionMenu.keisaiguide();
	    validateForm.oiwaikin();
	    validateForm.keisaiguide();
	    $('[ data-form-submit]').click(function () {
	        $('[data-form]').submit();
	        return false;
	    });
	});

/***/ })
/******/ ]);