$(function() {

	objSearchEm = {
		emId: 0,
		emName: '',
		listEm: {}
	}

	var list_event_after_close = {
		'show_popup_search_PC' : function(){
			$('.top-popup-search').fadeIn();
		},
		'show_popup_search_SP' : function(){
			$('#nav-search').fadeIn();
		},
		'default': function(){

		}
	};

	var list_event_before_open = {
		'close_popup_search_PC' : function(){
			$('.top-popup-search').fadeOut();
		},
		'close_popup_search_SP': function(){
			$('#nav-search').fadeOut();
		},
		'default': function(){

		}
	};

	var selectEm = $('.select-em');
	var closeButton = $('#close_popup_em');
	var popupSearch = $('#popup_search_box_em');
	var buttonBackToPreviousStep = $('.back-to-previous-step');
	var showPopupSearchEm = function(objSearchEm){
		beforeOpenPopupSearch();
		if($.isEmptyObject(objSearchEm.listEm))
		{
			getDataByAjax(objSearchEm);
		}
		popupSearch.fadeIn();
		$('body').addClass('disable-scroll');
	}

	var beforeOpenPopupSearch = function()
	{
		var funcName = $('.event-before-open-popup').val()||'default';
		list_event_before_open[funcName]&&list_event_before_open[funcName]();
	}

	var afterClosePopupSearch = function(){
		var funcName = $('.event-after-close-popup').val()||'default';
		list_event_after_close[funcName]&&list_event_after_close[funcName]();
	}

	var	closePopupSearch = function(){
		setDataAfterSearch();
		popupSearch.fadeOut();
		$('body').removeClass('disable-scroll');
		afterClosePopupSearch();
		//function from search_popup.js
		if($.countcondition)
		{
			$.countcondition();
		}
	}

	//show data in view
	var setDataForPopup = function(listData){
		var html = '';
		for (var key in listData) 
		{
			var em = listData[key];
			html += '<div class="metal-item" data-id="'+ key +'"><span class="i-label">'+ em +'</span><span class="i-change"></span></div>';
		}
		$("#box_search_em").html(html);
	}

	var getDataByAjax = function(objSearchEm){
		var url = '';
		url = '/search/getEmployment';
		$.ajax({url: url, 
			method: 'get', 
			dataType: 'json', 
			success: function(listData){
				if(listData)
				{
					setListEmForObject(objSearchEm, listData);
					setDataForPopup(listData);
				}
			}
		});
	}

	var setListEmForObject = function(objSearchEm, listData)
	{
		objSearchEm.listEm = listData;
	}

	//set data id and data name
	var setDataIdAndDataName = function(objSearchEm, dataId, dataName){
    	objSearchEm.emId = dataId;
		objSearchEm.emName = dataName;       
	}

	var setDataAfterSearch = function(){
		var dataEm = $('.data-em');
		var textboxEm = $('.select-em');
		textboxEm.val(objSearchEm.emName);
		dataEm.val(objSearchEm.emId);
	}

	selectEm.bind('click touchstart',function(e){
		var device = $(e.target).attr('data-popup');
		if($(e.target).attr('data-popup'))
		{
			$('.event-before-open-popup').val("close_popup_search_" + device)
			$('.event-after-close-popup').val("show_popup_search_" + device)
		}
		else
		{
			$('.event-before-open-popup').val("");
			$('.event-after-close-popup').val("");
		}
		showPopupSearchEm(objSearchEm);
		e.preventDefault();
		return false;
	});
	
	closeButton.click(function(){
		closePopupSearch();
	});

	//event get data-id
	$('#box_search_em').click(function(e){
		//element was be clicked
		var target = e.target;
				
		var targetClass = $(target).attr('class');
		if(targetClass != "metal-item")
		{
			if(targetClass == "i-label")
			{
				dataName = $(target).text();
			}
			else if(targetClass == "i-change")
			{
				dataName = $(target).siblings('.i-label').text();
			}
			else
			{
				return;
			}
			dataId = $(target).parent().attr('data-id');
		}
		else
		{
			if($(e.target).data('target') == "close")
			{
				closePopupSearch();
				return;
			}
			
			dataId = $(target).attr('data-id');
			dataName = $(target).children('.i-label').text();
		}

		if(dataId)
		{
			setDataIdAndDataName(objSearchEm, dataId, dataName);
			closePopupSearch();
		}
	});
	
	//event close popup
	$('#popup_search_box_em').click(function(e){
		if($(e.target).attr('class') == "popup-search-box")
		{
			closePopupSearch();
		}
	});
	
});