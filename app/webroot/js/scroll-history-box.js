/*scroll following screen v2.0*/

/*
	_wrapper : for scrolling, fading, fadout
	_bannerObj: to append `historybox` when top
	_sidebarObj: to append `historybox` when scrolling
	_historyBox : obj for appending
*/
var scrollHistoryBox = function (_obj, _bannerObj, _sidebarObj, _wrapper, _search_result) {
	var _this = this;
	this.obj 				= _obj;
	this.searchResult 		= _search_result;
	this.wrapper			= _wrapper;
	this.fixClassName 		= 'isFixed';
	this.bottomClassName 	= 'isBottom';
	this.topClassName 		= 'isTop';
	
	this.heightContent 		= this.obj.outerHeight(true);
	this.top 				= this.obj.offset().top;
	this.enableScroll 		= true;
	
	//this.posBoundary 		= this.top + this.heightContent + 20;
	this.posBoundary 		= this.searchResult.offset().top -30;
	//this.stopPoint			= $('#search-result').offset().top + $('#search-result').outerHeight() - 935;
	
	this.getStopPoint = function(){
		return _this.searchResult.offset().top + 
			   _this.searchResult.outerHeight() - 
			   _this.wrapper.height(); 
	}
		
	//console.log(this.stopPoint);
	
	this.changedBoundary 	= 0;
	
	this.scrollEvent = function (y) {
		//on banner : add History box to BANNER
		if(y <= _this.posBoundary)
		{
			if(_this.changedBoundary != 1)
			{
				_bannerObj.append(_this.obj);
				_this.obj.hide().fadeIn('slow');
				_this.changedBoundary = 1;
				_this.wrapper.removeClass(_this.fixClassName).addClass(_this.topClassName).fadeOut('slow');
			}
		}
		
		//on content : add History box to RIGHT COLUMN
		else
		{
			if(_this.changedBoundary != 2)
			{
				_sidebarObj.append(_this.obj);
				_this.wrapper.hide().fadeIn('slow');
				_this.obj.css('height', _this.heightContent);
				_this.changedBoundary = 2;
			}
			
			//is Fixed : add Fixed
			if(y < _this.getStopPoint())
			{
				_this.wrapper.removeClass(_this.bottomClassName).addClass(_this.fixClassName);	
			}
			
			//is Bottom : add Bottom
			else
			{
				_this.wrapper.removeClass(_this.fixClassName).addClass(_this.bottomClassName);	
			}
		}
	}

	//Event
	$(window).scroll(function (evt) {
		if(!_this.enableScroll) return;
		var y = $(this).scrollTop();
		_this.scrollEvent(y);
	});
	
	//Load
	if($(window).scrollTop() > _this.posBoundary)
	{
		this.scrollEvent($(window).scrollTop());
	}
	else
	{
		this.changedBoundary = 1;
	}
	
	return _this;
}