$(document).ready(function () {


	//Check validate form 1
	var form1 = $("#frm-contact-form1");
	form1.validate({
		errorPlacement: function errorPlacement(error, element) {
			element.before(error);
		},
		onkeyup: false,
		rules: {
			step1FullName: {
				required: true,
			},
			step1Phonetic: {
				required: true,
			},
			step1Birth: {
				required: true,
			},
			step1Phone: {
				required: true,
                phone_valid: true
			},
			step1Email: {
				required: true,
				email: true
			},
			// step1Content: {
			//     required: true,
			// },
		},
		messages:{
			step1FullName: {
				required: 'お名前を入力してください',
			},
			step1Phonetic: {
				required: 'ふりがなを入力してください',
			},
			step1Birth: {
				required: '生年月日を入力してください',
			},
			step1Phone: {
				required: '電話番号を入力してください',
			},
			step1Email: {
				required: '正しいメールアドレスを入力してください',
				email: '正しいメールアドレスを入力してください'
			},
		}

	});

	jQuery.validator.addMethod("phone_valid", function (phone_number, element) {
		phone_number = phone_number.replace(/\s+/g, "");
		return this.optional(element) || phone_number.length > 9 &&
			// phone_number.match(/([0-9]{3})([-])?([0-9]{4})([-])?([0-9]{4})/);
			phone_number.match(/^[0-9]{3}[-\s]{0,1}[0-9]{4}[-\s]{0,1}[0-9]{4}$/);
	}, "携帯電話番号は半角数字11桁で入力してください。");


	jQuery.extend(jQuery.validator.messages, {
		required: "この項目は必須です。",
		email: "正しいメールアドレスを入力してください。"
	});
	
	$('#btnSubmit').click(function () {
		$("#frm-contact-form1").submit();
	});
});