function sendContactMail(url, arr) {
	$.ajax({
		url: url,
		type: 'POST',
		// dataType: 'json',
		data: {
			name: arr[0],
			mail: arr[1],
			content: arr[2],
			phonetic: arr[3],
			birthday: arr[4],
			tel: arr[5],
			gender: arr[6],
			id_job: arr[7]
		},
		success: function (result) {
			return result;
		},
		error: function (jqXHR, textStatus, errorThrown) {
			return errorThrown;
		}
	});
}

$(document).ready(function () {

	$('#select-form-contact1').change(function () {

		$("#frm-contact-form2").hide();
		$("#frm-contact-form1").show();

		$("#frm-contact-form2").validate().resetForm();

		$("#step1FullName").append($("#st1-fullname"));
		$("#step1Email").append($("#st1-email"));
		$("#step1Content").append($("#st1-content"));

		$("#step2FullName").append($("#st2-fullname"));
		$("#step2Email").append($("#st2-email"));
		$("#step2Content").append($("#st2-content"));

	});

	//Check validate form 1
	var form1 = $("#frm-contact-form1");
	form1.validate({
		errorPlacement: function errorPlacement(error, element) {
			element.before(error);
		},
		onkeyup: false,
		rules: {
			step1FullName: {
				required: true,
			},
			step1Phonetic: {
				required: true,
			},
			step1Birth: {
				required: true,
			},
			step1Phone: {
				required: true,
			},
			step1Email: {
				required: true,
				email: true
			},
			// step1Content: {
			//     required: true,
			// },
		},
		messages:{
			step1FullName: {
				required: 'お名前を入力してください',
			},
			step1Phonetic: {
				required: 'ふりがなを入力してください',
			},
			step1Birth: {
				required: '生年月日を入力してください',
			},
			step1Phone: {
				required: '電話番号を入力してください',
			},
			step1Email: {
				required: '正しいメールアドレスを入力してください',
				email: '正しいメールアドレスを入力してください'
			},
		}

	});

	//contact form step by step
	$("#contact-form").steps({
		headerTag: "h2",
		bodyTag: "section",
		transitionEffect: "fade",
		cssClass: "tabcontrol",
		onStepChanging: function (event, currentIndex, newIndex) {
			//go to step 2
			if (newIndex == 1) {
				form1.validate().settings.ignore = ":disabled,:hidden";
				if (form1.valid()) {
					$("#contact-form .actions  ul li a[href=#next]").html("上記内容で送信する");
					$("#contact-main .div-contact-form .step2 .form-group #step2-full-name").attr("value", $("#contact-main .div-contact-form .step1 .form-group #step1-full-name").val());
					$("#contact-main .div-contact-form .step2 .form-group #step2Phonetic").attr("value", $("#contact-main .div-contact-form .step1 .form-group #step1Phonetic").val());

					$("#contact-main .div-contact-form .step2 .form-group #step2Birth").attr("value", $("#contact-main .div-contact-form .step1 .form-group #step1Birth").val());


					if ($('#contact-main .div-contact-form .step1 .form-group input[name=step1gender]:checked').attr("id") == "Male") {
						$('#contact-main .div-contact-form .step2 .form-group #Malestep2').prop('checked', true);
					} else {
						$('#contact-main .div-contact-form .step2 .form-group #Femalestep2').prop('checked', true);
					}

					$("#contact-main .div-contact-form .step2 .form-group #step2Phone").attr("value", $("#contact-main .div-contact-form .step1 .form-group #step1Phone").val());

					$("#contact-main .div-contact-form .step2 .form-group #step2-email").attr("value", $("#contact-main .div-contact-form .step1 .form-group #step1-email").val());
					$("#contact-main .div-contact-form .step2 .form-group #step2-content").html($("#contact-main .div-contact-form .step1 .form-group #step1-content").val());

					$("#contact-form .content").css("height", "1450px");
					disableRadioButton();
				} else {

					$("#contact-form .content").css("height", "1450px");
					return form1.valid();
				}
			}

			//go to step 3
			if (newIndex == 2) {
				var arr = [];
				arr[0] = $("#contact-main .div-contact-form .step1 .form-group #step1-full-name").val();
				arr[1] = $("#contact-main .div-contact-form .step1 .form-group #step1-email").val();
				arr[2] = $("#contact-main .div-contact-form .step1 .form-group #step1-content").val();
				arr[3] = $("#contact-main .div-contact-form .step1 .form-group #step1Phonetic").val();
				arr[4] = $("#contact-main .div-contact-form .step1 .form-group #step1Birth").val();
				arr[5] = $("#contact-main .div-contact-form .step1 .form-group #step1Phone").val();
				arr[6] = $('#contact-main .div-contact-form .step1 .form-group input[name=step1gender]:checked').val();
				arr[7] = $('#contact-main .div-contact-form #job_id').val();

				sendContactMail("/secret/SendMail", arr);
				$("#contact-form .actions  ul li a[href=#finish]").html("Finish");
				$("#contact-form .content").css("height", "400px");
				disableButtonStep();

				if (__add_yahoo_google_script !== undefined) {
					__add_yahoo_google_script();
				}
			}

			return true;
		},
		onFinishing: function (event, currentIndex) {
			return true;
		},
		onFinished: function (event, currentIndex) {
			//finish
			window.top.location.href = "http://" + document.location.hostname;
		}
	});

	$("#contact-form .actions  ul li a[href=#next]").html("確認画面へ進む");
	$("#contact-form .actions  ul li a[href=#previous]").parent().hide();


	jQuery.validator.addMethod("phone_valid", function (phone_number, element) {
		phone_number = phone_number.replace(/\s+/g, "");
		return this.optional(element) || phone_number.length > 9 &&
			// phone_number.match(/([0-9]{3})([-])?([0-9]{4})([-])?([0-9]{4})/);
			phone_number.match(/^[0-9]{3}[-\s]{0,1}[0-9]{4}[-\s]{0,1}[0-9]{4}$/);
	}, "携帯電話番号は半角数字11桁で入力してください。");


	jQuery.extend(jQuery.validator.messages, {
		required: "この項目は必須です。",
		email: "正しいメールアドレスを入力してください。"
	});

	function disableRadioButton() {
		if ($("#select-form-contact1").is(':checked')) {
			$("#select-form-contact2").attr('disabled', true);
			$("#color-disable2").addClass("color-disable");
		} else {
			$("#select-form-contact1").attr('disabled', true);
			$("#color-disable1").addClass("color-disable");
		}
	}

	function disableButtonStep() {
		$("li.first.done").addClass('disabled');
		$("li.current").addClass('disabled');
	}
});