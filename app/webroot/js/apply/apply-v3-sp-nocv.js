$(document).ready(function () {


	//Check validate form 1
	var form1 = $("#frm-contact-form1");
	form1.validate({
		errorPlacement: function errorPlacement(error, element) {
			// element.before(error);
			// console.log(element.attr('name'));
			switch(element.attr("name")){
				case "step1cv1":
					error.appendTo("#error-msg--cv1");
					break;
				case "step1cv2":
					error.appendTo("#error-msg--cv2");
					break;
				case "step1skill[]":
					error.appendTo("#error-msg--skill");
					break;
				case "step1Time":
					error.appendTo("#error-msg--time");
					break;
				default:
					error.insertAfter(element);
					break;
			}
		},
		onkeyup: false,
		rules: {
			step1FullName: {
				required: true
			},
			step1Phonetic: {
				required: true
			},
			step1Birth: {
				required: true
			},
			step1Phone: {
				required: true,
				phone_valid: true
			},
			step1Email: {
				required: true,
				email: true
			},
			'step1skill[]': {
				required: true
			},
			step1Time: {
				required: true
			}
			// step1Content: {
			//     required: true,
			// },
		},
		messages:{
			step1FullName: {
				required: 'お名前は必須です。'
			},
			step1Phonetic: {
				required: 'ふりがなは必須です。'
			},
			step1Birth: {
				required: '生年月日は必須です。'
			},
			step1Phone: {
				required: '電話番号は必須です。'
			},
			step1Email: {
				required: 'アドレスは必須です。'
				// email: 'アドレスに誤りがあります。'
			},
			'step1skill[]': {
				required: 'この項目は必須です。'
			},
			step1Time: {
				required: 'この項目は必須です。'
			}
		}

	});

	jQuery.validator.addMethod("phone_valid", function (phone_number, element) {
		phone_number = phone_number.replace(/\s+/g, "");
		return this.optional(element) || phone_number.length > 9 &&
			// phone_number.match(/([0-9]{3})([-])?([0-9]{4})([-])?([0-9]{4})/);
			phone_number.match(/^[0-9]{3}[-\s]{0,1}[0-9]{4}[-\s]{0,1}[0-9]{4}$/);
	}, "携帯電話番号は半角数字11桁で入力してください。");


	jQuery.extend(jQuery.validator.messages, {
		// required: "この項目は必須です。",
		email: "メールアドレスが不正です。"
	});
	
	$('#btnSubmit').click(function () {
		$("#frm-contact-form1").submit();
	});
});