$(function() {
	var _click = false;
	$('#fbButton').click(function () {
		if(_click) {
			alert('送信中...');
			return;
		}
		
		var msg = $('#fbMessage').val();
		msg = msg.trim();
		if(msg == "")
		{
			alert('ご意見をご入力ください。');
		}
		else
		{
			_click = true;
			$('#feedback').addClass('sending');
			$.post('/feedback/sendmail',{'message':msg}, function (result) {
				_click = false;
				$('#feedback').removeClass('sending');
				if(result == 1)
				{
					$('#feedback').addClass('done');
					$('#fbMessage').val('');
					alert('送信完了。\n貴重なご意見ありがとうございました。');
				}
				else
				{
					if(result == -1)
					{
						$('#feedback').addClass('done');
						alert('またのご意見をお待ちしております。');
					}
					else
					{
						alert('ご意見をご入力ください。');
					}
					
				}
			})
		}
	});
});