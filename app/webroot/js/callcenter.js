$(function() {
	$('#btnFilterArea').click(function () {
		var per  = $('#per').val();
		var city = $('#city').val();
		if(city != "" && city != undefined)
			city = '/city_' + city;
		else
			city = '';
		window.location = '/callcenter_matome/area/'+per+city;
	});
	
	
	var getCityBySelectedPer = function (currentCityId){
		var per = $('#per').val();
		var citySelectTag = $('#city');
		citySelectTag.empty();
		citySelectTag.append('<option value="">市町村の選択</option>');
		$.ajax({url:'/callcenter/getcitybyalias/'+per, 
				method: 'get', 
				dataType: 'json', 
				success: function(cityList){
					
					for(var index in cityList)
					{
						var id = cityList[index].area.id||'';
						var name = cityList[index].area.name||'';
						var selected = '';
						if(currentCityId == id)
						{
							selected = 'selected';
						}
						var htmlCity = '<option '+selected+' value="'+id+'">'+name+'</option>';
						citySelectTag.append(htmlCity);
					}
				}});
	}
	var currentCityId = $('#currentCityId').val();
	getCityBySelectedPer(currentCityId);
	
	$('#per').change(function () {
		getCityBySelectedPer(0);
	});
});