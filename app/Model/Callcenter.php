<?php
App::uses('AppModel', 'Model');
/**
 * ContentsEmail Model
 *
 */
class Callcenter extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $useTable = 'callcenter';

	public function getListByArea($page, $amount, $alias, $cityID = null)
	{
		$query = "SELECT * FROM callcenter c, area a WHERE a.id = c.city_id 
					AND a.parent_id = (SELECT id FROM area WHERE alias = :alias) ";

		$arrParam = array('alias' => $alias);
		if($cityID)
		{
			$query .= "AND c.city_id = :cityID ";
			$arrParam = array('alias' => $alias, 'cityID' => $cityID);
		}
		
		//$queryForTotal = "select count(*) from ($query) x";
		$queryForTotal = str_replace('*', 'count(*) as total', $query);
		if($amount != 0) {
			$start = ($page - 1) * $amount;
			$end = $amount;
			$query .= "ORDER BY c.city_id LIMIT $start,$end";
		}
		else
		{
			$query .= "ORDER BY c.city_id";
		}

		$db = $this->getDataSource();
		$data = $db->fetchAll($query, $arrParam);
		$total = $db->fetchAll($queryForTotal, $arrParam);
		
		if(isset($total[0][0]['total']))
		{
			$total = $total[0][0]['total'];
		}
		else
		{
			$total = 0;
		}
		
		return array(
			'data' => $data,
			'total' => $total,
		);
	}

	public function getDetail($area_alias,$city_id, $callcenter_id)
	{
		$callcenter_id = intval($callcenter_id);
		$db = $this->getDataSource();
		$query = 'select 
					c.*,
					city.name as city_name,
					city.parent_id,
					per.name as area_name,
					per.alias as area_alias
				from (callcenter c join area city on c.city_id = city.id) join area per on per.id = city.parent_id where c.id = '.$callcenter_id;
		
		$detail =  $db->fetchAll($query);	
		if(isset($detail[0]))
		{
			return $detail[0];
		}
		
		return array();
	}

	
	public function getRelatedCallCenter($ccId, $city_id)
	{
		$db = $this->getDataSource();
		$query = 'select 
							c.id,
							c.company_name,
							c.address,
							c.city_id,
							c.lat,
							c.lng,
							city.name as city_name,
							city.parent_id,
							per.name as area_name,
							per.alias as area_alias
					from (callcenter c join area city on c.city_id = city.id) join area per on per.id = city.parent_id 
					where c.id != :ccId and city.parent_id = (select a.parent_id from area a where a.id = :cityId)
					order by city.id = :cityId DESC';
		
		$pars = array('ccId'=>$ccId, 'cityId' => $city_id);
		$detail =  $db->fetchAll($query, $pars);	
		return $detail;
	}

	public function getListCallcenterV2($keyword = null){
        $data = false;
	    if(!empty($keyword)){
			$conditions = array();
			if(is_array($keyword))
			{
				foreach( $keyword as $val)
				{
					if(empty($val))
					{
						continue;
					}
										
					$conditions[] = array( 	'OR' => 
										array(
											'Callcenter.company_name like' =>'%'.$val.'%',
											'Callcenter.address like' =>'%'.$val.'%',
											'Area.name like' =>'%'.$val.'%',
										)
									);
				}
			}
			else
			{
				$conditions =  array( 	'OR' => 
										array(
											'Callcenter.company_name like' =>'%'.$keyword.'%',
											'Callcenter.address like' =>'%'.$keyword.'%',
											'Area.name like' =>'%'.$keyword.'%',
										)
									);
				
				
			}
			
			
            $data = $this->find('all', array(
                'fields'=> array('Callcenter.*','Area.name','Area.level'),
                'conditions' => $conditions,
                'joins' => array(
                    array(
                        'table' => 'area',
                        'alias' => 'Area',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Area.id = Callcenter.city_id',
                        ),
                    ),
                )));
        }

        return $data;
    }

    public function getCallCenterById($id)
    {
    	$data = $this->find('all', array(
                'fields'=> array('Callcenter.*','Area.name','Area.level'),
                'conditions' => array('Callcenter.id' => $id),
                'joins' => array(
                    array(
                        'table' => 'area',
                        'alias' => 'Area',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'Area.id = Callcenter.city_id',
                        ),
                    ),
                )));

    	return $data;
    }
}
