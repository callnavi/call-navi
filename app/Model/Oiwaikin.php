<?php
App::uses('AppModel', 'Model');
/**
 * ContentsEmail Model
 *
 */
class Oiwaikin extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $useTable = 'oiwaikin';
	
	/**
	 * 
	 * @return array (
	 *                => NAME,
	 *                => URL,
	 *                => IMAGE,
	 *                => DATE,
	 *                => VIEW
	 *                )
	 */

	
	public function add_new_data($data)
	{
        
        //get last id 
        
        $lastId = $this->query("SHOW TABLE STATUS WHERE name = 'oiwaikin'");

        //SET DATE 
        $ITME =  $this->combineDateString($data['startDay_year'],$data['startDay_month'],$data['startDay_day']);
        
        // Field into database
		$savedData['id'] = $lastId[0]['TABLES']['Auto_increment'];
		$savedData['apply_id'] = $data['entryId'];
		$savedData['first_day_job'] = $data['complete'];
		$savedData['name'] = $data['name'];
		$savedData['email'] = $data['mail'];
		$savedData['company_name'] = $data['company'];
		$savedData['start_working_date'] = $ITME;
		$savedData['kana_f_name'] = $data['account_firstName'];
        $savedData['kana_l_name'] = $data['account_lastName'];
        $savedData['remark'] = $data['remarks'];
		
        //Create and save data into database
		$this->create();
		$this->save($savedData);
		$flag = false;
        
        if($this->id = $lastId[0]['TABLES']['Auto_increment'] ){
            $flag = true;
        }
        
		return $flag;
	}
    private function combineDateString($year, $month, $day){
        $year_str = '';
        $month_str = '';
        $day_str = '';
        $year_str = $year ;
        $month_str = $month ;
        $day_str = $day ;
        
        //set string to date
        $time = $year_str . "-" .$month_str . "-" .  $day_str ;
        
        //convert string to date
        $date_ret = date("Y-m-d", strtotime($time));;

        //return date time
        return  $time ;
                            
    }
	

}
