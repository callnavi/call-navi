<?php
App::uses('AppModel', 'Model');
/**
 * ContentsEmail Model
 *
 */
class Keywords extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $useTable = 'keywords';
	
	public function setResultByKeyword($id, $result)
	{
		$this->save(['id' => $id, 'result' => $result]);
	}
	
	public function getHotkey()
	{
		return $this->find('all', [
			'fields' => array('id','keyword','result','secret_result'),
			'conditions' => array('status' => 1),
			'order' => array('priority')
		]);
	}

	public function getHotKeyOverOneHour()
	{
		$now = (new \DateTime())->format('Y-m-d H:i:s');
		$sql = "select id from keywords where modified <= DATE_SUB(:now,INTERVAL 1 HOUR) and status = 1";
		$pars = array('now' => $now);
		$db = $this->getDataSource();
		
		$data = $db->fetchAll($sql, $pars);

		return $data;
	}
}
