<?php
App::uses('AppModel', 'Model');
/**
 * ContentsEmail Model
 *
 */
class Area extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $useTable = 'area';
	
	
	public function getGroup($groupId = null)
	{
		$db = $this->getDataSource();
		$groupQuery = !empty($groupId) ? " and area.group = :group " : null;
		$arr = !empty($groupId) ? array('group'=>$groupId) : null;
		$query = 'select id, name, alias, `group`, callcenter_amount from area where level=0'.$groupQuery.'  order by `group`, priority';
		
		return $db->fetchAll($query, $arr);
	}
	
	public function getCityByProvinceAlias($provinceAlias)
	{
		$db = $this->getDataSource();
		$query = 'select id, name, alias, `group` from area where level=0 order by `group`, priority';
		return $db->fetchAll($query);
	}

	//return city id and city name by province aliasname  
	public function getCityByAliasName($alias)
	{
		$db = $this->getDataSource();
		$query = 'SELECT id, name FROM area WHERE parent_id = (SELECT id FROM area WHERE alias = :alias) ORDER BY id';
		return $db->fetchAll($query, array('alias' => $alias));
	}
	
	public function getAreaByAliasName($alias)
	{
		$db = $this->getDataSource();

		$query = 'SELECT id, name, area.group, alias FROM area WHERE alias = :alias';
		return $db->fetchAll($query, array('alias' => $alias));
	}
	
	public function getAreaById($id)
	{
		$db = $this->getDataSource();

		$query = 'SELECT id, name FROM area WHERE id = :areaId';
		return $db->fetchAll($query, array('areaId' => $id));
	}

	public function getProvinceByGroup($group)
	{
		$db = $this->getDataSource();

		$query = 'SELECT id, name FROM area WHERE search_group = :group ORDER BY priority';
		return $db->fetchAll($query, array('group' => $group));
	}

	public function getAreaByParentIDAndLevel($parentId, $level)
	{
		$db = $this->getDataSource();
		$query = 'SELECT id, name FROM area WHERE parent_id = :parentId AND area.level = :level ORDER BY priority';
		return $db->fetchAll($query, array('parentId' => $parentId, 'level' => $level));
	}

	public function getAllArea()
	{
		$db = $this->getDataSource();
		$query = 'SELECT id, name FROM area';
		return $db->fetchAll($query);
	}
}
