<?php
App::uses('AppModel', 'Model');
App::import('Component', 'Search');
/**
 * ContentsEmail Model
 *
 */
class CareerOpportunity extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $useTable = 'career_opportunities';
		
	public function getTotalOnly($params)
	{
		$conditions = $this->setCondition($params);
		$pars 	= $conditions['pars'];
		$where 	= $conditions['where'];
		
		$sql_total = "SELECT count(*) as _total
						FROM career_opportunities AS CareerOpportunity LEFT JOIN corporate_prs  
									ON (corporate_prs.id = CareerOpportunity.company_id)  
						WHERE CareerOpportunity.status=1 $where";
		
		$db = $this->getDataSource();
		$rs = $db->fetchAll($sql_total,$pars);

		if(isset($rs[0][0]['_total']))
			return $rs[0][0]['_total'];
		return 0;
	}
	
	public function getSecretByRange($params, $start, $end)
	{
		$conditions = $this->setCondition($params);
		$pars 	= $conditions['pars'];
		$where 	= $conditions['where'];
		
		$sql = "SELECT CareerOpportunity.*,corporate_prs.company_name,corporate_prs.corporate_logo,corporate_prs.id
						FROM career_opportunities AS CareerOpportunity LEFT JOIN corporate_prs  
									ON (corporate_prs.id = CareerOpportunity.company_id)  
						WHERE CareerOpportunity.status=1 $where
						ORDER BY CareerOpportunity.created DESC
						LIMIT $start,$end";
		
		$db = $this->getDataSource();
		$data = $db->fetchAll($sql,$pars);
		
		return $data;
	}

	public function getListSecret($page,$size,$params)
	{
		$searchComponent = new SearchComponent(new ComponentCollection);
		$conditions = $this->setCondition($params);
		$pars 	= $conditions['pars'];
		$where 	= $conditions['where'];
		
		$start= ($page-1)*$size;
		$end = $size;

		$sql_total = "SELECT CareerOpportunity.*,corporate_prs.company_name,corporate_prs.corporate_logo,corporate_prs.id
						FROM career_opportunities AS CareerOpportunity LEFT JOIN corporate_prs  
									ON (corporate_prs.id = CareerOpportunity.company_id)  
						WHERE CareerOpportunity.status=1 $where";

		$sql = "SELECT CareerOpportunity.*,corporate_prs.company_name,corporate_prs.corporate_logo,corporate_prs.id,contents.title
						FROM career_opportunities AS CareerOpportunity 
						LEFT JOIN corporate_prs  
									ON (corporate_prs.id = CareerOpportunity.company_id)  
						LEFT JOIN contents 
									ON (CareerOpportunity.id = contents.secret_job_id)
						WHERE CareerOpportunity.status=1 $where
						ORDER BY CareerOpportunity.created DESC
						LIMIT $start,$end";
		$db = $this->getDataSource();
		$data = $db->fetchAll($sql,$pars);
		$total = count($db->fetchAll($sql_total,$pars));
		return array(
			'data' => $data,
			'total' => $total,
		);

	}

	public function getTotal($where = null)
	{

		$sql = "SELECT CareerOpportunity.*, corporate_prs.company_name FROM career_opportunities AS CareerOpportunity LEFT JOIN corporate_prs  ON (corporate_prs.id = CareerOpportunity.company_id)  WHERE 1 = 1 ".$where."
		";
		//echo $query;
		$db = $this->getDataSource();
		$data = $db->fetchAll($sql);

		return count($data);
	}

	public function getSecret($idOrTitle)
	{

		if(is_numeric($idOrTitle)<1)
		{
			$field = 'job';
		}
		else
		{
			
			$idOrTitle = intval($idOrTitle);
			$field = 'id';
		}
		$db = $this->getDataSource();
		$pars = array($field => $idOrTitle);
		$query = "select CareerOpportunity.* from career_opportunities as CareerOpportunity 
				  where 
				  	exists(select * from corporate_prs where id = CareerOpportunity.company_id)
					and CareerOpportunity.status = 1
				  	and CareerOpportunity.$field= :$field
				  limit 1";
		$carrer = $db->fetchAll($query,$pars);
		
		//Get company info when this carrer exists
		if(count($carrer) == 1)
		{
			$carrer = $carrer[0]['CareerOpportunity'];
			$companyId = $carrer['company_id'];
			
			$queryCompany = 'select CorporatePrs.* 
							 from corporate_prs as CorporatePrs
							 where CorporatePrs.id = :companyId limit 1';
			$parsCompany = array('companyId' => $companyId);
			
			$company = $db->fetchAll($queryCompany,$parsCompany);
			
			if(count($company) == 1)
			{
				return array('CareerOpportunity' => $carrer, 
							 'CorporatePrs'      => $company[0]['CorporatePrs']);
			}
		}

		//when nothing
		return array();
	}
	
	
	public function getSecretShare($id)
	{
		$id = intval($id);
		$field = 'id';
		$db = $this->getDataSource();
		$pars = array($field => $id);
		$query = "select CareerOpportunity.* from career_opportunities as CareerOpportunity 
				  where 
				  	exists(select * from corporate_prs where id = CareerOpportunity.company_id)
				  	and CareerOpportunity.$field= :$field
				  limit 1";
		$carrer = $db->fetchAll($query,$pars);
		
		//Get company info when this carrer exists
		if(count($carrer) == 1)
		{
			$carrer = $carrer[0]['CareerOpportunity'];
			$companyId = $carrer['company_id'];
			
			$queryCompany = 'select CorporatePrs.* 
							 from corporate_prs as CorporatePrs
							 where CorporatePrs.id = :companyId limit 1';
			$parsCompany = array('companyId' => $companyId);
			
			$company = $db->fetchAll($queryCompany,$parsCompany);
			
			if(count($company) == 1)
			{
				return array('CareerOpportunity' => $carrer, 
							 'CorporatePrs'      => $company[0]['CorporatePrs']);
			}
		}

		//when nothing
		return array();
	}

	public function getSecretCompany($id)
	{
		$db = $this->getDataSource();
		$query = 'select CorporatePrs.* from corporate_prs as CorporatePrs
				  where CorporatePrs.id= :id
				  limit 1';
		$pars = array('id' => $id);
		$company = $db->fetchAll($query,$pars);

		//when nothing
		return $company[0];
	}
	
	public function countActiveJob()
	{
		$options = array('conditions' => array(
			'status' => 1
		));
		$total = $this->find('count', $options);
		
		return $total;
	}
	
	public function search($params, $page, $size)
	{
		//TODO: search
		$sql = "SELECT CareerOpportunity.*, 
					   corporate_prs.company_name,
					   corporate_prs.corporate_logo,
					   corporate_prs.id
					   
				FROM career_opportunities AS CareerOpportunity 
					 LEFT JOIN corporate_prs  
					 ON (corporate_prs.id = CareerOpportunity.company_id)  
				
				WHERE CareerOpportunity.status = 1
				ORDER BY RAND()
				LIMIT $size";
		
		$db = $this->getDataSource();
		$data = $db->fetchAll($sql);
		return $data;
	}
	
	public function pickup()
	{
		//TODO: youshouldwritesomethinghere
		$sql = "SELECT CareerOpportunity.*, 
					   corporate_prs.company_name,
					   corporate_prs.corporate_logo,
					   corporate_prs.id 
					   
				FROM career_opportunities AS CareerOpportunity 
					 LEFT JOIN corporate_prs  
					 ON (corporate_prs.id = CareerOpportunity.company_id)  
				
				WHERE CareerOpportunity.status = 1
				
				LIMIT 2";
		
		$db = $this->getDataSource();
		$data = $db->fetchAll($sql);
		return $data;
	}
	
	public function getSecretsForSlider($limit)
	{
		//TODO: getSecretsForSlider
		$sql = "SELECT CareerOpportunity.employment,
					   CareerOpportunity.salary,
					   CareerOpportunity.job,
					   CareerOpportunity.id,
                       CareerOpportunity.branch_name,
                       CareerOpportunity.corporate_logo,
					   corporate_prs.company_name,
					   corporate_prs.corporate_logo 
					   
				FROM career_opportunities AS CareerOpportunity 
					 LEFT JOIN corporate_prs  
					 ON (corporate_prs.id = CareerOpportunity.company_id)  
				
				WHERE CareerOpportunity.status = 1
				LIMIT $limit";
		
		$db = $this->getDataSource();
		$data = $db->fetchAll($sql);
		return $data;
	}
	
	protected function setCondition($params)
	{
		$pars = array();
		$where = "";

		if(!empty($params['keyword'])) {

			$where .= " AND ( job LIKE :_keyword 
												OR work_location LIKE :_keyword 
												OR work_features LIKE :_keyword 
												OR what_job LIKE :_keyword 
												OR corporate_prs.company_name LIKE :_keyword  
											)";
			$pars['_keyword'] = '%'.$params['keyword'].'%';
		}

		if(!empty($params['area']))
		{
			$arrArea = explode(',', $params['area']);
			$area = '';
			if($arrArea)
			{
				$area = end($arrArea);
			}
			$where .= " AND ( work_location LIKE :_work_location OR is_search=1)";
			$pars['_work_location'] = '%'.$area.'%';
		}
		else
		{
			if(!empty($params['group']))
			{
				$area = new Area();
				$provinces = $area->getProvinceByGroup($params['list_key_area']);

				if($provinces)
				{
					$where .= " AND (";

					foreach ($provinces as $key => $value) {
						if($key == 0)
						{
							$where .= "work_location LIKE '%" . $value['area']['name'] . "%'";
						}
						else
						{
							$where .= "OR work_location LIKE '%" . $value['area']['name'] . "%'";
						}
					}

					$where .= " OR is_search =1)";
				}
			}

		}

		if(!empty($params['em'])){
			$em_data = Configure::read('webconfig.employment_data');
			$em = $params['em'];
			$where .= " AND employment LIKE :_employment";
			$pars['_employment'] = '%'.$em.'%';
		}

		if(!empty($params['hotkey'])){
			$hotkeydata = Configure::read('webconfig.hotkey_data');
			$tmp = $params['hotkey'];
			$str_ = "";
			foreach ($tmp as $k => $v)
			{

				if($k==0){
					$str_ = " work_features LIKE :_hotkeydata$k";
				}else{
					$str_ .= " or work_features LIKE :_hotkeydata$k";
				}

				$pars['_hotkeydata'.$k] = '%'.$hotkeydata[$v].'%';
			}

			$where .= " and (" . $str_ . ") ";

		}
		
		$_conditions = array(
			'where' => $where,
			'pars' => $pars
		);
		return $_conditions;
	}
    public function TopSecretData(){
        $secret = $this->query("SELECT CareerOpportunity.*,corporate_prs.company_name,corporate_prs.corporate_logo,corporate_prs.id
						FROM career_opportunities AS CareerOpportunity LEFT JOIN corporate_prs  
									ON (corporate_prs.id = CareerOpportunity.company_id)  
						WHERE CareerOpportunity.status=1 ORDER BY CareerOpportunity.created DESC LIMIT 4;");
        return $secret;
    }
	
	/**
	 * combine: getInfoByBranchName, converDataToCallcenterMatome
	 * @param  $branch_name
	 * @return array callcenter
	 */
	public function getAndConvertInfoByBranchName($branch_name)
	{
		$data = $this->getInfoByBranchName($branch_name);
		$data = $this->converDataToCallcenterMatome($data);
		
		return $data;
	}
	
	/**
	 * get info by branch name
	 * @param  $branch_name
	 * @return array database
	 */
	public function getInfoByBranchName($branch_name)
	{
		$options = array(
			'branch_name LIKE' => "%$branch_name%",
		);
		
		$fields = array('DISTINCT CareerOpportunity.branch_name',
						'CareerOpportunity.id', 
						'CareerOpportunity.company_id', 
						'CareerOpportunity.map_location',
						'CareerOpportunity.lat',
						'CareerOpportunity.lng',
						
						'CorporatePrs.website',
						//'CorporatePrs.business',
					   );
		
		$join = array(
			array(
				'table' => 'corporate_prs',
				'alias' => 'CorporatePrs',
				'conditions' => array('CareerOpportunity.company_id = CorporatePrs.id')
			)	
		);
		
		$rs = $this->find('all', array(
			'fields' => $fields,
			'joins' => $join,
			'conditions' => $options));
		return $rs;
	}
	
	/**
	 * $data is a list of this table and will convert to Callcenter style
	 * 
	 * Input:
	 * Array(
	 * 		Array(
	 *   		'CareerOpportunity' => array('id' => 3, 'company_id' => 1, ...)
	 *   	)
	 * )
	 * 
	 * Output:
	 * Array(
	 * 		Array(
	 *   		'Callcenter' => array('id' => 116, 'city_id' => 1536, ...)
	 *     		'Area' => array('name' => '広島市中区', 'level' => 1)
	 *   	)
	 * )
	 * 
	 * @param array $record
	 * @return array database
	 */
	public function converDataToCallcenterMatome($data)
	{
		$callcenter = array();
		foreach( $data as $record)
		{
			if(empty($record['CareerOpportunity']) || empty($record['CorporatePrs']))
			{
				continue;
			}
			$COrecord = $record['CareerOpportunity'];
			$CPrecord = $record['CorporatePrs'];
			
			//---
			$newArea = array();
			$newArea['name'] = '';
			$newArea['level'] = 1;
			
			//---
			$newCallcenter = array();
			$newCallcenter['id'] = $COrecord['id'].'_'.$COrecord['company_id'];
			$newCallcenter['city_id'] = $COrecord['company_id'];
			$newCallcenter['keywords'] = $COrecord['branch_name'];
			$newCallcenter['company_name'] = $COrecord['branch_name'];
			$newCallcenter['address'] = $COrecord['map_location'];
			$newCallcenter['tel'] = '';
			$newCallcenter['link'] = $CPrecord['website'];
			$newCallcenter['business'] = '';
			
			$newCallcenter['lat'] = $COrecord['lat'];
			$newCallcenter['lng'] = $COrecord['lng'];
			
			$newRecord['Callcenter'] = $newCallcenter;
			$newRecord['Area'] = $newArea;
			$callcenter[] = $newRecord;
		}
		
		return $callcenter;
	}
}
