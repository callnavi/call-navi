<?php
App::uses('AppModel', 'Model');
/**
 * ContentsEmail Model
 *
 */
class PickUp extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $useTable = 'pick_up';
	
	/**
	 * 
	 * @return array (
	 *                => NAME,
	 *                => URL,
	 *                => IMAGE,
	 *                => DATE,
	 *                => VIEW
	 *                )
	 */
	public function getPickUp()
	{
		$data =  $this->find('first', [
			'fields' => array('id','type','article_id','status'),
			'conditions' => array('status' => 1),
			'order' => array('modified DESC')
		]);

		if($data)
		{
			$pickup = $data['PickUp'];
			switch($pickup['type'])
			{
				//Type content
				case 1:
					$modelContent = ClassRegistry::init('Contents');
					$rsContent =  $modelContent->find('first', [
						 'fields' => array(
							 			   'id',
										   'priority',
										   'Contents.status',
										   'site_id',
										   'province_id',
										   'url',
										   'category',
										   'view', 
										   'title_hint',
										   'title',
										   'image',
										   'category_alias',
                    					   'title_alias',
							 			   'Contents.created','category.category',
                    					   'TIMESTAMPDIFF(hour, Contents.created, NOW() ) AS totalHour'
										  ),
						'conditions' => array('Contents.status' => 1,'Contents.id' => $pickup['article_id'] ),
						'joins' => array(
							array(
								'table' => 'category',
								'type' => 'LEFT',
								'conditions' => array(
									'category.category_alias = Contents.category_alias',
								),
							),
						)
					]);

					$image = $rsContent['Contents']['image'];
					if (!empty($image)){
						$pos = strpos($image, "ccwork");
						if($pos === false)
						{
							$image= '/upload/contents/'.$image;
						}							
					}
					else
					{
						$image = '/img/instead/img_no-image_' . sprintf("%02d", rand(1, 10)) . '.jpg';
					}
					$title = $rsContent['Contents']['title'];
					$url = '/contents/'
							.$rsContent['Contents']['category_alias']
							.'/'
							.$rsContent['Contents']['title_alias'];
					
					$createDate = $rsContent['Contents']['created'];
					$view = $rsContent['Contents']['view'];

					$id = $rsContent['Contents']['id'];
					$cat_names = $rsContent['category']['category'];
					$cat_alias = $rsContent['Contents']['category_alias'];
					$title_alias = $rsContent['Contents']['title_alias'];
					
					return array(
						'title' 	  => $title,
						'url' 		  => $url,
						'image' 	  => $image,
						'createDate'  => $createDate,
						'view'		  => $view,
						'id' 		  => $id,
						'cat_names'   => $cat_names,
						'cat_alias'   => $cat_alias,
						'title_alias' => $title_alias,
						'type'		  => 'content',
					);
					
				//Type blog
				case 2:
					$modelBlog = ClassRegistry::init('Blogs');
					$rsBlog = $modelBlog->find('first', [
						 'fields' => array(
							 			   'id',
							 			   'Blogs.title',
										   'Blogs.status',
										   'pic',
										   'view',
										   'tag',
										   'Blogs.friendly_title',
										   'blogs_category.title as cat_title', 
										   'blogs_category.pic as cat_pic',
										   'blogs_category.category_alias',
										   'blogs_category.color',
										   'blogs_category.motto',
										   'blogs_category.interested',
										   'blogs_category.avatar',
										   'blogs_category.priority',
										   'blogs_category.friendly_title as cat_friendly_title',
							 			   'Blogs.created',
                    					   'TIMESTAMPDIFF(hour, Blogs.created, NOW() ) AS totalHour'
										  ),
						'conditions' => array('Blogs.status' => 1,'Blogs.id' => $pickup['article_id'] ),
						'joins' => array(
							array(
								'table' => 'blogs_category',
								'type' => 'LEFT',
								'conditions' => array(
									'blogs_category.cat_id = Blogs.cat_id',
								),
							),
						)
					]);
					
					$image = '/upload/blogs/' . $rsBlog['Blogs']['pic'];
					$title = $rsBlog['Blogs']['title'];
					$id = $rsBlog['Blogs']['id'];
					$url = '/blog/'
							.$rsBlog['blogs_category']['category_alias']
							.'/'
							.$id;

					$createDate = $rsBlog['Blogs']['created'];
					$view = $rsBlog['Blogs']['view'];
					
					return array(
						'title' 	  => $title,
						'url' 		  => $url,
						'image' 	  => $image,
						'createDate'  => $createDate,
						'view'		  => $view,
						'id' 		  => $id,
						'type'		  => 'blog'	
					);
				//Type news
				case 3:
					return [2];
					break;
			}
		}
		
		return null;
	}

}
