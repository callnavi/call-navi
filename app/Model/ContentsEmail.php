<?php
App::uses('AppModel', 'Model');
/**
 * ContentsEmail Model
 *
 */
class ContentsEmail extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $useTable = 'contents_email';
	
	public function getList($page, $size, $cat=null, $rand= null, $keywords= null, $isAdmin= null)
	{
		/*
			When intval(cat.id) > 0 => insert: where cat.id = {cat_id}
		*/
		$catQuery = '';
		$keywordsQuery = '';
		$start= ($page-1)*$size;
		$end = $size;
		//$pars = array('start' =>$start ,'end' => $end);
		$pars = [];
		if(!empty($cat))
		{
			$catQuery = "where cat.category_alias = :category_alias";
			$pars['category_alias'] = $cat;
		}

		if(!empty($keywords)){
			$keywordsQuery = "AND econtents.Title like :keywords ";
			$pars['keywords'] = '%'.$keywords.'%';
		}

		$status = empty($isAdmin) ? 'econtents.status = 1 ' :  'econtents.status <> 2 ';
		$order = !empty($rand) && $rand==1 ? 'rand()' : 'econtents.created';
		$query = 'select econtents.created,econtents.send_date ,econtents.featured_picture, econtents.id, econtents.Subtitle, econtents.view, econtents.Title,econtents.source,
		 		  TIMESTAMPDIFF(hour, econtents.send_date, NOW() ) AS totalHour,
		 		  group_cat.cat_names, group_cat.cat_ids, group_cat.cat_alias

				  from contents_email econtents inner join

				  (select ecat.content_id, GROUP_CONCAT(cat.category) as cat_names, GROUP_CONCAT(cat.id) as cat_ids , GROUP_CONCAT(cat.category_alias) as cat_alias

				  from contents_email_category ecat inner join category cat on ecat.email_to = cat.email_to 

				  '.$catQuery.'
				  
				  group by ecat.content_id) group_cat on econtents.id = group_cat.content_id
				  where '.$status.$keywordsQuery.'
				  order by '.$order.' desc limit '.$start.','.$end;
		$db = $this->getDataSource();
		
		$data = $db->fetchAll($query, $pars);
		return $data;
	}
	
	public function countList($cat=0)
	{
		$catQuery = '';
		
		$pars = [];
		
		if($cat>0)
		{
			$catQuery = 'where cat.id = :cat_id';
			$pars['cat_id'] = $cat;
		}

		$query = 'select count(econtents.id) as countList

				  from contents_email econtents inner join

				  (select ecat.content_id, GROUP_CONCAT(cat.category) as cat_names, GROUP_CONCAT(cat.id) as cat_ids 

				  from contents_email_category ecat inner join category cat on ecat.email_to = cat.email_to 
				  
				  '.$catQuery.'
				  
				  group by ecat.content_id) group_cat on econtents.id = group_cat.content_id  where econtents.status = 1';
		
		$db = $this->getDataSource();
		
		$data = $db->fetchAll($query, $pars);
		if(isset($data[0][0]['countList']) && $data[0][0]['countList'] > 0)
		{
			return $data[0][0]['countList']-1;
		}
		return 0;
	}

	public function countListByAlias($catAlias=null, $keywords= null, $isAdmin = null)
	{
		$catQuery = '';
		$keywordsQuery ='';
		$pars = [];

		if(!empty($catAlias))
		{
			$catQuery = "where cat.category_alias = :cat_alias";
			$pars['cat_alias'] = $catAlias;
		}

		if(!empty($keywords)){
			$keywordsQuery = "AND econtents.Title like :keywords ";
			$pars['keywords'] = '%'.$keywords.'%';
		}
		$status = empty($isAdmin) ? 'econtents.status = 1 ' :  'econtents.status <> 2';
		$query = 'select count(econtents.id) as countList

				  from contents_email econtents inner join

				  (select ecat.content_id

				  from contents_email_category ecat inner join category cat on ecat.email_to = cat.email_to

				  '.$catQuery.'

				  group by ecat.content_id) group_cat on econtents.id = group_cat.content_id  where '.$status.$keywordsQuery;

		$db = $this->getDataSource();

		$data = $db->fetchAll($query, $pars);
		if(isset($data[0][0]['countList']) && $data[0][0]['countList'] > 0)
		{
			return $data[0][0]['countList'];
		}
		return 0;
	}
	

	
	
	public function getRankList($top=5)
	{
		$query = 'select ContentsEmail.created, ContentsEmail.send_date, ContentsEmail.featured_picture, ContentsEmail.id, ContentsEmail.Subtitle, ContentsEmail.view, ContentsEmail.Title,
		 		  TIMESTAMPDIFF(hour, ContentsEmail.send_date, NOW() ) AS totalHour,
		 		  group_cat.cat_names, group_cat.cat_ids, group_cat.cat_alias

				  from contents_email ContentsEmail inner join

				  (select ecat.content_id, GROUP_CONCAT(cat.category) as cat_names, GROUP_CONCAT(cat.id) as cat_ids , GROUP_CONCAT(cat.category_alias) as cat_alias

				  from contents_email_category ecat inner join category cat on ecat.email_to = cat.email_to 
				 
				  group by ecat.content_id) group_cat on ContentsEmail.id = group_cat.content_id
				  where ContentsEmail.status = 1
				  order by ContentsEmail.view desc limit '.$top;
		
		$db = $this->getDataSource();
		
		$data = $db->fetchAll($query);
		
		return $data;
	}

	public function getDetail($id, $isAdmin= null)
	{
		$status = empty($isAdmin) ? 'econtents.status = 1 ' :  'econtents.status <> 2 ';

		$query = 'select econtents.created,econtents.send_date ,econtents.featured_picture, econtents.id, econtents.Subtitle, econtents.view, econtents.Title,econtents.status,econtents.source,
					econtents.header, econtents.Excerpt, econtents.content, econtents.relation_link, econtents.subject,
		 		  TIMESTAMPDIFF(hour, econtents.send_date, NOW() ) AS totalHour,
		 		  group_cat.cat_names, group_cat.cat_ids, group_cat.cat_alias

				  from contents_email econtents inner join

				  (select ecat.content_id, GROUP_CONCAT(cat.category) as cat_names, GROUP_CONCAT(cat.id) as cat_ids , GROUP_CONCAT(cat.category_alias) as cat_alias

				  from contents_email_category ecat inner join category cat on ecat.email_to = cat.email_to

				  group by ecat.content_id) group_cat on econtents.id = group_cat.content_id
				  where '.$status.' and econtents.id='.$id;
		$db = $this->getDataSource();

		$data = $db->fetchAll($query);
		return $data;
	}
}
