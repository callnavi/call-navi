<?php
App::uses('AppModel', 'Model');
/**
 * ContentsEmail Model
 *
 */
class Contact extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $useTable = 'Contact';
	
	/**
	 * 
	 * @return array (
	 *                => NAME,
	 *                => URL,
	 *                => IMAGE,
	 *                => DATE,
	 *                => VIEW
	 *                )
	 */

	
	public function add_new_data($data)
	{
        
        //get last id 
        
        $lastId = $this->query("SHOW TABLE STATUS WHERE name = 'Contact'");

        // Field into database
		$savedData['id'] = $lastId[0]['TABLES']['Auto_increment'];
		$savedData['company'] = $data['company'];
		$savedData['name'] = $data['name'];
		$savedData['mail'] = $data['mail'];
		$savedData['address'] = $data['address'];
		$savedData['phone'] = $data['phone'];
		$savedData['inquiry'] = $data['inquiry'];
        $savedData['remarks'] = $data['remarks'];
		
        //Create and save data into database
		$this->create();
		$this->save($savedData);
		$flag = false;
        
        if($this->id = $lastId[0]['TABLES']['Auto_increment'] ){
            $flag = true;
        }
        
		return $flag;
	}

	
	public function add_new_data_testmode($data)
	{
            $flag = true;
		return $flag;
	}

}