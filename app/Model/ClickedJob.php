<?php
App::uses('AppModel', 'Model');
/**
 * ContentsEmail Model
 *
 */
class ClickedJob extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $useTable = 'clicked_jobs';
	
	public function raiseUpClick($data)
	{
		$unique_id = $data['unique_id'];

		$job = $this->find('first', array(
											'fields' 		=> array('id','clicked'),
											'conditions' 	=> array('unique_id' => $unique_id),
    									)
						  );
		if(empty($job))
		{
			$this->create();
			$result = $this->save(array(
										'clicked' 	=> 1,
										'unique_id' => $unique_id,
										'site_id' 	=> $data['site_id'],
										'url' 		=> $data['url'],
									   )
								 );
		}
		else
		{
			$id = $job['ClickedJob']['id'];
			$clicked = $job['ClickedJob']['clicked'] + 1;
			$result = $this->save(array(
										'id' => $id, 
										'clicked' => $clicked
									   )
								 );
		}
		
		
		return $result;
	}

}
