<?php
App::uses('AppModel', 'Model');
/**
 * ContentsEmail Model
 *
 */
class Contents extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $useTable = 'contents';
	
	
	public function viewNewsContentsBlogs($page, $size, $keyword=null)
	{
		$condition = '';
		$pars = array();
		if($keyword)
		{
			if(is_array($keyword))
			{
				foreach( $keyword as $index=>$val)
				{
					if($val)
					{
						$argName = ':_keyword'.$index;
						$condition .= " list.title like $argName or list.cat_names like $argName or";
						$pars[$argName] = "%$val%";
					}
				}
				if($condition)
				{
					$condition = ' where '.rtrim($condition, 'or');
				}
			}
			else
			{
				$argName = ':_keyword';
				$condition = " where list.title like $argName or list.cat_names like $argName";
				$pars[$argName] = "%$keyword%";
			}
		}
		$start= ($page-1)*$size;
		$end = $size;
		$db = $this->getDataSource();
		$query = "select list.* from v_news_contents_blogs list 
				  $condition
				  order by list.created desc limit $start,$end";
		$data = $db->fetchAll($query,$pars);
		return $data;
	}
	public function viewNewsContentsBlogsCount()
	{
		$db = $this->getDataSource();
		$query = 'select count(*) as countList from v_news_contents_blogs';
		$data = $db->fetchAll($query);
		if(isset($data[0][0]['countList']) && $data[0][0]['countList'] > 0)
		{
			return $data[0][0]['countList']-1;
		}
		return 0;
	}
	
	public function viewRankingNewsContentsBlogs($limitNumberEveryType, $limitResult)
	{
		$db = $this->getDataSource();
		$query = "call prc_ranking_news_contents_blogs($limitNumberEveryType,$limitResult)";
		$data = $db->fetchAll($query);		
		return $data;
	}
	
	public function viewRankingNewsContentsBlogs_SP()
	{
		$db = $this->getDataSource();
		$query = "select list.* from v_ranking_news_contents_blogs_sp list order by `view` desc limit 5";
		$data = $db->fetchAll($query);
		return $data;
	}

	public function countJobToday()
	{
		$db = $this->getDataSource();
		$query = "select count(id) as totalJob from contents where date(created) = CURRENT_DATE()";
		$data = $db->fetchAll($query);
		if($data && !empty($data[0][0]['totalJob']))
		{
			return $data[0][0]['totalJob'];
		}
		return 0;
	}
	
	public function countSecretJobToday()
	{
		$db = $this->getDataSource();
		$query = "select count(id) as totalJob from career_opportunities where date(created) = CURRENT_DATE()";
		$data = $db->fetchAll($query);
		if($data && !empty($data[0][0]['totalJob']))
		{
			return $data[0][0]['totalJob'];
		}
		return 0;
	}


	public function getListSecretJobsHasInterview()
	{
		return $this->find('all', [
			'fields' => array('id','company_id','secret_job_id','title'),
			'conditions' => array('status' => 1, 'company_id IS NOT NULL',  'secret_job_id IS NOT NULL')
		]);
	}
    public function topInterviewGetId($secretId)
	{
		return $this->find('all', [
			'fields' => array('id','company_id','secret_job_id','title'),
			'conditions' => array('status' => 1, 'company_id IS NOT NULL', 'secret_job_id' => $secretId)
		]);
	}
}
