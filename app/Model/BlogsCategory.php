<?php
App::uses('AppModel', 'Model');
/**
 * ContentsEmail Model
 *
 */
class BlogsCategory extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $primaryKey = 'cat_id';
	public $useTable = 'blogs_category';

	public function getBlogCat($page, $size)
	{
		$start= ($page-1)*$size;
		$end = $size;
		//order by BlogsCategory.created DESC
		$query = "select blogs.created, blogs.cnt, BlogsCategory.* 
					from blogs_category BlogsCategory
					left join (
						select max(created) as created, cat_id, count(cat_id) as cnt
						from blogs where blogs.status = 1
						group by(cat_id)
					) blogs on BlogsCategory.cat_id = blogs.cat_id
					order by BlogsCategory.priority
                	limit $start,$end";

		$db = $this->getDataSource();
		$data = $db->fetchAll($query);
		return $data;
	}

	public function getBlogCatInfo($alias)
	{

		$query = "select blogs.created, blogs.cnt, BlogsCategory.* 
                from blogs_category BlogsCategory
                left join (
					select max(created) as created, cat_id, count(cat_id) as cnt
					from blogs where blogs.status = 1
					group by(cat_id)
                ) blogs on BlogsCategory.cat_id = blogs.cat_id
                WHERE BlogsCategory.category_alias = '".$alias."'
                order by BlogsCategory.created DESC
                limit 0,1";

		$db = $this->getDataSource();
		$data = $db->fetchAll($query);
		return $data;
	}
}
