<?php
App::uses('AppModel', 'Model');
/**
 * ContentsEmail Model
 *
 */
class Entry extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $useTable = 'entry';
    
	
	/**
	 * 
	 * @return array (
	 *                => NAME,
	 *                => URL,
	 *                => IMAGE,
	 *                => DATE,
	 *                => VIEW
	 *                )
	 */
    

    protected $_salt = '@DSOFDS*($^%$NKNSADUXOASDNA99#@!CDAS&*#)%*$^Y%&(&#E~~~BGQWDASISDAS';
	
	public function check_only($id)
	{
		$options = array(
			'id' => $id,
			'expired_to_download >' => date('Y-m-d')
		);
		$fields = array('cv1_path', 'cv2_path', 'job_id', 'created');
		$rs = $this->find('first', array(
			'fields' => $fields,
			'conditions' => $options));			  
		return $rs['Entry'];
	}
	
	public function check_and_get_file($id, $password, $file)
	{
		$rs = $this->check_and_get(
								   $id, 
								   $password, 
								   array('cv1_path', 'cv2_path', 'job_id', 'created')
								  );
		if($rs)
		{
			$fileName = '';
			switch($file)
			{
				case 'cv1': 
					if(!empty($rs['cv1_path']))
					{
						$fileName = $rs['cv1_path'];
					}
					break;
				case 'cv2': 
					if(!empty($rs['cv2_path']))
					{
						$fileName = $rs['cv2_path'];
					}
					break;
				default:
				break;
			}
			
			if(!empty($fileName))
			{
				$file = $this->getFile($fileName, $rs['created'], $rs['job_id']);
				if(file_exists($file))
				{
					return $file;
				}
			}
		}
		
		return '';
	}
	
	public function check_and_get($id, $password, $fields=array('cv1_path', 'cv2_path'))
	{
		$options = array(
			'id' => $id,
			'password' => md5($password.$this->_salt),
			'expired_to_download >' => date('Y-m-d')
		);
		
		$rs = $this->find('first', array(
			'fields' => $fields,
			'conditions' => $options));	
		if($rs)
		{
			return $rs['Entry'];
		}
		return false;
	}
	
	public function add_new($data , $apply_code)
	{
		if(empty($data['step1cv1_file']) && empty($data['step1cv2_file']))
		{
			return 0;
		}
		
        $savedData['job_id'] = $data['job_id'];
		$savedData['apply_code'] = $apply_code;
        $savedData['full_name'] = $data['step1FullName'];
		$savedData['phonetic_name'] = $data['step1Phonetic'];
		$savedData['birth_date'] = $data['step1Birth'];
		$savedData['gender'] = $data['step1gender'];
		$savedData['phone_number'] = $data['step1Phone'];
		$savedData['e-mail'] = $data['step1Email'];
        $savedData['skill'] = $this->combineSkill($data['step1skill']);
        if(isset($data['step1time'])){
            $savedData['working_time'] = $data['step1time'];
        }
        $savedData['content'] = $data['step1Content'];
        
        
        $savedData['expired_to_download'] = date('Y-m-d', strtotime("+3 days"));
		$savedData['password'] = md5($data['password'].$this->_salt);
		
		$this->create();
		$this->save($savedData);
		
		if($this->id)
		{
            if(isset($data['step1cv1_file'])){
			$cv1_path = $this->copyFileFromEntryCVtmp($data['step1cv1_file']['tmp_name'], 
										  $data['step1cv1_file']['name'], 
										  $this->id, $data['job_id'],  'cv1');
			$cv1_name = $data['step1cv1_file']['name'];                
            }
            if(isset($data['step1cv2_file'])){
			$cv2_path = $this->copyFileFromEntryCVtmp($data['step1cv2_file']['tmp_name'], 
										  $data['step1cv2_file']['name'], 
										  $this->id, $data['job_id'],  'cv2'); 

			$cv2_name = $data['step1cv2_file']['name'];                              
            }            
			
			//update again
			$update = array();
			$update['id'] 		= $this->id;
            if(isset($data['step1cv1_file'])){
                $update['cv1_path'] = $cv1_path?$cv1_path:'';
                $update['cv1_name'] = $cv1_name?$cv1_name:'';             
            }
            if(isset($data['step1cv2_file'])){
                $update['cv2_path'] = $cv2_path?$cv2_path:''; 
                $update['cv2_name'] = $cv2_name?$cv2_name:'';            
            }     
			
			$this->save($update);
			return $this->getLastInsertId();
		}
		else
		{
			return 0;
		}
	}	
    
    // adding new data without CVS
	public function add_new_nocv($data, $apply_code)
	{
        
		
        $savedData['job_id'] = $data['job_id'];
        //generate apply code with rank 
		$savedData['apply_code'] = $apply_code;
        $savedData['full_name'] = $data['step1FullName'];
		$savedData['phonetic_name'] = $data['step1Phonetic'];
		$savedData['birth_date'] = $data['step1Birth'];
		$savedData['gender'] = $data['step1gender'];
		$savedData['phone_number'] = $data['step1Phone'];
		$savedData['e-mail'] = $data['step1Email'];
        //combining array of skill into one string
        $savedData['skill'] = $this->combineSkill($data['step1skill']);
        //chenking data of working time has data or not
        if(isset($data['step1time'])){
            $savedData['working_time'] = $data['step1time'];
        }
        $savedData['content'] = $data['step1Content'];
        
        
      
		$this->create();
		$this->save($savedData);
		
		if($this->id)
		{
			return $this->getLastInsertId();
		}
		else
		{
			return 1;
		}        
    }
	public function saveTmpCv1($fileTmp, $originalName, $jobId)
	{
		$path = ROOT.'/Entry_CV_tmp/'.$jobId.'/';
		return $this->uploadFile($fileTmp, $originalName, microtime(true)*10000, $jobId, 'cv1', $path);
	}
	
	public function saveTmpCv2($fileTmp, $originalName, $jobId)
	{
		$path = ROOT.'/Entry_CV_tmp/'.$jobId.'/';
		return $this->uploadFile($fileTmp, $originalName, microtime(true)*10000, $jobId, 'cv2', $path);
	}
	
	protected function copyFileFromEntryCVtmp($fileTmp, $originalName, $entryId, $jobId, $prefix, $folder='/Entry_CV_files/')
	{
		if(empty($fileTmp) || empty($originalName))
		{
			return '';
		}
		$path= ROOT.$folder.date('y-m-d').'/'.$jobId.'/';
		return $this->copyFile($fileTmp, $originalName, $entryId, $jobId, $prefix, $path);
	}
	
	protected function uploadFile($fileTmp, $originalName, $entryId, $jobId, $prefix, $path)
	{
		if(!file_exists($path)){
			mkdir($path, 0755, true);
		}
		$ext = pathinfo($originalName, PATHINFO_EXTENSION);
		$fileName = $entryId.'_'.$jobId.'_'.$prefix.'.'.$ext;
		$movedFile = $path.$fileName;
		$rs = move_uploaded_file($fileTmp, $movedFile);
		return $movedFile;
	}
	
	protected function copyFile($fileTmp, $originalName, $entryId, $jobId, $prefix, $path)
	{
		if(!file_exists($path)){
			mkdir($path, 0755, true);
		}
		$ext = pathinfo($originalName, PATHINFO_EXTENSION);
		$fileName = $entryId.'_'.$jobId.'_'.$prefix.'.'.$ext;
		$movedFile = $path.$fileName;
		$rs = copy($fileTmp, $movedFile);
		return $fileName;
	}
	
	protected function getFile($fileName, $date, $job_id)
	{
		$folder = '/Entry_CV_files/';
		$path= ROOT.$folder.date('y-m-d', strtotime($date)).'/'.$job_id.'/'.$fileName;
		return $path;
	}



	/*
		Created at: 2017-07-19
		Author: Binh
		Des: download file
		$fileName: path file
		$date: date upload, exp: 2017-07-19 04:43:11
		$id: id of secret job

		return: true => download success, false => download failed
	*/
	public function downloadFile($fileName, $date, $id)
	{
		$file = $this->getFile($fileName, $date, $id);

		if (file_exists($file)) {
		    header('Content-Description: File Transfer');
		    header('Content-Type: application/octet-stream');
		    header('Content-Disposition: attachment; filename="'.basename($file).'"');
		    header('Expires: 0');
		    header('Cache-Control: must-revalidate');
		    header('Pragma: public');
		    header('Content-Length: ' . filesize($file));
		    readfile($file);
		    return true;
		}
		return false;
	}
    //function combining array of entry data to one string
    private function combineSkill($arr)
    {
        $result = "";
            foreach($arr as $data){
                if ($result == ""){
                    $result = $data;                     
                }else{
                    $result = $result . "," .$data; 
                }
            }
        return $result;
    }
    //combining date format for insert into databasa
    private function combineDate($date)
    {
        $result = substr($date,0,4) ."-". substr($date,4,2) ."-". substr($date,6,2) ;
        return $result;
    }
}
