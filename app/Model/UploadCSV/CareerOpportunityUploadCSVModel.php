<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppUploadCSVModel', 'Model');
App::import('Model', 'CorporatePrs');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class CareerOpportunityUploadCSVModel extends AppUploadCSVModel {
	
	public $useTable = 'career_opportunities';
	public $company = 0;
	public $companyName = null;

	protected $header = array(
		'求人ID' => 'job_code',
		'募集コールセンター名' => 'company_name',
		'募集コールセンター名（支店名）' => 'branch_name',
		'求人タイトル' => 'job',
		'お祝い金' => 'celebration',
		'トップ画像（最大サイズ：1メガバイト）' => 'corporate_logo',
		'コールナビ編集部コメント' => 'comment',
		'Map Location' => 'map_location',
		'公開日' => 'release_date',
		'全地域の検索' => 'is_search',
		'雇用形態' => 'employment',
		'求人特徴 (複数選択可）' => 'work_features',
		'仕事内容（概要）' => 'what_job',
		'勤務地' => 'work_location',
		'表示' => 'map_show',
		'アクセス' => 'access',
		'どんな人が働いていますか？' => 'what_kind_person',
		'上級アバター（最大サイズ：1メガバイト）' => 'senior_avata',
		'スタッフのお名前' => 'senior_name_of_staff',
		'インタビュータイトル' => 'senior_joining_history',
		'インタビュー内容' => 'senior_employee_interview',
		'仕事内容（詳細）' => 'specific_job',
		'対象となる方' => 'person_of_interest',
		'勤務時間' => 'working_hours',
		'給与' => 'salary',
		'業種' => 'business',
		'給与（見出し）' => 'hourly_wage',
		'こんな経験・スキルが活かせます' => 'skill_experience',
		'休日・休暇' => 'holiday_vacation',
		'待遇・福利厚生' => 'health_welfare',
		'応募方法' => 'application_method',
		'入社特典' => 'benefit',
		'応募先メールアドレス' => 'application_email',
		'その他備考' => 'other_remark',
		'サムネイル1' => 'thumb_1',
		'サムネイル2' => 'thumb_2',
		'サムネイル3' => 'thumb_3',
		'作成日' => 'created',
		'履歴書-職務経歴書・ショー' => 'is_upload_cv',
		'履歴書-職務経歴書・必須' => 'required_upload_cv',
		'パブリッシュ' => 'status',
	);
	
	//check Invalid before process
	protected function checkInvalid($record)
	{
		if(empty($record['branch_name']) || empty($record['job']))
		{
			return false;
		}
		return true;
	}	
	
	//@override this function please !!!
	protected function process($id, &$record, &$recordForReport)
	{
		$record['modified']= date("Y-m-d H:i:s");
		
		if(empty($record['created']))
		{
			$record['created']= date("Y-m-d H:i:s");
		}
		else
		{
			$this->process_date('created', $record, $recordForReport);
		}
		
		$this->process_date('release_date', $record, $recordForReport);
		$record['company_id'] = $this->getCompanyIdFromName($record['company_name']);
		$this->process_text_to_html($id, $record, $recordForReport);
		$this->process_job_code($id, $record, $recordForReport);
		
		//add message only for report
		$recordForReport['company_name'] = $this->setCompanyNameResult($record);
	}
	

	//find ID or 0 to check Update or Insert
	//@return int ID
	protected function findIdFunction($record)
	{
		$rs = $this->find('first', array(
			'fields'=>array('id'),
			'conditions' => array('job' => $record['job'])
		));
		return empty($rs['CareerOpportunityUploadCSVModel']['id'])?
				0
				:
				$rs['CareerOpportunityUploadCSVModel']['id'];
	}
	
	
	
	//---
	private function process_text_to_html($id, &$record, &$recordForReport)
	{
		/*
		===Convert to HTML for:===
		
		'コールナビ編集部コメント' => 'comment',
		'仕事内容（概要）' => 'what_job',
		'勤務地' => 'work_location',
		'アクセス' => 'access',
		'どんな人が働いていますか？' => 'what_kind_person',
		'インタビュータイトル' => 'senior_joining_history',
		'インタビュー内容' => 'senior_employee_interview',
		'仕事内容（詳細）' => 'specific_job',
		'対象となる方' => 'person_of_interest',
		'勤務時間' => 'working_hours',
		'給与' => 'salary',
		'こんな経験・スキルが活かせます' => 'skill_experience',
		'休日・休暇' => 'holiday_vacation',
		'待遇・福利厚生' => 'health_welfare',
		'応募方法' => 'application_method',
		'入社特典' => 'benefit',
		'その他備考' => 'other_remark',
		*/
		$this->process_item_text_to_html('comment', $record, $recordForReport);
		$this->process_item_text_to_html('what_job', $record, $recordForReport);
		$this->process_item_text_to_html('work_location', $record, $recordForReport);
		$this->process_item_text_to_html('access', $record, $recordForReport);
		$this->process_item_text_to_html('what_kind_person', $record, $recordForReport);
		$this->process_item_text_to_html('senior_joining_history', $record, $recordForReport);
		$this->process_item_text_to_html('specific_job', $record, $recordForReport);
		$this->process_item_text_to_html('person_of_interest', $record, $recordForReport);
		$this->process_item_text_to_html('working_hours', $record, $recordForReport);
		$this->process_item_text_to_html('salary', $record, $recordForReport);
		$this->process_item_text_to_html('skill_experience', $record, $recordForReport);
		$this->process_item_text_to_html('holiday_vacation', $record, $recordForReport);
		$this->process_item_text_to_html('health_welfare', $record, $recordForReport);
		$this->process_item_text_to_html('application_method', $record, $recordForReport);
		$this->process_item_text_to_html('benefit', $record, $recordForReport);
		$this->process_item_text_to_html('other_remark', $record, $recordForReport);
	}
	
	private function process_job_code($id, &$record, &$recordForReport)
	{
		//check 求人ID, if exists => set empty
		if(!empty($record['job_code']))
		{
			$isDuplicatedCode = $this->find('first', array(
				'fields'=>array('id'),
				'conditions' => array(
										'job_code' => $record['job_code'],
										'company_id' => $record['company_id'],
									 )
			));
			
			//die;
			if($isDuplicatedCode)
			{
				//Update: exist ID and ID diff with Updated ID => make empty
				if($id)
				{
					if($isDuplicatedCode['CareerOpportunityUploadCSVModel']['id'] != $id)
					{
						$record['job_code'] = '';
						$recordForReport['job_code'] .='<br><span class="error">Duplicated</span>';
					}
				}
				//Insert: exist ID => make empty
				else
				{
					$record['job_code'] = '';
					$recordForReport['job_code'] .='<br><span class="error">Duplicated</span>';
				}
			}
		}

		return $record;
	}
	
	private function getCompanyIdFromName($companyName)
	{
		if($this->company)
		{
			return $this->company;
		}
		else
		{
			$corporate = new CorporatePrs();
			$company = $corporate->find('first', array(
            	'fields'=>array('id'),
				'conditions' => array('company_name' => $companyName)
        	));
			if($company)
			{
				return $company['CorporatePrs']['id'];
			}
		}
		return 0;
	}
	
	private function setCompanyNameResult($record)
	{
		if($this->company)
		{
			$companyName = $this->getCompanyName($this->company);
			$record['company_name'] .= '<br><span class="error"> used: '.$companyName.'</span>';
		}
		else
		{
			if(!$record['company_id'])
			{
				$record['company_name'] .= '<br><span class="error">Not exist this company</span>';
			}
		}
		return $record['company_name'];
	}
	
	/**
	 * Get and Set global company name
	 * @return STRING
	 */
	private function getCompanyName($id)
	{
		if( $this->companyName === null )
		{
			$corporate = new CorporatePrs();
			$company = $corporate->find('first', array(
            	'fields'=>array('company_name'),
				'conditions' => array('id' => $id)
        	));
			
			if($company)
			{
				$this->companyName = $company['CorporatePrs']['company_name'];
			}
			else
			{
				$this->companyName = 'Not exist company with id: '.$id;
			}
		}
		
		return $this->companyName;
	}
}
