<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppUploadCSVModel', 'Model');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class CorporatePrsUploadCSVModel extends AppUploadCSVModel {
	
	public $useTable = 'corporate_prs';
	
	protected $header = array(
		'法人ID'			=> 'corporate_code',
		'企業メッセージ' 	=> 'corporate',
		'社長のアバター' 	=> 'president_avata',
		'社長のお名前' 	=> 'president_name',
		'ご紹介文' 	=> 'president_description',
		'企業名' 	=> 'company_name',
		'WEBサイト' 	=> 'website',
		'事業内容' 	=> 'business',
		'本社・その他事業所' => 'other_location',
		'代表者' 	=> 'representative',
		'設立日' 	=> 'establishment_date',
		'資本金' 	=> 'capital',
		'従業員数'	=> 'employee_number',
	);
	
	//check Invalid before process
	protected function checkInvalid($record)
	{
		if(empty($record['company_name']))
		{
			return false;
		}
		return true;
	}	
	
	//@override this function please !!!
	protected function process($id, &$record, &$recordForReport)
	{
		$record['created']= date("Y-m-d H:i:s");
        $record['modified']= date("Y-m-d H:i:s");
		$this->process_date('establishment_date', $record, $recordForReport);
		$this->process_text_to_html($id, $record, $recordForReport);
		$this->process_corporate_code($id, $record, $recordForReport);
	}
	

	//find ID or 0 to check Update or Insert
	//@return int ID
	protected function findIdFunction($record)
	{
		$rs = $this->find('first', array(
				'fields'=>array('id'),
				'conditions' => array('company_name' => $record['company_name'])
			));

		return empty($rs['CorporatePrsUploadCSVModel']['id'])?
				0
				:
				$rs['CorporatePrsUploadCSVModel']['id'];
	}
	
	//---
	private function process_text_to_html($id, &$record, &$recordForReport)
	{
		/*
		'企業メッセージ' 	=> 'corporate',
		'ご紹介文' 	=> 'president_description',
		'事業内容' 	=> 'business',
		'本社・その他事業所' => 'other_location',
		'代表者' 	=> 'representative',
		'資本金' 	=> 'capital',
		'従業員数'	=> 'employee_number',
		*/
		$this->process_item_text_to_html('corporate', $record, $recordForReport);
		$this->process_item_text_to_html('president_description', $record, $recordForReport);
		$this->process_item_text_to_html('business', $record, $recordForReport);
		$this->process_item_text_to_html('other_location', $record, $recordForReport);
		$this->process_item_text_to_html('representative', $record, $recordForReport);
		$this->process_item_text_to_html('capital', $record, $recordForReport);
		$this->process_item_text_to_html('employee_number', $record, $recordForReport);
	}
	
	private function process_corporate_code($id, &$record, &$recordForReport)
	{
		//check 法人ID, if exists => set empty
		if(!empty($record['corporate_code']))
		{
			$isDuplicatedCode = $this->find('first', array(
				'fields'=>array('id'),
				'conditions' => array('corporate_code' => $record['corporate_code'])
			));

			if($isDuplicatedCode)
			{
				//Update: exist ID and ID diff with Updated ID => make empty
				if($id)
				{
					if($isDuplicatedCode['CorporatePrsUploadCSVModel']['id'] != $id)
					{
						$record['corporate_code'] = '';
						$recordForReport['corporate_code'] .='<br><span class="error">Duplicated</span>';
					}
				}
				//Insert: exist ID => make empty
				else
				{
					$record['corporate_code'] = '';
					$recordForReport['corporate_code'] .='<br><span class="error">Duplicated</span>';
				}
			}
		}

		return $record;
	}
}
