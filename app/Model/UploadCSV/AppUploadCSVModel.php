<?php
/**
 * Application model for CakePHP.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Model', 'Model');
App::uses('CakeSession', 'Model/Datasource');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppUploadCSVModel extends Model {
	
	public $useTable = null;
	protected $header = null;
	
	//@override this function please !!!
	//check Invalid before process
	protected function checkInvalid($record)
	{
		echo 'please rewrite me: AppUploadCSVModel@checkInvalid';
		die;
	}	
	
	//@override this function please !!!
	//progress DATA before insert/update and report
	protected function process($id, &$record, &$recordForReport)
	{
		echo 'please rewrite me: AppUploadCSVModel@process';
		die;
	}	
	
	//@override this function please !!!
	//find ID or 0 to check Update or Insert
	//@return int ID
	protected function findIdFunction($record)
	{
		echo 'please rewrite me: AppUploadCSVModel@findIdFunction';
		die;
	}
	
	//@override this function please !!!
	//re-progress DATA again in case Insert or Update
	//@return $record again
	protected function beforeInsertOrUpdate($id, $record)
	{
		echo 'please rewrite me: AppUploadCSVModel@beforeInsertOrUpdate';
		die;
	}
	
	/**
	 * for only test call function
	 */
	public function test()
	{
		echo 'connect successful';
		die;
	}
	
	/**
	 * Main function to call
	 * @param  class $csvComponent   [[Description]]
	 * @param  array [$file = null] [[Description]]
	 * @return array [[Description]]
	 */
	public function updateByCsv($csvComponent, $file= null){
		$resultData = [];
		$header = $this->header;
		$objHeader = $csvComponent->makeHeaderPosition($header, $file[0]);
		$file[0] = $csvComponent->convertToObjectByHeader($objHeader, $file[0]);
		$file[0]['ex_result'] = 'ステータス';
		$resultData[] = $file[0];
		unset($file[0]);
		foreach($file as $record)
		{
			$newRecord = $csvComponent->convertToObjectByHeader($objHeader, $record);
			$afterRecord = $this->updateToDatabase($newRecord);
			$resultData[] = $afterRecord;
		}
		return $resultData;
    }
	
	//progress DATA before insert/update
	protected function updateToDatabase($record)
	{
		if(!$this->checkInvalid($record))
		{
			$record['ex_result'] = 'Invalid data';
			return $record;
		}
		/*
			1. backup 1 record for report
			2. find ID to detect Update or Insert
			3. process DATA before Save and Report
			4. Save then return message
			5. return $recordForReport
		*/
		$recordForReport = $record;
		$id = $this->findIdFunction($record);
		$this->process($id, $record, $recordForReport);
		$afterSavedMessage = $this->updateOrInsertToDatabase($id, $record);
		$recordForReport['ex_result'] = $afterSavedMessage;
		
		return $recordForReport;
	}	
	
	protected function updateOrInsertToDatabase($id,$record)
	{
		if($id)
		{
			try
			{
				$this->id = $id;
				if(CakeSession::check('test_mode'))
				{
					return 'test mode/update type';
				}
				else
				{
					if($this->save($record))
					{
						return 'update success';
					}
					else
					{
						return 'update failed';
					}
				}
			}
			catch(Exception $e)
			{
				return 'update failed<br>'.$e->getMessage();
			}
			
		}
		else
		{
			try
			{
				if(CakeSession::check('test_mode'))
				{
					return 'test mode/insert type';
				}
				else
				{
					$this->create();
					if($this->save($record)) 
					{
						return 'insert success';
					}
					else
					{
						return 'insert failed';
					}
				}
			}
			catch(Exception $e)
			{
				return 'insert failed<br>'.$e->getMessage();
			}
		}
	}
	
	protected function convertToHTML($content)
	{
		$content = nl2br($content);
		
		return $content;
	}
	
	protected function validateDate($date, $format = 'Y-m-d')
	{
		$d = DateTime::createFromFormat($format, $date);
		return $d && $d->format($format) == $date;
	}
	
	protected function process_date($key, &$record, &$recordForReport)
	{
		if(!$this->validateDate($record[$key]))
		{
			//don't insert/update this Field
			unset($record[$key]);
			$recordForReport[$key] .= '<br><span class="error">Invalided date</span>';
		}
	}
	
	protected function process_item_text_to_html($key, &$record, &$recordForReport)
	{
		$record[$key] = $this->convertToHTML($record[$key]);
		$recordForReport[$key] = $record[$key];
	}
}
