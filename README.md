## Git rules
1. use developer/master instead of master branch
2. new format for branch name: feature/<name>

ex: feature/issue#177

## Filezilla Info
FTP : 52.196.59.96
Username : ec2-user
Path : /var/www/html/callnavitest
Key File : https://drive.google.com/open?id=0B2lNlQesIjieOVJzZXJXYS1KNUU

## 2017-06-15
Add Redirect in */var/www/html/callnavi/.htaccess* for: 

https://callnavi.jp/contents/saiyou/pre_interview

https://callnavi.jp/contents/saiyou/shift_work



## 2017-04-07
In issue 304, add new column
```sql
alter table career_opportunities
add is_search tinyint default 0;
```


## 2017-07-18 add new oiwakin table
In issue 324, add new column
```sql
create table oiwaikin
( id int,
    apply_id varchar(25),
    first_day_job varchar(255) CHARACTER SET utf8 ,
    name varchar(255) CHARACTER SET utf8,
    email varchar(255) CHARACTER SET utf8,
    company_name varchar(255) CHARACTER SET utf8,
    start_working_date varchar(255) CHARACTER SET utf8,
    kana_f_name varchar(255) CHARACTER SET utf8,
    kana_l_name varchar(255) CHARACTER SET utf8,
    remark text CHARACTER SET utf8
);
```


## 2017-07-20
In issue 323, 
add new column entry , carreer oportunity, corporateprs
```sql
alter table career_opportunities
add job_code varchar(5) default "";

alter table corporate_prs
add corporate_code varchar(5) default "";

alter table entry
add job_id int,
add apply_code varchar(25),
add full_name varchar(255)  CHARACTER SET utf8,
add phonetic_name varchar(255)  CHARACTER SET utf8,
add birth_date date,
add gender varchar(50)  CHARACTER SET utf8,
add phone_number varchar(11) ,
add e-mail varchar(100),
add skill varchar(255)  CHARACTER SET utf8,
add working_time varchar(255)  CHARACTER SET utf8,
add content text  CHARACTER SET utf8;


```


## 2017-07-21
In issue #324, add new celebration carreer oportunity

```sql
alter table career_opportunities
add celebration text CHARACTER SET utf8;
```

## 2017-07-27
In issue #327, add new map show carreer oportunity

```sql
alter table career_opportunities
add map_show tinyint default 1;
```

## 2017-07-29
In issue #330, add move data carreer oportunity

```sql
alter table career_opportunities
add corporate text CHARACTER SET utf8;
add corporate_logo varchar(500) CHARACTER SET utf8;
add comment text CHARACTER SET utf8;
add map_location varchar(500) CHARACTER SET utf8;
add lat double;
add lng double;
```

edit view for top page v_news_contents_blogs

```sql
(select 'news' AS `type`,`econtents`.`Title` AS `title`,`econtents`.`created` AS `created`,`econtents`.`send_date` AS `send_date`,`econtents`.`featured_picture` AS `image`,`econtents`.`id` AS `id`,`econtents`.`Subtitle` AS `sub_title`,`econtents`.`view` AS `view`,timestampdiff(HOUR,`econtents`.`send_date`,now()) AS `totalHour`,`group_cat`.`cat_names` AS `cat_names`,`group_cat`.`cat_ids` AS `cat_ids`,`group_cat`.`cat_alias` AS `cat_alias`,NULL AS `title_alias`,NULL AS `company_id`,NULL AS `secret_job_id` from (`crawler_email`.`contents_email` `econtents` join `crawler_email`.`v_cats_news` `group_cat` on((`econtents`.`id` = `group_cat`.`content_id`))) where (`econtents`.`status` = 1) order by `econtents`.`created` desc)

union (select 'contents' AS `type`,`crawler_email`.`contents`.`title` AS `title`,`crawler_email`.`contents`.`created` AS `created`,NULL AS `send_date`,`crawler_email`.`contents`.`image` AS `image`,`crawler_email`.`contents`.`id` AS `id`,NULL AS `sub_title`,`crawler_email`.`contents`.`view` AS `view`,timestampdiff(HOUR,`crawler_email`.`contents`.`created`,now()) AS `totalHour`,`crawler_email`.`category`.`category` AS `cat_names`,`crawler_email`.`category`.`id` AS `cat_ids`,`crawler_email`.`category`.`category_alias` AS `cat_alias`,`crawler_email`.`contents`.`title_alias` AS `title_alias`,`crawler_email`.`contents`.`company_id` AS `company_id`,`crawler_email`.`contents`.`secret_job_id` AS `secret_job_id` from (`crawler_email`.`contents` left join `crawler_email`.`category` on((`crawler_email`.`contents`.`category_alias` = `crawler_email`.`category`.`category_alias`))) where ((`crawler_email`.`category`.`category_type` = 2) and (`crawler_email`.`contents`.`status` = 1))) 

union (select 'blog' AS `type`,`crawler_email`.`blogs`.`title` AS `title`,`crawler_email`.`blogs`.`created` AS `created`,NULL AS `send_date`,`crawler_email`.`blogs`.`pic` AS `image`,`crawler_email`.`blogs`.`id` AS `id`,NULL AS `sub_title`,`crawler_email`.`blogs`.`view` AS `view`,timestampdiff(HOUR,`crawler_email`.`blogs`.`created`,now()) AS `totalHour`,(`crawler_email`.`blogs_category`.`title` collate utf8_general_ci) AS `cat_names`,`crawler_email`.`blogs_category`.`cat_id` AS `cat_ids`,(`crawler_email`.`blogs_category`.`category_alias` collate utf8_general_ci) AS `cat_alias`,NULL AS `title_alias`,NULL AS `company_id`,NULL AS `secret_job_id` from (`crawler_email`.`blogs` left join `crawler_email`.`blogs_category` on((`crawler_email`.`blogs`.`cat_id` = `crawler_email`.`blogs_category`.`cat_id`))) where (`crawler_email`.`blogs`.`status` = 1))
```

## 02-08-2017 #330 Add branch name
```sql
alter table career_opportunities
add branch_name text CHARACTER SET utf8;
```

## 11-08-2017 
#323 add name cv
```aql
alter table entry
add cv1_name varchar(255)  CHARACTER SET utf8,
add cv2_name varchar(255)  CHARACTER SET utf8;


## 25-08-2017 
#332 add data contact
```sql
create table Contact
( id int,
    company  varchar(255) CHARACTER SET utf8,
    name varchar(255) CHARACTER SET utf8 ,
    mail varchar(255) CHARACTER SET utf8,
    address varchar(255) CHARACTER SET utf8,
    phone varchar(12) CHARACTER SET utf8,
    inquiry varchar(255) CHARACTER SET utf8,
    remarks text CHARACTER SET utf8
);
```

##21-08-2017
#334 ADD SLIDER JOB
```
alter table contents
add slider_job_1 varchar(12) default "",
add slider_job_2 varchar(12) default "",
add slider_job_3 varchar(12) default "",
add slider_job_4 varchar(12) default "",
add slider_job_5 varchar(12) default "",
add slider_job_6 varchar(12) default "";
```

##16-10-2017
#348 ADD CONTENTS LINK ID IN JOB ADMIN
```
alter table career_opportunities

add company_contents_id int default 0,

add contents_id int default 0,

add contents_show int default 0;

```


##10-11-2017
#334 change to new table
```
CREATE TABLE slider_job (
	contents_id  int default 0,
	slider_job_id varchar(12) default "",
	slider_job_name varchar(12) default "",
	slider_job_priority int default 0
);
```

##11-12-2017
#372 CSv add new field
```
alter table career_opportunities
add use_skill text CHARACTER SET utf8;
```
##25-12-2017
#364 concierge page

```
CREATE TABLE concierge (
    id int NOT NULL PRIMARY KEY AUTO_INCREMENT,
    name varchar(255)  CHARACTER SET utf8,
    kana varchar(255)  CHARACTER SET utf8,
    birthday_date date ,
    place	 varchar(255) CHARACTER SET utf8,
    jobType varchar(255)  CHARACTER SET utf8,
    phone varchar(12)  ,
    mail varchar(255)  CHARACTER SET utf8,
    remarks varchar(255)  CHARACTER SET utf8
    
    );

```
