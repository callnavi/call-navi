<div id="conditionsSearch">
    <div class="contentsInner">
        <form action="/search/result" method="get">
            <div class="area"><input type="text" name="area"  value="<?php if (isset($params->area)) { ?><?php echo $params->area; ?><?php } ?>" placeholder="勤務地（例：東京都渋谷区、池袋駅、大阪市北区）"></div>
            <div class="keyword"><input type="text" name="keyword" value="<?php if (isset($params->keyword)) { ?><?php echo $params->keyword; ?><?php } ?>" placeholder="キーワード（例：長期、高時給、短期、派遣、日払い、通信、保険、不動産）"></div>
            <div class="search"><button type="submit">検索</button></div>
        </form>

        <?php echo $this->partial('/layouts/body/recommend_keywords'); ?>
        
    </div><!-- / contentsInner -->
</div><!-- / conditionsSearch -->
