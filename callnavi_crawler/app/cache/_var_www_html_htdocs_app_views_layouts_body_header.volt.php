
<input id="gnavCheckbox" type="checkbox">
<header id="globalHeader">
    <div id="floatBlockConteiner">
        <div id="floatingHeader">
            <h1 id="ci"><a href="/" class="js-baseurl">コールナビ</a></h1>
            <label for="gnavCheckbox" id="gnavIcon"></label>  
        </div>
    </div><!-- / floatBlockConteiner -->
    <p id="shoulder">全国のコールセンター転職・求人一括検索！<br class="mediaSP">アルバイト・中途・新卒もおまかせ！<span class="num"><br>コールセンター求人数
            <em>
                <?php if ($alloffers >= 1000) { ?>
                    <?php echo ($alloffers - $alloffers % 1000) / 1000; ?>,<!-- 
                    --><?php } ?><!--
                    --><?php if ($alloffers < 1000) { ?><?php echo $alloffers; ?><!--
                    --><?php } elseif ($alloffers % 1000 >= 100) { ?><!--
                    --><?php echo $alloffers % 1000; ?><!--
                    --><?php } elseif ($alloffers % 1000 >= 10) { ?><!--
                    -->0<?php echo $alloffers % 1000; ?><!--
                    --><?php } else { ?><!--
                    -->00<?php echo $alloffers % 1000; ?>
                <?php } ?>                                  
            </em>件（毎日更新!）</span></p>

    <nav id="globalNav">
        <ul>
            <li class="mediaSP"><a href="/">HOME</a></li>
            <li class="about"><a href="/about">コールナビとは？</a></li>
            <li class="mediaSP"><a href="/ccwork">コールセンターの仕事とは？</a></li>
            <li class="mediaSP"><a href="/ccwork/detail12">全国コールセンターまとめ</a></li>
            <li class="contact"><a href="https://callnavi.jp/contact/input.html">お問い合わせ</a></li>
        </ul>
    </nav>
</header>
