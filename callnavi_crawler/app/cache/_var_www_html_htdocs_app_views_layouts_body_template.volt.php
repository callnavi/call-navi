<?php if(!stristr($_SERVER['REQUEST_URI'], '/pickupModule')): ?>
<?php echo $this->partial('/layouts/body/header'); ?>

<?php if(!stristr($_SERVER['REQUEST_URI'], '/detail/index')): ?>
<?php echo $this->partial('/layouts/body/search'); ?>
<?php endif; ?>


<div id="contents">
    <div class="contentsInner">
        <div id="contentsContainer">
            <div id="mainContents">
                <!-- InstanceBeginEditable name="mainContents" -->
<?php endif; ?>
                <?php echo $this->getContent(); ?>

                <!-- InstanceEndEditable -->
            <?php if(!stristr($_SERVER['REQUEST_URI'], '/pickupModule')): ?>    
            </div><!-- / mainContents -->

            <div id="subContents">
                <nav id="sideNavigation" class="mediaPC">
                    <ul>
                        <li><a href="/">HOME</a></li>
                        <li><a href="/ccwork">コールセンターの<br>仕事とは？</a></li>
                        <li><a href="/ccwork/detail12">全国コールセンターまとめ</a></li>
                    </ul>
                </nav>
                <section>
                    <ul class="bnrListA01 alignC mediaPC">
                    <li><a class="imgHover" href="/about"><img alt="全国のコールセンター求人を一括検索　コールナビとは？" class="mediaPC" height="170" src="/public/images/common/bnr_about-callnavi_01.jpg" width="200"></a></li>
                    <li><a class="imgHover" href="/ccwork/detail02"><img alt="コールセンターで働くメリットとは？" class="mediaPC" height="170" src="/public/images/common/bnr_melit_01.jpg" width="200"></a></li>
                    <li><a class="imgHover" href="/ccwork/detail03"><img alt="全国調査 コールセンターの時給って！" class="mediaPC" height="170" src="/public/images/common/bnr_report_01.jpg" width="200"></a></li>
                    <li><a class="imgHover" href="/ccwork/detail05"><img alt="もう面接は怖くない！コールセンター面接で失敗しないコツ！" class="mediaPC" height="170" src="/public/images/common/bnr_report_02.jpg" width="200"></a></li>
                    <li><a class="imgHover" href="/ccwork/detail04"><img alt="コールセンターの雇用形態" class="mediaPC" height="170" src="/public/images/common/topics03.jpg" width="200"></a></li>
                    </ul>
                </section>
            </div><!-- / subContents -->
        </div><!-- / contentsContainer -->

        <div id="asideContents">
            <?php echo $this->partial('/offer/rectangle'); ?>

            <section class="mediaPC">
            <h2 class="headingA01 topics">トピックス</h2>
            <ul class="bnrListA01 alignC">
            <li><a class="imgHover" href="/ccwork/detail06"><img alt="ストレスに負けないコツ！" class="mediaPC" height="170" src="/public/images/common/topics08.jpg" width="200"></a></li>
            <li><a class="imgHover" href="/ccwork/detail09"><img alt="知っておきたい体のケア" class="mediaPC" height="170" src="/public/images/common/topics07.jpg" width="200"></a></li>
            <li><a class="imgHover" href="/ccwork/detail08"><img alt="自分に合った働き方を見つけよう" class="mediaPC" height="170" src="/public/images/common/topics06.jpg" width="200"></a></li>
            <li><a class="imgHover" href="/ccwork/detail10"><img alt="アイディアオフィスで疲れを吹き飛ばそう！" class="mediaPC" height="170" src="/public/images/common/topics01.jpg" width="200"></a></li>
            <li><a class="imgHover" href="/ccwork/detail07"><img alt="バイトを辞めるならここに注意！！" class="mediaPC" height="170" src="/public/images/common/topics04.jpg" width="200"></a></li>
            <li><a class="imgHover" href="/ccwork/detail11"><img alt="これは必須！覚えておこう コールセンターの専門用語！" class="mediaPC" height="170" src="/public/images/common/topics02.jpg" width="200"></a></li>
            <li><a class="imgHover" href="/ccwork/detail12"><img alt="日本全国コールセンターまとめ" class="mediaPC" height="170" src="/public/images/common/topics09.jpg" width="200"></a></li></ul>
            </section>
            <!-- / asideContentsPC -->
            <section class="mediaSP">
            <h2 class="headingA01 topics">トピックス</h2>
            <ul class="bnrListA01 alignC">
            <li><a class="imgHover" href="/about"><img alt="全国のコールセンター求人を一括検索　コールナビとは？" height="116" src="/public/images/common/bnr_about-callnavi_01.jpg" width="136"></a></li>
            <li><a class="imgHover" href="/ccwork/detail02"><img alt="コールセンターで働くメリットとは？" height="116" src="/public/images/common/bnr_melit_01.jpg" width="136"></a></li>
            <li><a class="imgHover" href="/ccwork/detail03"><img alt="全国調査 コールセンターの時給って！" height="116" src="/public/images/common/bnr_report_01.jpg" width="136"></a></li>
            <li><a class="imgHover" href="/ccwork/detail05"><img alt="もう面接は怖くない！コールセンター面接で失敗しないコツ！" height="116" src="/public/images/common/bnr_report_02.jpg" width="136"></a></li>
            <li><a class="imgHover" href="/ccwork/detail04"><img alt="コールセンターの雇用形態" height="116" src="/public/images/common/topics03.jpg" width="136"></a></li>
            <li><a class="imgHover" href="/ccwork/detail06"><img alt="ストレスに負けないコツ！" height="116" src="/public/images/common/topics08.jpg" width="136"></a></li>
            <li><a class="imgHover" href="/ccwork/detail09"><img alt="知っておきたい体のケア" height="116" src="/public/images/common/topics07.jpg" width="136"></a></li>
            <li><a class="imgHover" href="/ccwork/detail08"><img alt="自分に合った働き方を見つけよう" height="116" src="/public/images/common/topics06.jpg" width="136"></a></li>
            <li><a class="imgHover" href="/ccwork/detail10"><img alt="アイディアオフィスで疲れを吹き飛ばそう！" height="116" src="/public/images/common/topics01.jpg" width="136"></a></li>
            <li><a class="imgHover" href="/ccwork/detail07"><img alt="バイトを辞めるならここに注意！！" height="116" src="/public/images/common/topics04.jpg" width="136"></a></li>
            <li><a class="imgHover" href="/ccwork/detail11"><img alt="これは必須！覚えておこう コールセンターの専門用語！" height="116" src="/public/images/common/topics02.jpg" width="136"></a></li>
            <li><a class="imgHover" href="/ccwork/detail12"><img alt="日本全国コールセンターまとめ" height="116" src="/public/images/common/topics09.jpg" width="136"></a></li>
            </ul>
            </section>
            <!-- / asideContentsSP -->

            <?php echo $this->partial('/offer/pickup_right_columns'); ?>

            <?php echo $this->partial('/offer/pickup_logo_banners'); ?>
        </div><!-- / asideContents -->

        <div id="pageTop"><a href="#globalHeader" class="smoothScroll"><img src="/public/images/common/pagetop_img_01.gif" width="63" height="63" alt="pagetop"></a></div>
    </div><!-- / contentsInner -->
</div><!-- / contents -->

<?php echo $this->partial('/layouts/body/footer'); ?>
<?php endif; ?>