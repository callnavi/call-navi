
<aside id="snsArea">
<ul>
<li class="fb"><a href="http://www.facebook.com/share.php?u=http://callnavi.jp/" onclick="window.open(this.href, 'FBwindow', 'width=650, height=450, menubar=no, toolbar=no, scrollbars=yes'); return false;">シェア</a></li>
<li class="tw"><a href="http://twitter.com/share?url=http://callnavi.jp/&text=全国のコールセンター転職・求人一括検索！　CALL Navi（コールナビ）" onclick="window.open(this.href, 'Gwindow', 'width=650, height=450, menubar=no, toolbar=no, scrollbars=yes'); return false;">ツイート</a></li>
<li class="gp"><a href="https://plus.google.com/share?url=http://callnavi.jp/" onclick="window.open(this.href, 'Gwindow', 'width=650, height=450, menubar=no, toolbar=no, scrollbars=yes'); return false;">Google Plus</a></li>
<li class="hb"><a href="http://b.hatena.ne.jp/add?mode=confirm&url=http://callnavi.jp/&title=全国のコールセンター転職・求人一括検索！　CALL Navi（コールナビ）" onclick="window.open(this.href, 'Gwindow', 'width=650, height=450, menubar=no, toolbar=no, scrollbars=yes'); return false;">はてなブックマーク</a></li>
</ul>
</aside>

<footer id="globalFooter">
<div class="contentsInner">
<ul id="footerNav">
<li><a href="/">HOME</a></li>
<li><a href="/ccwork">コールセンターの仕事とは？</a></li>
<li><a href="/ccwork/detail12">全国コールセンターまとめ</a></li>
<li><a href="/about">コールナビとは？</a></li>
<li><a href="https://callnavi.jp/contact/input.html">お問い合わせ</a></li>
</ul>
<ul id="footerutilityNav">
<li><a href="/customer/price">求人情報掲載について</a></li>
<li class="alignC"><a href="/about/company">運営会社</a></li>
<li><a href="/about/terms">プライバシーポリシー</a></li>
</ul>
<div id="footerLogo"><img src="/public/images/common/logo_02.png" width="" height="" alt="CALL Navi コールナビ" /></div>
<div id="copyright"><small>Copyright &copy; 2015 CALL Navi Inc.All Rights Reserved.</small></div>
</div><!-- / contentsInner -->
</footer>

