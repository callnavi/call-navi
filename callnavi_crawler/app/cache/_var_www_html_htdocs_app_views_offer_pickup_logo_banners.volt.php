<?php if ($this->length($pickup_logo_banners) != 0) { ?>
    <section>
        <h2 class="headingA01 recommend marginOff">おすすめ求人企業</h2>
        <ul class="bnrListB01">
            <?php foreach ($pickup_logo_banners as $banner) { ?>
                <li> <a href=<?php if ($banner->detail_url !== '' && $banner->detail_content_type == 'none') { ?>"<?php echo $banner->detail_url; ?>" target="_blank"<?php } elseif ($banner->detail_content_type !== 'none') { ?>"/public/index/pickupPreview?offer_id=<?php echo $banner->id; ?>" target="_blank"<?php } ?>><img class=" impression_offer pickup_offer" offer_id="<?php echo $banner->id; ?>" offer_type="pickup" src="/public/images/pickup/logo/<?php echo $banner->id; ?>.png" width="200" height="74" alt=""/><span><?php echo $banner->title; ?></span></a></li>
                        <?php } ?>
        </ul>
    </section>
<?php } ?>