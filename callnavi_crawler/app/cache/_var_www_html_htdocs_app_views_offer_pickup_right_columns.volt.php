<?php if ($this->length($pickup_right_columns) != 0) { ?>
    <section class="pickupArea">
        <h2 class="headingA02 pickup">Pickup求人</h2>
        <?php foreach ($pickup_right_columns as $column) { ?>
            <article class="pickupB01 impression_offer pickup_offer" offer_id="<?php echo $column->id; ?>" offer_type="pickup">
                 <a href=<?php if ($column->detail_url !== '' && $column->detail_content_type == 'none') { ?>"<?php echo $column->detail_url; ?>" target="_blank"<?php } elseif ($column->detail_content_type !== 'none') { ?>"/public/index/pickupPreview?offer_id=<?php echo $column->id; ?>" target="_blank"<?php } ?>>
                    <p class="image"><img src="/public/images/pickup/thum/<?php echo $column->id; ?>.png" width="160" height="120" alt="<?php echo $column->title; ?>"></p>
                    <h3><?php echo $column->job_category; ?></h3>
                    <p class="company"><?php echo $column->company_name; ?></p>
                    <p class="summary"><?php echo $column->message; ?></p>
                    <?php $hired_as = explode(':', $column->hired_as) ; ?>
                    <?php echo $this->partial('/data/hired_as_preview'); ?> 
                    <table>
                        <tr>
                            <th>給与</th>
                            <td><div><?php echo $column->salary_term_display; ?> <?php echo $column->salary_value_min; ?><?php echo $column->salary_unit_min_display; ?>～<?php echo $column->salary_value_max; ?><?php echo $column->salary_unit_max_display; ?></div></td>
                        </tr>
                        <tr>
                            <th>勤務地</th>
                            <td><div><?php echo $column->workplace_prefecture; ?><?php echo $column->workplace_area; ?><?php echo $column->workplace_address; ?></div></td>
                        </tr>
                    </table>
                </a>
            </article><!-- / pickupB02 -->
        <?php } ?>    
    </section>
<?php } ?>