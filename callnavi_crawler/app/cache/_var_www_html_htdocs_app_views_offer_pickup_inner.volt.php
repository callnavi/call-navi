<?php if (isset($pickup->id)) { ?>
    <article class="pickupA01 impression_offer pickup_offer" offer_id="<?php echo $pickup->id; ?>" offer_type="pickup">
        <a href=<?php if ($pickup->detail_url !== '' && $pickup->detail_content_type == 'none') { ?>"<?php echo $pickup->detail_url; ?>" target="_blank"<?php } elseif ($pickup->detail_content_type !== 'none') { ?>"/public/index/pickupPreview?offer_id=<?php echo $pickup->id; ?>" target="_blank"<?php } ?>>
            <h3><?php echo $pickup->job_category; ?></h3>
            <div class="contentsInner">
                <p class="image"><img src="/public/images/pickup/thum/<?php echo $pickup->id; ?>.png" width="298" height="223" alt=""></p>
                <p class="company"><?php echo $pickup->company_name; ?></p>
                <p class="summary mediaPC"><?php echo $pickup->message; ?></p>
            </div><!-- / contentsInner -->
            <div class="contentsInner">
              <div class="details">
               <?php $hired_as = explode(':', $pickup->hired_as) ; ?>  
               <?php echo $this->partial('/data/hired_as_preview'); ?>  
                <table>
                    <tr>
                        <th>給　与</th>
                        <td><div><?php echo $pickup->salary_term_display; ?>　<?php echo $pickup->salary_value_min; ?><?php echo $pickup->salary_unit_min_display; ?>　〜　<?php echo $pickup->salary_value_max; ?><?php echo $pickup->salary_unit_max_display; ?></div></td>
                    </tr>
                    <tr>
                        <th>勤務地</th>
                        <td><div><?php echo $pickup->workplace_prefecture; ?><?php echo $pickup->workplace_area; ?><?php echo $pickup->workplace_address; ?></div></td>
                    </tr>
                </table>
                </div>
                 <?php $features = $pickup->features_array ; ?>
                  <ul class="tags"> 
                   <?php echo $this->partial('/data/features_php'); ?>
                  </ul>
            </div>
        </a>
    </article>
<?php } ?>
