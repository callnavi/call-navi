<?php

/**
 * Account Model
 *
 * @category    Model
 */
class Account extends ModelBase {

    use Mailable;

    public $id;
    public $created;
    public $updated;
    public $deleted;
    public $company_name;
    public $company_name_kana;
    public $postal_code;
    public $address;
    public $telephone;
    public $fax_code;
    public $company_url;
    public $company_section;
    public $company_position;
    public $representative;
    public $representative_kana;
    public $email;
    public $password;
    public $security_question_id;
    public $security_question_answer;
    public $creditcard_id;
    public $is_admin;
    public $card_status;
    private $_salt = 'qyWb6APzayrM4gZKd9DCJ14R3FEVlkLsyAaRO0T2mq0UFy9w3iT0I2SlWZtUqDN0';

    /**
     * {@inheritdoc}
     */
    public function initialize() {
        parent::initialize();
        $this->setSource('accounts');
    }

    /**
     * {@inheritdoc}
     */
    public function afterFetch() {
        // blank
    }

    public function validation() {
        
    }

    /**
     * {@inheritdoc}
     */
    protected function _setDefaultAttributes() {

        $this->id = null;
        $this->created = '0000-00-00 00:00:00';
        $this->updated = '0000-00-00 00:00:00';
        $this->deleted = 0;
        $this->company_name = '';
        $this->company_name_kana = '';
        $this->postal_code = '';
        $this->address = '';
        $this->telephone = '';
        $this->fax_code = '';
        $this->company_url = '';
        $this->company_section = '';
        $this->company_position = '';
        $this->representative = '';
        $this->representative_kana = '';
        $this->email = '';
        $this->password = '';
        $this->security_question_id = 0;
        $this->security_question_answer = '';
        $this->creditcard_id = '';
        $this->is_admin = 0;
        $this->card_status = 'before_approval';
    }

    
        /**
     * data配列で受け取ったアカウントデータをDBに保存
     * @param array $data アカウントデータ
     * @return object 新規作成されたアカウントデータオブジェクト
     */
    public function addAccount($data) {    
        //まず初期値を取得
        $this->_setDefaultAttributes();

        //@todo:サーバ側validationも、validation()メソッドを用いて行おう
        //@todo:送信するデータの値を、js側、html側でも変更しなければならない
        $this->company_name = $data['company_name'];
        $this->company_name_kana = $data['company_name_kana'];
        $this->postal_code = $data['postal_code'];
        $this->address = $data['address'];
        $this->telephone = $data['telephone'];
        $this->fax_code = $data['fax_code'];
        $this->company_url = $data['company_url'];
        $this->company_section = $data['company_section'];
        $this->company_position = $data['company_position'];
        $this->representative = $data['representative'];
        $this->representative_kana = $data['representative_kana'];
        $this->email = $data['email'];
        $this->password = $this->encryptPassword($data['password']);
        $this->security_question_id = $data['security_question_id'];
        $this->security_question_answer = $data['security_question_answer'];

        $this->create();

        return $this;
    }

    /**
     * 暗号化のための関数
     * 
     * @param type $password
     * @return encrypt_password
     */
    public function encryptPassword($password) {
        return crypt($password, $this->_salt);
    }
    
     /**
     * アカウントデータの更新
     * @param array $data 更新後のアカウントデータ
     * @param int $register_id 更新対象となるアカウントのID
     * @param string $password_changed 更新後のパスワード(暗号化済)
     * @return void
     */
    public function changeAccount($data, $register_id, $password_changed) {

        $Account = $this->findFirstById($register_id);
        $Account->company_name = $data['company_name'];
        $Account->company_name_kana = $data['company_name_kana'];
        $Account->postal_code = $data['postal_code'];
        $Account->address = $data['address'];
        $Account->telephone = $data['telephone'];
        $Account->fax_code = $data['fax_code'];
        $Account->company_url = $data['company_url'];
        $Account->company_section = $data['company_section'];
        $Account->company_position = $data['company_position'];
        $Account->representative = $data['representative'];
        $Account->representative_kana = $data['representative_kana'];
        $Account->email = $data['email'];
        $Account->password = $password_changed;

        $Account->security_question_id = $data['security_question_id'];
        $Account->security_question_answer = $data['security_question_answer'];

        $Account->updated = date('Y-m-d H:i:s');
        $Account->update();
    }
     /**
     * アカウントパスワードの更新
     * @param int $account_id パスワード更新対象となるアカウントのID
     * @param string $encrypted_password 更新後のパスワード(暗号化済)
     * @return bool true
     */
    public function updatePassword($account_id, $encrypted_password) {

        $Account = $this->findFirstById($account_id);
//        $Account->company_name = $data['company_name'];
//        $Account->company_name_kana = $data['company_name_kana'];
//        $Account->postal_code = $data['postal_code'];
//        $Account->address = $data['address'];
//        $Account->telephone = $data['telephone'];
//        $Account->fax_code = $data['fax_code'];
//        $Account->company_url = $data['company_url'];
//        $Account->company_section = $data['company_section'];
//        $Account->company_position = $data['company_position'];
//        $Account->representative = $data['representative'];
//        $Account->representative_kana = $data['representative_kana'];
//        $Account->email = $data['email'];
        $Account->password = $encrypted_password;

//        $Account->security_question_id = $data['security_question_id'];
//        $Account->security_question_answer = $data['security_question_answer'];

        $Account->updated = date('Y-m-d H:i:s');
        $Account->update();

        return true;
    }
    
     /**
     * アカウントの論理削除(deletedカラムを1にする)
     * @param int $register_id 論理削除対象となるアカウントのID
     * @return void
     */
    public function deleteAccount($register_id) {

        $Account = $this->findFirstById($register_id);
        $pickup_offer = new PickupOffer;
        $conditions = "deleted = '0' AND account_id = :register_id:";
        $paramerters = array(
            "register_id" => $register_id
        );
        $offers = $pickup_offer->find(array(
            "conditions" => $conditions,
            "bind" => $paramerters
        ));
        foreach ($offers as $offer) {
          $pickup_offer->completePickupOffer($offer);
        }
        $Account->deleted = 1;
        $Account->update();
    }
     /**
     * ログインフォームにおいて、ログインできるか否かを判定
     * @param string $email メールアドレス
     * @param string $password パスワード(暗号化済)
     * @return object, bool  メールアドレスとパスワードの組み合わせに一致するアカウントが見つかった場合にはそのアカウントオブジェクトを、
       * 　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　見つからなかった場合にはfalseを返す
     */
    public function login($email, $password) {
        $result = $this->findFirst("email = '$email' AND password = '$password' AND deleted = 0");
        if (isset($result->email) && isset($result->password)) {
            return $result;
        } else {
            return false;
        }
    }
     /**
     * メールアドレスに基づきアカウントを検索
     * @param string $email メールアドレス
     * @return object, bool  メールアドレスに基づくアカウントが見つかった場合にはその最初のレコードを、
     *                                                   見つからなかった場合にはfalseを返す　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　
     */
    public function findFirstByMail($email) {
        $result = $this->findFirst("email = '$email' AND deleted = 0");
        if (isset($result->email)) {
            return $result;
        } else {
            return false;
        }
    }

    
     /**
     * IDに基づきアカウントを検索
     * @param int $id  アカウントID
     * @return object  アカウントIDに基づき検索されたアカウントレコード　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　
     */
    public static function findAccountById($id) {

        $account = new Account();

//        $output = [];
//
//        $output["id"] = $account->id;
//        $output["company_name"] = $account->company_name;
//        $output["company_name_kana"] = $account->company_name_kana;
//        $output["postal_code"] = $account->postal_code;
//        $output["address"] = $account->address;
//        $output["telephone"] = $account->telephone;
//        $output["fax_code"] = $account->fax_code;
//        $output["postal_code"] = $account->postal_code;
//        $output["company_url"] = $account->company_url;
//        $output["company_section"] = $account->company_section;
//        $output["company_position"] = $account->company_position;
//        $output["representative"] = $account->representative;
//        $output["representative_kana"] = $account->representative_kana;
//        $output["email"] = $account->email;

        return $account->findFirstById($id);
    }

     /**
     * 論理削除されずに存在する全アカウントを返す
     * 
     * @return array 論理削除されていない全アカウントレコードの配列　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　
     */
    public static function getAllAccounts() {
        return Account::find(array(
                    "conditions" => "deleted = '0'",
                        //"order" => "id",
        ));
    }
     
     /**
     * 論理削除されておらず、クレジットカード登録済みであり、バッチによるカード承認処理が未了である全アカウントを返す
     * 
     * @return array 論理削除されておらず、クレジットカード登録済みであり、バッチによるカード承認処理が未了である全アカウントレコードの配列　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　
     */
    public static function getAccountsForApproval() {
        return Account:: find(array(
               "conditions" => "deleted = :deleted: AND creditcard_id != :creditcard_id: AND card_status = :card_status:",
               "order" => "id",
               "bind" =>array(
                                "deleted" => "0",
                                "creditcard_id" => "",
                                "card_status" =>"before_approval"
                            )
                ));
           }
     
     /**
     * 全アカウントを返す。$is_chargeがtrueかfalseかにより、論理削除されたものを含むか否かを判定
     * @parm bool $is_charge 
     * @return array 全アカウントレコードの配列 $is_chargeがtrueかfalseかにより、論理削除されたものを含むか否かが分かれる　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　
     */
    public static function getAllAccountsForAdmin($is_charge = false) {

        $account = new Account();
        if($is_charge == true) {
            $accounts = $account->find(array(
                "order" => "id",
            ));
        } else {
            $accounts = $account->find(array(
                "conditions" => "deleted = '0'",
                "order" => "id",
            ));
        }

        /* $output = [];

          foreach ($offers as $offer) {

          $each_output = [];

          $each_output["id"] = $offer->id;
          $each_output["company_name"] = $offer->company_name;
          $each_output["representative"] = $offer->representative;
          $each_output["email"] = $offer->email;
          $each_output["telephone"] = $offer->telephone;

          array_push($output, $each_output);
          } */

        return $accounts;
    }
     
     /**
     * 検索結果に一致するアカウントを返す。$is_chargeがtrueかfalseかにより、論理削除されたものを含むか否かを判定
     * @parms string アカウントを検索するために入力される文字列
     * @parm bool $is_charge
     * @return array 検索結果に一致する全アカウントレコードの配列 $is_chargeがtrueかfalseかにより、論理削除されたものを含むか否かが分かれる　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　
     */
    public function getSearchResults($term, $is_charge = false) {

        if($is_charge == true) {
            //@todo sql injection対策として、boundしてる
            $conditions = "company_name LIKE '%" . $term . "%'
                OR company_name_kana LIKE '%" . $term . "%'
                OR representative LIKE '%" . $term . "%'
                OR representative_kana LIKE '%" . $term . "%'";
        } else {
            //@todo sql injection対策として、boundしてる
            $conditions = "deleted = '0' AND (
                company_name LIKE '%" . $term . "%'
                OR company_name_kana LIKE '%" . $term . "%'
                OR representative LIKE '%" . $term . "%'
                OR representative_kana LIKE '%" . $term . "%'
                )";
        }
        

        $accounts = $this->find(array(
            "conditions" => $conditions,
            "order" => "id",
        ));

        return $accounts;
    }
     
     /**
     * IDに基づき、論理されていないアカウントを返す
     * @parm int $id アカウントID 
     * @return object　 IDに基づき検索された、論理削除されていないアカウント　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　
     */
    public function findAccountForWebpay($id) {

        $conditions = "Account.id = :id: AND deleted = '0'";
        $parameters = array("id" => $id);

        return $this->findFirst(array(
                    $conditions,
                    "bind" => $parameters
        ));
    }
     
     /**
     * パスワード再設定を促すメールを送信
     * @parm array $data メール送信に必要な情報が格納された配列
     * @return function メール送信のための関数　　　　　　　　　　　　　　　　　　　　　　　　　　　　　
     */
    public function sendResetPasswordMail($data) {

        $subject = '【コールナビ】パスワード再設定用のご案内';
        $path = 'admin/resetPassword';
        $account_id = $data['account_id'];
        $account = $this->findFirstById($account_id);
        $token = $data['token'];
        $link = 'http://' . $_SERVER['SERVER_NAME'] . '/login/resetPasswordStart?account_id=' . $account_id . '&token=' . $token;
        $params = [
            'account' => $account,
            'link' => $link
        ];
        $mail = $data['email'];
        return $this->sendMail($mail, $subject, $path, $params);
    }

     /**
     * アカウント登録完了メールを送信
     * @parm object $account メール送信の対象となるアカウント
     * @return function メール送信のための関数　　　　　　　　　　　　　　　　　　　　　　　　　　　　　
     */
    public function sendThanksMail($account) {
        
        $subject = '【コールナビ】アカウント登録完了のお知らせ';
        $path = 'admin/account_thanks';
        $link = 'http://' . $_SERVER['SERVER_NAME'] . '/login/index';
        $params = [
            'link' => $link,
            'account' => $account,
        ];
        return $this->sendMail($account->email, $subject, $path, $params);
    }
    
     /**
     * バッチによるクレジットカード承認処理失敗を知らせるメールを送信
     * @parm object $account メール送信の対象となるアカウント
     * @return function メール送信のための関数　　　　　　　　　　　　　　　　　　　　　　　　　　　　　
     */
    public function sendInvalidMail($account) {
        
        $subject = '【CALL Navi】クレジットカードを再度ご登録下さい';
        $path = 'admin/card_invalid';
        $link = 'http://staging.callnavi.jp/credit/input';
        $params = [
            'link' => $link,
            'account' => $account,
        ];
        return $this->sendMail($account->email, $subject, $path, $params);
    }

     /**
     * アカウント登録完了メールを管理者へ送信
     * @parm string $mail 登録されたアカウントのメールアドレス
     * @return function メール送信のための関数　　　　　　　　　　　　　　　　　　　　　　　　　　　　　
     */
    public function sendThanksAdminMail($mail) {
        $subject = '【コールナビ】新規アカウント登録がありました';
        $path = 'admin/account_thanks_admin';
        $params = [
            'mail' => $mail
        ];
        $mail_to = $this->admin_email;
        return $this->sendMail($mail_to, $subject, $path, $params);
    }
    
    

}
