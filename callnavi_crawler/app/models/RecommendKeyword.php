<?php

/**
 * RecommendKeyword model
 *
 * @category    Model
 */
class RecommendKeyword extends ModelBase {

    /**
     * @var int
     * @Primary
     * @Identity
     * @Column(type="integer", nullable=false)
     */
    public $id;

    /**
     * @var string(32)
     * @Column(type="string", nullable=false)
     */
    public $keyword;

    /**
     * @var int(11)
     * @Column(type="integer", nullable=false)
     */
    public $count;

    /**
     * @var datetime
     * @Column(type="datetime", nullable=false)
     */
    public $created;

    /**
     * @var datetime
     * @Column(type="datetime", nullable=false)
     */
    public $updated;

    /**
     * @var boolean
     * @Column(type="tinyint(1)", nullable=false)
     */
    public $deleted;

    /**
     * {@inheritdoc}
     */
    public function initialize() {
        parent::initialize();
        $this->setSource('recommend_keywords');
    }

    /**
     * {@inheritdoc}
     */
    public function afterFetch() {
        // blank
    }

    /**
     * {@inheritdoc}
     */
    protected function _setDefaultAttributes() {
        $this->id = null;
        $this->keyword = '';
        $this->count = 0;
        $this->created = '0000-00-00 00:00:00';
        $this->updated = '0000-00-00 00:00:00';
        $this->deleted = 0;
    }

    public function getKeywords($limit = 17) {

        $keywords = $this->find(array(
            "conditions" => "deleted = 0",
            "limit" => $limit,
            "order" => "count DESC",
        ));

        $newKeywords = array();
        foreach ($keywords as $order => $keyword) {
            $level = 5 - floor($order / ($limit / 5));

            $keyword->level = $level;
            $newKeywords[] = (object) $keyword;
        }

        shuffle($newKeywords);

        return $newKeywords;
    }

    public function updateKeywords($query) {
        $query = $this->sanitize($query);

        $conditions = 'keyword = "' . $query . '"';
        $keywords = $this->find(array(
            "conditions" => $conditions,
        ));

        if ($keywords->count()) {
            $target = $keywords->getFirst();
            $target->count = ++$target->count;
            $this->updated = date('Y-m-d H:i:s');
            $target->save();
        } else {
            $this->addKeyword($query);
        }
    }

    public function addKeyword($keyword) {
        $this->_setDefaultAttributes();
        $this->keyword = $keyword;
        $this->updated = date('Y-m-d H:i:s');
        $this->save();

        return $this;
    }

}
