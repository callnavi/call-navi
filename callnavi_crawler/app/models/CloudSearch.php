<?php

use Aws\CloudSearch\CloudSearchClient;

/**
 * SearchedQuery model
 *
 * @category    Model
 */
class CloudSearch extends ModelBase {

    use TimeFormatable;

    protected $_config;
    protected $_client;

    public function initialize() {
        $this->setSource('crawls');

        require __DIR__ . '/../vendor/autoload.php';

        $this->_config = [
            'domain' => 'callnavi-db',
            'key' => 'AKIAIXS4I4MLZSX7DJHQ',
            'secret' => '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',
            'region' => 'ap-northeast-1',
        ];
        //User Name cloudsearch-user

        $configClient = CloudSearchClient::factory($this->_config);

        $this->_configClient = $configClient;

        $this->_client = $configClient->getDomainClient($this->_config['domain'], [
            'key' => $this->_config['key'],
            'secret' => $this->_config['secret'],
        ]);
    }
     /**
     * ユーザページの広告検索フォームにおける検索結果を取得
     * @parm array data フォームに入力された、勤務地及びキーワードを格納した配列
     * @parm int $page どの検索結果ページに表示される検索結果かを指定(1ページ目が0)
     * @parm int $size 検索結果ページ1ページに表示される広告件数を指定
     * @return array 該当ページ検索結果の配列、及び全検索件数　　　　　　　　　　　　　　　　　　　　　　　
     */
    public function search($data, $page = 0, $size = 20) {

        $query = '';

        /*
          if (isset($data['area']) and ! empty($data['area'])) {     //検索条件の追加が働くためには削除必要?
          $query .= "area_name:'" . $data['area'] . "' ";
          } */

        if (isset($data['keyword']) and ! empty($data['keyword'])) {
            $query .= "(or ";
            foreach (['title', 'workplace', 'company', 'desc'] as $target) {
                $query .= $target . ":'" . $data['keyword'] . "' ";
            }
            $query .= ")";
        }

        if (isset($data['area']) and ! empty($data['area'])) {
            $allareas = explode("or", $data['area']);
            $query .= "(or ";
            foreach (['title', 'workplace', 'company', 'desc'] as $target) {
                foreach ($allareas as $eacharea) {
                    $query .= $target . ":'" . $eacharea . "' ";
                }
            }
            $query .= ")";
        }

        if (isset($data['site_id']) and ! empty($data['site_id'])) {
            $query .= "site_id:'" . $data['site_id'] . "' ";
        }

        if ($query != '') {
            $query = '(and ' . $query . ')';
            $conditions = [
                'query' => $query,
                'queryParser' => 'structured',
            ];
        } else {
            $conditions = [
                'query' => '*:*',
                'queryParser' => 'lucene',
            ];
        }

        $conditions['sort'] = '_rand desc';
        $conditions['size'] = $size;
        $conditions['start'] = $page * $size;

        $results = $this->_client->search($conditions);


        $count = $results['hits']['found'];

        $resultObjects = [];
        foreach ($results['hits']['hit'] as $hit) {
            $eachResultObject = [];
            foreach ($hit['fields'] as $key => $valueArray) {
                $eachResultObject[$key] = $valueArray[0];
            }
            $resultObjects[] = $this->getResultInfo((object) $eachResultObject);
        }

        return array($resultObjects, $count);
    }
      
     /**
     * 検索結果を表示用に加工
     * @parm array $result 加工前の検索結果
     * @return array 加工後の検索結果　　　　　　　　　　　　　　　　　　　　　　
     */
    public function getResultInfo($result) {
        if(isset($result->labels)){
            $result->labels = explode(':', $result->labels);
        }else{
            $result->labels = [];
        }

        foreach($result->labels as $key => $value){
            if($key >= 5){
                unset($result->labels[$key]);
            }
        }
        $result->update_before = $this->convertDateTimeToMinuteBefore(intval($result->updatetime));

        foreach (['title', 'company', 'pic_url', 'desc', 'workplace', 'salary'] as $target) {
            $result->{$target} = mb_substr(strip_tags($result->{$target}), 0, 150, 'UTF8');
            $result->{$target} = str_replace(["\r\n", "\r", "\n", " ", "　"], '', $result->{$target});
        }
        $result->site_name = $this->_getSiteName($result->site_id);
        if (!isset($result->pic_url) or $result->pic_url == '') {
            $result->pic_url = '/images/common/img_no-image_' . sprintf("%02d", rand(1, 14)) . '.png';
        }

        return $result;
    }
      /**
     * site_idから広告媒体名を取得
     * @parm int $site_id 広告媒体を表すID
     * @return string 広告媒体名　　　　　　　　　　　　　　　　　　　　　
     */
    protected function _getSiteName($site_id) {
        if ($site_id == 1) {
            return 'マイナビバイト';
        } elseif ($site_id == 2) {
            return 'バイトル';
        } elseif ($site_id == 3) {
            return 'デューダ';
        } elseif ($site_id == 4) {
            return 'Eアイデム';
        } elseif ($site_id == 5) {
            return 'フロムエー';
        } elseif ($site_id == 6) {
            return 'ジョブセンス';
        } elseif ($site_id == 7) {
            return 'マイナビ転職';
            //20150320ここまで実装
        } elseif ($site_id == 8) {
            return 'an';
        } elseif ($site_id == 9) {
            return 'リクナビNEXT';
        } elseif ($site_id == 10) {
            return 'タウンワーク';
        } elseif ($site_id == 11) {
           return '無料広告';
        }
        
    }
     /**
     * 該当媒体についてのcloudsearch上の更新処理
     * @parm int $site_id 広告媒体を表す
     * @return void　　　　　　　　　　　　　　　　　　　　
     */
    public function refresh($siteId) {

        $this->_deletePreviousOffers($siteId);
        sleep(3);
        $this->_addOffers($siteId);
    }
     
    /**
     * 該当媒体について、offersテーブルにおいて論理削除されているものをcloudsearch上から削除
     * @parm int $site_id 広告媒体を表すID
     * @return void　　　　　　　　　　　　　　　　　　　　
     */
    public function _deletePreviousOffers($siteId) {
        //最新のクロール総数をカウントしておいて、それだけ削除する必要がある
        set_time_limit(0);

        $documents = [];
        $Offer = new Offer();
        $offers = $Offer->find(array("site_id = '$siteId' AND deleted = 1"));

        foreach ($offers as $offer) {
            $id = (empty($offer->cloud_search_id)) ? $offer->id : $offer->cloud_search_id;
            $documents[] = [
                    'type' => 'delete',
                    'id' => $id
            ];
        }

        if (!empty($documents) && count($documents) > 0) {
            $results = $this->_client->uploadDocuments([
                    'documents' => json_encode($documents),
                    'contentType' => 'application/json',
            ]);
            Phalcon\DI::getDefault()->getLogger()->info("Deleted cloudSearch");
        }
    }
       /**
     * cloudsearch_idを指定してcloudsearch上から削除(誤ったデータ削除用)
     * @parm int $cloudsearch_id
     * @return void　　　　　　　　　　　　　　　　　　　　
     */
    public function deletetById($cloudsearch_id) {
        $documents = [];
        
            
            $documents[0] = [
                    'type' => 'delete',
                    'id' => $cloudsearch_id
            ];
        
        if (!empty($documents) && count($documents) > 0) {
            $results = $this->_client->uploadDocuments([
                    'documents' => json_encode($documents),
                    'contentType' => 'application/json',
            ]);
        }
    }
    
     /**
     * 該当媒体の広告について、取得した求人をクラウドサーチに登録
     * @parm int $site_id 広告媒体を表すID
     * @return void　　　　　　　　　　　　　　　　　　　　
     */
    public function _addOffers($siteId) {

        $Offer = new Offer();
        $totalCnt = $Offer->count(array("site_id = '$siteId' AND deleted = 0"));

        Phalcon\DI::getDefault()->getLogger()->info("target total count : $totalCnt");

        $limit = Phalcon\DI::getDefault()->getCrawlerConfig()->limit;
        if (empty($limit) || ! is_numeric($limit)) {
            $limit = 1000;
        }

        for ($offset=0; $offset < $totalCnt; $offset = $offset + $limit) {
            Phalcon\DI::getDefault()->getLogger()->info("offset : $offset // limit : $limit");
            $offers = $Offer->find(array("site_id = '$siteId' AND deleted = 0", "order"=>"id", "limit"=>$limit, "offset" => $offset));

            $documents = [];
            foreach ($offers as $offer) {
                $documents[] = $this->_makeDocument($offer);
            }
          
            if (!empty($documents)) {
                Phalcon\DI::getDefault()->getLogger()->info("upload to cloudSearch");
                $results = $this->_client->uploadDocuments([
                        'documents' => json_encode($documents),
                        'contentType' => 'application/json',
                ]);
            } else {
                Phalcon\DI::getDefault()->getLogger()->info("upload data is empty");
            }
        }

        Phalcon\DI::getDefault()->getLogger()->info("upload finished");

        return 1;
    }

    /**
     *
     * @param ActiveRecord $offer
     * @return array
     */
    public function _makeDocument($offer) {

        return [
            'type' => 'add',
            'id' => $offer->cloud_search_id,
            'fields' => [
                'title' => $offer->title,
                'url' => $offer->url,
                'company' => $offer->company,
                'pic_url' => $offer->pic_url,
                'desc' => $offer->desc,
                'salary' => $offer->salary,
                'workplace' => $offer->workplace,
                'labels' => $offer->labels,
				'employment_type' => $offer->employment_type,
                'updatetime' => strtotime($offer->created),
                'site_id' => $offer->site_id,
                'unique_id' => $offer->unique_id,
                'hired_as' => $offer->hired_as
            ]
        ];
    }

}
