<?php

/**
 * ImpressionLog Model
 *
 * @category    Model
 */
class ImpressionLog extends ModelBase {

    public $id;
    public $created;
    public $offer_type;
    public $offer_id;
    public $listing_keyword_id;//必要そうなので追加

    /**
     * {@inheritdoc}
     */
    public function initialize() {
        parent::initialize();
        $this->setSource('impression_logs');
    }

    /**
     * {@inheritdoc}
     */
    public function afterFetch() {
        // blank
    }

    public function validation() {
        
    }

    /**
     * {@inheritdoc}
     */
    protected function _setDefaultAttributes() {
        $this->id = null;
        $this->created = '0000-00-00 00:00:00';
        $this->offer_type = '';
        $this->offer_id = 0;
        $this->listing_keyword_id = 0;
    }
     
      /**
        * 表示ログのカウント
        * @param object $offer 表示された広告レコード
        * @param string $type 広告の種類
        * @param array $scope 対象期間
        * @param bool リスティング広告でキーワードに基づくカウントである場合はtrue,その他の場合はfalse
        * @return int 表示ログの数
      */
    public function countImpressionLog($offer, $type, $scope, $isKeywords) {
       
       $start = $scope['start'];
       $end = $scope['end'];
       
       if ($isKeywords) {
           $conditions = "ImpressionLog.listing_keyword_id = :id: AND ImpressionLog.offer_type = :type: AND ImpressionLog.created BETWEEN :start: AND :end:";
       } else {
           $conditions = "ImpressionLog.offer_id = :id: AND ImpressionLog.offer_type = :type: AND ImpressionLog.created BETWEEN :start: AND :end:";
       }
       
       $parameters = array(
           'id' => $offer->id,
           'type' => $type,
           'start' => $start,
           'end' => $end
       );       
       $impressionLog = $this->count(array(
           $conditions,
           "bind" => $parameters
       ));
       
       return $impressionLog;

   }
     /**
        * 特定種類の全広告表示ログのカウント
        * @param string $type 広告の種類
        * @param array $scope 対象期間
        * @return int 表示ログの数
      */
   public function countAllImpressionLog($type, $scope) {
       
       $start = $scope['start'];
       $end = $scope['end'];
       
       
           $conditions = "ImpressionLog.offer_type = :type: AND ImpressionLog.created BETWEEN :start: AND :end:";
      
       
       $parameters = array(
     
           'type' => $type,
           'start' => $start,
           'end' => $end
       );       
       $impressionLog = $this->count(array(
           $conditions,
           "bind" => $parameters
       ));
       
       return $impressionLog;

   }
}
