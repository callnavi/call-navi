<?php

/**
 * PickupOfferDetail Model
 *
 * @category    Model
 */
class PickupOfferDetail extends ModelBase {

    public $id;
    public $created;
    public $updated;
    public $deleted;
    public $offer_id;
    public $html;
    public $job_description;
    public $requirement;
    public $office_hours;
    public $holidays;
    public $welfares;
    public $procedures;
    public $others;
    public $founded;
    public $president;
    public $capital;
    public $employees;
    public $business_description;
    public $company_url;
    public $detail_url;

    /**
     * {@inheritdoc}
     */
    public function initialize() {
        parent::initialize();
        $this->setSource('pickup_offer_details');
    }

    /**
     * {@inheritdoc}
     */
    public function afterFetch() {
        // blank
    }

    public function validation() {
        
    }

    /**
     * {@inheritdoc}
     */
    protected function _setDefaultAttributes() {

        $this->id = null;
        $this->created = '0000-00-00 00:00:00';
        $this->updated = '0000-00-00 00:00:00';
        $this->deleted = 0;
        $this->offer_id = '';
        $this->html = '';
        $this->job_description = '';
        $this->requirement = '';
        $this->office_hours = '';
        $this->holidays = '';
        $this->welfares = '';
        $this->procedures = '';
        $this->others = '';
        $this->founded = '';
        $this->president = '';
        $this->capital = '';
        $this->employees = '';
        $this->business_description = '';
        $this->company_url = '';
        $this->detail_url = '';
    }
  
    /**
      * 新規ピックアップ広告詳細画面フォーム記入分をDB(pickup_offerdtailsテーブル)に追加 
      * @param aray $data 新規ピックアップ広告詳細画面フォーム記入分データを格納した配列
      * return object 新規追加されたピックアップ広告詳細画面フォーム記入分レコード
      */
   public function addPickupOfferForm($data) {

        $this->_setDefaultAttributes();
        
        $this->job_description = $data['job_description'];
        $this->requirement = $data['requirement'];
        $this->office_hours = $data['office_hours'];
        $this->holidays = $data['holidays'];
        $this->welfares = $data['welfares'];
        $this->procedures = $data['procedures'];
        $this->others = $data['others'];
        $this->founded = $data['founded'];
        $this->president = $data['president'];
        $this->capital = $data['capital'];
        $this->employees = $data['employees'];
        $this->business_description = $data['business_description'];
        $this->company_url = $data['company_url'];
        $this->offer_id = $data['offer_id'];
       
        $this->detail_url = $data['detail_url'];
        
       
        


        $this->create();   



        return $this;


    }
    
     /**
      * 既に存在するピックアップ広告詳細画面フォーム記入分レコードを更新
      * @param array $data 更新後ピックアップ広告データを格納した配列
      * @param string $status 更新後のstatusカラム
      * return object 更新されたピックアップ広告レコード
      */
    public function updatePickupOfferForm($data) {
        $detail = $this->findPickupOfferDetailById($data['offer_id']);
    if (isset($detail->id)) {
    
        $detail->html = ''; 
        $detail->job_description = $data['job_description'];
        $detail->requirement = $data['requirement'];
        $detail->office_hours = $data['office_hours'];
        $detail->holidays = $data['holidays'];
        $detail->welfares = $data['welfares'];
        $detail->procedures = $data['procedures'];
        $detail->others = $data['others'];
        $detail->founded = $data['founded'];
        $detail->president = $data['president'];
        $detail->capital = $data['capital'];
        $detail->employees = $data['employees'];
        $detail->business_description = $data['business_description'];
        $detail->company_url = $data['company_url'];
       
        $detail->detail_url = $data['detail_url'];

        $detail->updateColumn();

        return $detail;
  
  }
    else { 
        return $this->addPickupOfferForm($data);
    }
     
}    

   
     /**
      * 新規ピックアップ広告詳細画面html記入分をDB(pickup_offerdtailsテーブル)に追加 
      * @paramint $offer_id ピックアップ広告のID
      * @param string $html 記入されたhtml
      * return object 新規追加されたピックアップ広告詳細画面html記入分レコード
      */
    public function addPickupOfferHtml($offer_id, $html) {
    
    $this->_setDefaultAttributes();

    $this->offer_id = $offer_id;
    $this->html = $html;
    $this->save();

    return $this;

    }
     
     /**
      * 既に存在するピックアップ広告詳細画面html記入分レコードを更新
      * @param int $offer_id ピックアップ広告のID
      * @param string $html 記入されたhtml
      * return object 更新されたピックアップ広告詳細画面html記入分レコード
      */
    public function updatePickupOfferHtml($offer_id, $html) {
    $detail = $this->findPickupOfferDetailById($offer_id);    
    if (isset($detail->id)) {    
     $detail->job_description = '';
     $detail->requirement = '';
     $detail->office_hours = '';
     $detail->holidays = '';
     $detail->welfares = '';
     $detail->procedures = '';
     $detail->others = '';
     $detail->founded = '';
     $detail->president = '';
     $detail->capital = '';
     $detail->employees = '';
     $detail->business_description = '';
     $detail->company_url = '';
     $detail->detail_url = '';
    $detail->html = $html;
    $detail->save();

    return $detail;
    
   }

   else { 
        return $this->addPickupOfferHtml($offer_id, $html);

   }
}

     /**
      * ピックアップ広告詳細画面表示(pickup_offer_detailsテーブル)のレコードを空("")にする
      * @param int $offer_id ピックアップ広告のID
      * return object 中身を空("")にされたピックアップ広告詳細画面レコード
      */
    public function vacatePickupOfferDetail($offer_id) {

    $detail = $this->findPickupOfferDetailById($offer_id);    
    if (isset($detail->id)) {    
     $detail->job_description = '';
     $detail->requirement = '';
     $detail->office_hours = '';
     $detail->holidays = '';
     $detail->welfares = '';
     $detail->procedures = '';
     $detail->others = '';
     $detail->founded = '';
     $detail->president = '';
     $detail->capital = '';
     $detail->employees = '';
     $detail->business_description = '';
     $detail->company_url = '';
     $detail->detail_url = '';
     $detail->html = '';
     $detail->updateColumn();

    return $detail;
    }
  } 
     
     /**
      * ピックアップ広告詳細画面レコードの検索
      * @param int $offer_id ピックアップ広告のID
      * return object ピックアップ広告詳細画面レコード
      */
    public function findPickupOfferDetailById($offer_id) {
        
        $conditions = "offer_id = :offer_id: AND deleted = '0'";
        $parameters = array("offer_id" => $offer_id);
        
        return $this->findFirst(array(
            "conditions" => $conditions,
            "bind" => $parameters
        ));
        
    }
    
    /*
    public function savePickupOfferHtml($offer_id, $html) {
    
    $this->_setDefaultAttributes();
    $this->offer_id = $offer_id;
    $this->html = $html;
    $this->create();

    return $this;

    }
     */
}
