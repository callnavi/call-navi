<?php

use Phalcon\Mvc\Model;
use Phalcon\Mvc\Model\Behavior\Timestampable;
use Phalcon\Mvc\Model\Behavior\SoftDelete;

class ModelBase extends Model {

    /**
     * server定数
     */
    public $server;
    public $admin_email;
    
    /**
     * Model自体を初期化
     */
    public function initialize() {
        $this->addBehavior(new Timestampable(array(
            'beforeCreate' => array(
                'field' => 'created',
                'format' => 'Y-m-d H:i:s'
            )
        )));

        $this->addBehavior(new Timestampable(array(
            'beforeUpdate' => array(
                'field' => 'updated',
                'format' => 'Y-m-d H:i:s'
            )
        )));

        $this->addBehavior(new SoftDelete(
                array(
            'field' => 'deleted',
            'value' => 1
                )
        ));

        $this->setup(array(
            'notNullValidations' => false
        ));

      if ($_SERVER['SERVER_NAME'] == 'callnavi.jp') {
	    	$this->admin_email = "info@callnavi.jp";
	    } elseif ($_SERVER['SERVER_NAME'] == 'staging.callnavi.jp') {
	    	$this->admin_email = "info@callnavi.jp,m-jigyo@metaphase.co.jp";
	    } else {
	    	$this->admin_email = "m-jigyo@metaphase.co.jp";
	    }
    }

    /**
     * Modelインスタンスを初期化
     */
    public function onConstruct($id = null) {
        $this->_setDefaultAttributes();
        if (!is_null($id)) {
            $obj = new self();
            $this->getFirstById($id);
        }
    }

    /**
     * save用に、あらかじめデフォルトの値をセットしておく
     */
    protected function _setDefaultAttributes() {
        /*
         * Baseにロジックは書かない
         */
    }
     /**
      * サニタイズ処理
      * @param array, string $data サニタイズ対象の文字列または配列
      * @param string $account_id 対象となるアカウントのID 全アカウントについて検索したい場合、求める検索条件に合わせてallやadminを代入
      * @return array , string サニタイズ後の結果
      */
    public function sanitize($data) {
        //配列ならば、中身それぞれにサニタイズ処理
        if (is_array($data)) {
            $infos = array();
            foreach ($data as $key => $each) {
                $infos[$key] = $this->sanitize($each);
            }
            return $infos;
        } else {
            if ($data !== NULL) {
                //特殊文字をHTMLエンティティに変換
                $data1 = htmlspecialchars($data, ENT_QUOTES);
                //改行コードを改行タグに
                $data2 = str_replace('&lt;br /&gt;', '<br />', $data1);
                //全角スペースの処理
                $data3 = str_replace('  ', '&ensp;&ensp;', $data2);
//                $data3 = mysql_real_escape_string($data2);
                return $data3;
            } else {
                return NULL;
            }
        }
    }
      /**
      * htmlエンティティを特殊文字に戻す
      * 
      * @return void
      */
    public function htmlDecode() {

           foreach ($this as $key => $value) {
                if (is_string($value) && substr($key, 0, 1) != '_') {
                    $this->$key = htmlspecialchars_decode($value, ENT_QUOTES);
                }
            }

        }
      /**
      * 改行を一つにまとめる
      * 
      * @return void
      */
    public function oneBr() {
        
        //改行を一つにまとめる
           foreach ($this as $key => $value) {
                if (is_string($value) && substr($key, 0, 1) != '_') {
                    $this->$key = preg_replace("/(\x0D\x0A|[\x0D\x0A]){2,}/","$1",$value);
                }
            }

        }
     /**
      * IDに基づき最初のレコードを検索
      * @param int ID
      * @return object 最初のレコード
      */
    public function findFirstById($id) {
        $result = $this->findFirst($id);
        if (isset($result->id)) {
            return $result;
        } else {
            return false;
        }
    }

      /**
      * DBへの新規レコード生成処理
      * @param bool $br 改行処理をするか否か
      * @param bool $sanitize サニタイズ処理をするか否か
      * @return object 生成されたレコード
      */
    public function create($br = true, $sanitize = true) {
        //primary keyがセットされていないことを確認
        if (!is_null($this->id)) {
            return false;
        }
        //改行を一つにまとめる
        $this->oneBr();
        //改行の処理
        if ($br) {
            foreach ($this as $key => $value) {
                if (is_string($value) && substr($key, 0, 1) != '_') {
                    $this->$key = nl2br($value);
                }
            }
        }
        
        //sanitize処理            
        if ($sanitize) {
            foreach ($this as $key => $value) {
                if (is_string($value) && substr($key, 0, 1) != '_') {
                    $this->$key = $this->sanitize($value);
                }
            }
        }

        $this->save();
        return $this;
    }
      
      /**
      * レコード更新処理
      * @param bool $br 改行処理をするか否か
      * @param bool $sanitize サニタイズ処理をするか否か
      * @return object 更新されたレコード
      */
    public function updateColumn($br = true, $sanitize = true) {//createでは更新ができないため追加。
        
        //改行を一つにまとめる
        $this->oneBr();
        //htmlデコード
        $this->htmlDecode();
        //改行の処理
        if ($br) {
            foreach ($this as $key => $value) {
                if (is_string($value) && substr($key, 0, 1) != '_') {
                    $this->$key = nl2br($value);
                }
            }
        }

        //sanitize処理            
        if ($sanitize) {
            foreach ($this as $key => $value) {
                if (is_string($value) && substr($key, 0, 1) != '_') {
                    $this->$key = $this->sanitize($value);
                }
            }
        }
        $this->save();
        return $this;
    }
     /**
      * 掲載中の広告を検索
      *
      * @return array 掲載中の広告レコードの配列
      */
    public function findPublishedOffers() {

        return $this->find("status = 'publishing' AND deleted = '0'");
    }
     /**
      * 掲載中の広告数をカウント
      *
      * @return int 掲載中の広告レコードの数
      */
    public function countPublishedOffers() {

        return count($this->findPublishedOffers());
    }
     /**
      * 特定アカウントに紐付いた広告数をカウント(論理削除されたものも含む)
      * @param int 特定アカウントのID
      * @return int 該当広告レコードの数
      */
    public function countMyOffers($account_id) {

        return count($this->find("account_id = $account_id AND is_confirmed = 1"));
    }

     /**
      * 特定アカウントに紐付いた広告数をカウント(論理削除されたものを除く)
      * @param int $account_id 特定アカウントのID
      * @return int 該当広告レコードの数
      */
    public function countMyOffersNow($account_id) {

        return count($this->find("account_id = $account_id AND is_confirmed = 1 AND deleted = 0 " ));
    }
     /**
      * 平均クリック単価を出力
      * 
      * @return int 平均クリック単価
      */
    public function averageClickPrice() {

        $allPublishedOffers = $this->findPublishedOffers();

        $clickPriceSum = 0;

        foreach ($allPublishedOffers as $eachPublishedOffer) {

            $clickPriceSum += $eachPublishedOffer->click_price;
        }

        return $this->countPublishedOffers() == 0 ? '' : ceil($clickPriceSum / $this->countPublishedOffers());
    }

     /**
      * 論理削除(deletedカラムを1に)
      * 
      * @return object 論理削除されたレコード
      */
    public function softDelete() {

        $this->deleted = 1;
        $this->updateColumn();
    }
     /**
      * 表示用に給与期間単位を加工
      * @param string $salary_tarm 加工前の給与期間単位
      * @return string 加工後の給与期間単位
      */
    public function calSalaryTermDisplay($salary_term) {
        switch ($salary_term) {
            case "year":
                $salary_term_display = "年収";
                break;
            case "month":
                $salary_term_display = "月給";
                break;
            case "day":
                $salary_term_display = "日給";
                break;
            case "hour":
                $salary_term_display = "時給";
                break;
        }
        return $salary_term_display;
    }
     
     /**
      * 表示用に給与単位を加工
      * @param string $salary_tarm 加工前の給与単位
      * @return string 加工後の給与単位
      */
    public function calSalaryUnitDisplay($salary_unit) {
        switch ($salary_unit) {
            case "man_yen":
                $salary_unit_display = "万円";
                break;
            case "yen":
                $salary_unit_display = "円";
                break;
            case "dollar":
                $salary_unit_display = "$";
                break;
        }
        return $salary_unit_display;
    }
      /**
      * 表示用に雇用形態を加工
      * @param string $salary_tarm 加工前の雇用形態
      * @return string 加工後の雇用形態
      */
    public function calHiredAsDisplay($hired_as) {

        $hired_as_japanese = [];


        foreach ($hired_as as $each_hired_as) {
            switch ($each_hired_as) {

                case "full_time":
                    array_push($hired_as_japanese, "正社員");
                    break;
                case "contract":
                    array_push($hired_as_japanese, "契約社員");
                    break;
                case "temporary":
                    array_push($hired_as_japanese, "派遣社員");
                    break;
                case "part_time":
                    array_push($hired_as_japanese, "パート");
                    break;   
                case "arbeit":
                    array_push($hired_as_japanese, "アルバイト");
                    break;
            }
        }

        $hired_as_display = implode("、", $hired_as_japanese);

        return $hired_as_display;
    }

}
