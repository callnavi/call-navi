<?php

/**
 * ClickLog Model
 *
 * @category    Model
 */
class ClickLog extends ModelBase {

    public $id;
    public $created;
    public $offer_type;
    public $offer_id;
    public $listing_keyword_id;
    public $cost;

    /**
     * {@inheritdoc}
     */
    public function initialize() {
        parent::initialize();
        $this->setSource('click_logs');
    }

    /**
     * {@inheritdoc}
     */
    public function afterFetch() {
        // blank
    }

    public function validation() {
        
    }

    /**
     * {@inheritdoc}
     */
    protected function _setDefaultAttributes() {

        $this->id = null;
        $this->created = '0000-00-00 00:00:00';
        $this->offer_type = '';
        $this->offer_id = 0;
        $this->cost = 0;
        $this->listing_keyword_id = 0;
    }
     /**
     * クリックログを検索
     * @parm object $offer 対象広告のレコード
     * @parm string $offer_type 対象広告の種類
     * @parm array $scope 対象期間(開始時と終了時を格納した配列)
     * @return array $click_logs 該当クリックログレコード全てを格納した配列　　　　　　　　　　　　　　　　　　　　　　　　
     */
    public function findClickLogs($offer, $type, $scope, $isKeywords) {

        $start = $scope['start'];
        $end = $scope['end'];

        if ($isKeywords) {
            $conditions = "ClickLog.listing_keyword_id = :id: AND ClickLog.offer_type = :type: AND ClickLog.created BETWEEN :start: AND :end:";
        } else {
            $conditions = "ClickLog.offer_id = :id: AND ClickLog.offer_type = :type: AND ClickLog.created BETWEEN :start: AND :end:";
        }

        $parameters = array(
            'id' => $offer->id,
            'type' => $type,
            'start' => $start,
            'end' => $end
        );

        $click_logs = $this->find(array(
            $conditions,
            "bind" => $parameters
        ));

        return $click_logs;
    }

     /**
     * 該当広告についてのクリック金額合計を取得
     * @parm object $offer 対象広告のレコード
     * @parm string $offer_type 対象広告の種類
     * @return int $costs 該当広告についてのクリック金額合計　　　　　　　　　　　　　　　　　　　　　　　
     */
    public function getAllCosts($offer_id, $offer_type) {
        $conditions = 'offer_id = :offer_id: AND offer_type = :offer_type:';
        $parameters = [
            'offer_id' => $offer_id,
            'offer_type' => $offer_type,
        ];
        $logs = $this->find([
            $conditions,
            "bind" => $parameters
        ]);

        $costs = 0;
        foreach ($logs as $log) {
            $costs += $log->cost;
        }

        return $costs;
    }

}
