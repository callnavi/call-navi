<?php

/**
 * BaseCrawl model
 *
 * @category    Model
 */
class BaitoMynaviCrawler extends BaseCrawler {

    public function initialize() {

        parent::initialize();

    }

    /**
     * @return string of conditions when filtering
     */
    protected function getConditionForSearchResult() {

        return '.boxKyujin';
    }

    /**
     *
     * @param xmlObject $resource
     * @return string url
     */
    public function getUrl($resource) {

        $href = $resource->filter('.boxTit a')->attr('href');
        return $href;
    }

    protected function getUniqueId($detailUrl){
        preg_match('/.*\/job\/(.*)\//', $detailUrl, $matches);
        return $matches[1];
    }

    /**
     *
     * {@inheritdoc}
     *
     */
    public function getElement($resource, $nodeResource) {

        $item = $this->makeDataSet();

        try {
            $item->title = $resource->filter('#main .detail-header-left h3')->text();
        } catch (Exception $e) {
            $item->title = '';
        }

        try {
            $item->company = $resource->filter('#main .detail-con-top p.titTenpo')->text();
        } catch (Exception $ex) {
            $item->company = '';
        }

        try {
            $pic_url = $resource->filter('.boxPht img')->attr('src');
            $item->pic_url = 'http://baito.mynavi.jp' . $pic_url;
        } catch (Exception $ex) {
            $item->pic_url = '';
        }

        try {
            $item->desc = $resource->filter('.boxDtlTit h2')->html();
        } catch (Exception $ex) {
            $item->desc = '';
        }

        try {
            $item->salary = $resource->filter('#main .tableDtl tr')->eq(1)->filter('td')->eq(0)->html();
        } catch (Exception $ex) {
            try {
                $item->salary = $resource->filter('.joukenDtlTable tr')->eq(0)->filter('td')->eq(0)->html();
            } catch (Exception $ex) {
                $item->salary = '';
            }
        }

        try {
            $item->workplace = $resource->filter('#main .tableDtl tr')->eq(2)->filter('td')->eq(0)->html();
        } catch (Exception $ex) {
            try {
                $item->workplace = $resource->filter('.joukenDtlTable tr')->eq(1)->filter('td')->eq(0)->html();
            } catch (Exception $ex) {
                $item->workplace = '';
            }
        }


        try {
            $labels = [];

            $labelsNode = $resource->filter('.tableDtl .lstIcon li');
            $labelsNode->each(function ($node) use (&$labels) {
                $labels[] = $node->filter('img')->attr('alt');
            });
            $item->labels = implode(':', $labels);
        } catch (Exception $ex) {
            $item->labels = '';
        }

        try {
            $employmentType = '';
            $employmentTypes = [];
            $employmentTypesNode = $resource->filter('ul.detail-tag-list li.tag-style');
            $employmentTypesNode->each(function ($node) use (&$employmentTypes) {
                $employmentTypes[] = $node->text();
            });
            $item->employment_type = implode(',', $employmentTypes);
        } catch (Exception $ex) {  
            $item->employment_type = '';          
        }              
        

        return $item;
    }  

}
