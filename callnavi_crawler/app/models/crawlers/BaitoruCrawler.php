<?php

/**
 * BaseCrawl model
 *
 * @category    Model
 */
class BaitoruCrawler extends BaseCrawler {

    public function initialize() {

        parent::initialize();

    }

    /**
     * @override
     */
    public function generateUrl() {

        if ($this->_page == 1) {
            return $this->_url;
        } else {
            return $this->_url . $this->_url_page . $this->_page . "/";
        }
    }

    /**
     * @return string of conditions when filtering
     */
    protected function getConditionForSearchResult() {

        return '.list-jobListDetail';
    }

    /**
     *
     * @param xmlObject $resource
     * @return string url
     */
    public function getUrl($resource) {
        $href = $resource->filter('h2 a')->attr('href');
        return 'http://www.baitoru.com'. $href;
    }

    protected function getUniqueId($detailUrl){
        preg_match('/.*\/(job\d*)\//', $detailUrl, $matches);
        return $matches[1];
    }

    /**
     *
     * {@inheritdoc}
     *
     */
    public function getElement($resource, $nodeResource) {

        $item = $this->makeDataSet();

        try {
            $item->title = $resource->filter('.detail-detailHeader h2')->text();
        } catch (Exception $e) {
            $item->title = '';
        }

        try {
            $item->company = $resource->filter('.detail-entryLink .pt02 h4 span.span01')->text();
        } catch (Exception $ex) {
            $item->company = '';
        }

        try {
            $item->pic_url = 'http://www.baitoru.com' . $resource->filter('.detail-jobPhoto .pt01 ul li')->first()->filter('img')->attr('data-replaceimage');
        } catch (Exception $ex) {
            $item->pic_url = '';
        }

        try {
            $item->desc = $resource->filter('.detail-recruitInfo .pt02 table tr')->eq(0)->filter('td')->html();
        } catch (Exception $ex) {
            $item->desc = '';
        }

        try {
            $item->salary = $resource->filter('.detail-basicInfo .pt02 .bg01 table .tr01 td')->html();
        } catch (Exception $ex) {
            $item->salary = '';
        }

        try {
            $item->workplace = $resource->filter('.detail-basicInfo .pt02 .bg01 table .tr03 td')->html();
        } catch (Exception $ex) {
            $item->workplace = '';
        }

        try {
            $labels = [];
            $labelsNode = $resource->filter('.detail-basicInfo .pt03 .pt03a ul li');
            $labelsNode->each(function ($node) use (&$labels) {

                $labels[] = $node->filter('span')->text();
            });
            $item->labels = implode(':', $labels);
        } catch (Exception $ex) {
            $item->labels = '';
        }

        try {
            $item->employment_type = $resource->filter('tr.tr04 td')->text();
        } catch (Exception $ex) {
            $item->employment_type = '';
        }

        return $item;
    }

}
