<?php

/**
 * BaseCrawl model
 *
 * @category    Model
 */
class RikunabiNextCrawler extends BaseCrawler {

    public function initialize() {

        parent::initialize();

    }

    /**
     * @return string of conditions when filtering
     */
    protected function getConditionForSearchResult() {

        return 'li.rnn-jobOfferList__item';
    }

    /**
     *
     * @param xmlObject $resource
     * @return string url
     */
    public function getUrl($resource) {

        $href = $resource->filter('h2.rnn-textLl a')->attr('href');
		$return = implode('/', array_slice(explode('/', $href), 0, 3)) . '/'.str_replace("nx1_","nx2_",array_slice(explode('/', $href), 3)[0]) .'/?leadtc=ngen_tab-top_info';
		return 'http://next.rikunabi.com' . $return;
    }

    protected function getUniqueId($detailUrl){
        preg_match('/.*\d+_rq(\d*)\/.*/', $detailUrl, $matches);
        return $matches[1];
    }    

    /**
     *
     * {@inheritdoc}
     *
     */
    public function getElement($resource, $nodeResource) {

        $item = $this->makeDataSet();

        try {
            $item->title = $nodeResource->filter('h2.rnn-textLl a')->text();
        } catch (Exception $e) {
            $item->title = '';
        }

        try {
            $item->company = str_replace("企業ページ","",$nodeResource->filter('p.rnn-jobOfferList__item__company__text')->text());
        } catch (Exception $ex) {
            $item->company = '';
        }

        try {
            $pic_url = $nodeResource->filter('div.rnn-offerCatch__image img')->attr('src');
            if (!empty($pic_url)) {
                $item->pic_url = 'http:'. $pic_url;
            }
        } catch (Exception $ex) {
            $item->pic_url = '';
        }

        try {
			$resource->filter('div.rnn-group .rnn-group--l > table.rnn-detailTable > tr.rnn-tableGrid')->each(function ($node) use (&$item) {
                $th = $node;
                if (stristr($th->filter('th')->text(), '仕事の内容')) {                    
					$item->desc = $node->filter('td')->html();
                }
            });
            //$item->desc = $resource->filter('div.css_layout div.css_layout dl.recruit_details.clr')->eq(0)->filter('dd')->html();
        } catch (Exception $ex) {
            $item->desc = '';
        }

        try {
			$resource->filter('div.rnn-group .rnn-group--l > table.rnn-detailTable > tr.rnn-tableGrid')->each(function ($node) use (&$item) {
                $th = $node;
                if (stristr($th->filter('th')->text(), '給与')) {                    
					$item->salary = $node->filter('td')->html();
                }
            });          
        } catch (Exception $ex) {
            $item->salary = '';
        }

        try {
            $resource->filter('div.rnn-group .rnn-group--l > table.rnn-detailTable > tr.rnn-tableGrid')->each(function ($node) use (&$item) {
                $th = $node;
                if (stristr($th->filter('th')->text(), '勤務地')) {                    
					$item->workplace = $node->filter('td')->html();
                }
            });
        } catch (Exception $ex) {
            $item->workplace = '';
        }

        try {
            $labelsNode = $nodeResource->filter('div.rnn-jobOfferList__item__offer__detail tr.rnn-tableGrid');
            $labelsNode->each(function ($node) use (&$item) {
				if (stristr($node->filter('th')->text(), '求める人材')) {                    
					$item->labels = $node->filter('td')->text();
                }                
            });
        } catch (Exception $ex) {
            $item->labels = '';
        }

        try {
            $employment_types = [];
            $employment_typesNode = $nodeResource->filter('ul.rnn-inlineList>li>span.rnn-label');
            $employment_typesNode->each(function ($node) use (&$employment_types) {
				$employment_types[] = trim($node->text());
            });			
            $item->employment_type = implode(',', $employment_types);
        } catch (Exception $ex) { 
            $item->employment_type = '';           
        }


        return $item;
    }  

	/**
     * get data by urls search
     *
     * @return array of url strings
     */
    public function getDataByUrlsSearch($limit = false) {

        $offerUrls = [];
		$page = 1;
		$numJob = 0;
		$maxJob = $this->getMaxJob();
        do {

            $prevCount = count($offerUrls);

            $url = $this->generateUrl();

            Phalcon\DI::getDefault()->getLogger()->log("### LIST URL : $url");

            if ($xmlResource = $this->parseResource($url)) {
                $datas = $this->getDataByUrls($xmlResource);
				//$test = 0;				
                foreach ($datas as $url => $data) {
					//$test ++;
                    Phalcon\DI::getDefault()->getLogger()->log($url);
					$numJob++;
                    if (!key_exists($url, $offerUrls)) {
                        $offerUrls[$url] = $data;
                    }
					//echo $url, "\n";
					if ($maxJob && $numJob >= $maxJob) {
						return $offerUrls;
					}
                }      

                if ($limit && $limit <= count($offerUrls)) {
                    $offerUrls = array_slice($offerUrls, 1, $limit);
                    break;
                }
                
            }
			$page += 50;
            $this->_page = $page.".html";
        } while ($prevCount != count($offerUrls));
        return $offerUrls;
    }    
}
