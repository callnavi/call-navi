<?php

/**
 * BaseCrawl model
 *
 * @category    Model
 */
class AnCrawler extends BaseCrawler {

    public function initialize() {

        parent::initialize();
    }

    /**
     * @override
     */
    public function generateUrl() {

        if ($this->_page == 1) {
            return $this->_url;
        } else {
            return $this->_url . $this->_url_page . $this->_page;
        }
    }

    /**
     * @return string of conditions when filtering
     */
    protected function getConditionForSearchResult() {

        return '.shopSummaryTypeA01';
    }

    /**
     *
     * @param xmlObject $resource
     * @return string url
     */
    public function getUrl($resource) {
        try{
            $href = $resource->filter('.catch a')->attr('href');
        } catch (Exception $ex) {
            $href = '';
        }
        return $href;
    }

    protected function getUniqueId($detailUrl){
        preg_match('/\/detail\/(\d*).html/', $detailUrl, $matches);
        return $matches[1];
    }

    /**
     *
     * {@inheritdoc}
     *
     */
    public function getElement($resource, $nodeResource = null) {
        $item = $this->makeDataSet();

        try {
            $item->title = $resource->filter('.shopSummaryTypeB01 .catch p')->text();			
        } catch (Exception $e) {
            $item->title = '';
        }

        try {
            $item->company = $resource->filter('h1.pageTitle')->text();            
        } catch (Exception $ex) {
            $item->company = '';
        }

        try {
			//selector updated at 2017-09-27
			//by Xuong
            $item->pic_url = $resource->filter('.slideshowB02 .slides li')->first()->filter('img')->attr('src');
        } catch (Exception $ex) {
            $item->pic_url = '';
        }

        try {
			//selector updated at 2017-09-27
			//by Xuong
            $item->desc = $resource->filter("th:contains('仕事内容')")->nextAll('td')->text();
        } catch (Exception $ex) {
            $item->desc = '';
        }

        try {
            $item->salary = $resource->filter('.tableTypeC01  tr')->eq(1)->filter('td')->text();
        } catch (Exception $ex) {
            $item->salary = '';
        }

        try {
			//selector updated at 2017-09-27
			//by Xuong
			$item->workplace = $resource->filter("th:contains('勤務地')")->nextAll('td')->text();
        } catch (Exception $ex) {
            $item->workplace = '';
        }

        try {
			//selector updated at 2017-09-27
			//by Xuong
            $labels = [];
            //$labelsNode = $resource->filter('div.imageBoxTypeA01 .categoryIconListTypeA01 li');
            $labelsNode = $resource->filter("th:contains('応募資格')")->nextAll('td')->filter('.categoryIconListTypeA01 li');
            $labelsNode->each(function ($node) use (&$labels) {
                $labels[] = $node->filter('li')->text();
            });
            $item->labels = implode(':', $labels);
        } catch (Exception $ex) {
            $item->labels = ''; 
        }

        try {
			//selector updated at 2017-09-27
			//by Xuong
            $employmentTypes = [];
            $employmentTypesNode = $resource->filter('.categoryIconListTypeA01 .empTypeStyle');
            $employmentTypesNode->each(function ($node) use (&$employmentTypes) {                
                $employmentTypes[] = $node->filter('li')->text();
            });
            $item->employment_type = implode(',', $employmentTypes);
        } catch (Exception $ex) {  
            $item->employment_type = '';          
        }

        return $item;
    }   

}
