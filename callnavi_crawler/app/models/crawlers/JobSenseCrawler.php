<?php

/**
 * BaseCrawl model
 *
 * @category    Model
 */
class JobSenseCrawler extends BaseCrawler {

    public function initialize() {

        parent::initialize();

   }

    /**
     * @return string of conditions when filtering
     */
    protected function getConditionForSearchResult() {

        return 'article.p-search-work';
    }

    /**
     *
     * @param xmlObject $resource
     * @return string url
     */
    public function getUrl($resource) {

        $href = $resource->filter('.p-search-work-heading-appeal')->attr('href');
        return 'https://j-sen.jp' . $href;
    }

    protected function getUniqueId($detailUrl){
        preg_match('/.*\/(.*)\/y/', $detailUrl, $matches);
        return $matches[1];
    }

    /**
     *
     * {@inheritdoc}
     *
     */
    public function getElement($resource, $nodeResource) {

        $item = $this->makeDataSet();

        try {
            $item->title = $resource->filter('header.p-detail-heading>h2')->text();
        } catch (Exception $ex) {
            $item->title = '';
        }

        try {
            $item->company = $resource->filter('header.p-detail-heading>.u-text-muted')->text();
        } catch (Exception $ex) {
            $item->company = '';
        }

        try {
            $item->pic_url = $nodeResource->filter('figure.p-search-work-photo img')->attr('data-original');
        } catch (Exception $ex) {
            $item->pic_url = '';
        }

        try {
            $item->desc = $resource->filter('section.p-detail-main p.p-detail-box-sensuke')->html();
        } catch (Exception $ex) {
            $item->desc = '';
        }

        try {
            $resource->filter('.p-detail-heading + .c-widget dl')->each(function ($node) use (&$item) {
                $th = $node;
                if (stristr($th->filter('dt')->text(), '給与')) {                    
					$item->salary = $node->filter('dd')->text();
                }
            }); 
        } catch (Exception $ex) {
            $item->salary = '';
        }


        try {
             $resource->filter('.p-detail-heading + .c-widget dl')->each(function ($node) use (&$item) {
                $th = $node;
                if (stristr($th->filter('dt')->text(), '交通')) {                    
					$item->workplace = $node->filter('dd')->text();
                }
            });    
        } catch (Exception $ex) {
            $item->workplace = '';
        }

        try {
            $labels = [];
            $labelsNode = $resource->filter('div.c-widget dl.p-detail-main-data dd em.c-label-type-4');
            $labelsNode->each(function ($node) use (&$labels) {				
				$labels[] = trim($node->text());
            });			
            $item->labels = implode(':', $labels);
        } catch (Exception $ex) {
            $item->labels = '';
        }

        $item->employment_type = 'アルバイト';//only アルバイト

        return $item;
    }

}
