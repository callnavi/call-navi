<?php

/**
 * BaseCrawl model
 *
 * @category    Model
 */
require_once __DIR__ . '/../../vendor/goutte.phar';

use Goutte\Client;

class BaseCrawler extends ModelBase {

    public $id;
    public $crawl_id;
    public $offer_id;
    public $title;
    public $url;
    public $company;
    public $pic_url;
    public $desc;
    public $salary;
    public $workplace;
    public $site_id;
    public $created;
    public $updated;
    public $deleted;
    public $labels;
    public $employment_type;
    public $unique_id;
    public $cloud_search_id;
    protected $_url;
    protected $_url_page;
    protected $_page;
    protected $_current_crawl_id;
    protected $_client;
    protected $_site_id;

    protected $crawlerConfig;
    private $_targetUrls;

    public $saveElementCnt = 0;

    /**
     * {@inheritdoc}
     */
    public function initialize() {
        parent::initialize();
        $this->setSource('offers');
        $this->_current_crawl_id = 0;

        $this->crawlerConfig = Phalcon\DI::getDefault()->get('targetCrawlerConfig');
        $this->_targetUrls = (array)$this->crawlerConfig->url;
        $this->_url_page = $this->crawlerConfig->urlPage;
        $this->_page = $this->crawlerConfig->page;
        $this->_site_id = $this->crawlerConfig->siteId;

        $this->saveElementCnt = 0;
    }

    /**
     * crawlIDを取得して記録する
     *
     */
    private function setCrawlId() {

        //crawlを記録する
        $Crawl = new Crawl();

        $newCrawl = $Crawl->recordCrawling(implode(',', $this->_targetUrls));
        $this->_current_crawl_id = $newCrawl->id;

    }

    /**
     * 詳細画面のURlからunque idとして使うidを取得する
     * 実装は各サイトのクローラークラスでする
     * @param unknown $detailUrl
     */
    protected function getUniqueId($detailUrl){}

    public function crawl() {

        set_time_limit(0);

        $this->setCrawlId();

        foreach ($this->_targetUrls as $targetUrl) {
            $this->_url = $targetUrl;
            //$urls = $this->getUrlsBySearch();            
            //$this->parseDetailPage($urls);
            $datas = $this->getDataByUrlsSearch();
            $this->parseDetailPage($datas);
            $this->_page = $this->crawlerConfig->page;
        }
		$datas = null;
    }

    /**
     * 詳細画面をパーシンクする
     * @param array $urls 詳細画面のURL
     */
    protected function parseDetailPage($datas) {
        
        $Offer = new Offer();

        foreach ($datas as $url=>$data) {            

            if ($pageXml = $this->parseResource($url)) {              

                Phalcon\DI::getDefault()->getLogger()->log("*** go to parsing and save processing : $url ***");

                try {                    

                    //Phalcon\DI::getDefault()->getLogger()->log("*** ktest : $url ***" . json_encode($data));
                    $offer = $this->getElement($pageXml, $data['dataNode']);
                    $offer->original_url = $url;
                    $offer->unique_id = $this->getUniqueId($url);

                    echo $offer->labels, "\n", $offer->employment_type, "\n", $offer->original_url, "\n";
				
                    if ($this->validateElement($offer)) {
                        if ($this->checkOmitList($offer)) {
                            if(!$Offer->checkJobExist($this->_site_id, $offer->title, $offer->company)) {
                                $this->saveElement($offer);
                                $this->saveElementCnt++;
                            }
                        }
                    }
                } catch (Exception $e) {
                    Phalcon\DI::getDefault()->getLogger()->error($url . ' : ' . $e->getMessage());
                    Phalcon\DI::getDefault()->getLogger()->error($e->getTraceAsString());
                    $this->__logValidateElementError('parse', '', $this->sanitize($e));
                }
            }
			$pageXml = null;
			$data = null;
        }
		$Offer = null;
    }

    public function checkOmitList($offer) {

        $omitList = ['セントメディア'];

        foreach ($omitList as $omitTarget) {
            if(stristr($offer->company, $omitTarget)) {
                return false;
            }
        }
        return true;
    }

    /**
     * 検索結果一覧ページから、URLを取得する
     *
     * @return array of url strings
     */
    public function getUrlsBySearch($limit = false) {

        $offerUrls = [];

        do {

            $prevCount = count($offerUrls);

            $url = $this->generateUrl();

            Phalcon\DI::getDefault()->getLogger()->log("### LIST URL : $url");

            if ($xmlResource = $this->parseResource($url)) {

                $urls = $this->getUrls($xmlResource);

                foreach ($urls as $url) {
                    Phalcon\DI::getDefault()->getLogger()->log($url);

                    if (!in_array($url, $offerUrls)) {
                        $offerUrls[] = $url;
                    }
                }

                if ($limit && $limit <= count($offerUrls)) {
                    $offerUrls = array_slice($offerUrls, 1, $limit);
                    break;
                }
            }

            $this->_page++;
        } while ($prevCount != count($offerUrls));

        return $offerUrls;
    }    

    /**
     * XmlObjectから、URLの場所を抽出する
     *
     * @param xmlObject $resource
     * @return array of url strings
     */
    public function getUrls($resource) {

        $filterConditions = $this->getConditionForSearchResult();
        $dom = $resource->filter($filterConditions);

        $urls = [];
        $dom->each(function ($node) use (&$urls) {

            $url = $this->getUrl($node);
            if ($this->validateUrl($url)) {

                $urls[] = $url;
            }
        });

        return $urls;
    }

    public function generateUrl() {

        return $this->_url . $this->_url_page . $this->_page;
    }

    /**
     *
     * @param string $url
     * @return string of resource
     */
    public function parseResource($url) {

        usleep(50000);

        $client = new Client();
        $this->_client = $client;
        try {
//         	$client->getClient()->setDefaultOption('config/curl/' . CURLOPT_SSL_VERIFYPEER, false);
            $resource = $client->request('GET', $url);

            $statusCode = $client->getResponse()->getStatus();
            if ($statusCode == 200) {
                return $resource;
            }

            Phalcon\DI::getDefault()->getLogger()->error("failed get resource : $url ### status code : $statusCode");
        } catch (Exception $e) {
            Phalcon\DI::getDefault()->getLogger()->error($url . ' : ' . $e->getMessage());
            Phalcon\DI::getDefault()->getLogger()->error($e->getTraceAsString());
            return false;
        }
    }

    /**
     * 詳細ページのHTMLから、ページに応じて必要な要素を抽出する
     *
     * abstract function
     *
     *
     */
    protected function getElement($resource, $nodeResource) {

    }

    /**
     *
     * @param object $offer
     * @return bool of insertioin succeed / fails
     *
     */
    public function saveElement($offer) {

        $this->_setDefaultAttributes();
        $this->title = $offer->title;
        $this->company = $offer->company;
        $this->pic_url = $offer->pic_url;
        $this->desc = $offer->desc;
        $this->salary = $offer->salary;
        $this->workplace = $offer->workplace;
        $this->labels = $offer->labels;
        $this->employment_type = $offer->employment_type;
        $this->crawl_id = $this->_current_crawl_id;
        $this->url = $offer->original_url;
        $this->site_id = $this->_site_id;
        $this->unique_id = $offer->unique_id;
        $this->cloud_search_id = $this->_site_id . '_' . $offer->unique_id;

        return ($this->create(true, false)) ? true : false;
    }

    public function _setDefaultAttributes() {

        $this->id = null;
        $this->crawl_id = 0;
        $this->title = '';
        $this->company = '';
        $this->pic_url = '';
        $this->desc = '';
        $this->salary = '';
        $this->workplace = '';
        $this->labels = '';
        $this->employment_type = '';
        $this->hired_as = '';
        $this->unique_id ='';
        $this->cloud_search_id = '';
        $this->created = '0000-00-00 00:00:00';
        $this->updated = '0000-00-00 00:00:00';
        $this->deleted = 0;
    }

    public function makeDataSet() {

        $dataset = new stdClass();

        $dataset->crawl_id = 0;
        $dataset->title = '';
        $dataset->company = '';
        $dataset->pic_url = '';
        $dataset->desc = '';
        $dataset->salary = '';
        $dataset->workplace = '';
        $dataset->labels = '';
        $dataset->employment_type = '';

        return $dataset;
    }

    public function validateUrl($string) {

        return (preg_match('/^(https?|ftp)(:\/\/[-_.!~*\'()a-zA-Z0-9;\/?:\@&=+\$,%#]+)$/', $string)) ? true : false;
    }

    public function validateElement($item) {

        if (!(mb_strlen($item->title) > 0 && 1024 > mb_strlen($item->title))) {
            Phalcon\DI::getDefault()->getLogger()->notice("$item->url : invalid title : " . $item->title);
            $this->__logValidateElementError('title', $item->title, '1文字以上1024文字未満');
            return false;
        }

        if (!(mb_strlen($item->company) > 0 && 1024 > mb_strlen($item->company))) {
            Phalcon\DI::getDefault()->getLogger()->notice("$item->url : invalid company : " . $item->company);
            $this->__logValidateElementError('company', $item->company, '1文字以上1024文字未満');
            return false;
        }
		
		if (empty($item->unique_id)) {
			Phalcon\DI::getDefault()->getLogger()->notice("$item->url : invalid unique_id : " . $item->unique_id);
			return false;
		}

//        if (!(strlen($item->pic_url) > 0 && 2048 > strlen($item->pic_url))) {
//            $this->__logValidateElementError('pic_url', $item->pic_url, '1文字以上2048文字未満');
//            return false;
//        }
//
//        if (!(mb_strlen($item->desc) > 0)) {
//            $this->__logValidateElementError('desc', $item->desc, '1文字以上2048文字未満');
//            return false;
//        }
//
//        if (!(mb_strlen($item->salary) > 0 && 2048 > mb_strlen($item->salary))) {
//            $this->__logValidateElementError('salary', $item->salary, '1文字以上2048文字未満');
//            return false;
//        }
//
//        if (!(mb_strlen($item->workplace) > 0 && 2048 > mb_strlen($item->workplace))) {
//            $this->__logValidateElementError('workplace', $item->workplace, '1文字以上2048文字未満');
//            return false;
//        }

        return true;
    }

    private function __logValidateElementError($field, $value, $message) {

        $CrawlFailure = new CrawlFailure();

        $CrawlFailure->addFailure($field, $value, $message, $this->_current_crawl_id);
    }


    /**
     * get data by urls search
     *
     * @return array of url strings
     */
    public function getDataByUrlsSearch($limit = false) {

        $offerUrls = [];
		$numJob = 0;
		$maxJob = $this->getMaxJob();
        do {

            $prevCount = count($offerUrls);

            $url = $this->generateUrl();

            Phalcon\DI::getDefault()->getLogger()->log("### LIST URL : $url");

            if ($xmlResource = $this->parseResource($url)) {
                
                $datas = $this->getDataByUrls($xmlResource);
                foreach ($datas as $url => $data) {
                    Phalcon\DI::getDefault()->getLogger()->log($url);
					$numJob++;
                    if (!key_exists($url, $offerUrls)) {
                        $offerUrls[$url] = $data;
                    }
					echo $numJob, "\n";
					echo $url, "\n";
					if ($maxJob && $numJob >= $maxJob) {
						return $offerUrls;
					}
                }                

                if ($limit && $limit <= count($offerUrls)) {
                    $offerUrls = array_slice($offerUrls, 1, $limit);
                    break;
                }

                //echo $url, "\n", $datas['nodes'][$url], "\n";
            }

            $this->_page++;
        } while ($prevCount != count($offerUrls));
		$datas = null;
        return $offerUrls;
    }

    /**
     * Get data by urls
     *
     * @param xmlObject $resource
     * @return array of url strings
     */
    public function getDataByUrls($resource) { 
        $filterConditions = $this->getConditionForSearchResult();
        $dom = $resource->filter($filterConditions);        
        $datas = [];
        $dom->each(function ($node) use (&$datas) {
            $url = $this->getUrl($node);            
            /*
            if ($this->validateUrl($url)) {                
                $urls[] = $url;       

            }          
            */            
            $datas[$url] = array('dataNode' => $node);
        });        
        return $datas;
    }

	function getMaxJob() {
		return 12000;
	}
}
