<?php

/**
 * BaseCrawl model
 *
 * @category    Model
 */
class EaidemCrawler extends BaseCrawler {

    public function initialize() {
        parent::initialize();
    }

    /**
     * @return string of conditions when filtering
     */
    protected function getConditionForSearchResult() {

        return 'div.kyujinInfoBox';
    }

    /**
     *
     * @param xmlObject $resource
     * @return string url
     */
    public function getUrl($resource) {

        $href = $resource->filter('p.kyujinTitle a')->attr('href');
        return 'http://www.e-aidem.com/aps' . substr($href, 1, strlen($href));
    }

    protected function getUniqueId($detailUrl){
        preg_match('/.*\/aps\/(.*)_detail\.htm/', $detailUrl, $matches);
        return $matches[1];
    }

    /**
     *
     * {@inheritdoc}
     *
     */
    public function getElement($resource, $nodeResource) {


        $item = $this->makeDataSet();

        try {
            $item->title = $resource->filter('#contents .jobinfoBox')->eq(0)->filter('.title')->text();
        } catch (Exception $ex) {
            $item->title = '';
        }

        try {
            $item->company = $resource->filter('.titleBox h2')->text();
        } catch (Exception $ex) {
            $item->company = '';
        }

        try {
            $item->pic_url = $resource->filter('.imageBox img')->attr('src');
        } catch (Exception $ex) {
            $item->pic_url = '';
        }

        try {
            $item->desc = $resource->filter('.jobinfoBox table td')->eq(1)->html();
        } catch (Exception $ex) {
            $item->desc = '';
        }

        try {
            $item->salary = $resource->filter('.jobinfoBox table td')->eq(2)->text();
        } catch (Exception $ex) {
            $item->salary = '';
        }

        try {
            $item->workplace = $resource->filter('.jobinfoBox table td')->eq(3)->text();
        } catch (Exception $ex) {
            $item->workplace = '';
        }

        try {
            $labels = [];
            $labelsNode = $resource->filter('.detailInnerBox p');
            $labelsNode->each(function ($node) use (&$labels) {

                $labels[] = $node->text();
            });
            $item->labels = implode(':', $labels);
        } catch (Exception $ex) {
            $item->labels = '';
        }

        try {
            $employmentType = $nodeResource->filter('div.kyujinInfoBox div.kyujinInfo02.abstyle_A div.kyujinWord table')->eq(3)->filter('td')->text();
            $item->employment_type = str_replace('▼詳細を見る', '', $employmentType);
        } catch (Exception $ex) {
            $item->employment_type = '';
        }

        return $item;
    }

}
