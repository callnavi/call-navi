<?php

/**
 * BaseCrawl model
 *
 * @category    Model
 */
class CallnaviCrawler extends BaseCrawler {

    public function initialize() {
        $this->setSource('offers');
    }
    
    public function crawl() {
        //webサーバの用free_offersテーブルからレコードを取得し、クローラサーバのoffersテーブルに記録
        
          //テストwebサーバ用のDBに切り替え
        $connection = new Phalcon\Db\Adapter\Pdo\Mysql(array(
           'host' => '10.0.225.223',
           'username' => 'mysql_user',
           'password' => 'WpgkPTxJdM67rwcM',
           'dbname' => 'callnavi',
          ' charset' => 'utf8',
         ));
        
        $connection->connect();
        $today = date('Y-m-d H:i:s');
        $six_month_ago = date('Y-m-d H:i:s', strtotime("$today - 6 month"));
        $free_offers_found = $connection->fetchAll("SELECT * FROM free_offers WHERE deleted = :deleted AND status = :status AND allowed >= :six_month_ago",
            Phalcon\Db::FETCH_ASSOC,
            array('deleted' => 0,
                  'status' => 'publishing',
                   'six_month_ago' => $six_month_ago)
          );
        $Free_offer = new FreeOffer();
        $free_offers = $Free_offer->setFreeData($free_offers_found);
        $this->saveElementCnt = count($free_offers_found);
        foreach ($free_offers as $free_offer) {
            $this->_setDefaultAttributes();
            $this->crawl_id = 0;
            $this->site_id = 11;
            $this->title = $free_offer['job_category'];
            $this->url = $free_offer['detail_url'];
            $this->company = $free_offer['company_name'];
            $offer_id = $free_offer['id'];
            $this->pic_url = "/public/images/free/$offer_id.png";
            $this->desc = $free_offer['job_description'];
            $this->salary = $free_offer['salary_term_display']."　".$free_offer['salary_value_min']."　".$free_offer['salary_unit_min_display']."　〜　".$free_offer['salary_value_max']."　".$free_offer['salary_unit_max_display'];
            $this->workplace = $free_offer['workplace_prefecture']." ".$free_offer['workplace_area']." ".$free_offer['workplace_address'];
            $this->hired_as = $free_offer['hired_as'];
            $this->labels = $free_offer['features'];
            $this->created = $free_offer['allowed'];
            $this->updated = $free_offer['updated'];
            $this->unique_id = $free_offer['id'];
            $this->cloud_search_id = "11"."-".$free_offer['id'];
            $this->deleted = 0;
            $this->create();
            
        }
        
 
    }
    
}
