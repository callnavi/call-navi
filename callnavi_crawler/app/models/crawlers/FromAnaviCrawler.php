<?php

/**
 * BaseCrawl model
 *
 * @category    Model
 */
class FromAnaviCrawler extends BaseCrawler {

    public function initialize() {

        parent::initialize();

    }

    /**
     * @return string of conditions when filtering
     */
    protected function getConditionForSearchResult() {

        return '.job-article-outer .job-article';
    }

    /**
     *
     * @param xmlObject $resource
     * @return string url
     */
    public function getUrl($resource) {

        $href = $resource->filter('.job-article__header-ttl-outer a')->attr('href');
        return 'http://www.froma.com' . $href;
    }

    protected function getUniqueId($detailUrl){
        preg_match('/.*\/(.*)\/\?st=.*/', $detailUrl, $matches);
        return $matches[1];
    }

    /**
     *
     * {@inheritdoc}
     *
     */
    public function getElement($resource, $nodeResource) {

        $item = $this->makeDataSet();

        try {
            $item->title = $resource->filter('.hd-plain-1--detail')->text();
        } catch (Exception $e) {
            $item->title = '';
        }

        try {
            $item->company = $resource->filter('h1')->text();
        } catch (Exception $ex) {
            $item->company = '';
        }

        try {
            $pic_url = $resource->filter('.slide-images-large__img img')->attr('src');
            $item->pic_url = 'http://www.froma.com' . $pic_url;
        } catch (Exception $ex) {
            $item->pic_url = '';
        }

        try {
            $item->desc = $resource->filter('.detail-freespace__items dd')->eq(0)->html();
        } catch (Exception $ex) {
            $item->desc = '';
        }

        try {
            $item->salary = $resource->filter('ul.outline li')->eq(1)->filter('dd')->eq(0)->html();
        } catch (Exception $ex) {
            $item->salary = '';
        }

        try {
            $item->workplace = $resource->filter('.tbl-base tr')->eq(0)->filter('td')->eq(0)->html();
        } catch (Exception $ex) {
            $item->workplace = '';
        }

        try {
            $labels = [];

            $labelsNode = $nodeResource->filter('ul.job-article__property-list li');
            $labelsNode->each(function ($node) use (&$labels) {
                $labels[] = $node->text();
            });
            $item->labels = implode(':', $labels);
        } catch (Exception $ex) {
            $item->labels = '';
        }

        try {
            $item->employment_type = $nodeResource->filter('table.job-article__section-detail-table tr')->eq(0)->filter('td')->eq(1)->html();
        } catch (Exception $ex) {
            $item->employment_type = '';
        }

        return $item;
    }

}
