<?php

/**
 * BaseCrawl model
 *
 * @category    Model
 */
class TownworkCrawler extends BaseCrawler {

    public function initialize() {

        parent::initialize();

    }

    /**
     * @return string of conditions when filtering
     */
    protected function getConditionForSearchResult() {

        return 'div.job-lst-main-cassette-wrap';
    }

    /**
     *
     * @param xmlObject $resource
     * @return string url
     */
    public function getUrl($resource) {

        $href = $resource->filter('div.job-lst-box-wrap a')->attr('href');        
        return 'https://townwork.net' . $href;
    }

    protected function getUniqueId($detailUrl){
        preg_match('/.*\/detail\/(clc_.*)\//', $detailUrl, $matches);
        return $matches[1];
    }

    /**
     *
     * {@inheritdoc}
     *
     */
    public function getElement($resource, $nodeResource) {

        $item = $this->makeDataSet();

        try {
            $item->title = $resource->filter('div.job-detail-tbl-wrap dl.job-ditail-tbl-inner dd')->eq(0)->text();
        } catch (Exception $e) {
            $item->title = '';
        }

        try {
            $item->company = $resource->filter('div.job-detail-ttl-wrap h3.job-detail-ttl-txt')->text();
        } catch (Exception $ex) {
            $item->company = '';
        }

        try {
            $pic_url = $resource->filter('ul.slideshow-main-lst.jsc-slideshow-main-lst li div.job-detail-img img')->attr('src');

            if (!empty($pic_url)) {
                $item->pic_url = 'https://townwork.net' . $pic_url;
            }
        } catch (Exception $ex) {
            $item->pic_url = '';
        }

        try {
            $item->desc = $resource->filter('div.job-detail-box-tbl dl.job-ditail-tbl-inner dd')->eq(0)->html();
        } catch (Exception $ex) {
            $item->desc = '';
        }

        try {
            $item->salary = $resource->filter('div.job-detail-tbl-wrap dl.job-ditail-tbl-inner dd')->eq(1)->html();
        } catch (Exception $ex) {
            $item->salary = '';
        }

        try {
            $item->workplace = $resource->filter('div.job-detail-box-inner div.job-detail-txt-wrap p.job-detail-txt')->html();
        } catch (Exception $ex) {
            $item->workplace = '';
        }

        try {
            $labels = [];
            $labelsNode = $resource->filter('div.job-detail-merit-wrap ul.job-detail-merit-inner li');
            $labelsNode->each(function ($node) use (&$labels) {
                $labels[] = $node->text();
            });
            $item->labels = implode(':', $labels);
        } catch (Exception $ex) {
            $item->labels = '';
        }

        try {
            $item->employment_type = $nodeResource->filter('div.job-lst-box-wrap a ul.job-lst-cassette-section-detail li')->text();
        } catch (Exception $ex) {
            $item->employment_type = '';
        }

        return $item;
    }
}
