<?php

/**
 * BaseCrawl model
 *
 * @category    Model
 */
class DodaCrawler extends BaseCrawler {

    public function initialize() {

        parent::initialize();

//        $this->_url = 'http://doda.jp/DodaFront/View/JobSearchList.action?dispNum=50&k=%83R%81%5B%83%8B%83Z%83%93%83%5E%81%5B&kwc=1&ss=1&fktt=4&pic=1&ds=0&tp=1&bf=1&mpsc_sid=10';
//        $this->_url_page = '&page=';
//        $this->_page = 1;
//        $this->_site_id = 3;
    }


    /**
     * @override
     */
    public function generateUrl() {

        if ($this->_page == 1) {
            return $this->_url;
        } else {
            return $this->_url . $this->_url_page . $this->_page;
        }
    }

    /**
     * @return string of conditions when filtering
     */
    protected function getConditionForSearchResult() {

        return '.layoutList02';
    }

    /**
     * 
     * @param xmlObject $resource
     * @return string url
     */
    public function getUrl($resource) {

        $href = $resource->filter('h2.title a')->attr('href');		
        $return = implode('/', array_slice(explode('/', $href), 0, 7)) . '/-tab__jd/';
		return $return;
    }


    protected function getUniqueId($detailUrl){
        preg_match('/\/j_jid__(\d*)/', $detailUrl, $matches);
        return $matches[1];
    }

    /**
     *
     * {@inheritdoc}
     *
     */
    public function getElement($resource, $nodeResource=null) {

        $item = $this->makeDataSet();

        try {
            $item->title = $resource->filter('div.head01 p.explain')->text();
        } catch (Exception $e) {
            $item->title = '';
        }

        try {
            $item->company = $resource->filter('h1')->text();
        } catch (Exception $ex) {
            $item->company = '';
        }

        try {
            $item->pic_url = "https:" . $nodeResource->filter('div.box02 img')->attr('data-original');//attr('src');
            //$item->pic_url = str_replace('	', '', strip_tags($pic_url));
        } catch (Exception $ex) {
            $item->pic_url = '';
        }

        try {
            $resource->filter('table.tblDetail01 tr')->each(function ($node) use (&$item) {
                $th = $node;
                if (stristr($th->filter('th')->text(), '仕事内容')) {
                    //$item->desc = $node->filter('.txt_area')->eq(0)->text();
                    //$item->desc .= $node->filter('.txt_area')->eq(1)->text();
					$item->desc = $node->filter('td')->html();
                }
            });
			
        } catch (Exception $ex) {
            $item->desc = '';
        }

        try {
			$resource->filter('table.tblDetail01 tr')->each(function ($node) use (&$item) {
                $th = $node;
                if (stristr($th->filter('th')->text(), '給与')) {                    
					$item->salary = $node->filter('td')->html();
                }
            });
            //$item->salary = $resource->filter('.recruit_box02 .txt_area')->eq(9)->html();
        } catch (Exception $ex) {
            $item->salary = '';
        }

        try {
			$resource->filter('table.tblDetail01 tr')->each(function ($node) use (&$item) {
                $th = $node;
                if (stristr($th->filter('th')->text(), '勤務地')) {                    
					$item->workplace = $node->filter('td')->html();
                }
            });
            //$item->workplace = $resource->filter('.recruit_box02 .txt_area')->eq(5)->html();
        } catch (Exception $ex) {
            $item->workplace = '';
        }

        try {			
            $labels = [];
            $labelsNode = $resource->filter('ul.detailIcons li');
            $labelsNode->each(function ($node) use (&$labels) {
				$className = $node->attr('class');
				if ($className != "icoType")
					$labels[] = trim($node->filter('li')->text());
            });			
            $item->labels = implode(':', $labels);
        } catch (Exception $ex) {
            $item->labels = '';
        }

        try {		
			$employment_types = [];
            $employment_typesNode = $resource->filter('ul.detailIcons li');
            $employment_typesNode->each(function ($node) use (&$employment_types) {
				$className = $node->attr('class');
				if ($className == "icoType")
					$employment_types[] = trim(strip_tags($node->filter('li')->text()));
            });			
            $item->employment_type = implode(',', $employment_types);
        } catch (Exception $ex) {
            $item->employment_type = '';
        }

        return $item;
    }

}
