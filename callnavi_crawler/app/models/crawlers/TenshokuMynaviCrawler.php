<?php

/**
 * BaseCrawl model
 *
 * @category    Model
 */
class TenshokuMynaviCrawler extends BaseCrawler {

    public function initialize() {

        parent::initialize();

    }

    /**
     * @return string of conditions when filtering
     */
    protected function getConditionForSearchResult() {

        return '.result';
    }

    /**
     *
     * @param xmlObject $resource
     * @return string url
     */
    public function getUrl($resource) {

        $href = $resource->filter('.sub_title a')->attr('href');
        $href = str_replace('/msg', '', $href);
        return 'https://tenshoku.mynavi.jp' . $href;
    }

    protected function getUniqueId($detailUrl){
        preg_match('/.*\/jobinfo-(.*)\/\?.*/', $detailUrl, $matches);
        return $matches[1];
    }

    /**
     *
     * {@inheritdoc}
     *
     */
    public function getElement($resource, $nodeResource) {

        $item = $this->makeDataSet();

        try {
            $item->title = $resource->filter('h1 span')->text();
        } catch (Exception $e) {
            $item->title = '';
        }

        try {
            $item->company = $resource->filter('#companyInfo .title')->text();
        } catch (Exception $ex) {
            $item->company = '';
        }

        try {
            $pic_url = $resource->filter('#information  .lead .image img')->attr('src');
            $item->pic_url = 'http://'.substr($pic_url, 2, strlen($pic_url) - 2);
        } catch (Exception $ex) {
            $item->pic_url = '';
        }

        try {
            $item->desc = $resource->filter('#information .content table tr')->eq(1)->filter('td')->html();
        } catch (Exception $ex) {
            $item->desc = '';
        }

        try {
            $item->salary = $resource->filter('#information .content table tr')->eq(5)->filter('td')->html();
        } catch (Exception $ex) {
            $item->salary = '';
        }

        try {
            $item->workplace = $resource->filter('#information .content table tr')->eq(3)->filter('td')->html();
        } catch (Exception $ex) {
            $item->workplace = '';
        }

        try {
            $labels = [];
            $labelsNode = $resource->filter('#companyInfo .mark ul li');
            $labelsNode->each(function ($node) use (&$labels) {

                $labels[] = $node->text();
            });
            $item->labels = implode(':', $labels);
        } catch (Exception $ex) {
            $item->labels = '';
        }

        try {
            /*        
                1)正社員 - src="//mycom.hs.llnwd.net/e7/img/2012/jobsearch/icn_seishain.gif"
                2)派遣 - src="//mycom.hs.llnwd.net/e7/img/2012/jobsearch/icn_ippanhaken.gif"
                3)契約社員 - src="//mycom.hs.llnwd.net/e7/img/2012/jobsearch/icn_keiyakushain.gif"
                4)アルバイト - src="//mycom.hs.llnwd.net/e7/img/2012/jobsearch/icn_part_albeit.gif"
             */
            $employmentTypeDefine = [
                '//mycom.hs.llnwd.net/e113/img/2012/jobsearch/icn_seishain.gif' => '正社員', //mycom.hs.llnwd.net/e7/img/2012/jobsearch/icn_seishain.gif
                '//mycom.hs.llnwd.net/e113/img/2012/jobsearch/icn_ippanhaken.gif' => '派遣',
                '//mycom.hs.llnwd.net/e113/img/2012/jobsearch/icn_keiyakushain.gif' => '契約社員',
                '//mycom.hs.llnwd.net/e113/img/2012/jobsearch/icn_part_albeit.gif' => 'アルバイト',
                '//mycom.hs.llnwd.net/e113/img/2012/jobset/text_12.gif' => '',
                '//mycom.hs.llnwd.net/e113/img/2012/jobsearch/icn_gyoumuitaku.gif' => '業務委託',
            ];            
            $employmentTypes = [];
            $employmentTypesNode = $resource->filter('div.body h1>img');
            $employmentTypesNode->each(function ($node) use (&$employmentTypes, $employmentTypeDefine) {                
                if (isset($employmentTypeDefine[$node->attr('src')]) && $employmentTypeDefine[$node->attr('src')] != '')
                    $employmentTypes[] = $employmentTypeDefine[$node->attr('src')];
                //$employmentTypes[] = $node->attr('alt');
            });
            $item->employment_type = implode(',', $employmentTypes);
        } catch (Exception $ex) {            
            $item->employment_type = '';
        }

        return $item;
    }
	
	function getMaxJob() {
		return 300;
	}
}
