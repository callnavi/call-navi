<?php

/**
 * FreeOffer Model
 *
 * @category    Model
 */
class FreeOffer extends OfferBase {

use VariablesCalculatable;

use Mailable;

use WaitingOfferPublishable;

    public $id;
    public $created;
    public $updated;
    public $deleted;
    public $title;
    public $job_category;
    public $hired_as;
    public $hired_as_array;
    public $company_name;
    public $salary_term;
    public $salary_value_min;
    public $salary_unit_min;
    public $salary_value_max;
    public $salary_unit_max;
    public $workplace_prefecture;
    public $workplace_area;
    public $workplce_address;
    public $job_dexcription;
    public $has_pic;
    public $features;
    public $features_array;
    public $detail_url;
    public $accepted;
    public $allowed;
    public $status;
    public $account_id;
    public $impression_log;
    public $click_log;
    public $click_ratio;
   
    public $impression_ratio;
    public $type;
    public $is_confirmed;
    
    

    /**
     * {@inheritdoc}
     */
    public function initialize() {
        parent::initialize();
        $this->setSource('free_offers');
    }

    /**
     * {@inheritdoc}
     */
    public function afterFetch() {
        // blank
    }

    public function validation() {
        
    }

    /**
     * {@inheritdoc}
     */
    protected function _setDefaultAttributes() {

        $this->id = null;
        $this->created = '0000-00-00 00:00:00';
        $this->updated = '0000-00-00 00:00:00';
        $this->deleted = 0;
        $this->title = '';
        $this->has_pic = 0;
        $this->detail_url = '';
        $this->accepted = '0000-00-00 00:00:00';
        $this->allowed = '0000-00-00 00:00:00';
        $this->job_category = '';
        $this->hired_as = '';
        $this->company_name = '';
        $this->salary_term = '';
        $this->salary_value_min = '';
        $this->salary_unit_min = '';
        $this->salary_value_max = '';
        $this->salary_unit_max = '';
        $this->workplace_prefecture = '';
        $this->workplace_area = '';
        $this->workplce_address = '';
        $this->job_description = '';
        $this->features = '';
        $this->status = '';
      
        $this->account_id = 0;
        $this->type = 'free'; 
        $this->is_confirmed = 0;
        
    }
     
      /**
      * 無料広告データの検索(データ生成時降順)
      * @param array $scope 対象期間
      * @param string $account_id 対象となるアカウントのID 全アカウントについて検索したいたい場合、求める検索条件に合わせてallやadminを代入
      * @return array 検索結果の配列(データ生成時降順)
      */
    public function findFreeOffers($scope, $account_id) {

        if ($account_id == 'all') {
            $conditions = "status != 'judging' AND is_confirmed = '1'";
            $parameters = array();
        } elseif ($account_id == 'admin') {
            $conditions = "status = 'judging' AND deleted = '0' AND is_confirmed = '1'";
            $parameters = array();
        } else {
            $conditions = "account_id = :account_id: AND deleted = '0' AND is_confirmed = '1'";
            $parameters = array("account_id" => $account_id);
        }
        $offers = $this->find(array(
            "conditions" => $conditions,
            "bind" => $parameters,
            "order" => "created DESC"
        ));

        $output_offers = [];

        foreach ($offers as $offer) {
            
            $impression_log = new ImpressionLog;
            $offer->impression_log = $impression_log->countImpressionLog($offer, 'free', $scope, false);
            $all_impression_log = $impression_log->countAllImpressionLog('free', $scope);
            $click_log = new ClickLog;
            $click_logs = $click_log->findClickLogs($offer, 'free', $scope, false);
            $offer->click_log = count($click_logs);
            $offer->click_ratio = $this->calClickRatio($offer->impression_log, $offer->click_log);
            $offer->impression_ratio = $this->calImpressionRatio($offer->impression_log, $all_impression_log);
            $offer->type = "free";
            $all_impression_log = $impression_log->countAllImpressionLog('free', $scope);
            $offer->impression_ratio = $this->calImpressionRatio($offer->impression_log, $all_impression_log);
            $account = new Account;
            $account_info = $account->findAccountById($offer->account_id);
            $offer->account = $account_info->company_name;
            $offer->creditcard_id = $account_info->creditcard_id;
            
            
            $offer->type = "free";
            
            $output_offers[] = $offer;
        }

        return $output_offers;
    }
     /**
      * 無料広告データの検索(受付時昇順)
      * @param array $scope 対象期間
      * @param string $account_id 対象となるアカウントのID 全アカウントについて検索したい場合、求める検索条件に合わせてallやadminを代入
      * @return array 検索結果の配列(受付時昇順)
      */
    public function findAcceptedFreeOffers($scope, $account_id) {

        if ($account_id == 'all') {
            $conditions = "status != 'judging' AND status != 'editing' AND is_confirmed = '1'";
            $parameters = array();
        } elseif ($account_id == 'admin') {
            $conditions = "status = 'judging' AND deleted = '0' AND is_confirmed = '1'";
            $parameters = array();
        } else {
            $conditions = "account_id = :account_id: AND deleted = '0' AND is_confirmed = '1'";
            $parameters = array("account_id" => $account_id);
        }
        $offers = $this->find(array(
            "conditions" => $conditions,
            "bind" => $parameters,
            "order" => "accepted ASC"
        ));

        $output_offers = [];

        foreach ($offers as $offer) {

            $impression_log = new ImpressionLog;
            $offer->impression_log = $impression_log->countImpressionLog($offer, 'free', $scope, false);
            $click_log = new ClickLog;
            $click_logs = $click_log->findClickLogs($offer, 'free', $scope, false);
            $offer->click_log = count($click_logs);
            $offer->click_ratio = $this->calClickRatio($offer->impression_log, $offer->click_log);
            
            $offer->type = "free";

            $account = new Account;
            $account_info = $account->findAccountById($offer->account_id);
            $offer->account = $account_info->company_name;
            $offer->creditcard_id = $account_info->creditcard_id;

            $offer->type = "free";
            
            $output_offers[] = $offer;
        }

        return $output_offers;
    } 

      /**
      * 無料広告の検索(承認時降順)
      * @param array $scope 対象期間
      * @param string $account_id 対象となるアカウントのID アカウントをまたいで表示させたい場合、求める検索条件に合わせてallやadminを代入
      * @return array 検索結果の配列(承認時降順)
      */
    public function findAllowedFreeOffers($scope, $account_id) {

        if ($account_id == 'all') {
            $conditions = "status != 'judging'  AND is_confirmed = '1'";
            $parameters = array();
        } elseif ($account_id == 'admin') {
            $conditions = "status != 'judging'  AND status != 'editing' AND deleted = '0' AND is_confirmed = '1'";
            $parameters = array();
        } else {
            $conditions = "account_id = :account_id: AND deleted = '0' AND is_confirmed = '1'";
            $parameters = array("account_id" => $account_id);
        }
        $offers = $this->find(array(
            "conditions" => $conditions,
            "bind" => $parameters,
            "order" => "allowed DESC"
        ));

        $output_offers = [];

        foreach ($offers as $offer) {

            $impression_log = new ImpressionLog;
            $offer->impression_log = $impression_log->countImpressionLog($offer, 'free', $scope, false);
            $all_impression_log = $impression_log->countAllImpressionLog('free', $scope);
            $click_log = new ClickLog;
            $click_logs = $click_log->findClickLogs($offer, 'free', $scope, false);
            $offer->click_log = count($click_logs);
            $offer->click_ratio = $this->calClickRatio($offer->impression_log, $offer->click_log);
            $offer->impression_ratio = $this->calImpressionRatio($offer->impression_log, $all_impression_log);
            $offer->type = "free";

            $account = new Account;
            $account_info = $account->findAccountById($offer->account_id);
            $offer->account = $account_info->company_name;
            $offer->creditcard_id = $account_info->creditcard_id;

            $offer->type = "free";
            
            $output_offers[] = $offer;
        }

        return $output_offers;
    } 
    

    
     /**
      * IDに基づき無料広告レコードを検索(is_confirmedカラムが1のもの)
      * @param int $id 検索対象の無料広告のID
      * return object 無料広告レコード(is_confirmedカラムが1のもの)
      */
    public function findFreeOfferById($id) {

        $conditions = "id = :id: AND is_confirmed = '1'";
        $parameters = array("id" => $id);

        $offer = $this->findFirst(array(
            "conditions" => $conditions,
            "bind" => $parameters
        ));

        return $offer;
    }

     /**
      * IDに基づき無料広告レコードを検索(is_confirmedカラムが0のもの)
      * @param int $id 検索対象の無料広告のID
      * return object 無料広告レコード(is_confirmedカラムが0のもの)
      */
    public function findOnConfirmFreeOfferById($id) {

        $conditions = "id = :id: AND deleted = '0'";
        $parameters = array("id" => $id);

        $offer = $this->findFirst(array(
            "conditions" => $conditions,
            "bind" => $parameters
        ));

        return $offer;
    }
      
     /**
      * 新規無料広告レコードをDB(free_offersテーブル)に追加 statusカラムはjudging(審査中)
      * @param aray $data 新規無料広告データを格納した配列
      * return object 新規追加された無料広告レコード
      */
    public function addFreeOffer($data) {

        $this->_setDefaultAttributes();
        $this->title = $data['title'];
        $this->job_category = $data['job_category'];
        $this->hired_as = $data['hired_as_string'];
        $this->company_name = $data['company_name'];
        $this->salary_term = $data['salary_term'];
        $this->salary_value_min = $data['salary_value_min'];
        $this->salary_unit_min = $data['salary_unit_min'];
        $this->salary_unit_max = $data['salary_unit_min'];
        $this->salary_value_max = $data['salary_value_max'];
        $this->workplace_prefecture = $data['workplace_prefecture'];
        $this->workplace_area = $data['workplace_area'];
        $this->workplace_address = $data['workplace_address'];
        $this->job_description = $data['job_description'];
        $this->features = $data['features_string'];
        $this->detail_url = $data['detail_url'];
        $this->status = 'editing';
        $this->account_id = $data['account_id'];
        $this->has_pic = $data['has_pic'];
        $this->is_confirmed = 1;
        $this->create();

        return $this;
    }
     /**
      * 出稿完了未了の無料広告レコードを出稿完了済みにする(statusカラムをediting(編集中)からjudging(審査中)にする)
      * @param int $offer_id 対象広告のID
      * return object void
      */
    public function confirmFreeOffer($offer_id) {
        $FreeOffer = $this->findFirstById($offer_id);



        $FreeOffer->is_confirmed = 1;

        $FreeOffer->status = 'judging';

        $date = new DateTime();
        $today = $date->format('Y-m-d H:i:s');
        $FreeOffer->accepted = $today;

        $FreeOffer->update();
    }
      
      /**
      * 新規無料広告レコードをDB(free_offersテーブル)に追加 statusカラムはjudging(編集中)
      * @param aray $data 新規無料広告データを格納した配列
      * return object 新規追加された無料広告レコード
      */
    public function saveFreeOffer($data) {

        $this->_setDefaultAttributes();

        $this->title = $data['title'];
        $this->job_category = $data['job_category'];
        $this->hired_as = $data['hired_as_string'];
        $this->company_name = $data['company_name'];
        $this->salary_term = $data['salary_term'];
        $this->salary_value_min = $data['salary_value_min'];
        $this->salary_unit_min = $data['salary_unit_min'];
        $this->salary_unit_max = $data['salary_unit_min'];
        $this->salary_value_max = $data['salary_value_max'];
        $this->workplace_prefecture = $data['workplace_prefecture'];
        $this->workplace_area = $data['workplace_area'];
        $this->workplace_address = $data['workplace_address'];
        $this->job_description = $data['job_description'];
        $this->features = $data['features_string'];
        $this->detail_url = $data['detail_url'];
        $this->status = 'editing';
        $this->has_pic = $data['has_pic'];
        $this->account_id = $data['account_id'];
        $this->is_confirmed = 1;

        $this->create();

        return $this;
    }

      /**
      * 既に存在する無料広告レコード(is_confirmedカラムは1)を更新
      * @param array $data 更新後無料広告データを格納した配列
      * @param string $status 更新後のstatusカラム
      * return object 更新された無料広告レコード
      */
    public function updateFreeOffer($data, $status) {

        $offer = $this->findFreeOfferById($data['id']);

        $offer->title = $data['title'];
        $offer->job_category = $data['job_category'];
        $offer->hired_as = $data['hired_as_string'];
        $offer->company_name = $data['company_name'];
        $offer->salary_term = $data['salary_term'];
        $offer->salary_value_min = $data['salary_value_min'];
        $offer->salary_unit_min = $data['salary_unit_min'];
        $offer->salary_unit_max = $data['salary_unit_min'];
        $offer->salary_value_max = $data['salary_value_max'];
        $offer->workplace_prefecture = $data['workplace_prefecture'];
        $offer->workplace_area = $data['workplace_area'];
        $offer->workplace_address = $data['workplace_address'];
        $offer->job_description = $data['job_description'];
        $offer->features = $data['features_string'];
        $offer->has_pic = $data['has_pic'];
        $offer->detail_url = $data['detail_url'];

        $offer->status = $status;

        $offer->account_id = $data['account_id'];

        $offer->updateColumn();

        return $offer;
    }


    


     /**
      * ユーザ側表示用に、無料広告レコードを加工
      * @param array $frees 加工前の無料広告レコードの配列
      * return array 加工後の無料広告レコードの配列
      */
    public function setFreeData($frees) {
        
        $frees_set = [];
        foreach ($frees as $free) {
            
            if ($free['salary_term'] == 'year') {
                $free['salary_term_display'] = '年収';
            } elseif ($free['salary_term'] == 'month') {
                $free['salary_term_display'] = '月給';
            } elseif ($free['salary_term'] == 'day') {
                $free['salary_term_display'] = '日給';
            } elseif ($free['salary_term'] == 'hour') {
                $free['salary_term_display'] = '時給';
            }
            
            if ($free['salary_unit_min'] == 'man_yen') {
                $free['salary_unit_min_display'] = '万円';
            } elseif ($free['salary_unit_min'] == 'yen') {
                $free['salary_unit_min_display'] = '円';
            } elseif ($free['salary_unit_min'] == 'dollar') {
                $free['salary_unit_min_display'] = '$';
            }

            if ($free['salary_unit_max'] == 'man_yen') {
                $free['salary_unit_max_display'] = '万円';
            } elseif ($free['salary_unit_max'] == 'yen') {
                $free['salary_unit_max_display'] = '円';
            } elseif ($free['salary_unit_max'] == 'dollar') {
                $free['salary_unit_max_display'] = '$';
            }
            
            $free['features_array'] = explode(':', $free['features']);
            
            array_push($frees_set, $free);
        }
        return $frees_set;
    }
    
      /**
      * 無料広告がクリックされた際、クリックログを生成(click_logsテーブル)
      * @param int $offer_id クリックされた無料広告のID
      * return void
      */
    public function click($offer_id) {

        $click_log = new ClickLog();

        $click_log->_setDefaultAttributes();
        $click_log->offer_id = $offer_id;
        $click_log->offer_type = 'free';

        $click_log->create();
    }
      
      /**
      * 無料広告が表示された際、表示ログを生成(impression_logsテーブル)
      * @param int $offer_id 表示された無料広告のID
      * return void
      */
    public function impression($offer_id) {

        $impression_log = new ImpressionLog();

        $impression_log->_setDefaultAttributes();
        $impression_log->offer_id = $offer_id;
        $impression_log->offer_type = 'free';
        $impression_log->create();
    }
    
     /**
      * 入力されたワードに基づき、無料広告レコードを検索
      * @param array $scope 対象期間
      * @param string $term 入力されたワード
      * return void
      */
    public function findFreeOffersForSearch($scope, $term) {

        //@todo sql injection対策として、boundしてる
        $conditions = "status != 'judging' AND (
            title LIKE '%" . $term . "%'
            OR job_category LIKE '%" . $term . "%'
            OR company_name LIKE '%" . $term . "%'
            )";

        $offers = $this->find(array(
            "conditions" => $conditions,  
            "order" => "allowed DESC",
        ));

        $output_offers = [];

        foreach ($offers as $offer) {

            $impression_log = new ImpressionLog;
            $offer->impression_log = $impression_log->countImpressionLog($offer, 'free', $scope, false);
            $click_log = new ClickLog;
            $click_logs = $click_log->findClickLogs($offer, 'free', $scope, false);
            $offer->click_log = count($click_logs);
            $offer->click_ratio = $this->calClickRatio($offer->impression_log, $offer->click_log);
           
     
            $offer->type = "free";

            $account = new Account;
            $offer->account = $account->findAccountById($offer->account_id)->company_name;
            $offer->type = "free";
            
            $output_offers[] = $offer;
        }

        return $output_offers;
    }
      /**
      * 無料広告受付完了メールを出稿者へ送信
      * @param int $offer_id 対象となる無料広告のID
      * return function メール送信用の関数
      */
    public function sendThanksMail($offer_id) {

        $offer = $this->findFirstById($offer_id);
        $Account = new Account();
        $account = $Account->findAccountById($offer->account_id);

        $date = (object) [
                    'year' => date('Y'),
                    'month' => date('m'),
                    'day' => date('d'),
                    'time' => date('H:i'),
        ];

        $subject = '【コールナビ】求人広告受付完了のお知らせ';
        $path = 'free/thanks';
        $params = [
            'date' => $date,
            'offer' => $offer,
            'account' => $account,
        ];
        return $this->sendMail($account->email, $subject, $path, $params);
    }
      
      /**
      * 無料広告掲載期間が残り7日間である旨を管理者へ送信
      * @param object $offer 対象となる無料広告レコード
      * return function メール送信用の関数
      */
    public function sendStopFreeMail($offer) {

        $Account = new Account();
        $account = $Account->findAccountById($offer->account_id);

        $date = (object) [
                    'year' => date('Y', strtotime($offer->accepted)),
                    'month' => date('m', strtotime($offer->accepted)),
                    'day' => date('d', strtotime($offer->accepted)),
                    'time' => date('H:i', strtotime($offer->accepted)),
        ];

        $subject = '【コールナビ】求人広告の掲載が残り7日間です';
        $path = 'free/stop';
        $params = [
            'date' => $date,
            'offer' => $offer,
            'account' => $account,
        ];
        $mail_to = "info@callnavi.jp,m-jigyo@metaphase.co.jp";
        return $this->sendMail($mail_to, $subject, $path, $params);
    }
     
     /**
      * 掲載期間が残り7日間である無料広告を検索
      *
      * return array 掲載期間が残り７日間である無料広告レコードの配列
      */
    public function findStoppingOffers() {
        $today = date("Y/m/d");
        $seven_day_after = date('Y/m/d', strtotime("$today + 7 day"));
        $six_month_ago = date('Y/m/d', strtotime("$seven_day_after - 6 month"));
        $six_month_ago_next = date('Y/m/d', strtotime("$six_month_ago + 1 day"));
        
        $offers = $this->find(array(
            "conditions" => "deleted = '0' AND status = 'publishing' AND allowed >= '$six_month_ago' AND allowed < '$six_month_ago_next'  AND is_confirmed = '1'"
        ));
        return $offers;
        
    }
     /**
      * 無料広告受付完了メールを管理者へ送信
      * @param int $offer_id 対象となる無料広告のID
      * return function メール送信用の関数
      */
    public function sendThanksAdminMail($offer_id) {

        $offer = $this->findFirstById($offer_id);

        $subject = '【コールナビ】求人広告を受付いたしました';
        $path = 'free/admin_thanks';

        $date = (object) [
                    'year' => date('Y'),
                    'month' => date('m'),
                    'day' => date('d'),
                    'time' => date('H:i'),
        ];

        $params = [
            'date' => $date,
            'offer' => $offer
        ];

        $mail_to = $this->admin_email;
        return $this->sendMail($mail_to, $subject, $path, $params);
    }
    
    
      /**
      * cloudsearch登録のためにoffersテーブルに格納すべき無料広告レコードを、free?offersテーブルから検索
      * 
      * return object offersテーブルに格納すべき無料広告レコード(ユーザ側表示用に加工済み)
      */
    public function getOffersForCrawler(){
        $today = date('Y-m-d H:i:s');
        $six_month_ago = date('Y-m-d H:i:s', strtotime("$today - 6 month"));
        $conditions = "status = :status:  AND deleted = :deleted: AND allowed>= :six_month_ago:";
        $parameters = array("status" => 'publishing', "deleted" => 0, "six_month_ago" => $six_month_ago);
        $offers = $this->find([
            "conditions" => $conditions,
            "bind" => $parameters,
            "order" => "allowed DESC",
        ]);       
        return $this->setFreeData($offers);      
    }

 }