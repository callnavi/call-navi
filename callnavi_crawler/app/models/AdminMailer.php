<?php

/**
 * AdminMailer Model
 *
 * @category    Model
 */
class AdminMailer extends ModelBase {

    use Mailable;

    public $id;
    public $created;
    public $updated;
    public $offer_id;
    public $reason01;
    public $reason02;
    public $reason03;
    public $reason04;
    public $reason05;
    public $reason06;
    public $comment;

    /**
     * {@inheritdoc}
     */
    public function initialize() {
        parent::initialize();
        $this->setSource('unauthorized_offers');
    }

    /**
     * {@inheritdoc}
     */
    public function afterFetch() {
        // blank
    }

    public function validation() {

    }

    /**
     * {@inheritdoc}
     */
    protected function _setDefaultAttributes() {

        $this->id = null;
        $this->created = '0000-00-00 00:00:00';
        $this->updated = '0000-00-00 00:00:00';
        $this->offer_id = '0';
        $this->reason01 = '0';
        $this->reason02 = '0';
        $this->reason03 = '0';
        $this->reason04 = '0';
        $this->reason05 = '0';
        $this->reason06 = '0';
        $this->comment = '';
    }
     /**
     * 広告掲載開始メールを出稿者へ送信
     * @parm int $offer_id 該当広告のID
     * @parm string $offer_type 該当広告の種類
     * @return function メール送信用の関数　　　　　　　　　　　　　　　　　　　　　　　　
     */
    public function informPublishingMail($offer_id, $offer_type) {

        if ($offer_type == 'listing') {
            $ListingOffer = new ListingOffer();
            $offer = $ListingOffer->findFirstById($offer_id);
            $offer_type_display = 'リスティング';
        } elseif ($offer_type == 'rectangle') {
            $RectangleOffer = new RectangleOffer();
            $offer = $RectangleOffer->findFirstById($offer_id);
            $offer_type_display = 'レクタングル';
        } elseif ($offer_type == 'pickup') {
            $PickupOffer = new PickupOffer();
            $offer = $PickupOffer->findFirstById($offer_id);
            $offer_type_display = 'ピックアップ';
        }
        
        elseif ($offer_type == 'free') {
            $FreeOffer = new FreeOffer();
            $offer = $FreeOffer->findFirstById($offer_id);
            $offer_type_display = '無料';
        }
        $Account = new Account();
        $account = $Account->findAccountById($offer->account_id);

        $date = (object) [
            'year' => date('Y'),
            'month' => date('m'),
            'day' => date('d'),
            'time' => date('H:i'),
        ];

        $subject = '【コールナビ】求人広告掲載開始のお知らせ';
        $path = 'admin/publishing';
        $params = [
            'date' => $date,
            'account' => $account,
            'offer' => $offer,
            'offer_type' => $offer_type_display,
        ];
        $mail = $account->email;

        return $this->sendMail($mail, $subject, $path, $params);  
    }

//    public function informNotAllowedMail($offer_id, $offer_type, $reasons) {
//
//        if($offer_type == 'listing') {
//            $listing_offer = new ListingOffer();
//            $offer = $listing_offer->findListingOfferById($offer_id);
//            $offer_type_display = 'リスティング';
//        } elseif($offer_type == 'rectangle') {
//            $rectangle_offer = new RectangleOffer();
//            $offer = $rectangle_offer->findRectangleOfferById($offer_id);
//            $offer_type_display = 'レクタングル';
//        } else {
//            $pickup_offer = new PickupOffer();
//            $offer = $pickup_offer->findPickupOfferById($offer_id);
//            $offer_type_display = 'ピックアップ';
//        }
//
//        $Account = new Account();
//        $account = $Account->findAccountById($offer->account_id);
//
//        $date = (object) [
//                    'year' => date('Y'),
//                    'month' => date('m'),
//                    'day' => date('d'),
//                    'time' => date('H:i'),
//        ];
//
//        $link = 'http://' . $_SERVER['SERVER_NAME'] . '/login/index';
//
//        $subject = '【コールナビ】求人広告内容の非承認のお知らせ';
//        $path = 'admin/not_allowed';
//        $params = [
//            'date' => $date,
//            'account' => $account,
//            'link' => $link,
//            'offer_type' => $offer_type_display,
//            'reason01' => $reasons['reason01'],
//            'reason02' => $reasons['reason02'],
//            'reason03' => $reasons['reason03'],
//            'reason04' => $reasons['reason04'],
//            'reason05' => $reasons['reason05'],
//            'reason06' => $reasons['reason06'],
//            'comment' => $reasons['comment'],
//        ];
//        return $this->sendMail($this->admin_email, $subject, $path, $params);
//    }
     
     /**
     * 広告出港後、カード登録未了の場合カード登録待ちメールを出稿者へ送信
     * @parm int $offer_id 該当広告のID
     * @parm string $offer_type 該当広告の種類
     * @return function メール送信用の関数　　　　　　　　　　　　　　　　　　　　　　　　
     */
    public function informWaitingCreditMail($offer_id, $offer_type) {

        if ($offer_type == 'listing') {
            $ListingOffer = new ListingOffer();
            $offer = $ListingOffer->findFirstById($offer_id);
            $offer_type_display = 'リスティング';
        } elseif ($offer_type == 'rectangle') {
            $RectangleOffer = new RectangleOffer();
            $offer = $RectangleOffer->findFirstById($offer_id);
            $offer_type_display = 'レクタングル';
        } elseif ($offer_type == 'pickup') {
            $PickupOffer = new PickupOffer();
            $offer = $PickupOffer->findFirstById($offer_id);
            $offer_type_display = 'ピックアップ';
        }

        $Account = new Account();
        $account = $Account->findAccountById($offer->account_id);

        $date = (object) [
                    'year' => date('Y'),
                    'month' => date('m'),
                    'day' => date('d'),
                    'time' => date('H:i'),
        ];

        $link = 'http://' . $_SERVER['SERVER_NAME'] . '/login/index';

        $subject = '【コールナビ】クレジットカード情報登録のご案内';
        $path = 'admin/waiting_credit';

        $params = [
            'date' => $date,
            'account' => $account,
            'link' => $link,
            'offer' => $offer,
            'offer_type' => $offer_type_display,
        ];
        $mail = $account->email;

        return $this->sendMail($mail, $subject, $path, $params);
    }
      //ピックアップ広告が将来掲載予定である旨のメールを送信　使用変更前　現在は使用せず
    public function informWaitingPublishingMail($offer_id) {

        $PickupOffer = new PickupOffer();
        $offer = $PickupOffer->findFirstById($offer_id);
        $offer_type_display = 'ピックアップ';

        $Account = new Account();
        $account = $Account->findAccountById($offer->account_id);

        $date = (object) [
            'year' => date('Y'),
            'month' => date('m'),
            'day' => date('d'),
            'time' => date('H:i'),
        ];

        $link = 'http://' . $_SERVER['SERVER_NAME'] . '/login/index';

        $subject = '【コールナビ】求人広告承認のお知らせ';
        $path = 'admin/waiting_publishing';

        $params = [
            'date' => $date,
            'account' => $account,
            'link' => $link,
            'offer' => $offer,
            'offer_type' => $offer_type_display,
        ];
        $mail = $account->email;

        return $this->sendMail($mail, $subject, $path, $params);
    }

      /**
     * 広告非承認メールを出稿者へ送信
     * @parm int $offer_id 該当広告のID
     * @parm string $offer_type 該当広告の種類
     * @parm array $reasons 非承認理由
     * @return function メール送信用の関数　　　　　　　　　　　　　　　　　　　　　　　　
     */
    public function sendUnauthorizedMail($offer_id, $offer_type, $reasons) {

        if($offer_type == 'listing') {
            $listing_offer = new ListingOffer();
            $offer = $listing_offer->findListingOfferById($offer_id);
            $offer_type_display = 'リスティング';
        } elseif($offer_type == 'rectangle') {
            $rectangle_offer = new RectangleOffer();
            $offer = $rectangle_offer->findRectangleOfferById($offer_id);
            $offer_type_display = 'レクタングル';
        } else {
            $pickup_offer = new PickupOffer();
            $offer = $pickup_offer->findPickupOfferById($offer_id);
            $offer_type_display = 'ピックアップ';
        }

        $Account = new Account();
        $account = $Account->findAccountById($offer->account_id);

        $date = (object) [
            'year' => date('Y'),
            'month' => date('m'),
            'day' => date('d'),
            'time' => date('H:i'),
        ];

        $link = 'http://' . $_SERVER['SERVER_NAME'] . '/login/index';

        $subject = '【コールナビ】求人広告内容の非承認のお知らせ';
        $path = 'admin/unauthorized';
        $params = [
            'date' => $date,
            'account' => $account,
            'link' => $link,
            'offer' => $offer,
            'offer_type' => $offer_type_display,
            'reason01' => $reasons['reason01'],
            'reason02' => $reasons['reason02'],
            'reason03' => $reasons['reason03'],
            'reason04' => $reasons['reason04'],
            'reason05' => $reasons['reason05'],
            'reason06' => $reasons['reason06'],
            'comment' => $reasons['comment'],
        ];
        return $this->sendMail($account->email, $subject, $path, $params);
    }
}
