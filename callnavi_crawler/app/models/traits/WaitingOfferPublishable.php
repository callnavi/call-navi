<?php

trait WaitingOfferPublishable {
     /*
         * statusカラムがwaiting_card(カード登録待ち)である広告のstatusカラムをpublishingに変更
         * @parm $account_id アカウントID
         */
    public function publishWaitingOffer($account_id) {
        
        $conditions = "account_id = :account_id: AND deleted = '0' AND status = 'waiting_card'";
        $parameters = array(
            "account_id" => $account_id
                );

        $offers = $this->find(array(
            "conditions" => $conditions,
            "bind" => $parameters
                ));
        
        foreach($offers as $offer) {
            
            $offer->status = 'publishing';
            $offer->updateColumn();
                 
        }
        
    }
    
}
