
<?php

trait Mailable {
     /**
     * メール送信処理のための関数
     * @parm string $mail 送信先メール
     * @parm string $subject メール題名
     * @parm string $path　メール内容viewへのパス
     * @parm string $parm　メール送信内容となるパラメータ
     * @return function void　　　　　　　　　　　　　　　　　　　　　　　　　　　　
     */
    public function sendMail($mail, $subject, $path, $params) {

        $this->__initSendMail();

        //追加ヘッダーを指定。mb_send_mailの第４引数
        $additionalHeaders = "From:".mb_encode_mimeheader("コールナビ"). "<info@callnavi.jp>" ."\n";

        //パラメータ代入
        foreach ($params as $name => $param) {
            $this->view->setVar($name, $param);
        }

        //pathを設定
        $pathArray = explode('/', $path);
        $mailDir = 'mail/' . $pathArray[0];
        $mailFile = $pathArray[1];

        $this->view->start();
//        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        $this->view->render($mailDir, $mailFile);
        $this->view->finish();

        $content = $this->view->getContent();

        $content = explode('<div is_mail_content="true">', $content)[1];
        $content = explode('</div is_mail_content="true">', $content)[0];

        //@todo mb_send_mailの間はタグはなし
        $content = strip_tags($content);
        $content = mb_convert_kana($content, "K", "UTF-8");
        $content = htmlspecialchars_decode($content, ENT_QUOTES);
        mb_send_mail($mail, $subject, $content, $additionalHeaders);
    }
     /**
      * メール送信のための初期化処理
      */
    public function __initSendMail() {
        mb_language("ja");
        mb_internal_encoding("UTF-8");

        $di = new \Phalcon\DI;
        $this->view = $di->getDefault()->getView();
    }
}
