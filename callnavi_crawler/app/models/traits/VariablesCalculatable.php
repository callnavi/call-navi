<?php

trait VariablesCalculatable {
    
       /*
         * クリック率の出力
         * @param int $impression_log 表示数
         * @parm int $click_log クリック数
         * @return  int クリック率
         */
    public function calClickRatio($impression_log, $click_log) {

        if ($impression_log > 0) {
            $click_ratio = $click_log / $impression_log * 100;
            return round($click_ratio, 1);
        } else {
            return false;
        }
    }
      /*
         * 表示頻度の出力
         * @param int $impression_log 表示数
         * @parm int $all_impression_log 全表示数
         * @return int 表示頻度
         */
    public function calImpressionRatio($impression_log, $all_impression_log) {

        if ($all_impression_log > 0) {
            $impression_ratio = $impression_log / $all_impression_log * 100;
            return round($impression_ratio, 1);
        } else {
            return false;
        }
    }
    public function calExpense($click_logs) {

        $expense = 0;
        foreach($click_logs as $click_log) {
            $expense += $click_log->cost;
        }

        return $expense;
    }
    
}
