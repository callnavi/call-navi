<?php

trait TimeFormatable {

    /**
     * datetime形式の関数をslashに置き換え
     * 
     * @param datetime $datetime
     * @return string mm/dd
     */
    public function convertDateTimeToSlash($datetime) {
        $time = explode(' ', $datetime);
        $date = explode('-', $time[0]);

        return intval($date[1]) . '/' . intval($date[2]);
    }

    public function convertDateTimeToBefore($datetime) {
        if (time() - $datetime <= 31 * 24 * 60 * 60) {
            return floor((time() - $datetime) / (24 * 60 * 60)) . '日前';
        } elseif (time() - $datetime <= 12 * 31 * 24 * 60 * 60) {
            return floor((time() - $datetime) / (31 * 24 * 60 * 60)) . 'ヶ月前';
        } else {
            return floor((time() - $datetime) / (12 * 31 * 24 * 60 * 60)) . '年前';
        }
    }

    public function convertDateTimeToMinuteBefore($datetime) {
        if (time() - $datetime <= 3600) {
            return floor((time() - $datetime) / 60) . '分前';
        } elseif (time() - $datetime <= 24 * 60 * 60) {
            return floor((time() - $datetime) / (60 * 60)) . '時間前';
        } elseif (time() - $datetime <= 31 * 24 * 60 * 60) {
            return floor((time() - $datetime) / (24 * 60 * 60)) . '日前';
        } elseif (time() - $datetime <= 12 * 31 * 24 * 60 * 60) {
            return floor((time() - $datetime) / (31 * 24 * 60 * 60)) . 'ヶ月前';
        } else {
            return floor((time() - $datetime) / (12 * 31 * 24 * 60 * 60)) . '年前';
        }
    }

    public function convertDateToTaskLeft($date) {
        $diff = strtotime($date) + (24 * 60 * 60) - time();
        if ($date == '0000-00-00') {
            return '未設定';
        } else if ($diff > 24 * 60 * 60) {
            return 'あと' . floor($diff / (24 * 60 * 60)) . '日';
        } elseif ($diff > 0) {
            return '今日まで';
        } else {
            return floor((time() - strtotime($date)) / (24 * 60 * 60)) . '日遅れ';
        }
    }

    public function convertDateTimeToCommentCreated($datetime) {
        $datetimeArray = explode(' ', $datetime);
        $date = explode('-', $datetimeArray[0]);
        $time = explode(':', $datetimeArray[1]);

        $thisYear = date('Y');

        return (($date[0] != $thisYear) ? $date[0] . ', ' : '') . intval($date[1]) . '/' . intval($date[2]) . ' ' . $time[0] . ':' . $time[1];
    }

}
