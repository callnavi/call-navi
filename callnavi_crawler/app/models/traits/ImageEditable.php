<?php

trait ImageEditable {

    public function getImage($image_url, $mineType) {
        $img_file = $image_url;
        //画像ファイルデータを取得
        $img_data = file_get_contents($img_file);
        //MIMEタイプの取得
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        $mineType = finfo_buffer($finfo, $img_data);
        finfo_close($finfo);
        $minetypeArray = explode('/', $mineType);
        $fileType = $minetypeArray[1];
        if (in_array($fileType, array('jpeg', 'jpg', 'JPEG', 'JPG'))) {
            $image = imagecreatefromjpeg($image_url);
        } elseif (in_array($fileType, array('png', 'PNG'))) {
            $image = imagecreatefrompng($image_url);
        } elseif (in_array($fileType, array('gif', 'GIF'))) {
            $image = imagecreatefromgif($image_url);
        }

        
        return $image;
    }
    public function minimizeImage($image, $size) {
        //ファイルサイズを縮小
        $width = ImageSX($image);
        $height = ImageSY($image);
        $rate = 1;

        if ($width > $size) {
//            $rate = $size / $width;
        }
        if ($height > $size) {
//            $rate = $size / $height;
        }

        $out = ImageCreateTrueColor($rate * $width, $rate * $height);
        imagesavealpha($out, true);
        imagealphablending($out, false);
        ImageCopyResampled($out, $image, 0, 0, 0, 0, $rate * $width, $rate * $height, $width, $height);

        return $out;
    }

    public function locatePngImage($image, $path, $replace = true) {        
        if (file_exists($path)) {
            if($replace){
                unlink($path);                
            }
        }
        imagepng($image, $path);
    }

}
