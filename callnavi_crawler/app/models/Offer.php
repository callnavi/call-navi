<?php

/**
 * SearchedQuery model
 *
 * @category    Model
 */
class Offer extends ModelBase {

    /**
     * {@inheritdoc}
     */
    public function initialize() {
        parent::initialize();
        $this->setSource('offers');
    }

     /**
     * offersテーブルからレコードを物理削除
     */
    public function truncate() {
        $db = $this->getDi()->getShared('db');
        $db->query('TRUNCATE TABLE offers');
    }
      /**
     * offersテーブルのレコード数をカウント
     * return offersテーブルのレコード数 
     */
    public function countAllOffers() {
        return count($this->find());
    }

//    public function sendOffersToUserServer() {
//        $this->recordOffersToFile();
//        exec('sudo scp -i /home/ec2-user/.ssh/development-callnavi-key.pem  /var/www/html/htdocs/public/offers/count_offers.txt ec2-user@54.64.145.162:/var/www/html/htdocs/public/offers/count_offers.txt');
//        exit;
//    }
      
     /**
     * offersテーブルのレコード数(求人数)をファイルに記載
     */
    public function recordOffersToFile() {
        $countOffers = $this->countAllOffers();

//         $filepath = '/var/www/html/htdocs/public/offers/count_offers.txt';

        $dirPath = __DIR__ . DIRECTORY_SEPARATOR . '..'. DIRECTORY_SEPARATOR . '..'. DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'offers'. DIRECTORY_SEPARATOR;
        if (is_dir($dirPath) === false) {
            mkdir($dirPath);
        }
        $filepath = $dirPath . 'count_offers.txt';

        $fp = fopen($filepath, 'w');
        fwrite($fp, $countOffers);
        fclose($fp);
    }
     
    /**
     * offersテーブルのレコードを、$siteIdに基づき論理削除
     * @param int $siteId 広告媒体を表すID
     */
    public function changeToDelete($siteId) {
        if (empty($siteId)) {
            return false;
        }
        $db = $this->getDi()->getShared('db');
        $db->query("UPDATE offers SET deleted = 1 WHERE site_id = $siteId");
    }

     /**
     * offersテーブルのレコードで論理削除されているものを、$siteIdに基づき物理削除
     * @param int $siteId 広告媒体を表すID
     */
    public function deleteBySiteId($siteId) {
        if (empty($siteId)) {
            return false;
        }
        $db = $this->getDi()->getShared('db');
        $rs = $db->query("DELETE FROM offers WHERE deleted = 1 AND site_id = $siteId");
    }
    
    /**
     * check job exist
     * @param  [type] $title
     * @param  [type] $company
     * @param  [type] $siteId
     * @return [type]           [description]
     */
    public function checkJobExist($siteId, $title, $company) {

        $conditions = "site_id = :site_id: AND title LIKE :title: AND company LIKE :company: AND deleted = '0'";
        $parameters = array(
            "site_id" => $siteId,
            "title" => $title,
            "company" => $company,
        );

        $offer = $this->findFirst(array(
            "conditions" => $conditions,
            "bind" => $parameters
        ));

        return $offer;
    }
//    protected function _setDefaultAttributes() {
//
//        $this->id = null;
//        $this->created = '0000-00-00 00:00:00';
//        $this->updated = '0000-00-00 00:00:00';
//        $this->deleted = '0';
//        $this->site_id = 0;
//        $this->offer_id =0;
//        $this->title = '';
//        $this->url = 1;
//        $this->company = '';
//        $this->url = '';
//        $this->pic_url = '';
//        $this->desc = '';
//        $this->salary = '';
//        $this->workplace = '';
//        $this->labels = '';
//        
//    }
}
