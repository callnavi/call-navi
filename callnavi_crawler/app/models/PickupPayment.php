<?php

/**
 * PickupPayment Model
 *
 * @category    Model
 */
class PickupPayment extends ModelBase {

    public $id;
    public $created;
    public $offer_id;
    public $cost;

    /**
     * {@inheritdoc}
     */
    public function initialize() {
        parent::initialize();
        $this->setSource('pickup_payments');
    }

    /**
     * {@inheritdoc}
     */
    public function afterFetch() {
        // blank
    }

    public function validation() {
        
    }

    /**
     * {@inheritdoc}
     */
    protected function _setDefaultAttributes() {

        $this->id = null;
        $this->created = '0000-00-00 00:00:00';
        $this->offer_id = 0;
        $this->cost = 0;
        $this->completed = 0;
    }

     /**
      * 特定ピックアップ広告について課金額合計を取得
      * @param int $offer_id ピックアップ広告のID
      * @param array $scope 対象期間
      * return int 課金額合計
      */
    public function getPickupPayments($offer_id, $scope) {

        $start = $scope['start'];
        $end = $scope['end'];

        $conditions = "PickupPayment.offer_id = :offer_id: AND PickupPayment.created BETWEEN :start: AND :end:";

        $parameters = array(
            'offer_id' => $offer_id,
            'start' => $start,
            'end' => $end
        );

        $pickup_payments = $this->find(array(
            $conditions,
            "bind" => $parameters
        ));
        
        $payment_sum = 0;
        
        foreach($pickup_payments as $pickup_payment) {
            $payment_sum += $pickup_payment->cost;
        }

        return $payment_sum;
    }
    
    /**
      * 特定ピックアップ広告全期間について課金額合計を取得(completedカラムが1のもの(一度掲載終了しているもの)は除く)
      * @param int $offer_id ピックアップ広告のID
      * return int 課金額合計
      */
    public function getAllPickupPayments($offer_id) {

        $conditions = "PickupPayment.offer_id = :offer_id: AND PickupPayment.completed = 0";

        $parameters = array(
            'offer_id' => $offer_id
            
        );

        $pickup_payments = $this->find(array(
            $conditions,
            "bind" => $parameters
        ));
        
        $payment_sum = 0;
        
        foreach($pickup_payments as $pickup_payment) {
            $payment_sum += $pickup_payment->cost;
        }

        return $payment_sum;
    }
    
    /**
      * 特定ピックアップ広告に対応する全レコードの、completedカラムを1にする
      * @param int $offer_id ピックアップ広告のID
      * return int 課金額合計
      */
    public function complete($offer_id) {

        $conditions = "PickupPayment.offer_id = :offer_id: AND PickupPayment.completed = 0";

        $parameters = array(
            'offer_id' => $offer_id
            
        );

        $pickup_payments = $this->find(array(
            $conditions,
            "bind" => $parameters
        ));
        
        foreach($pickup_payments as $pickup_payment) {
            $pickup_payment->completed = 1;
            $pickup_payment->updateColumn();
            
        }

    
    }
      /**
      * ピックアップ広告課金額を出力
      * @param object $offer 課金対象ピックアップ広告
      * return object 課金額
      */
    public function calPayment($offer) {
        
        if($offer->status == 'publishing') {
            $this->_setDefaultAttributes();
            $this->offer_id = $offer->id;
            $cost = $offer->calExpense($offer->display_area, $offer->publish_week);
            if ($offer->has_published == 0) {
                $this->cost = $cost;
          } else {
            $former_cost = $this->getAllPickupPayments($offer->id);
            $this->cost = $cost - $former_cost;        
          }
            $this->create();
            return $this->cost;
        }
        
    }

}
