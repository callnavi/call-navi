<?php

/**
 * ListingKeyword Model
 *
 * @category    Model
 */
class ListingKeyword extends ModelBase {

    use VariablesCalculatable;

    public $id;
    public $created;
    public $updated;
    public $deleted;
    public $offer_id;
    public $keywords;
    public $status;
    public $click_price;
    public $impression_log;
    public $click_log;
    public $click_ratio;
    public $expense;
    public $is_available;

    /**
     * {@inheritdoc}
     */
    public function initialize() {
        parent::initialize();
        $this->setSource('listing_keywords');
    }

    /**
     * {@inheritdoc}
     */
    public function afterFetch() {
        // blank
    }

    public function validation() {
        
    }

    /**
     * {@inheritdoc}
     */
    protected function _setDefaultAttributes() {

        $this->id = null;
        $this->created = '0000-00-00 00:00:00';
        $this->updated = '0000-00-00 00:00:00';
        $this->deleted = '0';
        $this->offer_id = 0;
        $this->keywords = '';
        $this->is_available = 1;
        $this->status = '';
    }

    public function countKeywords($offer_id) {

        $conditions = "offer_id = :offer_id: AND deleted = '0'";
        $parameters = array("offer_id" => $offer_id);

        $keywords = $this->count(array(
            "conditions" => $conditions,
            "bind" => $parameters
        ));

        return $keywords;
    }

     /**
        * 対象期間とリスティング広告IDに基づき、検索キーワードを検索,適宜プロパティ(表示数、クリック数、クリック率クリック単価、クリック額合計)を追加して返す　
        * @param array $scope 対象期間
        * @param int $offer_id  対象リスティング広告のID
        * @return array  検索キーワードレコードの配列　適宜プロパティ(表示数、クリック数、クリック率クリック単価、クリック額合計)を追加
      */
    public function findKeywordsById($scope, $offer_id) {

        $conditions = "offer_id = :offer_id: AND deleted = '0'";
        $parameters = array("offer_id" => $offer_id);
      
        $keywords = $this->find(array(
            "conditions" => $conditions,
            "bind" => $parameters
        ));
      
        $listing_offer = new ListingOffer;
        $offer = $listing_offer->findListingOfferById($offer_id);

        $output_keywords = [];

        foreach ($keywords as $keyword) {

            $keyword->click_price = $offer->click_price;
            $impression_log = new ImpressionLog;
            $keyword->impression_log = $impression_log->countImpressionLog($keyword, 'listing', $scope, true);
            $click_log = new ClickLog;

            $click_logs = $click_log->findClickLogs($keyword, 'listing', $scope, true);
            $keyword->click_log = count($click_logs);
            $keyword->click_ratio = $this->calClickRatio($keyword->impression_log, $keyword->click_log);
            $keyword->expense = $this->calExpense($click_logs);

            $output_keywords[] = $keyword;
        }

        return $output_keywords;
    }
      
      /**
        * 検索キーワードのIDに基づき、検索キーワードを検索
        * @param int $keyword_id  検索キーワードのID
        * @return object  検索キーワードレコード
      */
    public function findListingKeywordById($keyword_id) {

        $conditions = "id = :id: AND deleted = 0";
        $parameters = array("id" => $keyword_id);

        $keyword = $this->findFirst(array(
            "conditions" => $conditions,
            "bind" => $parameters
        ));

        return $keyword;
    }
     /**
        * リスティング広告のIDに基づき、最初の検索キーワードレコードを検索
        * @param int $keyword_id  リスティング広告のID
        * @return array  最初の検索キーワードレコード
      */
    public function findListingKeywordByOfferId($offer_id) {

        $conditions = "offer_id = :offer_id: AND deleted = 0";
        $parameters = array("offer_id" => $offer_id);

        $keyword = $this->findFirst(array(
            "conditions" => $conditions,
            "bind" => $parameters
        ));

        return $keyword;
    }

      /**
        * 検索キーワードをDB(listing_keywordsテーブル)に格納 statusカラムはjudging(審査中)
        * @param array $allkeywords  格納すべき全検索キーワードの配列
        * @param int $offer_id  リスティング広告のID
        * @return array 新たに生成された検索キーワードレコードの配列
      */
    public function addKeywords($allkeywords, $offer_id) {



        foreach ($allkeywords as $eachkeyword) {
            $eachkeyword_A = str_replace("　", "", $eachkeyword);
            $eachkeyword_B = str_replace(" ", "", $eachkeyword_A);
            if ($eachkeyword_B !== "") {
                $this->_setDefaultAttributes();
                $this->offer_id = $offer_id;
                $this->keywords = $eachkeyword;
                $this->status = 'judging';

                $this->create();
            }
        }
        return $this;
    }

      /**
        * 検索キーワードをDB(listing_keywordsテーブル)に格納 statusカラムはediting(編集中)
        * @param array $allkeywords  格納すべき全検索キーワードの配列
        * @param int $offer_id  リスティング広告のID
        *  @return array 新たに生成された検索キーワードレコードの配列
      */
    public function saveKeywords($allkeywords, $offer_id) {

        foreach ($allkeywords as $eachkeyword) {
            $eachkeyword_A = str_replace("　", "", $eachkeyword);
            $eachkeyword_B = str_replace(" ", "", $eachkeyword_A);
            if ($eachkeyword_B !== "") {
                $this->_setDefaultAttributes();
                $this->offer_id = $offer_id;
                $this->keywords = $eachkeyword;
                $this->status = 'editing';

                $this->create();
            }
        }
        return $this;
    }

     /**
        * リスティング広告IDに紐付いた全検索キーワードを取得し、それらを改行タグでimplode
        * @param int $offer_id  リスティング広告のID
        * @return string 取得した全検索キーワードを改行タグでimplode
      */
    public function implodeKeywords($offer_id) {

        $conditions = "offer_id = :offer_id: AND deleted = '0'";
        $parameters = array("offer_id" => $offer_id);

        $keywords = $this->find(array(
            "conditions" => $conditions,
            "bind" => $parameters
        ));

        $output = [];

        foreach ($keywords as $keyword) {
            $output[] = $keyword->keywords;
        }

        $keyword_list = implode("\n", $output);
        return $keyword_list;
    }
     
     /**
        * 検索キーワードの表示頻度を取得
        * @param string $keyword_phrase キーワード
        * @param int $click_price クリック単価
        * @param int offer_id リスティング広告のID
        * @return int 表示頻度
      */ 
    public function calImpressionRatio($keyword_phrase, $click_price, $offer_id) {

        //キーワードから検索キーワードレコードを取得し、それらのoffer_idカラムの値を配列$unique_id_poolに格納
        $conditions = "deleted = '0' AND status = 'publishing' AND keywords LIKE '%" . $keyword_phrase . "%'";
        $parameters = array(
        );
        $keywords = $this->find(array(
            "conditions" => $conditions,
            "bind" => $parameters
        ));

        $id_pool = [];

        foreach ($keywords as $keyword) {
            $id_pool[] = $keyword->offer_id;
        }

        $id_pool[] = $offer_id;

        $unique_id_pool = array_unique($id_pool);
        $offers = [];

       //
        foreach ($unique_id_pool as $unique_id) {

            $offer_conditions = "id = :id: AND deleted = '0' AND is_confirmed = '1'";
            $offer_parameters = array(
                "id" => $unique_id
            );

            $listing_offer = new ListingOffer;
            $offers[] = $listing_offer->findFirst(array(
                "conditions" => $offer_conditions,
                "bind" => $offer_parameters
            ));
        }

        $click_price_sum = 0;

        //$unique_id_poolの値に基づき広告を検索、クリック単価の合計を取得
        foreach ($offers as $offer) {
            if ($offer->id == $offer_id || $offer->status == 'publishing') {
                $click_price_sum += $offer->click_price;
            }
        }

        //クリック単価の合計と、当該キーワードのクリック単価の値から表示頻度を出力
        if ($click_price_sum > 0) {
            $impression_ratio = ($click_price / $click_price_sum) * 100;
            return round($impression_ratio, 2);
        } else {
            return null;
        }
    }

      /**
        * 検索キーワードの論理削除(deletedカラムを0から1へ変更)
        * @param int $offer_id リスティング広告のID
        * @return void
      */ 
    public function softDeleteKeywords($offer_id) {

        $conditions = "deleted = '0' AND offer_id = :offer_id:";
        $parameters = array(
            "offer_id" => $offer_id
        );

        $previous_keywords = $this->find(array(
            "conditions" => $conditions,
            "bind" => $parameters
        ));

        foreach ($previous_keywords as $previous_keyword) {
            $previous_keyword->softDelete();
        }
    }

     /**
        * 検索キーワードのstatusカラムをpublising(掲載中)へ変更
        * @param int $offer_id リスティング広告のID
        * @return void
      */ 
    public function publishKeywords($offer_id) {

        $conditions = "deleted = '0' AND offer_id = :offer_id:";
        $parameters = array(
            "offer_id" => $offer_id
        );

        $keywords = $this->find(array(
            "conditions" => $conditions,
            "bind" => $parameters
        ));

        foreach ($keywords as $keyword) {

            $keyword->status = 'publishing';
            $keyword->updateColumn();
        }
    }
     /**
        * 検索キーワードのstatusカラムをwaiting_card(カード登録待ち)へ変更
        * @param int $offer_id リスティング広告のID
        * @return void
      */ 
    public function stopKeywords($offer_id) {
      
        $conditions = "deleted = '0' AND offer_id = :offer_id:";
        $parameters = array(
            "offer_id" => $offer_id
        );

        $keywords = $this->find(array(
            "conditions" => $conditions,
            "bind" => $parameters
        ));

        foreach ($keywords as $keyword) {

            $keyword->status = 'waiting_card';
            $keyword->updateColumn();
        }
    }

      /**
        * 検索キーワードのstatusカラムをwaiting_card(カード登録待ち)へ変更
        * @param int $offer_id リスティング広告のID
        * @return void
      */ 
    public function waitKeywords($offer_id) {

        $conditions = "deleted = '0' AND offer_id = :offer_id:";
        $parameters = array(
            "offer_id" => $offer_id
        );

        $keywords = $this->find(array(
            "conditions" => $conditions,
            "bind" => $parameters
        ));

        foreach ($keywords as $keyword) {

            $keyword->status = 'waiting_card';
            $keyword->updateColumn();
        }
    }
      /**
        * 上限予算到達して表示されなくなった(is_avairableカラムが0)検索キーワードをリセット(is_avairableカラムを1に)
        * 
        * @return void
      */ 
    public function resetIsAvailable() {
        $conditions = "deleted = 0 AND is_available = 0";
                
        $keywords = $this->find(array(
            "conditions" => $conditions,
        ));

        foreach ($keywords as $keyword) {

            $keyword->is_available = 1;
            $keyword->updateColumn();
        }
    }

}
