<?php

class PickupOfferStatus extends ModelBase {
        //掲載期間が終了したピックアップ広告(pickup_offersテーブル)のstatusをcompletedに変更
    public function complete() {
        
        $pickup_offer = new PickupOffer;
        
        $today = date("Y/m/d");
        
        $offers = $pickup_offer->find(array(
            "conditions" => "deleted = '0' AND (status = 'publishing' OR status = 'canceled' OR status = 'waiting_card') AND is_confirmed = '1' AND published_to < '$today'"
        ));
        
        
        foreach($offers as $offer) {            
                
                $offer->status = 'completed';
                $offer->has_published = 0;
                
                //pickup_paymentsテーブルに置いて、該当offer_idのcompletedカラムを1にする。
                $pickup_payment = new PickupPayment;
                $pickup_payment->complete($offer->id);
                
                $offer->updateColumn();
            
        }
        
    }
    
}
