<?php

set_include_path('/var/www/html/htdocs/app/vendor');
//set_include_path('/var/www/app/vendor');
require "autoload.php";
use WebPay\WebPay;

class Pay extends ModelBase {
    
    use ScopeCalculatable;
        
        /**
     * 月次課金処理(通常決済額が50円を超える場合のみ)
     * 
     * @param object $account 課金対象となるアカウント
     * @return void
     */
    public function charge($account) {
       
        $scope = $this->changeMonth(0);

        $listing_offer = new ListingOffer;
        $listing_offers = $listing_offer->findListingOffersForCharge($scope, $account->id);
        $listing_expense_sum = $this->calExpenseSum($listing_offers);

        $rectangle_offer = new RectangleOffer;
        $rectangle_offers = $rectangle_offer->findRectangleOffersForCharge($scope, $account->id);
        $rectangle_expense_sum = $this->calExpenseSum($rectangle_offers);

        $pickup_offer = new PickupOffer;
        $pickup_offers = $pickup_offer->findPickupOffersForCharge($account->id, $scope);
        $pickup_expense_sum = $this->calExpenseSum($pickup_offers);

        $expense_sum = $listing_expense_sum + $rectangle_expense_sum + $pickup_expense_sum;
        
        if($expense_sum >= 50) {
          
            $expense_sum = floor( $expense_sum * 1.08);
        
            $webpay = new WebPay('live_secret_dMo0nX6lw0os5eAcDofOg55j');
            $webpay->charge->create(array(
                "amount" => $expense_sum,
                "currency" => "jpy",
                "customer" => $account->creditcard_id,
                "description" => "広告料金"
            ));

            $payment_log = new PaymentLog;
            $payment_log->addPaymentLog($account->id, $expense_sum);
        
        }
        
    }
          /**
        * カードテストのための、50円仮課金
        * @param object $account 課金対象となるアカウント
        * @return void
        */
    public function chargeForTest($account) {
           $webpay = new WebPay('live_secret_dMo0nX6lw0os5eAcDofOg55j');
           $charge = $webpay->charge->create(array(
                "amount" => 50,
                "currency" => "jpy",
                "customer" => $account->creditcard_id,
                "caputured" => false,
                "description" => "課金テスト",
                "expire_days" => 1
            ));
           return $charge;
        }
          /**
        * カードテスト時仮課金の返金処理
        * @param int $id 返金対象となるcustomerオブジェクトのid
        * @return void
        */
    public function refund($id) {
           $webpay = new WebPay('live_secret_dMo0nX6lw0os5eAcDofOg55j');
           $webpay->charge->refund(array("id"=>$id));

    }
}
