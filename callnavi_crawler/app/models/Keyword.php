<?php


class Keyword extends ModelBase {
    
    public $id;
    public $offer_id;
    public $keyword;
    public $status;
    public $deleted;
    public $price;
    
    public function initialize() {
        parent::initialize();
        $this->setSource('keywords');
        $this->belongsTo('offer_id', 'ListingOffer', 'id');
    }
    
    public static function countKeywords($offer_id) {
         
        $listing_keyword = new ListingKeyword;
        return $listing_keyword->count(array(
             "offer_id = $offer_id"
        ));
    }
     
    public static function findKeywordsById($offer_id) {
         
        $listing_keyword = new ListigKeyowrd;
        $keywords = $listing_keyword->find(array(
          "offer_id = $offer_id"
        ));

        $listing_offer = new ListingOffer;
        $offer = $listing_offer->findListingOfferById($offer_id);
        $offer_job_type = $offer->job_type;
        $keywords->price = $offer->price;

        return array('offer_job_type' => $offer_job_type, 'parameters' => $keywords);
    }
}
