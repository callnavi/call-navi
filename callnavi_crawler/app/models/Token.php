<?php

/**
 * Token Model
 *
 * @category    Model
 */
class Token extends ModelBase {

    public $id;
    public $created;
    public $token;

    /**
     * {@inheritdoc}
     */
    public function initialize() {
        parent::initialize();
        $this->setSource('tokens');
    }

    /**
     * {@inheritdoc}
     */
    public function afterFetch() {
        // blank
    }

    public function validation() {
        
    }
     
     /**
        * パスワード再発行の際のトークンレコード(tokensテーブル)を生成
        * @param string $token　トークン
        * @return object トークンレコード
      */ 
    public function addToken($token) {

        $this->_setDefaultAttributes();

        //$this->id = '0';
        $this->created = '0000-00-00 00:00:00';
        $this->token = $token;

        $this->create();

        return $this;
    }

     /**
        * 特定のトークンを持つトークンレコードを検索、あれば当該レコードを、無ければfalseを返す
        * @param $token トークン
        * @return object トークンレコード、またはfalse
      */ 
    public function checkToken($token) {

        return $this->findFirstByToken($token);
    }

     /**
        * 特定のトークンを持つトークンレコードを検索、あれば当該レコードを、無ければfalseを返す
        * @param $token トークン
        * @return object トークンレコード、またはfalse
      */ 
    public function findFirstByToken($token) {

        $result = $this->findFirst("token = '$token'");
        if (isset($result->token)) {
            return $result;
        } else {
            return false;
        }
    }

    /**
     * {@inheritdoc}
     */
    protected function _setDefaultAttributes() {

        $this->id = null;
        $this->created = '0000-00-00 00:00:00';
        $this->token = '';
    }
}
