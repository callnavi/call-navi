<?php

/**
 * PickupOffer Model
 *
 * @category    Model
 */
class PickupOffer extends ModelBase {

    use VariablesCalculatable;

use Mailable;

use WaitingOfferPublishable;

    public $id;
    public $created;
    public $updated;
    public $deleted;
    public $title;
    public $job_category;
    public $hired_as;
    public $hired_as_array;
    public $company_name;
    public $salary_term;
    public $salary_value_min;
    public $salary_unit_min;
    public $salary_value_max;
    public $salary_unit_max;
    public $workplace_prefecture;
    public $workplace_area;
    public $workplce_address;
    public $message;
    public $features;
    public $has_first_pic;
    public $has_second_pic;
    public $has_third_pic;
   
    public $has_thum_pic;
    public $display_area;
    public $detail_url;
    public $published_from;
    public $published_to;
    public $detail_content_type;
    public $status;
    public $account_id;
    public $click_log;
    public $publish_week;
    public $accepted;
    public $allowed;
    public $expense;
    public $accoun;
    public $type;
    public $features_array;
    public $is_confirmed;
    public $salary_term_display;
    public $salary_unit_min_display;
    public $salary_unit_max_display;
    public $without_card;
    public $has_published;
    /**
     * {@inheritdoc}
     */
    public function initialize() {
        parent::initialize();
        $this->setSource('pickup_offers');
    }

    /**
     * {@inheritdoc}
     */
    public function afterFetch() {
        // blank
    }

    public function validation() {
        
    }

    /**
     * {@inheritdoc}
     */
    protected function _setDefaultAttributes() {

        $this->id = null;
        $this->created = '0000-00-00 00:00:00';
        $this->updated = '0000-00-00 00:00:00';
        $this->deleted = 0;
        $this->title = '';
        $this->job_category = '';
        $this->hired_as = '';
        $this->company_name = '';
        $this->salary_term = '';
        $this->salary_value_min = '';
        $this->salary_unit_min = '';
        $this->salary_value_max = '';
        $this->salary_unit_max = '';
        $this->workplace_prefecture = '';
        $this->workplace_area = '';
        $this->workplce_address = '';
        $this->message = '';
        $this->features = '';
        $this->has_first_pic = 0;
        $this->has_second_pic = 0;
        $this->has_third_pic = 0;
        
        $this->has_thum_pic = 0;
        $this->display_area = '';
        $this->detail_url = '';

        $this->published_from = '0000-00-00 00:00:00';
        $this->published_to = '0000-00-00 00:00:00';
        $this->publish_week = 0;
        $this->accepted = '0000-00-00 00:00:00';
        $this->allowed = '0000-00-00 00:00:00';
        $this->detail_content_type = '';
        $this->status = '';
        $this->without_card = 0;
        $this->account_id = 0;
        $this->is_confirmed = 0;
        $this->type = 'pickup';
        $this->has_published = 0;
    }
      
      /**
      * ピックアップ広告データの検索(データ生成時降順)
      * @param array $scope 対象期間
      * @param string $account_id 対象となるアカウントのID 全アカウントについて検索したいたい場合、求める検索条件に合わせてallやadminを代入
      * @return array 検索結果の配列(データ生成時降順)
      */
    public function findPickupOffers($scope, $account_id) {

        if ($account_id == 'all') {
            $conditions = "status != 'judging' AND is_confirmed = '1'";
            $parameters = array();
        } elseif ($account_id == 'admin') {
            $conditions = "status = 'judging' AND deleted = '0' AND is_confirmed = '1'";
            $parameters = array();
        } else {
            $conditions = "account_id = :account_id: AND deleted = '0' AND is_confirmed = '1'";
            $parameters = array("account_id" => $account_id);
        }
        $offers = $this->find(array(
            "conditions" => $conditions,
            "bind" => $parameters,
            "order" => "created DESC"
        ));

        $output_offers = [];

        foreach ($offers as $offer) {

            $offer->type = "pickup";

            $click_log = new ClickLog;
            $click_logs = $click_log->findClickLogs($offer, "pickup", $scope, false);
            $offer->click_log = count($click_logs);
            $offer->published_from = date("Y/m/d", strtotime($offer->published_from));
            $offer->published_to = date("Y/m/d", strtotime($offer->published_to));

            $offer->expense = $this->calExpense($offer->display_area, $offer->publish_week);
            $offer->type = "pickup";

            $account = new Account;
            $account_info = $account->findAccountById($offer->account_id);
            $offer->account = $account_info->company_name;
            $offer->creditcard_id = $account_info->creditcard_id;
            $offer->card_status = $account_info->card_status;
            $output_offers[] = $offer;
        }

        return $output_offers;
    }
    
     /**
      * ピックアップ広告データの検索(受付時昇順)
      * @param array $scope 対象期間
      * @param string $account_id 対象となるアカウントのID 全アカウントについて検索したい場合、求める検索条件に合わせてallやadminを代入
      * @return array 検索結果の配列(受付時昇順)
      */
    public function findAcceptedPickupOffers($scope, $account_id) {

        if ($account_id == 'all') {
            $conditions = "status != 'judging' AND status != 'editing' AND is_confirmed = '1'";
            $parameters = array();
        } elseif ($account_id == 'admin') {
            $conditions = "status = 'judging' AND deleted = '0' AND is_confirmed = '1'";
            $parameters = array();
        } else {
            $conditions = "account_id = :account_id: AND deleted = '0' AND is_confirmed = '1'";
            $parameters = array("account_id" => $account_id);
        }
        $offers = $this->find(array(
            "conditions" => $conditions,
            "bind" => $parameters,
            "order" => "accepted ASC"
        ));

        $output_offers = [];

        foreach ($offers as $offer) {

            $offer->type = "pickup";

            $click_log = new ClickLog;
            $click_logs = $click_log->findClickLogs($offer, "pickup", $scope, false);
            $offer->click_log = count($click_logs);
            $offer->published_from = date("Y/m/d", strtotime($offer->published_from));
            $offer->published_to = date("Y/m/d", strtotime($offer->published_to));

            $offer->expense = $this->calExpense($offer->display_area, $offer->publish_week);
            $offer->type = "pickup";

            $account = new Account;
            $account_info = $account->findAccountById($offer->account_id);
            $offer->account = $account_info->company_name;
            $offer->creditcard_id = $account_info->creditcard_id;
            $offer->card_status = $account_info->card_status;
            $output_offers[] = $offer;
        }

        return $output_offers;
    }  

     /**
      * ピックアップ広告の検索(承認時降順)
      * @param array $scope 対象期間
      * @param string $account_id 対象となるアカウントのID アカウントをまたいで表示させたい場合、求める検索条件に合わせてallやadminを代入
      * @return array 検索結果の配列(承認時降順)
      */
    public function findAllowedPickupOffers($scope, $account_id) {

        if ($account_id == 'all') {
            $conditions = "status != 'judging' AND is_confirmed = '1'";
            $parameters = array();
        } elseif ($account_id == 'admin') {
            $conditions = "status = 'judging' AND status != 'editing' AND deleted = '0' AND is_confirmed = '1'";
            $parameters = array();
        } else {
            $conditions = "account_id = :account_id: AND deleted = '0' AND is_confirmed = '1'";
            $parameters = array("account_id" => $account_id);
        }
        $offers = $this->find(array(
            "conditions" => $conditions,
            "bind" => $parameters,
            "order" => "allowed DESC"
        ));

        $output_offers = [];

        foreach ($offers as $offer) {

            $offer->type = "pickup";

            $click_log = new ClickLog;
            $click_logs = $click_log->findClickLogs($offer, "pickup", $scope, false);
            $offer->click_log = count($click_logs);
            $offer->published_from = date("Y/m/d", strtotime($offer->published_from));
            $offer->published_to = date("Y/m/d", strtotime($offer->published_to));

            $offer->expense = $this->calExpense($offer->display_area, $offer->publish_week);
            $offer->type = "pickup";

            $account = new Account;
            $account_info = $account->findAccountById($offer->account_id);
            $offer->account = $account_info->company_name;
            $offer->creditcard_id = $account_info->creditcard_id;
            $offer->card_status = $account_info->card_status;
            $output_offers[] = $offer;
        }

        return $output_offers;
    }  

     /**
      * ピックアップ広告の検索(請求画面用、論理削除されたものも出力)
      * @param array $scope 対象期間
      * @param string $account_id 対象となるアカウントのID アカウントをまたいで表示させたい場合、求める検索条件に合わせてallやadminを代入
      * @return array 検索結果の配列
      */
    public function findPickupOffersForCharge($account_id, $scope) {

        if ($account_id == 'all') {
            $conditions = "status != 'judging'";
            $parameters = array();
        } elseif ($account_id == 'admin') {
            $conditions = "status = 'judging'";
            $parameters = array();
        } else {
            $conditions = "account_id = :account_id:";
            $parameters = array("account_id" => $account_id);
        }
        $offers = $this->find(array(
            "conditions" => $conditions,
            "bind" => $parameters
        ));

        $output_offers = [];

        foreach ($offers as $offer) {

            $click_log = new ClickLog;
            $click_logs = $click_log->findClickLogs($offer, "pickup", $scope, false);
            $offer->click_log = count($click_logs);
            $offer->published_from = date("Y/m/d", strtotime($offer->published_from));
            $offer->published_to = date("Y/m/d", strtotime($offer->published_to));
            $offer->type = "pickup";

            $pickup_payment = new PickupPayment;

            $offer->expense = $pickup_payment->getPickupPayments($offer->id, $scope);

            $output_offers[] = $offer;
        }

        return $output_offers;
    }

    /* public function findPickupOffersForAdmin() {
      return $this->findPickupOffers('admin')['pickup_offers'];
      }

      public function findAllPickupOffersForAdmin() {
      return $this->findPickupOffers('all');
      } */

     /**
      * IDに基づきピックアップ広告レコードを検索(is_confirmedカラムが1のもの)
      * @param int $id 検索対象のピックアップ広告のID
      * return object ピックアップレコード(is_confirmedカラムが1のもの)
      */
    public function findPickupOfferById($id) {

        $conditions = "id = :id: AND is_confirmed = '1'";
        $parameters = array("id" => $id);

        $offer = $this->findFirst(array(
            "conditions" => $conditions,
            "bind" => $parameters
        ));

        $offer->published_from_display = date("Y/m/d", strtotime($offer->published_from));
        $offer->published_to_display = date("Y/m/d", strtotime($offer->published_to));

        $offer->expense1 = $this->calExpense1($offer->publish_week);
        $offer->expense2 = $this->calExpense2($offer->display_area);
        //
        $offer->expense = $this->calExpense($offer->display_area, $offer->publish_week);
        $offer->type = "pickup";

        $account = new Account;
        $offer->account = $account->findAccountById($offer->account_id)->company_name;

        return $offer;
    }

     /**
      * IDに基づきピックアップ広告レコードを検索(is_confirmedカラムが0のもの)
      * @param int $id 検索対象のピックアップ広告のID
      * return object ピックアップ広告レコード(is_confirmedカラムが0のもの)
      */
    public function findOnConfirmPickupOfferById($id) {

        $conditions = "id = :id: AND deleted = '0'";
        $parameters = array("id" => $id);

        $offer = $this->findFirst(array(
            "conditions" => $conditions,
            "bind" => $parameters
        ));
        /* 
        $offer->published_from = date("Y/m/d", strtotime($offer->published_from));
        $offer->published_to = date("Y/m/d", strtotime($offer->published_to));

        $offer->expense = $this->calExpense($offer->display_area, $offer->publish_week);
        $offer->type = "pickup";
        */


        return $offer;
    }

     /**
      * 新規ピックアップ広告レコードをDB(pickup_offersテーブル)に追加 statusカラムはediting(編集中)
      * @param aray $data 新規ピックアップ広告データを格納した配列
      * return object 新規追加されたピックアップ広告レコード
      */
    public function addPickupOffer($data) {

        $this->_setDefaultAttributes();
        $this->title = $data['title'];
        $this->job_category = $data['job_category'];
        $this->hired_as = $data['hired_as_string'];
        $this->company_name = $data['company_name'];
        $this->salary_term = $data['salary_term'];
        $this->salary_value_min = $data['salary_value_min'];
        $this->salary_unit_min = $data['salary_unit_min'];
        $this->salary_unit_max = $data['salary_unit_min'];
        $this->salary_value_max = $data['salary_value_max'];
        $this->workplace_prefecture = $data['workplace_prefecture'];
        $this->workplace_area = $data['workplace_area'];
        $this->workplace_address = $data['workplace_address'];
        $this->message = $data['message'];
        $this->features = $data['features_string'];

        $this->display_area = $data['display_area'];
        $this->published_from = $this->calPublishedFrom();
        $this->published_to = $this->calPublishedTo($this->calPublishedFrom(), $data['publish_week']);
        $this->publish_week = $data['publish_week'];
        $this->detail_content_type = $data['detail_content_type'];
        $this->has_first_pic = $data['has_first_pic'];
        $this->has_second_pic = $data['has_second_pic'];
        $this->has_third_pic = $data['has_third_pic'];
        
        $this->has_thum_pic = $data['has_thum_pic'];
        $this->is_confirmed = 1;
        if ($data['detail_content_type'] !== "html") {
            $this->detail_url = $data['detail_url'];
        }

        $this->status = 'editing';

        $this->account_id = $data['account_id'];
        
        

        $this->create();



        return $this;
    }

     /**
      * 出稿完了未了のピックアップ広告レコードを出稿完了済みにする(statusカラムをediting(編集中)からjudging(審査中)にする)
      * @param int $offer_id 対象広告のID
      * return object void
      */
    public function confirmPickupOffer($offer_id) {
        $PickupOffer = $this->findFirstById($offer_id);



        $PickupOffer->is_confirmed = 1;
        $PickupOffer->status = "judging";

        $date = new DateTime();
        $today = $date->format('Y-m-d H:i:s');
        $PickupOffer->accepted = $today;

        $PickupOffer->update();
    }

     /**
      * 新規ピックアップ広告レコードをDB(pickup_offersテーブル)に追加 statusカラムはediting(編集中)
      * @param aray $data 新規ピックアップ広告データを格納した配列
      * return object 新規追加されたピックアップ広告レコード
      */
    public function savePickupOffer($data) {
        $this->_setDefaultAttributes();
        $this->title = $data['title'];
        $this->job_category = $data['job_category'];
        $this->hired_as = $data['hired_as_string'];
        $this->company_name = $data['company_name'];
        $this->salary_term = $data['salary_term'];
        $this->salary_value_min = $data['salary_value_min'];
        $this->salary_unit_min = $data['salary_unit_min'];
        $this->salary_unit_max = $data['salary_unit_min'];
        $this->salary_value_max = $data['salary_value_max'];
        $this->workplace_prefecture = $data['workplace_prefecture'];
        $this->workplace_area = $data['workplace_area'];
        $this->workplace_address = $data['workplace_address'];
        $this->message = $data['message'];
        $this->features = $data['features_string'];

        $this->display_area = $data['display_area'];
        $this->published_from = $this->calPublishedFrom();
        $this->published_to = $this->calPublishedTo($this->calPublishedFrom(), $data['publish_week']);
        $this->publish_week = $data['publish_week'];

        $this->detail_content_type = $data['detail_content_type'];
        $this->has_first_pic = $data['has_first_pic'];
        $this->has_second_pic = $data['has_second_pic'];
        $this->has_third_pic = $data['has_third_pic'];
        
        $this->has_thum_pic = $data['has_thum_pic'];
        $this->is_confirmed = 1;


        if ($data['detail_content_type'] !== "html") {
            $this->detail_url = $data['detail_url'];
        }


        $this->status = 'editing';
        $this->account_id = $data['account_id'];

        $date = new DateTime();
        $today = $date->format('Y-m-d H:i:s');
        $this->accepted = $today;

        $this->create();

        return $this;
    }

     /**
      * 既に存在するピックアップ広告レコード(is_confirmedカラムは1)を更新
      * @param array $data 更新後ピックアップ広告データを格納した配列
      * @param string $status 更新後のstatusカラム
      * return object 更新されたピックアップ広告レコード
      */
    public function updatePickupOffer($data, $status) {  
        
        $offer = $this->findPickupOfferById($data['id']);
        
        $offer->title = $data['title'];
        $offer->job_category = $data['job_category'];
        $offer->hired_as = $data['hired_as_string'];
        $offer->company_name = $data['company_name'];
        $offer->salary_term = $data['salary_term'];
        $offer->salary_value_min = $data['salary_value_min'];
        $offer->salary_unit_min = $data['salary_unit_min'];
        $offer->salary_unit_max = $data['salary_unit_min'];
        $offer->salary_value_max = $data['salary_value_max'];
        $offer->workplace_prefecture = $data['workplace_prefecture'];
        $offer->workplace_area = $data['workplace_area'];
        $offer->workplace_address = $data['workplace_address'];
        $offer->message = $data['message'];
        $offer->features = $data['features_string'];

        $offer->display_area = $data['display_area'];
        
        $has_published =  $offer->has_published;
        if ($has_published == 0) {
        $offer->published_from = $this->calPublishedFrom();
        }
        $offer->published_to = $this->calPublishedTo($offer->published_from, $data['publish_week']);
        $offer->publish_week = $data['publish_week'];
        $offer->detail_content_type = $data['detail_content_type'];
        $offer->has_first_pic = $data['has_first_pic'];
        $offer->has_second_pic = $data['has_second_pic'];
        $offer->has_third_pic = $data['has_third_pic'];
        
        $offer->has_thum_pic = $data['has_thum_pic'];
        if ($data['detail_content_type'] !== "html") {
            $offer->detail_url = $data['detail_url'];
        }


        $offer->status = $status;
        $offer->account_id = $data['account_id'];


       
        $offer->updateColumn();

        return $offer;
    }
     
     
    
      /**
      * インナー広告の数をカウント
      *
      * return int インナー広告の数
      */
     public function countInners() {



        return count($this->find("display_area = 'inner' AND (status = 'publishing' OR status = 'will_publish' OR status = 'canceled') AND deleted = 0 AND is_confirmed = 1"));
    }

     /**
      * インナーパネル広告の数をカウント
      *
      * return int インナーパネル広告の数
      */
    public function countInnerHalves() {

        return count($this->find("display_area = 'inner_half' AND (status = 'publishing' OR status = 'will_publish' OR status = 'canceled') AND deleted = 0 AND is_confirmed = 1"));
    }

     /**
      * 右カラム広告の数をカウント
      *
      * return int 右カラム広告の数
      */
    public function countRightColumns() {

        return count($this->find("display_area = 'right_column' AND (status = 'publishing' OR status = 'will_publish' OR status = 'canceled') AND deleted = 0 AND is_confirmed = 1"));
    }

    

    










    /**
      * 掲載期間を出力する
      * @param datetime $published_from 掲載開始日
      * @param datetime $published_to 掲載終了日
      * return int 掲載期間
      */

    
    public function calPublishWeek($published_from, $published_to) {

        $published_from_second = strtotime($published_from);
        $published_to_second = strtotime($published_to);
        $interval = abs($published_to_second - $published_from_second);
        $week = $interval / (7 * 24 * 60 * 60);
        return round($week, 0);
    }
    /**
      * 掲載終了日を出力する
      * @param datetime $published_from 掲載開始日
      * @param int $published_week 掲載期間
      * return datetime 掲載終了日
      */
    public function calPublishedTo($published_from, $publish_week) {

        return date('Y-m-d H:i:s', strtotime("$published_from +$publish_week week"));
    }
    /**
      * 掲載期間に基づく費用を出力
      * @param int $published_week 掲載開期間
      * return int 掲載期間に基づく費用
      */
    public function calExpense1($publish_week) {

        switch ($publish_week) {
            case "0":
                $expense = 0;
                break;
            case "1":
                $expense = 15000;
                break;
            case "2":
                $expense = 26000;
                break;
            case "3":
                $expense = 36000;
                break;
            case "4":
                $expense = 40000;
                break;
                               
        }

        return $expense;
    }
    
     /**
      * 新着表示に基づく費用を出力
      * @param string $display_area 新着表示エリア
      * return int 新着表示に基づく費用
      */
    public function calExpense2($display_area) {

        switch ($display_area) {
            case "none":
                $expense = 0;
                break;
            case "inner":
                $expense = 2000;
                break;
            case "inner_half":
                $expense = 1000;
                break;
            case "right_column":
                $expense = 500;
                break;
           
        }

        return $expense;
    }
     /**
      * 掲載期間に基づく費用と新着表示に基づく費用の合計を出力
      * @param string $display_area 新着表示エリア
      * @param int $published_week 掲載開期間
      * return int 掲載期間に基づく費用と新着表示に基づく費用の合計
      */
    public function calExpense($display_area, $publish_week) {

        $expense1 = $this->calExpense1($publish_week);
        $expense2 = $this->calExpense2($display_area);
        $expense = $expense1 + $expense2;
        return $expense;

    }
     /**
      * 掲載開始日付(本日)を出力
      * 
      * return datetime 掲載開始日付(本日)
      */
    public function calPublishedFrom() {
        $date = new DateTime();
        $today = $date->format('Y-m-d H:i:s');
        
        return $today;
    }

    
     /**
      * ピックアップ広告がクリックされた際、クリックログを生成(click_logsテーブル)
      * @param int $offer_id クリックされたピックアップ広告のID
      * return void
      */
    public function click($offer_id) {

        $click_log = new ClickLog();

        $click_log->_setDefaultAttributes();
        $click_log->offer_id = $offer_id;
        $click_log->offer_type = 'pickup';
        $click_log->create();  
    }
     /**
      * ピックアップ広告が表示された際、表示ログを生成(impression_logsテーブル)
      * @param int $offer_id 表示されたピックアップ広告のID
      * return void
      */
    public function impression($offer_id) {

        $impression_log = new ImpressionLog();

        $impression_log->_setDefaultAttributes();  
        $impression_log->offer_id = $offer_id;
        $impression_log->offer_type = 'pickup';
        $impression_log->create();
    }
     /**
      * 入力されたワードに基づき、ピックアップ広告レコードを検索
      * @param string $term 入力されたワード
      * @param array $scope 対象期間
      * return void
      */
    public function findPickupOffersForSearch($scope, $term) {

        //@todo sql injection対策として、boundしてる
        $conditions = "status != 'judging' AND (
            title LIKE '%" . $term . "%'
            OR job_category LIKE '%" . $term . "%'
            OR company_name LIKE '%" . $term . "%'
            )";

        $offers = $this->find(array(
            "conditions" => $conditions,
            "order" => "allowed DESC",
        ));

        $output_offers = [];

        foreach ($offers as $offer) {

            $offer->type = "pickup";

            $click_log = new ClickLog;
            $click_logs = $click_log->findClickLogs($offer, "pickup", $scope, false);
            $offer->click_log = count($click_logs);
            ;
            $offer->published_from = date("Y/m/d", strtotime($offer->published_from));
            $offer->published_to = date("Y/m/d", strtotime($offer->published_to));

            $offer->expense = $this->calExpense($offer->display_area, $offer->publish_week);
            $offer->type = "pickup";

            $account = new Account;
            $offer->account = $account->findAccountById($offer->account_id)->company_name;

            $output_offers[] = $offer;
        }

        return $output_offers;
    }
     /**
      * 特定ピックアップ広告のstatusカラムをcompleted(掲載終了)に
      * @param object $offer 特定ピックアップ広告
      * return void
      */
    public function completePickupOffer($offer) {

//        $offer->published_to = date("Y-m-d H:i:s");
//        $this->publish_week = $this->calPublishWeek($offer->published_from, $offer->published_to);

        $pickup_payment = new PickupPayment;
        $offer->expense = $pickup_payment->calPayment($offer);

        $offer->status = "completed";

        $offer->has_published = 0;

        $offer->updateColumn();
    }
     /**
      * ピックアップ広告受付完了メールを出稿者へ送信
      * @param int $offer_id 対象となるピックアップ広告のID
      * return function メール送信用の関数
      */
    public function sendThanksMail($offer_id) {

        $offer = $this->findFirstById($offer_id);
        $Account = new Account();
        $account = $Account->findAccountById($offer->account_id);

        $date = (object) [
                    'year' => date('Y'),
                    'month' => date('m'),
                    'day' => date('d'),
                    'time' => date('H:i'),
        ];

        $subject = '【コールナビ】求人広告受付完了のお知らせ';
        $path = 'pickup/thanks';
        $params = [
            'date' => $date,
            'offer' => $offer,
            'account' => $account,
        ];
        return $this->sendMail($account->email, $subject, $path, $params);
    }
     /**
      * ピックアップ広告の掲載が前日終了した旨を出稿者へ送信
      * @param object $offer 対象となるピックアップ広告レコード
      * return function メール送信用の関数
      */
    public function sendStopPickupMail($offer) {
        $Account = new Account();
        $account = $Account->findAccountById($offer->account_id);

        $date = (object) [
                    'year' => date('Y', strtotime($offer->accepted)),
                    'month' => date('m', strtotime($offer->accepted)),
                    'day' => date('d', strtotime($offer->accepted)),
                    'time' => date('H:i', strtotime($offer->accepted)),
        ];

        $subject = '【コールナビ】求人広告の掲載が終了しました';
        $path = 'pickup/stop';
        $params = [
            'date' => $date,
            'offer' => $offer,
            'account' => $account,
        ];
        return $this->sendMail($account->email, $subject, $path, $params);
    }
     /**
      * 掲載が前日終了したピックアップ広告を検索
      * 
      * return object 掲載が前日終了したピックアップ広告を検索
      */
    public function findStoppedOffers() {
        $today = date("Y/m/d");
        $one_day_ago = date('Y/m/d', strtotime("$today - 1 day"));
        $offers = $this->find(array(
            "conditions" => "deleted = '0' AND status = 'completed' AND published_to >= '$one_day_ago' AND published_to < '$today' AND is_confirmed = '1'"
        ));
        return $offers;
    }
     
     /**
      * ピックアップ広告受付完了メールを管理者へ送信
      * @param int $offer_id 対象となるピックアップ広告のID
      * return function メール送信用の関数
      */
    public function sendThanksAdminMail($offer_id) {

        $offer = $this->findFirstById($offer_id);

        $subject = '【コールナビ】求人広告を受付いたしました';
        $path = 'pickup/admin_thanks';

        $date = (object) [
                    'year' => date('Y'),
                    'month' => date('m'),
                    'day' => date('d'),
                    'time' => date('H:i'),
        ];

        $params = [
            'date' => $date,
            'offer' => $offer
        ];
        $mail_to = $this->admin_email;

        return $this->sendMail($mail_to, $subject, $path, $params);
    }
     
     /**
        * ピックアップ広告のstatusをwaiting_card(カード登録待ち)からpublishing(掲載中)に変更 
        * @param int $account_id アカウントのID
        * @return void
      */ 
    public function publishWaitingOffer($account_id) {

        $conditions = "account_id = :account_id: AND deleted = '0' AND status = 'waiting_card' AND is_confirmed = 1";
        $parameters = array(
            "account_id" => $account_id
        );

        $offers = $this->find(array(
            "conditions" => $conditions,
            "bind" => $parameters
        ));

        foreach ($offers as $offer) {

            

            $has_published =  $offer->has_published;
            if ($has_published == 0) {
            $offer->published_from = $this->calPublishedFrom();
                }
            $offer->published_to = $this->calPublishedTo($offer->published_from, $offer->publish_week);
            $date = new DateTime();
            $today = $date->format('Y-m-d H:i:s');  
            
            $offer->status = "publishing";
            $offer->has_published = 1;
            //pickup_paymentsへの、課金レコード作成処理(月次請求の際に参照)
                $pickup_payment = new PickupPayment;
                $pickup_payment->calPayment($offer);

            $offer->updateColumn();
        }

        if (count($offers) > 0) {
            $AdminMailer = new AdminMailer();
            $AdminMailer->informPublishingMail($offers[0]->id, 'pickup');
        }
    }
     /**
        * ピックアップ広告のstatusをpublishing(掲載中)からwaiting_card(カード登録待ち)に変更
        * @param int $account_id アカウントのID
        * @return void
      */ 
    public function stopPublishingOffer($account_id) {

        $conditions = "account_id = :account_id: AND deleted = '0' AND (status = 'publishing' OR status = 'canceled' OR status = 'will_publish') AND is_confirmed = 1 AND without_card = 0";
        $parameters = array(
            "account_id" => $account_id
        );

        $offers = $this->find(array(
            "conditions" => $conditions,
            "bind" => $parameters
        ));

        foreach ($offers as $offer) {
            //カード登録削除後再登録した場合、掲載期間は、登録削除の前のまま変わらないようにする
            /* 
            $offer->published_from = $this->calPublishedFrom();
            $offer->published_to = $this->calPublishedTo($offer->published_from, $offer->publish_week);
              */
            $offer->status = "waiting_card";
            $offer->updateColumn();
        }
    }
     /**
        * 掲載開始7日以内のインナーパネル広告を出力
        * 
        * @return array 掲載開始7日以内のインナーパネル広告レコードの配列
      */ 
    public function newInners() {

        
        $today = date('Y-m-d H:i:s');
        $recent_past = date('Y-m-d H:i:s', strtotime("$today - 7 day"));
        $conditions = "status = :status:  AND deleted = 0 AND published_from >= :resent_past: AND display_area = :display_area:";
        $parameters = array("status" => 'publishing', "resent_past" => $recent_past, "display_area" =>'inner');

        $inners = $this->find([
            "conditions" => $conditions,
            "bind" => $parameters,
            "limit" => 5,
            "order" => "rand()",
        ]);
        

        return $this->setPickupData($inners);
    
    }

     /**
        * 掲載開始7日以内のインナーハーフ広告を出力
        * 
        * @return array 掲載開始7日以内のインナーハーフ広告レコードの配列
      */ 
    public function newInnerHalfs() {

        
        $today = date('Y-m-d H:i:s');
        $recent_past = date('Y-m-d H:i:s', strtotime("$today - 7 day"));
        $conditions = "status = :status:  AND deleted = 0 AND published_from >= :resent_past: AND display_area = :display_area:";
        $parameters = array("status" => 'publishing', "resent_past" => $recent_past, "display_area" =>'inner_half');

        $inner_halfs = $this->find([
            "conditions" => $conditions,
            "bind" => $parameters,
            "limit" => 4,
            "order" => "rand()",
        ]);

        return $this->setPickupData($inner_halfs);
    
    }
     /**
        * 掲載開始7日以内の右カラム広告を出力
        * 
        * @return array 掲載開始7日以内の右カラム広告レコードの配列
      */ 
    public function newColumns() {

        
        $today = date('Y-m-d H:i:s');
        $recent_past = date('Y-m-d H:i:s', strtotime("$today - 7 day"));
        $conditions = "status = :status:  AND deleted = 0 AND published_from >= :resent_past: AND display_area = :display_area:";
        $parameters = array("status" => 'publishing', "resent_past" => $recent_past, "display_area" =>'right_column');

        $columns = $this->find([
            "conditions" => $conditions,
            "bind" => $parameters,
            "limit" => 2,
            "order" => "rand()",
        ]);
        

        return $this->setPickupData($columns);
    
    }
     /**
     * ピックアップ求人一覧を取得
     * 
     * @parm int $page どの検索結果ページに表示されるピックアップ求人かを指定(1ページ目が0)
     * @parm int $size 検索結果ページ1ページに表示される広告件数を指定
     * @return array 該当ページピックアップ求人一覧の配列、　　　　　　　　　　　　　　　　　　　　　　
     */
    public function findPickupLists($page = 0, $size = 20) {
        $conditions = "status = :status:  AND deleted = :deleted: ";
        $parameters = array("status" => 'publishing', "deleted" => 0);
        $pickups = $this->find([
            "conditions" => $conditions,
            "bind" => $parameters,
            "order" => "published_from DESC",
        ]);
        $pickups_selected = [];
        
        for ($i = 0; $i < $size; $i++) {
            if (isset($pickups[$i + $page*$size])) {
            $pickups_selected[$i] = $pickups[$i + $page*$size];
            }
        }
         return $this->setPickupData($pickups_selected);
    }
     /**
      * ユーザ側表示用に、ピックアップ広告レコードを加工
      * @param array $frees 加工前のピックアップレコードの配列
      * return array 加工後のピックアップ広告レコードの配列
      */
    public function setPickupData($pickups) {
        
        $pickups_set = [];
        foreach ($pickups as $pickup) {

            if ($pickup->salary_term == 'year') {
                $pickup->salary_term_display = '年収';
            } elseif ($pickup->salary_term == 'month') {
                $pickup->salary_term_display = '月給';
            } elseif ($pickup->salary_term == 'day') {
                $pickup->salary_term_display = '日給';
            } elseif ($pickup->salary_term == 'hour') {
                $pickup->salary_term_display = '時給';
            }

            if ($pickup->salary_unit_min == 'man_yen') {
                $pickup->salary_unit_min_display = '万円';
            } elseif ($pickup->salary_unit_min == 'yen') {
                $pickup->salary_unit_min_display = '円';
            } elseif ($pickup->salary_unit_min == 'dollar') {
                $pickup->salary_unit_min_display = '$';
            }

            if ($pickup->salary_unit_max == 'man_yen') {
                $pickup->salary_unit_max_display = '万円';
            } elseif ($pickup->salary_unit_max == 'yen') {
                $pickup->salary_unit_max_display = '円';
            } elseif ($pickup->salary_unit_max == 'dollar') {
                $pickup->salary_unit_max_display = '$';
            }

            $pickup->features_array = explode(':', $pickup->features);

            array_push($pickups_set, $pickup);
        }
        return $pickups_set;
    }
    
     /**
      * 入力されたワードに基づき、ピックアップ広告を出力
      * @param string $keyword　ユーザ側検索フォームに入力されたワード
      * return object 出力されるピックアップ広告
      */
    public function choose($keyword) {

        //キーワード分割
        $keyword = mb_convert_kana($keyword, "as", "UTF-8");
        $keywords = preg_split('/[\s]+/', $keyword, -1, PREG_SPLIT_NO_EMPTY);
        
        /**
         * 表示ロジック
         * キーワードの中からランダムでN(=10)個選び、その中で一番高いものを出す
         */
        $conditions = "status = :status: AND deleted = 0 ";
        $parameters = ["status" => 'publishing'];
        foreach($keywords as $key => $keyword){
            $conditions .= 'AND (message LIKE :keyword'.$key.': OR company_name LIKE :keyword'.$key.': OR job_category LIKE :keyword'.$key.':) ';
            $parameters['keyword'.$key] = '%' . $keyword . '%';
        }    
        
        $pickup_offer = new PickupOffer();

        $pickups = $pickup_offer->find([
            "conditions" => $conditions,
            "bind" => $parameters,
            "order" => "published_from DESC",
        ]);
                        
       return $this->setPickupData($pickups);
    }
            
}
