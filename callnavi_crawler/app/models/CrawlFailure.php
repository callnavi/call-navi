<?php

/**
 * SearchedQuery model
 *
 * @category    Model
 */
class CrawlFailure extends ModelBase {

    public $id;
    public $message;
    public $field;
    public $value;
    public $crawl_id;
    public $created;
    public $updated;
    public $deleted;

    /**
     * {@inheritdoc}
     */
    public function initialize() {
        parent::initialize();
        $this->setSource('crawl_failures');
    }

    public function validation() {
        
    }

    /**
     * {@inheritdoc}
     */
    protected function _setDefaultAttributes() {
        $this->id = null;
        $this->message = '';
        $this->field = '';
        $this->value = '';
        $this->crawl_id = 0;
        $this->created = '0000-00-00 00:00:00';
        $this->updated = '0000-00-00 00:00:00';
        $this->deleted = 0;
    }
      
       /**
        * クロール失敗時の記録をDBに格納
        * @param string $field 不適切な値が含まれたカラム
        * @param string $value 上記カラムの、実際の不適切な値
        * @param string $message エラーメッセージ
        * @param int $crawl_id
        * @return object DB格納後のオブジェクト
        */
    public function addFailure($field, $value, $message, $crawl_id) {
        
        $this->_setDefaultAttributes();
        $this->field = $field;
        $this->value = $value;
        $this->message = $message;
        $this->crawl_id = $crawl_id;
        $this->create();
    }

}
