<?php

/**
 * PaymentLog Model
 *
 * @category    Model
 */
class PaymentLog extends ModelBase {

    public $id;
    public $created;
    public $account_id;
    public $expense_sum;

    /**
     * {@inheritdoc}
     */
    public function initialize() {
        parent::initialize();
        $this->setSource('payment_logs');
    }

    /**
     * {@inheritdoc}
     */
    public function afterFetch() {
        // blank
    }

    public function validation() {
        
    }

    /**
     * {@inheritdoc}
     */
    protected function _setDefaultAttributes() {

        $this->id = null;
        $this->created = '0000-00-00 00:00:00';
        $this->account_id = 0;
        $this->expense_sum = 0;
        $this->creditcard_id = 0;
    }
     /**
     * 月次決済記録(payment_logテーブル)を生成
     * @param int $account_id アカウントのID
     * * @param int $expense_sum 当該アカウントの決済額合計 
     */
    public function addPaymentLog($account_id, $expense_sum) {
        
        $this->_setDefaultAttributes();
        $this->account_id = $account_id;
        $this->expense_sum = $expense_sum;
        $this->create();
        
    }
    
}
