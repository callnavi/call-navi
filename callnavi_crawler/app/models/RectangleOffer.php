<?php

/**
 * RectangleOffer Model
 *
 * @category    Model
 */
class RectangleOffer extends OfferBase {

use VariablesCalculatable;

use Mailable;

use WaitingOfferPublishable;

    public $id;
    public $created;
    public $updated;
    public $deleted;
    public $title;
    public $has_pic;
    public $click_price;
    public $budget_max;
    public $detail_url;
    public $accepted;
    public $allowed;
    public $status;
    public $account_id;
    public $impression_log;
    public $click_log;
    public $click_ratio;
    public $expense;
    public $impression_ratio;
    public $type;
    public $is_available;
    public $is_confirmed;
    public $without_card;
    

    /**
     * {@inheritdoc}
     */
    public function initialize() {
        parent::initialize();
        $this->setSource('rectangle_offers');
    }

    /**
     * {@inheritdoc}
     */
    public function afterFetch() {
        // blank
    }

    public function validation() {
        
    }

    /**
     * {@inheritdoc}
     */
    protected function _setDefaultAttributes() {

        $this->id = null;
        $this->created = '0000-00-00 00:00:00';
        $this->updated = '0000-00-00 00:00:00';
        $this->deleted = 0;
        $this->title = '';
        $this->has_pic = 0;
        $this->click_price = null;
        $this->budget_max = null;
        $this->detail_url = '';
        $this->accepted = '0000-00-00 00:00:00';
        $this->allowed = '0000-00-00 00:00:00';
        $this->status = '';
        $this->without_card = 0;
        $this->account_id = 0;
        $this->is_available = 1;
        $this->type = 'rectangle';  
        $this->is_confirmed = 0;
        
    }
     /**
      * レクタングル広告データの検索(データ生成時降順)
      * @param array $scope 対象期間
      * @param string $account_id 対象となるアカウントのID 全アカウントについて検索したいたい場合、求める検索条件に合わせてallやadminを代入
      * @return array 検索結果の配列(データ生成時降順)
      */
    public function findRectangleOffers($scope, $account_id) {

        if ($account_id == 'all') {
            $conditions = "status != 'judging' AND is_confirmed = '1'";
            $parameters = array();
        } elseif ($account_id == 'admin') {
            $conditions = "status = 'judging' AND deleted = '0' AND is_confirmed = '1'";
            $parameters = array();
        } else {
            $conditions = "account_id = :account_id: AND deleted = '0' AND is_confirmed = '1'";
            $parameters = array("account_id" => $account_id);
        }
        $offers = $this->find(array(
            "conditions" => $conditions,
            "bind" => $parameters,
            "order" => "created DESC"
        ));

        $output_offers = [];

        foreach ($offers as $offer) {

            $impression_log = new ImpressionLog;
            $offer->impression_log = $impression_log->countImpressionLog($offer, 'rectangle', $scope, false);
            $click_log = new ClickLog;
            $click_logs = $click_log->findClickLogs($offer, 'rectangle', $scope, false);
            $offer->click_log = count($click_logs);
            $offer->click_ratio = $this->calClickRatio($offer->impression_log, $offer->click_log);
            $offer->expense = $this->calExpense($click_logs);
            $offer->impression_ratio = $this->calImpressionRatio($offer->click_price, $offer->id);
            $offer->type = "rectangle";

            $account = new Account;
            $account_info = $account->findAccountById($offer->account_id);
            $offer->account = $account_info->company_name;
            $offer->creditcard_id = $account_info->creditcard_id;
            $offer->card_status = $account_info->card_status;
            $offer->type = "rectangle";
            $offer->job_category = "";
            $offer->company_name = "";
            $output_offers[] = $offer;
        }

        return $output_offers;
    }
     
     /**
      * レクタングル広告データの検索(受付時昇順)
      * @param array $scope 対象期間
      * @param string $account_id 対象となるアカウントのID 全アカウントについて検索したい場合、求める検索条件に合わせてallやadminを代入
      * @return array 検索結果の配列(受付時昇順)
      */
    public function findAcceptedRectangleOffers($scope, $account_id) {

        if ($account_id == 'all') {
            $conditions = "status != 'judging' AND status != 'editing' AND is_confirmed = '1'";
            $parameters = array();
        } elseif ($account_id == 'admin') {
            $conditions = "status = 'judging' AND deleted = '0' AND is_confirmed = '1'";
            $parameters = array();
        } else {
            $conditions = "account_id = :account_id: AND deleted = '0' AND is_confirmed = '1'";
            $parameters = array("account_id" => $account_id);
        }
        $offers = $this->find(array(
            "conditions" => $conditions,
            "bind" => $parameters,
            "order" => "accepted ASC"
        ));

        $output_offers = [];

        foreach ($offers as $offer) {

            $impression_log = new ImpressionLog;
            $offer->impression_log = $impression_log->countImpressionLog($offer, 'rectangle', $scope, false);
            $click_log = new ClickLog;
            $click_logs = $click_log->findClickLogs($offer, 'rectangle', $scope, false);
            $offer->click_log = count($click_logs);
            $offer->click_ratio = $this->calClickRatio($offer->impression_log, $offer->click_log);
            $offer->expense = $this->calExpense($click_logs);
            $offer->impression_ratio = $this->calImpressionRatio($offer->click_price, $offer->id);
            $offer->type = "rectangle";

            $account = new Account;
            $account_info = $account->findAccountById($offer->account_id);
            $offer->account = $account_info->company_name;
            $offer->creditcard_id = $account_info->creditcard_id;
            $offer->card_status = $account_info->card_status;
            $offer->type = "rectangle";
            $offer->job_category = "";
            $offer->company_name = "";
            $output_offers[] = $offer;
        }

        return $output_offers;
    } 

     /**
      *レクタングル広告の検索(承認時降順)
      * @param array $scope 対象期間
      * @param string $account_id 対象となるアカウントのID アカウントをまたいで表示させたい場合、求める検索条件に合わせてallやadminを代入
      * @return array 検索結果の配列(承認時降順)
      */
    public function findAllowedRectangleOffers($scope, $account_id) {

        if ($account_id == 'all') {
            $conditions = "status != 'judging' AND is_confirmed = '1'";
            $parameters = array();
        } elseif ($account_id == 'admin') {
            $conditions = "status != 'judging' AND AND status != 'editing' AND deleted = '0' AND is_confirmed = '1'";
            $parameters = array();
        } else {
            $conditions = "account_id = :account_id: AND deleted = '0' AND is_confirmed = '1'";
            $parameters = array("account_id" => $account_id);
        }
        $offers = $this->find(array(
            "conditions" => $conditions,
            "bind" => $parameters,
            "order" => "allowed DESC"
        ));

        $output_offers = [];

        foreach ($offers as $offer) {

            $impression_log = new ImpressionLog;
            $offer->impression_log = $impression_log->countImpressionLog($offer, 'rectangle', $scope, false);
            $click_log = new ClickLog;
            $click_logs = $click_log->findClickLogs($offer, 'rectangle', $scope, false);
            $offer->click_log = count($click_logs);
            $offer->click_ratio = $this->calClickRatio($offer->impression_log, $offer->click_log);
            $offer->expense = $this->calExpense($click_logs);
            $offer->impression_ratio = $this->calImpressionRatio($offer->click_price, $offer->id);
            $offer->type = "rectangle";

            $account = new Account;
            $account_info = $account->findAccountById($offer->account_id);
            $offer->account = $account_info->company_name;
            $offer->creditcard_id = $account_info->creditcard_id;
            $offer->card_status = $account_info->card_status;
            $offer->type = "rectangle";
            $offer->job_category = "";
            $offer->company_name = "";
            $output_offers[] = $offer;
        }

        return $output_offers;
    } 
     /**
      * レクタングル広告の検索(請求画面用、論理削除されたものも出力)
      * @param array $scope 対象期間
      * @param string $account_id 対象となるアカウントのID アカウントをまたいで表示させたい場合、求める検索条件に合わせてallやadminを代入
      * @return array 検索結果の配列
      */
    public function findRectangleOffersForCharge($scope, $account_id) {

        if ($account_id == 'all') {
            $conditions = "status != 'judging' AND is_confirmed = '1'";
            $parameters = array();
        } elseif ($account_id == 'admin') {
            $conditions = "status = 'judging' AND is_confirmed = '1'";
            $parameters = array();
        } else {
            $conditions = "account_id = :account_id: AND is_confirmed = '1'";
            $parameters = array("account_id" => $account_id);
        }
        $offers = $this->find(array(
            "conditions" => $conditions,
            "bind" => $parameters
        ));

        $output_offers = [];

        foreach ($offers as $offer) {

            $impression_log = new ImpressionLog;
            $offer->impression_log = $impression_log->countImpressionLog($offer, 'rectangle', $scope, false);
            $click_log = new ClickLog;
            $click_logs = $click_log->findClickLogs($offer, 'rectangle', $scope, false);
            $offer->click_log = count($click_logs);
            $offer->click_ratio = $this->calClickRatio($offer->impression_log, $offer->click_log);
            $offer->expense = $this->calExpense($click_logs);
            $offer->impression_ratio = $this->calImpressionRatio($offer->click_price, $offer->id);
            $offer->type = "rectangle";

            $account = new Account;
            $offer->account = $account->findAccountById($offer->account_id)->company_name;
            $offer->type = "rectangle";
            $offer->job_category = "";
            $offer->company_name = "";
            $output_offers[] = $offer;
        }

        return $output_offers;
    }

    /* public function findRectangleOffersForAdmin() {
      return $this->findRectangleOffers('admin');
      }

      public function findAllRectangleOffersForAdmin($scope) {
      return $this->mergeOffer($scope, 'all');
      } */

     /**
      * IDに基づきレクタングル広告レコードを検索(is_confirmedカラムが1のもの)
      * @param int $id 検索対象のピックアップ広告のID
      * return object レクタングル広告レコード(is_confirmedカラムが1のもの)
      */
    public function findRectangleOfferById($id) {

        $conditions = "id = :id: AND is_confirmed = '1'";
        $parameters = array("id" => $id);

        $offer = $this->findFirst(array(
            "conditions" => $conditions,
            "bind" => $parameters
        ));

        return $offer;
    }
      
      /**
      * IDに基づきレクタングル広告レコードを検索(is_confirmedカラムが0のもの)
      * @param int $id 検索対象のピックアップ広告のID
      * return object レクタングル広告レコード(is_confirmedカラムが0のもの)
      */
    public function findOnConfirmRectangleOfferById($id) {

        $conditions = "id = :id: AND deleted = '0'";
        $parameters = array("id" => $id);

        $offer = $this->findFirst(array(
            "conditions" => $conditions,
            "bind" => $parameters
        ));

        return $offer;
    }
      /**
      * 新規レクタングル広告レコードをDB(pickup_offersテーブル)に追加 statusカラムはediting(編集中)
      * @param aray $data 新規レクタングル広告データを格納した配列
      * return object 新規追加されたレクタングル広告レコード
      */
    public function addRectangleOffer($data) {

        $this->_setDefaultAttributes();
        $this->title = $data['title'];
        if ($data['click_price'] !== "") {
            $this->click_price = $data['click_price'];
        }

        if ($data['budget_max'] !== "") {
            $this->budget_max = $data['budget_max'];
        }

        $this->detail_url = $data['detail_url'];
        $this->status = 'editing';
        $this->account_id = $data['account_id'];
        $this->has_pic = $data['has_pic'];
        $this->is_confirmed = 1;
        $this->create();

        return $this;
    }

     /**
      * 出稿完了未了のレクタングル広告レコードを出稿完了済みにする(statusカラムをediting(編集中)からjudging(審査中)にする)
      * @param int $offer_id 対象広告のID
      * return object void
      */
    public function confirmRectangleOffer($offer_id) {
        $RectangleOffer = $this->findFirstById($offer_id);



        $RectangleOffer->is_confirmed = 1;

        $RectangleOffer->status = 'judging';

        $date = new DateTime();
        $today = $date->format('Y-m-d H:i:s');
        $RectangleOffer->accepted = $today;

        $RectangleOffer->update();
    }

     /**
      * 新規レクタングル広告レコードをDB(pickup_offersテーブル)に追加 statusカラムはediting(編集中)
      * @param aray $data 新規レクタングル広告データを格納した配列
      * return object 新規追加されたレクタングル広告レコード
      */
    public function saveRectangleOffer($data) {

        $this->_setDefaultAttributes();

        $this->title = $data['title'];
        $this->has_pic = $data['has_pic'];
        if ($data['click_price'] !== "") {
            $this->click_price = $data['click_price'];
        }

        if ($data['budget_max'] !== "") {
            $this->budget_max = $data['budget_max'];
        }
        $this->detail_url = $data['detail_url'];
        $this->status = 'editing';
        $this->account_id = $data['account_id'];
        $this->is_confirmed = 1;

        $this->create();

        return $this;
    }

     /**
      * 既に存在するレクタングル広告レコード(is_confirmedカラムは1)を更新
      * @param array $data 更新後レクタングル広告データを格納した配列
      * @param string $status 更新後のstatusカラム
      * return object 更新されたレクタングル広告レコード
      */
    public function updateRectangleOffer($data, $status) {

        $offer = $this->findRectangleOfferById($data['id']);

        $offer->title = $data['title'];
        $offer->has_pic = $data['has_pic'];

        if ($data['click_price'] !== "") {
            $offer->click_price = $data['click_price'];
        } else {
            $offer->click_price = null;
        }


        if ($data['budget_max'] !== "") {
            $offer->budget_max = $data['budget_max'];
        } else {
            $offer->budget_max = null;
        }
        $offer->detail_url = $data['detail_url'];

        $offer->status = $status;

        $offer->account_id = $data['account_id'];

        $offer->updateColumn();
        $offer->checkIsAvailable($offer->id);

        return $offer;
    }

     

     /**
      * レクタングル広告の表示頻度を出力
      * @param int $click_price　クリック単価
      * @param int $id 当該レクタングル広告のID
      * return int 表示頻度
      */
    public function calImpressionRatio($click_price, $id) {

        $conditions = "deleted = '0' AND (status = 'publishing' OR id = :id:) AND is_confirmed = '1'";
        $parameters = array(
            "id" => $id
        );

        $rectangle_offers = $this->find(array(
            "conditions" => $conditions,
            "bind" => $parameters
        ));

        $price_sum = 0;
        foreach ($rectangle_offers as $rectangle_offer) {

            $price_sum += $rectangle_offer->click_price;
        }

        $impression_ratio = ($click_price / $price_sum) * 100;

        return round($impression_ratio, 2);
    }
      /**
      * ユーザ側レクタングル広告の出力
      *
      * return void
      */
    public function choose() {

        /**
         * 表示ロジック
         * 全体の中からランダムでN(=10)個選び、その中で一番高いものを出す
         */
        $conditions = "status = :status: AND is_available = 1 AND deleted = 0";
        $parameters = array("status" => 'publishing');

        $offers = $this->find([
            "conditions" => $conditions,
            "bind" => $parameters,
            "limit" => 10,
            "order" => "rand()",
        ]);

        $highestPrice = 0;
        $highestPriceOffer = null;
        foreach ($offers as $offer) {
            if ($highestPrice < $offer->click_price) {
                $highestPriceOffer = $offer;
            }
        }

        return $highestPriceOffer;
    }  
     /**
      * クリック額が上限予算を超えていないかをチェックし、is_availableカラムを、超えていれば0,超えていなければ1とする
      * @param int $offer_id チェック対象となるレクタングル広告のID
      * return void
      */
    public function checkIsAvailable($offer_id) {

        $click_log = new ClickLog();

        $sumCosts = $click_log->getAllCosts($offer_id, 'rectangle');

        $offer = $this->findFirstById($offer_id);

        if ($offer->budget_max != NULL) {
            if ($offer->click_price) {
                $is_available = ($sumCosts + $offer->click_price <= $offer->budget_max) ? true : false;
                
            } else {
                $is_available = false;
            }
        } else {
            $is_available = true;
        }

        $offer->is_available = $is_available;
        $offer->updateColumn();
    }
     /**
      * レクタングル広告がクリックされた際、クリックログを生成(click_logsテーブル)
      * @param int $offer_id クリックされたレクタングル広告のID
      * return void
      */
    public function click($offer_id) {

        $click_price = $this->findFirstById($offer_id)->click_price;

        $click_log = new ClickLog();

        $click_log->_setDefaultAttributes();
        $click_log->offer_id = $offer_id;
        $click_log->offer_type = 'rectangle';
        $click_log->cost = $click_price;
        $click_log->create();
    }
     
     /**
      * レクタングル広告が表示された際、表示ログを生成(impression_logsテーブル)
      * @param int $offer_id 表示されたリステイング広告のID
      * return void
      */
    public function impression($offer_id) {

        $impression_log = new ImpressionLog();

        $impression_log->_setDefaultAttributes();
        $impression_log->offer_id = $offer_id;
        $impression_log->offer_type = 'rectangle';
        $impression_log->create();
    }
    
     /**
      * 入力されたワードに基づき、レクタングル広告レコードを検索
      * @param array $scope 対象期間
      * @param string $term 入力されたワード
      * return void
      */
    public function findRectangleOffersForSearch($scope, $term) {

        //@todo sql injection対策として、boundしてる
        $conditions = "status != 'judging' AND
            title LIKE '%" . $term . "%'
            ";

        $offers = $this->find(array(
            "conditions" => $conditions,  
            "order" => "allowed DESC",
        ));

        $output_offers = [];

        foreach ($offers as $offer) {

            $impression_log = new ImpressionLog;
            $offer->impression_log = $impression_log->countImpressionLog($offer, 'rectangle', $scope, false);
            $click_log = new ClickLog;
            $click_logs = $click_log->findClickLogs($offer, 'rectangle', $scope, false);
            $offer->click_log = count($click_logs);
            $offer->click_ratio = $this->calClickRatio($offer->impression_log, $offer->click_log);
            $offer->expense = $this->calExpense($click_logs);
            $offer->impression_ratio = $this->calImpressionRatio($offer->click_price, $offer->id);
            $offer->type = "rectangle";

            $account = new Account;
            $offer->account = $account->findAccountById($offer->account_id)->company_name;
            $offer->type = "rectangle";
            $offer->job_category = "";
            $offer->company_name = "";
            $output_offers[] = $offer;
        }

        return $output_offers;
    }
     
     /**
      * レクタングル広告受付完了メールを出稿者へ送信
      * @param int $offer_id 対象となるレクタングル広告のID
      * return function メール送信用の関数
      */
    public function sendThanksMail($offer_id) {

        $offer = $this->findFirstById($offer_id);
        $Account = new Account();
        $account = $Account->findAccountById($offer->account_id);

        $date = (object) [
                    'year' => date('Y'),
                    'month' => date('m'),
                    'day' => date('d'),
                    'time' => date('H:i'),
        ];

        $subject = '【コールナビ】求人広告受付完了のお知らせ';
        $path = 'rectangle/thanks';
        $params = [
            'date' => $date,
            'offer' => $offer,
            'account' => $account,
        ];
        return $this->sendMail($account->email, $subject, $path, $params);
    }
      /**
      * レクタングル広告受付完了メールを管理者者へ送信
      * @param int $offer_id 対象となるレクタングル広告のID
      * return function メール送信用の関数
      */
    public function sendThanksAdminMail($offer_id) {

        $offer = $this->findFirstById($offer_id);

        $subject = '【コールナビ】求人広告を受付いたしました';
        $path = 'rectangle/admin_thanks';

        $date = (object) [
                    'year' => date('Y'),
                    'month' => date('m'),
                    'day' => date('d'),
                    'time' => date('H:i'),
        ];

        $params = [
            'date' => $date,
            'offer' => $offer
        ];

        $mail_to = $this->admin_email;
        return $this->sendMail($mail_to, $subject, $path, $params);
    }
     /**
        * レクタングル広告のstatusをwaiting_card(カード登録待ち)からpublishing(掲載中)に変更
        * @param int $account_id アカウントのID
        * @return void
      */ 
    public function publishWaitingOffer($account_id) {

        $conditions = "account_id = :account_id: AND deleted = '0' AND status = 'waiting_card' AND is_confirmed = 1";
        $parameters = array(
            "account_id" => $account_id
        );

        $offers = $this->find(array(
            "conditions" => $conditions,
            "bind" => $parameters
        ));

        foreach ($offers as $offer) {

            $offer->status = 'publishing';
            $offer->updateColumn();
        }

        if (count($offers) > 0) {
            $AdminMailer = new AdminMailer();
            $AdminMailer->informPublishingMail($offers[0]->id, 'rectangle');
        }
    }
     /**
        * レクタングル広告のstatusをpublishing(掲載中)からwaiting_card(カード登録待ち)に変更
        * @param int $account_id アカウントのID
        * @return void
      */ 
    public function stopPublishingOffer($account_id) {

        $conditions = "account_id = :account_id: AND deleted = '0' AND (status = 'publishing' OR status = 'canceled') AND is_confirmed = 1 AND without_card = 0";
        $parameters = array(
            "account_id" => $account_id
        );

        $offers = $this->find(array(
            "conditions" => $conditions,
            "bind" => $parameters
        ));

        foreach ($offers as $offer) {

            $offer->status = 'waiting_card';
            $offer->updateColumn();
        }
    }
    
      /**
        * 上限予算到達して表示されなくなった(is_avairableカラムが0)レクタングル広告をリセット(is_avairableカラムを1に)
        * 
        * @return void
      */ 
    public function resetIsAvailable() {
        $conditions = "deleted = 0 AND is_available = 0";
                
        $offers = $this->find(array(
            "conditions" => $conditions,
        ));

        foreach ($offers as $offer) {

            $offer->is_available = 1;
            $offer->updateColumn();
        }
    }
}
