<?php

/**
 * ListingOffer Model
 *
 * @category    Model
 */
class ListingOffer extends OfferBase {

use VariablesCalculatable;

use Mailable;

use WaitingOfferPublishable;

    public $id;
    public $created;
    public $updated;
    public $deleted;
    public $title;
    public $job_category;
    public $hired_as;
    public $hired_as_array;
    public $company_name;
    public $salary_term;
    public $salary_value_min;
    public $salary_unit_min;
    public $salary_value_max;
    public $salary_unit_max;
    public $workplace_prefecture;
    public $workplace_area;
    public $workplce_address;
    public $job_description;
    public $features;
    public $features_array;
    public $has_pic;
    public $click_price;
    public $budget_max;
    public $detail_url;
    public $status;
    public $accepted;
    public $allowed;
    public $account_id;
    public $keyword;
    public $impression_log;
    public $click_log;
    public $click_ratio;
    public $expense;
    public $account;
    public $impression_ratio;
    public $type;
    public $is_confirmed;
    public $without_card;
    public $is_available;


    /**
     * {@inheritdoc}
     */
    public function initialize() {
        parent::initialize();
        $this->setSource('listing_offers');
    }

    /**
     * {@inheritdoc}
     */
    public function afterFetch() {
        // blank
    }

    public function validation() {

    }

    /**
     * {@inheritdoc}
     */
    protected function _setDefaultAttributes() {

        $this->id = null;
        $this->created = '0000-00-00 00:00:00';
        $this->updated = '0000-00-00 00:00:00';
        $this->deleted = '0';
        $this->title = '';
        $this->job_category = '';
        $this->hired_as = '';
        $this->company_name = '';
        $this->salary_term = '';
        $this->salary_value_min = '';
        $this->salary_unit_min = '';
        $this->salary_value_max = '';
        $this->salary_unit_max = '';
        $this->workplace_prefecture = '';
        $this->workplace_area = '';
        $this->workplce_address = '';
        $this->job_description = '';
        $this->features = '';
        $this->has_pic = 0;
        $this->click_price = null;
        $this->budget_max = null;
        $this->detail_url = '';
        $this->accepted = '0000-00-00 00:00:00';
        $this->allowed = '0000-00-00 00:00:00';
        $this->status = '';
        $this->without_card = 0;
        $this->account_id = 0;
        $this->type = 'listing';
        $this->is_confirmed = 0;


    }
      /**
      * リスティング広告データの検索(データ生成時降順)
      * @param array $scope 対象期間
      * @param string $account_id 対象となるアカウントのID 全アカウントについて検索したいたい場合、求める検索条件に合わせてallやadminを代入
      * @return array 検索結果の配列(データ生成時降順)
      */
    public function findListingOffers($scope, $account_id) {

        if ($account_id == 'all') {
            $conditions = "status != 'judging' AND is_confirmed = '1'";
            $parameters = array();
        } elseif ($account_id == 'admin') {
            $conditions = "status = 'judging' AND deleted = '0' AND is_confirmed = '1'";
            $parameters = array();
        } else {
            $conditions = "account_id = :account_id: AND deleted = '0' AND is_confirmed = '1'";
            $parameters = array("account_id" => $account_id);
        }
        $offers = $this->find(array(
            "conditions" => $conditions,
            "bind" => $parameters,
            "order" => "created DESC"
        ));

        $output_offers = [];

        foreach ($offers as $offer) {

            $listing_keyword = new ListingKeyword;
            $offer->keyword = $listing_keyword->countKeywords($offer->id);
            $first_keyword  = $listing_keyword->findListingKeywordByOfferId($offer->id);
            if (isset($first_keyword->id)) {
                $offer->is_available = $first_keyword->is_available;
                }
            $account = new Account;
            $account_info = $account->findAccountById($offer->account_id);
            $offer->account = $account_info->company_name;
            $offer->creditcard_id = $account_info->creditcard_id;
            $offer->card_status = $account_info->card_status;
            $impression_log = new ImpressionLog;
            $offer->impression_log = $impression_log->countImpressionLog($offer, 'listing', $scope, false);
            $click_log = new ClickLog;
            $click_logs = $click_log->findClickLogs($offer, 'listing', $scope, false);

            $offer->click_log = count($click_logs);
            $offer->click_ratio = $this->calClickRatio($offer->impression_log, $offer->click_log);

            $offer->expense = $this->calExpense($click_logs);
            $offer->impression_ratio = $this->calAverageImpressionRatio($scope, $offer->id, $offer->click_price);


            $offer->type = "listing";

            $output_offers[] = $offer;
        }

        return $output_offers;
    }



      /**
      * リスティング広告データの検索(受付時昇順)
      * @param array $scope 対象期間
      * @param string $account_id 対象となるアカウントのID 全アカウントについて検索したい場合、求める検索条件に合わせてallやadminを代入
      * @return array 検索結果の配列(受付時昇順)
      */
    public function findAcceptedListingOffers($scope, $account_id) {

        if ($account_id == 'all') {
            $conditions = "status != 'judging' AND status != 'editing' AND is_confirmed = '1'";
            $parameters = array();
        } elseif ($account_id == 'admin') {
            $conditions = "status = 'judging' AND deleted = '0' AND is_confirmed = '1'";
            $parameters = array();
        } else {
            $conditions = "account_id = :account_id: AND deleted = '0' AND is_confirmed = '1'";
            $parameters = array("account_id" => $account_id);
        }
        $offers = $this->find(array(
            "conditions" => $conditions,
            "bind" => $parameters,
            "order" => "accepted ASC"
        ));

        $output_offers = [];

        foreach ($offers as $offer) {

            $listing_keyword = new ListingKeyword;
            $offer->keyword = $listing_keyword->countKeywords($offer->id);
            $first_keyword  = $listing_keyword->findListingKeywordByOfferId($offer->id);
            if (isset($first_keyword->id)) {
                $offer->is_available = $first_keyword->is_available;
                }

            $account = new Account;
            $account_info = $account->findAccountById($offer->account_id);
            $offer->account = $account_info->company_name;
            $offer->creditcard_id = $account_info->creditcard_id;
            $offer->card_status = $account_info->card_status;
            $impression_log = new ImpressionLog;
            $offer->impression_log = $impression_log->countImpressionLog($offer, 'listing', $scope, false);
            $click_log = new ClickLog;
            $click_logs = $click_log->findClickLogs($offer, 'listing', $scope, false);
            $offer->click_log = count($click_logs);
            $offer->click_ratio = $this->calClickRatio($offer->impression_log, $offer->click_log);
            $offer->expense = $this->calExpense($click_logs);
            $offer->impression_ratio = $this->calAverageImpressionRatio($scope, $offer->id, $offer->click_price);
            $offer->type = "listing";

            $output_offers[] = $offer;
        }

        return $output_offers;
    }
      /**
      * リスティング広告の検索(承認時降順)
      * @param array $scope 対象期間
      * @param string $account_id 対象となるアカウントのID アカウントをまたいで表示させたい場合、求める検索条件に合わせてallやadminを代入
      * @return array 検索結果の配列(承認時降順)
      */
    public function findAllowedListingOffers($scope, $account_id) {

        if ($account_id == 'all') {
            $conditions = "status != 'judging'  AND  is_confirmed = '1'";
            $parameters = array();
        } elseif ($account_id == 'admin') {
            $conditions = "status = 'judging' AND deleted = '0' AND is_confirmed = '1'";
            $parameters = array();
        } else {
            $conditions = "account_id = :account_id: AND deleted = '0' AND is_confirmed = '1'";
            $parameters = array("account_id" => $account_id);
        }
        $offers = $this->find(array(
            "conditions" => $conditions,
            "bind" => $parameters,
            "order" => "allowed DESC"
        ));

        $output_offers = [];

        foreach ($offers as $offer) {

            $listing_keyword = new ListingKeyword;
            $offer->keyword = $listing_keyword->countKeywords($offer->id);
            $first_keyword  = $listing_keyword->findListingKeywordByOfferId($offer->id);
            if (isset($first_keyword->id)) {
                $offer->is_available = $first_keyword->is_available;
                }

            $account = new Account;
            $account_info = $account->findAccountById($offer->account_id);
            $offer->account = $account_info->company_name;
            $offer->creditcard_id = $account_info->creditcard_id;
            $offer->card_status = $account_info->card_status;

            $impression_log = new ImpressionLog;
            $offer->impression_log = $impression_log->countImpressionLog($offer, 'listing', $scope, false);
            $click_log = new ClickLog;
            $click_logs = $click_log->findClickLogs($offer, 'listing', $scope, false);
            $offer->click_log = count($click_logs);
            $offer->click_ratio = $this->calClickRatio($offer->impression_log, $offer->click_log);
            $offer->expense = $this->calExpense($click_logs);
            $offer->impression_ratio = $this->calAverageImpressionRatio($scope, $offer->id, $offer->click_price);
            $offer->type = "listing";

            $output_offers[] = $offer;
        }

        return $output_offers;
    }

      /**
      * リスティング広告の検索(請求画面用、論理削除されたものも出力)
      * @param array $scope 対象期間
      * @param string $account_id 対象となるアカウントのID アカウントをまたいで表示させたい場合、求める検索条件に合わせてallやadminを代入
      * @return array 検索結果の配列
      */
    public function findListingOffersForCharge($scope, $account_id) {

        if ($account_id == 'all') {
            $conditions = "status != 'judging' AND is_confirmed = '1'";
            $parameters = array();
        } elseif ($account_id == 'admin') {
            $conditions = "status = 'judging' AND is_confirmed = '1'";
            $parameters = array();
        } else {
            $conditions = "account_id = :account_id: AND is_confirmed = '1'";
            $parameters = array("account_id" => $account_id);
        }
        $offers = $this->find(array(
            "conditions" => $conditions,
            "bind" => $parameters
        ));

        $output_offers = [];

        foreach ($offers as $offer) {

            $listing_keyword = new ListingKeyword;
            $offer->keyword = $listing_keyword->countKeywords($offer->id);


            $account = new Account;
            $offer->account = $account->findAccountById($offer->account_id)->company_name;

            $impression_log = new ImpressionLog;
            $offer->impression_log = $impression_log->countImpressionLog($offer, 'listing', $scope, false);
            $click_log = new ClickLog;
            $click_logs = $click_log->findClickLogs($offer, 'listing', $scope, false);
            $offer->click_log = count($click_logs);
            $offer->click_ratio = $this->calClickRatio($offer->impression_log, $offer->click_log);
            $offer->expense = $this->calExpense($click_logs);
            $offer->impression_ratio = $this->calAverageImpressionRatio($scope, $offer->id, $offer->click_price);
            $offer->type = "listing";

            $output_offers[] = $offer;
        }

        return $output_offers;
    }

    /* public function findListingOffersForAdmin() {
      return $this->findListingOffers('admin');
      }

      public function findAllListingOffersForAdmin($scope) {
      return $this->mergeOffer($scope, 'all');
      }

      public function mergeListingOfferForAdmin($scope, $account_id) {
      return $this->mergeOffers($scope, $account_id);
      }
     */

     /**
      * IDに基づきリスティング広告レコードを検索(is_confirmedカラムが1のもの)
      * @param int $id 検索対象のリスティング広告のID
      * return object リスティング広告レコード(is_confirmedカラムが1のもの)
      */
    public function findListingOfferById($id) {

        $conditions = "id = :id: AND is_confirmed = '1'";
        $parameters = array("id" => $id);

        $offer = $this->findFirst(array(
            "conditions" => $conditions,
            "bind" => $parameters
        ));

        return $offer;
    }

     /**
      * IDに基づきリスティング広告レコードを検索(is_confirmedカラムが0のもの)
      * @param int $id 検索対象のリスティング広告のID
      * return object リステイング広告レコード(is_confirmedカラムが0のもの)
      */
    public function findOnConfirmListingOfferById($id) {

        $conditions = "id = :id: AND deleted = '0'";
        $parameters = array("id" => $id);

        $offer = $this->findFirst(array(
            "conditions" => $conditions,
            "bind" => $parameters
        ));

        return $offer;
    }

     /**
      * 新規リスティング広告レコードをDB(lsting_offersテーブル)に追加 statusカラムはjudging(審査中)
      * @param aray $data 新規リスティング広告データを格納した配列
      * return object 新規追加されたリスティング広告レコード
      */
    public function addListingOffer($data) {

        $this->_setDefaultAttributes();
        $this->title = $data['title'];
        $this->job_category = $data['job_category'];
        $this->hired_as = $data['hired_as_string'];

        $this->company_name = $data['company_name'];
        $this->salary_term = $data['salary_term'];
        $this->salary_value_min = $data['salary_value_min'];
        $this->salary_unit_min = $data['salary_unit_min'];
        $this->salary_unit_max = $data['salary_unit_min'];
        $this->salary_value_max = $data['salary_value_max'];
        $this->workplace_prefecture = $data['workplace_prefecture'];
        $this->workplace_area = $data['workplace_area'];
        $this->workplace_address = $data['workplace_address'];
        $this->job_description = $data['job_description'];
        $this->features = $data['features_string'];
        $this->has_pic = $data['has_pic'];
        $this->is_confirmed = 1;
        if ($data['click_price'] !== "") {
            $this->click_price = $data['click_price'];
        }

        if ($data['budget_max'] !== "") {
            $this->budget_max = $data['budget_max'];
        }
        $this->detail_url = $data['detail_url'];
        $this->status = 'editing';
        $this->account_id = $data['account_id'];

        $this->create();

        return $this;
    }

      /**
      * 出稿完了未了のリスティング広告レコードを出稿完了済みにする(is_confirmedカラムを0から1にする)
      * @param int $offer_id 対象広告のID
      * return object void
      */
    public function confirmListingOffer($offer_id) {
        $ListingOffer = $this->findFirstById($offer_id);



        $ListingOffer->is_confirmed = 1;
        $ListingOffer->status = 'judging';
        $date = new DateTime();
        $today = $date->format('Y-m-d H:i:s');
        $ListingOffer->accepted = $today;
        $ListingOffer->update();
    }
     /**
      * 新規リスティング広告レコードをDB(lsting_offersテーブル)に追加 statusカラムはediting(編集中)
      * @param aray $data 新規リスティング広告データを格納した配列
      * return object 新規追加されたリスティング広告レコード
      */
    public function saveListingOffer($data) {

        $this->_setDefaultAttributes();
        $this->title = $data['title'];
        $this->job_category = $data['job_category'];
        $this->hired_as = $data['hired_as_string'];

        $this->company_name = $data['company_name'];
        $this->salary_term = $data['salary_term'];
        $this->salary_value_min = $data['salary_value_min'];
        $this->salary_unit_min = $data['salary_unit_min'];
        $this->salary_unit_max = $data['salary_unit_min'];
        $this->salary_value_max = $data['salary_value_max'];
        $this->workplace_prefecture = $data['workplace_prefecture'];
        $this->workplace_area = $data['workplace_area'];
        $this->workplace_address = $data['workplace_address'];
        $this->job_description = $data['job_description'];
        $this->features = $data['features_string'];
        $this->has_pic = $data['has_pic'];
        if ($data['click_price'] !== "") {
            $this->click_price = $data['click_price'];
        }

        if ($data['budget_max'] !== "") {
            $this->budget_max = $data['budget_max'];
        }
        $this->detail_url = $data['detail_url'];
        $this->status = 'editing';
        $this->account_id = $data['account_id'];
        $this->is_confirmed = 1;

        $this->create();

        return $this;
    }
      /**
      * 既に存在するリスティング広告レコード(is_confirmedカラムは1)を更新
      * @param array $data 更新後リスティング広告データを格納した配列
      * @param string $status 更新後のstatusカラム
      * return object 更新されたリスティング広告レコード
      */
    public function updateListingOffer($data, $status) {

        $this->_setDefaultAttributes();

        $conditions = "deleted = '0' AND id = :id:";
        $paramerters = array(
            "id" => $data['id']
        );
        $offer = $this->findFirst(array(
            "conditions" => $conditions,
            "bind" => $paramerters
        ));

        $offer->title = $data['title'];
        $offer->job_category = $data['job_category'];
        $offer->hired_as = $data['hired_as_string'];

        $offer->company_name = $data['company_name'];
        $offer->salary_term = $data['salary_term'];
        $offer->salary_value_min = $data['salary_value_min'];
        $offer->salary_unit_min = $data['salary_unit_min'];
        $offer->salary_unit_max = $data['salary_unit_min'];
        $offer->salary_value_max = $data['salary_value_max'];
        $offer->workplace_prefecture = $data['workplace_prefecture'];
        $offer->workplace_area = $data['workplace_area'];
        $offer->workplace_address = $data['workplace_address'];
        $offer->job_description = $data['job_description'];
        $offer->features = $data['features_string'];
        $offer->has_pic = $data['has_pic'];
        if ($data['click_price'] !== "") {
            $offer->click_price = $data['click_price'];
        } else {
            $offer->click_price = null;
        }

        if ($data['budget_max'] !== "") {
            $offer->budget_max = $data['budget_max'];
        } else {
            $offer->budget_max = null;
        }
        $offer->detail_url = $data['detail_url'];
        $offer->status = $status;
        $offer->account_id = $data['account_id'];

        $offer->updateColumn();
        $offer->checkIsAvailable($offer->id);

        return $offer;
    }



     /**
      * 平均表示頻度を出力
      * @param array $scope 対象期間
      * @param int $offer_id リスティング広告のID
      * @param int $click_price クリック単価
      * return int 平均表示頻度
      */
    public function calAverageImpressionRatio($scope, $offer_id, $click_price) {
        //全キーワードにつき表示頻度を計算した上で、それらの平均を計算
        $listing_keyword = new ListingKeyword;

        $keywords = $listing_keyword->findKeywordsById($scope, $offer_id);

        $keywords_phrase = [];

        foreach ($keywords as $keyword) {

            $each_phrases = explode("\n", $keyword->keywords);

            foreach ($each_phrases as $each_phrase) {
                $keywords_phrase[] = $each_phrase;
            }
        }

        $impression_ratioes = [];

        foreach ($keywords_phrase as $keyword_phrase) {
            $impression_ratioes[] = $listing_keyword->calImpressionRatio($keyword_phrase, $click_price, $offer_id);
        }

        $impression_ratio_sum = array_sum($impression_ratioes);
        $impression_ratio_count = count($impression_ratioes);

        if ($impression_ratio_count > 0) {

            $average_impression_ratio = $impression_ratio_sum / $impression_ratio_count;

            return round($average_impression_ratio, 2);
        } else {
            return "error";
        }


    }
     /**
      * 入力されたワードに基づき、リスティング広告を出力
      * @param string $searchdKeyword　ユーザ側検索フォームに入力されたワード
      * return object 出力されるリスティング広告
      */
    public function choose($searchKeyword) {

        //キーワード分割
        $searchKeyword = mb_convert_kana($searchKeyword, "as", "UTF-8");
        $searchKeywords = preg_split('/[\s]+/', $searchKeyword, -1, PREG_SPLIT_NO_EMPTY);

        /**
         * 表示ロジック
         * キーワードの中からランダムでN(=10)個選び、その中で一番高いものを出す
         */
        $conditions = "status = :status: AND is_available = 1 AND deleted = 0 ";
        $parameters = ["status" => 'publishing'];
        if (count($searchKeywords) == 1) {
            $conditions .= 'AND keywords = :keyword: ';
            $parameters['keyword'] = $searchKeyword;
        } else {
            foreach($searchKeywords as $key => $keyword){
                $conditions .= 'AND keywords LIKE :keyword'.$key.': ';
                $parameters['keyword'.$key] = '%' . $keyword . '%';
            }

            // 「カスタマーサポート　東京都」を検索する際に「カスタマーサポート　東京都　転職なし」も引っかかる問題を防ぐため
            // キーワードの長さも検索条件にする
            $conditions .= 'AND CHAR_LENGTH(keywords) = CHAR_LENGTH(:keyword_len:) ';
            $parameters['keyword_len'] = $searchKeyword;
        }

        $listing_keyword = new ListingKeyword();

        $keywords = $listing_keyword->find([
            "conditions" => $conditions,
            "bind" => $parameters,
            "limit" => 10,
            "order" => "rand()",
        ]);

        $new_keywords = [];
        foreach($keywords as $key => $keyword){
            $target_keyword = mb_convert_kana($keyword->keywords, "as", "UTF-8");
            $target_keywords = preg_split('/[\s]+/', $target_keyword, -1, PREG_SPLIT_NO_EMPTY);
            // 検索キーワードと広告のキーワードが合致するもののみ対象にする
            $diff1 = array_diff($searchKeywords, $target_keywords);
            $diff2 = array_diff($target_keywords, $searchKeywords);
            if (empty($diff1) && empty($diff2)) {
                $new_keywords[] = $keyword;
            }
        }

        $highestPrice = 0;
        $highestPriceOffer = null;
        foreach ($new_keywords as $keyword) {
            //@todo　要リファクタリング
            $listing_offer = $this->findFirstById($keyword->offer_id);
            if ($highestPrice < $listing_offer->click_price) {
                $highestPriceOffer = $listing_offer;
                $highestPriceOffer->listing_keyword_id = $keyword->id;
            }
        }

        return $this->setListingData($highestPriceOffer);
    }

     /**
      * クリック額が上限予算を超えていないかをチェックし、対象リスティング広告に対応するlisting_keywordsのis_availableカラムを、超えていれば0,超えていなければ1とする
      * @param int $offer_id チェック対象となるリスティング広告のID
      * return void
      */
    public function checkIsAvailable($offer_id) {
        $click_log = new ClickLog();

        $sumCosts = $click_log->getAllCosts($offer_id, 'listing');

        $offer = $this->findFirstById($offer_id);

        if ($offer->budget_max != NULL) {
            if ($offer->click_price) {
                $is_available = ($sumCosts + $offer->click_price <= $offer->budget_max) ? true : false;

            } else {
                $is_available = false;
            }
        } else {
            $is_available = true;
        }

        $listing_keywords = new ListingKeyword();

        $conditions = "offer_id = :offer_id: ";
        $parameters = array("offer_id" => $offer_id);

        $keywords = $listing_keywords->find([
            "conditions" => $conditions,
            "bind" => $parameters,
        ]);

        foreach ($keywords as $keyword) {
            $keyword->is_available = $is_available;
            $keyword->updateColumn();
        }
    }
     /**
      * リスティング広告がクリックされた際、クリックログを生成(click_logsテーブル)
      * @param int $offer_id クリックされたリスティング広告のID
      * return void
      */
    public function click($offer_id, $listing_keyword_id) {

        $click_price = $this->findFirstById($offer_id)->click_price;

        $click_log = new ClickLog();

        $click_log->_setDefaultAttributes();
        $click_log->offer_id = $offer_id;
        $click_log->offer_type = 'listing';
        $click_log->cost = $click_price;
        $click_log->listing_keyword_id = $listing_keyword_id;
        $click_log->create();
    }

     /**
      * リスティング広告が表示された際、表示ログを生成(impression_logsテーブル)
      * @param int $offer_id 表示されたリステイング広告のID
      * @param int $listing_keyword_id 表示に用いられた検索キーワードのID
      * return void
      */
    public function impression($offer_id, $listing_keyword_id) {

        $impression_log = new ImpressionLog();

        $impression_log->_setDefaultAttributes();
        $impression_log->offer_id = $offer_id;
        $impression_log->offer_type = 'listing';
        $impression_log->listing_keyword_id = $listing_keyword_id;
        $impression_log->create();
    }
     /**
      * 入力されたワードに基づき、リスティング広告レコードを検索
      * @param array $scope 対象期間
      * @param string $term 入力されたワード
      * return void
      */
    public function findListingOffersForSearch($scope, $term) {

        //@todo sql injection対策として、boundしてる
        $conditions = "status != 'judging' AND (
            title LIKE '%" . $term . "%'
            OR job_category LIKE '%" . $term . "%'
            OR company_name LIKE '%" . $term . "%'
            )";

        $offers = $this->find(array(
            "conditions" => $conditions,
            "order" => "allowed DESC",
        ));

        $output_offers = [];

        foreach ($offers as $offer) {

            $listing_keyword = new ListingKeyword;
            $offer->keyword = $listing_keyword->countKeywords($offer->id);
            $first_keyword  = $listing_keyword->findListingKeywordByOfferId($offer->id);
            $offer->is_available = $first_keyword->is_available;

            $account = new Account;
            $offer->account = $account->findAccountById($offer->account_id)->company_name;

            $impression_log = new ImpressionLog;
            $offer->impression_log = $impression_log->countImpressionLog($offer, 'listing', $scope, false);
            $click_log = new ClickLog;
            $click_logs = $click_log->findClickLogs($offer, 'listing', $scope, false);
            $offer->click_log = count($click_logs);
            $offer->click_ratio = $this->calClickRatio($offer->impression_log, $offer->click_log);
            $offer->expense = $this->calExpense($click_logs);
            $offer->impression_ratio = $this->calAverageImpressionRatio($scope, $offer->id, $offer->click_price);
            $offer->type = "listing";

            $output_offers[] = $offer;
        }

        return $output_offers;
    }
     /**
        * リスティング広告のstatusをwaiting_card(カード登録待ち)からpublishing(掲載中)に変更
        * @param int $account_id アカウントのID
        * @return void
      */
    public function publishWaitingOffer($account_id) {

        $conditions = "account_id = :account_id: AND deleted = '0' AND status = 'waiting_card' AND is_confirmed = 1";
        $parameters = array(
            "account_id" => $account_id
        );

        $offers = $this->find(array(
            "conditions" => $conditions,
            "bind" => $parameters
        ));

        foreach ($offers as $offer) {

            $offer->status = 'publishing';
            $offer->updateColumn();

            $listing_keyword = new ListingKeyword;
            $listing_keyword->publishKeywords($offer->id);
        }

        if (count($offers) > 0) {
            $AdminMailer = new AdminMailer();
            $AdminMailer->informPublishingMail($offers[0]->id, 'listing');
        }
    }
     /**
        * リスティング広告のstatusをpublishing(掲載中)からwaiting_card(カード登録待ち)に変更
        * @param int $account_id アカウントのID
        * @return void
      */
    public function stopPublishingOffer($account_id) {

        $conditions = "account_id = :account_id: AND deleted = '0' AND (status = 'publishing' OR status = 'canceled') AND is_confirmed = 1 AND without_card = 0";
        $parameters = array(
            "account_id" => $account_id
        );

        $offers = $this->find(array(
            "conditions" => $conditions,
            "bind" => $parameters
        ));

        foreach ($offers as $offer) {

            $offer->status = 'waiting_card';
            $offer->updateColumn();

            $listing_keyword = new ListingKeyword;
            $listing_keyword->stopKeywords($offer->id);
        }
    }

     /**
      * リスティング広告受付完了メールを出稿者へ送信
      * @param int $offer_id 対象となるリスティング広告のID
      * return function メール送信用の関数
      */
    public function sendThanksMail($offer_id) {

        $offer = $this->findFirstById($offer_id);
        $Account = new Account();
        $account = $Account->findAccountById($offer->account_id);

        $date = (object) [
                    'year' => date('Y'),
                    'month' => date('m'),
                    'day' => date('d'),
                    'time' => date('H:i'),
        ];

        $subject = '【コールナビ】求人広告受付完了のお知らせ';
        $path = 'listing/thanks';
        $params = [
            'date' => $date,
            'offer' => $offer,
            'account' => $account,
        ];
        return $this->sendMail($account->email, $subject, $path, $params);
    }

     /**
      * リスティング広告受付完了メールを管理者へ送信
      * @param int $offer_id 対象となるリスティング広告のID
      * return function メール送信用の関数
      */
    public function sendThanksAdminMail($offer_id) {

        $offer = $this->findFirstById($offer_id);

        $subject = '【コールナビ】求人広告を受付いたしました';
        $path = 'listing/admin_thanks';

        $date = (object) [
                    'year' => date('Y'),
                    'month' => date('m'),
                    'day' => date('d'),
                    'time' => date('H:i'),
        ];

        $params = [
            'date' => $date,
            'offer' => $offer
        ];
        $mail_to = $this->admin_email;

        return $this->sendMail($mail_to, $subject, $path, $params);
    }

//    public function informPublishingMail($offer_id) {
//
//        $ListingOffer = new ListingOffer();
//        $offer = $ListingOffer->findFirstById($offer_id);
//        $offer_type_display = 'リスティング';
//
//        $Account = new Account();
//        $account = $Account->findAccountById($offer->account_id);
//
//        $date = (object) [
//            'year' => date('Y'),
//            'month' => date('m'),
//            'day' => date('d'),
//            'time' => date('H:i'),
//        ];
//
//        $subject = '【コールナビ】求人広告掲載開始のお知らせ';
//        $path = 'admin/publishing';
//        $params = [
//            'date' => $date,
//            'account' => $account,
//            'offer_type' => $offer_type_display,
//        ];
//        return $this->sendMail($this->admin_email, $subject, $path, $params);
//    }
//
//    public function informWaitingCreditMail($offer_id) {
//
////        $ListingOffer = new ListingOffer();
////        $offer = $ListingOffer->findFirstById($offer_id);
////        $offer_type_display = 'リスティング';
////
////        $Account = new Account();
////        $account = $Account->findAccountById($offer->account_id);
////
////        $date = (object) [
////            'year' => date('Y'),
////            'month' => date('m'),
////            'day' => date('d'),
////            'time' => date('H:i'),
////        ];
////
////        $link = 'http://' . $_SERVER['SERVER_NAME'] . '/login/index';
//
//        $subject = '【コールナビ】クレジットカード情報登録のご案内';
//        $path = 'admin/test';
//
////        $params = [
////            'date' => $date,
////            'account' => $account,
////            'link' => $link,
////            'offer_type' => $offer_type_display,
////        ];
//        $params = ['a' => 'a'];
//        return $this->sendMail($this->admin_email, $subject, $path, $params);
//    }

     /**
      * ユーザ側表示用に、リスティング広告レコードを加工
      * @param array $listing 加工前のリスティング広告レコード
      * return array 加工後のリスティング広告レコード
      */
    public function setListingData($listing) {

        if (isset($listing->id)) {

            if ($listing->salary_term == 'year') {
                $listing->salary_term_display = '年収';
            } elseif ($listing->salary_term == 'month') {
                $listing->salary_term_display = '月給';
            } elseif ($listing->salary_term == 'day') {
                $listing->salary_term_display = '日給';
            } elseif ($listing->salary_term == 'hour') {
                $listing->salary_term_display = '時給';
            }

            if ($listing->salary_unit_min == 'man_yen') {
                $listing->salary_unit_min_display = '万円';
            } elseif ($listing->salary_unit_min == 'yen') {
                $listing->salary_unit_min_display = '円';
            } elseif ($listing->salary_unit_min == 'dollar') {
                $listing->salary_unit_min_display = '$';
            }

            if ($listing->salary_unit_max == 'man_yen') {
                $listing->salary_unit_max_display = '万円';
            } elseif ($listing->salary_unit_max == 'yen') {
                $listing->salary_unit_max_display = '円';
            } elseif ($listing->salary_unit_max == 'dollar') {
                $listing->salary_unit_max_display = '$';
            }

            $listing->features_array = explode(':', $listing->features);
            $listing->hired_as_array = explode(':', $listing->hired_as);
            return $listing;
        } else {
            return null;
        }
    }

}
