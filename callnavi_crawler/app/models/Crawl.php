<?php

/**
 * SearchedQuery model
 *
 * @category    Model
 */
class Crawl extends ModelBase {

    public $id;
    public $url;
    public $created;
    public $updated;
    public $deleted;

    /**
     * {@inheritdoc}
     */
    public function initialize() {
        parent::initialize();
        $this->setSource('crawls');
    }

    public function validation() {
        
    }

    /**
     * {@inheritdoc}
     */
    protected function _setDefaultAttributes() {
        $this->id = null;
        $this->url = '';
        $this->created = '0000-00-00 00:00:00';
        $this->updated = '0000-00-00 00:00:00';
        $this->deleted = 0;
    }
      /**
     * クロールした求人のurlをDBに格納
     * @parm string $url　クロールした求人のurl 
     * @return object DBに作成されたレコード　　　　　　　　　　　　　　　　　
     */
    public function recordCrawling($url) {
        $this->_setDefaultAttributes();
        $this->url = $url;
        return $this->create();
    }

}
