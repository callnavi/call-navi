<?php

/**
 * UnauthorizedOffer Model
 *
 * @category    Model
 */
class UnauthorizedOffer extends ModelBase {

    use Mailable;

    public $id;
    public $created;
    public $updated;
    public $offer_id;
    public $reason01;
    public $reason02;
    public $reason03;
    public $reason04;
    public $reason05;
    public $reason06;
    public $comment;

    /**
     * {@inheritdoc}
     */
    public function initialize() {
        parent::initialize();
        $this->setSource('unauthorized_offers');
    }

    /**
     * {@inheritdoc}
     */
    public function afterFetch() {
        // blank
    }

    public function validation() {
        
    }

    /**
     * {@inheritdoc}
     */
    protected function _setDefaultAttributes() {

        $this->id = null;
        $this->created = '0000-00-00 00:00:00';
        $this->updated = '0000-00-00 00:00:00';
        $this->offer_id = '0';
        $this->reason01 = '0';
        $this->reason02 = '0';
        $this->reason03 = '0';
        $this->reason04 = '0';
        $this->reason05 = '0';
        $this->reason06 = '0';
        $this->comment = '';
    }

     /**
        * 非承認理由レコードを生成(unauthorized_offersテーブル)
        * @param array $data 非承認理由
        * @return object 生成された非承認理由レコード
      */ 
    public function resisterUnauthorizedReasons($data) {

        $this->_setDefaultAttributes();

        $this->offer_id = $data['offer_id'];
        $this->reason01 = $data['reason01'];
        $this->reason02 = $data['reason02'];
        $this->reason03 = $data['reason03'];
        $this->reason04 = $data['reason04'];
        $this->reason05 = $data['reason05'];
        $this->reason06 = $data['reason06'];
        $this->comment = $data['comment'];

        $this->create();

        return $this;
    }
     /**
        * 非承認メールを出稿者に送信
        * @param int $offer_id 対象広告ID
        * @param string $offer_type 対象広告種類
        * @param array $reason 非承認理由
        * @return object 生成された非承認理由レコード
      */ 
    public function sendUnauthorizedMail($offer_id, $offer_type, $reasons) {

        if($offer_type == 'listing') {
            $listing_offer = new ListingOffer();
            $offer = $listing_offer->findListingOfferById($offer_id);
            $offer_type_display = 'リスティング';
        } elseif($offer_type == 'rectangle') {
            $rectangle_offer = new RectangleOffer();
            $offer = $rectangle_offer->findRectangleOfferById($offer_id);
            $offer_type_display = 'レクタングル';
        } elseif ($offer_type == 'pickup') {
            $pickup_offer = new PickupOffer();
            $offer = $pickup_offer->findPickupOfferById($offer_id);
            $offer_type_display = 'ピックアップ';
        }    
           else {
               $free_offer = new FreeOffer();
               $offer = $free_offer->findFreeOfferById($offer_id);
               $offer_type_display = '無料';
        } 
        

        $Account = new Account();
        $account = $Account->findAccountById($offer->account_id);

        $date = (object) [
            'year' => date('Y'),
            'month' => date('m'),
            'day' => date('d'),
            'time' => date('H:i'),
        ];

        $link = 'http://' . $_SERVER['SERVER_NAME'] . '/login/index';

        $subject = '【コールナビ】求人広告内容の非承認のお知らせ';
        $path = 'admin/unauthorized';
        $params = [
            'date' => $date,
            'account' => $account,
            'link' => $link,
            'offer' => $offer,
            'offer_type' => $offer_type_display,
            'reason01' => $reasons['reason01'],
            'reason02' => $reasons['reason02'],
            'reason03' => $reasons['reason03'],
            'reason04' => $reasons['reason04'],
            'reason05' => $reasons['reason05'],
            'reason06' => $reasons['reason06'],
            'comment' => $reasons['comment'],
        ];
        return $this->sendMail($account->email, $subject, $path, $params);
    }
}
