    <?php

require_once __DIR__ . '/../../util/Utils.php';

class TestTask extends \Phalcon\CLI\Task {
    use Mailable;
    private $targetEnv = '';
    public $startTime;

    public function beforeExecuteRoute($dispatcher) {
        Phalcon\DI::getDefault()->getLogger()->info("Crawler start : " . date( "Y/m/d (D) H:i:s", time() ));
        $this->startTime = Utils::getMicrotimeFloat();
    }

    public function afterExecuteRoute($dispatcher) {
        $endTime = Utils::getMicrotimeFloat();
        $time = $endTime - $this->startTime;
        Phalcon\DI::getDefault()->getLogger()->info("$time seconds");

        Phalcon\DI::getDefault()->getLogger()->info("Crawler finished : " . date( "Y/m/d (D) H:i:s", time() ));
    }

    public function mainAction() {
        echo "\nThis is the default task and the default action \n";
        echo $_SERVER['SERVER_NAME'];
        echo $_SERVER['SERVER_ADDR'];
    }
        /**
     * クローラー処理
     * @param array $params 第一引数で広告媒体の種類を指定(必須)
     */
    public function crawlAction(array $params = null) {

        for ($i=0; $i < count($params); $i++) {
            Phalcon\DI::getDefault()->getLogger()->info("params[$i] : $params[$i]");
        }

        if (empty($params)) {
            Phalcon\DI::getDefault()->getLogger()->error("Params are required");
            exit(1);
        }

        if (!in_array($params[0], (array)$this->crawlerConfig->site)) {
            Phalcon\DI::getDefault()->getLogger()->error("invalid param : $params[0]");
            exit(1);
        }

        if (isset($params[1])) {
            Phalcon\DI::getDefault()->getLogger()->info("Target env : " . $params[1]);
            $this->targetEnv = strtolower($params[1]);
        }

        $crawlerConfig = $this->crawlerConfig->toArray();

        $siteNames = $crawlerConfig['siteNames'];

        $target = $params[0];
        $modelPrefix = ucfirst($target);
        $siteId = $crawlerConfig[$target]['siteId'];


        Phalcon\DI::getDefault()->getLogger()->info("Target : $target");
        //AWSから削除すべきデータのdeletedを1にする
        $this->useModel('Offer');
        $this->Offer->changeToDelete($siteId);

        Phalcon\DI::getDefault()->set('targetCrawlerConfig', $this->crawlerConfig->$target);

        $targetModel = $modelPrefix . 'Crawler';
        $this->useModel($targetModel);
       // $this->$targetModel->crawl();

        Phalcon\DI::getDefault()->getLogger()->info($siteNames[$siteId] . "の取得件数 : " . $this->$targetModel->saveElementCnt);

	if ($siteId == 8) {
		$this->useModel('CloudSearch');
		$this->CloudSearch->_deletePreviousOffers($siteId);
		exit('test ok');

	}

        if ($this->$targetModel->saveElementCnt > 0) {
            //store data to aws
            $this->useModel('CloudSearch');
            $this->CloudSearch->refresh($siteId);

            // deletedが1のものを削除する
            $this->Offer->deleteBySiteId($siteId);

            //件数をクローラーサーバーに記録する
            $this->useModel('Offer');
            $this->Offer->recordOffersToFile();
        } else if ($siteId != '11') {
            // 無料広告ではなく取得件数が0件の場合、メールで通報
            mb_language('ja');
            mb_internal_encoding('UTF-8');
            $to = $this->crawlerConfig->mail_addr_for_result_reporting;
            $mailTimeStamp = date( "Y/m/d (D) H:i:s", Utils::getMicrotimeFloat() );
            $subject = $siteNames[$siteId] ."のクローラーの取得件数が0件です。 ({$mailTimeStamp})";
            $message = "取得件数 : " . $this->$targetModel->saveElementCnt . "\nsite_id = " . $siteId;
            $additionalHeaders = "From:info@callnavi.jp\n";

            if (mb_send_mail($to, $subject, $message, $additionalHeaders)) {
                Phalcon\DI::getDefault()->getLogger()->info("sent to result mail");
            } else {
                Phalcon\DI::getDefault()->getLogger()->info("failed send to result mail");
            }

            Phalcon\DI::getDefault()->getLogger()->warning("*** site_id : $siteId // クローリングによる取得データがありません。 : " . $this->$targetModel->saveElementCnt . " ***");
        }
    }

    /**
     * ピックアップ広告関連
     * 上限予算感解除
     * 毎朝４時
     */
    public function updatePickupAction() {
          //ピックアップ広告のステータスを更新
        $this->useModel('PickupOfferStatus');
        $this->PickupOfferStatus->complete();


        //リスティング、レクタングルの上限チェックの解除
        $this->useModel('ListingKeyword');
        $this->ListingKeyword->resetIsAvailable();

        $this->useModel('RectangleOffer');
        $this->RectangleOffer->resetIsAvailable();

    }
//


    /**
     * Webpayによる請求
     * 毎月25日朝４時
     *
     */
    public function webpayAction() {

        $this->useModel('Account');
        $accounts = $this->Account->getAllAccounts();
        $this->useModel('Pay');
        foreach ($accounts as $account) {
            $this->Pay->charge($account);
        }
    }

    //10分毎の、承認確認処理
    public function validateCardAction() {
        $this->useModel('Account');
        $accounts = $this->Account->getAccountsForApproval();
        $this->useModel('Pay');
        foreach ($accounts as $account) {
            try {
                 $charge = $this->Pay->chargeForTest($account);
                 $account->card_status = "valid";
                 $this->useModel('ListingOffer');
                 $this->ListingOffer->publishWaitingOffer($account->id);
                 $this->useModel('RectangleOffer');
                 $this->RectangleOffer->publishWaitingOffer($account->id);
                 $this->useModel('PickupOffer');
                 $this->PickupOffer->publishWaitingOffer($account->id);

                 echo "charged";
                    // API リクエスト
                } catch (\WebPay\ErrorResponse\ErrorResponseException $e) {
                    $error = $e->data->error;
                    switch ($error->causedBy) {
                        case 'buyer':
                            echo 'buyer';
                            // カードエラーなど、購入者に原因がある
                            // エラーメッセージをそのまま表示するのがわかりやすい
                            $account->card_status = "invalid";
                            $account->creditcard_id = "";
                            $this->useModel('Account');
                            $this->Account->sendInvalidMail($account);
                            break;
                        case 'insufficient':
                            echo 'insufficient';
                            // 実装ミスに起因する
                            $account->card_status = 'before_approval';
                            break;
                        case 'missing':
                            echo 'missing';
                            // リクエスト対象のオブジェクトが存在しない
                            $account->card_status = "invalid";
                            $account->creditcard_id = "";
                            $this->useModel('Account');
                            $this->Account->sendInvalidMail($account);
                            break;
                        case 'service':
                            echo 'service';
                            // WebPayに起因するエラー
                            $account->card_status ='before_approval';
                            break;
                        default:
                            // 未知のエラー
                            echo 'unknown';
                            $account->card_status ='before_approval';
                            break;
                    }
                } catch (\WebPay\ApiException $e) {
                    // APIからのレスポンスが受け取れない場合。接続エラーなど
                    echo 'not_received';
                    $account->card_status ='before_approval';
                    break;
                }
                        /**
                    * 仮課金が行われた場合、ループ処理により返金が確実に行われるようにする
                    *
                    */
                if (isset($charge)) {
                    $refunded = 0;
                    while ($refunded == 0) {
                    try {
                         // API リクエスト
                        $id = $charge->id;
                        $this->Pay->refund($id);
                        echo "refunded";
                        $refunded = 1;
                        } catch (\WebPay\ErrorResponse\ErrorResponseException $e) {
                            $error = $e->data->error;
                            switch ($error->causedBy) {
                                case 'buyer':
                                    // カードエラーなど、購入者に原因がある
                                    // エラーメッセージをそのまま表示するのがわかりやすい
                                    $refunded = 0;
                                    break;
                                case 'insufficient':
                                    // 実装ミスに起因する
                                    $refunded = 0;
                                    break;
                                case 'missing':
                                    // リクエスト対象のオブジェクトが存在しない
                                    $refunded = 0;
                                    break;
                                case 'service':
                                    // WebPayに起因するエラー
                                    $refunded = 0;
                                    break;
                                default:
                                    // 未知のエラー
                                    $refunded = 0;
                                    break;
                            }
                        } catch (\WebPay\ApiException $e) {
                            // APIからのレスポンスが受け取れない場合。接続エラーなど
                            $refunded = 0;

                        }
                    }
                }
                $account->updatecolumn();

        }
    }
        /**
     * 前日掲載が終了したピックアップ広告に対してメールを送信
     * 毎日0:00
     */
    public function sendStopPickupMailAction() {
           $this->useModel('PickupOffer');
           $offers = $this->PickupOffer->findStoppedOffers();


           foreach ($offers as $offer) {
               $this->PickupOffer-> sendStopPickupMail($offer);
           }

    }
           /**
     * 掲載が残り7日の無料広告に対してメールを送信
     * 毎日0:00
     */
    public function sendStopFreeMailAction() {
           $this->useModel('FreeOffer');

           $offers = $this->FreeOffer->findStoppingOffers();


           foreach ($offers as $offer) {
               $this->FreeOffer-> sendStopFreeMail($offer);
           }


    }
        /**
     *単一モデルの読み込み関数
     *
     * @param object $models
     * @return void
     */
    public function useModel($model) {
        $this->$model = new $model();
    }


}
