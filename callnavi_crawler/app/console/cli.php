<?php

use Phalcon\DI\FactoryDefault;
use Phalcon\Mvc\View;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use Phalcon\Session\Adapter\Files as SessionAdapter;
use Phalcon\Logger\Multiple as MultipleStream;
use Phalcon\Logger\Adapter\File as FileAdapter;
use Phalcon\Logger\Formatter\Line as LineFormatter;
use Phalcon\Logger\Adapter\Stream as StreamAdapter;

error_reporting(E_ALL);

try {

    //Read the configuration
    $config = require __DIR__ . "/../config/config.php";

    $loader = new \Phalcon\Loader();

    $loader->registerDirs(
            array(
                $config->application->controllersDir,
                $config->application->controllersAdminDir,
                $config->application->controllersUserDir,
                $config->application->controllerTraitsDir,
                $config->application->modelsDir,
                $config->application->modelTraitsDir,
                $config->application->crawlerDir,
                $config->application->batchDir,
                $config->application->vendorDir,
                $config->application->tasksDir,
            )
    )->register();

    $loader->registerNamespaces(
            array(
                "Library\Text" => "/../../app/library/Text",
            )
    )->register();

    define('PEAR_PATH', __DIR__ . "/../../app/library");



    /**
     * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
     */
    $di = new \Phalcon\DI\FactoryDefault\CLI();

    $di->set('url', function () use ($config) {
        $url = new UrlResolver();
        $url->setBaseUri($config->application->baseUri);

        return $url;
    }, true);

    /**
     * サーバー用のコンフィグ設定を追加する
     * @since 2014/05/04
     */
    $_SERVER['SERVER_NAME'] = '';
    $server_info = 'local_dev';
    if (strtoupper(substr(PHP_OS, 0, 3)) !== 'WIN') {
        $server_info = trim(file_get_contents('/home/callnavi/crawler/app/config/server_info.txt'));
    }

    $di->set('view', function () use ($config) {

        $view = new View();

        $view->setViewsDir($config->application->viewsDir);

        $view->registerEngines(array(
            '.volt' => function ($view, $di) use ($config) {

        $volt = new VoltEngine($view, $di);

        $volt->setOptions(array(
            'compiledPath' => $config->application->cacheDir,
            'compiledSeparator' => '_'
        ));

        return $volt;
    },
            '.phtml' => 'Phalcon\Mvc\View\Engine\Php'
        ));

        return $view;
    }, true);

    /**
     * Database connection is created based in the parameters defined in the configuration file
     */
    $dbTarget = 'db_' . $server_info;
    $di->set('db', function () use ($config, $dbTarget) {
        return new DbAdapter(array(
                'host' => $config->$dbTarget->host,
                'username' => $config->$dbTarget->username,
                'password' => $config->$dbTarget->password,
                'dbname' => $config->$dbTarget->dbname,
                'charset' => $config->$dbTarget->charset,
        ));
    });

    /**
     * If the configuration specify the use of metadata adapter use it or use memory otherwise
     */
    $di->set('modelsMetadata', function () {
        return new MetaDataAdapter();
    });

    /**
     * Start the session the first time some component request the session service
     */
    $di->set('session', function () {
        $session = new SessionAdapter(array(
            'lifetime' => 14 * 24 * 60 * 60,
        ));
        $session->start();

        return $session;
    });

    /**
     * メール設定を読み込み
     */
    $di->set('mail', function() {
        return new Mail();
    });

    /**
     * クローラー設定
     */
    $di->set('crawlerConfig', function() {
        return new \Phalcon\Config\Adapter\Ini(__DIR__ . "/../../app/config/crawlerConfig.ini");
    });

    $di->set('logger', function () use ($config) {
        $pid = getmypid();
        $today = date( "Y-m-d", time() );

        $logger = new MultipleStream();
        $fileAdapter = new FileAdapter($config->application->logDir."crawler-$today.log");
        $fileAdapter->setLogLevel(Phalcon\Logger::DEBUG);

        $streamAdapter = new StreamAdapter('php://stdout');
        $streamAdapter->setLogLevel(Phalcon\Logger::INFO);

        $logger->push($fileAdapter);
        $logger->push($streamAdapter);
        $formatter = new LineFormatter("[$pid][%date%][%type%] %message%");
        $logger->setFormatter($formatter);

        return $logger;
    });

    $console = new \Phalcon\CLI\Console();
    $console->setDI($di);
    // pass command-line arguments to handle
    $arguments = array();
    $params = array();

    foreach ($argv as $k => $arg) {
        if ($k == 1) {
            $arguments['task'] = $arg;
        } elseif ($k == 2) {
            $arguments['action'] = $arg;
        } elseif ($k >= 3) {
            $params[] = $arg;
        }
    }
    if (count($params) > 0) {
        $arguments['params'] = $params;
    }
    $console->handle($arguments);
} catch (Phalcon\Exception $e) {
    echo $e->getMessage();
} catch (PDOException $e) {
    echo $e->getMessage();
}
