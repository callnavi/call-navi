<script src="/public/scripts/index.js"></script>

<section class="mediaPC">
    <div class="searchMapA01">
    <p><img src="/public/images/index/index_img_02.png" width="720" height="140" alt="全国のコールセンター求人を一括検索。簡単検索であなたにピッタリのコールセンターがきっと見つかる！" /></p>
        <form action="/search/result" method="get" class="searchArea">
            <h2>《簡単求人一括検索》</h2>
            <!--<p><input type="text" name="area"  value="{% if params.area is defined %}{{params.area}}{% endif %}"  placeholder="勤務地（都道府県名または市区町村名など）"><br>例：東京都渋谷区、池袋駅、大阪市北区</p>-->
            <p><input type="text" name="keyword" value="{% if params.keyword is defined %}{{params.keyword}}{% endif %}" placeholder="キーワード（例：港区、新宿区、札幌、福岡、高時給など）"　><br>例：東京都、インセンティブ、テレアポ、アウトバウンド</p>
            <p class="search"><button type="submit">検索</button></p>
        </form><!-- / searchArea -->
        <ul class="area">
            <li class="area01"><a href="/search/result?area=北海道">北海道</a></li>
　　　　　　　<li class="area02"><a href="/search/result?area=岩手or福島or秋田or青森or山形or宮城">東北</a><li>
            <li class="area03"><a href="/search/result?area=茨城or栃木or群馬or埼玉or千葉or東京or神奈川">関東</a></li>
            <li class="area04"><a href="/search/result?area=新潟or富山or石川or福井or長野or岐阜or静岡or愛知or三重">中部</a></li>
            <li class="area05"><a href="/search/result?area=滋賀or三重or京都or奈良or大阪or和歌山or兵庫">近畿</a></li>
            <li class="area06"><a href="/search/result?area=鳥取or島根or岡山or広島or山口">中国</a></li>
            <li class="area07"><a href="/search/result?area=徳島or香川or愛媛or高知">四国</a></li>
            <li class="area08"><a href="/search/result?area=福岡or鹿児島or佐賀or長崎or大分or熊本or宮崎">九州</a></li>
            <li class="area09"><a href="/search/result?area=沖縄">沖縄</a></li>
        </ul>
    </div><!-- / searchMapA01 -->
</section>

<div class="slideA01 js-slider marginB30">
<div class="slidesContainer">
<ul class="slides topics">
<li><a href="/ccwork/detail05">
<div style="text-align:center; margin-left:auto; margin-right:auto;"><p class="image"><img style="width: 100%;" alt="もう面接も怖くない！コールセンター面接で失敗しないコツ！" src="/public/images/common/mainimg01.jpg"></p></div></a></li>
<li><a href="/about"><div style="text-align:center; margin-left:auto; margin-right:auto;"><p class="image"><img style="width: 100%;" alt="日本全国のコールセンター求人を一括検索！コールナビとは？" src="/public/images/common/mainimg02.jpg"></p></div></a></li>
<li><a href="/ccwork/detail06"><div style="text-align:center; margin-left:auto; margin-right:auto;"><p class="image"><img alt="ストレスに負けないコツ！" style="width: 100%;" src="/public/images/common/mainimg03.jpg"></p></div></a></li>
</ul>
</div>
<div class="slideControl topicsSlideControl">
<ul class="cursor topicsCursor">
<li class="prev"><a href="javascript:;">prev</a></li>
<li class="next"><a href="javascript:;">next</a></li>
</ul>
<ul class="select topicsSelect">
<li><a href="javascript:;">1</a></li>
<li><a href="javascript:;">2</a></li>
<li><a href="javascript:;">3</a></li>  
</ul>
</div>
</div>
<!-- / slideA01 -->

<?php if (count($pickup_inners) !== 0 or count($pickup_inner_halfs) !== 0 ) : ?>
<h2 class="headingA02 pickup">新着ピックアップ求人</h2>
<div class="slideA01 js-slider marginB30">
    <div class="slidesContainer">
        <ul class="slides pickupPanel">
            {% for inner in pickup_inners %}
<?php
$inner->message = strip_tags($inner->message, "<br />");
?>
                <li>
                    {{ partial("/offer/pickup_inner", ['pickup': inner]) }}
                </li>
            {% endfor %}
        </ul>
    </div>
<?php if (count($pickup_inners) > 1) : ?>
    <div class="slideControl pickupPanelSlideControl">
        <ul class="cursor pickupPanelCursor">
            <li class="prev"><a href="javascript:;">prev</a></li>
            <li class="next"><a href="javascript:;">next</a></li>
        </ul>
        <ul class="select pickupPanelSelect">
            {% for count in 1..pickup_inners | length %}
                <li><a href="javascript:;">count</a>
                </li>
            {% endfor %}
        </ul> 
    </div>
<?php endif; ?>
</div><!-- / slideA01 -->
<?php endif; ?>
<div class="pickupConteiner">
    {% for inner in pickup_inner_halfs %}

<?php
$inner->message = strip_tags($inner->message, "<br />");
?>

        {{ partial("/offer/pickup_inner_half", ['pickup': inner]) }}
    {% endfor %}
</div><!-- pickupConteiner -->
{% if pickups_count !== 0 %}
<div class="buttonBlockB01">
<p>あなたにオススメのコールセンター求人がたくさんあります！</p>
<div class="alignC"><a href="/index/pickupList" class="btnB03"><span>ピックアップ求人一覧</span></a>
</div>
</div>

{% endif %}
