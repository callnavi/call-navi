
<div id="mainContents">
<!-- InstanceBeginEditable name="mainContents" -->
<section>
<div class="resultA01">
{% if pickups_count !== 0 %}<!--
    -->{% for pickup in pickups %}<!--
        -->{{ partial("/offer/pickup_listing") }}
     {% endfor %}
 {% endif %} 
            <footer>
                <dl>
                    <dt>検索結果ページ</dt>                                      
                    <dd>
                        <ul>
                            {% if page > 1  %}
                                <li class="prev">
                                    <a href="/index/pickupList?page={{(page - 2)}}">前へ</a>
                                </li>
                                <li>
                                    <a href="/index/pickupList?page={{(page - 2)}}">{{page-1}}</a>
                                </li>                                
                                <li>
                                    <a href="/index/pickupList?page={{(page - 1)}}">{{page}}</a>
                                </li>                                
                            {% elseif page > 0 %}
                                <li class="prev">
                                    <a href="/index/pickupList?page={{(page - 1)}}">前へ</a>                                   
                                </li>
                                <li>
                                    <a href="/index/pickupList?page={{(page - 1)}}">{{page}}</a>
                                </li>                                
                            {% endif %}

                            <li>                                
                                <em>{{page+1}}</em>
                            </li>

                            {% if countNextPages > 1 %}
                                <li>
                                    <a href="/index/pickupList?page={{(page + 1)}}">{{page+2}}</a>                                 
                                </li>
                                <li>
                                    <a href="/index/pickupList?page={{(page + 2)}}">{{page+3}}</a>                                  
                                </li>
                                <li class="next">
                                    <a href="/index/pickupList?page={{(page + 2)}}">次へ</a>                                
                                </li>
                            {% elseif countNextPages > 0 %}
                                <li>
                                    <a href="/index/pickupList?page={{(page + 1)}}">{{page+2}}</a>                                    
                                </li>
                                <li class="next">
                                    <a href="/index/pickupList?page={{(page + 1)}}">次へ</a>                                   
                                </li>
                            {% endif %}
                        </ul>
                    </dd>
                </dl>
            </footer>
        </div><!-- / resultA01 -->
        </section>
        <!-- InstanceEndEditable -->
       </div><!-- / mainContents -->
