<script src="/public/scripts/detail.js"></script>

{% if data.detail_content_type=="form" %}
<section>
<div class="pickupC01">
<h2><span class="pickup">Pickup</span>{{data.job_category}}</h2>
<div class="slideA01 js-slider">
<div class="slidesContainer">
{% if data.has_third_pic==1 %}
<ul class="slides eyeCatch">
<li><img src="/public/images/pickup/first/{{ data.id }}.png" width="100%" height="" alt=""></li>
<li><img src="/public/images/pickup/second/{{ data.id }}.png" width="100%" height="" alt=""></li>
<li><img src="/public/images/pickup/third/{{ data.id }}.png" width="100%" height="" alt=""></li>
</ul>
{% elseif data.has_second_pic==1 %}
<ul class="slides eyeCatch">
<li><img src="/public/images/pickup/first/{{ data.id }}.png" width="100%" height="" alt=""></li>
<li><img src="/public/images/pickup/second/{{ data.id }}.png" width="100%" height="" alt=""></li>
</ul>
{% elseif data.has_first_pic==1 %}
<ul class="slides eyeCatch">
<li><img src="/public/images/pickup/first/{{ data.id }}.png" width="100%" height="" alt=""></li>
{% elseif data.detail_content_type=="form" %}
<li><img src="/public/admin_images/no_upload/img_no_upload_01.png" width="100%" height="" alt=""></li>
</ul>
{% else %}
{% endif %}
</div>

<div class="slideControl eyeCatchSlideControl">
{% if data.has_third_pic==1 %}
<ul class="cursor eyeCatchCursor">
<li class="prev"><a href="javascript:;">prev</a></li>
<li class="next"><a href="javascript:;">next</a></li>
</ul>
<ul class="select eyeCatchSelect">
<li><a href="#">1</a></li>
<li><a href="#">2</a></li>
<li><a href="#">3</a></li>
</ul>
{% elseif data.has_second_pic==1 %}
<ul class="cursor eyeCatchCursor">
<li class="prev"><a href="javascript:;">prev</a></li>
<li class="next"><a href="javascript:;">next</a></li>
</ul>
<ul class="select eyeCatchSelect">
<li><a href="#">1</a></li>
<li><a href="#">2</a></li>
</ul>
{% elseif data.has_first_pic==1 %}
<!--<ul class="select eyeCatchSelect">
<li><a href="#">1</a></li>
</ul>-->
{% endif %}
</div>

</div><!-- / slideA01 -->
<p class="company">{{data.company_name}}</p>
{% if data.message=='' %}
<p class="error">まだデータ入力がありません</p>
{% else %}
<p class="summary">{{data.message}}</p>
{% endif %}
<dl>
<dt><span>職　種</span></dt>
{% if data.job_category=="" %}
<dd class="error">まだデータ入力がありません。</dd>
{% else %}
<dd>{{data.job_category}}</dd>
{% endif %}
</dl>
<dl>
<dt><span>給　与</span></dt>
<dd><div>
{% if data.salary_term == "year" %}
     年収
{% elseif data.salary_term == "month" %}
     月給
{% elseif data.salary_term == "day" %}
     日給
{% elseif data.salary_term == "hour" %}
     時給
   {% endif %}
 {% if data.salary_value_min=="" %}
 <div class="error">まだデータ入力がありません</div>
 {% else %}  
 {{data.salary_value_min }} 
{% endif %}
 {% if data.salary_unit_min == "man_yen" %}
  万円
  {% elseif data.salary_unit_min == "yen" %}
  円
  {% elseif data.salary_unit_min == "dollar" %}
   ＄ 
 {% endif %} 
  ～ 
{% if data.salary_value_max=="" %}
 <div class="error">まだデータ入力がありません。</div>
 {% else %}  
 {{ data.salary_value_max }} 
{% endif %}
{% if data.salary_unit_min == "man_yen" %}
 万円 
 {% elseif data.salary_unit_min == "yen" %} 
  円
{% elseif data.salary_unit_min == "dollar" %}
  ＄
 {% endif %}
  </div></dd>
</dl>
<dl>
<dt><span>勤務地</span></dt>
<dd> <div>

 {% if data.workplace_prefecture=="" %}<!--
--><div class="error">まだデータ入力がありません。</div><!--
-->{% else %}
{{data.workplace_prefecture}}
{% endif %}
{% if data.workplace_area=="" %}<!--
--><div class="error">まだデータ入力がありません。</div><!--
-->{% else %}<!--
-->{{data.workplace_area}}<!--
-->{% endif %}<!--
-->{% if data.workplace_address=="" %}<!--
--><div class="error">まだデータ入力がありません。</div><!--
-->{% else %}<!--
-->{{data.workplace_address}}<!--
-->{% endif %}
</div></dd>
</dl>
<dl>
<dt><span>雇用形態</span></dt>
<dd>
{% if data.hired_as=="" %}
<div class="error">まだデータ入力がありません。</div>
{% else %}
{{hired_as_display}}
{% endif %}

</dd>
</dl>
<dl>
<dt><span>こだわり</span></dt>
<dd>
{% if features is defined %}
<ul class="tagListA01">
{{ partial("/data/features_php")}}
</ul>
{% endif %}
</dd>
</dl>
</div><!-- / pickupC01 -->
</section>


<section>

<section class="unitA01">
<h2 class="headingB02">募集要項</h2>
<table class="tableA01">
<col style="width:130px;">
<col>
<tr>
<th>仕事内容</th>
<td>{% if detail.job_description =="" %}<div class="error">まだデータ入力がありません。</div>{% else %}{{detail.job_description}}{% endif %}</td>
</tr>
<tr>
<th>応募資格</th>
<td>{% if detail.requirement=="" %}<div class="error">まだデータ入力がありません。</div>{% else %}{{detail.requirement}}{% endif %}</td>
</tr>
<tr>
<th>勤務時間</th>
<td>{% if detail.office_hours=="" %}<div class="error">まだデータ入力がありません。</div>{% else %}{{detail.office_hours}}{% endif %}</td>
</tr>
<tr>
<th>休日／休暇</th>
<td>{% if detail.holidays=="" %}<div class="error">まだデータ入力がありません。</div>{% else %}{{detail.holidays}}{% endif %}</td>
</tr>
<tr>
<th>待遇／福利厚生</th>
<td>{% if detail.welfares=="" %}<div class="error">まだデータ入力がありません。</div>{% else %}{{detail.welfares}}{% endif %}</td>

</tr>
{% if detail.procedures !== "" %}
<tr>
<th>応募後の選考プロセス</th>
<td>{{detail.procedures}}</td>
</tr>
{% endif %}
{% if detail.others !== "" %}
<tr>
<th>その他・特記事項</th>
<td>{{detail.others}}</td>
</tr>
{% endif %}
</table>
</section>

<section class="unitA01 mobileUnitB02">
<h2 class="headingB02">会社情報</h2>
<table class="tableA01">
<col style="width:130px">
<col>
<tr>
<th>設立</th>
<td>{% if detail.founded=="" %}<div class="error">まだデータ入力がありません。</div>{% else %}{{detail.founded}}{% endif %}</td>
</tr>
<tr>
<th>代表者</th>
<td>{% if detail.president=="" %}<div class="error">まだデータ入力がありません。</div>{% else %}{{detail.president}}{% endif %}</td>
</tr>
<tr>
<th>資本金</th>
<td>{% if detail.capital=="" %}<div class="error">まだデータ入力がありません。</div>{% else %}{{detail.capital}}{% endif %}</td>
</tr>
<tr>
<th>従業員数</th>
<td>{% if detail.employees=="" %}<div class="error">まだデータ入力がありません。</div>{% else %}{{detail.employees}}{% endif %}</td>
</tr>
<tr>
<th>事業内容</th>
<td>{% if detail.business_description=="" %}<div class="error">まだデータ入力がありません。</div>{% else %}{{detail.business_description}}{% endif %}</td>
</tr>
<tr>
<th>会社URL</th>
<td><a {% if detail.company_url !=="" %}href="{{detail.company_url}}" target="_blank"{% endif %}>{% if detail.company_url=="" %}<div class="error">まだデータ入力がありません。</div>{% else %}{{detail.company_url}}{% endif %}</a></td>
</tr>
</table>
</section>

</section>


<div class="mobileUnitC01">
<p class="buttonBlockA01 alignC marginOff"><a href="{{data.detail_url}}" target="_blank" class="btnA01 fontSize30">この求人に応募する</a></p>
</div>
{% elseif data.detail_content_type == "html"%}
{{detail.html}}
{% endif %}

                
            
