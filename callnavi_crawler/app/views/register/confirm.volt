<div id="contents">
<div id="contentsContainer">
<!-- InstanceBeginEditable name="mainContents" -->

<div class="unitA01">
<h2 class="headingA01">新規アカウント作成</h2>
<div class="headingB01">
<h3>入力情報のご確認</h3>
</div>
<table class="tableB01 marginB30">
<tr>
<th class="ttl">会社名</th>
<td class="ipt">{{data['company']}}</td>
</tr>
<tr>
<th class="ttl">会社名フリガナ</th>
<td class="ipt">{{data['company_kana']}}</td>
</tr>
<tr>
<th class="ttl">郵便番号</th>
<td class="ipt">{{data['postcode_A']}}-{{data['postcode_B']}}</td>
</tr>
<tr>
<th class="ttl">住所</th>
<td class="ipt">{{data['adress']}}</td>
</tr>
<tr>
<th class="ttl">電話番号</th>
<td class="ipt">{{data['tel_A']}}-{{data['tel_B']}}-{{data['tel_C']}}</td>
</tr>
<tr>
<th class="ttl">FAX番号</th>
<td class="ipt">{{data['fax_A']}}-{{data['fax_B']}}-{{data['fax_C']}}</td>
</tr>
<tr>
<th class="ttl">会社URL</th>
<td class="ipt">http://{{data['url']}}</td>
</tr>
<tr>
<th class="ttl">所属部署</th>
<td class="ipt">{{data['section']}}</td>
</tr>
<tr>
<th class="ttl">役職</th>
<td class="ipt">{{data['position']}}</td>
</tr>
<tr>
<th class="ttl">ご担当者名</th>
<td class="ipt">{{data['person_family']}} {{data['person_given']}}</td>
</tr>
<tr>
<th class="ttl">ご担当者名フリガナ</th>
<td class="ipt">{{data['person_family_kana']}} {{data['person_given_kana']}}</td>
</tr>
<tr>
<th class="ttl">Eメールアドレス<br>（ログインID）</th>
<td class="ipt">{{data['mail']}}</td>
</tr>
<tr>
<th class="ttl">パスワード</th>
<td class="ipt">{{data['password']}}</td>
</tr>
</table>

<div class="alignC">
<div style="display:inline-flex">
<form method="post" action="/register/input">
<input type="hidden" name="company" value="{{data['company']}}" />
<input type="hidden" name="company_kana" value="{{data['company_kana']}}" />
<input type="hidden" name="postcode_A" value="{{data['postcode_A']}}" />
<input type="hidden" name="postcode_B" value="{{data['postcode_B']}}" />
<input type="hidden" name="adress" value="{{data['adress']}}" />
<input type="hidden" name="tel_A" value="{{data['tel_A']}}" />
<input type="hidden" name="tel_B" value="{{data['tel_B']}}" />
<input type="hidden" name="tel_C" value="{{data['tel_C']}}" />
<input type="hidden" name="fax_A" value="{{data['fax_A']}}" />
<input type="hidden" name="fax_B" value="{{data['fax_B']}}" />
<input type="hidden" name="fax_C" value="{{data['fax_C']}}" />
<input type="hidden" name="url" value="{{data['url']}}" />
<input type="hidden" name="section" value="{{data['section']}}" />
<input type="hidden" name="position" value="{{data['position']}}" />
<input type="hidden" name="person_family" value="{{data['person_family']}}" />
<input type="hidden" name="person_given" value="{{data['person_given']}}" />
<input type="hidden" name="person_family_kana" value="{{data['person_family_kana']}}" />
<input type="hidden" name="person_given_kana" value="{{data['person_given_kana']}}" />
<input type="hidden" name="mail" value="{{data['mail']}}" />
<input type="hidden" name="password" value="{{data['password']}}" />
<input type="hidden" name="question" value="{{data['question']}}" />
<input type="hidden" name="answer" value="{{data['answer']}}" />
<p class="alignC"><button class="btnA01">入力内容を修正する</button></p>
</form>

&nbsp;&emsp;<form method="post" action="/register/thanks">
<input type="hidden" name="company" value="{{data['company']}}" />
<input type="hidden" name="company_kana" value="{{data['company_kana']}}" />
<input type="hidden" name="postcode_A" value="{{data['postcode_A']}}" />
<input type="hidden" name="postcode_B" value="{{data['postcode_B']}}" />
<input type="hidden" name="adress" value="{{data['adress']}}" />
<input type="hidden" name="tel_A" value="{{data['tel_A']}}" />
<input type="hidden" name="tel_B" value="{{data['tel_B']}}" />
<input type="hidden" name="tel_C" value="{{data['tel_C']}}" />
<input type="hidden" name="fax_A" value="{{data['fax_A']}}" />
<input type="hidden" name="fax_B" value="{{data['fax_B']}}" />
<input type="hidden" name="fax_C" value="{{data['fax_C']}}" />
<input type="hidden" name="url" value="{{data['url']}}" />
<input type="hidden" name="section" value="{{data['section']}}" />
<input type="hidden" name="position" value="{{data['position']}}" />
<input type="hidden" name="person_family" value="{{data['person_family']}}" />
<input type="hidden" name="person_given" value="{{data['person_given']}}" />
<input type="hidden" name="person_family_kana" value="{{data['person_family_kana']}}" />
<input type="hidden" name="person_given_kana" value="{{data['person_given_kana']}}" />
<input type="hidden" name="mail" value="{{data['mail']}}" />
<input type="hidden" name="password" value="{{data['password']}}" />
<input type="hidden" name="question" value="{{data['question']}}" />
<input type="hidden" name="answer" value="{{data['answer']}}" />
<p class="alignC"><button class="btnA02">この内容で登録する</button></p>
</form>
</div>
</div>
</div><!-- /.unitA01 -->
<!-- InstanceEndEditable -->
</div><!-- / contentsContainer -->
</div><!-- / contents -->
