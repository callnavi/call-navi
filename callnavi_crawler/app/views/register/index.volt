<div id="contents">
<div id="contentsContainer">
<!-- InstanceBeginEditable name="mainContents" -->
<div class="unitA01">
<h2 class="headingA01">求人広告管理（ホーム）</h2>
<p class="marginB30">『コールナビ』では2種類の求人広告プランを用意しております。</p>
<div class="headingB01">
<h3>リスティング広告</h3>
</div>
<p>求職者が入力する検索キーワードと連動して動的に広告が掲載されます。<br>
表示エリアについては<a href="#">こちら</a>をご確認ください。</p>
<ul class="boxA01">
<li>費用</li>
<li>入札形式（1クリック：<span class="price">10</span>円～）<br>
<span class="alert">※入札単価が高い順に求人広告の表示頻度が高くなります。</span></li>
</ul>
</div>

<div class="unitA01">
<div class="headingB01">
<h3>レクタングル広告</h3>
</div>
<p>トップページ、及び求職者が検索した求人情報一覧ページに固定で広告が掲載されます。<br>
表示エリアについては<a href="#">こちら</a>をご確認ください。</p>
<ul class="boxA01">
<li>費用</li>
<li>入札形式（1クリック：<span class="price">30</span>円～）<br>
<span class="alert">※入札単価が高い順に求人広告の表示頻度が高くなります。</span></li>
</ul>
</div>

<div class="unitA01">
<div class="headingB01">
<h3>ピックアップ広告</h3>
</div>
<p>トップページに固定で広告が表示されます。<br>
表示エリアについては<a href="#">こちら</a>をご確認ください。</p>
<ul class="boxA01">
<li>費用</li>
<li class="ltt">
<table>
<tr>
<td class="alignL">上段（メイン）</td>
<td class="alignR"><span class="price">10,000</span>円</td>
<td class="alignL">&nbsp;／1週（5社限定）</td>
</tr>
<tr>
<td class="alignL">中段（サブ）</td>
<td class="alignR"><span class="price">8,000</span>円</td>
<td class="alignL">&nbsp;／1週（8社限定）</td>
</tr>
<tr>
<td class="alignL">右カラム（ロゴ表示）</td>
<td class="alignR"><span class="price">1,000</span>円</td>
<td class="alignL">&nbsp;／1週（10社限定）</td>
</tr>
</table>
</li>
</ul>
</div>

<div class="unitA01">
<h2 class="headingA01">求人広告費用のお支払い方法</h2>
<p class="marginB10">下記いずれかのクレジットカード決済です。ご利用された分のみのお支払いとなります。</p>
<div class="alignC"><img src="/public/admin_images/c00_img_01.png" width="502" height="122" alt=""></div>
</div>

<div class="unitA01">
<h2 class="headingA01">求人広告の作成方法</h2>
<p>新規アカウント作成後、管理画面から簡単に求人広告を作成することができます。</p>
<p class="alignC"><a href="/register/input" class="btnB01">今すぐ求人広告を作成する<br>
<span>（新規アカウント作成）</span></a></p>
</div>
<!-- InstanceEndEditable -->
</div><!-- / contentsContainer -->
</div><!-- / contents -->