{% if pickup_right_columns | length != 0 %}
    <section class="pickupArea">
        {%for column in pickup_right_columns %}

<?php
$column->message = strip_tags($column->message, "<br />");                                                                                                          ?>

             <article class="pickupB01 impression_offer pickup_offer" offer_id="{{column.id}}" offer_type="pickup">
                 
                 <a href={% if column.detail_url !=="" and column.detail_content_type == "none" %}"{{column.detail_url}}" target="_blank"{% elseif column.detail_content_type !== "none" %}"/public/index/pickupPreview?offer_id={{column.id}}" target="_blank"{% endif %}>
                    <h3><span class="new">NEW</span><span class="pickup">PICKUP</span><br>{{column.job_category}}</h3>
                    <p class="image"><img src="/public/images/pickup/thum/{{column.id}}.png" width="" height="" alt="{{column.title}}"></p>
                    <p class="company">{{column.company_name}}</p>
                    <p class="summary">{{column.message}}</p>
                    <?php $hired_as = explode(':', $column->hired_as) ; ?>
                    {{ partial("/data/hired_as_preview")}} 
                    <table>
                        <tr>
                            <th>給与</th>
                            <td><div>{{column.salary_term_display}} {% if column.salary_unit_min !== "dollar" %}{{column.salary_value_min}}{{column.salary_unit_min_display}}〜{{column.salary_value_max}}{{column.salary_unit_max_display}}{% else %}{{column.salary_unit_min_display}}{{column.salary_value_min}}〜<br>{{column.salary_unit_max_display}}{{column.salary_value_max}}{% endif %}</div></td>
                        </tr>
                        <tr>
                            <th>勤務地</th>
                            <td><div>{{column.workplace_prefecture}}{{column.workplace_area}}{{column.workplace_address}}</div></td>
                        </tr>
                    </table>
                </a>
            </article><!-- / pickupB02 -->
        {% endfor %}    
    </section>
{% endif %}