{% if pickup_logo_banners | length != 0 %}
    <section>
        <h2 class="headingA01 recommend marginOff">おすすめ求人企業</h2>
        <ul class="bnrListB01">
            {% for banner in pickup_logo_banners %}
                <li> <a href={% if banner.detail_url !=="" and banner.detail_content_type == "none" %}"{{banner.detail_url}}" target="_blank"{% elseif banner.detail_content_type !== "none" %}"/public/index/pickupPreview?offer_id={{banner.id}}" target="_blank"{% endif %}><img class=" impression_offer pickup_offer" offer_id="{{banner.id}}" offer_type="pickup" src="/public/images/pickup/logo/{{banner.id}}.png" width="200" height="74" alt=""/><span>{{banner.title}}</span></a></li>
                        {% endfor %}
        </ul>
    </section>
{% endif %}