<article class="pickupA01 impression_offer listing_offer" offer_id="{{listing.id}}" offer_type="listing" listing_keyword_id="{{listing.listing_keyword_id}}">
    <a href="{{listing.detail_url}}" target="_blank"> 
       <h3><span>PR</span>{{listing.job_category}}</h3>
        <div class="contentsInner">
            <p class="image"><img src="/public/images/listing/{{listing.id}}.png" width="" height="" alt=""></p>
            <p class="company">{{listing.company_name}}</p>
            <p class="summary mediaPC">{{listing.job_description}}</p>
        </div><!-- / contentsInner -->
        <div class="contentsInner">
          <div class="details">
              <?php $hired_as = $listing->hired_as_array ; ?>
             {{ partial("/data/hired_as_preview")}}
            <table>
                <tr>
                    <th>給　与</th>
                    <td><div>{{listing.salary_term_display}} {% if listing.salary_unit_min !== "dollar" %}{{listing.salary_value_min}}{{listing.salary_unit_min_display}}〜{{listing.salary_value_max}}{{listing.salary_unit_max_display}}{% else %}{{listing.salary_unit_min_display}}{{listing.salary_value_min}}〜{{listing.salary_unit_max_display}}{{listing.salary_value_max}}{% endif %}</div></td>
                </tr>
                <tr>
                    <th>勤務地</th>
                    <td><div>{{listing.workplace_prefecture}}{{listing.workplace_area}}{{listing.workplace_address}}</div></td>
                </tr>
            </table>
              </div>
            <?php $features = $listing->features_array ; ?>
             <ul class="tags"> 
              {{ partial("/data/features_php")}}
             </ul>
        </div>
    </a>  
</article>
