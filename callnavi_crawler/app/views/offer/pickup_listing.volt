{% if pickup.id is defined %}
    <article class="pickupA01 impression_offer pickup_offer" offer_id="{{pickup.id}}" offer_type="pickup">
        <a href={% if pickup.detail_url !=="" and pickup.detail_content_type == "none" %}"{{pickup.detail_url}}" target="_blank"{% elseif pickup.detail_content_type !== "none" %}"/public/index/pickupPreview?offer_id={{pickup.id}}" target="_blank"{% endif %}>
            <h3>{% if pickup.published_from >= recent_past %}<span class="new">NEW</span>{% endif %}<span class="pickup">PICKUP</span>{{pickup.job_category}}</h3>
            <div class="contentsInner">
                <p class="image"><img src="/public/images/pickup/thum/{{pickup.id}}.png" width="" height="" alt=""></p>
                <p class="company">{{pickup.company_name}}</p>
                <p class="summary mediaPC">{{pickup.message}}</p>
            </div><!-- / contentsInner -->
            <div class="contentsInner">
              <div class="details">
               <?php $hired_as = explode(':', $pickup->hired_as) ; ?>
               {{ partial("/data/hired_as_preview")}}
                <table>
                    <tr>
                        <th>給　与</th>
                        <td><div>{{pickup.salary_term_display}} {% if pickup.salary_unit_min !== "dollar" %}{{pickup.salary_value_min}}{{pickup.salary_unit_min_display}}〜{{pickup.salary_value_max}}{{pickup.salary_unit_max_display}}{% else %}{{pickup.salary_unit_min_display}}{{pickup.salary_value_min}}〜{{pickup.salary_unit_max_display}}{{pickup.salary_value_max}}{% endif %}</div></td>
                    </tr>
                    <tr>
                        <th>勤務地</th>
                        <td><div>{{pickup.workplace_prefecture}}{{pickup.workplace_area}}{{pickup.workplace_address}}</div></td>
                    </tr>
                </table>
              </div>  
                 <?php $features = $pickup->features_array ; ?>
                 <ul class="tags"> 
                {{ partial("/data/features_php")}}  
                 </ul>
                
            </div>
        </a>
    </article>
{% endif %}
