{% if free.id is defined %}
    <article class="freeA01 impression_offer free_offer" offer_id="{{free.id}}" offer_type="free">
        <a href="{{free.detail_url}}" target="_blank">
    
            <div class="contentsInner">
                <p class="image"><img src="/public/images/free/{{free.id}}.png" width="" height="" alt=""></p>
                <p class="company">{{free.company_name}}</p>
                <p class="summary mediaPC">{{free.job_description}}</p>
            </div><!-- / contentsInner -->
            <div class="contentsInner">
              <div class="details">
               <?php $hired_as = explode(':', $free->hired_as) ; ?>
               {{ partial("/data/hired_as_preview")}}
                <table>
                    <tr>
                        <th>給　与</th>
                        <td><div>{{free.salary_term_display}} {{free.salary_value_min}}{{free.salary_unit_min_display}}〜{{free.salary_value_max}}{{free.salary_unit_max_display}}</div></td>
                    </tr>
                    <tr>
                        <th>勤務地</th>
                        <td><div>{{free.workplace_prefecture}}{{free.workplace_area}}{{free.workplace_address}}</div></td>
                    </tr>
                </table>
              </div>  
                 <?php $features = $free->features_array ; ?>
                 <ul class="tags"> 
                {{ partial("/data/features_php")}}
                 </ul>
                
            </div>
        </a>
    </article>
{% endif %}