<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">知っ得情報</span></div>
  <h1>コールセンターの面接で失敗しないコツ？</h1>
  </header>
  
<!--- 段落１ 開始 --->  
  <section class="sect01">
    <img src="/public/images/ccwork/004/main001.jpg" alt="コールセンターの面接で失敗しないコツ？" />
  <p class="marginBottom10px">コールセンターの人事担当者が面接で最も注目するのは話し方です。<br>
大事なのは<span class="Blue_text">「ハキハキと」「丁寧に」</span>話すこと。<br>
コールセンターは受信発信に関わらず、『話す』お仕事です。対面で話すのとは違って、電話は声の調子や大きさ、言葉づかいだけでお客さまへの印象が決まってしまいます。ですから、他の職種よりも話し方が面接の重要なチェックポイントになるのです。
<br>
人事担当者はあなたを不採用にするためでなく、採用するために面接をしています。緊張してしまいがちな面接ですが、リラックスして臨んでみてください。
</p>
  </section>
<!--- 段落１ 終了 --->  

<!--- 段落２ 開始 --->  
  <section class="sect02">
  <h2 class="sideBlueBorder_blueText02">面接の基本的なマナーって？</h2>
  <p class="marginBottom30px">せっかく条件に合ったアルバイトを見つけたのに面接で失敗した…なんてことにならないように、事前に基本マナー・よくある質問をおさえておきましょう。
</p>
    <h3 class="sideBlueBorder_blueText02">面接前にチェック</h3>
    <img src="/public/images/ccwork/004/main002.jpg" alt="面接前にチェック" />
<p class="dot_bluecircle_text">面接に行く前に忘れ物がないか、もう一度確認しましょう。本社と面接場所が違うことがあるので注意！ 住所・訪問担当者の部署名・氏名・電話番号などメモの準備、聞きたいこともまとめておくといいですよ。</p>
<table width="100%" border="0" class="bluet004">
  <tbody>
    <tr>
      <td  class="bluebox_lefttext_blue">持ち物<br>チェック</td>
      <td  class="bluebox_righttext_white">履歴書などの応募書類 / 筆記具 / 地図 / 訪問先の住所・担当者の部署・氏名・電話番号</td>
    </tr>
  </tbody>
</table>


<p class="dot_bluecircle_text">面接は第一印象で決まります。<span class="Blue_text">服装・髪型・お化粧は相手に不快感を与えない</span>よう注意しましょう。また、会社や仕事内容によっては、服装規定や「茶髪・ピアスNG」などの社内ルール決まっているところもあるので、事前に確認しておきましょう。</p>

<p class="dot_bluecircle_text">遅刻は厳禁ですが、早すぎる訪問もいけません。<span class="Blue_text">5分〜10分前に到着</span>するように心がけましょう。できれば早めに到着・場所を確認してから、近くのカフェや公園などで時間を潰すのがベストです。</p>

<p class="dot_bluecircle_text">万が一遅れてしまったり、やむを得ない事情で面接を受けられなくなった場合、<span class="Blue_text">わかった時点ですぐに連絡・謝罪し、事情を説明</span>しましょう。遅刻であればどのくらい遅れるかの連絡、面接を受けられない場合には面接日時を変えてもらえるかの確認を忘れずに。</p>

</section>
<!--- 段落２ 終了 --->  
  
<!--- 段落３ 開始 --->  
  <section class="sect03">
    <h2 class="sideBlueBorder_blueText02">面接時の注意</h2>
<img src="/public/images/ccwork/004/main003.jpg" alt="面接時の注意" />  
  <div class="contentsInner">
<p class="dot_bluecircle_text">面接直前の身だしなみチェックは忘れずに。「携帯電話の電源は切ったか」「時計のアラームなど、音が出るようなものはオフにしたか」「服装は、その仕事の面接にふさわしいか」「髪型や化粧は、不快感を与えないか」しっかり確認しましょう。</p>

<p class="dot_bluecircle_text">面接場所についたら、「アルバイトの面接で伺った○○です」と自分の名前、担当者の部署・名前を伝えます。待つように控え室に通されても、飲食・タバコはいけません。面接場所に着いた時点で面接は始まっていると考えましょう。</p>

<p class="dot_bluecircle_text">できるだけ<span class="Blue_text">相手の顔を見て姿勢良く</span>面接を受けます。「下を向いたままずっと目を合わさない」「きょろきょろしている」「足を組む・貧乏ゆすりをしている」「しきりに髪を触ったり、手が落ち着かない」こんな態度はNG。</p>

<p class="dot_bluecircle_text">面接では言葉のキャッチボールが大事です。<span class="Blue_text">相手の質問をよく聞き、それに対して素直に答える</span>よう心がけましょう。また、<span class="Blue_text">不安なこと、わからないことはどんどん質問する</span>のも大切です。仕事を始めてから「イメージと違った」と後悔しないよう、気になることは事前に聞いておきましょう。</p>

<p class="dot_bluecircle_text">面接終了時には元気よく「ありがとうございました」と最後まできちんとした対応を。ダラダラ歩く、ドアを乱暴に閉めるという態度はNGです。</p>


  </div><!-- / contentsInner -->
  </section>
<!--- 段落３ 終了 --->  

<!--- 段落４ 開始 --->
   <section class="sect04">
    <h2 class="sideBlueBorder_blueText02">面接でよくある質問</h2>

<div class="imageBlockB01">
<p class="image"><span class="bluefukidasi_bluetext">よく聞かれる質問</span>
      <img src="/public/images/ccwork/004/sub001.jpg"alt="よく聞かれる質問" class=" image_left" /></p>
<div class="contentsInner">
<div class="bluefukidasi">
<p class="bluefukidasi_bluetext_dot">この仕事・この会社を選んだ理由（志望動機）は？</p>
<p class="bluefukidasi_bluetext_dot">これまでに経験したことのあるアルバイトは？</p>
<p class="bluefukidasi_bluetext_dot">いつから働けますか？</p>
<p class="bluefukidasi_bluetext_dot">通勤時間はどのくらいかかりますか？</p>
<p class="bluefukidasi_bluetext_dot">週に何日・1日何時間くらい働けますか？</p>
<p class="bluefukidasi_bluetext_dot">あなたの長所・短所はなんですか？</p>
<p class="bluefukidasi_bluetext_dot">他に面接を受けている会社はありますか？</p>

</div><!-- / bluefukidasi -->
</div><!-- / contentsInner -->
</div><!-- / imageBlockB01 -->


<div class="imageBlockA01">
<p class="image">
<span class="pinkfukidasi_pinktext">こちらから確認したいこと</span>
<img src="/public/images/ccwork/004/sub002.jpg" alt="こちらから確認したいこと" class=" image_right" /></p>
<div class="contentsInner">
<div class="pinkfukidasi">
<p class="pinkfukidasi_pinktext_dot">具体的な仕事内容を教えてください。</p>
<p class="pinkfukidasi_pinktext_dot">どのくらいの人数が働いていますか？</p>
<p class="pinkfukidasi_pinktext_dot">働いている人は何歳くらいの人が多いですか？</p>
<p class="pinkfukidasi_pinktext_dot">休日はいつですか？(自由に決められますか？)</p>
<p class="pinkfukidasi_pinktext_dot">通勤時の交通費は支給されますか？</p>
<p class="pinkfukidasi_pinktext_dot">試用期間はありますか？</p>
<p class="pinkfukidasi_pinktext_dot">合否の結果はいつわかりますか？</p>
</div><!-- / bluefukidasi -->
</div><!-- / contentsInner -->
</div><!-- / imageBlockA01 -->


  <div class="contentsInner">
  </div><!-- / contentsInner -->
  <p class="marginB20">上記のように、志望動機、勤務時間の希望、日数などは必ず聞かれる質問です。ここで曖昧な返答をすると不採用になる可能性があるので、きちんと用意しておきましょう。採用する側は、やる気のある人、元気で素直な人に来てほしいと思ってるので、特に志望動機は面接官の立場になって「こんな人なら採用したい」と思える答えを考えましょう。</p>

  </section>
<!--- 段落４ 終了 --->
  
  <aside>
  <dl>
  <dt class="headingG01">「コールセンター基礎知識」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail02">コールセンターで働くメリットってなに？</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  
  <aside class="tabNav_ccwork">
  <ul id="js-tab01">
  <li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li>
  </ul>
  </aside><!-- / tabNav -->
  
  
</section>
