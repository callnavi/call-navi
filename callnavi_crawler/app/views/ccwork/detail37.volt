<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">基礎知識</span></div>
  <h1>私のテレアポ営業奮闘記～テレアポは怖くない！</h1>
  </header>
  
  <section class="sect01">
  <p class="marginBottom20px">こんにちは！テレアポでの電話営業を始めて、5ヶ月目のN美24歳です。<br />本日は私の〜テレアポ営業奮闘記〜をご紹介します。</p>
  <img src="/public/images/ccwork/037/main001.jpg" alt="「ストレスに負けない！」コールセンターバイト" class="marginBottom25px">
</section>
  
  <section class="sect02">
  <p class="marginBottom10px">私は現在通信系のコールセンターにてお客様からご契約をいただく、電話営業のアルバイトをしています。</p>
  <p class="marginBottom10px">最初は高時給に惹かれてこのアルバイトに応募をしました。<br />実は先月、お給料も少し上がったんです♪</p>
  <p class="marginBottom20px">時給の良さに魅力を感じているのは現在も変わりありませんが、今はそれ以上に仕事内容にやりがいを感じています。<br />少しずつテレアポ営業にも慣れてきて、お客様の新規開拓に力が入る今日この頃です！</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">テレアポ開始２週間～恐怖心～</h2>
  <p class="marginBottom10px">そんな私ですが…ここまでの５か月間、順風満帆というわけではありませんでした。<br />始めの２週間は、お客様に電話をすることに強い「恐怖心」を抱いていました。</p>
  <p class="marginBottom10px">私の所属するコールセンターでは、お客様の環境に合った通信商材をご紹介し、メリットを感じて頂いた方にご契約してもらう、という流れの営業をしています。</p>
  <p class="marginBottom10px">なので、もちろん「今はいいです～」と断られることも多く、ましてや新人の私は、知識や言いまわしも乏しかったので、余計お断りが続いてしまった時期でもありました。</p>
  <p class="marginBottom10px">その時抱いてしまったのが、電話での飛び込み営業に対する「苦手意識」です。<br />
「電話を切られるのが怖い…。」<br />
「お客さまに上手に説明できるかな…」</p>
  <p class="marginBottom10px">どのような業種や職種でも、うまく仕事をこなせるようになるまでは、どうしても時間がかかるものですよね。</p>
  <p class="marginBottom20px">私の場合こんな状況を救ってくれたのは、職場の先輩と、１冊の本でした。<br />詳細については、後ほど紹介していきます。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">テレアポ開始１ヵ月～克服</h2>
  <h3 class="blueblockBG">お客様がサービスを「使う」か「使わない」かの確認作業</h3>
  <p class="marginBottom10px">テレアポはお客様にとって、お得な情報をお伝えするテレマーケティングのお仕事です。</p>
  <p class="marginBottom10px">例えば通信系であれば、「今と全く変わらない通信環境でお値段だけ安くなるプランをご紹介します」や、商品販売系であれば「この商品があることによってお客様の生活が～の点で便利になります！」</p>
  <p class="marginBottom20px">など、必ずお客様になんらかのメリットのある情報をお届けしています。</p>
  <p class="marginBottom10px">研修中、チーフからのお言葉で、私の恐怖心を溶かしてくれた言葉がこちらです。</p>
  
  
  <ul class="fukidasi02_graybox">
<p class="marginBottom_none">まず、お客様にメリットのあるご案内をしている、ということを忘れないで欲しい。そのサービスを知らなかったがために、損をしているお客様もいるんだよ。そのサービスを知った上で、契約をするかしないかはもちろんお客様の決める事だ。Ｎ美の仕事は、情報を伝えて、お客様がそれを「使う」か「使わない」かの確認をとることなんだよ。たから例えいらないって言われても、落ち込むことはないよ。全員に気に入ってもらえるサービスなんて世界中探してもないからね！でも、1人でも多くの人に、お得な情報をお伝えして、お客様の「選択肢」を広げてあげたいよね！Ｎ美が情報を伝えることで、現状より良い状態になって、喜ぶお客様が沢山いるんだよ！</p>
</ul>

  <p class="marginBottom40px">この話を聞いて、「売らなきゃ！」と力が入りすぎていたかも…と感じました。<br />「確認作業」と考えると、断れることが一気に怖くなくなったのです！！<br />断られても「あ、このお客様は利用しないのか～残念！」程度の感覚に変わりました。</p>
  </section>
  
  <section class="sect03">
  <h3 class="blueblockBG">相手がどう思うかは考えない</h3>
  <p class="marginBottom20px">テレアポを始めたばかりの頃、自分の電話を受けたお客様の心境ばかりを考えてしまっていた時期がありました。例えば「この電話を面倒くさいと思っているかなぁ」「早く切りたいと思ってそうだなぁ」といった具合に、本当に思っているのかどうかわからないことをイチイチ考えてしまっていたのです。</p>
  <p class="marginBottom10px">そんなとき、フラっと立ち寄った最寄りの本屋さんで、私の意識を変える一冊の本と出会いました。その本に記載されていた一部分をご紹介致します。</p>
<p class="blueblockBG_title marginBottom10px">「 相手がどう思っているか、は私の考える問題ではない 」<br />※「嫌われる勇気―――自己啓発の源流「アドラー」の教え」より</p>
  <p class="marginBottom10px">本書の中では、これを「課題の分離」と呼んでいました。常に「これは誰の課題」なのかを考えることで、「自分の課題」と「他者の課題」を分け、「他者の課題」には踏み込まないようにするのです。</p>
  <p class="marginBottom10px">例えば、お客様に営業電話をして、お客様がこの営業電話に対してどう思うか―これは私にどうこうできる問題ではありませんよね。「お客様がどう思うか、この商品をいいと思うか、契約するかしないか」＝他者の課題であり、私の考えるべき問題ではありません。関与できないならば、最初からその問題に関しては「切り捨てる」必要がある、というようなことが書かれていました。</p>
  <p class="marginBottom30px">また、相手の出方を気にしてしまうのは自分自身の「承認欲求」を満たしたいからとのことです。その承認欲求を他者からの承認で満たすのはナンセンスです。自分で自分を承認できるようになりましょうね。というようなことも合わせて書かれていました。</p>
  <p class="marginBottom10px">正に、目から鱗が落ちるとは、こういうことか！と実感した瞬間でした。<br />テレアポをしながら「他者の課題」ばっかり気にしていた自分に気づいたのです。テレアポのアルバイトで悩みを抱えたからこそ、出会えた本でしたが、プライベートにおいても使用できる考え方だと思いました。</p>
  <p class="marginBottom10px">最初はマニュアルを棒読み気味だった私も、今ではアドリブを効かせての対応もできるようになってきました。テレアポのお仕事はとにかく実践を積み、様々なお客様とお話し、自分の中にモデルケースを沢山持つことだと思います。<br />営業成績は、その先についてくるはずです！！最近は将来、キラキラした営業マン（ウーマン！？）になるのもいいなぁ、と考え始めるなど、テレアポは私の人生の選択肢を増やしてくれています！</p>
  <p class="marginBottom40px">ビジネスマナーやスキルが身に付くテレアポのお仕事、ぜひあなたにもおすすめです！
</p>
  </section>

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター基礎知識」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail13">NGワードとクッション言葉に気をつけよう！</a></li>
  <li><a href="/ccwork/detail23">日本語を正しく使えていますか？コールセンター業務に必要な丁寧語、謙譲語、尊敬語</a></li>
  </ul></dd>
  </dl>
  </aside>
  
<aside class="tabNav_ccwork">
<ul><li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_a.png" alt="基礎知識" class="mediaPC" height="" width="192"><img src="/public/images/ccwork/nav/tabnav_01_03_a.png" alt="基礎知識" class="mediaSP" height="" width="286"></a></li>
<li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" alt="知っ得情報" class="mediaPC" height="" width="192"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" alt="知っ得情報" class="mediaSP" height="" width="286"></a></li>
<li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" alt="働く人の声" class="mediaPC" height="" width="192"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" alt="働く人の声" class="mediaSP" height="" width="286"></a></li></ul>
</aside><!-- / tabNav -->
  
</section>