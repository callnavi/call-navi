<section class="entryA01 ccwork_entry">
<header>
<div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">働く人の声</span></div>
<h1>女性SVに聞く<br />～コールセンターのお仕事あれこれ～</h1>
</header>
  
<section class="sect01">
<p class="marginBottom20px">コールセンターで働く人の中には<span class="Blue_text">SV（スーパーバイザー）</span>としてキャリアアップする人もいます。SVになると、プレーヤーとしての実力を求められるだけでなく、部下の育成・指導や目標達成にむけたチーム全体の改善など、管理するものもプレッシャーも増えてくるはず。<span class="Blue_text">コールセンターは「営業」！</span>強いメンタルも求められ、まだまだ女性の責任者が少ないのも現状です。そこで今回は営業最前線で頑張る女性SVに聞いた、お仕事のあれこれをまとめました。</p>
<img src="/public/images/ccwork/078/main001.jpg" class="imagemargin_T10B30" alt="女性SVに聞く～コールセンターのお仕事あれこれ～" />

<h2 class="sideBlueBorder_blueText02 marginBottom10px">私たちは、これでSVになることを決めました！</h2>
<p class="marginBottom10px">誰でも最初はアポインターからのスタート。そこからSVとしてキャリアアップしたきっかけを聞きました。</p>
<div class="freebox01_pinkBG_wrapper marginBottom40px">
<p class="marginBottom_none"><span class="Pink_text">■</span>私は負けず嫌いな性格。根拠はなかったのですが、責任者に昇格してもやっていける自信がありましたし、期待に応えたいと思いました。<span class="text_small">（26歳／前職 人事）</span></p>
<p class="marginBottom_none"><span class="Pink_text">■</span>入社して半年くらい経った頃、上司に昇格の話し頂きました。不安はありましたが、自分の力を試したかったのと新しい事にチャレンジしたかったので決めました。<span class="text_small">（29歳／前職　営業）</span></p>
<p class="marginBottom_none"><span class="Pink_text">■</span>新事務所立ち上げと同時に入社した私。人数が増えて来た頃に、チームの新人トレーナーを任されました。そこから人をまとめたり、育てたりすることが楽しいな、と感じSVにチャレンジしようと決めました。<span class="text_small">（24歳／前職　保育士）</span></p>
</div>

<img src="/public/images/ccwork/078/sub_001.jpg" class="imagemargin_T10B30" alt="アポインター時代とはここが違う" />
<h2 class="sideBlueBorder_blueText02 marginBottom10px">アポインター時代とはここが違う</h2>
<p class="marginBottom10px">SVになってから気づいた、自分のアポインター時代との違いについて聞いてみたところ、手応えを感じている人が多そうです。</p>
<div class="freebox01_pinkBG_wrapper marginBottom40px">
<p class="marginBottom_none"><span class="Pink_text">■</span>責任が増えましたね。チームでの達成がミッションになりましたから、自分の成績だけではなく、予算を持ち、達成させることを意識するようになりました。<span class="text_small">（28歳／前職 アパレル） 人事）</span></p>
<p class="marginBottom_none"><span class="Pink_text">■</span>SVになってから視野や自分のキャパが広がりました。また会社の利益や売上も意識する様になりました。<span class="text_small">（30歳／前職 営業）</span></p>
</div>

<img src="/public/images/ccwork/078/sub_002.jpg" class="imagemargin_T10B30" alt="SVってこんなに大変・・・" />
<h2 class="sideBlueBorder_blueText02 marginBottom10px">SVってこんなに大変・・・</h2>
<p class="marginBottom10px">その立場に立ってみて、初めてその楽しさや辛さを痛感するものです。SVという立場になってみて初めて経験する苦労や苦悩・・・色々あるようです。</p>
<div class="freebox01_pinkBG_wrapper marginBottom40px">
<p class="marginBottom_none"><span class="Pink_text">■</span>部下一人一人の悩みに耳を傾けることがとても大変です。アポインター時代も大変なことはありましたが、今度は、それを解決してあげる立場になりますし、それぞれの悩みがあるので・・・。人数が多くなるにつれ、悩みの数も増えます。<span class="text_small">（25歳／前職 スポーツ関連）</span></p>
<p class="marginBottom_none"><span class="Pink_text">■</span>目標達成のためには部下のマネージメントが必須です。なので、部下には、どう伝えれば上手く相手に理解してもらえるか日々考えながら指導をしています。一緒に問題解決していくことも、大変な分、解決したときの喜びや達成感が大きいです。<span class="text_small">（22歳／前職　営業アシスタント）</span></p>
</div>

<img src="/public/images/ccwork/078/sub_003.jpg" class="imagemargin_T10B30" alt="やっていて良かった！一番多かったのは「ありがとう」の声" />
<h2 class="sideBlueBorder_blueText02 marginBottom10px">やっていて良かった！<br />一番多かったのは「ありがとう」の声</h2>
<p class="marginBottom10px">大変なことも多いSVのお仕事。でもその半面やりがいも大きいようです。</p>
<div class="freebox01_pinkBG_wrapper marginBottom40px">
<p class="marginBottom_none"><span class="Pink_text">■</span>部下の成績を上げなければという責任は大きいです。そのため、日々指導を重ね、部下が達成できたときの喜びは大きいです！「〇〇さんのおかげで、ここまでできました。」と喜びの報告を受けた時は本当に嬉しかったですね！<span class="text_small">（27歳／前職 営業）</span></p>
<p class="marginBottom_none"><span class="Pink_text">■</span>部署が達成した時や自分の部署のアポインターが達成した時は、毎回自分の事の様にうれしいです！<span class="text_small">（25歳／前職　事務）</span></p>
<p class="marginBottom_none"><span class="Pink_text">■</span>うちの会社は、責任者以上の社員旅行等があったり、役職の手当がついたりとロイアリティが充実しています。SVでも、達成したらインセンティブが入りますし、責任者になってからは良かったこと尽くし（笑）モチベーションが上がる事が沢山あります！<span class="text_small">（28歳／前職　飲食）</span></p>
<p class="marginBottom_none"><span class="Pink_text">■</span>説明が全て終わった後に、お客様から「ありがとうございました」とお礼を言われた時です。これまで頑張ったことが認められたようで、やっていて良かったなと思います！<span class="text_small">（22歳／前職　アパレル）</span></p>
<p class="marginBottom_none"><span class="Pink_text">■</span>日々数字を追いかけるあまり、気持ちが熱くなってしまう事もあります。基本、冷静な性格なので意外と言われる事もありますが新たな自分を発見したようです。<span class="text_small">（26歳／前職 営業アシスタント）</span></p>
<p class="marginBottom_none"><span class="Pink_text">■</span>未経験で入社し、まさか自分が責任者になるなんて思ってもいなかったというのが正直なところです。入社当時に比べたら今までにないくらいのスピードで成長できましたし仕事にこんなにやりがいを見いだせるなんて想像もしていなかったです。<span class="text_small">（23歳／前職　アルバイト）
</span></p>
</div>

<img src="/public/images/ccwork/078/sub_004.jpg" class="imagemargin_T10B30" alt="ずばり、女性が向いていると思う？" />
<h2 class="sideBlueBorder_blueText02 marginBottom10px">ずばり、女性が向いていると思う？</h2>
<p class="marginBottom10px">SV経験のある人に、ずばり聞いてみました。<br />人それぞれ考え方は違うようなのでこれからSVになる人は参考にしてみてくださいね。</p>
<div class="freebox01_pinkBG_wrapper marginBottom40px">
<p class="marginBottom_none"><span class="Pink_text">【向いているという意見】</span></p>
<p class="marginBottom_none"><span class="Pink_text">■</span>電話営業は女性の方が向いている。部下への接しかたも、仕事に関しても、やはり女性の方が細かいと思います。<span class="text_small">（26歳／前職 人事）</span></p>
<p class="marginBottom_none"><span class="Pink_text">■</span>テレマは顔が見えないので声が重要になりますので、男性よりも声が高い女性の方が有利のような気がします。<span class="text_small">（24歳／前職　保育士）</span></p>
</div>
<div class="freebox01_blueBG_wrapper marginBottom40px">
<p class="marginBottom_none"><span class="Blue_text">【どちらとも言えないという意見】</span></p>
<p class="marginBottom_none"><span class="Blue_text">■</span>一概には言えないですね。でも、そこは、営業！頑張った文だけ結果がついてくるのでやりがいはありますし成長もします。ただ、電話口でキツイことを言われたり、プレッシャーもあったりするので、メンタルと体力に自信のある方であれば向いていると思います。<span class="text_small">（30歳／前職 営業）</span></p>
</div>

<img src="/public/images/ccwork/078/sub_005.jpg" class="imagemargin_T10B30" alt="多かったのは、女性の責任者を増やしたいという意見" />
<h2 class="sideBlueBorder_blueText02 marginBottom10px">多かったのは、女性の責任者を増やしたいという意見</h2>
<p class="marginBottom10px">最後に、SVとして、働く環境について求めることを伺いました。</p>
<div class="freebox01_pinkBG_wrapper marginBottom40px">
<p class="marginBottom_none"><span class="Pink_text">■</span>男性責任者に比べると、女性責任者がまだまだ少ない現状なので、営業でも女性が負担なく活躍できる様な制度があれば良いと思います。<span class="text_small">（35歳／前職　保険営業）</span></p>
<p class="marginBottom_none"><span class="Pink_text">■</span>女性責任者を増やしたいですね。ああなりたいと、具体的な目標になったり、目指す人が多くなったりするはず、と思います。<span class="text_small">（27歳／前職 営業）</span></p>
<p class="marginBottom_none"><span class="Pink_text">■</span>体の不調やお客さんや周りのスタッフとの接し方など、同性でないと話しづらいこともあるので、もっと女性責任者が増えるといいと思います。<span class="text_small">（25歳／前職　事務）</span></p>
</div>

<p class="marginBottom40px sectborder2">いかがでしたか？<span class="Blue_text">新たなことにチャレンジしたい</span>という理由からSVを目指す女性が多いようですね！目標数字を達成するという喜びはもちろん、きめ細かい気配りで、部下を育てたり、お客様から感謝の言葉をいただいたりする点に<span class="Blue_text">やりがいを感じる</span>という意見が多かったです。アポインターの女性の皆さんも、そろそろ別の道に進もうかな、新しいことにチャレンジしてみたいなと迷ったら、SVを目指してみてはいかがでしょうか！</p>
</section>

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター働く人の声」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail57">結婚・引越し・出産があっても大丈夫！コールセンターの魅力とは？</a></li>
  <li><a href="/ccwork/detail58">コールセンターバイトで、就職活動を有利に！　電話対応、コミュ力、ビジネスマナーは最大の武器！</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  <aside class="tabNav_ccwork">
  <ul id="js-tab01">
  <li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_a.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_a.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li>
  </ul>
  </aside><!-- / tabNav -->
  
  
</section>
