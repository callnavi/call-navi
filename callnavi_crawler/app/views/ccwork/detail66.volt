<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">知っ得情報</span></div>
  <h1>保険関係のコールセンターで行う４つの業務とは？</h1>
  </header>
<section class="sect01">
<img src="/public/images/ccwork/066/main001.jpg" alt="保険関係のコールセンターで行う４つの業務とは？"  class="imagemargin_B25"/>
<p class="marginBottom10px">コールセンターと一言でいっても、アウトバウンド(発信)・インバウンド(受信)の違いから始まり、カスタマーサポートや受注窓口、料金の督促、ヘルプデスク、などさまざまな種類に分けられます。それぞれどんな仕事内容で、どんなお客様をご案内するのでしょうか？</p>
<p class="marginBottom20px">今回は<span class="Blue_text">保険関係のコールセンターについてご紹介</span>いたします！大手保険会社のコールセンターは、ネームバリューがあってなんだか安心感がありますし、興味を持たれる方も多いのではないでしょうか？でも実際働くとなるとどんな業務があるのか、いまいちピンときませんよね。どんなお仕事内容なのかチェックしておきましょう！</p>

<h2 class="sideBlueBorder_blueText02 marginBottom20px">主な業務内容とは？</h2>
<img src="/public/images/ccwork/066/sub_001.jpg" alt="主な業務内容とは？"  class="imagemargin_B25"/>
<div class="freebox03_grayBG">
<p class="marginBottom_none"><span class="Blue_text">①資料郵送の確認</span></p>
<p class="marginBottom_none">お客様に保険商品の資料をお送りしてもいいか確認していきます。有名な保険会社なら、お客様からキツく断られる心配もあまりありません。ガツガツと「営業」という感じではなく、お客様によりそってご提案していくというお仕事です。</p>
<p class="marginBottom_none"><span class="Blue_text">②郵送した資料の到着確認や内容確認</span></p>
<p class="marginBottom_none">郵送した資料はきちんとお客様に届いているか、内容に不備はないか、を確認します。また、申込書の書き方についての問い合わせも多いみたいです。</p>
<p class="marginBottom_none"><span class="Blue_text">③手続きのご案内</span></p>
<p class="marginBottom_none">入会の手続きや、保険料の支払いについての手続きなど、対応に少し専門的な知識が必要になります。ですが必ず研修期間で知識を身につけることができますし、自分での対応が難しいときにはSV(スーパーバイザー)に代わってもらえば大丈夫です。</p>
<p class="marginBottom_none"><span class="Blue_text">④受付</span></p>
<p class="marginBottom_none">保険に加入されているお客様が事故に巻き込まれたり、病気になってしまったりしたときにまず対応する窓口です。お客様から事故状況などの詳細情報のヒアリングを行い、データ入力などをします。お客様の中にはパニックになってしまっている方もいるので、落ち着いてもらえるよう、親身になってお話を聞くことが大事です。</p>
</div>
<p class="marginBottom10px">おおまかに言うとこのような業務が行われています。求人募集に出ている仕事内容をよく確認して、どんなことをするのかイメージしておきましょう！</p>

<h2 class="sideBlueBorder_blueText02 marginBottom20px">こんな方でも大丈夫！</h2>
<img src="/public/images/ccwork/066/sub_002.jpg" alt="こんな方でも大丈夫！"  class="imagemargin_B25"/>
<p class="dot_underline">初めてコールセンターでバイトをする方</p>
<p class="marginBottom10px">保険会社は信用が大事！アルバイトさんにもきちんと教育してくれますので安心です。言葉遣いはもちろん、商品知識なども研修で教えてもらえますので、保険の勉強にもなるかも？！</p>
<p class="dot_underline">保険のことが、まったくわからないという方</p>
<p class="marginBottom40px">先程書いたように、会社の信用に関わりますので研修等で必ずフォローしてくれます。そもそも、「保険に詳しい！」という方の方が少ないのでご安心を。問い合わせ対応等には、多岐にわたる保険のパターンに加えて専門的な知識が必要となりますが、業務内容によっては簡単な保険の知識さえあれば問題ないお仕事もありますよ♪</p>
</section>

<aside>
  <dl>
  <dt class="headingG01">「コールセンター知っ得情報」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail72">テンションが高くない人必見！コールセンターバイトでは、無理にテンションを上げる必要はない！？</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  
  <aside class="tabNav_ccwork">
  <ul>
  <li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_a.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_a.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li>
<li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li>
  </ul>
  </aside><!-- / tabNav -->
  
  
</section>
