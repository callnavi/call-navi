<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">知っ得情報</span></div>
  <h1>コールセンター心理学！<br />これであなたもコミュニケーションの達人に！</h1>
  </header>
  
<!--- 段落１ 開始 --->  
  <section class="sect01">
  <img src="/public/images/ccwork/044/main001.jpg" class="imagemargin_T10B10" alt="友達同士でバイトに応募！他にはない！？コールセンターのメリット" />
  <p class="marginBottom20px">心理学と聞くと、なんだか難しそう…と思ってしまうかもしれません。ですが<span class="Blue_text">心理学は、人の「行動」や「意識」などを解明していく学問で、コミュニケーションのヒントを与えてくれることもあります。</span>今回は、コールセンターでの心理学活用方法について、紹介します。</p>

  <div class="yellowmiddlecircle2">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/044/icon_sp_001.jpg"  class="yellowmiddlecircle_titleimg2" /></p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">フット・イン・ザ・ドア</p>
  </div>  
  <p class="marginBottom20px">この名称の由来は、営業マンが訪問先でつま足をドアに入れて閉まらないようにして、話を聞いてもらったことにあります。<span class="Blue_text">ハードルの低いお願いから徐々に段階を踏んで、より高い要求を承諾してもらうために使われる営業テクニック</span>として有名です。</p>

    <h3 class="orangeblockBG">実用例</h3>
  <p class="marginBottom20px"><span class="text_bold_b">Lv.1「少しだけお時間いただいてもいいですか？」</p>
  <p class="marginBottom20px"><span class="text_bold_b">Lv.2「この商品のポイントを説明してもいいですか？」</p>
  <p class="marginBottom20px"><span class="text_bold_b">Lv.3「お試しで使ってみませんか？」</p>
  <p class="marginBottom20px"><span class="text_bold_b">Lv.4「よければこのまま使ってみませんか？」</p>
  <p class="marginBottom30px">少しずつお願いのハードルを上げていき、最終的に商品の購入の問い（クロージング）までこぎつけています。最初の「少しだけお時間いただいてもいいですか？」は、この最終目標へのいわば布石です。</p>

  <div class="yellowmiddlecircle3">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/044/icon_sp_002.jpg"  class="yellowmiddlecircle_titleimg3" /></p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">セルフ・マニピュレーション</p>
  </div>  
  <p class="marginBottom10px">プレゼンテ―ションを行う人の自信が、態度や雰囲気に表れていれば、そのプレゼンテーションは成功したも同然です！逆にプレゼンの内容がいくら素晴らしくても、プレゼンを行っている人がボソボソと小声で喋っていたり、無意識に体や髪をいじったりしていると、「自信がなさそう」「あやしい」などと思われてしまいます。</p>
  <p class="marginBottom20px">
このように<span class="Blue_text">態度やしぐさで自信を表現することを、セルフ・マニピュレーションといいます。</span>コールセンターのお仕事は対面ではないため、声のみで判断されます。こちらの自信が伝わるようこころがけて、お客様に話しかけましょう。</p>

  <p class="dot_yellowtext_Number">1</p>
  <p class="dot_yellowtext_title">聞き取りやすい声で、はっきりと話す</p>
  <p class="marginB20">これは基本中のキホンです。特に自分の名前を名乗ったり、要件を伝えたりするさいには、必ず相手に伝わっていなければ意味がありません。</p>
  <p class="dot_yellowtext_Number">2</p>
  <p class="dot_yellowtext_title">早口になりすぎない</p>
     <p class="marginB20">伝えたい内容がたくさんあって、ついつい早口になってしまうこともあるかもしれませんが、焦っているように聞こえてしまうこともあります。こちらは「テンポよく」話したつもりでも、相手には「早口」ととられてしまうこともあるので、要注意です。</p>
  <p class="dot_yellowtext_Number">3</p>
  <p class="dot_yellowtext_title">質問された時がチャンス！自信を持ちすぐ答える</p>
     <p class="marginB20">お客様から質問される度におどおどしていては、信頼されるチャンスを逃しているようなもの。質問されたときこそチャンスだと思い、自信を持って、ハキハキと答えましょう！</p>

  <div class="yellowmiddlecircle4">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/044/icon_sp_003.jpg"  class="yellowmiddlecircle_titleimg5" /></p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">両面提示と片面提示</p>
  </div>  
  <p class="marginBottom10px">物事の一面性のみを伝えることを「片面提示」と言い、物事の二面性を伝えることを「両面提示」と言います。</p>
  <p class="Blue_Btextbold">■「こちらの商品は、今ならこのサービスを無料でおつけします！」</p>
  <p class="Blue_Btextbold">■「こちらの商品は、今ならこのサービスを無料でおつけします！ただし、半年後からの利用は有料です。」</p>

  <p class="marginBottom10px">前者は相手にとってのメリットしか提示されていませんが、後者はメリットとデメリットの両方がきちんと提示されています。</p>
  <p class="marginBottom10px">売る側の立場で考えると、極力デメリットは伝えたくないと思われがちですが、メリットばかり伝えて、「そんな上手い話があるの？」と疑われてしまっては、せっかくの商談も台無しになってしまいます。また、デメリットについて言った、言わないでクレームにつながる可能性もあります。</p>
  <p class="marginBottom10px">一方、相手が提案に不信感を持っているからこそ、初めからデメリットも提示しておくことで、安心感をもってもらえる場合もあります。<span class="Blue_text">相手の知りたい情報を見極めて、メリットとデメリットをバランスよく提示していくことが大事</span>ですね。</p>

  <div class="yellowmiddlecircle5">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/044/icon_sp_004.jpg"  class="yellowmiddlecircle_titleimg4" /></p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">クローズド・クエスチョンチョン</p>
  </div>  
  <p class="marginBottom10px">質問したときに「YES or NO」もしくは「A or Ｂ」のように<span class="Blue_text">二者択一で答えてもらう質問の仕方を、クローズド・クエスチョンといいます。</span>反対に「それはどこ？(Where)」「どう思う？(What)」のように<span class="Blue_text">相手に自由に答えてもらう質問の仕方を、オープン・クエスチョンといいます。</span></p>
  <p class="Blue_Btextbold">■「この内容はご存じでしたか？」</p>
  <p class="Blue_Btextbold">■「ここまでの説明は大丈夫でしょうか？」</p>
  <p class="Blue_Btextbold">■「こちらの商品でお間違えないですか？」</p>
  <p class="marginBottom10px">このように相手の同意を得たり、認識に関して確認したりすることで、安心感を与えることが出来ます。そうはいっても、余りに質問を重ねてばかりだと会話が広がらず、尋問されているように感じられてしまうこともあります。使い方としては、<span class="Blue_text">クローズド・クエスチョンで相手との信頼関係を築いて、オープン・クエスチョンで会話を広げるのが良いでしょう。</span></p>

  <div class="yellowmiddlecircle6">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/044/icon_sp_005.jpg"  class="yellowmiddlecircle_titleimg5" /></p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">バックトラッキング</p>
  </div>  
  <p class="marginBottom10px">バックトラッキングとは相手の言葉を繰り返すことで共感を示し、会話を引き出すコミュニケーションテクニックです。いわゆる「オウム返し」というやつですね。</p>

    <img src="/public/images/ccwork/044/talk_001.jpg" class="marginBottom_none" alt="「私は△△の商品を使っています」" />
    <img src="/public/images/ccwork/044/talk_002.jpg" class="marginBottom_none" alt="「△△の商品をお使いなんですね」" />
    <img src="/public/images/ccwork/044/talk_003.jpg" class="marginBottom_none" alt="「ええ、この商品に変えてからとっても調子が良いんです」" />
    <img src="/public/images/ccwork/044/talk_004.jpg" class="marginBottom_none" alt="「調子が良いんですか！それはよかったですね」" />
    <img src="/public/images/ccwork/044/talk_005.jpg" class="marginBottom_none" alt="「１年前に買ったばかりの洗濯機なんですけど、もう調子が悪くて…。主人にはまだ動くだろって言われるんですけどね。たまに途中で止まっていたりするので結局二度手間になって買い物に行く時間も無くなるんです」" />
    <img src="/public/images/ccwork/044/talk_006.jpg" class="marginBottom20px" alt="「洗濯機の調子が悪くて時間を取られてしまうんですね。それは困りますね」" />

  <p class="marginBottom10px">前者のように相手の言葉をそのまま返し、感情に同意し、肯定することで「話を聞いてくれている」「この人になら話しやすい」と思ってもらいやすくなりますが、やりすぎは良くありません。<span class="Blue_text">話を聞いていることを認識してもらうためにも、時々、相手の言葉を要訳して返すなど、コミュニケーションを工夫していきましょう。</span></p>
  <p class="marginBottom40px">このようにして話をしやすい相手として信頼関係を築いていくことで、スムーズな会話のやりとりができるようになります。ビジネスでも、普段の会話でも、活かせるこのテクニック、ぜひ活かしてみてください。</p>

  </section>
<!--- 段落１ 終了 --->

<aside>
  <dl>
  <dt class="headingG01">「コールセンター知っ得情報」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail18">学歴なんて関係ない！コールセンターなら、実力次第で高収入が目指せる</a></li>
  <li><a href="/ccwork/detail46">コールセンター三種の神器！　パソコン、ヘッドセット、顧客管理ソフトは、こうなっています！</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  
  <aside class="tabNav_ccwork">
  <ul>
  <li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_a.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_a.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li>
<li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li>
  </ul>
  </aside><!-- / tabNav -->
  
  
</section>

