<section class="entryA01 ccwork_entry">
<header>
<div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">働く人の声</span></div>
<h1>カラオケ上手は、テレアポ上手！？<br />意外な共通点と、心に刺さるトークのコツとは？</h1>
</header>
  
<section class="sect01">
<p class="marginBottom20px">懇親会・送別会といった会社のイベントや、飲み会の２次会などに利用することの多いカラオケですが、カラオケの上手な人は、コールセンターでも優秀な成績を出しているってご存知でした？</p>
<img src="/public/images/ccwork/079/main001.jpg" class="imagemargin_T10B30" alt="カラオケ上手は、テレアポ上手！？意外な共通点と、心に刺さるトークのコツとは？" />

<h2 class="sideBlueBorder_blueText02 marginBottom10px">カラオケ好きですか？</h2>
<p class="marginBottom10px">仕事帰りに職場の友達と、久しぶりにあった友達と、デートやファミリーでもカラオケに行く機会がありますよね。最近は、ニンテンドーDSやiPhoneアプリでもカラオケができてしまうので、わざわざDAMやJOYSOUNDなどに行く必要はない、なんていう人もいますが、「カラオケボックスでワイワイワイワイガヤガヤ」の楽しさは、一回行くと癖になってしまいそうです。</p>
<p class="marginBottom20px">最近のランキングや人気の曲をチェックしてみると、「ひまわりの約束（秦基博）」「R.Y.U.S.E.I.【本人出演】（三代目 J Soul Brothers from EXILE TRIBE）」「糸（中島みゆき）」「君がくれた夏（家入レオ）」「ハナミズキ【本人出演】（一青窈）」などがあります。カラオケ好きなら、一度はうたった曲もあるかも知れません。</p>

<h2 class="sideBlueBorder_blueText02 marginBottom10px">カラオケが上手とは？</h2>
<p class="marginBottom20px">友人や職場の仲間とカラオケに行くと、必ずと言っていいほど上手な人がいますが、カラオケが上手な人はどんな人でしょうか？<br />声量/迫力がある歌い方・心に響くバラード・友達だからこそ響くものなどあるかと思いますが、単純に採点カラオケで高得点というケースも多いかも知れません。テレビ番組の企画でもありますが、採点カラオケで高得点を出すと、とても気分が良く、盛り上がりますよね。</p>
<img src="/public/images/ccwork/079/sub_001.jpg" class="imagemargin_T10B30" alt="カラオケが上手とは？" />

<h2 class="sideBlueBorder_blueText02 marginBottom10px">採点カラオケで高得点を出すポイントとは？</h2>
<div class="freebox01_blueBG_wrapper">
<p class="marginBottom_none"><span class="Blue_textbold">音程を合わせる</span></p>
<p class="marginBottom_none">歌ですので、楽譜に合わせて音程を合わせて歌うことが必要です。出ない音があるときには、無理せずキーを下げたり、オクターブ下で歌ったりすることがポイントです。音痴とまではいかなくても、メロディーとボーカルを合わせることは大切です。</p>
</div>
<div class="freebox01_blueBG_wrapper">
<p class="marginBottom_none"><span class="Blue_textbold">お腹から声を出す</span></p>
<p class="marginBottom_none">喉で歌うと、音程の幅も出にくいし、喉を痛めてしまう可能性があります。楽器の演奏など<span class="Blue_text">腹式呼吸</span>がいいと言われますが、歌も一緒です。お腹で呼吸するように、意識してうたいましょう。必要なら準備運動をするもおすすめです。</p>
</div>
<div class="freebox01_blueBG_wrapper">
<p class="marginBottom_none"><span class="Blue_textbold">抑揚をつける</span></p>
<p class="marginBottom_none">その歌を聞いて感動するか、心に響くかということは、歌そのものの上手下手もありますが、抑揚がポイントです。<span class="Blue_text">歌詞に込められた感情を声の大きさ(抑揚)でも表現</span>することになりますが、採点カラオケなので、ちょっと大げさにするくらいでちょうどいいかも知れません。</p>
</div>
<img src="/public/images/ccwork/079/sub_002.jpg" class="imagemargin_T10B30" alt="分かりやすいトークとは？" />
<h2 class="sideBlueBorder_blueText02 marginBottom10px">分かりやすいトークとは？</h2>
<p class="marginBottom10px">ここで、お客様にとって分かりやすい話し方（トーク）についてみていきます。</p>
<div class="yellowmiddlecircle">
<p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />ポイント</p>
<p class="yellowmiddlecircle_titlenumber">1</p>
</div>
<div class="yellowmiddlecircle_titlebox">
<p class="yellowmiddlecircle_titletextblue">話が論理的で、順序立てている。又はストーリーに従って話している。</p>
</div>

<div class="yellowmiddlecircle">
<p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />ポイント</p>
<p class="yellowmiddlecircle_titlenumber">2</p>
</div>
<div class="yellowmiddlecircle_titlebox">
<p class="yellowmiddlecircle_titletextblue">難しい表現を使わずに、極端な話中学生でも分かるような言葉で話している。</p>
</div>

<div class="yellowmiddlecircle">
<p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />ポイント</p>
<p class="yellowmiddlecircle_titlenumber">3</p>
</div>
<div class="yellowmiddlecircle_titlebox">
<p class="yellowmiddlecircle_titletextblue">抑揚をつけて、話のポイントを分かりやすく話している。</p>
</div>
<p class="marginBottom10px">上記のような話し方をすれば、お客様の頭の中でイメージしやすく、ポイントも分かりやすいのではないでしょうか。</p>
<p class="marginBottom10px">論理的な話し方や、簡単な言葉で話すことは、スクリプトと言われるトークの台本でカバーできていると思われますが、<span class="Blue_text">抑揚に関しては読む人によって変わってきます。</span></p>
<p class="marginBottom20px">あまりにも強弱が極端だと、聞こえにくかったり、脅迫めいて聞こえてしまったりしてしまうかも知れないし、強弱が分かりにくいと話のポイントが分かりにくくなってしまいます。</p>

<h2 class="sideBlueBorder_blueText02 marginBottom10px">カラオケとテレアポの関係</h2>
<p class="marginBottom10px">カラオケとテレアポの両方でポイントとなっていたことは何でしょうか？<br />それは抑揚です。人は基本的に長い話は聞けず、その中でも自分の都合のいいところしか聞いていないと言われます。ですから、この部分は特に大事ですよ！というのを抑揚つけて表現してあげることによって、相手に聞いてもらいやすくなります。<br />これは、カラオケでもテレアポでも一緒で、強調したいところを相手に分かりやすい様に強調することがポイントです。</p>
<p class="marginBottom20px">ですから、友人や職場の仲間とカラオケに行ったとき、ボーカル的にはあまり上手ではないけど、心に響くなぁ、と思ったら、抑揚に着目して聞いてみましょう。意識、無意識に関わらず、抑揚をつけて歌っていることでしょう。</p>

<h3 class="blueblockBG">最後に</h3>
<img src="/public/images/ccwork/079/sub_003.jpg" class="imagemargin_T10B30" alt="まとめ" />
<p class="marginBottom40px">今回は分かりやすいポイントとして、カラオケを例に出しましたが、カラオケに行かなくても、抑揚をつけた話し方は練習できます。アーティストになったつもりで、テレアポの時も抑揚をつけて表現してみてはいかがでしょうか。</p>
</section>

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター働く人の声」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail57">結婚・引越し・出産があっても大丈夫！コールセンターの魅力とは？</a></li>
  <li><a href="/ccwork/detail58">コールセンターバイトで、就職活動を有利に！　電話対応、コミュ力、ビジネスマナーは最大の武器！</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  <aside class="tabNav_ccwork">
  <ul id="js-tab01">
  <li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_a.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_a.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li>
  </ul>
  </aside><!-- / tabNav -->
  
  
</section>
