<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">基礎知識</span></div>
  <h1>性格まで明るくなっちゃう？<br />コールセンターで充実ライフを手に入れよう！</h1>
  </header>

<!--- 段落１ 開始 --->
  <section class="sect01">
  <p class="marginBottom_none">コールセンターで働くなんてこれまで考えたことなかった、というコールセンター初心者のあなた！コールセンターで働くとどんな良いことがあるのか、気になりませんか？そんなコールセンター初心者のみなさんに向け、実は「おいしい」コールセンターあれこれをご紹介します！</p>
  <img src="/public/images/ccwork/015/main001.jpg" alt="コールセンター風景01" class="imagemargin_T20B20" />
</section>
<!--- 段落１ 終了 --->

<!--- 段落２ 開始 --->
    <section class="sect02">
  <p class="yellowsmallcircle_Number">1</p>
  <p class="yellowsmallcircle_title">スキルが身に付く</p>
  <p class="marginB20">コールセンターと言えば、オペレーターがお客様へ次々に電話をかけている、または、お問い合わせやクレームの電話を受けているイメージ。ではどんなスキルが身につくのでしょうか。一番大きなものと言えば、コミュニケーション能力。顔の見えないお客様に、いかに、メリットを伝えられるか、また、いかにお客様が何を求めているのかをヒアリングできるか、コールセンターのお仕事は、まさにコミュニケーションなしには語れません。もうひとつはPCスキルです。扱う商材や担当する業務の幅にもよりますが、実はパソコンに向かっている時間も長いのです。場合によっては、見積書や報告書なども作成し、それも、次の電話をするために、限られた時間の中でてきぱきこなさなければならないので、短期間で高いPCスキルが身に付くでしょう。
</p>
   <img src="/public/images/ccwork/015/sub_001.jpg" alt="コールセンター風景02" class="imagemargin_T20B20" />
  </section>  
<!--- 段落２ 終了 --->

<!--- 段落３ 開始 --->  
  <section class="sect02">
  <p class="yellowsmallcircle_Number">2</p>
  <p class="yellowsmallcircle_title">自分にあった働き方ができる</p>
  <p class="marginB20">コールセンターの多くは、シフトに融通が効き、直接の対面でないため、服装・髪型自由で働けます。自分のプライベートの時間を守りながら働けるので、夢を追うフリーターさん、忙しいママさんにもぴったりのお仕事です！</p>
   <img src="/public/images/ccwork/015/sub_002.jpg" alt="コールセンター風景02" class="imagemargin_T20B20" />
  </section>
<!--- 段落４ 終了 --->

<!--- 段落５ 開始 --->
  <section class="sect05">
  <p class="yellowsmallcircle_Number">3</p>
  <p class="yellowsmallcircle_title">時給が高い</p>
  <p class="marginB20">コールセンターの醍醐味と言えば、やはり時給が高いこと！コールセンターはサービスのご提案や、案内がメインのため、商材の知識が必要。専門性が高いためお給料も高いのです。コールセンター求人を見ていても、1000円以上のところが多いですよね。場合によっては2000円近くに上ることもあります！</p>
   <img src="/public/images/ccwork/015/sub_003.jpg" alt="コールセンター風景02" class="imagemargin_T20B20" />
  </section>
<!--- 段落５ 終了 --->

<!--- 段落６ 開始 --->
  <section class="sect06">
  <p class="yellowsmallcircle_Number">4</p>
  <p class="yellowsmallcircle_title">性格が明るくなる？</p>
  <p class="marginB20">コールセンターで働くと性格が明るくなる、そんなバカな・・・。でも、実はこれ本当なんです！あなたは、1日に何人の人とお話しますか？人にもよると思いますが、だいたい多くても5～10人といったところではないでしょうか。ところが、コールセンターなら、1日に300人のお客様とお話するなんてことも！これだけたくさんの人とお話すると、自然にコミュニケーション能力が養われてきますよね。コミュニケーション能力がつくと、自身自信もついて性格も明るくなって学校で人気者に！仕事を始めて3ケ月、始めたばかりの頃と表情が全然違う！なんてことも現場ではたくさん起きているのです。</p>
  <img src="/public/images/ccwork/015/sub_004.jpg" alt="コールセンター風景02" class="imagemargin_T20B20" />
  </section>
<!--- 段落６ 終了 --->

<!--- 段落７ 開始 --->
  <section class="sect07">
  <p class="marginBottom40px">様々なライフスタイルに合わせて働けて、性格までポジティブに変わっちゃう？そんなメリットいっぱいのコールセンター。<br />あなたも、正社員でキャリアアップを目指すもよし！アルバイトでガッツリ稼ぐもよし！コールセンターで充実ライフを手に入れませんか！</p>
  </section>
<!--- 段落７ 終了 --->


  <aside>
  <dl>
  <dt class="headingG01">「コールセンター基礎知識」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail24">声で人生が変わる！？コールセンターで好印象な声を会得する方法</a></li>
  <li><a href="/ccwork/detail33">テレマから人事部へ！コールセンター業務の経験が、その後の昇進を決める！？</a></li>
  </dd>
  </dl>
  </aside>
  
<aside class="tabNav_ccwork">
<ul><li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_a.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_a.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li><li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li><li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li></ul>
</aside><!-- / tabNav -->
  
</section>