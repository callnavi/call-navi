<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">基礎情報</span></div>
  <h1>NGワードとクッション言葉に気をつけよう！</h1>
  </header>
  
  <section class="sect01">
     <p class="marginBottom20px">コールセンターでの言葉使いは、普段使っている言葉とは違う言葉が多くあります。しかし、経験が浅い・知らない方のために、今回はNGワードとクッション言葉について簡単に紹介して行きましょう。</p>
    <img src="/public/images/ccwork/013/main1.jpg" class="marginBottom10px" alt="NGワードとクッション言葉に気をつけよう！" />
  </section>
  
  <section class="sect02">
  <h2 class="sideBlueBorder_blueText02">NGワードって何？</h2>
     <p class="marginBottom_none">「NGワード」とは、<span class="textA01">一般的に使ってはいけない言葉・禁句</span>のことです。コールセンターやコンタクトセンターでは、お客様と対話するときに<span class="Blue_text">使ってはいけない言葉</span>があります。お客様にとって失礼な言葉や、わかりにくい表現はもちろんですが、他にもNGとされているものがあります。</p>
     <div class="freebox03_grayBG">
       <p class="freebox03_grayBG_textbold">①社内用語</p>
       <p><span class="Blue_text">会社内で使っている用語や略称</span>など、呼びやすく短縮してしまい、社内の人間には意味がわかっても、社外の人には全く意味がわからない言葉</p>
       
       <p class="freebox03_grayBG_textbold">②業界用語・専門用語</p>
       <p>会社内で使っている用語や略称など、呼びやすく短縮してしまい、社内用語と似ていますが、<span class="Blue_text">該当する業界だけで通用する表現 ・言葉</span></p>
     </div>
     <p class="marginBottom_none">会社で使用していると無意識のうちに使ってしまっていることも少なくありません。業務に慣れた人や専門家の人などと対話するときはスムーズに話が進みますが、初心者や専門用語を知らないお客様には、できるだけわかりやすい言葉で話すことが大切です。 </p>
</section>

<section class="sect03">
    <h3 class="blueblockBG">これってNGワードなの？？</h3>
    <p class="marginBottom_none">使っていいのか悪いのか、お客様によって納得する表現は様々です。そんな中でも、どっちが正解なのか迷ってしまう代表的な表現を、少し紹介していきましょう。</p>
    <img src="/public/images/ccwork/013/blue.jpg" alt="◯◯でよろしかったでしょうか？" class="imagemargin_T25B25" />
    <p class="marginBottom_none">これはファミリーレストランなどでも良く耳にする言葉ですね。誰が言い始めたのかはわかりませんが、丁寧に言っている風に聞こえます。しかし、この日本語は間違っているそうです。言葉使いに厳しいお客様と会話しているとき、「○○でよろしかったでしょうか？」と言ってしまい、お客様の機嫌をそこね、最悪の場合クレームに発展することもあります。<span class="Blue_text">お客様に確認するときは、「○○でよろしいですか？」と言った方が日本語としては正しいようです。</span></p>
    <img src="/public/images/ccwork/013/pink.jpg" alt="ダメです。ムリです。" class="imagemargin_T25B25" />
   <p class="marginBottom10px sectborder1">こう答えたいときは、どう考えても対応できない、無茶なことを言われたときかもしれません。ですが、「ダメです。」「ムリです。」と答えられるとお客様は、ないがしろにされたような気分になってしまいます。こういった場合は、<span class="Blue_text">「できかねる」と答えるのがコールセンターとしてはベターです。</span>意味は同じなのですが、表現がやわらかく丁寧になります。どうしても強く否定をしなければならない場面では、「大変申し上げにくいのですが・・・」や「言い方は悪いのですが・・・」と頭につけてから「出来ないのです。」などと言った方が効果的な場合もあります。</p>
</section>

  <section class="sect04">
  <h2 class="sideBlueBorder_blueText02">クッション言葉ってなに？</h2>
  <p class="marginBottom_none">「クッション言葉」とは、依頼・断り・反対の意見を言う場合などに、 <span class="Blue_text">文頭につけて使用する言葉のことです。</span>直接的な表現を避けられ、丁寧で優しい印象を相手に与える効果があります。否定的な言葉や言いにくい内容でも、相手に失礼無く伝えることができるので、コミュニケーションはよりスムーズになります。ただし、クッション言葉を多用するとワザとらしかっ たり、まわりくどい印象に取られる場合があるので、程よいバランスで使用すると誠意が伝わるでしょう。それでは、どのようなクッション言葉があるのか、具体的に例を挙げて見てみましょう。</p>
  <h3 class="blueblockBG">代表的なクッション言葉</h3>
    <ul class="dot_bluetext">
    <li><span>1</span>恐れ入りますが</li>
    <li><span>2</span>大変恐縮ですが</li>
    <li><span>3</span>お手数ですが</li>
    <li><span>4</span>もし、よろしければ</li>
    </ul>
   <p class="marginBottom_none">例えば、「お手数ですが、ご記入ください。」と、文頭につけた方が、ただ「ご記入ください。」と言うよりも相手に対して与える印象はグッと良くなります。また、コールセンターの場合は「イエス・バット話法」「イエス・アンド話法」という便利な会話術も活用できるので最期に簡単に紹介します。</p>
      </section>

  <section class="sect05">
  <h2 class="sideBlueBorder_blueText02">イエス・バット話法、<br>
  イエス・アンド話法って何？</h2>
   <p class="marginT20B20">「イエス・バット話法」とは、相手が自分と異なる意見のときに、「○○なのですね、しかし、△△はいかがですか？」などと、<span class="Blue_text">相手の意見を肯定した後、but（逆説）を加え、自分の意見を伝えるテクニック</span>です。そして、「イエス・アンド話法」とは、「○○なのですね、だから、△△はいかがですか？」などと、<span class="Blue_text">相手の意見を肯定した後、and（順接）を加え、自分の意見を伝えるテクニック</span>となります。</p>
     
  <p class="marginB20">これは、コールセンターの現場でも効果的です。例えば、お客様との会話の中で、相手の意見をいきなり否定してしまうのは、得策ではありません。相手の意見に同意した後、自分の意見を伝えると、相手に否定的な印象を与えずに済み、こちらの話をスムーズに聞き入れやすくなります。</p>
  <p class="marginBottom40px">相手と自分の意見が、必ずしも一致していなくても使えるのが「おっしゃることはよくわかります」「ごもっとも」というフレーズです。意見を肯定する印象を与えられる便利な表現なので、覚えておいて、損はないでしょう。</p>
  </section>

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター基礎知識」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail03">全国調査！コールセンターの時給比較</a></li>
  <li><a href="/ccwork/detail04">コールセンターの雇用形態</a></li>
  <li><a href="/ccwork/detail13">NGワードとクッション言葉に気をつけよう！</a></li>
  <li><a href="/ccwork/detail11">これは必須！コールセンターの専門用語！！</a></li>
  </dd>
  </dl>
  </aside>
  
<aside class="tabNav_ccwork">
<ul><li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_a.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_a.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li><li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li><li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li></ul>
</aside><!-- / tabNav -->
  
  
</section>

