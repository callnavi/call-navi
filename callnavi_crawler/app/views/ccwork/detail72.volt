<section class="entryA01 ccwork_entry">
<header>
<div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">働く人の声</span></div>
<h1>テンションが高くない人必見！<br />コールセンターバイトでは、<br />無理にテンションを上げる必要はない！？</h1>
</header>
  
<section class="sect01">
<p class="marginBottom20px">コールセンターでの仕事と聞くと、自分には向かない・・・と言う人がいます。理由を聞いてみると、そのうちの一つにテンションが高くないから、というのがあります。<br />でも、実際のところ、<span class="Blue_text">コールセンターの仕事はテンションが高くないとできないのでしょうか？</span></p>
<img src="/public/images/ccwork/072/main001.jpg" class="imagemargin_T10B30" alt="テレアポも営業だから" />

<h2 class="sideBlueBorder_blueText02 marginBottom10px">テレアポも営業だから</h2>
<p class="marginBottom20px">コールセンターの業務には、顧客リストに電話をかけて（架電）、電話営業をするテレアポ・ヘルプディスクや通販受付のようにかかってきた電話に対応する（受電）テレオペ、そしてマーケティング的な要素が含まれるテレマ（テレフォンマーケティング）があります。</p>
<p class="marginBottom20px">この中で、テレマは営業の一種で、売り込みをしたい商品やサービスがあって、潜在顧客のリストに片っ端から電話をかけていくことが多いです。</p>
<p class="marginBottom10px">全く面識がない人に、いきりなり電話をかけて、商品やサービスの説明をするわけですから、明るくハキハキと、元気よく話をする方が、良さそうですよね！</p>

<h2 class="sideBlueBorder_blueText02 marginBottom10px">そもそも営業はテンションが高くないとダメなの？</h2>
<img src="/public/images/ccwork/072/sub_001.jpg" class="imagemargin_T10B30" alt="そもそも営業はテンションが高くないとダメなの？" />
<p class="marginBottom20px">保険でも携帯電話でも、健康食品でも何でもいいのですが、バリバリの営業マンというと、どういう人をイメージするでしょうか？<br />元気で明るい人でしょうか？<span class="blue_text">テンションの高い人で</span>しょうか？話し好きで、一度話すと長い時間話し込んでしまう人でしょうか？</p>
<p class="marginBottom20px">少し前、車の購入を前提にディーラーに行きました。試乗もさせてくれるなど、サービスは言うことありませんでした。こちらの質問にも全て完璧に答えてくれました。でも、残念ながら購入する気にはなりませんでした。</p>
<p class="marginBottom20px">前から欲しかった車なので、車には全く問題がないと思います。家に帰ったあとでゆっくり考えてみると、あまりにもテンションが高く、少し暑苦しい感じがしました。そして、人の話を遮ってまで、自分の話を続けるところが気になってしまいました。</p>
<p class="marginBottom20px">人間関係がある人に多少強引な話をされたり、迷っているときに背中を押してくれたりするのはありがたいこともありますが、初対面の人だと、ちょっと引いちゃうこともあるかも知れませんね。</p>
<p class="marginBottom10px">本当に成績の良い営業マンは、一緒にいて楽しい人が多いと聞きます。ひょっとしたら、営業ができる・できないは、テンションとは関係がないのかも知れません。</p>

<h2 class="sideBlueBorder_blueText02 marginBottom10px">人の話を聞くより、自分の話を聞いてもらいたい</h2>
<p class="marginBottom20px">話好き・人と話すのが苦手・話が上手/下手など、色々なタイプがあるかとは思いますが、多くの方は<span class="blue_text">人が話しているのを聞くよりも、自分が話したい！</span>という欲求が大きいのではないでしょうか？</p>
<p class="marginBottom10px">自分の事を認めて欲しい！という欲求の表れかも知れませんが、いきなり知らない人から営業電話が来て、商品の説明とは言え、一方的な話でストレスがたまるのは誰しも同じことだと思います。</p>

<h2 class="sideBlueBorder_blueText02 marginBottom10px">話上手よりも、聞き上手がいい！</h2>
<p class="marginBottom20px">書店に行けば、コミュニケーションに関する本がたくさん出版されていて、オウム返しなどコミュニケーションスキルは世の中に溢れ返っています。どんなにスキルを磨いたとしても、聞きたくない話は聞きたくないです。コミュニケーションスキルに頼りすぎないことも大切です。</p>
<p class="marginBottom20px">友人との飲み会を考えてみると分かりやすいかも知れませんが、その場を盛り上げてくれる人はとてもありがたいですが、一方的に話す人がいて、その人の話を聞くだけだと、終わった後にどっと疲れが出てきたりしますからね。</p>
<p class="marginBottom10px">話上手も大切なスキルだとは思いますが、聞き上手はもっと大切だと思います。親身になって話を聞いてもらうことで、親近感・信頼感が生まれてくる場合も多いですから。</p>

<h2 class="sideBlueBorder_blueText02 marginBottom10px">そうは言っても、商品知識はしっかりと身につける</h2>
<img src="/public/images/ccwork/072/sub_002.jpg" class="imagemargin_T10B30" alt="そうは言っても、商品知識はしっかりと身につける" />
<p class="marginBottom20px">話上手よりも、聞き上手がいい！と言われたからと言って、テレアポでひたすらお客さんの話を聞いているだけでは、商品を売ることは難しいでしょう。</p>
<p class="marginBottom10px">商品知識をしっかりと身につけ、必要な説明をして、お客さんの質問にも的確に答える。あとは、お客さんによって、こちらが話すバランスを変えられるのが理想的ですね。</p>

<h2 class="sideBlueBorder_blueText02 marginBottom10px">テレアポの成功者ってどんな人？</h2>
<p class="marginBottom20px">テレアポバイトの魅力は、時給が高いことでも知られていますが、これに外にもインセンティブがもらえるケースも多いようです。中には、<span class="blue_text">月に10万円以上のインセンティブを給料の他に稼いでいるツワモノも</span>いるそうです。</p>
<p class="marginBottom40px">このような成功者の印象としては、強いオーラがあって、営業が得意！という人も中にはいますが、普段はおとなしい感じの人が多いように思います。<br />自ら話すよりは、普段はあまりしゃべらずこちらからの質問に対して答えていく人が多いです。</p>

<h3 class="blueblockBG">まとめ</h3>
<p class="marginBottom10px">最後に「コールセンターの業務を行うには、テンションが高くないと駄目なの？」についてまとめると、以下のようになります。</p>
<ul class="bluebox_dotsentence">
<li>明るく元気な方がいいが、無理にテンションを上げる必要はない</li>
<li>話上手よりも、聞き上手の方がいい</li>
<li>商品知識など必要なスキルは身につける</li>
</ul>
<p class="marginBottom40px"><span class="">テンションが高くないからコールセンターの仕事は無理！</span>と思っていた方も、実際に応募して電話をかけてみると、以外に面白いかも知れませんよ！</p>
</section>

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター働く人の声」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail58">コールセンターバイトで、就職活動を有利に！　電話対応、コミュ力、ビジネスマナーは最大の武器！</a></li>
  <li><a href="/ccwork/detail73">【質問】コールセンターバイトでの休憩時間は、仲間と積極的に話すべき！？できれば一人でゆっくりしたいのですが･･･</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  <aside class="tabNav_ccwork">
  <ul id="js-tab01">
  <li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_a.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_a.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li>
  </ul>
  </aside><!-- / tabNav -->
  
  
</section>
