<section class="unitA01">
<section class="uniqueCcwork01">
<h1 class="mediaPC"><img src="/public/images/ccwork/index_ttl_01.png" width="720" height="" alt="コールセンターの仕事とは？　コールセンターお役立ち講座"></h1>

<div class="unitA02">
<div class="tabNav_ccwork">
<ul id="js-tab01">
<li><a href="#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_02_n.png" width="93" height="" alt="基礎知識" class="mediaSP"></a></li>
<li><a href="#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_02_n.png" width="93" height="" alt="知っ得情報" class="mediaSP"></a></li>
<li><a href="#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_02_n.png" width="93" height="" alt="働く人の声" class="mediaSP"></a></li>
</ul>
</div><!-- / tabNav -->

<div class="tabContents">

<div id="tabContents01">
<ul class="entryListA01 moreReadList">

<li><a href="/ccwork/detail51">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub1_20.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate01">基礎知識</span>営業経験が全くないのですが、コールセンターの仕事はできますか？</dt>
<dd>営業経験がないために、コールセンターへの応募を躊躇している方はいませんか？今回は、コールセンターと…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail48">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub1_19.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate01">基礎知識</span>アルバイト入社でも実力次第で、正社員になれるコールセンター</dt>
<dd>コールセンターの仕事がしたい！と思って、求人サイトや求人広告を見たとき、どこをチェックしますか？時給？…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail47">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub1_18.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate01">基礎知識</span>「集中力を大発揮！あなたのジンクスはなに！？」</dt>
<dd>就業中、集中力をずっと維持するのは、なかなか至難の業ですよね。さっきまで、ものすごい集中力でバリバリ仕事をしていた…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail46">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub1_17.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate01">基礎知識</span>コールセンターの三種の神器！PC・ヘッドセット・管理ソフト！</dt>
<dd>コールセンターの求人情報に興味があるけど、応募しようか迷っている方に、コールセンターで使っている機器について紹介…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail45">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub1_16.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate01">基礎知識</span>テレアポ・テレオペ・テレマの違いって？自分にピッタリの仕事を見つける！</dt>
<dd>コールセンター系のバイトや、転職先を探していると、テレアポ・テレマ・テレオペといったキーワードが出てきますが…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail37">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub1_15.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate01">基礎知識</span>私のテレアポ営業奮闘記～テレアポは怖くない！</dt>
<dd>こんにちは！テレアポでの電話営業を始めて、5ヶ月目のN美24歳です。本日は私の〜テレアポ営業奮闘記〜をご紹介します。…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail35">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub1_14.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate01">基礎知識</span>初心者でも安心！コールセンターの研修や教育でやっていること</dt>
<dd>コールセンター初心者にとっては、商材の知識、クレーム対応など、会社の窓口としてサービスをご案内するにあたって、不安な…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail28">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub1_13.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate01">基礎知識</span>「ファミコン敬語」使っていませんか？以外と知らない？！正しい「ビジネス敬語」</dt>
<dd>社会人になって敬語ができている！と思っても、意外とビジネス敬語ではNG言葉を使っているかもしれないですよ。 お客様を不快に…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail26">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub1_12.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate01">基礎知識</span>初めての方必見！コールセンター応募、面接、採用、入社初日までの流れ</dt>
<dd>コールセンターに履歴書を送ってから、入社・研修までの流れはどうなっているの？今回は、こちらについて、をお届けします。入社までの…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail23">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub1_11.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate01">基礎知識</span>日本語を正しく使えていますか？コールセンター業務に必要な丁寧語、謙譲語、尊敬語</dt>
<dd>このお仕事では、お客様とのやりとりは、基本的に電話越しです。ということは、声のトーンや言葉遣いのひとつひとつが、お客様に…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail22">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub1_10.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate01">基礎知識</span>やればやるだけ報酬UP！だから成長できるインセンティブのひみつ</dt>
<dd>コールセンターの仕事を探していると「基本給＋インセンティブ」という表記をよく目にしますよね。なんとなく分かるけど、結局どうい…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail21">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub1_9.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate01">基礎知識</span>「コールセンターで働くあの子！どんな１日を過ごしているの？」</dt>
<dd>コールセンターと聞くと、ひたすら電話と向き合う孤独な職業…というイメージをお持ちの方も、いらっしゃるのではないでしょうか。…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail20">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub1_8.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate01">基礎知識</span>話すのが苦手な方へ、テレビショッピングのコールセンターをおすすめする理由！</dt>
<dd>コールセンターと一言でいっても、アウトバウンド(発信)・インバウンド(受信)の違いから始まり、カスタマーサポートや受注窓口、料金の…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail15">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub1_7.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate01">基礎知識</span>性格まで明るくなっちゃう？コールセンターで充実ライフを手に入れよう！</dt>
<dd>コールセンターで働くなんてこれまで考えたことなかった、というコールセンター初心者のあなた！コールセンターで働くとどんな良いことが…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail01">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub1_1.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate01">基礎知識</span>コールセンターってどんな仕事？</dt>
<dd>コールセンターとは、企業の商品・サービスに関するお問い合わせや、注文受付などの電話対応業務を専門的に行う事業所・窓口のことです。…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail11">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub1_2.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate01">基礎知識</span>これは必須！コールセンターの専門用語！！</dt>
<dd>コールセンターで働くにあたって、「それってどういうこと？」 と疑問に思うワードがありませんか？意味がわからないと会社の中でコミュニケー…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail13">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub1_3.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate01">基礎知識</span>NGワードとクッション言葉に気をつけよう！</dt>
<dd>コールセンターでの言葉使いは、普段使っている言葉とは違う言葉が多くあります。 しかし、経験が浅い・知らない方のために、今回はNGワードとク…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail03">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub1_4.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate01">基礎知識</span>全国調査！コールセンターの時給比較</dt>
<dd>働く上で気になるのは時給。コールセンターは、商材知識が必要であるなど専門性が強いため、他の職種と比べて時給は高めです。…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail04">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub1_5.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate01">基礎知識</span>コールセンターの雇用形態</dt>
<dd>コールセンターで働く際の雇用形態は、正社員やアルバイトなどさまざまです。それぞれメリット・デメリットをしっかり確認し、…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail07">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub1_6.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate01">基礎知識</span>バイトを辞めるときはここに注意！</dt>
<dd>学業に専念したい、就職が決まった、ちょっと職場に合わないなど、 アルバイトを辞める時はなどさまざまな理由があると思います。無用なトラブ…<span class="more">続きを読む</span></dd>
</dl>
</a></li>
</ul>
</div><!-- / tabContents01 -->





<div id="tabContents02">
<ul class="entryListA01 moreReadList">

<li><a href="/ccwork/detail75">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub2_36.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate02">知っ得情報</span>一度始めたら辞められない！？コールセンターバイトの５つの魅力！</dt>
<dd>コールセンターのバイトってネガティブなイメージをもたれることが多いです。働く前だと実際のコールセンターを見る機会はほとんどないので…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail70">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub2_35.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate02">知っ得情報</span>タダでディズニーランド！？実際にあったコールセンターでの豪華賞品！</dt>
<dd>コールセンターは高時給、高インセンティブで有名ですよね。実は、コールセンターのメリットは、報酬が良いだけではないのです。…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail66">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub2_34.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate02">知っ得情報</span>保険関係のコールセンターで行う４つの業務とは？</dt>
<dd>コールセンターと一言でいっても、アウトバウンド(発信)・インバウンド(受信)の違いから始まり、カスタマーサポートや受注窓口、料金の…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail65">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub2_33.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate02">知っ得情報</span>副業のススメ！コールセンターで副業するときには、ここに気をつけよう！</dt>
<dd>コールセンターで働いている人の中には、本業が別にあって、副業としてオペレーターをやっている人が結構いるって知っていました！？…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail64">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub2_32.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate02">知っ得情報</span>髪型自由は、本当に自由なの？髪型自由だとこんなメリットがある</dt>
<dd>アルバイトを探す上で最も外せない条件と言えば？　給与、駅からの距離、仕事内容…みなさんそれぞれあると思います。でも中には…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail62">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub2_31.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate02">知っ得情報</span>睡魔を吹き飛ばせ！眠気対策には、これを知っておけば大丈夫</dt>
<dd>仕事に勉強に、頑張る皆さんにとっての最大にして、永遠の敵と言えば、そう！「睡魔」です。特にデスクワークの多い方は動きも…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail61">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub2_30.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate02">知っ得情報</span>大丈夫！？即日勤務OK！ 日払いOK！アルバイトとその実態</dt>
<dd>すぐにお金が必要な時、即日勤務OK！日払い可能なアルバイトは大変ありがたいですが、これらの仕事は実際のところ、どうなんで…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail60">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub2_29.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate02">知っ得情報</span>色んな人と出会えるチャンス！コールセンターで友達＆恋人もゲットしちゃおう</dt>
<dd>コールセンターのお仕事って女性が多く働いているイメージですよね。でも実はコールセンターで働いている男性って多いんです。業種にも…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail59">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub2_28.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate02">知っ得情報</span>冷房病にご注意！エアコンの効いたコールセンター働く際の健康管理とは？</dt>
<dd>暑い夏場、職場のつめた～いエアコンにお悩みの方は多いはず。体が冷えてなんとなくダルい…。頭痛がする…。喉が痛い…。でも…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail55">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub2_27.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate02">知っ得情報</span>取扱いにご注意！個人情報とコールセンター</dt>
<dd>コールセンターでは、お客様の個人情報を取り扱っています。しかし、ちょっとしたことでその情報が漏れてしまえば、たちまち…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail53">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub2_26.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate02">知っ得情報</span>コールセンターで生かせる資格「コン検」！</dt>
<dd>ビジネスマナー検定、パソコン検定、秘書検定・・・世の中には数えきれないほどの資格や検定が存在します。仕事をしていく上で…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail52">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub2_25.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate02">知っ得情報</span>社員登用制度を利用して、アルバイトから正社員へレベルアップ！</dt>
<dd>多くのコールセンターでは、アルバイトからの社員への登用制度を積極的に行っています。その理由と実態について調べてみました。…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail50">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub2_24.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate02">知っ得情報</span>あなたはどっち？ SV転向、アポインター継続</dt>
<dd>コールセンターで働くメリットは多くあります。例えば、時給が高かったり、時間の融通が効いたり、座ったままできるので体力的にも…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail44">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub2_23.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate02">知っ得情報</span>コールセンター心理学！これであなたもコミュニケーションの達人に！</dt>
<dd>心理学と聞くと、なんだか難しそう…と思ってしまうかもしれません。ですが心理学は、人の「行動」や「意識」などを解明していく学問で…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail43">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub2_22.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate02">知っ得情報</span>友達同士でバイトに応募！他にはない！？コールセンターのメリット</dt>
<dd>海外へ行きたい・バイクが欲しい・免許の取得にお金がかかる…など、まとまったお金が欲しいとき、コールセンターのバイトはとっても…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail38">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub2_21.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate02">知っ得情報</span>短期バイトで稼ぎたい！それなら、コールセンターがおすすめです。</dt>
<dd>短期バイトを探しています！大学や専門学校の長期休みの前は、このような方々は多くなるのではないでしょうか？今回は、短期のアルバイト…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail36">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub2_20.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate02">知っ得情報</span>地方で働きたい！コールセンターが地方で増える理由とメリット</dt>
<dd>コールセンターが地方に増えているらしい！その理由とは？地方創生のキッカケ？！コールセンターへの誘致合戦がはじまっているらしい…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail34">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub2_19.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate02">知っ得情報</span>就活の秘訣！コールセンター経験者が就活を有利に進められる理由とは？</dt>
<dd>せっかく働くのなら、バイトにしても何にしても、何かしらのスキルを身につけたいですね！ちなみにコールセンターで働いたら…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail32">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub2_18.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate02">知っ得情報</span>どうするクレーム？しっかり対応するための魔法の４ステップ！</dt>
<dd>「大変！クレームになってしまった！お客様が怒っている！」まず一番初めに湧き起ってくる感情は「焦りや恐怖」だと思います。今の…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail31">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub2_17.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate02">知っ得情報</span>仕事とプライベートの両立！シフト制は、スケジュール管理の魔法の杖となるのか？</dt>
<dd>アルバイト、派遣社員、契約社員、正社員に関わらず、シフト制の仕事ってありますよね！？これって、好きな時に出社して、好きな時に…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail30">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub2_16.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate02">知っ得情報</span>説明上手になれる！？その秘訣は･･･</dt>
<dd>仕事をする時に人に説明する場面って多いですよね。「明日、プレゼンだけど…上司にうまく自分の意見で説得できない…。」「お客様の…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail29">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub2_15.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate02">知っ得情報</span>これで受かる！コールセンター応募の志望動機</dt>
<dd>コールセンターで働きたいけど、志望動機に何を書けば良いの？未経験の場合どんなことをアピールすれば良い？履歴書の悩みは就職活動に…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail27">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub2_14.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate02">知っ得情報</span>コールセンターの年齢事情</dt>
<dd>応募をするにあたり、年齢不問と記載があっても、応募の多い年代やどれくらいの年齢層が働いているのか気になりませんか？今回は…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail25">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub2_13.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate02">知っ得情報</span>コールセンターの困ったお客様</dt>
<dd>コールセンターで働いていると、たまに対応に困るお客様に遭遇します。特に24時間営業をしているコールセンターでは、そんな電話も…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail24">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub2_12.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate02">知っ得情報</span>声で人生が変わる！？コールセンターで好印象な声を会得する方法</dt>
<dd>コールセンターで働く人にとって、声は命！　声の印象や話し方が良ければ、お客さんも安心して話を聞いてくれて、こちらの言いたいことも…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail19">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub2_11.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate02">知っ得情報</span>ゲーム以上に仕事が楽しい！？楽しく仕事をする４つのポイント</dt>
<dd>休みが終わってしまう日曜日の夜…。「また1週間が始まる…」と思うと会社に行くのが、おっくうになったりする方も多いのではないで…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail18">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub2_10.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate02">知っ得情報</span>学歴なんて関係ない！コールセンターなら、実力次第で高収入が目指せる</dt>
<dd>自分にぴったりのコールセンター求人を見つけていざ履歴書を作成しようとしても、学歴がないから、職歴が十分じゃないから･･･と諦めては…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail17">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub2_9.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate02">知っ得情報</span>おしゃれを楽しみたい！服装自由のコールセンターが多いわけ</dt>
<dd>コールセンターのバイトのメリットのひとつとしてあげられるのが、服装自由でOK！という点です。でも本当になんでもいいの？どこまでOK…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail16">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub2_8.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate02">知っ得情報</span>隠されたドラマがある！？コールセンターの舞台裏！</dt>
<dd>働く前になかなか見ることができないコールセンター。どんな人がどんな思いで働いているのかがわかりづらいところではありますよね…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail12">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub2_1.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate02">知っ得情報</span>日本全国コールセンターまとめ</dt>
<dd>働きたい地域にコールセンターがあるか探したいけど見つからない。そんなときに、コールナビのコールセンターまとめ！都道府県ごとに…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail06">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub2_2.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate02">知っ得情報</span>「ストレスに負けない！」コールセンターバイト</dt>
<dd>ストレスを溜めないように！とよく聞くけど、 ストレスを溜めないようにするにはどうしたら良いんだろう…。そもそもストレスが全くない人はい…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail09">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub2_3.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate02">知っ得情報</span>健康の秘訣！知っておきたい、体のケア</dt>
<dd>コールセンターは、座りながらひたすら喋るお仕事なので、 のどや耳の不調、座りっぱなしで腰が痛いなど様々な不調が出てくると思います。…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail08">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub2_4.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate02">知っ得情報</span>ライフスタイルに合った働き方を見つけよう！</dt>
<dd>「子供が学校から帰ってくるまでに帰りたい…」 「家事もあるので毎日は無理…」コールセンターのお仕事はこういったママさんたちの強い味方！…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail10">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub2_5.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate02">知っ得情報</span>アイディアオフィスで疲れを吹き飛ばそう！</dt>
<dd>コールセンターは電話をかける設備があれば良い？いやいや、そんなことはありません。 オペレーターさんが気持ちよく働けて、なおかつ…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail05">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub2_6.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate02">知っ得情報</span>コールセンターの面接で失敗しないコツ？</dt>
<dd>コールセンターの人事担当者が面接で最も注目するのは話し方です。大事なのは「ハキハキと」「丁寧に」話すこと。コールセンターは受信…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail02">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub2_7.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate02">知っ得情報</span>コールセンターで働くメリットってなに？</dt>
<dd>コールセンター業務の最大のメリット、それは「コミュニケーション能力」がつくことです。コールセンターの仕事は電話でのやりとりと…<span class="more">続きを読む</span></dd>
</dl>
</a></li>
</ul>
</div><!-- / tabContents02 -->


<div id="tabContents03">
<ul class="entryListA01 moreReadList">

<li><a href="/ccwork/detail80">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub3_23.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate03">働く人の声</span>コミュニケーションのコツはVAK！テレアポはもちろん、普段の会話にも劇的に変わります！</dt>
<dd>コミュニケーションが伝わりやすかったとき・伝わりにくかったときこれらの違いについて考えたことはありますか？今回は…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail79">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub3_22.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate03">働く人の声</span>カラオケ上手は、テレアポ上手！？意外な共通点と、心に刺さるトークのコツとは？</dt>
<dd>懇親会・送別会といった会社のイベントや、飲み会の２次会などに利用することの多いカラオケですが、カラオケの上手な人は、コールセンターでも…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail78">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub3_21.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate03">働く人の声</span>女性SVに聞く～コールセンターのお仕事あれこれ～</dt>
<dd>コールセンターで働く人の中にはSV（スーパーバイザー）としてキャリアアップする人もいます。SVになると、プレーヤーとしての実力を…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail77">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub3_20.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate03">働く人の声</span>喉が命！コールセンターバイトもしゃべりのプロ！喉のケア方法と喉にいい食材はこちら</dt>
<dd>コールセンターでは声が命綱！声がお仕事道具になるので、喉のケアはかかせません！テレアポの達人でも、喉が本調子でないと、突然契約が…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail76">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub3_19.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate03">働く人の声</span>お笑い芸人の卵です！そんな方は、高時給・シフト制のコールセンターで、当面の生活費と将来の夢の両立を！</dt>
<dd>コールセンターで仕事をしていると、「えっ！　何でこの人がテレアポしているの？」と言いたくなる人がいることがあります。「実は…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail74">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub3_18.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate03">働く人の声</span>高時給バイトのテレアポ体験談！苦情は意外と気にならない！？</dt>
<dd>「生活にゆとりが欲しいから、給料の良いアルバイトを探している」「大学生活最後の年だから、多少お金がかかっても思う存分楽しみ…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail73">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub3_17.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate03">働く人の声</span>【質問】コールセンターバイトでの休憩時間は、仲間と積極的に話すべき！？できれば一人でゆっくりしたいのですが...</dt>
<dd>今回は、コールセンター業務中の休憩時間の過ごし方です。パート、バイト、派遣社員、正社員のどれでもいいのですが、休憩時間は…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail72">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub3_16.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate03">働く人の声</span>テンションが高くない人必見！コールセンターバイトでは、無理にテンションを上げる必要はない！？</dt>
<dd>コールセンターでの仕事と聞くと、自分には向かない・・・と言う人がいます。理由を聞いてみると、そのうちの一つにテンションが高く…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail71">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub3_15.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate03">働く人の声</span>実録インタビュー！実際にコールセンターで働いた印象</dt>
<dd>お給料、仕事内容、人間関係・・・働いていると不満や悩みはつきものです。もっと早く帰れると思ったのに・もっとお給料が高ければ頑張れ…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail69">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub3_14.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate03">働く人の声</span>教育スタッフに聞いた、新人がやりがちなミス5選</dt>
<dd>どんな仕事でも始めはみんな新人。職場でもわからないことだらけですし、不安も多いですよね。コールセンターも例外ではありません。初めての…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail68">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub3_13.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate03">働く人の声</span>テレマーケティングできついこと〜アウトバウンド編〜</dt>
<dd>電話をかける側のお仕事、アウトバウンドテレマーケティング。お給料が高く、シフトの融通がきく、オフィスで働けるなどメリットもたくさん…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail67">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub3_12.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate03">働く人の声</span>裏技教えます！本当は教えたくない受注が取れるコツ</dt>
<dd>コールセンターで周りを見渡せば、どんどん受注を取っているスーパー営業マン！いるんですよね！どうしていつも、あの人はあんなに…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail63">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub3_11.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate03">働く人の声</span>眠たいとき、どうする？理想の睡眠時間と業務効率について</dt>
<dd>仕事や勉強の最中に猛烈な眠気が襲ってくる…。こんな経験ありませんか？特にデスクワークの多い人は、お昼休憩を挟んだら急に眠く…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail58">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub3_10.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate03">働く人の声</span>コールセンターバイトで、就職活動を有利に！電話対応、コミュ力、ビジネスマナーは最大の武器！</dt>
<dd>大学生、専門学校生など、学生さんのアルバイトは、何を基準に選んでいますか？アルバイトを選ぶ基準と、選んだアルバイトが今後の…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail57">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub3_9.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate03">働く人の声</span>結婚・引越し・出産があっても大丈夫！コールセンターの魅力！</dt>
<dd>コールセンターには、「コールセンター歴５年です！」など‘コールセンター一筋！’といったベテランな方々が沢山います！電話で…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail56">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub3_8.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate03">働く人の声</span>えっ、あの人も！？テレアポに転職した先輩の意外な過去とは？</dt>
<dd>こんにちは。こちらのコンテンツをご覧くださっているあなたは現在転職活動の真最中でしょうか？転職活動中の方も、なんとな…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail54">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub3_7.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate03">働く人の声</span>【コールセンター体験談！】コールセンターで実際に研修を受けてみた！</dt>
<dd>コールセンターでのお仕事を始めるに辺り、どのような研修があなたを待ちわびているか気になりますよね。本日は、実際に私が…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail49">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub3_6.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate03">働く人の声</span>肩が凝る？それ、座りっぱなし症候群ではないですか？</dt>
<dd>コールセンター業務だけではなく、座り仕事に従事していると、気になってくるのが肩こりや腰の痛みなど体の凝りですよね。…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail42">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub3_5.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate03">働く人の声</span>ガムを噛め！仕事中のリフレッシュは、これに限る！</dt>
<dd>仕事中の気分転換、あなたはどのようにしていますか？「先輩！気分転換に今から海に行ってきます！」そんなことが許されればいいのですが…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail41">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub3_4.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate03">働く人の声</span>まさかの逆効果！？甘いものの罠。それリフレッシュになっていません。</dt>
<dd>甘いもの大好きな女子Ｙ子です。仕事中、どうしても甘いものに手が伸びてしまうのは、私だけではないと思います。ちょっと小腹がすいた…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail40">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub3_3.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate03">働く人の声</span>ミュージシャンは声で稼げ！コールセンターで、バンドマンが重宝されるワケ</dt>
<dd>「よし！コールセンターで働こう！」そう思った時、一緒に働く仲間がどんな人達なのか気になりますよね。都内にある某コールセンターでは…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail39">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub3_2.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate03">働く人の声</span>初心者大歓迎！高時給バイトなら、コールセンターが狙い目</dt>
<dd>アルバイトの中には、時給700円から800円程度の仕事から、時給2000円を超える仕事まで、色々なものが存在します。今回は、この中でも…<span class="more">続きを読む</span></dd>
</dl>
</a></li>

<li><a href="/ccwork/detail33">
<div class="thumb"><img src="/public/images/ccwork/thumb/sub3_1.jpg" alt=""></div>
<dl>
<dt><span class="cate ccworkCate03">働く人の声</span>テレマから人事部へ！コールセンター業務の経験が、その後の昇進を決める！？</dt>
<dd>テレマーケティングからスタートして、今はバックヤード（営業部を支える裏方）で働いています。入社時はテレマーケティング担当だった…<span class="more">続きを読む</span></dd>
</dl>
</a></li>
</ul>
</div><!-- / tabContents03 -->

</div><!-- / tabContents -->

<div class="tabNav_ccwork">
<ul id="js-tab02">
<li><a href="#tabContents01"><img src="/public/images/ccwork/nav/tabnav_04_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_04_02_n.png" width="93" height="" alt="基礎知識" class="mediaSP"></a></li>
<li><a href="#tabContents02"><img src="/public/images/ccwork/nav/tabnav_05_n.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_05_02_n.png" width="93" height="" alt="知っ得情報" class="mediaSP"></a></li>
<li><a href="#tabContents03"><img src="/public/images/ccwork/nav/tabnav_06_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_06_02_n.png" width="93" height="" alt="働く人の声" class="mediaSP"></a></li>
</ul>
</div><!-- / tabNav -->
</div><!-- / unitA02 -->

</section>

</section>
