<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">基礎知識</span></div>
  <h1>テレアポ・テレオペ・テレマの違いって？<br />自分にピッタリの仕事を見つける！</h1>
  </header>
  
<!--- 段落１ 開始 --->  
  <section class="sect01">
 <p class="marginBottom10px">コールセンター系のバイトや、転職先を探していると、<span class="Blue_text">テレアポ・テレマ・テレオペ</span>といったキーワードが出てきますが、これらの違いを把握していますか？<br />今回は、テレアポ・テレオペ・テレマの違いについて、紹介します。</p>
    <img src="/public/images/ccwork/045/main001.jpg" class="imagemargin_T10B10" alt="ガムを噛め！仕事中のリフレッシュは、これに限る！" />
</section>

  <section class="sect02">
  <h2 class="sideBlueBorder_blueText02 marginBottom10px">テレアポ・テレオペ・テレマって何？</h2>
  <p class="dot_yellowtext_Number">1</p>
  <p class="dot_yellowtext_title">テレアポとは？</p>
  <p class="marginB20">最も良く聞く名称として、「テレアポ」があります。テレアポとは、<span class="Blue_text">テレフォンアポインターの略で、電話営業の仕事</span>となります。<br />住所録などから取得された名簿に、片っ端から電話をかけ（アウトバウンド・発信業務・架電）商品やサービスを販売する為にお客様と営業マンとのアポイントを取得します。</p>

  <p><span class="Blue_Btextbold">e.g.</span></p>
  <p class="marginBottom20px sectborder"><span class="Blue_text">★</span>生命保険の営業アポイント　<span class="Blue_text">★</span>無料サンプル等のモニター獲得業務<br /><span class="Blue_text">★</span>太陽光発電新規アポ　<span class="Blue_text">★</span>経理担当者向けの税務セミナー集客業務</p>
</section>

  <section class="sect03">
  <p class="dot_yellowtext_Number">2</p>
  <p class="dot_yellowtext_title">テレオペとは？</p>
  <p class="marginB20">「テレオペ」とは、<span class="Blue_text">テレフォンオペレーターの略で、電話対応や電話取り次ぎ業務の仕事</span>となります。テレアポとは反対に、電話を受ける業務（インバウンド・受信業務）となります。<br />上場企業やショッピングサイトなどで、お客様からのお問い合わせに対応するお仕事となります。カスタマーサポート、テクニカルサポート、クレーム処理などがこれにあたります。</p>

  <p><span class="Blue_Btextbold">e.g.</span></p>
  <p class="marginBottom20px sectborder"><span class="Blue_text">★</span>上場企業の健康食品の通販受注　<span class="Blue_text">★</span>商品説明<br /><span class="Blue_text">★</span>WEB広告業でのカスタマーサポート</p>
</section>

  <section class="sect04">
  <p class="dot_yellowtext_Number">3</p>
  <p class="dot_yellowtext_title">テレマとは？</p>
  <p class="marginB20 sectborder">「テレマ」とは、<span class="Blue_text">テレフォンマーケティングの略で、既存のお客様に対して、商品やサービスの販売促進（アップセルなど）を行う電話業務の仕事</span>となります。<br />既にお客様となっている顧客リストへの電話、及び以前にお問い合わせを頂いた潜在顧客へのマーケティング手法となり、インバウンド、アウトバウンドといった区別はありません。<br />また、テレマには、既存顧客に対して、顧客満足度などを市場調査する目的も含まれていますので、仮に案内する製品が不要の場合でも顧客ニーズをヒアリングする必要があります。
</p>
</section>

  <section class="sect05">
  <h2 class="sideBlueBorder_blueText02 marginBottom10px">テレアポ・テレオペ・テレマの違いは？</h2>
    <img src="/public/images/ccwork/045/sub_001.jpg" class="imagemargin_T10B10" alt="ガムを噛む" />
  <p class="marginBottom20px">テレアポ・テレオペ・テレマについての説明は、上記に紹介した通りですが、これらの違いを簡単に説明すると、以下のようになります。</p>
    <img src="/public/images/ccwork/045/sub_002.jpg" class="imagemargin_B25" alt="ガムを噛む" />
     <p class="marginBottom20px">この中でどの仕事がいいかは人それぞれによって異なってきますが、とにかく手っ取り早く仕事を始めたければ、<span class="Blue_text">専門知識が不要なテレアポがおすすめ</span>です。該当する分野での専門知識に詳しければ、カスタマーサポート系のテレオペがおすすめです。両社のバランスをとりながら<span class="Blue_text">報酬もたくさん欲しい方は、テレマがおすすめ</span>です。<br />しかしながら、テレアポ・テレマ・テレマについては、明確に使い分けをしていない企業もありますので、求人情報を確認するとともに、応募面接などでもしっかりと確認しておきましょう。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">それぞれの成功の法則！</h2>
    <img src="/public/images/ccwork/045/sub_003.jpg" class="imagemargin_B25" alt="ガムを噛む" />
  <p class="dot_yellowtext_Number">1</p>
  <p class="dot_yellowtext_title">テレアポ成功法則</p>
  <p class="marginB20 sectborder">テレアポで成功するコツは、<span class="Blue_text">とにかく数をこなすことです。断られるのが当たり前・留守が当たり前を前提に、その商品に少しでも興味があるお客様を見つけることが大切です。<br />そして、製品やサービスに興味があるお客様を見つけた時の為に、マニュアルの内容はしっかりと把握し、スクリプトの練習もしておきましょう。</p>

  <p class="dot_yellowtext_Number">2</p>
  <p class="dot_yellowtext_title">テレオペ成功法則</p>
  <p class="marginB20 sectborder">テレオペの場合、かかってきた電話に対する対応となりますので、まずはマニュアルをしっかりと読んで、やるべきこと、習得すべき技術を明確にしておきましょう。<br />カスタマーサポートやテクニカルサポートになると、技術力などの専門知識が大切になってきますので、少しでも空いた時間ができたらどんどん勉強していく姿勢も大切です。</p>

  <p class="dot_yellowtext_Number">3</p>
  <p class="dot_yellowtext_title">テレマ成功法則</p>
  <p class="marginB20">テレマの場合、<span class="Blue_text">アウトバウンドとインバウンドの両方</span>となりますが、基本的には電話をかけることが多くなります。既に製品やサービスをお使いのお客様に新しい商品をご案内する仕事なので、比較的話を聞いてくれる環境にあるでしょう。<br />しかしながら、お客様自身も製品について実際に使われている場合が多く、<span class="Blue_text">お客様に負けない商品知識を身につけること</span>が必要です。<br />つまり、紹介する製品やサービスに興味を持ってくれるお客様を探していくことが大切です。テレマは、色々なスキルが必要とされる分<span class="Blue_text">時給やインセンティブも高くなる</span>傾向があります。営業成績の良い先輩を見習い、良いところは真似をして自分自身の営業スキルも向上させていきましょう。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">コールセンターで希望する職種を見つけるために</h2>
  <p class="marginBottom10px">一言でコールセンターと言っても、テレアポ・テレオペ・テレマなど様々な業務があります。さらに、扱う商品やサービスによって会社や企業も変わってきます。<br />自分はどの業界のどの仕事が向いているのか！？最初のうちは良く分からないかも知れませんが、あせらずじっくりと探していきましょう。</p>
  <p class="marginBottom40px">本当に希望する仕事がどうかに関しては、求人案内を見るだけではなかなか分かりにくい部分もあるので、実際に説明会や面接に出かけて詳しい話を聞きましょう。<br />その上でそのコールセンターで自分が実際に働いている姿がリアルに想像できて、その状態にワクワクできるようだったら、自分に向いている仕事かも知れませんね。</p>
</section>
<!--- 段落 終了 ---> 
  
  <aside>
  <dl>
  <dt class="headingG01">「コールセンター基礎知識」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail13">NGワードとクッション言葉に気をつけよう！</a></li>
  <li><a href="/ccwork/detail23">日本語を正しく使えていますか？コールセンター業務に必要な丁寧語、謙譲語、尊敬語</a></li>
  </ul></dd>
  </dl>
  </aside>
  
<aside class="tabNav_ccwork">
<ul><li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_a.png" alt="基礎知識" class="mediaPC" height="" width="192"><img src="/public/images/ccwork/nav/tabnav_01_03_a.png" alt="基礎知識" class="mediaSP" height="" width="286"></a></li><li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" alt="知っ得情報" class="mediaPC" height="" width="192"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" alt="知っ得情報" class="mediaSP" height="" width="286"></a></li><li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" alt="働く人の声" class="mediaPC" height="" width="192"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" alt="働く人の声" class="mediaSP" height="" width="286"></a></li></ul>
</aside><!-- / tabNav -->
  
</section>

