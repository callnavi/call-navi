<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">働く人の声</span></div>
  <h1>ガムを噛め！<br />仕事中のリフレッシュは、これに限る！</h1>
  </header>
  
<!--- 段落１ 開始 --->  
  <section class="sect01">
    <img src="/public/images/ccwork/042/main001.jpg" class="imagemargin_T10B10" alt="ガムを噛め！仕事中のリフレッシュは、これに限る！" />
 <p class="marginBottom10px">仕事中の気分転換、あなたはどのようにしていますか？</p>
 <p class="marginBottom10px">「先輩！気分転換に今から海に行ってきます！」</p>
 <p class="marginBottom10px">そんなことが許されればいいのですが、そうもいかないのが社会人ですよね。<br /><span class="Blue_text">どのようなお仕事であっても、ストレスや疲れは付きものなので、上手な気分転換スキルが社会人には必須</span>です。</p>
 <p class="marginBottom10px">コールセンターの仕事は、お客様の声のみを頼りに、お客様の話の意図や感情を汲み取らないといけないので、想像以上に集中力が必要になります。</p>
 <p class="marginBottom10px">次のお客様に新たな気持ちで対応ができるよう、休憩時間にオフィスでサッとできる、<span class="Blue_text">簡単だけど効果的なリフレッシュ方法をご紹介</span>します。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">ガムを噛む</h2>
    <img src="/public/images/ccwork/042/sub_001.jpg" class="imagemargin_T10B10" alt="ガムを噛む" />
  <p class="marginBottom10px">え！？王道すぎない？！と思いましたか？確かに、王道中の王道ですが、下記の効果を知って、ガムを噛めばあなたもたったの数分でリラックスできちゃうはず！</p>

  <ul class="fukidasi02_graybox">
<p class="marginBottom_none"><span class="Blue_Btextbold">◯脳内の幸せホルモン「セロトニン」が活性化</span></p>
<p class="marginBottom20px">ガムを噛む動作は、体を一定のリズムで繰り返し動かす「リズム運動」のひとつ。そのリズム運動を繰り返すと脳内にハッピーホルモン「セロトニン」が分泌され、心にやすらぎや落ち着きを感じるのです。</p>
<p class="marginBottom_none"><span class="Blue_Btextbold">◯集中力アップ！</span></p>
<p class="marginBottom_none">ガムを噛むと顎や口の筋肉運動が脳を刺激し、脳の血液循環が促され、脳の神経細胞が活性化するそうです。その結果、集中力がアップするという嬉しい効果もあります。次のお客様対応に向けて、集中力を再度アップさせるのには、ちょうどいいアイテムですね！</p>
</ul>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">トイレに行く</h2>
    <img src="/public/images/ccwork/042/sub_002.jpg" class="imagemargin_T10B10" alt="トイレに行く" />
  <p class="marginBottom10px">これも非常に一般的なのですが、経験上「場所を変える」という観点から、非常に効果があります。どうしても集中が切れてしまったり、うまくいかないことがあったり…。</p>
  <p class="marginBottom10px">そのようなときは、<span class="Blue_text">少し自分のデスクから離れてみましょう。</span></p>
  <p class="marginBottom10px">私もコールセンターでのアルバイト経験があるのですが、今の流れをたち切りたい時や、どうしても気分が乗らない時は、トイレに行って軽いストレッチをして席に戻っていました。休憩時間であれば、コンビニや外に出て室内とは違う風に当たってみるのもお勧めです。煮詰まりすぎる前に、早めに対処しておきましょうね。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">アロマオイルを香ってみる</h2>
    <img src="/public/images/ccwork/042/sub_003.jpg" class="imagemargin_T10B10" alt="アロマオイルを香ってみる" />
  <p class="marginBottom10px">人間はストレスや緊張を感じている時、呼吸が浅くて速くなる傾向があります。またコールセンターも含め、座りながらパソコンを使うお仕事だと、どうしても姿勢が悪くなりがちです。</p>
  <p class="marginBottom10px">胸郭がつぶれた状態が慢性化し、呼吸時に上下に動く横隔膜の動きが妨げられるため、より呼吸が浅くなる結果を生みます。そんな時、深い呼吸に戻すお手伝いをしてくれるのが「アロマオイル」です。</p>
  <p class="marginBottom10px">最近では１本１０００円程度から様々な香りのアロマオイルが販売されていますよね。アロマオイルのボトルは小さいので鞄にいれて持ち運びをしても邪魔になりません。<span class="Blue_text">ストレスを感じたときにアロマの香りを嗅ぐと、自然と呼吸が深くなり、楽に呼吸ができるようになります。深い呼吸は体のこわばりをほぐしてくれるので、心の状態も楽になります。</span></p>
  <p class="marginBottom10px">私も５年前位からラベンダーのアロマオイルのボトルを持ち歩いています。朝出かける際にハンカチにアロマオイルを１、２滴垂らして外出することも。そうすると鞄もいい香りになりますし、ハンカチを出す度に、ほのかにアロマオイルが香り「いい女度」が上がったような気分を味わうことができます。</p>
  <p class="marginBottom10px">またハンカチのアロマの香りも6時間程度は持つので、アロマオイルのボトルを持ち運ぶ必要もなくなります。（アロマオイルにもよりますが）私はお客様に電話をしながら、ハンカチのアロマを香っていたり、通勤の満員電車でも、ハンカチを鼻にあてていたりしました。</p>
  <p class="marginBottom20px">ぜひあなたのお好きな香りのアロマを１本、鞄に忍ばせておくことをオススメします★</p>

    <h3 class="blueblockBG">最後に</h3>
  <p class="marginBottom40px">いかがでしたでしょうか？自分でできるセルフケア術を身に付け、いつでもストレスフリーでハツラツと働きたいものですね♪集中力を必要とするコールセンターのでのお仕事ですが、うまくリフレッシュしながら、楽しく新鮮な心でお客様対応をしていきましょう！</p>
</section>
<!--- 段落１ 終了 ---> 
  

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター働く人の声」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail32">どうするクレーム？しっかり対応するための魔法の４ステップ！</a></li>
  <li><a href="/ccwork/detail41">まさかの逆効果に！？甘いものの罠。それリフレッシュになっていません！</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  
  <aside class="tabNav_ccwork">
  <ul id="js-tab01">
  <li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_a.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_a.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li>
  </ul>
  </aside><!-- / tabNav -->
  
  
</section>

