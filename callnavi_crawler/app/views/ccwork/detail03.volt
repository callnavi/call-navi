<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">基礎知識</span></div>
  <h1>全国調査！<br>コールセンターの時給比較</h1>
  </header>
  
  <section class="sect01">
  <p class="marginBottom20px">働く上で気になるのは時給。<br>
  コールセンターは、商材知識が必要であるなど専門性が強いため、他の職種と比べて時給は高めです。特に電話営業、いわゆるテレアポは時給が高めに設定されており、地域によっても金額に差があります。関東が最も平均時給が高く、北海道・東北・中国・四国地方は全国平均をやや下回る傾向にあります。</p>
  <div></div>
   <img src="/public/images/ccwork/002/img01.png" alt="表「全国コールセンター時給比較」" />
  <p class="link_margin60px">※<a href="http://saponet.mynavi.jp/baitojikyu/data/baitojikyu201402.pdf" target="_blank">マイナビバイト～2014年2月平均時給データ～</a>による。</p>
  </section>

<aside>
<dl>
<dt class="headingG01">「コールセンター基礎知識」オススメのページ</dt>
<dd>
<ul class="linkListA01">
<li><a href="/ccwork/detail01">コールセンターってどんな仕事？</a></li>
<li><a href="/ccwork/detail04">コールセンターの雇用形態</a></li>
</ul>
</dd>
</dl>
</aside>


<aside class="tabNav_ccwork">
<ul><li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_a.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_a.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li><li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li><li><!--<a href="/ccwork#tabContents03">--><span><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></span><!--</a>--></li></ul>
</aside><!-- / tabNav -->


</section>
