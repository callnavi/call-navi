<section class="entryA01 ccwork_entry">
<header>
<div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">働く人の声</span></div>
<h1>裏技教えます！<br />本当は教えたくない受注が取れるコツ</h1>
</header>
  
<section class="sect01">
<h2 class="sideBlueBorder_blueText02 marginBottom10px">大公開！本当は教えたくないのですが・・・</h2>
<p class="marginBottom10px">コールセンターで周りを見渡せば、どんどん受注を取っているスーパー営業マン！<br />いるんですよね！どうしていつも、あの人はあんなに取れているの？あの人は何か特別なスキルや裏技を持っているのでは？そう思っているあなたに送りたい！取れる人が実践している<span class="Blue_text">本当は教えたくない</span>裏技を特別に大公開します。</p>

<div class="freebox01_blueBG_wrapper">
<p class="Blue_Btextbold">お客様と一緒に・・・</p>
<p class="marginBottom_none">僕は、お客様が<span class="Blue_text">悩んで黙り込んだらチャンス</span>だと思っています。申し込もうかどうか悩んだときですね。サービスのメリットなどをアピールして、薦めたくなるところを、ちょっと我慢。沈黙をおそれず一緒に黙ってみます。するとなぜか、前向きな回答が返ってくることが多いです！正確な理由はわからないのですが、お客様を尊重している姿勢が伝わったのでしょう。自分だったら、押し売りをされるよりも、<span class="Blue_text">親身になって一緒に考えてくれる方が信頼できる</span>と思いますから。<br /><span class="text_small">（正社員23歳／男性）</span></p>
</div>
<img src="/public/images/ccwork/067/main001.jpg" class="imagemargin_T10B10" alt="裏技教えます！本当は教えたくない受注が取れるコツ" />

<div class="freebox01_blueBG_wrapper">
<p class="Blue_Btextbold">ポイントは時間帯！</p>
<p class="marginBottom_none">私は、時間ごとに区切って、電話をかけるリストを作っています。職業よってお客様の生活スタイルは様々です。なので、<span class="Blue_text">休憩時間や慌ただしくない時間帯などを見計らって</span>、電話をかけるようにしています。「今忙しい」と言われたら、<span class="Blue_text">「では、何時頃ならご迷惑ではないでしょうか。」と時間を聞いて</span>、見込みのあるお客さまを作っていき、リスト分けしたりもしています。最近のコールセンターは、システムも整っていて、課電するお客様のリストを見やすく管理できたりもするので助かっています！<br /><span class="text_small">（アルバイト22歳／女性）</span></p>
</div>
<img src="/public/images/ccwork/067/sub_001.jpg" class="imagemargin_T10B10" alt="ポイントは時間帯！" />

<div class="freebox01_blueBG_wrapper">
<p class="Blue_Btextbold">求められていることに応える</p>
<p class="marginBottom_none">サービスのことではないですよ！<span class="Blue_text">合わせるのは僕自身です。</span>例えば、ご年配の方だったらお孫さんになったつもりで接したり、相手が女性の方だったら安心してもらえるように柔らかい口調や優しい口調を心がけ友達と話しているような雰囲気を作ったり、また、お金のことを気にされている方だったらキャッシュバック特典などがあればそこをアピールしたりしていますね。常に、サービスはもちろん、人間的な接し方も含め、<span class="Blue_text">この方は何を求めているだろう？</span>と考えながら会話をしていますね。<br /><span class="text_small">（アルバイト30歳／男性）</span></p>
</div>
<img src="/public/images/ccwork/067/sub_002.jpg" class="imagemargin_T10B10 sectborder1" alt="求められていることに応える" />

<h2 class="sideBlueBorder_blueText02 marginBottom10px">勝利の法則があるんです！</h2>
<p class="marginBottom10px">そして、経験を積んだ現場のマネージャーからも、知って得する勝利の法則を聞きました。</p>
<img src="/public/images/ccwork/067/sub_003.jpg" class="imagemargin_T10B10" alt="勝利の法則があるんです！" />

<h3 class="blueblockBG">勝利の法則</h3>
<ul class="dot_bluetext">
<li><span>1</span>架電リスト</li>
<li><span>2</span>コール数</li>
<li><span>3</span>トーク</li>
<li><span>4</span>気持ち</li>
</ul>
<p class="marginBottom20px sectborder1">これからコールセンターで働く、または働いている皆さんに実践してもらいたいのは<span class="Blue_text">③と④</span>ですね！リストとトークは、ある種の武器です。<br />それは経験を積んだ責任者が用意して戦略を考えてくれます。<br />皆さんは、与えられた武器を使って、とにかくコール数を上げて、<span class="Blue_text">繋がったお客様はすべて受注できる！</span>そういう強い気持ちで架電してもらいたいと思います。必ず受注できます。</p>

<p class="marginBottom40px">戦略やコツはたくさんあるので、徐々に身につけていくもの！<br />まず大切なのは、<span class="Blue_text">絶対受注できる</span>という強い気持ちと自信を持つこと！これが受注を取れるようになる一番の近道なのかもしれないですね！</p>
</section>

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター働く人の声」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail58">コールセンターバイトで、就職活動を有利に！電話対応、コミュ力、ビジネスマナーは最大の武器！</a></li>
  <li><a href="/ccwork/detail73">【質問】コールセンターバイトでの休憩時間は、仲間と積極的に話すべき！？　できれば一人でゆっくりしたいのですが･･･</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  
  <aside class="tabNav_ccwork">
  <ul id="js-tab01">
  <li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_a.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_a.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li>
  </ul>
  </aside><!-- / tabNav -->
  
  
</section>
