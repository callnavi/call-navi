<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">基礎知識</span></div>
  <h1>正社員登用制度を活用せよ！<br />アルバイト入社でも実力次第で、正社員になれるコールセンター</h1>
  </header>
  
<!--- 段落１ 開始 --->  
  <section class="sect01">
 <p class="marginBottom10px">コールセンターの仕事がしたい！と思って、求人サイトや求人広告を見たとき、どこをチェックしますか？時給？勤務時間？雇用形態？
今回は、コールセンターの雇用形態について、ご紹介します。
</p>
    <img src="/public/images/ccwork/048/main001.jpg" class="imagemargin_T10B10" alt="正社員登用" />
</section>

  <section class="sect02">
  <h2 class="sideBlueBorder_blueText02 marginBottom10px">コールセンターの仕事</h2>
  <p class="marginBottom10px">まずは、コールセンターでのお仕事について簡単に説明します。大きく分けて、<span class="Blue_text">インバウンドとアウトバウンド</span>があります。<br /><span class="Blue_text">インバウンド</span>は、電話を受ける（受電）仕事で、問い合わせ対応・通販商品の受注・カスタマーサポートなどがあります。<br /><span class="Blue_text">アウトバウンド</span>は、電話をかける（架電）仕事で、潜在顧客リストに電話をかけて商品やサービスの説明を行い、商品購入やサービスの契約を行います。テレアポ（テレフォンアポインター）がこれにあたります。</p>
  <p class="marginBottom20px">正社員・派遣社員・アルバイトといった雇用形態に関わらず、インバウンド・アウトバウンドの仕事があります。正規雇用と非正規雇用で見ると、業界や会社によっても異なりますが、非正規雇用の方が多くなっています。</p>
    <img src="/public/images/ccwork/048/sub_001.jpg" class="imagemargin_B25" alt="コールセンターの仕事" />

  <p class="dot_yellowtext_Number">1</p>
  <p class="dot_yellowtext_title">アルバイト・パートの仕事</p>
  <p class="marginB20">テレアポの仕事といったら、アルバイトが最初に思い浮かぶ方も多いでしょう。<span class="Blue_text">学歴不問・経験不問・高時給・シフト制・エアコンの効いた社内で仕事ができる</span>こともあり、アルバイトの中でも人気の職種となっています。</p>
  <p class="marginBottom20px sectborder">アルバイトのメリットは、正社員や派遣社員と比較してノルマなど責任があまりないことや、アウトバウンドでは<span class="Blue_text">営業成績を上げれば時給アップやインセンティブなどによって、しっかり稼げる</span>ことではないでしょうか。これらは、コールセンターを運営している会社の方針や業界などによって異なってくると思いますが、やったらやっただけ稼げる魅力があります。<br />あとは、<span class="Blue_text">シフト制で学業やプライベートとの両立</span>がしやすかったり、<span class="Blue_text">残業がなかったり（または少ない）、駅チカ</span>など好きな勤務地が選べたり、コールセンターによっては<span class="Blue_text">日払いOK</span>などに対応してくれたりすることがあります。</p>

  <p class="dot_yellowtext_Number">2</p>
  <p class="dot_yellowtext_title">派遣社員の仕事</p>
  <p class="marginB20">派遣社員は、正社員とアルバイトの中間地点のような感じで、責任は正社員ほどではないけどアルバイトよりは重い、給料も正社員ほどではないけどアルバイトより高いといった感じです。</p>
  <p class="marginBottom20px sectborder">残業をする・しないも選べるところが多いようで、頑張り方によっては、<span class="Blue_text">SV（スーパーバイザー）に昇格</span>できる可能性があることも魅力です。<br />あとは、直接雇用ではなく派遣会社に所属しているので、<span class="Blue_text">派遣会社の研修制度</span>や<span class="Blue_text">福利厚生</span>が利用できたり、契約期間が終了しても<span class="Blue_text">別の配属先を紹介</span>してもらえるメリットがあります。<br />派遣の契約期間は最長でも<span class="Blue_text">3年</span>なので、その後は配属先の直接雇用となるか、いったんはその配属先を離れるかの選択が必要になります。専門的なスキルや知識は身につけたいけど、一ヶ所に留まるのではなく様々な業種や会社のコールセンターを経験したい方に適した雇用形態です。</p>

  <p class="dot_yellowtext_Number">3</p>
  <p class="dot_yellowtext_title">正社員の仕事</p>
  <p class="marginB20">コールセンターの正社員募集は、実際のところそんなに多くはありません。チーム全体をまとめるリーダーやマネージャー的な存在だったり、コールセンター全体をまとめるセンター長の役割だったりすることが多いです。</p>
  <p class="marginBottom20px"><span class="Blue_text">ノルマや売上目標</span>に責任を持ったり、オペレーターの代わりに<span class="Blue_text">クレーム処理を対応</span>したり、オペレーターの教育を行ったりします。<br />正社員であるが故に<span class="Blue_text">雇用が安定</span>していたり、給料もアルバイトや正社員と比較して高めであったりする一方で、<span class="Blue_text">試用期間</span>も存在したり、<span class="Blue_text">残業代が月給に組み込まれている場合もある</span>ので、気になる点は面接時などに確認しておきましょう。<br />正社員としてコールセンターに転職したい方は、業界や会社によっては、<span class="Blue_text">未経験</span>でも正社員を募集している場合があるので、興味がある方は求人情報をこまめにチェックしてください。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">アルバイトから、社員登用で正社員へ</h2>
    <img src="/public/images/ccwork/048/sub_002.jpg" class="imagemargin_T10B10" alt="アルバイトから、社員登用で正社員へ" />
  <p class="marginBottom10px">上記で様々な雇用形態について紹介しましたが、<span class="Blue_text">入社時はアルバイトであっても、後に正社員として業務に従事</span>している人は多いようです。</p>
  <p class="marginBottom20px">学歴・職歴・資格・年齢・性別などによらず、結果を出した人を現場のリーダー・SV・マネージャーなどに昇格させていく文化がありますので、まずは与えられた仕事をまっとうして、少しでも早く結果を出せるよう努力していきましょう。<br />入社時は、バンドマンで茶髪にピアス、社会人としてのマナーも勉強中という方も、売上を上げていくにしたがって人間が変わって<span class="Blue_text">スーツが似合うようになった！</span>なんてケースもあるようです。<br />コールセンターは、<span class="Blue_text">いい意味で実力主義が徹底された職場が多い</span>です。しかも結果を出すためには、研修やトレーニングを真面目にこなし、マニュアルをしっかりと読み込み、トークの練習を怠らないなど、与えられた環境でしっかりと学習することで得られるものが多いです。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">コールセンターの仕事を探している方へ</h2>
  <p class="marginBottom40px">コールセンターは日本全国にたくさんあります。求人サイトを見ると、求人数はコンスタントに多いです。しかし、<span class="Blue_text">自分にあった職場を見つけるのはなかなか難しい</span>もの。しかも、雇用形態が自分と合っていないと、候補のリストから削除してしまう場合もあるかも知れません。<br />このコールセンターで働きたい！と思ったら、正社員・派遣社員・アルバイトといった<span class="Blue_text">雇用形態が希望通りでなくても、まずは働いてみましょう。</span><br />その中で結果を出すことにより、SV・正社員への道が開け、給料・雇用条件も良くなっていくかも知れません。</p>
</section>

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター基礎知識」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail13">NGワードとクッション言葉に気をつけよう！</a></li>
  <li><a href="/ccwork/detail23">日本語を正しく使えていますか？コールセンター業務に必要な丁寧語、謙譲語、尊敬語</a></li>
  </ul></dd>
  </dl>
  </aside>
  
<aside class="tabNav_ccwork">
<ul><li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_a.png" alt="基礎知識" class="mediaPC" height="" width="192"><img src="/public/images/ccwork/nav/tabnav_01_03_a.png" alt="基礎知識" class="mediaSP" height="" width="286"></a></li><li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" alt="知っ得情報" class="mediaPC" height="" width="192"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" alt="知っ得情報" class="mediaSP" height="" width="286"></a></li><li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" alt="働く人の声" class="mediaPC" height="" width="192"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" alt="働く人の声" class="mediaSP" height="" width="286"></a></li></ul>
</aside><!-- / tabNav -->
  
</section>

