<section class="entryA01 ccwork_entry">
<header>
<div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">働く人の声</span></div>
<h1>コミュニケーションのコツはVAK！<br />テレアポはもちろん、普段の会話にも劇的に変わります！</h1>
</header>
  
<section class="sect01">
<p class="marginBottom20px">コミュニケーションが<span class="Blue_text">伝わりやすかったとき・伝わりにくかったとき</span>これらの違いについて考えたことはありますか？<br />今回は、伝わりやすいコミュニケーションについて紹介します。</p>
<img src="/public/images/ccwork/080/main001.jpg" class="imagemargin_T10B30" alt="コミュニケーションについて" />

<h2 class="sideBlueBorder_blueText02 marginBottom10px">コミュニケーションについて</h2>
<p class="marginBottom20px">コミュニケーションと聞いて、何をイメージしますか？<br />ちゃんと意思疎通ができたことよりも、言いたいことがなかなか伝わらなくてイライラしてしまったことや、相手が何を言っているか理解できず困ってしまった経験が思い出されるのではないでしょうか。</p>

<h2 class="sideBlueBorder_blueText02 marginBottom10px">伝わらなくて苦労するとき</h2>
<img src="/public/images/ccwork/080/sub_001.jpg" class="imagemargin_T10B30" alt="伝わらなくて苦労するとき" />
<div class="freebox01_blueBG_wrapper">
<p class="marginBottom_none Blue_text">■駅のアナウンスが聞き取りにくい</p>
<p class="marginBottom_none Blue_text">■職場で上司の言っていることが理解できない</p>
<p class="marginBottom_none Blue_text">■アメリカ英語学んだけど、イギリス英語が理解できない</p>
<p class="marginBottom_none Blue_text">■リスニングテストで、男性の声が低くて聞き取れない</p>
</div>
<p class="marginBottom10px">などなど、コミュニケーションがうまくいかなくて苦労することがあると思いますが、この中でも駅のアナウンスや、職場でのコミュニケーションに苦労している人は多いのではないでしょうか？</p>
<p class="marginBottom20px">伝えるプロである柴田祐規子アナ・守本奈美アナ・高橋美鈴アナといったNHKの女子アナのしゃべり方を研究してみたり、専門のカウンセリングやレッスンを受けたり、説得力などに関する本を読んだりするなど、方法はいくつかありますが、実はそんな難しいことをしなくても、コミュニケーションを劇的に改善する方法があるんです。</p>

<h2 class="sideBlueBorder_blueText02 marginBottom10px">何で伝わらないの？</h2>
<p class="marginBottom10px">そもそも、伝えるための道具であるはずのコミュニケーションで、なぜこんなにも伝わらないのでしょうか？コミュニケーションでちゃんと伝わらない例としては、以下が考えられます。</p>
<div class="freebox01_blueBG_wrapper">
<p class="marginBottom_none Blue_text">■何を言っているのか、意味が分からない</p>
<p class="marginBottom_none Blue_text">■声が小さく、よく聞こえない</p>
<p class="marginBottom_none Blue_text">■（聞く側が、相手の話を）聞く気になれない</p>
<p class="marginBottom_none Blue_text">■（話す側の）声や態度が受け付けない</p>
<p class="marginBottom_none Blue_text">■ノリやテンションが違う</p>
<p class="marginBottom_none Blue_text">■そもそも伝えようとする意志がない</p>
</div>
<p class="marginBottom10px">これらは一部聞く側の問題のように見えますが、<span class="Blue_text">実は、ほとんど全ての問題は話す側に起因しています。</span>もちろん、聞く側で聴こうとしていることが前提ですが。</p>
<p class="marginBottom20px">私たちは小学校以来、「ちゃんと話を聞きなさい！」とは言われてきましたが、<span class="Blue_text">相手が興味を持つような話し方・相手に伝わるような話し方</span>については、教育を受けてきた方は少ないのではないでしょうか？</p>

<h2 class="sideBlueBorder_blueText02 marginBottom10px">聞き取りやすさの基準は、万人に共通しているの？</h2>
<p class="marginBottom10px">「あの人の話分かりやすいよね～」<br />「●●先生の講義は、分かりやすくて人気があるよね。」>br />なんて話に出てくることがありますが、このような方に共通しているものとして、以下があります。</p>
<div class="freebox01_blueBG_wrapper">
<p class="marginBottom_none Blue_text">■論理的（または、順序立てて）話す</p>
<p class="marginBottom_none Blue_text">■分かりやすい言葉で話す</p>
<p class="marginBottom_none Blue_text">■抑揚をつけて、ポイントを分かりやすく話す</p>
</div>
<p class="marginBottom20px">なるほど！と思うものもあって、これを改善すれば良さそうですが、実はこれだけではないんです。</p>

<h2 class="sideBlueBorder_blueText02 marginBottom10px">聞き取りやすさの基準で、共通しない部分</h2>
<p class="marginBottom20px">聞き取りやすさの部分で共通しない部分として、話すスピードと言葉の種類があります。<br />VAKといって、人間には大きく分けて<span class="Blue_text">V（視覚重視タイプ Visual）・A（聴覚重視 Auditory）・K（感覚重視 kinesthetic）</span>の３つのタイプがあります。<br />共通しない部分とは、VAKの部分です。相手がどのタイプかを見極めて、それにあった話し方をするのが効果的です。</p>

<ul class="dot_bluetext">
<li><span>V</span>視覚重視タイプ</li>
</ul>
<p class="marginBottom20px">頭の中にイメージが次々と浮かんできて、それをしゃべっているので、基本的に早口です。何かを考えたりするときに、上を見ることも多いです。<br />聴覚重視タイプには、頭にイメージしやすい言葉をテンポ良くしゃべるのが効果的です。しゃべり方が遅いと、頭が悪いと思われてしまい、話を聞いてくれないこともあるので要注意です。</p>

<ul class="dot_bluetext">
<li><span>A</span>聴覚重視タイプ</li>
</ul>
<p class="marginBottom20px">音に対して敏感です。アナウンサーなどに多いタイプで、とても聞きやすいです。「ドン！」「カシャ♪」など<span class="Blue_text">擬声語・擬音語</span>を使うのも特徴です。<br />視覚重視タイプには、一定のテンポで、擬声語・擬音語を混ぜながらしゃべるのが効果的です。話のペースは、相手がAタイプだと分かったら、それに合わせるのがいいでしょう。</p>

<ul class="dot_bluetext">
<li><span>K</span>感覚重視タイプ</li>
</ul>
<p class="marginBottom20px">体の感覚を大事にします。そして、その感覚を説明しようとするので、基本的に<span class="Blue_text">ゆっくりとした口調</span>で、言葉にするのは苦手な方多いかも知れません。<span class="Blue_text">温かい・冷たい</span>など感覚に関するキーワードを使うのが特徴的です。<br />感覚重視タイプには、ゆっくりと喋るのが効果的です。どのような製品やサービスなのか、感覚としてイメージしやすいように説明することがポイントとなります。</p>

<p class="marginBottom20px"><span class="Blue_text">心理学用語でラポール</span>という言葉があります。お互いのことに好感を持って、信頼して、心が通じ合っている状態のことを言います。<br />仲の良い恋人同士の行動が同じだったり、子供に話しかけるときに、いつの間にか赤ちゃん言葉になってしまったりすることを考えれば分かりやすいでしょう。</p>

<h2 class="sideBlueBorder_blueText02 marginBottom10px">VAKの使い方「こんなときどうする？」</h2>
<img src="/public/images/ccwork/080/sub_002.jpg" class="imagemargin_T10B30" alt="VAKの使い方「こんなときどうする？」" />
<p class="dot_yellowtext_Number">1</p>
<p class="dot_yellowtext_title">テキパキ主婦</p>
<p class="marginBottom20px sectborder1">早口で、テキパキをしゃべる主婦。多分家事も効率的に仕上げているんだろうなあ、という感じの方は、<span class="Blue_text">V（視覚重視）である可能性が高い</span>です。<br />相手の方がイメージしやすい言葉を使って、テンポよく会話を進めていきましょう。自分がK（感覚重視）タイプだと、言葉に詰まってしまう可能性もあるので、予めキーワードをまとめておくとよいでしょう。</p>

<p class="dot_yellowtext_Number">2</p>
<p class="dot_yellowtext_title">テレアポの仲間どうし</p>
<p class="marginBottom20px sectborder1">テレアポというしゃべる職業についている方は、<span class="Blue_text">A（聴覚重視）の方が多い</span>です。この方の場合、一定のテンポで、早くもなく、遅くもなくしゃべるのが特徴です。相手がA（聴覚重視）の場合は、相手のテンポに合わせながら、音に関する言葉を混ぜてコミュニケーションをとっていくのがいいでしょう。</p>

<p class="dot_yellowtext_Number">3</p>
<p class="dot_yellowtext_title">体育会系の友人</p>
<p class="marginBottom20px sectborder1">スポーツ選手・スポーツインストラクター・マッサージ師など体に関するスペシャリストの方は、<span class="Blue_text">K（感覚重視）の方が比較的多い</span>です。<br />この方は、感覚器から非常に多くの情報を受け取って、それを処理しています。そして、その情報は非常に言語化しにくい為、コミュニケーション上、ストップしているように見えることがあります。<br />そんな場合には、話すペースをその方に合わせるか、感覚的な言葉を使って相手の方が理解しやすいようにして上げることが大切です。</p>

<p class="dot_yellowtext_Number">4</p>
<p class="dot_yellowtext_title">お年寄り</p>
<p class="marginBottom20px sectborder1">これは、応用編かも知れません。まず、耳が遠ければ、ゆっくりと大きな声でしゃべったり、<span class="Blue_text">最近の言葉ではなく、お年寄りがしゃべったりする言葉をしゃべるのがいい</span>でしょう。その上で、VAKのタイプを見極め、コミュニケーションをとっていくのがいいでしょう。</p>

<p class="dot_yellowtext_Number">5</p>
<p class="dot_yellowtext_title">理系男子</p>
<p class="marginBottom20px sectborder1">仕事ができるイメージが強いかも知れませんが、ひょっとしたらプライドも高いだけかも知れません。<span class="Blue_text">技術関係の言葉や専門用語など、知ったかぶりをしないで、思い切って聞いてみる方法</span>もあります。相手の自尊心を尊重した上で、VAKにあったコミュニケーションをするとよいでしょう。</p>

<p class="dot_yellowtext_Number">6</p>
<p class="dot_yellowtext_title">キラキラ女子</p>
<p class="marginBottom20px sectborder1">見る人によっては、一見すると、制御不能に見えるかもしれません。でも、彼女たちの使っている言葉に注意を向けてみると、ある一定のルールがあると思います。<span class="Blue_text">カワイイ！・きれい！・おとく！・スゴイ！</span>など、これらから、ある程度の価値観を想定して、それに見合った話題や切り口を提供するのがいいでしょう。もちろん、VAKも忘れずに。</p>

<h2 class="sideBlueBorder_blueText02 marginBottom10px">テレアポでは</h2>
<img src="/public/images/ccwork/080/sub_003.jpg" class="imagemargin_T10B30" alt="テレアポでは" />
<p class="marginBottom10px">当然ですが、<span class="Blue_text">テレアポは電話での営業なので、身振り手振りは届かず、声しか届きません。</span>しかもその声は、マイクから入ったものがスピーカーやヘッドフォンから再生されるもので、声の調子やテンポが微妙に変わってしまうかも知れません。</p>
<p class="marginBottom10px">さらに、初対面の相手なのでお互いどんな人物か分からず、警戒心があったり、少なからず壁があったりします。</p>
<p class="marginBottom20px">このような不利な状況下であるからこそ、VAKのテクニックを使って相手にあったしゃべり方をすることによって、<span class="Blue_text">何となく気が会う・話が合う</span>と思ってもらえるかも知れません。</p>

<h2 class="sideBlueBorder_blueText02 marginBottom10px">VAKの応用編</h2>
<p class="marginBottom10px">VAKのスキルを使ってプレゼンテーションすると、<span class="Blue_text">自分とは違ったタイプの方にもメッセージを伝えることができる</span>ようになります。</p>
<p class="marginBottom10px">美容室・マッサージ・洋服の仕立て・コンピュータやスマホのカスタマーサービスにおいて、「これは、本当に相手に伝わっただろうか？」と心配になることがありますが、<span class="Blue_text">相手のアクセスモード（VAK）に合わせてしゃべることで、コミュニケーションの行き違いを最小限に抑える</span>ことができます。</p>
<p class="marginBottom10px">上記の他にも、初対面の方とコミュニケーションをとる場合に有効です。<span class="Blue_text">就職/転職などの面接・旅行中の値段交渉・道順を尋ねるとき</span>にも有効ですし、ひょっとしたらナンパの成功率も上がるかもしれません（笑）。</p>
<p class="marginBottom10px">VAKのスキルは、初対面の方とのコミュニケーションに有効だと思われていますが、友人・恋人・家族など身近な方とのコミュニケーションについても活用できます。<br />私たちは、相手との関係でどこまで理解できるか（理解してもらえるか）無意識のうちに決めてしまうところがありますが、相手のアクセスモード（VAK）に合わせてコミュニケーションをとることによって、実は意思疎通が十分にとれていなかったことに気がつく場合もあります。</p>

<h3 class="blueblockBG">まとめ</h3>
<img src="/public/images/ccwork/080/sub_004.jpg" class="imagemargin_T10B30" alt="まとめ" />
<p class="marginBottom40px">いかがでしたか？<br />コミュニケーションにはコツがあって、このコツをマスターすることで、コミュニケーションがスムーズに進むことが多いです。でも、スキルだけではダメで、まずは相手の方に興味を持つことが大切です。その上で、VAKのスキルを使うことで、相手の方との距離がより縮まりやすくなるでしょう。間違っても、相手をコントロールしようとしてはいけませんよ。</p>
</section>

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター働く人の声」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail57">結婚・引越し・出産があっても大丈夫！コールセンターの魅力とは？</a></li>
  <li><a href="/ccwork/detail58">コールセンターバイトで、就職活動を有利に！　電話対応、コミュ力、ビジネスマナーは最大の武器！</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  <aside class="tabNav_ccwork">
  <ul id="js-tab01">
  <li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_a.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_a.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li>
  </ul>
  </aside><!-- / tabNav -->
  
  
</section>
