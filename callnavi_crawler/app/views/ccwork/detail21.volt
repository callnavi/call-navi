<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">基礎情報</span></div>
  <h1>コールセンターで働くあの子！どんな１日を過ごしているの？</h1>
  </header>
  
  <section class="sect01">
    <img src="/public/images/ccwork/021/main001.jpg" class="marginBottom20px" alt="コールセンター応募、面接、採用、入社初日までの流れ" />
  <h2 class="sideBlueBorder_blueText02s">■「コールセンターで働きたいけど、実際のお仕事の１日の流れって、どうなっているの？」</h2>
  <h2 class="sideBlueBorder_blueText02s">■「まさか、ずっと電話をかけっぱなしじゃないよね！？」</h2>

<p class="marginBottom10px">コールセンターと聞くと、ひたすら電話と向き合う孤独な職業…というイメージをお持ちの方も、いらっしゃるのではないでしょうか。しかし、そんなことはありません！従業員をまとめているＳＶ（スーパーバイザー、現場のリーダーのこと）が、場を盛り上げ、スタッフの士気を高めてくれますので、部署全体に団結力があります。またお客様対応中に、不明点や疑問点がある場合は、電話を保留にしてＳＶに聞いたり、電話をＳＶに代わってもらったり、という場面もあるので、円滑なチームワークも必要になってきます。</p>
<p class="marginBottom10px">今日はコールセンターの実情が気になるあなたに、某都内コールセンターで新卒一般社員として勤務中の「Ａ子さんの１日」をお届けしたいと思います。</p>

  </section>
  
<section class="sect02">
  <h2 class="sideBlueBorder_blueText02">某都内コールセンター勤務：一般社員Ａ子の1日</h2>
  <div class="imageBlockA02 marginBottom30px">
<p class="image"><img src="/public/images/ccwork/021/sub_001.jpg" alt="箇条書きにする" /></p>
<div class="contentsInner1">
<p class="marginBottom20px">勤務経験：新入社員３ヶ月目<br />年齢：２３歳<br />性別：女子</p>
<p class="marginBottom_none">【仕事内容】<br />個人宅への通信系商材の紹介を担当</p>
</div>
</div>

</section>

<section class="sect03">
    <ul class="dot_bluetext">
    <li><span>1</span>11:45 出勤</li>
    <p class="marginBottom10px">朝は遅めなので、前日の夜に女子会があってもラクラク出勤ができます。満員電車に揉まれなくて済むのも、魅力の一つです。ときには、駆け込みセーフで、ギリギリ出勤になってしまうこともありますが、お昼でも電車が遅延することがありますので、時間には余裕をもって出勤しています。</p>
    <li><span>2</span>12:00〜12:10</li>
    <p class="marginBottom10px">１日の始まりは朝礼からです。コールセンターのお仕事は、声が命です。朝礼では発声練習代わりにスクリプト（お客様対応の台本のようなもの）の読み合わせをしたり、ＳＶから本日の目標や現在の進捗情報などの共有事項の話があったり、朝イチから（実際には昼イチ！？）仕事モード全開です！聞いた話によると、朝礼で発声練習をするコールセンターもあるそうですよ！<br />さぁ！今日も１日、丁寧な対応と素敵な笑声を心掛けましょう！</p>
    <li><span>3</span>13:00〜14:00 お昼休憩</li>
    <p class="marginBottom10px">んっ？！働き始めて１時間でお昼休憩は変じゃないの？！と思いませんでしたか？<br />そうなのです。コールセンターではシフト制を採用しているとこが多く、私より前の９時とか１０時に出勤している方もいるので、全体スケジュールとして、お昼は１３時からになります。</p><p class="marginBottom10px">私は休憩室完備のコールセンターで働いているので、そちらで同僚と昼食を取ることが多いです。節約と料理の腕を上げる練習を兼ねて、お弁当を持っていくことが多いですね。たまには同期とランチに出かけることもあります♪新宿や渋谷などコールセンターが都会にある場合には、外食もおオススメです！休憩時間は、場所によっては、ご当地グルメ巡り！なんて素敵ですね。このような楽しみが加わると、張り切って仕事ができそうですね！</p>
    <li><span>4</span>14:00〜14:10 昼礼</li>
    <p class="marginBottom10px">お腹もいっぱいになったところで、改めてＳＶによる昼礼があります。人によっては、ちょっぴり笑える小ネタやギャグを挟みながら、従業員のモチベーションを高めるような話をして下さったり、必要事項を再度確認したりします。「１人ひとりのお客様を大切に」対応しようと再確認ができる瞬間です。ここから午後の稼働がスタートします。</p>
    <li><span>5</span>14:10〜16:00 稼働（実際に電話対応を行います）</li>
    <li><span>6</span>16:00〜16:10 休憩+ＳＶによる意識合わせのお話</li>
    <p class="marginBottom10px">ここからは、基本的に２時間稼働し、１０分間の休憩のサイクルを繰り返します。そして各休憩の後には、ＳＶからやる気がぐーんと湧いてくるようなお話や、扱っている商材やサービスに関しての、情報や知識のアップデートがあります。<br />「電話ばかりだと、飽きちゃうよ〜」と思うかも知れませんが、そうならないための工夫が散りばめられていますので、安心して下さいね！<br />コールセンターにもよりますが、私の場合は、２時間で３０名～５０名の方とお話しています。従業員の喉を気遣い、トイレにうがい薬を常備していたり、マスクや空気洗浄機を置いていたりするコールセンターも多いようです。私は、お客様対応の合間に、こまめに水分補給をし、必要に応じてのど飴を食べています。<br />声が商売道具ですから、風邪等には万全の注意を払っています。同様や仲間と、のど飴を交換するのも、楽しみの一つになっていますよ！</p>
    <li><span>7</span>20:00</li>
    <p class="marginBottom10px">夜もふけてきましたね。少し疲れも出てきましたが、あと１時間で今日のお仕事も終わりです！疲れたなぁ～と思っていると、必ずＳＶが声を掛けにきてくれるんです！ＳＶはいつも従業員に目を配ってくれていて、お客様対応が長くなったり、様子が変だったりすると、いつの間にか隣にいてくれる、頼れる存在です。そして２０時以降は「２０時以降にお電話する」とお約束した方のみお電話をしています。<br />この時間にお電話をするさいは「夜分に恐れ入ります…」と必ず一言付け加えるように指導されています！この一言があるかないかでお客様への印象も変わりますよね！</p>
    <li><span>8</span>21:00 本日の営業終了！</li>
    <p class="marginBottom10px">お疲れ様でした！本日のコールセンター業務は、これにて終了です！始業の時間が遅めなので、終業時間も少々遅めです。コールセンターの一般社員の場合、残業をすることは滅多にないです。金曜日の夜などは、この後に同僚や先輩と飲み行って親睦を深めることも多いです。</p>
   </ul>
</section>

<section class="sect07">
<h3 class="blueblockBG">最後に</h3>
<p class="marginBottom40px">いかがでしたか？Ａ子さんは、個人宅向け担当のオペレーターなので１２時出勤でしたが、法人担当のオペレ―ターだと、９時や１０時に出勤して１８時退社のことも多いようです。また、各コールセンターの扱う商材やサービスによっても、勤務時間は変わってきますのでチェックしてみて下さいね！</p>
</section>

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター基礎知識」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail31">仕事とプライベートの両立！シフト制は、スケジュール管理の魔法の杖となるのか？</a></li>
  <li><a href="/ccwork/detail17">おしゃれを楽しみたい！服装自由のコールセンターが多いわけ</a></li>
  </dd>
  </dl>
  </aside>
  
<aside class="tabNav_ccwork">
<ul><li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_a.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_a.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li><li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li><li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li></ul>
</aside><!-- / tabNav -->
  
</section>
