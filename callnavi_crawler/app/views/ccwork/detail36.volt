<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">知っ得情報</span></div>
  <h1>地方で働きたい！<br />コールセンターが地方で増える理由とメリット</h1>
  </header>
  
<!--- 段落１ 開始 --->  
  <section class="sect01">
  <p class="marginBottom10px">コールセンターが地方に増えているらしい！その理由とは？<br />地方創生のキッカケ？！コールセンターへの誘致合戦がはじまっているらしい。同じ仕事を地方でも！切り札としてコールセンターが期待されているらしい。</p>
  <img src="/public/images/ccwork/036/main001.jpg" class="imagemargin_T10B10" alt="就活の秘訣！" />
  <p class="marginBottom30px">最近、地方でコールセンター進出の機運が高まっているそう。<br />その理由を調べてみました。</p>
  <p class="dot_yellowtext_Number">1</p>
  <p class="dot_yellowtext_title">コールセンター助成制度がアツい！</p>
  <img src="/public/images/ccwork/036/sub_001.jpg" class="imagemargin_T10B10" alt="就活の秘訣！" />
  <p class="marginBottom30px">地方自治体では、コールセンターを地方に誘致しようと様々な助成制度を実施しているんです。たとえば北海道札幌市では、「新規常用雇用者１人あたり20万円(正社員・障がい者は30万円)の交付」や、福岡県福岡市では「業務施設の年間賃借額(敷金、権利金を除く)の1/2の交付」など、多くの自治体で企業への具体的なメリットを提示しています。<br />参照：一般社団法人 日本コールセンター協会会報「各自治体のコールセンター誘致助成制度一覧」( 2014年8月発行)</p>

  <p class="dot_yellowtext_Number">2</p>
  <p class="dot_yellowtext_title">どうしてコールセンターを誘致しているの？</p>
  <img src="/public/images/ccwork/036/sub_002.jpg" class="imagemargin_T10B10" alt="就活の秘訣！" />
  <p class="marginBottom20px">それでは、コールセンターを誘致している理由を見てみましょう。</p>
    <h3 class="blueblockBG">主な理由は2つあります。</h3>
    <ul class="dot_bluetext">
    <li><span>1</span>雇用を創出したいため</li>
    <li><span>2</span>地域の活性化へ繋げたいため</li>
    </ul>
  <p class="marginBottom20px">地方の主な問題は「雇用が無い」ことに尽きます。そこで、一度に大量の人材を雇用でき、しかも設備による環境への負担もほぼ出ないコールセンターは魅力的なんです。</p>

  <p class="dot_yellowtext_Number">3</p>
  <p class="dot_yellowtext_title">コールセンターにとってのメリットは？</p>
  <img src="/public/images/ccwork/036/sub_003.jpg" class="imagemargin_T10B10" alt="就活の秘訣！" />
  <p class="marginBottom20px">コールセンターにとっても、地方に進出することでメリットがたくさんあるんです。</p>
    <ul class="dot_bluetext">
    <li><span>1</span>各自治体の助成制度</li>
    <li><span>2</span>人件費・家賃が都市部より安い</li>
    <li><span>3</span>人材が確保しやい</li>
    <li><span>4</span>災害時のリスクが分散される</li>
    </ul>
  <p class="marginBottom10px">上記の理由などで、ネットの環境があればどこでも仕事ができるIT系のベンチャー企業なども、地方へサテライトオフィスを創設しているところが増えてきました。</p>
  <p class="marginBottom40px">仕事があれば、ゆったりとした環境が望める地方で働きたい！という若者も増えている昨今。
地方×コールセンターのカタチに今後も注目です！</p>

  </section>
<!--- 段落１ 終了 --->

<aside>
  <dl>
  <dt class="headingG01">「コールセンター知っ得情報」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail18">学歴なんて関係ない！コールセンターなら、実力次第で高収入が目指せる</a></li>
  <li><a href="/ccwork/detail51">営業経験が全くないのですが、コールセンターの仕事はできますか？</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  
  <aside class="tabNav_ccwork">
  <ul>
  <li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_a.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_a.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li>
<li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li>
  </ul>
  </aside><!-- / tabNav -->
  
  
</section>

