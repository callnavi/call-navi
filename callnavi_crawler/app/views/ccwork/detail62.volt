<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">知っ得情報</span></div>
  <h1>睡魔を吹き飛ばせ！<br />眠気対策には、これを知っておけば大丈夫</h1>
  </header>
<section class="sect01">
<p class="marginBottom20px">仕事に勉強に、頑張る皆さんにとっての最大にして、永遠の敵と言えば、そう！「睡魔」です。特にデスクワークの多い方は動きも少ないため、疲れているときや寝不足の時などは気がつけば、うとうと・・・なんて経験もあるのではないでしょうか。<br />今回は、そんなみなさんの強い味方、一瞬で睡魔に打ち勝つ方法を調べました。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">睡魔を吹き飛ばす方法</h2>
  <p class="dot_yellowtext_Number">1</p>
  <p class="dot_yellowtext_title">ツボ押し</p>
<img src="/public/images/ccwork/062/main001.jpg" alt="ツボ押し"  class="imagemargin_B25"/>

<p class="marginBottom10px">眠気を吹き飛ばすと言われている有名なツボ3箇所をご紹介します！</p>
<p class="marginBottom20px">まずは、<span class="Blue_text">合谷（ごうこく）</span>と呼ばれる手の甲にあるツボです。親指と人差し指の間の骨の付け根部分から少し上に位置します。ここを反対の手の親指で、長く強めに押しましょう。<br />続いて、<span class="Blue_text">百会（ひゃくえ）</span>と呼ばれる頭のてっぺんにあるツボです。両耳と鼻の延長戦が交差するところにあり、あたまのてっぺんの少し凹んだ箇所です。ちなみに百会はほかにも、頭痛改善やストレス解消、癒しの効果もあると言われている万能ツボです。<br />最後に<span class="Blue_text">中衝（ちゅうしょう）</span>。こちらは、手の中指のツメの生え際になります。ここを強めに押していきます。だんだんと痛みがなくなってくると眠気も解消すると言われています。</p>

  <p class="dot_yellowtext_Number">2</p>
  <p class="dot_yellowtext_title">ストレッチ</p>
<img src="/public/images/ccwork/062/sub_001.jpg" alt="ストレッチ"  class="imagemargin_B25"/>
<p class="marginBottom10px">椅子に座ったままでもできる簡単なストレッチ方法をご紹介します。</p>
  <div class="freebox01_pinkBG_wrapper">
  <p class="pinkfukidasi_pinktext_dot">肩をすくめる</p>
  <p class="marginBottom10px">背骨をぴんと伸ばして椅子に腰かけます。その状態から、肩を耳に思いっきり近づけます。背筋を伸ばしたまま、一気に肩の力を抜いて元に戻します。この上げ下げストレッチを10回行いましょう。</p>
  <p class="pinkfukidasi_pinktext_dot">左右の腕を引っ張り合う</p>
  <p class="marginBottom10px">片方の腕を頭上に上げ、もう一方の手で手首をつかみ、ゆっくり引っ張ります。反対側も同じようにして、気持ち良いところで止めます。呼吸をとめないようにして。肩甲骨・肩・上腕が伸びているのを意識しましょう。</p>
  </div>

  <p class="dot_yellowtext_Number">3</p>
  <p class="dot_yellowtext_title">耳を引っ張る</p>
<img src="/public/images/ccwork/062/sub_002.jpg" alt="耳を引っ張る"  class="imagemargin_B25"/>
<p class="marginBottom10px">続いては場所を選ばないで、しかも目立たずにできる方法をご紹介します。<br />それは、耳をひっぱること！</p>
  <div class="freebox01_pinkBG_wrapper">
  <p class="marginBottom_none"><span class="Pink_textbold">■</span>まず耳を親指と人差し指でつかみます。<br />そのまま上下左右に引っ張っていきます。<br />※あまり強くやりすぎないように注意しましょう。</p>
  <p class="marginBottom_none"><span class="Pink_textbold">■</span>続いて、手で耳を「くしゃくしゃ」丸めるイメージで揉んでいきます。</p>
  <p class="marginBottom_none"><span class="Pink_textbold">■</span>最後にツボ押しです。<br />耳たぶの裏、顎と耳の付け根のあたりにある骨の凹みの部分をゆっくり押していきます。</p>
  </div>
<p class="marginBottom10px">耳には100以上のツボがあるとも言われており、引っ張っているうちに、眠気解消だけでなく他の部位もどんどん健康になっていくかもしれませんね！</p>

  <p class="dot_yellowtext_Number">4</p>
  <p class="dot_yellowtext_title">息を止める</p>
<img src="/public/images/ccwork/062/sub_003.jpg" alt="息を止める"  class="imagemargin_B25"/>
<p class="marginBottom30px sectborder">こちらも目立たずに、しかも短時間でできる方法。<br />苦しくなる、ぎりぎりまで息を止めて、限界が来たら吐きだします。人にもよりますが、45秒から50秒が目安です。命の危機を感じさせる、まさに強硬手段です。ただし、無理は禁物です。</p>

  <p class="dot_yellowtext_Number">5</p>
  <p class="dot_yellowtext_title">人知れず妄想する</p>
<img src="/public/images/ccwork/062/sub_004.jpg" alt="人知れず妄想する"  class="imagemargin_B10"/>
<p class="marginBottom30px">楽しいことを考えたり、昔の恥ずかしい思い出を思い出したり、怖いことを考えてみたり、一気に頭が活性化されて目が覚めます。というか、眠れなくなるかもしれないですね。あまり長期間やっていると仕事に手がつかなくなるのでほどほどに。</p>

  <p class="dot_yellowtext_Number">6</p>
  <p class="dot_yellowtext_title">声を出す</p>
<img src="/public/images/ccwork/062/sub_005.jpg" alt="声を出す"  class="imagemargin_B10"/>
<p class="marginBottom10px">実践するには、環境にもよりますが、声を出すことは眠気を吹き飛ばすのにとても効果的な方法です。近くに同僚がいるなら話しかけてみるのも良いですし、ドライブ中に眠くなったら歌を歌うのも効果的。コールセンターで働く人にとってはぴったりの方法ですね！眠くなったら、ひたすら電話をかける・又は受けて（架電・受電）成績や効率もアップ！？</p>

  <p class="dot_yellowtext_Number">7</p>
  <p class="dot_yellowtext_title">冷やす</p>
<img src="/public/images/ccwork/062/sub_006.jpg" alt="冷やす"  class="imagemargin_B10"/>
<p class="marginBottom10px">冷たい飲み物を飲むのも効果的です。<br />人が眠くなるのは、体温が下がるときだと言われています。したがって、冷たいものが体内に入ってくると、体温を上げようとするため、眠気も覚めてくるという原理です。</p>
<p class="marginBottom10px">いかがでしたか？どうしても眠い時は、やはり仮眠をとったり夜の睡眠を改善したりと根本的な解決方法を見つけるのが、健康にとっては一番ですが、「どうしても！」という緊急時にはぜひ試してみてください！</p>

</section>

<aside>
  <dl>
  <dt class="headingG01">「コールセンター知っ得情報」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail06">「ストレスに負けない!」コールセンターバイト</a></li>
  <li><a href="/ccwork/detail24">声で人生が変わる！？　コールセンターで好印象な声を会得する方法</a></li>
  </ul>
  </dd>
  </dl>
</aside>
  
  
  <aside class="tabNav_ccwork">
  <ul>
  <li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_a.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_a.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li>
<li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li>
  </ul>
  </aside><!-- / tabNav -->
  
  
</section>
