<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">基礎知識</span></div>
  <h1>営業経験が全くないのですが、<br />コールセンターの仕事はできますか？</h1>
  </header>
  
  <section class="sect01">
  <img src="/public/images/ccwork/051/main001.jpg" alt="営業経験が全くないのですが、
コールセンターの仕事はできますか？"  class="imagemargin_B25" />
 <p class="marginBottom10px">営業経験がないために、コールセンターへの応募を躊躇している方はいませんか？今回は、コールセンターと営業経験の必要性について、ご紹介します。</p>
</section>

<section class="sect02">
  <h2 class="sideBlueBorder_blueText02 marginBottom10px">コールセンターってどんな仕事をするの？</h2>
<p class="marginBottom20px">まずは、コールセンターでどんな仕事をするのが、軽くおさらいします。コールセンター業務には、大きく分けて、<span class="Blue_text">インバウンド（受信）</span>と<span class="Blue_text">アウトバウンド（架電）</span>があります。</p>
  <div class="freebox01_blueBG_wrapper">
  <p class="freebox01_blueBG_title01">インバウンドとは</p>
  <p>求人情報ではテレオペ（テレフォンオペレーション）と呼ばれ、電話を受ける仕事です。化粧品などの通販受注や、メーカーのカスタマーサポート、携帯電話やスマホなどのテクニカルサポートなどがあります。お客様からかかってきた電話に対して、質問に答えたり、必要な対応を行ったりする電話対応の仕事となります。</p>
  </div>
  <div class="freebox01_pinkBG_wrapper">
  <p class="freebox01_pinkBG_title01">アウトバウンドとは</p>
  <p>テレアポやテレマと呼ばれることが多く、電話をかける仕事です。（テレマは、インバウンドとアウトバウンドの両方で、行われます）新しい商品やサービスを説明して、お客様に興味をもって貰ったり、クロージングをして、実際に買ってもらったりする場合があります。</p>
  </div>
  <p class="marginBottom20px">インバウンド、アウトバウンドのどちらも、アルバイト、派遣、正社員と様々な雇用形態がありますが、<span class="Blue_text">アルバイトでの求人が圧倒的に多い</span>です。また、<span class="Blue_text">アルバイトの条件としても、シフト制、服装自由、髪型自由、駅ナカ、駅チカ、福利厚生充実、各種ボーナスありなど充実している</span>ケースが多いようです。</p>
</section>

<section class="sect03">
    <h2 class="sideBlueBorder_blueText02 marginBottom10px">コールセンターで働くのに、営業経験は必要なの？</h2>
    <p class="marginBottom20px">インバウンドなら、営業行為は行うことはほとんどないので、営業経験は必要ありません。一方アウトバウンドなら、必須ではないが、ないよりあった方がいい、といった程度のニーズであると思ってもらえればと思います。また、営業経験といっても、<span class="Blue_text">飛び込み営業するようなファイトや体力、訪問販売を続けるような精神力の強さまでは、必要ない</span>と思われます。電話営業の場合、訪問営業と比べて、対応できるお客様の数が圧倒的に多いため、その商品やサービスに興味があるかどうか、電話口から確認をしていくイメージで考えておけば良いでしょう。また、アウトバウンドであっても、内容確認やフォローコールのような確認作業だけなら、営業スキルは必要ないでしょう。</p>
</section>

<section class="sect04">
    <h2 class="sideBlueBorder_blueText02 marginBottom10px">営業の仕事ってどんなイメージ？</h2>
    <img src="/public/images/ccwork/051/sub_001.jpg" alt=""  class="marginBottom25px" />
    <p class="marginBottom10px">ちなみに、営業の仕事に対して、どんなイメージがありますか？明るくて、存在感やオーラが合って、魅力的で、何があってもへこたれなくて、マシンガントークでしゃべり続けられる人・・・・</p>
    <p class="marginBottom20px">確かにそのような営業マンもいるかも知れませんが、必ずしもしゃべりが得意である必要はありません。<span class="Blue_text">お客様のニーズを上手に聞き出す聞き上手の方が、お客さんに信頼されたり、人気があって紹介を受けたりと、営業成績がいい場合もあるようです。</span><br />ちなみに、アプローチ（クラッチ合わせ）、ニーズ調査、クロージングなど一般的なデモや商談などで使う営業スキルは、コールセンターではそんなに意識しなくても問題ないでしょう。</p>
</section>

<section class="sect05">
    <h2 class="sideBlueBorder_blueText02 marginBottom10px">コールセンターに向いている人は？</h2>
    <p class="marginBottom10px">では、コールセンターと営業経験という観点から、コールセンターにはどんな人が向いているんでしょうか？</p>
    <p class="marginBottom10px">コールセンターであってもインバウンドなら営業経験は不要と考えて良いでしょう。ここでは、アウトバウンドでテレアポなど営業電話をする場合の話となります。<br />コールセンターでは、スクリプトと呼ばれる台本のようなものが用意されていて、基本的にそれにしたがって、お客様にその商品に興味があるかどうか、確認作業を繰り返すことになります。ですから、興味のないお客様、買いたくないお客様を説得するのではなく、<span class="Blue_text">とにかく数をこなして、興味があるお客様を探し続ける継続力が必要</span>となります。</p>
    <p class="marginBottom10px">あと、こちらの話を一方的に話したり、無理な営業トークをしたりするのではなく、<span class="Blue_text">会話の途中に「ご不明な点はございませんか？」「質問はありませんか？」などなど、お客様の話を効くことも大切</span>です。<br />話し上手で、営業が得意な方の中には、話したくてもうまく話せない人の気持ちがなかなか理解できず、このようなお客様を逃してしまうことも多いと聞きます。その一方で、コミュニケーションがあまり得意ではない人は、このような方の気持ちを理解して、話ができるので、多くの方が信用して、話をしてくれる場合も多いようです。</p>
    <p class="marginBottom20px">これは、コールセンターに限った話ではないかも知れませんが、話し上手よりも、<span class="Blue_text">聞き上手の方が営業という仕事には向いているのかも</span>知れませんね。</p>
</section>

<section class="sect06">
    <h2 class="sideBlueBorder_blueText02 marginBottom10px">コールセンターでは、研修があるから安心</h2>
    <img src="/public/images/ccwork/051/sub_002.jpg" alt=""  class="marginBottom25px" />
    <p class="marginBottom10px">そうは言っても、全く営業経験のない方、対面での接客経験のない方が、いきなりテレアポやテレマをするのは、無理があります。</p>
    <p class="marginBottom10px">コールセンターでは、<span class="Blue_text">通常、研修期間が設けられていて、電話対応のマナーを含めたトークやスクリプトなど、実際に、どんな風に話をすれば良いか教えてくれます。</span><br />コミュニケーションが苦手な方でも、ロールプレイングなど実際に会話を想定した練習を重ねることにより、徐々に話ができるようになっていくでしょう。<br />外回りの営業だと、基本的に一人で訪問することが多く、不明点やトラブル、クレームなどがあった場合でも、一人で対応しなければならないこともありますが、コールセンターでは、<span class="Blue_text">協力体制を含めた環境が整っていて安心</span>です。</p>
    <p class="marginBottom20px">たとえば、経験豊富なリーダー（SV）や、上司が近くで仕事をしているので、分からないときには、いつでも質問をすることができます。また、通話の内容を録音で確認したり、責任者に聞いていてもらったりして、アドバイスを受けることもできます。<br />万が一、クレームとなってしまった案件であっても、経験豊富な上司や責任者の方が対応してくれる場合も多いので、<span class="Blue_text">まずは相談することが大切</span>です。</p>
</section>

<section class="sect07">
    <h2 class="sideBlueBorder_blueText02 marginBottom10px">コールセンター応募の前に</h2>
    <p class="marginBottom10px">営業経験がなくても、コールセンターで活躍できることはわかってもらえたと思いますが、応募の際のポイントについてまとめておきます。</p>
    <p class="marginBottom10px">コールセンターの採用試験では、履歴書や職務経歴書などで、学歴や職歴を厳しくチェックしてマイナス評価することは、ほとんどありません。それよりも、<span class="Blue_text">面接が重要</span>です。<br /><span class="Blue_text">面接では、自分の名前や志望動機などハキハキと答えられることが大切ですが、スクリプト（台本）が配られて、実際に読んでみることがあります。</span></p>
    <p class="marginBottom20px">最初は、うまく読めなくても問題ないと思いますが、注意された点など、すぐに改善していくことは、とても大切です。これは実際に仕事を行った際に、上司からの指示内容などが素早く反映できるか見ていますので、緊張するかとは思いますが、この点は頭の片隅に入れておきましょう。</p>
</section>

<section class="sect08">
    <h2 class="sideBlueBorder_blueText02 marginBottom10px">コールセンター入社後に気をつけること</h2>
    <p class="marginBottom10px">コールセンターのいいところを書いてきましたが、<span class="Blue_text">実はコールセンターの離職率は高い</span>のです。なかには、お客様対応に疲れてしまう場合もありますが、自分で目標を立てて、それに向かって進んでいくことを意識していない場合に、辞めてしまうケースが多いようです。</p>
    <p class="marginBottom20px">売上目標、時給アップ、インセンティブやボーナスなど、何でもいいですが、<span class="Blue_text">具体的な目標を立てて、それに向かって日々改善をしていく、そして、その状態を上司や同僚とともに楽しんでいける人は、コールセンターでの仕事が向いている</span>のではないでしょうか。</p>
</section>

  <section class="sect09">
    <h3 class="blueblockBG">最後に</h3>
  <p class="marginB20">コールセンターで結果を出している人を見てみると、必ずしもしゃべりが上手な人ばかりではありません。口下手で、口数が少ない人でも、成功している人は多いです。</p>
    <p class="marginBottom40px">コミュニケーションスキルに自信がない人でも、まずは応募してみてはいかがでしょうか。</p>
  </section>

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター基礎知識」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail13">NGワードとクッション言葉に気をつけよう！</a></li>
  <li><a href="/ccwork/detail23">日本語を正しく使えていますか？コールセンター業務に必要な丁寧語、謙譲語、尊敬語</a></li>
  </ul></dd>
  </dl>
  </aside>
  
<aside class="tabNav_ccwork">
<ul><li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_a.png" alt="基礎知識" class="mediaPC" height="" width="192"><img src="/public/images/ccwork/nav/tabnav_01_03_a.png" alt="基礎知識" class="mediaSP" height="" width="286"></a></li><li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" alt="知っ得情報" class="mediaPC" height="" width="192"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" alt="知っ得情報" class="mediaSP" height="" width="286"></a></li><li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" alt="働く人の声" class="mediaPC" height="" width="192"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" alt="働く人の声" class="mediaSP" height="" width="286"></a></li></ul>
</aside><!-- / tabNav -->
  
</section>

