<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">基礎知識</span></div>
  <h1>バイトを辞めるときはここに注意！</h1>
  </header>
<!--- 段落１ 開始 --->
  <section class="sect01">
  <p class="marginBottom20px">学業に専念したい、就職が決まった、ちょっと職場に合わないなど、アルバイトを辞める時はなどさまざまな理由があると思います。無用なトラブルを避けるためにも、常識に則って辞められるようにルールをしっかり確認しましょう。
</p>
  <img src="/public/images/ccwork/007/main001.jpg" alt="バイトを辞めるときはここに注意！" class="marginBottom20px" />
</section>
<!--- 段落１ 終了 --->

<!--- 段落２ 開始 --->
    <section class="sect02">
  <h2 class="sideBlueBorder_blueText02 marginBottom10px">退職を伝える時期とタイミング</h2>
  
  <div class="freebox02_wrapper">
  <img src="/public/images/ccwork/007/sub_001.jpg" width="117" height="117" alt="" />
  <p class="freebox02_smart">伝える人</p>
  <p class="freebox02_text freebox02_margintop12px">まずは<span class="Blue_text">同じ部署の上司か人事担当者</span>に伝えましょう。
  上司へ報告する前に仲間に話した結果、上司に間接的に伝わってしまった、とならないように。</p>
  </div>
  
  <div class="freebox02_wrapper">
  <img src="/public/images/ccwork/007/sub_002.jpg" width="117" height="117" alt="" />
  <p class="freebox02_smart">伝え方</p>
  <p class="freebox02_text">メール・LINEはマナー違反です。<span class="Blue_text">直接会って伝え、
  最後まで責任をもった対応</span>をしましょう。また、話を聞いてもらうタイミングは、
  仕事が終わり手が空いている時か、別に時間をもらいましょう。</p>
  </div>

  <div class="freebox02_wrapper">
  <img src="/public/images/ccwork/007/sub_003.jpg" width="117" height="117" alt="" />
  <p class="freebox02_smart">辞める時</p>
  <p class="freebox02_text  freebox02_margintop4px">多くのコールセンターではマニュアルを配っているところが
  多いと思いますので、<span class="Blue_text">資料や備品は忘れずに返却</span>しましょう。慌てないように事前に
  何を返却すべきか確認しておいたほうがいいと思います。</p>
  </div>
</section>
<!--- 段落２ 終了 --->  

<!--- 段落３ 開始 --->
    <section class="sect03">
  <h2 class="sideBlueBorder_blueText02 marginBottom20px">こんなトラブル、どうしよう？</h2>
  <img src="/public/images/ccwork/007/cont_001.jpg" width="90" height="90" alt="女性「突然明日からこなくていいと言われてしまった、、、！」" class="fukidasi02_bg" />
<div class="pinkfukidasi02">突然「明日から来なくていい」と言われてしまった…！</div>
  <p class="fukidasi02_answer"><span class="Blue_text">解雇予告手当の支払い もしくは 解雇の30日前の予告が必要</span>となるので、突然の解雇はできません。説明、解雇理由を問いましょう。
ただし、下記の条件で働いている人は例外となります。
</p>
<ul class="fukidasi02_graybox">
<li>・日雇い労働の場合</li>
<li>・2か月以内と期間を定めて雇用されている場合</li>
<li>・季節労働者で4か月以内の期間を定めて雇用されている場合</li>
<li>・試用期間中の場合</li>
</ul>
<p class="marginB20 sectborder">また、本人に重大または悪質な義務違反や
背信行為があると労働基準監督署が認める場合には、
解雇予告や解雇予告手当の義務がなくなります。</p>

  
  <img src="/public/images/ccwork/007/cont_002.jpg" width="90" height="90" alt="男性「バイトを辞めたものの、バイト代が貰えていない、、、」" class="fukidasi02_bg" />
<div class="bluefukidasi02">バイトを辞めたものの、バイト代が貰えていない…</div>
<p class="fukidasi02_answer">働いた分のバイト代はもらう権利があるので、「無断で行かなくなった」など自分勝手な辞め方をしていなければ、
  電話 または 直接会社に行き問い合わせましょう。 <span class="Blue_text">受け取り方や、受取り日時の確認</span>もお忘れなく。 </p>
    </section>
<!--- 段落３ 終了 --->

  <section class="sect08">
    <h3 class="blueblockBG">最後に</h3>
  <p class="marginBottom40px">ストレスに負けないコツを紹介しましたがいかがでしたか？
  癖や思考を急に変えるのは難しいことですが、改善できそうなことから少しずつ実行してみましょう。</p>
  </section>

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター基礎知識」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail03">全国調査！コールセンターの時給比較</a></li>
  <li><a href="/ccwork/detail04">コールセンターの雇用形態</a></li>
  <li><a href="/ccwork/detail13">NGワードとクッション言葉に気をつけよう！</a></li>
  <li><a href="/ccwork/detail11">これは必須！コールセンターの専門用語！！</a></li>
  </dd>
  </dl>
  </aside>
  
  
<aside class="tabNav_ccwork">
<ul><li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_a.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_a.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li><li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li><li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li></ul>
</aside><!-- / tabNav -->
  
  
  
</section>

