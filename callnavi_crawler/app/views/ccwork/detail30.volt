<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">知っ得情報</span></div>
  <h1>説明上手になれる?!その秘訣は･･･</h1>
  </header>
  
  <section class="sect01">
  <p class="marginBottom5px">仕事をする時に人に説明する場面って多いですよね。「明日、プレゼンだけど…上司にうまく自分の意見で説得できない…。」「お客様の受電対応で、冷静に対応できない。」など自分ではうまく話せていると思っていても、意外と言い返されて説得できずに困っていませんか？克服するためのポイントを、いくつかご紹介します。
  <img src="/public/images/ccwork/030/main001.jpg" alt="説明上手になれる？！その秘訣は･･･"  class="imagemargin_T10B10" />
</p>

</section>
  
  <section class="sect03">
  <h2 class="sideBlueBorder_blueText02 marginBottom20px">説明下手を克服する4つのポイント</h2>
  
  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/030/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />ポイント</p>
  <p class="yellowmiddlecircle_titlenumber">1</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">結論から述べる</p>
  </div>  
  <p class="marginBottom40px sectborder1">説明をした後、相手に「だから何？」と返されてしまった経験はないでしょうか。<span class="Blue_text">結論から話すことで相手が理解しやすくなり、その先の話に相手を引き込むことができます。</span>ただし、結論だけ話しても伝わりませんよね。その後の説明はダラダラ長くせずに、<span class="Blue_text">あらすじとポイント</span>を伝える程度がいいでしょう。</p>
 
  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/030/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />ポイント</p>
  <p class="yellowmiddlecircle_titlenumber">2</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">箇条書きにする</p>
  </div>

  <div class="imageBlockA02 sectborder1 marginBottom30px">
<p class="image"><img src="/public/images/ccwork/030/sub_001.jpg" alt="箇条書きにする" /></p>
<div class="contentsInner1">
<p>書面を見ながら話せる状況であれば、箇条書きにすることで自分の頭の中を整理しながら話すことができます。なぜ、箇条書きがいいのでしょうか。それは、伝えたい内容がたくさんあって、頭が混乱したとき、箇条書きにすると読みやすいからです。1つ1つの項目が視覚的にはっきりし、効率的に情報を伝達することができるのが箇条書きの利点です。</p>
</div>
</div>
 
  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/030/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />ポイント</p>
  <p class="yellowmiddlecircle_titlenumber">3</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">「〜です。」「〜ます。」で言い切る</p>
  </div>  
  <p class="marginBottom20px">語尾を曖昧にしてしまうと、相手を不安にさせてしまいます。</p>
 <img src="/public/images/ccwork/030/sub_002.jpg" alt="お客様「もう少し安くならないの？」" class="marginBottom0" />
 <img src="/public/images/ccwork/030/sub_003.jpg" alt="×オペレーター「これ以上は･･･お安くならないと思うのですが･･･」" class="marginBottom10px" />
   <p class="marginBottom10px">これでは「それだったらいいや」と言われてしまったり、話が前に進みません。</p>
 <img src="/public/images/ccwork/030/sub_004.jpg" alt="◯オペレーター「お客様に1番おトクなプランとなっております」" class="marginBottom0" />
  <p class="marginBottom40px sectborder1">このように、<span class="Blue_text">語尾を言い切ることで相手を不安にさせず、説得のプラスな言葉になります。</span></p>

  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/030/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />ポイント</p>
  <p class="yellowmiddlecircle_titlenumber">4</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">ゆっくり落ち着いて話す</p>
  </div>  
  <p class="marginBottom20px">早口で話すと、相手も考える時間がなくなり、理解されにくくなります。ゆっくり話すと冷静に話ができるとおもわれるため、知的なイメージをもたれます。また、ゆっくり話すことで聞き流しを防ぎ、相手は落ち着いて聞くことができる上、自分も理解しながら話すことができます。</p>
  </section>

  <section class="sect08">
    <h3 class="blueblockBG">最後に</h3>
  <p class="marginBottom40px">今回は「説明ベタを克服する方法」をご紹介しました。いかがでしたか？すぐに克服するのは難しいですが、<span class="textA01">1番大事なことは「相手を思いやること」</span>です。説明がうまくなることで、プレゼンやお客様の対応など、スムーズに行えるようになります。また、うまく説明ができるようになると、<span class="Blue_text">自然と自分に自信が持てるようになります。</span>焦らずゆっくり、克服していきましょう。
</p>
  </section>

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター知っ得情報」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail32">どうするクレーム？しっかり対応するための魔法の４ステップ！</a></li>
  <li><a href="/ccwork/detail24">声で人生が変わる！？コールセンターで好印象な声を会得する方法</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
<aside class="tabNav_ccwork">
<ul><li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li><li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_a.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_a.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li><li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li></ul>
</aside><!-- / tabNav -->
  
</section>

