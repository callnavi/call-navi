<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">知っ得情報</span></div>
  <h1>ライフスタイルに合った働き方を見つけよう！</h1>
  </header>
  
  <section class="sect01 freebox04_border">
    <img src="/public/images/ccwork/008/main001.jpg" alt="ママさんオペレーター" />
    <div class="pinkbox01">
    <p class="pinkbox01_leftwhitetext_title"><span>ここが</span>オススメ</p>
    <ul>
    <li>シフトが自由</li>
    <li>急な休みも取りやすい</li>
    <li>座ったまま動ける</li>
    </ul>
    </div>
     <p class="marginBottom10px">「子供が学校から帰ってくるまでに帰りたい…」
  「家事もあるので毎日は無理…」コールセンターのお仕事はこういった
  ママさんたちの強い味方！午前中のみの勤務や、
  週２～３日という風に<span class="pinkbox01_textpink">自分のペースで働くことができる</span>から、
  家事に育児に忙しいママさんたちがたくさん働いています。</p>
  </section>
  
  <section class="sect02 freebox04_border">
    <img src="/public/images/ccwork/008/main002.jpg" alt="大学生オペレーター" />
    <div class="bluebox01">
    <p class="bluebox01_leftwhitetext_title"><span>ここが</span>オススメ</p>
    <ul>
    <li>スキルが身につく</li>
    <li>友達と一緒に働ける</li>
    <li>色んな人と知り合える</li>
    </ul>
    </div>
     <p class="marginBottom10px">授業にサークルに忙しいけど、
     遊ぶお金だって欲しい！コールセンターはそんな大学生にもってこいのアルバイト。
     また、コールセンターでたくさんのお客様と対話することで会話力が鍛えられ、
     <span class="bluebox01_textblue">就職活動のときにとても役立ちます。</span>
     特にアウトバウンドの経験があると、
     営業職へのアピールにもなるのでオススメです！</p>
  </section>
  
  <section class="sect03 freebox04_border">
    <img src="/public/images/ccwork/008/main003.jpg" alt="Wワーカーオペレーター" />
    <div class="orangebox01">
    <p class="orangebox01_leftwhitetext_title"><span>ここが</span>オススメ</p>
    <ul>
    <li>単位時間で稼げる</li>
    <li>余った時間に働ける</li>
    <li>声の印象で勝負できる</li>
    </ul>
    </div>
     <p class="marginBottom10px">コールセンターではWワークOKの
     ところがほとんど。<span class="orangebox01_textorange">平均的に時給が高く、
     短時間で稼げる</span>ので、他の仕事と掛け持ちをしながら働いている人も多いです。
     金銭面だけが理由ではなく、残業規制や週休二日制によって余った時間を
     有効的に使いたい、メインの仕事と違う業界で働き充実感を得たい、なんて人も。
     就職活動のときにとても役立ちます。特にアウトバウンドの経験があると、
     営業職へのアピールにもなるのでオススメです！</p>
  </section>

  <section class="sect04 freebox04_border_none">
    <img src="/public/images/ccwork/008/main004.jpg" alt="フリーターオペレーター" />
    <div class="greenbox01">
    <p class="greenbox01_leftwhitetext_title"><span>ここが</span>オススメ</p>
    <ul>
    <li>頑張った分手当がつく</li>
    <li>正社員登用が多い</li>
    <li>人前に出なくて済む</li>
    </ul>
    </div>
     <p class="marginBottom10px">コールセンターは時給とは別に頑張った分だけ
     手当がもらえるインセンティブ制度有りの所が多いので、月に数十万以上稼ぐ方も
     います。また、<span class="greenbox01_textgreen">シフトの融通が利き、人前に出ずに
     できるお仕事</span>だからミュージシャンや俳優・声優を目指している方が多く働いています。
     もしかしたら未来のスターと一緒に働けるかも？</p>
  </section>

  <section class="sect08">
    <h3 class="blueblockBG">最後に</h3>
  <p class="marginBottom40px">コールセンターには「これが正しい！」
  という働き方はありません。自分のライフスタイルに合わせて楽しく
  長く働ける会社を見つけましょう！</p>
  </section>

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター知っ得情報」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail01">コールセンターってどんな仕事？</a></li>
  <li><a href="/ccwork/detail11">これは必須！コールセンターの専門用語!!</a></li>
  <li><a href="/ccwork/detail07">バイトを辞めるときはここに注意！</a></li>
  <li><a href="/ccwork/detail04">コールセンターの雇用形態</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  
<aside class="tabNav_ccwork">
<ul><li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li><li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_a.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_a.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li><li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li></ul>
</aside><!-- / tabNav -->
    
</section>

