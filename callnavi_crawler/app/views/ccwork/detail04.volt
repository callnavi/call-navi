<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">基礎知識</span></div>
  <h1>コールセンターの雇用形態</h1>
  </header>

<!--- 段落１ 開始 --->
  <section class="sect01">
  <p class="marginBottom20px">コールセンターで働く際の雇用形態は、正社員やアルバイトなどさまざまです。それぞれメリット・デメリットをしっかり確認し、自分にあった働き方を見つけましょう。
</p>
  <div>
<table border="1" cellpadding="5" cellspacing="0" class="blue_listbox">
<tr>
<th></th>
<td class="blue">メリット</td>
<td class="blue">デメリット</td>
</tr>
<tr>
<th>
<p class="pcnonetext">正社員</p>
<img src="/public/images/ccwork/003/001.png" alt="女性正社員" />
</th>
<td class="ok">
<p class="bluetitlepcnone">メリット</p>
  <ul>
  <li>雇用が安定している(雇用期間が無期限)</li>
  <li>社会保険や福利厚生が適用される</li>
  <li>ボーナスの支給あり</li>
  </ul>
</td>
<td class="triangle">
<p class="bluetitlepcnone">デメリット</p>
  <ul>
    <li>勤務時間が定められている</li>
    <li>場合によっては、残業や転勤の必要がある</li>
  </ul>

</td>
</tr>

<tr>
<th>
<p class="pcnonetext">派遣社員</p>
<img src="/public/images/ccwork/003/002.png" alt="女性派遣社員" />
</th>
<td class="ok">
<p class="bluetitlepcnone">メリット</p>
  <ul>
    <li>派遣先を選ぶことで、自由な勤務時間で働くことができる</li>
    <li>派遣先を変えない限り転勤なし</li>
    <li>有給休暇を取得しやすい</li>
  </ul>
</td>
<td class="triangle">
<p class="bluetitlepcnone">デメリット</p>
  <ul>
    <li>派遣されている期間のみの雇用のため、安定性がない</li>
    <li>ボーナスの支給なし</li>
  </ul>

</td>
</tr>

<tr>
<th>
<p class="pcnonetext">契約社員</p>
<img src="/public/images/ccwork/003/003.png" alt="女性契約社員" />
</th>
<td class="ok">
<p class="bluetitlepcnone">メリット</p>
  <ul>
    <li>正社員と同等の社会保険や福利厚生が適用される</li>
    <li>正社員よりも有給休暇が取得しやすい</li>
  </ul>
</td>
<td class="triangle">
<p class="bluetitlepcnone">デメリット</p>
  <ul>
    <li>勤務時間が定められている
</li>
    <li>雇用期間が契約によって定められている（継続の場合もある）</li>
  </ul>

</td>
</tr>

<tr>
<th>
<p class="pcnonetext">アルバイト</p>
<img src="/public/images/ccwork/003/004.png" alt="男性アルバイト" />
</th>
<td class="ok">
<p class="bluetitlepcnone">メリット</p>
  <ul>
    <li>働いた分だけ、給与に反映される</li>
    <li>勤務時間を自由に選べる</li>
    <li>転勤なし</li>
  </ul>
</td>
<td class="triangle">
<p class="bluetitlepcnone">デメリット</p>
  <ul>
    <li>派遣先を選ぶことで、自由な勤務時間で働くことができる</li>
    <li>派遣先を変えない限り転勤なし</li>
    <li>有給休暇を取得しやすい</li>
  </ul>

</td>
</tr>
</table>

  <p class="marginBottom_none">あくまで一般的な雇用形態の特徴になりますので、詳しい給与や福利厚生の有無などは必ず勤務先の状況をご確認ください。
無理な勤務や待遇への不満は、自身にも勤務先にもいいことがありません。
自分のライフスタイルにあった雇用形態で働きましょう。

</p>
</div>
  </section>
<!--- 段落１ 終了 --->

<!--- 段落２ 開始 --->
  <section class="sect02">
  <h2 class="sideBlueBorder_blueText02">コールセンターの組織</h2>
  <p class="marginBottom_none">コールセンターによって最適な組織体制・役職・呼び名は違いますが、一般的には以下のような組織体制でマネジメント業務を分担しているところが多いです。
「スーパーバイザー」の下に十数人のオペレーター、という形が多いですが、大人数のコールセンターの場合には、スーパーバイザーの下に「リーダー」をつけます。
また、「トレーナー」（人材教育担当）を専門におくコールセンターも増えてきています。</p>
  <img src="/public/images/ccwork/003/main001.png" alt="組織図" />
  <p class="marginBottom10px">コールセンターの業務は、<span class="textA01">お客様からの電話を受ける「インバウンド（受電業務）」</span>と、<span class="textA01">お客様へ電話をかける「アウトバウンド（発信業務）」</span>の二つに大きく分かれます。<br>
    電話の受け答えが中心となるため、オフィスワークが初めての方や接客業務が好きな方に人気！また、シフト制・残業なしの会社が多いので仕事を掛け持ちしたい人や、自分の時間を確保したい人ががたくさん働いています。</p>
    
    <h3 class="blueblockBG">オペレーションスタッフ</h3>
    <p class="blueblockBG_title">●センターマネージャー<span>コールセンターの総責任者</span></p>
    <p>コールセンターの経営の責任/ 人材育成</p>
    <p class="blueblockBG_title">●マネージャー<span>コールセンターの管理責任者</span></p>
    <p>スタッフ全員の指導・評価 / 経営目標の達成 / コールセンターの効率化
    <p class="blueblockBG_title">●リーダー<span>スーパーバイザーの補助担当</span></p>
    <p>新人教育 / オペレーターの管理</p>
    <p class="blueblockBG_title">●オペレーター<span>電話応対担当</span></p>
    <p>受発信業務</p>

    <h3 class="orangeblockBG">マネジメントスタッフ</h3>
    <p class="orangeblockBG_title">●トレーナー<span>オペレーターの管理・教育</span></p>
    <p>オペレーターの管理 / 受発信の戦略、戦術を開発、運用</p>
    <p class="orangeblockBG_title">●クオリティコントロール<span>品質管理</span></p>
    <p>案内・トーク内容の品質向上・改善</p>

    
    <h3 class="pinkblockBG">サポートスタッフ</h3>
    <p class="pinkblockBG_title">●バックヤード<span>現場のサポート</span></p>
    <p>入力業務などのデータ作業全般など</p>
    <p class="pinkblockBG_title">●コーディネーター<span>人材募集など</span></p>
    <p>就業に関わる契約 / 人材募集</p>

  </section>
<!--- 段落２ 終了 --->

<aside>
<dl>
<dt class="headingG01">「コールセンター基礎知識」オススメのページ</dt>
<dd>
<ul class="linkListA01">
<li><a href="/ccwork/detail01">コールセンターってどんな仕事？</a></li>
<li><a href="/ccwork/detail03">全国調査！コールセンターの時給比較</a></li>
</ul>
</dd>
</dl>
</aside>


<aside class="tabNav_ccwork">
<ul><li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_a.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_a.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li><li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li><li><!--<a href="/ccwork#tabContents03">--><span><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></span><!--</a>--></li></ul>
</aside><!-- / tabNav -->


</section>
