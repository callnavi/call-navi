<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">基礎知識</span></div>
  <h1>「ファミコン敬語」使っていませんか？<br />以外と知らない？！正しい「ビジネス敬語」</h1>
  </header>
  
  <section class="sect01">
  <p class="marginBottom20px">社会人になって敬語ができている！と思っても、意外とビジネス敬語ではNG言葉を使っているかもしれないですよ。
      <span class="Blue_text">お客様を不快にさせる「バイト敬語」ではなく、正しい「ビジネス敬語」をご紹介</span>します。
</p>
  <img src="/public/images/ccwork/028/main001.jpg" alt="「ストレスに負けない！」コールセンターバイト"  class="marginBottom25px" />
      <p class="marginBottom10px">「バイト敬語」は別名ファミリーレストランやコンビニエンスストアの頭文字を取って、「ファミコン敬語」とも呼ばれるそうです。そもそも、<span class="Blue_text">ファミコン敬語とは？お客様をとりあえず不快にさせない、一見丁寧な印象のバイト敬語のこと</span>です。</p>

</section>
  
  <section class="sect02">
  <h2 class="sideBlueBorder_blueText02 marginBottom10px">◯◯のほう</h2>
  <div class="freebox05_circle01">
  </div>
  <div class="freebox05_circle_titlebox">
  <p class="freebox05_circle_titletext">お見積もり<span class="Blue_text">のほう</span>を作成いたしました。</p>
  </div>  
  <div class="freebox05_circle02">
  </div>
  <div class="freebox05_circle_titlebox">
  <p class="freebox05_circle_titletext">お見積もり<span class="Pink_text">を</span>作成いたしました。</p>
  </div>  
  <p class="marginBottom40px sectborder1">「◯◯のほう」というのは、<span class="Blue_text">方向を示すときに使う言葉</span>です。お見積もりは方向を表す言葉ではないため、「◯◯のほう」という言葉を使うのは適切ではありません。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">◯◯を頂戴する</h2>
  <div class="freebox05_circle01">
  </div>
  <div class="freebox05_circle_titlebox">
  <p class="freebox05_circle_titletext">お名前を<span class="Blue_text">を頂戴しても</span>よろしいですか？</p>
  </div>  
  <div class="freebox05_circle02">
  </div>
  <div class="freebox05_circle_titlebox">
  <p class="freebox05_circle_titletext">お名前<span class="Pink_text">をお教えいただけます</span>でしょうか。</p>
  </div>  
  <p class="marginBottom40px sectborder1"><span class="Blue_text">「お名前」は頂戴できるものはない</span>ため「お名前を頂戴する」「お名前をいただく」という表現は正しくありません。「お電話番号」や「ご住所」も同様です。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">◯◯でよろしかったでしょうか？</h2>
  <div class="freebox05_circle01">
  </div>
  <div class="freebox05_circle_titlebox">
  <p class="freebox05_circle_titletext">以上で<span class="Blue_text">よろしかった</span>でしょうか？</p>
  </div>  
  <div class="freebox05_circle02">
  </div>
  <div class="freebox05_circle_titlebox">
  <p class="freebox05_circle_titletext">以上で<span class="Pink_text">よろしい</span>でしょうか。</p>
  </div>  
  <p class="marginBottom40px sectborder1">現在進行形であることを過去形で伝えると、相手の言ったことを疑うような印象を与えてしまいます。そのため、<span class="Blue_text">過去形を使わないのが正しい敬語</span>です。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">◯◯の形になる</h2>
  <div class="freebox05_circle01">
  </div>
  <div class="freebox05_circle_titlebox">
  <p class="freebox05_circle_titletext">10分ほど、お待ち<span class="Blue_text">いただく形</span>になります。</p>
  </div>  
  <div class="freebox05_circle02">
  </div>
  <div class="freebox05_circle_titlebox">
  <p class="freebox05_circle_titletext">10分ほど、お待ち<span class="Pink_text">いただきます。</span>よろしいでしょうか。</p>
  </div>  
  <p class="marginBottom40px sectborder1">本来<span class="Blue_text">「形」は形状や外形をさす言葉</span>です。形のないものに「◯◯の形」を使わずに、シンプルな言い回しにしたほうが良い印象になります。また「大変申し訳ございませんが」など、謝罪の言葉を「お待ちいただきます」の前につけると、柔らかい表現になります。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">◯◯と◯◯どちらにいたしますか？</h2>
  <div class="freebox05_circle01">
  </div>
  <div class="freebox05_circle_titlebox">
  <p class="freebox05_circle_titletext">コーヒーと紅茶どちらに<span class="Blue_text">いたしますか？</span></p>
  </div>  
  <div class="freebox05_circle02">
  </div>
  <div class="freebox05_circle_titlebox">
  <p class="freebox05_circle_titletext">コーヒーと紅茶どちらに<span class="Pink_text">なさいますか？</span></p>
  </div>  
  <p class="marginBottom20px"><span class="Blue_text">「いたす」は謙譲語で、お客様に使うには失礼な言葉遣い</span>になります。<span class="Blue_text">お客様にたずねる場合は尊敬語を使うのが適切</span>なため、「する」の尊敬語の「なさる」になります。電話対応や来客の際にも使うことが多い言葉遣いなので、正しい敬語で覚えましょう。</p>

  </section>
  

  <section class="sect08">
    <h3 class="blueblockBG">最後に</h3>
  <p class="marginBottom40px">正しいビジネス敬語について、ご紹介しましたがいかがでしたか？普段、会社などで無意識に正しいと思って使っていた敬語が、実は誤った敬語でお客様や上司に失礼な言葉遣いをしていた方もいるのではないでしょうか。大事な場面で失敗をしないよう、正しい敬語を覚えていきましょう。</p>
  </section>

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター基礎知識」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail13">NGワードとクッション言葉に気をつけよう！</a></li>
  <li><a href="/ccwork/detail23">日本語を正しく使えていますか？コールセンター業務に必要な丁寧語、謙譲語、尊敬語</a></li>
  </dd>
  </dl>
  </aside>
  
<aside class="tabNav_ccwork">
<ul><li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_a.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_a.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li><li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li><li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li></ul>
</aside><!-- / tabNav -->
  
</section>


