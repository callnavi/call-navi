<section class="entryA01 ccwork_entry">
<header>
<div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">働く人の声</span></div>
<h1>教育スタッフに聞いた、<br />新人がやりがちなミス5選</h1>
</header>
  
<section class="sect01">
<p class="marginBottom10px">どんな仕事でも始めはみんな新人。職場でもわからないことだらけですし、不安も多いですよね。コールセンターも例外ではありません。初めての電話がけ、お問い合わせ対応、ご案内、情報の管理・・・。それに、お客様相手のお仕事。新人とは言え、なるべくトラブルは起こしたくないものです。初めてなので、小さなミスは避けられないのは当然ですが、事前にどんなことに注意したら良いのか知っておくだけでも心強いですよね！<br />そこで今回は、<span class="Blue_text">新人が陥りがちなミス</span>について、とあるコールセンターの教育専門スタッフにについて聞いてみました。</p>
<img src="/public/images/ccwork/069/main001.jpg" class="imagemargin_T10B10" alt="新人が陥りがちなミス" />

<h2 class="sideBlueBorder_blueText02 marginBottom10px">これさえおさえれば大丈夫！<br />新人がやりがちなミス5選</h2>
<p class="dot_yellowtext_Number">1</p>
<p class="dot_yellowtext_title">お客様の話を聞けてない</p>
<p class="marginBottom10px">コールセンターでは必ずと言って良いほどトークマニュアルやスクリプトがあるのですが、新人に多いのは、お客様と電話でお話しする際にこのトークのマニュアルを読むのに必死で、肝心のお客様の話を聞けていないということ！</p>
<p class="marginBottom20px sectborder1">これは、トークを読み慣れていない事や、トークのポイントが掴めていないために起こります。<br />ここを改善するためには、<span class="Blue_text">トークの内容を暗記すること！</span>つまり、暗記するくらい読み込んでトークのポイントを理解することが第一歩です。<br /><span class="Blue_text">きちんと相槌を打つことも非常に大事</span>ですね。お客様に「聞く姿勢」を見せるようにしましょう！</p>

<p class="dot_yellowtext_Number">2</p>
<p class="dot_yellowtext_title">知識を一生懸命入れようと、頭でっかちになる</p>
<img src="/public/images/ccwork/069/sub_001.jpg" class="imagemargin_T10B10" alt="知識を一生懸命入れようと、頭でっかちになる" />
<p class="marginBottom20px sectborder1">わからない質問が来たらどうしよう・・・と不安になることから、知識をたくさん詰め込もうとする人もいますが、これもやりがちなミス。<br /><span class="Blue_text">入社時に教わる基本的な知識だけしっかり覚えればOK</span>です！<br />知識が1番だというのは間違った考えなんです。<span class="Blue_text">大事なのは知識の多さではなくトークのスキル</span>。話し方の方が大事だということを理解してくださいね！<br />あなただったら、頭がいい人と、話していて楽しい人とどっちと話したいですか？</p>

<p class="dot_yellowtext_Number">3</p>
<p class="dot_yellowtext_title">恐怖で電話一つ一つに、時間がかかる</p>
<p class="marginBottom20px sectborder1">初めてのアルバイトなら誰だって通る道ですね。レジ打ちでも、レストランのサービスでも、初めは失敗するのが怖くて、なかなか自分から積極的になれない気持ちはよ～くわかります！特にコールセンターの場合は電話越しの1対1・・・。間違ったことを伝えてしまったらどうしよう・・・って不安で、余計な事を考え過ぎる前に、まずはとにかくコール（架電・電話をかけること）してみましょう！ポイントは<span class="Blue_text">勇気を出すこと</span>です！！あとは慣れですので、あまり考えすぎないようにしましょう。コールセンターの良いところは1日に多くのお客さんとお話できることです。<br />場数を踏めるから短期間に成長できるという強みもあるんですよ！</p>

<p class="dot_yellowtext_Number">4</p>
<p class="dot_yellowtext_title">話し方に癖がある</p>
<p class="marginBottom20px sectborder1">音読感が強かったり、話し方がたどたどしかったり、逆に張り切りすぎてワザとらしい話し方になってしまったりするのも、新人がよくやってしまうミスのひとつです。<br />単純に、トークマニュアルを読み慣れないことも一因ですが、<span class="Blue_text">自分の話し方を客観的に聞いた時どのような印象に感じるか、自覚がないことが原因</span>です。<br />一度自分の声を録音して聞いてみると良いでしょう！「えっ！こんな話し方だったんだ！」という発見もありますし、自分の喋り方の癖などを把握することができます。</p>

<p class="dot_yellowtext_Number">5</p>
<p class="dot_yellowtext_title">スムーズな返答ができない</p>
<p class="marginBottom20px sectborder1">なんと返答すればいいか慣れておらず、スムーズな返答ができないのも新人にありがちですよね。お客様の質問に対する返答の模範例がすぐに出てこないためです。こちらも回数をこなすうちに慣れてくるのでよく来る質問事項についてはメモしたり、すぐにわかるように、<span class="Blue_text">トークマニュアルの返答模範例に目印</span>をつけたりしましょう！<br /><span class="Blue_text">近くにいる先輩の話し方を真似してみる</span>のも良いですね！</p>

<img src="/public/images/ccwork/069/sub_002.jpg" class="imagemargin_T10B10" alt="知識を一生懸命入れようと、頭でっかちになる" />
<p class="marginBottom40px">いかがでしたか。最初からは無理でも、心得ておくだけで安心ですよね。<br />最初はみんな失敗するものです。勇気 持って電話をかけましょう！</p>
</section>

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター働く人の声」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail67">裏技教えます！本当は教えたくない受注が取れるコツ</a></li>
  <li><a href="/ccwork/detail58">コールセンターバイトで、就職活動を有利に！電話対応、コミュ力、ビジネスマナーは最大の武器！</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  <aside class="tabNav_ccwork">
  <ul id="js-tab01">
  <li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_a.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_a.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li>
  </ul>
  </aside><!-- / tabNav -->
  
  
</section>
