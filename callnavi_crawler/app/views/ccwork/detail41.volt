<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">働く人の声</span></div>
  <h1>まさかの逆効果！？<br />甘いものの罠。それリフレッシュになっていません。</h1>
  </header>
  
<!--- 段落１ 開始 --->  
  <section class="sect01">
  <p class="marginBottom10px">甘いもの大好きな女子Ｙ子です。<br />仕事中、どうしても甘いものに手が伸びてしまうのは、私だけではないと思います。</p>
    <img src="/public/images/ccwork/041/main001.jpg" class="imagemargin_T10B10" alt="ひょっとしたら、あの人･･･！？コールセンターに近い将来の有名人が集まる理由" />
  <p class="marginBottom10px">ちょっと小腹がすいたＡＭ11：00、昼ごはんにはまだ早いし、なんか甘いものでも食べておこっと♪集中力が切れてきたから少しお休み、ＰＭ3：30、よく働いたなぁ！気分転換におかし食べよっと♪や、やばい！〆切なのに間に合わない！ＰＭ7：30、栄養補給しなきゃ！甘いもの食べておこう！</p>
  <p class="marginBottom10px">というような具合に、私は最低１日に３度は甘いものを口にしています。</p>
  <p class="marginBottom10px">疲れた時や、ストレスが溜まった時、無性に甘いものが欲しくなりますよね。しかし、実は<span class="Blue_text">リフレッシュのつもりで食べていた甘いものが、逆にストレスの原因</span>になっていたことをご存知でしたか？もしかしたら、あなたは砂糖依存症かもしれません！</p>
  <p class="marginBottom10px">甘いものに含まれる<span class="Blue_text">砂糖は吸収が非常に早く、血糖値を急上昇させます。</span></p>
  <p class="marginBottom10px">血糖値が急上昇すると、一旦は非常に元気になったような気がするのです。<br />栄養ドリンクを飲むと、元気になるものこの現象の場合が多いとか。</p>
  <p class="marginBottom10px">急上昇した血糖値と言うのは急降下も速くなり、そうするとまたすぐに甘いものを欲してしまいます。</p>
  <p class="marginBottom10px">その結果、甘いものを口にし続けないとテンションを保てない、という悪循環にはまっていってしまうのです。</p>
  <p class="marginBottom30px">気分転換に間食をしていたつもりが、逆にやる気を奪われ、振り回されてしまう羽目に…。「やる気が起きない」と訴える人の多くは、主食の食べ過ぎ、甘いもののとりすぎ、日常的に清涼飲料水や砂糖入りの飲み物を飲んでいると、糖質過多の傾向がみられます。</p>

<div class="imageBlockA02 marginBottom30px">
<p class="image"><img src="/public/images/ccwork/041/sub_001.jpg" alt="箇条書きにする" /></p>
<div class="contentsInner1">
<p class="Blue_Btextbold">◯ 砂糖依存症チェック！</p>
  <p class="marginBottom10px">甘いものがないと落ち着かないという人の大半は、「もともと甘いものが好きだから」「仕事が忙しいせいだ」と考えがちで、自覚症状がないことが多いのも特徴です。以下のチェック項目であなたが砂糖依存症かどうか調べてみましょう！ </p>
</div>
</div>

<ul class="fukidasi02_graybox">
<li>・甘いものを食べないと満足できない？</li>
<li>・甘いものをしばらく食べていないとぼんやりしてしまう</li>
<li>・お腹は空いていないのに、何かしら食べたくなる</li>
<li>・一口だけのつもりでつまんだお菓子が、止まらなくなってしまう</li>
<li>・ 年々甘いものを食べる量が増えてきている</li>
<li>・甘いものを食べるとすっきりする</li>
<li>・甘いものを控えようと思っていても、つい食べてしまう</li>
</ul>

  <p class="marginBottom20px">わ、わたしほぼすべて当てはまっています…！どうすれば改善できるのでしょうか？改善方法を一緒に探ってみましょう！</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">ご飯やお肉はしっかり食べていますか？</h2>
    <img src="/public/images/ccwork/041/sub_002.jpg" class="imagemargin_T10B10" alt="ひょっとしたら、あの人･･･！？コールセンターに近い将来の有名人が集まる理由" />
  <p class="marginBottom10px">実は<span class="Blue_text">甘いものが欲しくなる１番の原因は、栄養不足</span>だからだそうです。</p>
  <p class="marginBottom10px">ダイエット中の方は、早く結果をだそう！と頑張って、ご飯やお肉を避けていたりしませんか？その結果、甘いものへの欲求を強めてしまっている可能性があります。</p>
  <p class="marginBottom10px">お肉などのたんぱく質を食べないと、体が糖度の高いものを欲しがってしまう仕組みになっているそう。</p>
  <p class="marginBottom10px">なので「ご飯」と「お肉」をしっかり食べることで、あなたの甘いものへの欲求を少し減らすことができるようになるかもしれません。</p>
  <p class="marginBottom10px">中でも<span class="Blue_text">玄米や豚肉をしっかり取ることがおススメ</span>です！ビタミンＢ群やミネラルが豊富に含まれるこれらの食品を摂取すると、甘いものの過剰摂取で不足した栄養素を補給し、脳が砂糖を求める悪循環を防ぎます。</p>
  <p class="marginBottom20px">ご飯を「玄米」に変えるだけで、ダイエット効果も期待できますし、砂糖中毒から抜け出すきっかけにもなり、一石二鳥ですね！</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">どうしても甘いものが欲しくなったら、<br />果物やお芋を食べましょう</h2>
    <img src="/public/images/ccwork/041/sub_003.jpg" class="imagemargin_T10B10" alt="ひょっとしたら、あの人･･･！？コールセンターに近い将来の有名人が集まる理由" />
  <p class="marginBottom20px"><span class="Blue_text">果物には酵素やビタミン、ミネラルが豊富に含まれています。</span>砂糖のように中毒性もないので、食べすぎてしまう心配もありません。また、果物には甘さ以外にも、渋味や苦味、酸味などがあり、これらの味覚を味わうことで、自然とお菓子などの人口的な甘さを欲しなくなっていきます。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">10 秒ダッシュ＆息止め</h2>
    <img src="/public/images/ccwork/041/sub_004.jpg" class="imagemargin_T10B10" alt="ひょっとしたら、あの人･･･！？コールセンターに近い将来の有名人が集まる理由" />
  <p class="marginBottom10px">どうしてもどうしても、甘いものが欲しい！我慢ができない！<br />というときは、</p>
  <p class="marginBottom10px"><span class="Blue_text">その場から10秒間×20回のダッシュをしてみましょう！もしくは息を止める<br />（欲求が収まるまで何度でも）</span></p>
  <p class="marginBottom20px">これをすると、脳内のアドレナリンが分泌され、血糖値を上昇させてくれるそうです。つまり、アドレナリンを出して、脳を勘違いさせ、食欲を紛らわせる方法です！</p>

  <div class="freebox01_blueBG_wrapper marginBottom40px">
  <p class="freebox01_blueBG_title01">「老化」を進行させる「糖化」</p>
  <p>糖化とは、体の中に糖が蓄積された状態のことです。この糖化が進むと、肌や髪、骨などの老化が進んでしまうのです。肌の弾力が失われ、たるみ、くすみが発生したり…。砂糖のとり過ぎは、いつまでも美しくいたい女子には大敵です！</p>
  </div>


</section>
<!--- 段落１ 終了 ---> 
  

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター働く人の声」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail32">どうするクレーム？しっかり対応するための魔法の４ステップ！</a></li>
  <li><a href="/ccwork/detail42">ガムを噛め！仕事中のリフレッシュは、これに限る！</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  
  <aside class="tabNav_ccwork">
  <ul id="js-tab01">
  <li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_a.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_a.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li>
  </ul>
  </aside><!-- / tabNav -->
  
  
</section>

