<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">基礎知識</span></div>
  <h1>集中力を大発揮！あなたのジンクスはなに！？</h1>
  </header>
  
<!--- 段落１ 開始 --->  
  <section class="sect01">
      <img src="/public/images/ccwork/047/main001.jpg" class="imagemargin_B25" alt="あなたは自分の集中力に自信がありますか？" />
 <p class="marginBottom10px"><span class="Blue_Btextbold">あなたは自分の集中力に自信がありますか？</span></p>
 <p class="marginBottom10px">就業中、集中力をずっと維持するのは、なかなか至難の業ですよね。さっきまで、ものすごい集中力でバリバリ仕事をしていたのに、急に集中できなくなってしまった…なんてこともあると思います。</p>
 <p class="marginBottom10px">コールセンターのお仕事は、想像以上に集中力を必要とします。声だけが頼りなので、聞き間違いをしないように、お客様の話に全神経を集中させる必要があります。また、お客対応中に集中力が途切れてしまって「あ、今聞いてなかった！」なんてことが頻繁に起これば、お客様を不快な思いにさせてしまうだけではなく、コールセンターの評判も悪くなってしまうでしょう。</p>
 <p class="marginBottom10px">そんなことがないように、集中力を高めて、お客様満足の高い対応を心掛けていきたいですね！</p>
</section>

  <section class="sect02">
  <h2 class="sideBlueBorder_blueText02 marginBottom10px">仕事の効率を上げる！集中力を維持する方法</h2>
  <h3 class="blueblockBG">ノルアドレナリンを活性化</h3>

  
  <p class="marginBottom10px">集中するために必要不可欠な神経伝達物質<span class="Blue_text">「ノルアドレナリン」</span><br />この物質は追い詰められた時に発揮され、<span class="Blue_text">集中力を高める働きがあります。</span><br />いわゆる、火事場の馬鹿力ってやつですね！</p>
  <p class="marginBottom20px">このノルアドレナリンの働きを高める簡単な方法があります！</p>

  <p class="dot_yellowtext_Number">1</p>
  <p class="dot_yellowtext_title">自分で作業に締め切りをつける。</p>
  <p class="marginB20">これを<span class="Blue_text">「デッドライン効果」</span>といいます。「少し焦る」「ちょっとギリギリ」程度の、頑張れば達成できそうな、<span class="Blue_text">ほどよい緊張とプレッシャーがノルアドレナリンの活性化に効果的</span>だとか。</p>
  <p class="marginBottom20px">私がコールセンターで電話営業（テレアポ）の仕事をしていた時、月末は従業員の集中力が一段と高まるのを常に感じていました。「月末締め」という大きな締め切りがありましたからね（笑）。このデッドライン効果を利用して、「今日は何時までに何人に電話するぞ！」など、小さい締め切りを設けていけば、常に集中力を維持することができそうです！</p>

  <p class="dot_yellowtext_Number">2</p>
  <p class="dot_yellowtext_title">集中の合間に必ず休憩を入れる。</p>
  <p class="marginB20">適度に休憩を取ることで、次のノルアドレナリンがきちんと働くための準備ができるそうです。様々な説がありますが、人間の集中力は30分が限度といいますよね。<span class="Blue_text">30分毎にちょこちょこ息抜きを挟んで、長時間いい状態で集中力が続くように工夫しましょう。</span></p>

  <p class="dot_yellowtext_Number">3</p>
  <p class="dot_yellowtext_title">難しい作業と比較的簡単な作業は交互にする</p>
  <p class="marginB20">こうすることでノルアドレナリンをムダにせず、分泌させすぎないようにすることができます。<span class="Blue_text">ノルアドレナリンの分泌が多すぎると、興奮状態になり、怒りっぽくなったりします。</span>適度に簡単な仕事も織り交ぜ、興奮しすぎて疲れた…ということのないように配慮しましょう。</p>

    <h3 class="blueblockBG">「作業興奮」の作用を活かす</h3>
  <p class="marginBottom10px">嫌々取り組んだ仕事でも、<span class="Blue_text">いったんやり始めると楽しくなってきて、スピードが速くなったり、正確さが増したりする現象を「作業興奮」</span>というそうです。</p>
  <p class="marginBottom10px">「こんなにはかどるなら、もっと早く取り掛かればよかった～」と思うこと、ありますよね。<br />「作業興奮」は脳の「側坐核」に働く<span class="Blue_text">ドーパミン</span>が関与しています。「側坐核」は、いわゆる私たちの<span class="Blue_text">「やる気スイッチ」</span>です。しかしこのやる気スイッチは、ある程度の刺激がないと活性化しないので、作業の後半～終盤が集中力のピークになります。</p>
  <p class="marginBottom20px">やる気がなかなか出ない場合でも、とりあえず前向きに取り組んでみましょう。<br />そうすれば、徐々にあなたのやる気スイッチが作動してくるはずです！</p>

    <h3 class="blueblockBG">「作業興奮後の疲労」を感じたら、あっさり休みましょう</h3>
  <p class="marginBottom20px">「作業に疲れた・飽きた」と感じたら集中力が切れてしまい、パフォーマンス力も低下します。その時は、トイレに行ったり、ご飯休みにしたり、同僚に話かけてみたり、<span class="Blue_text">脳の活動を休ませることが大切</span>です。早め早めに休ませておいた方が、脳の回復が早くなるそうです。</p>

    <h3 class="blueblockBG">集中力のピーク時を知ろう</h3>
        <img src="/public/images/ccwork/047/sub_001.jpg" class="imagemargin_T10B10" alt="集中力のピーク時を知ろう" />
  <p class="marginBottom10px">集中を発揮できるタイミングは人間の体温の上昇と密な関係があります。</p>
  <p class="marginBottom10px">目が覚めてから2～3時間経過した午前中が体温も上昇し、覚醒レベルが高くなります。また午後3時から夕方にかけても同様に覚醒レベルが上がります。</p>
  <p class="marginBottom10px">な、なるほど！私も以前より、午前中と夕方に仕事がはかどるな～と思っていたのですが、体温の上昇と関係があったのですね！</p>
  <p class="marginBottom20px">コールセンターの場合、集中力が必要な案件対応などは、自分の集中力が上がっている時間を見計らって、お客様に電話をしてみてもいいかもしれませんね。</p>

    <h3 class="blueblockBG">話すことで脳に刺激を与える</h3>
        <img src="/public/images/ccwork/047/sub_002.jpg" class="imagemargin_T10B10" alt="話すことで脳に刺激を与える" />
  <p class="marginBottom20px">集中力が途切れたと感じたら、同僚や先輩との短いおしゃべりすることを推奨します！え！？それで集中力が高まるの？と不思議に思うかもしれませんが、<span class="Blue_text">短い談話によって気持ちの切り替えと、脳のリフレッシュをすることができます。</span>そうすることで、また集中力や注意力がアップするのです。お客様とお話しするのとは一味違う、リラックスした会話を楽しんで下さいね。そこに「笑い」の要素もあれば、更に集中力を高めることができます！</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">アスリートから学ぶ！高い集中直発揮の秘訣</h2>
  <p class="marginBottom10px">困難な状態やスランプに陥っても、次々とそんな状態を打破していく、アスリート達。そんなアスリート達のこだわりから、集中力を高めるヒントを探っていきたいと思います。</p>

    <h3 class="blueblockBG">バッターボックスでの動作がお馴染み、イチロー</h3>
        <img src="/public/images/ccwork/047/sub_003.jpg" class="imagemargin_T10B10" alt="バッターボックスでの動作がお馴染み、イチロー" />
  <p class="marginBottom10px">ご存知の方も多いと思いますが、イチローはバッターボックスに立つさいに、毎回行う儀式のようなものがあります。まずバッターボックスに入る前に足の屈伸運動をします。そしてバッターボックス内では、バットをまっすぐに立て、ピッチャーに向け、そしてユニフォームの右肩を左手でまくってバットを構えます。</p>
  <p class="marginBottom10px">イチローはこの一連の動作をすることにより、集中力を高めているのです。<span class="Blue_text">「この動作をする＝最大限に集中するとき」ということが潜在知識にプログラムされています。</span></p>
  <p class="marginBottom10px">この方法、仕事にも適用できますよね！</p>
<div class="imageBlockA01 marginBottom30px">
<p class="image"><img src="/public/images/ccwork/047/sub_004.jpg" width="250px" alt="箇条書きにする" /></p>
<div class="contentsInner">
<p>例えば、朝イチバンにお客様対応を始める前に、１杯熱いコーヒーを飲む、手のストレッチをしてから電話対応に入る、などあなたの好きな動作を習慣化するのも効果が期待できそうです。その動作を行うことで、脳が集中モードへと切り替わるように、潜在意識に組み込んでいきましょう。</p>
</div>
</div>

    <h3 class="blueblockBG">左足からオフィスに入る！？浅田真央</h3>
        <img src="/public/images/ccwork/047/sub_005.jpg" class="imagemargin_T10B10" alt="左足からオフィスに入る！？浅田真央" />
  <p class="marginBottom10px">フィギアスケートの浅田真央選手は、非常にジンクスを大切にしているアスリートの１人。例えば、左足に強いこだわりを持っていて、スケート靴も必ず左足から履くそうです。更に試合のリンクにも左足から入ることを決めているのだとか。ジンクスを守ることで集中力を高めて、いい演技ができるよう備えているのですね。</p>
  <p class="marginBottom20px sectborder">こちらも気軽に日常の生活に取り入れることができそうですね。例えば、オフィスには必ず左足から入室するとか、デスク周りを掃除すると集中力が高まる！など、自分のオリジナルのジンクスを作ってみるのもおもしろそうですね！</p>
</section>

  <section class="sect08">
  <p class="marginBottom40px">ちなみに私は資格の勉強をしているさいに、シャワーを浴びると集中力がパワーアップする！というジンクスを作りました！<br />またコールセンターで働いていた際は、トイレの洗面所で手首まで水にぬらし、集中力を高める！という儀式を休憩時に行っていました！<br />ぜひあなただけの「儀式」を作って、集中力マックスでお仕事に臨んでくださいね！</p>
  </section>

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター基礎知識」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail13">NGワードとクッション言葉に気をつけよう！</a></li>
  <li><a href="/ccwork/detail23">日本語を正しく使えていますか？コールセンター業務に必要な丁寧語、謙譲語、尊敬語</a></li>
  </ul></dd>
  </dl>
  </aside>
  
<aside class="tabNav_ccwork">
<ul><li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_a.png" alt="基礎知識" class="mediaPC" height="" width="192"><img src="/public/images/ccwork/nav/tabnav_01_03_a.png" alt="基礎知識" class="mediaSP" height="" width="286"></a></li><li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" alt="知っ得情報" class="mediaPC" height="" width="192"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" alt="知っ得情報" class="mediaSP" height="" width="286"></a></li><li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" alt="働く人の声" class="mediaPC" height="" width="192"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" alt="働く人の声" class="mediaSP" height="" width="286"></a></li></ul>
</aside><!-- / tabNav -->
  
</section>
