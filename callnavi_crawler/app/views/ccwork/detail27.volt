<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">知っ得情報</span></div>
  <h1>コールセンターの年齢事情</h1>
  </header>
  
  <section class="sect01">
  <p class="marginBottom20px">応募をするにあたり、年齢不問と記載があっても、応募の多い年代やどれくらいの年齢層が働いているのか気になりませんか？今回はコールセンター全般の年齢層を応募者・就業者別に紹介します。
  <img src="/public/images/ccwork/027/main001.jpg" alt="インカム画像"  class="imagemargin_T10B10" />
</p>
</section>
  
  <section class="sect02">
  <h2 class="sideBlueBorder_blueText02 marginB20">応募者の年齢事情</h2>
  <img src="/public/images/ccwork/027/sub_001.jpg" alt="グラフ「応募者の年齢事情」"  class="imagemargin_T10B10" />
  <p class="marginBottom30px">20代が一番多いですが、そこまで大きな差があるわけではなく、20歳以下の学生から年配者まで幅広く応募していることがわかりますね。しかし、定年年齢を上限に募集している求人もあるので応募要項をしっかり確認してから応募するようにしましょう。<br />
※<span class="Blue_text">はたらいく 2014～2015職種別平均年収・月収100職種徹底調査</span>による。</p>
  <h2 class="sideBlueBorder_blueText02 marginB20">就業者の年齢事情</h2>
  <img src="/public/images/ccwork/027/sub_002.jpg" alt="グラフ「就業者の年齢事情」"  class="imagemargin_T10B10" />
  <p class="marginBottom20px">応募者とは違い、僅差で30代が一番多いですが、他の事務系職種と比べると年齢の高い方も多く活躍されており、就業者でも、幅広い年代が活躍しているのがわかりますね。また、「コールセンターは女性の職場」というイメージを持たれやすく女性も働きやすい職種ですが、10代は男性の割合が多く、最近では男性の割合が多くなっているコールセンターもあります。<br />
※<span class="Blue_text">コールセンターの課題改善アンケート調査</span>による。</p>
   </section>
  
  <section class="sect08">
    <h3 class="blueblockBG">最後に</h3>
  <p class="marginBottom40px">コールセンターのお仕事は、24時間で対応しているコールセンターもあり、<span class="Blue_text">「好きな時間に働ける」「未経験からでも働きやすい」</span>などの理由から、全般的に幅広い年齢層の人が応募も就業もしているようです。ただ、扱う商材 などによって年代が偏っているところもあるので応募要項を確認し、自分のアピールポイントを活かして自分の働ける環境・働きたい環境でコールセンターの仕事に携わって行ってくださいね。
</p>
  </section>

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター知っ得情報」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail18">学歴なんて関係ない！コールセンターなら、実力次第で高収入が目指せる</a></li>
  <li><a href="/ccwork/detail08">ライフスタイルに合った働き方を見つけよう！</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  
<aside class="tabNav_ccwork">
<ul><li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li><li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_a.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_a.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li><li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li></ul>
</aside><!-- / tabNav -->
  
  
</section>

