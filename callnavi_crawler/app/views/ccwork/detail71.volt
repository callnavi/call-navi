<section class="entryA01 ccwork_entry">
<header>
<div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">働く人の声</span></div>
<h1>実録インタビュー！<br />実際にコールセンターで働いた印象</h1>
</header>
  
<section class="sect01">
<p class="marginBottom20px">お給料、仕事内容、人間関係・・・働いていると不満や悩みはつきものです。もっと早く帰れると思ったのに・もっとお給料が高ければ頑張れる～！なんて思ったことありませんか？逆に、営業ってきついと思ったけどいろんな人とコミュニケーションとるのって楽しい！とか、苦手だと思っていたPC作業って結構面白い、事務向いているかも！など、思っていたよりもずっと楽しいとプラスの印象が変わることもありますよね！</p>
<p class="marginBottom10px">そこで今回は、<span class="Blue_text">仕事を始めてから変わったコールセンターの印象</span>をまとめました。
</p>

<h2 class="sideBlueBorder_blueText02 marginBottom10px">開放感のあるオフィス</h2>
<img src="/public/images/ccwork/071/main001.jpg" class="imagemargin_T10B30" alt="実録インタビュー！実際にコールセンターで働いた印象" />
<p class="marginBottom20px">狭いブースで一人黙々と電話をかけるというイメージはもう古い！？</p>
<div class="freebox01_blueBG_wrapper"><p class="marginBottom10px"><span class="Blue_textbold">●</span>ずっと、せまい室内でブースにこもって黙々と電話しているという印象だったのですが、オフィスはい意外と広く、明るい照明などで閉鎖感は感じないです。<br /><span class="text_small">（アポインター／20歳／女性）</span></p>
<p class="marginBottom10px"><span class="Blue_textbold">●</span>休憩できるエリアや、研修やミーティングスペースなどがあり、一日中自席でじっとしているということはないです。休憩スペースはソファやテレビ、自動販売機などもあってかなりリフレッシュできますね！<br /><span class="text_small">（アポインター31歳／女性）</span></p></div>

<h2 class="sideBlueBorder_blueText02 marginBottom10px">立ってコールも！？意外とアグレッシブ</h2>
<img src="/public/images/ccwork/071/sub_001.jpg" class="imagemargin_T10B30" alt="立ってコールも！？意外とアグレッシブ" />
<p class="marginBottom20px">PCとにらめっこしながら、リストにひたすら電話。座って大人しく電話していないと怒られる・・・そんなイメージでしたが。</p>
<div class="freebox01_orangeBG_wrapper"><p class="marginBottom10px"><span class="orange_textbold">●</span>うちの会社では立って電話しています。現場に覇気（はき）が無いとスーパーバイザーから立って電話するように指示が出たので驚きました(笑)<br /><span class="text_small">（アポインター／20歳／男性）</span></p>
<p class="marginBottom10px"><span class="orange_textbold">●</span>現場の志気を高めていくとアポインターのやる気につながり、結果も伴ってくるので、活気のある現場づくりは大事です。現場をなごませたり、笑いをとったりしながら明るく楽しい現場づくりを心掛けていますね！<br /><span class="text_small">（マネージャー／31歳／男性）</span></p><p class="marginBottom10px"><span class="orange_textbold">●</span>入社前は取れるのが当たり前だと思っていたのでそんなに難しいとは思っていなかったのですが、実際たくさん電話をかけても最後まで話を聞いてくれる人はわずか、その中でも受注が取れるのほんの一握りなので大変ですが、その代わりに取れたときは上司もチームもものすごく喜んでくれました。一気にやる気が出てきましたね！<br /><span class="text_small">（スーパーバイザー／24歳／男性）</span></p></div>

<h2 class="sideBlueBorder_blueText02 marginBottom10px">仲間と一緒に頑張れる！</h2>
<p class="marginBottom20px">コールセンターは作業的な仕事だという認識を持って始める方も多いと思うのですが、そこは<span class="Blue_text">対、人間</span>。やっぱりオフィス内でのコミュニケーションや同僚との情報交換も大切になってきます。</p>
<div class="freebox01_blueBG_wrapper"><p class="marginBottom10px"><span class="Blue_textbold">●</span>一人で作業的に仕事をしているだけで、周りとのコミュニケーションが少ないのかな、と思ったのですが、オフィスでは部署やチームごとに分かれており、上司や仲間と定期的にコミュニケーションや情報交換をしながら取り組めるので楽しいです！
<br /><span class="text_small">（アポインター／30歳／女性）</span></p>
<p class="marginBottom10px"><span class="Blue_textbold">●</span>活気があり、同僚や、上司との連携、会話も多く、『仕事をしているなぁ』という感じがしますね。個人プレーで黙々とやっている印象が覆されました！<br /><span class="text_small">（スーパーバイザー／27歳／男性）</span></p>
<p class="marginBottom10px"><span class="Blue_textbold">●</span>意外とイベント豊富で面白いですね！月間や週間で表彰式などのイベントがあってみんなで、目標達成の喜びを分かち合ったり、成績優秀だと、ご褒美の食事会があったりして、同僚や社員さんとも仲良くなれますし楽しいです。<br /><span class="text_small">（アポインター／20歳／男性）</span></p></div>

<h2 class="sideBlueBorder_blueText02 marginBottom10px">意外にも熱中！勝負気質が試される。</h2>
<img src="/public/images/ccwork/071/sub_002.jpg" class="imagemargin_T10B30" alt="意外にも熱中！勝負気質が試される。" />
<p class="marginBottom20px">スキルよりも大事なものは、モチベーションだった。</p>
<div class="freebox01_orangeBG_wrapper"><p class="marginBottom10px"><span class="orange_textbold">●</span>長時間電話するなんて大変だな、と思っていたのですが、他のアルバイトに比べ   時間がたつのがあっという間です！受注が取れなかったりすると、悔しくて、スーパーバイザーに指導してもらいながら何度も何度も練習しました。コツをつかんで取れてくると面白いですね！だんだんゲーム感覚で楽しめるようになってきました！<br /><span class="text_small">（アポインター／22歳／女性）</span></p>
<p class="marginBottom10px"><span class="orange_textbold">●</span>入社前までは、ノルマ達成が目標で、結構辛いのかな、という印象。でも、テレマーケティングは、やればやっただけ結果がついてくる仕事だということがわかりました。なので、頑張って結果がついてきたときに自信がつきましたし、何事にも、初めから自分はやろうとしてないだけで、やろうとすれば勝てるんだ、という強い信念が身に付いたと思います！<br /><span class="text_small">（スーパーバイザー／25歳／男性）</span></p><p class="marginBottom10px"><span class="orange_textbold">●</span>数字であらわされる営業成績は勝ち負けを明確にするので、思っていたより負けず嫌いな自分に気づきました！<br /><span class="text_small">（アポインター／19歳／女性）</span></p></div>

<h2 class="sideBlueBorder_blueText02 marginBottom10px">話す仕事、でも磨けたのは・・・</h2>
<p class="marginBottom20px">基本は電話をかけて話すお仕事。コミュニケーションが磨かれると言われますが、スキルは話す方だけではないようです。</p>
<div class="freebox01_blueBG_wrapper"><p class="marginBottom10px"><span class="blue_textbold">●</span>顔の見えないお客さまとお話をするので、ヒアリング力は相当磨かれました！<br /><span class="text_small">（アポインター／22歳／男性）</span></p>
<p class="marginBottom10px"><span class="blue_textbold">●</span>人の話をしっかり聞けるようになりました。本当のコミュニケーション能力だと思います。<br /><span class="text_small">（アポインター／30歳／女性）</span></p><p class="marginBottom10px"><span class="blue_textbold">●</span>プライベートでも、これまでは、話を聞いているつもりでも、実は相手が何を言いたいのか、ちゃんと集中して聞いていなかったのかな、と気づきました。今では相手の話をしっかり聞いてちゃんと理解できるようになったと思います。<br /><span class="text_small">（スーパーバイザー／23歳／男性）</span></p></div>

<h2 class="sideBlueBorder_blueText02 marginBottom10px">意外とここが大変・・・。</h2>
<img src="/public/images/ccwork/071/sub_003.jpg" class="imagemargin_T10B30" alt="電話は対面よりも体力を使わない？でも顔が見えない分難しいことも・・。" />
<p class="marginBottom20px">電話は対面よりも体力を使わない？でも顔が見えない分難しいことも・・。</p>
<div class="freebox01_orangeBG_wrapper marginBottom40px"><p class="marginBottom10px"><span class="orange_textbold">●</span>お客様のことを思ってやったことがうまく伝わらず、不快な気持ちにさせてしまったことがありました。<br /><span class="text_small">（アポインター／23歳／男性）</span></p>
<p class="marginBottom10px"><span class="orange_textbold">●</span>顔が見えない分、お客様に信頼してもらえるまでに時間がかかったりします。『同じような電話が何回も来る、どうせ何かの勧誘でしょ？』と言った感じで。最初の頃は、主旨まで伝えられずに切られてしまうことも多々ありました・・・。<br /><span class="text_small">（アポインター／25歳／女性）</span></p><p class="marginBottom10px"><span class="orange_textbold">●</span>責任者に昇格させていただいたのですが、部下の指導や勤務退出の管理、報告書の作成などをやっていると時間がかかり、勤務時間が長くなったのは大変です・・・。
<br /><span class="text_small">（スーパーバイザー／25歳／女性）</span></p></div>

<img src="/public/images/ccwork/071/sub_004.jpg" class="imagemargin_T25B25 sectborder2" alt="電話は対面よりも体力を使わない？でも顔が見えない分難しいことも・・。" />
<p class="marginBottom40px">入社前で多かったのが、一人黙々と電話をかけているだけというのがコールセンターの印象。でもこうして聞いてみると、<span class="blue_text">活気のある職場で閉塞感が無く驚いた</span>という意見が多かったです。<br />企業にもよりますが、オフィス環境も、上司・部下のコミュニケーションも、アポインターさんのモチベーションを大切にした取り組みがなされているところが多いようですね！</p>
</section>

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター働く人の声」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail40">ミュージシャンは声で稼げ！コールセンターで、バンドマンが重宝されるワケ</a></li>
  <li><a href="/ccwork/detail58">コールセンターバイトで、就職活動を有利に！電話対応、コミュ力、ビジネスマナーは最大の武器！</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  <aside class="tabNav_ccwork">
  <ul id="js-tab01">
  <li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_a.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_a.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li>
  </ul>
  </aside><!-- / tabNav -->
  
  
</section>
