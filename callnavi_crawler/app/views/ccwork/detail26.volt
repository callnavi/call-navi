<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">基礎情報</span></div>
  <h1>初めての方必見！<br />コールセンター応募、面接、採用、入社初日までの流れ</h1>
  </header>
  
  <section class="sect01">
    <img src="/public/images/ccwork/026/main001.jpg" class="marginBottom10px" alt="コールセンター応募、面接、採用、入社初日までの流れ" />
<p class="marginBottom10px">コールセンターに履歴書を送ってから、入社・研修までの流れはどうなっているの？今回は、こちらについて、をお届けします。入社までの一連の流れをおさえておけば、気持ちに余裕を持って応募・面接に臨めますよね！</p><p class="marginBottom_none">各コールセンターによって、選考の手順に若干の違いはありますが、こちらでは入社・研修までのモデルケースをお伝えします。採用形態が「アルバイト」もしくは「正社員」かによっても、少々異なってきますので要チェックです★</p>

  </section>
  
  <section class="sect02">
  <h2 class="sideBlueBorder_blueText02">アルバイト</h2>
  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />ステップ</p>
  <p class="yellowmiddlecircle_titlenumber">1</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">履歴書送付orインターネット上の<br />求人媒体から応募</p>
  </div>  
  <p class="marginBottom10px">最近はインターネット上の求人媒体を通しての求人が多くなっているので、ボタン１つで応募が完了することもあります。インターネット求人の場合、志望動機を書かなくても応募できることもあるかと思いますが、この段階から審査は始まっています！面倒くさがらず、志望動機も書いておきましょう。</p>
  <p class="marginBottom20px"><a href="/ccwork/detail29" class="imgHover">⇒「履歴書、志望動機の書き方」はこちら！</a></p>
</section>

<section class="sect03">
  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />ステップ</p>
  <p class="yellowmiddlecircle_titlenumber">2</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">いざ面接へ！</p>
  </div>  
  <p class="marginBottom10px">書類合格の通知が届いたら、面接の日時を相談し、さぁドキドキの面接です！コールセンターによっては、面接でスクリプト（お客様対応の台本のようなもの）を、実際に、読んでみることもあります。コールセンター業務が未経験だと、どのように読めば良いのかわからず、不安になりますよね。<br />でも大丈夫！ここでは、朗読やコミュニケーションスキルなどの技術を見られている訳ではありません。面接官が見ているのは「元気の良さ」や「素直さ」など、あなた自身の素質です。</p><p class="marginBottom10px">コールセンターは、声だけで情報や感情を伝えるお仕事なので、「元気があるか」「全体的に感じがいいか」という点が、非常に大切になってきます。加えてスクリプトの読み方で、面接官から指摘された項目をすぐに修正できるかなど、「素直さ」や「臨機応変さ」も見られていると思っておきましょう。とにかく、元気でハツラツと、面接に臨んでくださいね！</p>

  <p class="marginBottom20px"><a href="/ccwork/detail05" class="imgHover">⇒「コールセンターの面接で失敗しないコツ」はこちら！</a></p>
</section>

  <section class="sect04">
  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />ステップ</p>
  <p class="yellowmiddlecircle_titlenumber">3</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">合格</p>
  </div>  
  <p class="marginBottom20px">合格おめでとうございます！<br />合格通知が届いたら、その後は入社手続きの日程を決めます。</p>
  </section>

  <section class="sect05">
  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />ステップ</p>
  <p class="yellowmiddlecircle_titlenumber">4</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">入社手続き</p>
  </div>  
  <p class="marginBottom20px">入社手続きでは、会社の説明に加え、就業規則や必要書類の書き方について説明があります。指示に従って、記入や必要箇所への押印を済ませましょう。また、時給、給料日、シフトなど疑問点や不明点があれば、このときに確認しておきます。</p>
  </section>

  <section class="sect06">
  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />ステップ</p>
  <p class="yellowmiddlecircle_titlenumber">5</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">入社・研修</p>
  </div>  
  <p class="marginBottom20px">ついに入社の日を迎えましたね！おめでとうございます！お客様対応を始める前に、まずは研修を受けます。各コールセンターにもよりますが、研修時間は１日～２日程度で、４時間～８時間位が多いようです。</p>

    <h3 class="blueblockBG">主な研修内容<span>（一例です。各コールセンターによって内容は異なります）</span></h3>
    <ul class="dot_bluetext">
    <li><span>1</span>扱う商材やサービスの知識研修</li>
    <li><span>2</span>電話マナー研修</li>
    <li><span>3</span>クレーム対応研修</li>
    <li><span>4</span>スクリプト読み合わせ</li>
    <li><span>5</span>実践練習</li>
    <li><span>6</span>先輩の電話対応聴話</li>
    <li><span>7</span>設備システムの使い方</li>
   </ul>
  <p class="marginBottom_none sectborder1">これらは各コールセンターで業務するにあたり、必要な知識やスキルを習得する研修ですので、しっかりと学んでから、お客様対応をスタートすることとなります。実際にお客様対応を開始した後でも、最初のうちは、スーパーバイザー（ＳＶ）がフォローをしてくれるので安心です。万が一、何かあった場合でも、あなたとお客様の話を聞きながら適切な指示をくれますので、これに従いましょう。</p>
  </section>

  <section class="sect07">
  <h2 class="sideBlueBorder_blueText02">正社員</h2>
  <p class="marginBottom20px">正社員の場合も、基本は上記のアルバイト採用の流れと同じことが多いようです。しかし正社員の場合、将来的には一般社員やアルバイトをまとめる責任者に昇格し、活躍してもらいたい！という会社側の思いがあるので、面接回数が１から２回多くなり、選考が厳しくなる傾向にあります。<br />また１次面接で筆記テストがあったり、最終選考では社長自ら面接官を務めている会社もあったりと、アルバイトの選考よりもハードルが上がります。正社員だと、ＳＶへの昇格が期待されるほか、ボーナス支給や保険加入の制度もあるので、アルバイトよりも選考が厳しくなるのは当然ですよね！</p>
      <h3 class="blueblockBG">最後に</h3>
<p class="marginBottom10px">いかがでしたでしょうか？
コールセンターに入社するまでのイメージはつきましたか？
正社員雇用で「応募から入社まで最短２週間！」としている会社もありますので、「早くバリバリ働きたい！」と考えている方には嬉しい限りですね！
早速、あなたの条件に合うコールセンターを探してみてはどうでしょうか？</p>
<p class="marginBottom40px">＜補足＞<br />上記の内容は、あくまでも一例となります。実際の流れは、各コールセンターによって異なってきますので、そちらにご確認くださいますよう、お願いいたします。</p>

  </section>

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター基礎知識」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail04">コールセンターの雇用形態</a></li>
  <li><a href="/ccwork/detail31">仕事とプライベートの両立！シフト制は、スケジュール管理の魔法の杖となるのか？</a></li>
  </dd>
  </dl>
  </aside>
  
<aside class="tabNav_ccwork">
<ul><li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_a.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_a.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li><li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li><li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li></ul>
</aside><!-- / tabNav -->
  
</section>
