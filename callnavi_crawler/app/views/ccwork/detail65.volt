<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">知っ得情報</span></div>
  <h1>副業のススメ！コールセンターで副業するときには、ここに気をつけよう！</h1>
  </header>
<section class="sect01">
<img src="/public/images/ccwork/065/main001.jpg" alt="髪型自由は、本当に自由なの？髪型自由だとこんなメリットがある"  class="imagemargin_B25"/>
<p class="marginBottom_none">コールセンターで働いている人の中には、本業が別にあって、副業としてオペレーターをやっている人が結構いるって知っていました！？</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom20px">なぜコールセンターで副業するの？</h2>
  <p class="marginBottom_none">コールセンターに限らず、副業を行う理由として考えられるのは、</p>
<div class="freebox03_grayBG">
  <p class="dot_bluecircle_text">今の給料に満足できない・足らない（給料が安い、上がらない）</p>
  <p class="dot_bluecircle_text">今の仕事が面白くない</p>
  <p class="dot_bluecircle_text">他の仕事を疑似体験したい</p>
</div>
  <p class="marginBottom10px">があります。</p>
  <p class="marginBottom10px">まとまったお金が必要だけど、今の仕事だけでは無理だから。家族の入院費が必要な場合や、借金を返済する場合などが考えられます。今、正社員として勤めている会社からの給料では足らず、不景気等の影響により将来的にも給料が上がる期待が持てないと、副業に走ってしまうかも知れませんね。</p>
  <p class="marginBottom20px">今の仕事がつまらないといった理由もあります。仕事に面白みがなくなると、違う仕事を体験したくなる場合もあります。本当は、しゃべる仕事、人とコミュニケーションをとる仕事をしたかったのに、事務職をしている。そんな方は、いきなり営業職に転職するよりも、コールセンターのテレアポバイトで、営業の疑似体験をするのがいいかも知れません。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom20px">副業をするときに気をつけること</h2>
  <p class="marginBottom10px">まずは、<span class="Blue_text">大前提として自己責任で行動する</span>こと。<br />正社員で働いている人は、他の会社の社員や、派遣社員になることはできません。アルバイトとして働くことになります。<span class="Blue_text">会社の就業規則に副業禁止が記載されていないかも確認しておきましょう。</span></p>
  <p class="marginBottom10px"><span class="Blue_text">勤務地も重要なポイント</span>です。交通に便利で、就業後などに通勤しやすい場所でありながら、会社の方とは顔を合わせない場所がいいでしょう。シフトも大切です。プロジェクトの終盤など残業が発生しやすい時期は避けるなど、掛け持ちでのダブルブッキングとならないよう、気をつけましょう。</p>
  <p class="marginBottom20px"><span class="Blue_text">副業時の面接の際の志望動機も大切</span>です。家族が病気で入院費用が必要とか、今の仕事では身につけにくいスキルを身につけたいなど、単なる冷やかしではなく、本当に働きたい理由をしっかりと伝えましょう。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom20px">コールセンターで働くメリット</h2>
<img src="/public/images/ccwork/065/sub_001.jpg" alt="コールセンターで働くメリット"  class="imagemargin_B25"/>
  <p class="marginBottom10px">コールセンターで働くと、<span class="Blue_text">コミュニケーション力が身につきます。</span>電話でのお客様対応となるので、ビジネスマナーも学べます。テレアポなら、営業の疑似体験、テレオペなら専門知識など様々なスキルが身につくこともポイントです。<br />事務職や専門職など、普段は特定のひととしか関わらない仕事の方がコールセンターで働くと、今の仕事とは、まったく違った体験ができるでしょう。</p>
  <p class="marginBottom10px">これにともない、人間関係も変わってくるので、今までの生活環境では、まったく知りあうことができなかった新しい友人ができるかも知れません。ひょっとしたら、将来の有名人がいるかもしれないですよ。</p>
  <p class="marginBottom20px">あとは、声や喉の大切さに気がつく人もいます。今まで当たり前のように思っていた声や喉が、こんなにも大切で、普段からの健康管理の大切さを再認識する人もいます。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom20px">コールセンターで働くデメリット</h2>
<img src="/public/images/ccwork/065/sub_002.jpg" alt="コールセンターで働くデメリット"  class="imagemargin_B25"/>
  <p class="marginBottom20px">コールセンターに限った話ではありませんが、副業するということは、単純に考えて、普通の人の倍働くわけですから、体力的にも、精神的にも疲れます。スケジュール調整も大変になり、時間のやりくりを工夫する必要もあるでしょう。<br />収入が増えることによって、確定申告など税金対策が必要となります。<span class="Blue_text">そして、これが一番のデメリットになるかと思いますが、会社にばれたときのリスクです。</span>会社側が事情を理解して、受け入れてくれればいいですが、そうでなければ、厳重注意、減給、降格、左遷、ひょっとしたら懲戒解雇もあり得るかも知れません。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom20px">副業っていけないものなの？</h2>
  <p class="marginBottom10px">そもそも副業っていけないものなのでしょうか？　倫理的、法律的には、会社の就業規則や法律によるところがありますが、今の不景気の時代、収入が一つだけというのは、大きなリスクがあります。きちんと働いていても、会社の業績が悪いと、残業代が出なかったり、昇給しなかったり、ひどい場合には、給料が下がったりします。ひょっとしたら倒産して、仕事を失うかも知れません。ベンチャー企業の多くは、残念ながら10年以内につぶれてしまっているようです。株式や不動産の運用ができる方は別ですが、<span class="Blue_text">複数の仕事をもったライフスタイルが普通になる時代がやってくるかも知れません。</span></p>
  <p class="marginBottom20px">戦後、日本の景気は上向きが続き、企業は雇用を確保するため、年功序列、終身雇用を行ってきました。これにより、新卒で就職した会社に一生勤めるといった働き方が普通とされてきました。好景気の時は、これでも良かったかも知れませんが、今の時代は、世の中の移り変わりも速く、一つの専門性だけで、一生食べていけるとは限りません。実際にこのようなことが起きてから考えるのもいいですが、副業を経験しておくことにより、いざというときの気持ちが全然違ってくるでしょう。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom20px">なぜ副業がいけないのか（企業側の意見として）</h2>
<img src="/public/images/ccwork/065/sub_003.jpg" alt="なぜ副業がいけないのか（企業側の意見として）"  class="imagemargin_B25"/>
  <p class="marginBottom10px">念のため、企業側が副業をあまり良く思わない理由についても、確認しておきましょう。副業をすると、それだけ多くの時間働くわけですから、疲労も溜まります。そうすると、会社の業務に支障が出てしまうことを心配します。<span class="Blue_text">仕事だけでなく、プライベートな時間も拘束したいの？と思ってしまいますが、仕事時間以外は十分に休養して、万全の状態で仕事に臨んでもらいたい！？といった願望があるのかも知れません。</span></p>
  <p class="marginBottom20px">あとは、<span class="Blue_text">副業をすることによって、会社の秘密情報が漏れだしたり、優秀な人材が外部に流出したりする可能性もないわけではありません。</span>他の職場では、自分の存在を高く評価してくれて、給料も職場環境も、今よりも好条件を提示してくれる、となると、転職のきっかけになるかも知れません。</p>
  <h2 class="sideBlueBorder_blueText02 marginBottom20px">確定申告も忘れずに</h2>
  <p class="marginBottom20px"><span class="Blue_text">副業を行って、年間で20万円以上の副収入が発生した場合には、確定申告が必要</span>になります。また、月々のお給料から源泉徴収されている払いすぎた所得税も、確定申告で返金されます。</p>
  <h2 class="sideBlueBorder_blueText02 marginBottom20px">コールセンターで副業するときの注意</h2>
  <p class="marginBottom20px">大切なことは、副業を行うことによって、今の職場にも、コールセンターにも迷惑をかけないこと。その上で、本業も副業もきちんと仕事をして、結果を出していけば、文句を言われる可能性も少なくなるでしょう。</p>

    <h3 class="blueblockBG">補足</h3>
  <p class="marginBottom40px">今回は、副業について紹介しましたが、これは副業を奨励するものではありません。実際に副業を行う場合には、自己責任で行ってください。</p>
</section>

<aside>
  <dl>
  <dt class="headingG01">「コールセンター知っ得情報」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail61">大丈夫！？即日勤務OK！日払いOK！アルバイトとその実態</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  
  <aside class="tabNav_ccwork">
  <ul>
  <li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_a.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_a.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li>
<li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li>
  </ul>
  </aside><!-- / tabNav -->
  
  
</section>
