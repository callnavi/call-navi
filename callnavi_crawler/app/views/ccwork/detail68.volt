<section class="entryA01 ccwork_entry">
<header>
<div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">働く人の声</span></div>
<h1>テレマーケティングできついこと<br />〜アウトバウンド編〜</h1>
</header>
  
<section class="sect01">
<p class="marginBottom10px">電話をかける側のお仕事、アウトバウンドテレマーケティング。<br />お給料が高く、シフトの融通がきく、オフィスで働けるなどメリットもたくさんなのですが、やはり好待遇の影には、きつい・辛いと感じることもあるのでは？<br />確かに、お客様の状況を考えると、電話がかかってきて、商品やサービスの案内をされる。「あまり興味無いのにな。」「今忙しいのに・・・。」と迷惑に思った経験、みなさん一度はあると思います。そう考えると、やはり、テレマーケティング営業はまだまだお客様から好感を得るのは難しいのかも知れません。</p>
<p class="marginBottom20px">お客さんから何度も断られたり、きついことを言われたり、それがずっと続くとなると精神的に辛くなってしまいます。<br />そこで、今回は実際にアウトバウンドでテレマーケティングをしている人に聞いた、<span class="Blue_text">きつかった経験と、責任者に聞いたその対処法</span>をまとめてみました！</p>
<img src="/public/images/ccwork/068/main001.jpg" class="imagemargin_T10B10" alt="きつかった経験と、責任者に聞いたその対処法" />

<h2 class="sideBlueBorder_blueText02 marginBottom10px">これを心得ておけば大丈夫！<br />アウトバウンドで辛いこと定番３選</h2>
<p class="dot_yellowtext_Number">1</p>
<p class="dot_yellowtext_title">ガチャ切り</p>
<img src="/public/images/ccwork/068/sub_001.jpg" class="imagemargin_T10B10" alt="ガチャ切り" />
<p class="marginBottom20px">つまり、電話で私たちの声を聞いてサービスを話し始めた瞬間に切られてしまうこと・・・。「もしもし。わたくし、株式会社●●の▲▲と申しますが、この度・・」ガチャ！（ここで、電話を切られてしまう）となるわけです。</p>
<div class="imageBlockB01">
<p class="image"><span class="bluefukidasi_bluetext">経験者の声</span>
<img src="/public/images/ccwork/068/w_001.jpg" alt="経験者の声" class=" image_left" /></p>
<div class="contentsInner">
<div class="bluefukidasi">
<p class="marginBottom_none">まるで、自分自身を全否定されたみたいな気持になりますし、これが続くと相当辛いです。<br />自分の存在が日本中の人に迷惑がられているような気持ちにさえなります・・・。</p>
</div><!-- / bluefukidasi -->
</div><!-- / contentsInner -->
</div><!-- / imageBlockB01 -->
<p class="marginBottom_none">話を聞いてもらってからお断り・・・ではなくて、その前に立ちはだかる壁ですね。<br />これはどうしたら良いのでしょうか。</p>

<div class="imageBlockA01 sectborder1 marginBottom30px">
<p class="image">
<span class="pinkfukidasi_pinktext">SV・マネージャーからのアドバイス</span>
<img src="/public/images/ccwork/068/w_002.jpg" alt=""  class=" image_right" /></p>
<div class="contentsInner">
<div class="pinkfukidasi">
<p class="marginBottom_none">落ち込むことはありません！あなたの人間性が否定されたわけではないのです。問題は最初の声のテンション！「お客さまにメリットがあるから紹介しているんですよ」という気持ちで、自信を持つことが大事です。ハキハキと元気よく話してくださいね！元気がなかったり、ぼそぼそと話されたりすると、聞いている方もイライラしてしまします。また、ガチャ切りをされたら、こちらは電話を静かに切って一呼吸。さあ！気を取り直してコール数を上げていきましょう！</p>
</div><!-- / bluefukidasi -->
</div><!-- / contentsInner -->
</div><!-- / imageBlockA01 -->

<p class="dot_yellowtext_Number">2</p>
<p class="dot_yellowtext_title">クレームが怖い</p>
<img src="/public/images/ccwork/068/sub_002.jpg" class="imagemargin_T10B10" alt="きつかった経験と、責任者に聞いたその対処法" />
<p class="marginBottom20px">サービス案内していると、偶然怖いお客様にあたってしまうこともあります！</p>
<div class="imageBlockB01">
<p class="image"><span class="bluefukidasi_bluetext">経験者の声</span>
<img src="/public/images/ccwork/068/m_001.jpg" alt="経験者の声" class=" image_left" /></p>
<div class="contentsInner">
<div class="bluefukidasi">
<p class="marginBottom_none">話していたら、うまく伝わらなかったり、お客様の機嫌が悪かったりして怒らせてしまいました・・・。延々と怒られ、謝り続けていたのですが、電話口できついことを言われるなど、もう泣きたくなってしまいました。次に電話をかけるのが怖いです・・・。</p>
</div><!-- / bluefukidasi -->
</div><!-- / contentsInner -->
</div><!-- / imageBlockB01 -->
<p class="marginBottom_none">なるほど、お客様を怒らせてしまい、クレームになってしまった場合ですね。<br />顔の見えない相手とはいえ、電話口できついことを言われたり、怒られ続けたりするのはつらいものがありますね。こんなときはどうしたら良いでしょうか。</p>

<div class="imageBlockA01 sectborder1 marginBottom30px">
<p class="image">
<span class="pinkfukidasi_pinktext">SV・マネージャーからのアドバイス</span>
<img src="/public/images/ccwork/068/w_002.jpg" alt=""  class=" image_right" /></p>
<div class="contentsInner">
<div class="pinkfukidasi">
<p class="marginBottom_none"><span class="Pink_text">お客様を怒らせてしまった場合はとにかく、謝りつづけることです。</span>紳士な気持ちで営業マンとして対応してください。どうしてもわかっていただけず、怒りがおさまらない場合は、早々に責任者に代わって対応してもらいましょう。顔が見えない相手とはいえ、雰囲気や気持ちは、意外と声で伝わるものです。「<span class="Pink_text">お客様の貴重な時間をいただいている</span>」という感謝の気持ちを持って対応すれば、それが伝わって怒りが収まって、逆に話を聞いてくださることだってあるんです！きついことを言われても深呼吸して、気分転換をしましょう！<br />困ったときは責任者に代わってもらえば大丈夫ですね！だからクレームをおそれず、元気良く次の電話をしましょう♪</p>
</div><!-- / bluefukidasi -->
</div><!-- / contentsInner -->
</div><!-- / imageBlockA01 -->

<p class="dot_yellowtext_Number">3</p>
<p class="dot_yellowtext_title">受注取れません！</p>
<img src="/public/images/ccwork/068/sub_003.jpg" class="imagemargin_T10B10" alt="受注取れません！" />
<p class="marginBottom20px">コールセンターで働いていると、一度は立ちふさがる壁と言っても過言ではないですね。みなさん、これはどのように乗り越えているのでしょうか。</p>
<div class="imageBlockB01">
<p class="image"><span class="bluefukidasi_bluetext">経験者の声</span>
<img src="/public/images/ccwork/068/m_001.jpg" alt="経験者の声" class=" image_left" /></p>
<div class="contentsInner">
<div class="bluefukidasi">
<p class="marginBottom_none">なかなか取れないです・・・。こんなに苦戦すると思っていませんでした。お客さんとお話していると、慣れない質問をされたり、一人ひとり状況が違ったりして、そのたびに焦ってしまいます。その人たちから受注を獲得しなきゃと思うと、とてもプレッシャー。電話をかけるのが辛いです！</p>
</div><!-- / bluefukidasi -->
</div><!-- / contentsInner -->
</div><!-- / imageBlockB01 -->

<div class="imageBlockA01 sectborder1 marginBottom30px">
<p class="image">
<span class="pinkfukidasi_pinktext">SV・マネージャーからのアドバイス</span>
<img src="/public/images/ccwork/068/w_002.jpg" alt=""  class=" image_right" /></p>
<div class="contentsInner">
<div class="pinkfukidasi">
<p class="marginBottom_none">なかなか説明がうまく伝わらず、受注に繋がらないというのは新人なら必ず通る道！誰しもが経験していることです。でも実は、<span class="Pink_text">トークのスキルというのはあまり関係ない</span>のです。ほとんどのコールセンターでは責任者がトークの内容を考えて戦略を打っています。なので、電話をかける人はそれを実践するだけ。つまり<span class="Pink_text">特別なスキルは関係なく「必ず取れる！」と信じる強い気持ちがあれば大丈夫！</span>一度コツをつかむと楽しくなってくるという声はアルバイトさんからもよく聞かれます。<br />質問に対するマニュアルを作成しているところも多いです。初めて聞く質問と感じても、落ち着いて考えてみると案外マニュアルに載っていたりするので大丈夫です！どうしてもダメな場合はSVやマネージャーに相談してみてください。きっと、皆さんができるようになるまで指導してくれますよ！
</p>
</div><!-- / bluefukidasi -->
</div><!-- / contentsInner -->
</div><!-- / imageBlockA01 -->

<p class="marginBottom10px">問題を一つひとつ解決していくことで、自信もスキルも身に付けていけるというわけですね！コールセンターで働く前は、簡単に契約や受注を取れると思っていたという人もいると思いますが、このように、ガチャ切りされてしまう、お子様が対応など判断できる人が留守、クレームになってしまう・・・など、電話をかけた先で受注を取れる確率というのは、（商材やサービスにもよりますが、）すごく低いのです！<br />やはり乗り切るのは、<span class="Blue_text">「負けないぞ！」</span>という強い気持ちなのかもしれないですね！</p>
<p class="marginBottom40px">何より、訪問やカウンターでのサービスよりも営業の場数が踏めるのがコールセンターの良いところでもあります。1日のうちにたくさんの人と話すことで、スキル・自信・強い気持ちが身について行くのですね！</p>
</section>

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター働く人の声」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail58">コールセンターバイトで、就職活動を有利に！　電話対応、コミュ力、ビジネスマナーは最大の武器！</a></li>
  <li><a href="/ccwork/detail54">【コールセンター体験談！】コールセンターで実際に研修を受けてみた！</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  <aside class="tabNav_ccwork">
  <ul id="js-tab01">
  <li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_a.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_a.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li>
  </ul>
  </aside><!-- / tabNav -->
  
  
</section>
