<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">知っ得情報</span></div>
  <h1>「ストレスに負けない!」コールセンターバイト</h1>
  </header>
  
  <section class="sect01">
  <p class="marginBottom20px">ストレスを溜めないように！とよく聞くけど、
  ストレスを溜めないようにするにはどうしたら良いんだろう…。そもそもストレスが全くない人はいません。
</p>
  <img src="/public/images/ccwork/006/main001.jpg" alt="「ストレスに負けない！」コールセンターバイト" class="marginBottom20px" />
      <p class="marginBottom20px">人それぞれストレス耐性に違いがあります。メンタルを鍛えることと同時に、<span class="Blue_text">ストレスをコントロールする知恵を身につけることが大切</span>です。そこで今回は、ストレスをコントロールするコツをご紹介します。</p>
</section>
  
  <section class="sect02">
  <h2 class="sideBlueBorder_blueText02 marginBottom30px">ストレスをコントロールするコツとは？</h2>
  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />ポイント</p>
  <p class="yellowmiddlecircle_titlenumber">1</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">「私は意欲的に頑張っている！」など常にポジティブな言葉に言い換える。</p>
  </div>  
  <p class="marginB20 sectborder">何か嫌な事があったり、失敗したりすると、ついついネガティブな言葉を使いがちになりますよね。ネガティブな言葉が出てきたら、ポジティブな言葉に置き換えてみましょう。</p>

  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />ポイント</p>
  <p class="yellowmiddlecircle_titlenumber">2</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">優先順位をつける</p>
  </div>  
  <p class="marginB20 sectborder">今日一日にしなければいけないことに優先順位をつけましょう。そして、一番順位の低いことは無理にその日にやらないようにすること。</p>

  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />ポイント</p>
  <p class="yellowmiddlecircle_titlenumber">3</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">記録をつける</p>
  </div>  
  <p class="marginB20 sectborder">成功したこと、うまくいったことの記録をつける。ポジティブな感情を癖にするのは、なかなか難しいものです。記録をつけることで落胆しそうになっても、自信を回復できるようになります。また、自分の考えを書きこむことで、冷静に見つめ直すこともできます。</p>

  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />ポイント</p>
  <p class="yellowmiddlecircle_titlenumber">4</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">常に顔を上げる癖をつける</p>
  </div>  
  <p class="marginB20 sectborder">「顔をあげる」この行為をしている最中は、
  ネガティブ思考になりにくい性質があります。
  極力、顔は上げて前向きな姿勢を崩さないようにしましょう。</p>
  
  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />ポイント</p>
  <p class="yellowmiddlecircle_titlenumber">5</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">プラスの言葉と行動をとる人と一緒にいる時間をつくる</p>
  </div>  
  <p class="marginB20 sectborder">ポジティブな人と一緒にいると、
  自然と元気なオーラが伝わってくるものです。元気をもらうことで、積極性も出てきます。</p>

  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />ポイント</p>
  <p class="yellowmiddlecircle_titlenumber">6</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">生活リズムを整える</p>
  </div>  
  <p class="marginB20">規則正しい生活と、栄養バランスのとれた食事が理想です。
  しかし、毎日規則正しい行動をとるのは難しいですよね。では、どうしたら生活リズムを整えられるのでしょうか。
  ポイントになるのは「朝」です。平日も休日も「決まった時間に起きる」これだけでも生活リズムは朝型になり、
  自然と日中に目が冴えて活動しやすくなる身体になっていきます。</p>

  </section>
  

  <section class="sect08">
    <h3 class="blueblockBG">最後に</h3>
  <p class="marginBottom40px">ストレスに負けないコツを紹介しましたがいかがでしたか？
  癖や思考を急に変えるのは難しいことですが、改善できそうなことから少しずつ実行してみましょう。</p>
  </section>

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター知っ得情報」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail05">コールセンターの面接で失敗しないコツ？</a></li>
  <li><a href="/ccwork/detail06">「ストレスに負けない！」コールセンターバイト</a></li>
  <li><a href="/ccwork/detail09">健康の秘訣！知っておきたい体のケア</a></li>
  <li><a href="/ccwork/detail08">ライフスタイルに合った働き方を見つけよう！</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  
<aside class="tabNav_ccwork">
<ul><li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li><li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_a.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_a.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li><li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li></ul>
</aside><!-- / tabNav -->
  
  
</section>

