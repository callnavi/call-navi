<section class="entryA01 ccwork_entry">
<header>
<div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">働く人の声</span></div>
<h1>お笑い芸人の卵です！<br />そんな方は、高時給・シフト制のコールセンターで、<br />当面の生活費と将来の夢の両立を！</h1>
</header>
  
<section class="sect01">
<img src="/public/images/ccwork/076/main001.jpg" class="imagemargin_T10B30" alt="当面の生活費と将来の夢の両立を！" />
<p class="marginBottom20px">コールセンターで仕事をしていると、「えっ！　何でこの人がテレアポしているの？」と言いたくなる人がいることがあります。「実は、ミュージシャンになりたくて・・・」なんて言っている夢を追いかけている人に聞いてみると、短時間で、お金をたくさん稼いで、デビューの為に時間を使いたい！スケジュール的に他の仕事ができないから、なんて答えが帰ってきます。</p>

<h2 class="sideBlueBorder_blueText02 marginBottom10px">自分の夢を追い求めるのは、良いこと？悪いこと？</h2>
<img src="/public/images/ccwork/076/sub_001.jpg" class="imagemargin_T10B30" alt="自分の夢を追い求めるのは、良いこと？悪いこと？" />
<p class="marginBottom10px">そもそもの話になってしまいますが、自分の夢を追い求めるのは、良いことなんでしょうか？　それとも悪いことなんでしょうか？大きくなったら、野球選手になりたい！サッカー選手・建築家・医者・エンジニア・デザイナー・スチワーデス・学校の先生・科学者などなど、世の中には色々な職業があって、大多数の人はそれに向けて、大学や専門学校など進学先を選んでいくことになります。</p>
<p class="marginBottom10px">一方、ミュージシャン、女優（または男優。俳優）、お笑い芸人など、自分の努力だけでは実現が難しい（と一般的には考えられる）職業では、いつまでも夢を追いかけていると、両親や親戚から定職につくことを勧められたりすることも多いようです。</p>
<p class="marginBottom10px">でも、自分の周りで夢を実現している人が多かったり、<span class="Blue_text">どうしても諦めきれない夢だったりすると、夢を追い求められる環境が必要</span>になりますよね。誰でも、本当は、自分の大好きなことでお金を稼ぎたいと思っているし、大好きなことの方が、情熱的に仕事ができることは、心の底では理解していることでしょう。</p>
<p class="marginBottom20px">でも、今すぐに夢をかなえることが難しい。そんな時には、<span class="Blue_text">当面の生活費を稼ぐ手段として、コールセンターのアルバイトを経験するのもいいかも知れません。</span></p>

<h2 class="sideBlueBorder_blueText02 marginBottom10px">なぜ正社員にならないの？</h2>
<img src="/public/images/ccwork/076/sub_002.jpg" class="imagemargin_T10B30" alt="なぜ正社員にならないの？" />
<p class="marginBottom10px"><span class="Blue_text">夢を追いかけている途中の人は、正社員よりも、アルバイト、または派遣社員、契約社員の人が多いような気がします。</span>ミュージシャンになりたい為に、就職活動をあまりしてこなかった方が地方から上京して、東京でコールセンターのアルバイトをしているケースも多いようです。アルバイト一覧から、職歴不問・学歴不問・高時給などを探していると、警備員・深夜のコンビニ店員・居酒屋や飲食店の店長・そしてコールセンターの求人が見つかります。</p>

<h2 class="sideBlueBorder_blueText02 marginBottom10px">なぜ、コールセンターがいいの？</h2>
<p class="marginBottom10px">まずは、生活費を稼がなくてはいけないので、時給が高いバイトである必要があります。それでいて、<span class="Blue_text">自分の芸能活動も並行して行う為、シフト制で流動的なスケジュールだと都合がいい</span>です。</p>
<p class="marginBottom10px">そして、ここがポイントなのですが、<span class="Blue_text">お客さんと対面で接しない仕事が人気</span>です。アパレル店員や携帯電話のショップ店員だと、いくら売り出し中のアイドルとは言っても、顔がばれてしまう可能性がありますからね。また、屋外の仕事だと日焼けをしてしまうため、紫外線対策が必須になりますが、オフィスワークであるコールセンターでは、その必要性はありません。</p>
<p class="marginBottom10px">ここまでの条件がそろっていて、学歴不問、職歴不問なバイトって、コールセンター以外には、なかなか見つけられないのではないでしょうか？電話営業で成績がいいと、インセンティブが貰えるところもあり、結構な好条件になってしまいます。</p>
<p class="marginBottom20px"><span class="Blue_text">芸能界を目指している方は、コミュニケーション能力も高く、しゃべる仕事が得意な方が多い</span>ようです。特にお笑い芸人の方などは、<span class="Blue_text">お客様とのトークの中で、切り返しの練習にもなります</span>からね。</p>

<h2 class="sideBlueBorder_blueText02 marginBottom10px">コールセンター出身の芸能人</h2>
<p class="marginBottom10px">ここで、コールセンター出身の芸能人をみていきましょう。</p>
<p class="dot_underline">オアシス大久保佳代子さん</p>
<p class="marginBottom10px">最近、年下男性との結婚の話題がありましたオアシスの大久保佳代子さんは、OL時代にコールセンターのコミュニケーターをしていたそうです。<br />テレオペで、クレームを受ける仕事だったそうですが、SV（スーパーバイザー）として、新人教育もしていたとのこと。週2とかにペースを落として、しばらくは芸能活動と並行してコールセンターにも通っていたそうです。本人的には、休職を希望していたが、結局は退職となったという話もあります。今は大ブレイクしている大久保佳代子さんですが、コールセンター時代の貴重な経験が、今のネタにも生きているのかもしれませんね。</p>
<p class="dot_underline">チャーミング井上二郎さん</p>
<p class="marginBottom10px">2013年のキングオブコントで準優勝を決めたり、『芸人生活』を彩図社から出版したりと、売れっ子タレントとなった井上二郎さんも、下積み時代には、コールセンターでアルバイトの経験があるそうです。こちらも、クレーム処理が担当で、若い人はすぐにやめてしまうような職場だったようですが、井上二郎さんは大丈夫だったみたいです。インタビューでは、毎日相方（野田航裕さん）に人格を否定されていたので、大丈夫！なんて言っていたみたいですが、ここでもコールセンター時代の経験が生きているのかも知れませんね。</p>
<p class="dot_underline">じゅんいちダビッドソン</p>
<p class="marginBottom20px">フジテレビで放送されている「Ｒ―１ぐらんぷり２０１５」という番組で、なんと3751人という今までにない人数の頂点を極めたじゅんいちダビットソンさんは、全国ネットの冠番組と500万円の賞金も手に入れたそうです。優勝を決めたネタは、本田圭佑選手（サッカー日本代表でイタリア１部リーグ・ＡＣミラン所属）が、クレーム電話に対応する！？　しかもその設定が、賞味期限切れの惣菜を購入したお客さんの、クレーム対応をするスーパーの店員だそうです。深夜のスーパーでのバイト経験もあるようですが、コールセンターでの経験って、芸能人の方にとって、本当に貴重な存在なのかな！？　なんて思ってしまいます。</p>

<h2 class="sideBlueBorder_blueText02 marginBottom10px">コールセンターで働くコツは？</h2>
<img src="/public/images/ccwork/076/sub_003.jpg" class="imagemargin_T10B30" alt="コールセンターで働くコツは？" />
<p class="marginBottom10px">最後に、このように芸能界などに夢を求めている方が、コールセンターで働くときのコツについてまとめておきます。</p>
<p class="marginBottom10px"><span class="Blue_text">まずは、結果を出すこと！</span>しゃべりなど、自分にしかないスキルは積極的に活用して、お客さんはもちろん、同僚とも積極的にコミュニケーションをとりながら、仕事を進めていきましょう。そして、稼げるときに思いっきり稼いでおくことです。</p>
<p class="marginBottom10px">シフト制もフルに活用しておきましょう！レッスンやオーディションなど、変則的なスケジュールになってしまうかも知れませんが、シフト制なら大丈夫です。<br />だんだん売れてくると、<span class="Blue_text">時間的に厳しくなってくるので、稼げるときに稼ぐことは、大切</span>なことです。</p>
<p class="marginBottom40px">そして、<span class="Blue_text">仕事を楽しみましょう！</span>滑舌良くしゃべって、スムーズな電話対応も大切ですが、たとえ失敗してしまったり、クレームになってしまったりしたとしても、将来ブレイクしたときの貴重なネタになります。そのときまでの道のりは、長いかもしれないし、ひょっとしたら、すぐそこに来ているかも知れませんが、今の仕事であるコールセンター業務も大切にしながら、夢を追い求めていきましょう！</p>
</section>

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター働く人の声」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail58">コールセンターバイトで、就職活動を有利に！電話対応、コミュ力、ビジネスマナーは最大の武器！</a></li>
  <li><a href="/ccwork/detail57">結婚・引越し・出産があっても大丈夫！コールセンターの魅力とは？</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  <aside class="tabNav_ccwork">
  <ul id="js-tab01">
  <li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_a.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_a.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li>
  </ul>
  </aside><!-- / tabNav -->
  
  
</section>
