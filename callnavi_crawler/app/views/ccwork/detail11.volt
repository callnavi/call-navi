<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">基礎情報</span></div>
  <h1>コールセンターの専門用語！！</h1>
  </header>
  
  <section class="sect01">
     <p class="marginB20">コールセンターで働くにあたって、「それってどういうこと？」
     と疑問に思うワードがありませんか？意味がわからないと会社の中でコミュニケーションが上手くいきません。
     今回はそんなコールセンター用語の中でも、代表的な用語を皆さんにご紹介して行きます！</p>
  </section>
  
  <section class="sect02">
  <h2 class="sideBlueBorder_blueText02">初級編</h2>
  
  <img src="/public/images/ccwork/014/sub_001.jpg" alt="テレオペ・テレアポってなに？" class="imagemargin_T10B10" />
     <p class="marginT20B20">テレオペとは、「テレフォンオペレーター」のことで、
     <span class="Blue_text">オペレーターは電話を受ける仕事をします。</span>お客様からの商品の問合せや、クレーム対応など、
     交換台的な役割をするのが主な業務になります。 テレアポとは、「テレフォンアポインター」のことで、
     <span class="Blue_text">アポインターは電話をかける仕事をします。</span> 商品の販売や、お客様とのアポイントをとったりするのが主な業務になります。 
     求人募集の場合は、あえて区別なく書いてあることが多いので、応募の際は、募集要項を確認したり、面接のときにきちんと確認しましょう！</p>
     
  <img src="/public/images/ccwork/014/sub_002.jpg" alt="コンタクトセンターとコールセンターの違いって何？" class="imagemargin_T10B10" />
     <p class="marginT20B20">テレオペとは、「テレフォンオペレーター」のことで、
     まず、「コンタクトセンター」というのは、お客さまからの問い合わせ、
     FAX・メール・Webなど、<span class="Blue_text">電話に限らず多岐に渡り対応</span>をしているところです。そして、「コールセンター」というのは、
     お客さまの質問に回答したり、お客さまからの注文を受け付けたり、<span class="Blue_text">電話での対応</span>
     をしているところです。似ているようで、違いがあるんですね。</p>
    
  <img src="/public/images/ccwork/014/sub_003.jpg" alt="インバウンド・アウトバウンドって何？" class="imagemargin_T10B10" />
     <p class="marginT20B20">「インバウンド」は、お客様から<span class="textA01">「電話を受ける」</span>お仕事のことで、「アウトバウンド」は、こちらからお客様に<span class="Blue_text">「電話をかける」</span>お仕事のことです。</p>
  <img src="/public/images/ccwork/014/main_001.jpg" alt="「テレオペ＝インバウンド」「テレアポ＝アウトバウンド」"class="imagemargin_B25" />
    
  <img src="/public/images/ccwork/014/sub_004.jpg" alt="テレマーケティング・テレマって何？" class="imagemargin_T10B10" />
     <p class="marginT20B20">テレマーケティングとは、電話などでお客様に、
     <span class="Blue_text">商品やサービスの販売促進を行なうマーケティング手法のひとつ。</span> お客さまから「電話を受ける」
     インバウンドの仕事と、こちらからお客様に「電話をかける」アウトバウンドの仕事など区別なく、
     電話業務を指すときに使います。また、「テレマ」の正式名称は「テレマーケティング」を略称したものです。
     求人募集などでも良く見かけるワードですね。</p>

</section>

  <section class="sect02">
  <h2 class="sideBlueBorder_blueText02">上級編</h2>
  
  <img src="/public/images/ccwork/014/sub_005.jpg" alt="SV（スーパーバイザー）って何？" class="imagemargin_T10B10" />
     <p class="marginT20B20"><span class="Blue_text">コールセンターで働くオペレーターをまとめている責任者。</span>
     「SV」なんて聞くと、なんだか強そうに聞こえますが、実際に現場で強い力を発揮する人を指します。 
     全体的な対応レベルの水準を上げる役割を担うほか、シフトの作成や収支管理などセンターの運営にも携わることもあります。 
     また、クレーム対応や苦情の電話など、難易度の高い業務を受け持つのも特徴です。昇進の順番的には
     「オペレーター　→　リーダー　→　SV（スーパーバイザー）」という形が一般的なようですが、
     各企業ごとに様々で、昇進試験（面接・筆記・模擬研修など）を設けられているケースが多いようです。
  <img src="/public/images/ccwork/014/main_002.jpg" alt="オペレーター▶︎リーダー▶︎SV"class="imagemargin_T10B10" />
     </p>
     
  <img src="/public/images/ccwork/014/sub_006.jpg" alt="トーク・トークスクリプトって何？" class="imagemargin_T10B10" />
     <p class="marginT20B20">テレオペとは、「テレフォンオペレーター」のことで、
    コールセンターの「トーク・トークスクリプト」とは、
     オペレーターが電話対応するときに渡される、 <span class="Blue_text">基本的な台本のようなもの</span>を指します。
     色々な問い合わせに対応できるように、様々なケースを想定して作成された対話用のガイドラインです。
     オペレーター個々のお客さま対応クオリティを、一定のレベルに維持したり、間違ったことを案内しないためにも、
     重要な資料となります。 現場では「トークを読んで」なんて言いますが「トーク」
     自体が何かわからなければ、一体何を言っているのやら、わからないですよね。</p>
    
    
  <img src="/public/images/ccwork/014/sub_007.jpg" alt="モニタリングって何？" class="imagemargin_T10B10" />
     <p class="marginT20B20">コールセンターの「モニタリング」とは、
     主に<span class="Blue_text">オペレーターとお客様との通話内容をSVが、聞き、評価することを言います。</span>
     （録音されたログで聞く場合もあります） お客様とのやり取りから、オペレーター個々の能力を見極め、
     研修やコールセンター全体の通話品質の向上に活かされています。また、経験の浅いオペレーターが電話対応で行き詰まった際、
     モニタリングをしてフォローをしたり、新人にベテランとお客様が実際に話している所を聞かせてくれる
     モニタリング研修を行うコールセンターもあります。</p>

  <img src="/public/images/ccwork/014/sub_008.jpg" alt="笑声って何？" class="imagemargin_T10B10" />
     <p class="marginT20B20">普段話すときよりも、<span class="Blue_text">声のトーンをやや高めにして、
     笑顔で話すこと</span>を「笑声（えごえ）」と言います。笑顔で話すと、自然と声も明るく表情豊かになるのです。
      コールセンターでは、声の印象が全てなので、印象が良いの笑声を使って仕 事をしています。 
      声の笑顔で、気持ちの良い電話をしたいですね。</p>
</section>

  
  
  <section>
    <h3 class="blueblockBG">最後に</h3>
     <p class="marginBottom40px">まだまだ他にもたくさんコールセンターの業界用語はあります。
     その会社によって、使っている用語が違ったりすることもありますが、ほとんど同じ用語を使っている所が多いようです。
     今回紹介したのはほんのその一部です。専門用語と聞くと少し構えてしまいますが、
     働きながら、覚えていけるものばかりなので、自分のペースで覚えて働きやすい環境で働きましょう！</p>
      </section>

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター基礎知識」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail03">全国調査！コールセンターの時給比較</a></li>
  <li><a href="/ccwork/detail04">コールセンターの雇用形態</a></li>
  <li><a href="/ccwork/detail13">NGワードとクッション言葉に気をつけよう！</a></li>
  <li><a href="/ccwork/detail11">これは必須！コールセンターの専門用語！！</a></li>
  </dd>
  </dl>
  </aside>
  
<aside class="tabNav_ccwork">
<ul><li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_a.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_a.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li><li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li><li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li></ul>
</aside><!-- / tabNav -->
  
</section>

