<!-- InstanceBeginEditable name="mainContents" -->
<section class="entryA01">
<header>
<div class="caption">コールセンターの仕事とは？<span class="cate">基礎知識</span></div>
<h1>コールセンターってどんな仕事？</h1>
</header>

<section class="sect01">
<p class="marginB20">コールセンターとは、<span class="textA01">企業の商品・サービスに関するお問い合わせや、注文受付などの電話対応業務を専門的に行う事業所・窓口</span>のことです。一口にコールセンターと言っても、お客様センターや、テクニカルサポート、テレビ通販の注文受付など、様々な業務内容があるので、まずコールセンターのお仕事内容について理解しましょう。</p>
<div><img src="/public/images/ccwork/basic01/img_01.jpg" alt="" /></div>
</section>

<section class="sect02">
<h2 class="headingC02">コールセンターの業務内容とは？</h2>
<p class="marginB20">コールセンターの業務は、<span class="textA01">お客様からの電話を受ける「インバウンド（受電業務）」</span>と、<span class="textA01">お客様へ電話をかける「アウトバウンド（発信業務）」</span>の二つに大きく分かれます。<br>
電話の受け答えが中心となるため、オフィスワークが初めての方や接客業務が好きな方に人気！また、シフト制・残業なしの会社が多いので仕事を掛け持ちしたい人や、自分の時間を確保したい人ががたくさん働いています。</p>
<ul class="generalBlockC01">
<li class="contentsInner">
<img src="/public/images/ccwork/basic01/img_02.png" alt="インバウンド（受電業務）" class="mediaPC"><img src="/public/images/ccwork/basic01/img_02_02.png" width="286" height="" alt="インバウンド（受電業務）" class="mediaSP">
<dl class="captionBoxA01">
<dt>業務内容</dt>
<dd>テレビ通販の注文受付 / チケット受付<br>サービス・商品のサポートデスク<br>トラブル時のヘルプデスク　など</dd>
</dl>
</li>
<li class="contentsInner">
<img src="/public/images/ccwork/basic01/img_03.png" alt="アウトバウンド（発信業務）" class="mediaPC"><img src="/public/images/ccwork/basic01/img_03_02.png" width="286" height="" alt="アウトバウンド（発信業務）" class="mediaSP">
<dl class="captionBoxA01">
<dt>業務内容</dt>
<dd>申込内容の事前確認 / イベント案内<br>フォローコール / アンケート調査<br>サービスや商品の紹介・PR　など</dd>
</dl>
</li>
</ul>
</section>

<section class="sect03">
<h2 class="headingC02">コールセンター未経験でも大丈夫？</h2>
<div class="imageBlockA01">
<p class="image"><img src="/public/images/ccwork/basic01/img_04.jpg" alt="" /></p>
<div class="contentsInner">
<p>ほとんどのコールセンターは、スキルや資格は必要なし！どの会社も事前研修や、対応マニュアルがあるので、違う職種から転職する人、初めてアルバイトをする人でも安心して働けますよ。<br>
また、エクセルなどのパソコンスキルがないといけないと思う人も多いと思いますが、<span class="textA01">パソコンの基本操作や、文章入力ができれば初心者の方でも大丈夫</span>です。</p>
</div><!-- / contentsInner -->
</div><!-- / imageBlockA01 -->
</section>

<aside>
<dl>
<dt class="headingG01">「コールセンター基礎知識」オススメのページ</dt>
<dd>
<ul class="linkListA01">
<li><a href="/ccwork/detail03">全国調査！コールセンターの時給比較</a></li>
<li><a href="/ccwork/detail04">コールセンターの雇用形態</a></li>
</ul>
</dd>
</dl>
</aside>


<aside class="tabNav_ccwork">
<ul><li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_a.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_a.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li><li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li><li><!--<a href="/ccwork#tabContents03">--><span><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></span><!--</a>--></li></ul>
</aside><!-- / tabNav -->


</section>
