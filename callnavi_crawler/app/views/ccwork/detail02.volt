<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">知っ得情報</span></div>
  <h1>コールセンターで働くメリットってなに？</h1>
  </header>
  
<!--- 段落１ 開始 --->  
  <section class="sect01">
  <h2 class="sideBlueBorder_blueText02">コールセンターの業務内容とは？</h2>
  <img src="../public/images/ccwork/001/img_01.jpg" alt="コールセンターの業務内容とは？" />
  <p class="marginB20">コールセンター業務の最大のメリット、それは<span class="Blue_text">「コミュニケーション能力」がつく</span>ことです。
コールセンターの仕事は電話でのやりとりとなるので、お客様とは声のみでコミュニケーションを取らなくてはいけません。お客様から身振り手振りや表情が見えないので、あなたの声が表情となり、言葉遣いがあなた自身となります。

「ただの電話に大げさな」と思うかもしれませんが、会って話せたらすぐにわかってもらえるのに、電話だとなかなか伝わらない！なんて経験ありませんか？
声だけで言いたいことを伝えるというのは意外と難しいことで、<span class="Blue_text">声のトーンひとつで印象が逆になってしまう</span>事もあります。そんな状況だからこそ、コミュニケ―ション能力が自然と向上していくのです。
コミュニケーション能力は一生モノ。就職活動や今後別の仕事に就いた際にも、大いに役にたつ財産になります。
</p>
  </section>
<!--- 段落１ 終了 --->


<!--- 段落２ 開始 --->    
  <section class="sect02 marginBottom30px">
  <h2 class="sideBlueBorder_blueText02">職場環境面でのメリット</h2>
  <div class="contentsInner">
  
  <div class="yellowcirclewrapper">
  <!-- / 編集部分 -->
    <div class="yellowcircle">
    <p>MERIT<span class="yellownumber">1</span></p>
    <span>勤務時間に融通がきく</span>
    </div>
      <p class="yellowcircletext">ほとんどのコールセンターがシフト制なので<span class="Blue_text">、自分の都合のいい時間帯で働くことが可能</span>です。午前中のみといった短時間の勤務や、週に2〜3日といった働き方ができるので、主婦の方や学生の方が多く働いています。</p>
    <!-- / 編集部分 -->
    </div>

  <div class="yellowcirclewrapper">
  <!-- / 編集部分 -->
    <div class="yellowcircle">
    <p>MERIT<span class="yellownumber">2</span></p>
    <span>好待遇だから働きやすい</span>
    </div>
      <p class="yellowcircletext">商品知識などの専門性が高い仕事となりますので、<span class="Blue_text">時給は他の接客業より高め、残業がほとんどない</span>好待遇と働きやすい職場が多いです。また、商材研修など<span class="Blue_text">サポート制度が整っている</span>ところが多いので、未経験者でも安心して働けます。</p>
    <!-- / 編集部分 -->
    </div>

  <div class="yellowcirclewrapper">
  <!-- / 編集部分 -->
    <div class="yellowcircle">
    <p>MERIT<span class="yellownumber">3</span></p>
    <span>髪型・服装自由</span>
    </div>
      <p class="yellowcircletext">企業の規則にもよりますが、電話での対応なので基本的には服装・髪型が自由です。着替えずに済んだり、制服に気を遣わなくてもいいから気楽に働けます。</p>
    <!-- / 編集部分 -->
    </div>

  <div class="yellowcirclewrapper">
  <!-- / 編集部分 -->
    <div class="yellowcircle">
    <p>MERIT<span class="yellownumber">4</span></p>
    <span>快適環境で働ける</span>
    </div>
      <p class="yellowcircletext"><span class="Blue_text">空調の効いたオフィスでのデスクワーク</span>となるため、暑すぎたり寒すぎるといった環境面でのストレスがありません。座って仕事をするので、体力に自信がない方でも大丈夫です。</p>
    <!-- / 編集部分 -->
    </div>
  </div><!-- / contentsInner -->
  </section>
<!--- 段落２ 終了 --->

<!--- 段落3 開始 --->    
  <section class="sect03">
  <p class="marginBottom30px">さまざまな業界のコールセンターがあるので、学生の方や転職を考えている方は就職の前に業界の内情を知ることができます。また、電話対応が身につくので、事務や営業希望の方には特に大きなメリットとなります。
将来目標、キャリアアップ、スクール、趣味、育児などライフスタイルを大切にしながら効率よく働きましょう。
</p>
  </section>
<!--- 段落３ 終了 --->

<aside>
  <dl>
  <dt class="headingG01">「コールセンター知っ得情報」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail05">コールセンターの面接で失敗しないコツ？</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  <aside class="tabNav_ccwork">
  <ul>
  <li><a href="/ccwork#tabContents01"><img src="../public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="../public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents02"><img src="../public/images/ccwork/nav/tabnav_02_a.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="../public/images/ccwork/nav/tabnav_02_03_a.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li>
<li><a href="/ccwork#tabContents03"><img src="../public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="../public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li>
  </ul>
  </aside><!-- / tabNav -->
  
</section>

