<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">知っ得情報</span></div>
  <h1>あなたはどっち？ SV転向、アポインター継続</h1>
  </header>
  
  <section class="sect01">
  <img src="/public/images/ccwork/050/main001.jpg" class="imagemargin_T10B10" alt="あなたはどっち？ SV転向、アポインター継続" />
  <p class="marginBottom10px">コールセンターで働くメリットは多くあります。例えば、時給が高かったり、時間の融通が効いたり、座ったままできるので体力的にもきつくなかったり・・。長く働き、成績も伸びてコツをつかんでくると、周囲からも頼られることも増えてきますよね。</p>
  <p class="marginBottom10px">そんな中、<span class="Blue_text">スーパーバイザー（SV）と呼ばれる責任者として、キャリアアップする選択肢</span>が出てくることもあります。</p>
  <p class="marginBottom10px">そこで、今回は、キャリアチェンジで迷っている人や、コールセンターでのお仕事にチャレンジしてみたい人必見！スーパーバイザーに転向（キャリアアップ）する場合と、アポインターでいる場合のメリットとデメリットをまとめました。
</p>
</section>

  <section class="sect02">
  <h2 class="sideBlueBorder_blueText02 marginBottom10px">スーパーバイザー（SV）の仕事は？</h2>
  <img src="/public/images/ccwork/050/sub_001.jpg" class="imagemargin_T10B10" alt="スーパーバイザー（SV）の仕事は？" />
  <p class="marginBottom10px"><span class="Blue_text">スーパーバイザーとは部署をまとめる管理役の責任者。</span>アポインターと大きく違うのは、スタッフを管理し、育成することです。アポインターは架電して案内することがメインでしたが、スーパーバイザーは彼らを指導していく立場になります。</p>
  <p class="marginBottom10px">スーパーバイザーという職種があるのはコールセンターだけでなく、飲食店や小売業にも当てはまります。その場合、スーパーバイザーは、複数の店舗の運営を管理する役割を担います。店長に運営のアドバイスをしたり、スタッフを育成したり、他のエリアの状況を調べて、管理する店舗の改善に役立てたりしながら事業を成功に導いて行くのが仕事です。</p>
  <p class="marginBottom10px">コールセンターの場合も同じで、当然ながら<span class="Blue_text">アポインターよりも経営目線を持ちながら、全体の売上を上げていくことを目指して仕事をしていきます。</span></p>
</section>

  <section class="sect03">
  <h2 class="sideBlueBorder_blueText02 marginBottom10px">スーパーバイザーのメリット</h2>
  <img src="/public/images/ccwork/050/sub_002.jpg" class="imagemargin_T10B10" alt="スーパーバイザーのメリット" />
  <p class="marginBottom10px">前述にもあるように、<span class="Blue_text">スーパーバイザーはみんなの管理・指導役</span>です。当然、負う責任も大きくなるので、それに見合った給与が与えられ、基本給も高くなるでしょう。また、営業成績を上げた成果でもらえるインセンティブが、部署全体の成果として評価されるため、会社にもよりますが、単価も高くなります。</p>
  <p class="marginBottom10px">また、チームの底上げのために様々な工夫をこらしていく必要があり、お客様対応でも責任者としてレベルの高い対応が必要となるため、<span class="Blue_text">知識やビジネスマナーも身に付く</span>でしょう。</p>
</section>

  <section class="sect04">
    <h3 class="pinkblockBG">スーパーバイザーの方の声</h3>
  <img src="/public/images/ccwork/050/talk_001.jpg" class="marginBottom_none" alt="自分一人で目標達成した時よりも、チームで達成したときの喜びの方が断然大きい。" />
  <img src="/public/images/ccwork/050/talk_002.jpg" class="marginBottom_none" alt="自分一人で目標達成した時よりも、チームで達成したときの喜びの方が断然大きい。" />
  <img src="/public/images/ccwork/050/talk_003.jpg" class="marginBottom_none" alt="自分一人で目標達成した時よりも、チームで達成したときの喜びの方が断然大きい。" />
</section>

  <section class="sect05">
  <h2 class="sideBlueBorder_blueText02 marginBottom10px">ここが大変！スーパーバイザー</h2>
  <img src="/public/images/ccwork/050/sub_003.jpg" class="imagemargin_T10B10" alt="スーパーバイザーのメリット" />
  <p class="marginBottom10px">責任が重くなるということは、やはり大変なことも増えてきます。部署全体の売上を管理していくため、上司から売上のプレッシャーをかけられたり、部下がミスした場合の責任を取らなければならなかったりと、ストレスも増えてくるでしょう。</p>
  <p class="marginBottom10px">また、報告をまとめたり、会社によっては、部下の出勤・退社の管理を行ったりするところもあるので、アポインターに比べて帰りが遅くなることも考えられます。<span class="Blue_text">部下一人ひとりのフォローを行うことは、スキルだけでなく人間的な分野でもあるので、たやすいことではなく、当然、仕事についやす時間が多くなる</span>でしょう。</p>
</section>

  <section class="sect05">
  <h2 class="sideBlueBorder_blueText02 marginBottom10px">あなたはアポインター継続派？</h2>
  <img src="/public/images/ccwork/050/sub_004.jpg" class="imagemargin_T10B10" alt="スーパーバイザーのメリット" />
  <p class="marginBottom10px">一方で<span class="Blue_text">アポインターを継続していくことは、時間の拘束が少なく、自分のペースでシフトを入れることができるのが魅力</span>です。また、会社にもよりますが、スーパーバイザーほどではなくても、実力に応じてコツコツと時給や基本給を上げていくことができますし、自分の目標を達成すればインセンティブもそれなりにもらえます。</p>
  <p class="marginBottom10px">一方で、仕事の範囲が決まっているので、特別なスキルや成果と言えるものが表現しづらいというデメリットもあります。</p>
</section>

  <section class="sect05">
  <h2 class="sideBlueBorder_blueText02 marginBottom10px">アポインターとスーパーバイザーだけじゃない</h2>
  <img src="/public/images/ccwork/050/sub_005.jpg" class="imagemargin_T10B10" alt="スーパーバイザーのメリット" />
  <p class="marginBottom10px">また、アポインターからスーパーバイザーになる前に、アポインターのリーダーとして、後輩の育成に携わったりする階級を設けている会社もありますし、アルバイトでは難しいですが、スーパーバイザーをまとめる、さらに上のマネージャーと呼ばれる人たちを置いているコールセンターもあります。</p>
  <p class="marginBottom10px">一口にコールセンタースタッフと言っても様々な役割を担う人たちがいて、<span class="Blue_text">それぞれのライフスタイルに合ったキャリア設計も目指していける</span>でしょう。</p>
</section>

  <section class="sect05">
    <h3 class="blueblockBG">最後に</h3>
  <img src="/public/images/ccwork/050/sub_006.jpg" class="imagemargin_T10B10" alt="スーパーバイザーのメリット" />
  <p class="marginBottom10px">アルバイトでコールセンタースタッフとして始めたものの、結果がついてきたり、コミュニケーション能力が身についたりして、楽しくなり、<span class="Blue_text">そのまま社員になってキャリアアップ（社員登用制度）を目指す人も意外と多い</span>ようです。</p>
  <p class="marginBottom40px">高時給・シフトの融通が効くといった理由から、副業のつもりで選んだ仕事が思ってもみなかったやりがいやスキルに変わった！という嬉しい体験談です。仕事を通しての更なるスキルアップを目指すのならスーパーバイザーへの転向という道を選んでみても良いかもしれないですね！</p>
  </section>


<aside>
  <dl>
  <dt class="headingG01">「コールセンター知っ得情報」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail18">学歴なんて関係ない！コールセンターなら、実力次第で高収入が目指せる</a></li>
  <li><a href="/ccwork/detail39">初心者大歓迎！高時給バイトなら、コールセンターが狙い目</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  
  <aside class="tabNav_ccwork">
  <ul>
  <li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_a.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_a.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li>
<li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li>
  </ul>
  </aside><!-- / tabNav -->
  
  
</section>

