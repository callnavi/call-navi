<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">知っ得情報</span></div>
  <h1>これで受かる！コールセンター応募の志望動機</h1>
  </header>
  
  <section class="sect01">
     <p class="marginBottom_none">コールセンターで働きたいけど、志望動機に何を書けば良いの？未経験の場合どんなことをアピールすれば良いの？履歴書の悩みは就職活動につきもの！そこで今回は、コールセンターに応募する際の志望動機のポイントをご紹介します。</p>
    <img src="/public/images/ccwork/029/main001.jpg" alt="これで受かる！コールセンター応募の志望動機" class="imagemargin_T10B10" />
  </section>
  
  <section class="sect02">
  <h2 class="sideBlueBorder_blueText02">履歴書を書く、その前に・・・</h2>
     <p>コールセンターのオペレーターは、企業の商品セールスやお問い合わせ対応、クレーム対応などを行うお仕事です。会社の顔として多くのお客様と接するため、<span class="Blue_text">電話口での対応の良さ</span>が求められます。高給・シフト制と、条件が良いことが特徴の一つですが、長時間の電話対応では忍耐力も求められます。そのため、コールセンターで働く場合は、<span class="Blue_text">精神的に強く、前向きで、コミュニケーション能力が高いことなどが求められる</span>と認識しておきましょう！</p>
  <h2 class="sideBlueBorder_blueText02">タイプ別おすすめ！志望動機の書き方</h2>
    <img src="/public/images/ccwork/029/sub_001.jpg" alt="経験者におすすめな志望動機" class="imagemargin_T10B10" />
    <p class="pinkblockBG_title">●コールセンターでオペレーターを経験</p>
    <p class="pinkblockBG_title">●スーパーバイザーとしてキャリアアップしたい</p>
     <p class="marginT20B20">こんな人は、前職で培った業界知識やコールセンターでの実務経験があるので、募集ポジションで即戦力になれることをアピールすると良いでしょう！</p>

     <div class="freebox03_grayBG">
     <p class="text_bold marginBottom5px">例 ）</p>
       <p class="marginBottom_none">　御社の、●●サービスを幅広く展開している点に魅力を感じました。御社で案内されている●●サービスは、前職と同じ業界であることから、まずは前職で培ったサービスの知識をいかして貢献していけると思います。<br/>　また、私はコールセンターの営業経験から、自分の伝え方ひとつで効果が変わることを学びました。前職ではそのノウハウを仲間にも共有した結果、チーム全体の受注率向上につながりました。御社で採用していただけましたら、これまで以上に対応力を磨き、お客様満足度の向上につなげるとともに、チーム一丸となって会社の業績アップに貢献できるスーパーバイザーを目指して行きたいと思います。</p>
     </div>
    
     <p class="marginBottom_none"><span class="Pink_text">採用担当者が注目するのは、扱っていたサービスの概要や商材</span>などです。「役割」「担当したサービス」「顧客層・顧客数」「対応件数の目安」などをまとめておくと伝わりやすいです。その上で、「お客様満足度の向上」「業務効率化」「コスト削減」など、取り組んだことや実績をアピールすると良いでしょう。また、職歴にオペレーター数や担当業務、仕事内容など勤務していたコールセンターの概要をまとめておくのもわかりやすくて良いですね！ </p>
</section>

<section class="sect03">
    <img src="/public/images/ccwork/029/sub_002.jpg" alt="経験者におすすめな志望動機" class="imagemargin_T10B10" />
    <p class="blueblockBG_title">●バイト未経験</p>
    <p class="blueblockBG_title">●前職がまったく違う職種</p>
     <p class="marginT20B20">バイト未経験の人は「なぜコールセンターという仕事を選んだのか」、前職がまったく違う職種だった人は「キャリアチェンジしたい理由や、身に付けたいスキル」などを書くと良いでしょう。</p>

     <div class="freebox03_grayBG">
     <p class="text_bold marginBottom5px">例 ）バイト未経験の場合</p>
       <p class="marginBottom_none">　以前から働きながら会話のスキルを身につけることができる、コールセンターという仕事に魅力を感じていました。御社は研修が充実していて、未経験でも安心して始められるとのことでしたので応募させていただきました。相手の顔が見えない中でのコミュニケーションは大変なことも多いと思いますが、その分とてもやりがいのある仕事ではないかと感じています。<br />　私は人の話を聞くこと、会話をすることが好きなので、明るく誠実に、お客様との間に信頼関係を築いていけるよう頑張ります。</p>
     </div>

     <div class="freebox03_grayBG">
     <p class="text_bold marginBottom5px">例 ）前職が違う業種の場合</p>
       <p class="marginBottom_none">　私は前職で、●●ブランドの販売員として3年間勤務しておりました。お客様の雰囲気や表情に合わせて言葉を選び、話すスピードを工夫することによって、多くの方に納得していただけるご案内を心がけてまいりました。接客という仕事を通して、人と接することや、コミュニケーションを取る仕事にやりがいを感じ、さらにコミュニケーション能力を磨いて営業力を身に付けたいと考え、コールセンターのお仕事にチャレンジしようと思いました。<br />　ずっと接客の仕事に就いてまいりましたので、正しい敬語使いや失礼のない電話応対が出来る自信がございます。 それらを元に、新たな職場ルールを早く身に付け即戦力となれるよう頑張ります。</p>
     </div>

     <p class="marginBottom20px"><span class="Blue_text">採用担当者が注目する向上心やキャリアアップチェンジの理由</span>です。<br />今後、身につけたいスキルが述べられて、頑張ってくれそうという可能性を感じる文面だと、評価につながるでしょう。</p>

</section>

  <section class="sect04">
  <h2 class="sideBlueBorder_blueText02">こんな志望動機に注意</h2>

  <div class="freebox05_circle03_attention">
  </div>
  <div class="freebox05_circle_titlebox">
  <p class="freebox05_circle_titletext">シフト勤務で時間の融通が利きそうだから</p>
  </div>  
  <p class="marginBottom20px">特に正社員での転職をお考えなら、シフトのことを最初に書いてしまうと、あんまり働きたくないのかな、という印象を抱かれてしまう可能性があります。仕事以外にやりたいことがある、家庭の事情があって時間に制限があるなど、事情を説明した上で前向きに仕事に取り組みたいということをアピールするようにしましょう！</p>

  <div class="freebox05_circle03_attention">
  </div>
  <div class="freebox05_circle_titlebox">
  <p class="freebox05_circle_titletext">職場や悪口や批判</p>
  </div>  
  <p class="marginBottom10px">転職理由や退職理由が、「以前の職場が合わなかったから」という方もいらっしゃると思います。しかし、前の職場を批判する発言が多いと、文句が多く、自社の悪口も公開されかねない危険な人という印象を持たれてしまいます。批判的な表現よりも応募する会社のどこに惹かれているのかを考えるようにしましょう。</p>
  <p class="marginBottom50px">応募する企業の会社情報や扱っているサービスなどを調べてから、そこでどんな働きをしたいか明確に伝えることが履歴書突破の秘訣のようです。</p>

  <div class="freebox05_circle03_imagesA">
  </div>
  <div class="freebox05_circle_titlebox marginBottom20px">
  <p class="freebox05_circle_titletext">単調な仕事をコツコツと続ける忍耐力とコミュニケーション力に注目します。働く目的や身につけたいスキルなどをしっかり記載してくれる人は会ってみたいですね。</p>
  </div>

  <div class="freebox05_circle03_imagesB">
  </div>
  <div class="freebox05_circle_titlebox marginBottom20px">
  <p class="freebox05_circle_titletext">未経験の人でも、好き嫌いがなく、営業に抵抗がなさそうな方には可能性を感じます。</p>
  </div>  

  <div class="freebox05_circle03_imagesC">
  </div>
  <div class="freebox05_circle_titlebox marginBottom20px">
  <p class="freebox05_circle_titletext">面接では笑顔！元気！はきはき！この3つができていれば
OKです！</p>
  </div>
      </section>

  <section class="sect05">
  <p class="marginBottom40px">といった声が聞かれました。
やはり、前向きな姿勢が好印象を持たれるようです。<br />あくまでも参考に、自分の素直な思いやどんなことを目指したいかを伝えるように心がけましょう！</p>
  </section>

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター知っ得情報」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail18">学歴なんて関係ない！コールセンターなら、実力次第で高収入が目指せる</a></li>
  <li><a href="/ccwork/detail39">初心者大歓迎！高時給バイトなら、コールセンターが狙い目</a></li>
 </ul>
  </dd>
  </dl>
  </aside>
  
<aside class="tabNav_ccwork">
<ul><li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li><li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_a.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_a.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li><li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li></ul>
</aside><!-- / tabNav -->
  
  
</section>

