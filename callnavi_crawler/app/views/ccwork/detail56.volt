<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">働く人の声</span></div>
  <h1>えっ、あの人も！？テレアポに転職した先輩の意外な過去とは？</h1>
  </header>
  
<!-- 段落１ 開始 -->  
  <section class="sect01">
    <img src="/public/images/ccwork/056/main001.jpg" class="imagemargin_T10B10" alt="えっ、あの人も！？テレアポに転職した先輩の意外な過去とは？" />
  <p class="marginBottom10px">こんにちは。こちらのコンテンツをご覧くださっているあなたは現在転職活動の真最中でしょうか？転職活動中の方も、なんとなーく「コールセンターってどんなだろう？」とこちらのページを開いて下さった方も、コールセンターにご興味をお持ちいただきありがとうございます！</p>
  <p class="marginBottom10px">今回は、<span class="Blue_text">コールセンター業務未経験で全くの別業種からテレアポに転身し、大活躍を収めている方々のエピソードをご紹介</span>します！</p>

  <h2 class="sideBlueBorder_blueText02">Ｍさん27歳　男</h2>
  <p class="text_bold">経理・人事⇒コールセンター スーパーバイザー（課長相当）</p>
  <p class="marginBottom10px">現在コールセンターの営業職に転職をして３年目のＭさん。現在はコールセンター、スーパーバイザー職（SV、課長相当）でバリバリ活躍をしています。</p>
  <p class="marginBottom10px">そんなＭさんですが、最初今の会社に応募したさいは、経験職の「人事・経理」を希望していました。会社の方針で、事務系の職種に就く場合でもあっても、会社の事業をより深く知るために営業職からのスタートでした。</p>
  <p class="marginBottom20px">前向きなＭさんは、まずは与えられた仕事を精一杯頑張ろう！と電話営業に精を出したところ、なんと３ヵ月で営業職の課長代理にまで昇りつめてしまったのです！そして３年目に、自分の部署のチームメンバー達と営業成績で会社の記録を更新し、マネージャーに昇進しました。</p>
<div class="imageBlockB01">
<p class="image"><span class="bluefukidasi_bluetext">Mさん</span>
      <img src="/public/images/ccwork/004/sub001.jpg"alt="" class=" image_left" /></p>
<div class="contentsInner">
<div class="bluefukidasi">
  <p class="marginBottom_none">「前職は人事や経理の事務職をしていたので、まさか自分がこんなにも人を動かすことのできる人間だとは思っていませんでした。自分の新たな能力に気づかされたって感じです。コールセンター職は未経験でも応募可能な企業が多いので、ぜひ多くの人にこの仕事の面白さを知って欲しいですね！」</p>
</div><!-- / bluefukidasi -->
</div><!-- / contentsInner -->
</div><!-- / imageBlockB01 -->

  <h2 class="sideBlueBorder_blueText02">Nさん29歳　女</h2>
  <p class="text_bold">ケーキ屋勤務⇒コールセンター　サブスーパーバイザー（課長代理相当）</p>
  <p class="marginBottom10px">以前はコールセンターとは全く畑違いの、製菓の専門学校を卒業後、ケーキ屋さんでケーキを作りや販売の仕事をしていたＮさん。まさか自分がコールセンターで8人の部下と一緒に働くなんて考えてもいなかったそうです！</p>
  <p class="marginBottom10px">コールセンターに応募したきっかけは、学生時代からお菓作り一本で過ごしてきたので、ほかの世界も見たくなったからだとか。思い切って前職を退職後、ちょっとしたお小遣い稼ぎ感覚で始めたのが、コールセンターのアルバイトだったのです！</p>
  <p class="marginBottom10px">最初はお客様からのご質問等に答える受電の対応から始めたのですが、だんだん自分からもお客様に営業の電話をしてみたくなったという強者でいらっしゃいます！</p>
  <p class="marginBottom10px">営業の電話をかけ始めたら、意外にも契約がどんどん取れて、テレアポの楽しさに魅了されてしまいました！就職活動をするのを忘れてしまう程夢中で働いていたそうです。</p>
  <p class="marginBottom20px">その成果もあり、ある月にコールセンターで１位の成績を収めました！そこで会社側から正社員で働きませんか？というオファーを受け、役職も上がり、今では部下と一緒に毎月の数字を追う、女性スーパーバイザーをしています。今は、もう１つ上の役職を目指して、日々精進していらっしゃいます！</p>

  <h2 class="sideBlueBorder_blueText02">Yさん24歳　女</h2>
  <p class="text_bold">ホテル勤務⇒一般事務⇒コールセンター　一般社員</p>
  <p class="marginBottom10px">専門学校を卒業後、某ホテルで接客のお仕事をされていました。しかし、接客業ではなくＯＬになりたい！と思い、一般事務職に転職。その後ＯＬとして１年勤務しますが、裏方ではなく自分がプレイヤーとして働きたい！と強く思い、コールセンターの営業に応募を決意しました。</p>
  <p class="marginBottom10px">今は、一般社員でコールセンターにて営業のお仕事をしています。Ｙさんは営業経験が全くなかったので、最初は「自分からアプローチをして物を売る」ということに抵抗があったそうです。しかし今までの受け身の仕事とは違い、「自分で販路を開拓している感」があり、非常にやりがいを感じる！とおっしゃっています。Ｙさんにテレアポ営業の魅力ってなに？と尋ねてみたところ、こんな答えが返ってきました。</p>
<div class="imageBlockA01">
<p class="image">
<span class="pinkfukidasi_pinktext">Yさん</span>
<img src="/public/images/ccwork/004/sub002.jpg" alt=""  class=" image_right" /></p>
<div class="contentsInner">
<div class="pinkfukidasi">
  <p class="marginBottom_none">テレアポは電話での声のトーンや、間のあけ方や話し方など、全てが自分次第なんです。自分で試行錯誤をして工夫を続ければ、それが成績に繋がっていく。目に見えて成果が出るのでそれが快感です。もちろん、結果を出したときにもらえる賞金なども、やりがいの１つです♪</p>
</div><!-- / bluefukidasi -->
</div><!-- / contentsInner -->
</div><!-- / imageBlockA01 -->

    <h3 class="blueblockBG">最後に</h3>
  <p class="marginBottom10px">いかがでしたか？<br />コールセンターには様々な経歴をお持ちの方がいらっしゃいます。<br />「電話での対応は今までしたことないから…」<br />「なんとなく大変なイメージがあるから…」<br />とコールセンターで働くことをあきらめるのはまだ早いです！</p>
  <p class="marginBottom40px">今回は３名とも電話営業のお仕事をされていますが、コールセンターには営業ではない職種も沢山ご用意があります。上記３名のようにイキイキと働くことのできる、あなたの志向に合ったコールセンターを見つけてみてくださいね！</p>
</section>
<!-- 段落１ 終了 --> 
  

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター働く人の声」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail32">どうするクレーム？しっかり対応するための魔法の４ステップ！</a></li>
  <li><a href="/ccwork/detail51">営業経験が全くないのですが、コールセンターの仕事はできますか？</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  
  <aside class="tabNav_ccwork">
  <ul id="js-tab01">
  <li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_a.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_a.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li>
  </ul>
  </aside><!-- / tabNav -->
  
  
</section>
