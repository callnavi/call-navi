<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">知っ得情報</span></div>
  <h1>健康の秘訣！知っておきたい、体のケア</h1>
  </header>
  
  <section class="sect01">
     <p class="marginB20">コールセンターは、座りながらひたすら喋るお仕事なので、のどや耳の不調、座りっぱなしで腰が痛いなど様々な不調が出てくると思います。特にお客様と対応するのに、声が出なかったら大変ですよね…。大切なのは、体調が悪化する前に予防することです。そこで、コールセンターで働くうえで知っておきたい、体のケアをご紹介します。</p>
    <img src="/public/images/ccwork/009/main001.jpg" class="marginBottom10px" alt="健康の秘訣！知っておきたい、体のケア" />
  </section>
  
  <section class="sect02">
  <h2 class="sideBlueBorder_blueText02">のどのケア</h2>
  <p class="dot_underline">乾燥を防ぐ</p>
     <p class="marginB20">乾燥はのどの大敵です。<span class="Blue_text">こまめに水分を取り、のどを潤すようにしましょう。</span>オススメな飲み物は、身体への吸収が早いスポーツドリンクです。また、冷たいものより常温のものが良いとも言われています。</p>
    <div class="bluebox01">
    <p class="bluebox01_leftwhitetext_title"><span>オススメな</span>飲み物</p>
    <ul>
    <li>ポカリスウェット</li>
    <li>レモンジュース</li>
    <li>オレンジジュース</li>
    </ul>
    </div>
    
      <p class="dot_underline">のどを休める時間を作る</p>
     <p class="marginB10">なかなか、のどを休める時間を作るのは難しいと思います。しかし、<span class="Blue_text">ほんの少しでも良いので声を出さない時間を作り、のどのお休み時間を作る</span>よう、心がけてみましょう。</p>

      <p class="dot_underline">睡眠をしっかり取る</p>
     <p class="marginB20">のどを休めるのに1番大切なのは、睡眠をしっかりとることです。睡眠不足にならないようにしましょう。しかし、長時間とることが必ずしも良いわけではなく「質の良い睡眠をとること」です。<span class="Blue_text">90分の倍数を意識して起きる時間を設定</span>するといいでしょう。<span class="Blue_text">通常、7時間30分が1番良い睡眠時間</span>になります。</p>

    <img src="/public/images/ccwork/009/main002.jpg" alt="グラフ「国別の平均睡眠時間」" class="images_marginbottom30px" />

      <p class="dot_underline">肺活量を増やすこと</p>
     <p class="marginB20">のどなのに肺活量？と思った方も、いるかと思います。実は、肺活量が多ければ、のどに負担をかけずに声を出すことができ、のどを痛めにくくすることができます。</p>
  </section>
  
  
  <section class="sect03">
  <h2 class="sideBlueBorder_blueText02">耳のケア</h2>
  <p class="dot_underline">受話音量を小さめにする</p>
     <p class="marginB10">人によって声の大きさは違いますが、なるべく受話音量は小さめに設定し、<span class="Blue_text">相手によってこまめに調整</span>するようにしましょう。</p>
    

      <p class="dot_underline">耳の休息をとる</p>
     <p class="marginB20">コールセンターのお仕事は、耳もずっと働きづめですよね。1日の内、<span class="Blue_text">たまに騒音を避けて静かな場所で、休息する時間をつくる</span>ようにしましょう。</p>
  </section>


  <section class="sect04">
  <h2 class="sideBlueBorder_blueText02">腰のケア</h2>
  <p class="dot_underline">座りながら少し腰を動かす</p>
     <p class="marginB10">椅子に座りっぱなしだったりPCに向かいっぱなしなど、毎日同じ動作しかしなくなりがち。<span class="Blue_text">「体を動かさなすぎる」ことも腰痛になる原因の1つ</span>です。しかし、動き回ることも難しいですよね。腰が固まってきたなと感じたら、少しでもいいので腰を動かしましょう。<span class="Blue_text"> 腰を動かすことで、固まった筋肉が動きます。そして、血流が良くなり、腰まわりの疲労もとれていきます。</span></p>
    
      <p class="dot_underline">腹筋をつける</p>
     <p class="marginB10">毎日できるだけ、無理をしない程度に、腹筋をしてお腹の筋肉をつけるよう心がけましょう。</p>
      <p class="dot_underline">血流を良くする</p>
     <p class="marginB10">お風呂に入って腰をあたためて、血流をよくしましょう。その後、<span class="Blue_text">寝る前に軽めなストレッチ</span>をすると、硬くなった筋肉をほぐすことができるので腰痛予防に効果的です。</p>
     
      <p class="dot_underline">運動をする</p>
     <p class="marginB20"><span class="Blue_text">腰に良い運動は、水中でのウォーキングです。</span>水の抵抗で筋力がつき、浮力 により腰への負担が軽くなります。</p>

    <div class="bluebox01">
    <p class="bluebox01_leftwhitetext_title"><span>オススメな</span>運動</p>
    <ul class="commonbox_innermargin">
    <li>水中ウォーキング</li>
    <li>ハイキング</li>
    <li>ヨガ</li>
    </ul>
    </div>
  </section>


  <section class="sect08">
    <h3 class="blueblockBG">最後に</h3>
  <p class="marginBottom40px">いかがでしたか？<span class="Blue_text">体を健康な状態に保つためには、適度な休息と運動が必要</span>です。作業中、実行するのが難しいものもあるかと思います。できることからはじめてみましょう。</p>
  </section>

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター知っ得情報」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail05">コールセンターの面接で失敗しないコツ？</a></li>
  <li><a href="/ccwork/detail06">「ストレスに負けない！」コールセンターバイト</a></li>
  <li><a href="/ccwork/detail10">アイディアオフィスで疲れを吹き飛ばそう！</a></li>
  <li><a href="/ccwork/detail08">ライフスタイルに合った働き方を見つけよう！</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
<aside class="tabNav_ccwork">
<ul><li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li><li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_a.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_a.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li><li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li></ul>
</aside><!-- / tabNav -->
  
</section>

