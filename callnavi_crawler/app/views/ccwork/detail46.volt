<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">基礎知識</span></div>
  <h1>コールセンターの三種の神器！<br />PC・ヘッドセット・管理ソフト！</h1>
  </header>
  
<!--- 段落１ 開始 --->  
  <section class="sect01">
 <p class="marginBottom10px">コールセンターの求人情報に興味があるけど、応募しようか迷っている方に、コールセンターで使っている機器について紹介しますので、まずは職場環境について、イメージしてみましょう。</p>
    <img src="/public/images/ccwork/046/main001.jpg" class="imagemargin_T10B10" alt="コールセンターの三種の神器！PC・ヘッドセット・管理ソフト！" />
</section>

  <section class="sect02">
  <h2 class="sideBlueBorder_blueText02 marginBottom10px">コールセンターで使う電話</h2>
  <p class="marginBottom10px">コールセンター、ヘルプディスク、コンタクトセンター、サポートセンターなど、色々な種類がありますが、<span class="Blue_text">電話にてお客様対応を行う仕事を、テレアポ、テレオペ、テレマといいます。</span></p>
  <p class="marginBottom20px">ここでは、大きく分けて、電話とパソコンを使います。</p>
    <img src="/public/images/ccwork/046/sub_001.jpg" class="imagemargin_T10B10" alt="コールセンターで使う電話" />
  <p class="marginBottom10px">特徴的なのが、電話です。普段家庭で使っている電話とは違って、<span class="Blue_text">ヘッドセットあるいはインカムと呼ばれるマイク付きヘッドフォンのようなものを使います。</span><br />スカイプ通話などで使っている方も多いかも知れませんね。<br />一日中、電話対応、電話営業の仕事なので、ビジネスフォンのような電話の受話器を持っていると疲れてしまいますが、<span class="Blue_text">ヘッドセットだと、ハンズフリーで、両手が使えます。電話をかけながらパソコン操作を行うコールセンターの仕事では、このほうが便利</span>ですね！</p>
  <p class="marginBottom10px">右耳、左耳など片耳対応や、両耳にイヤフォンがついているヘッドセットがあり、右利き、左利きなど癖によって使いやすいものが違います。可能であれば、自分にあったものを使わせてもらえるか、確認しておくといいかも知れません。</p>
  <p class="marginBottom20px">あと、電話番号はフリーダイアルになっていることが多いですが、電話をかける側としては、IP電話になっていることも多いので、声が少し遅れるなど、ちょっとタイミングずれる場合もあるので、注意しておきましょう。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">コールセンターで使うパソコン</h2>
    <img src="/public/images/ccwork/046/sub_002.jpg" class="imagemargin_T10B10" alt="コールセンターで使うパソコン" />
  <p class="marginBottom10px">今の時代、パソコンはどの仕事でも必須のスキルと言えるかも知れませんが、コールセンターでも同じことです。</p>
  <p class="marginBottom10px">しかし、難しいソフトウェアや、プログラミングを必要としているわけではなく、コールセンターごとにインストール（セットアップ）されている顧客管理ソフトを起動して、そこから電話をかけたり（架電）、お客様から受けた電話（受電）の記録を残したりします。</p>
  <p class="marginBottom10px">ワード、エクセルなどのオフィスソフトを使う場合もありますが、ほとんどの場合は上記だけで問題ないようです。もちろん、使える方が、マニュアル作成やデータ解析など活躍の場が広がる可能性はあります。</p>
  <p class="marginBottom20px">コールセンターの<span class="Blue_text">PC操作でのポイント</span>は、早く、正確にタイピングできること。名前や住所などのお客様情報や、対応履歴などを入力することが多いですが、<span class="Blue_text">理想的には話すスピードと同じくらいの速さでタイピングできる方がいい</span>です！データベースから<span class="Blue_text">調べ物をする場合にも、素早く入力して、検索ができる方がいい</span>です！そういった意味では、<span class="Blue_text">タッチタイピング（ブラインドタッチ）ができて、いちいちキーボードを見なくても、入力できる方がいい</span>でしょう。特別なパソコンスキルは必要ありませんが、キーボードやマウス操作が軽快にできるといいですね。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">ヘッドセットと受話器、どっちがいい？</h2>
    <img src="/public/images/ccwork/046/sub_003.jpg" class="imagemargin_T10B10" alt="ガムを噛め！仕事中のリフレッシュは、これに限る！" />
  <p class="marginBottom10px">上記では、ヘッドセットを紹介しましたが、ビジネスフォンなど受話器を使って通話する場合もあります。受話器を使った場合だと、電話をかけたり、かかってきた電話を転送したり、その他の機能を使うときに便利です。</p>
  <p class="marginBottom10px">どちらを使うかは、コールセンターの意向にもよりますが、ずっと電話をかけているコールセンター、ヘルプディスク、通販の受付などではヘッドセットの場合が多いようです。</p>
  <p class="marginBottom10px">
その一方で、<span class="Blue_text">電話をかける業務と、調べ物や事務仕事が多いテクニカルサポート業務、電話代行や秘書業務などでは、受話器を使ったほうが便利</span>ですね。もちろん、複数人で同一の電話機を共有する場合も、受話器は必要になります。
</p>
</section>

  <section class="sect03">
  <h2 class="sideBlueBorder_blueText02 marginBottom10px">その他、コールセンターにあったらいいもの</h2>
  <p class="marginBottom10px">電話機、パソコンなどは、どこのコールセンターでも必ずといっていいほどありますが、必須ではないものの、あった方がいいもがあります。<br />一日中しゃべりっぱなしだと、喉への負担も大きいので、<span class="Blue_text">加湿器、うがい薬、マスク、のど飴など常備しているコールセンターもあります。</span>あと、大人数で一斉に電話をしていると、熱気でエアコンがききづらくなる場合もあるので、扇風機やサーキュレーターで空気を循環している環境だといいですね。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">コールセンターに持ち込めないもの</h2>
  <p class="marginBottom10px">服装時給、髪型自由、シフト制など自由度が高いバイトとして、コールセンターが人気ですが、残念ながら職場に持ち込めないものもあります。お客様の個人情報や、営業マニュアルなど企業秘密の部分もあるので、<span class="Blue_text">カメラ付きケータイ（スマホも含めて）は、注意されなくても、持ち込まない方がいい</span>でしょう。

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">コールセンターから持ち出しちゃいけないもの</h2>
  <p class="marginBottom40x">コールセンターの研修を受けて、スクリプトを読み込んで、ロールプレイングなどで練習を積んでくると、もっと上手になりたくて、家でも練習したくなります。<br />家でもトレーニングを積んで、少しでも早く営業成績を上げたい心意気は素晴らしいのですが、<span class="Blue_text">マニュアルの持ち帰りは厳禁</span>にしているところもあるので、まずはSVやリーダーに確認をとりましょう。</p>
</section>

  <section class="sect08">
    <h3 class="blueblockBG">最後に</h3>
  <p class="marginBottom10px"><span class="Blue_textbold">コールセンターで働く前に</span><br />「コールセンターってどんなとこ？」に対して、冷房のきいたオフィスで一日中電話をかけている仕事、ということはイメージできると思うのですが、具体的には、良く分からなかった方も多いかも知れません。</p>
  <p class="marginBottom10px">上記のように実際に使っているものや職場環境について、<span class="Blue_text">事前にネットなどで調べたり、面接時などに確認しておくと、実際に自分がその会社で働いたときに、どんな感じになるか、具体的にイメージしやすい</span>でしょう。</p>
  <p class="marginBottom40px">アルバイト、派遣、正社員に関わらず、新しい仕事に就く前は、不安もたくさんあるかも知れませんが、コールセンターに関する不安が少しでも和らいてくれたら幸いです。</p>
  </section>

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター基礎知識」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail13">NGワードとクッション言葉に気をつけよう！</a></li>
  <li><a href="/ccwork/detail23">日本語を正しく使えていますか？コールセンター業務に必要な丁寧語、謙譲語、尊敬語</a></li>
  </ul></dd>
  </dl>
  </aside>
  
<aside class="tabNav_ccwork">
<ul><li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_a.png" alt="基礎知識" class="mediaPC" height="" width="192"><img src="/public/images/ccwork/nav/tabnav_01_03_a.png" alt="基礎知識" class="mediaSP" height="" width="286"></a></li><li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" alt="知っ得情報" class="mediaPC" height="" width="192"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" alt="知っ得情報" class="mediaSP" height="" width="286"></a></li><li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" alt="働く人の声" class="mediaPC" height="" width="192"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" alt="働く人の声" class="mediaSP" height="" width="286"></a></li></ul>
</aside><!-- / tabNav -->
  
</section>

