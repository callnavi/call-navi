<section class="entryA01 ccwork_entry">
<header>
<div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">働く人の声</span></div>
<h1>喉が命！コールセンターバイトもしゃべりのプロ！<br />喉のケア方法と喉にいい食材はこちら</h1>
</header>
  
<section class="sect01">
<p class="marginBottom10px">コールセンターでは<span class="Blue_text">声が命綱！</span><br />声がお仕事道具になるので、喉のケアはかかせません！<br />テレアポの達人でも、喉が本調子でないと、突然契約が取れなくなる…と言われているくらい喉は重要です！対面の販売であっても、同じかと思います。商売道具の声、ぜひ大切にしたいですよね。</p>
<p class="marginBottom20px">今回は、<span class="Blue_text">声を出しやすくするコツや喉にいい食べ物、乾燥からの喉を守る方法</span>などをお伝え致します！万全なケアで、喉を守っていきましょうね！</p>
<img src="/public/images/ccwork/077/main001.jpg" class="imagemargin_T10B30" alt="コールセンターでは声が命綱！" />

<h2 class="sideBlueBorder_blueText02 marginBottom10px">喉も疲れる！？</h2>
<p class="marginBottom10px">皆さん、喉も疲れるって知っていましたか！？<br />声は、声帯・舌・唇などの発声器官を動かして発声されます。もちろんこれらの器官は筋肉でできており、中でも声帯はデリケートで疲れやすい部位らしく注意が必要です。</p>
<p class="marginBottom20px">長く話していて疲れたなぁ～と感じたとき、もしかしたら<span class="Blue_text">喉声（のどごえ）</span>になっているかもしれません！</p>

<h3 class="blueblockBG">喉声とは？</h3>
<p class="marginBottom20px">喉に力が入ってしまっている声のことを指します。<br />無駄な力が入っているせいで、とても喉は疲労しやすくなっている状態です。<br />詰まったような聞き苦しい声になってしまい、お客様も聞きにくいでしょう。</p>

<h3 class="blueblockBG">喉声にならないためには！？</h3>
<ul class="dot_bluetext">
<li><span>1</span>腹式呼吸</li>
</li>
</ul>
<p class="marginBottom10px">お腹から声をだすようにすると、<span class="Blue_text">喉の負担が軽減</span>します。</p>

<ul class="dot_bluetext">
<li><span>2</span>禁煙</li>
</li>
</ul>
<p class="marginBottom10px">たばこを吸っている人はやめましょう！<br />タバコの煙に含まれるタールが原因に！<br /><span class="Blue_text">声帯に炎症</span>が起こりやすく、喉声になりやすいとか。</p>

<ul class="dot_bluetext">
<li><span>3</span>喉仏の位置</li>
</li>
</ul>
<p class="marginBottom10px">喉仏が上がってしまっていたり、下がりすぎていたりしませんか？<br />あごはリラックスした状態で開いておきましょう。<br />喉仏は<span class="Blue_text">上がってもいない、下がってもいない状態</span>もしくは<span class="Blue_text">少し下がっている位</span>がベストだそうです。</p>

<ul class="dot_bluetext">
<li><span>4</span>首、肩、顔のストレッチ</li>
</li>
</ul>
<p class="marginBottom20px">声を出そう！とすると首周りの筋肉が力み、喉声になってしまうそうです。<br />お客様に電話を掛ける前に、<span class="Blue_text">肩を上下に動かしてみたり、軽く首を回してみたり、</span>とにかく力を抜いてリラックスできるような<span class="Blue_text">ストレッチを取り入れて</span>みましょう。<br />それだけでも、声は出やすくなるはずです。</p>

<h2 class="sideBlueBorder_blueText02 marginBottom10px">風邪気味！？喉が痛くなってしまった時は？</h2>
<img src="/public/images/ccwork/077/sub_001.jpg" class="imagemargin_T10B30" alt="風邪気味！？喉が痛くなってしまった時は？" />
<h3 class="blueblockBG">喉が痛くなる主な原因</h3>
<p class="marginBottom20px">細菌やウィルスが喉の奥にある扁桃腺や咽頭に感染し、炎症が起こり喉に痛みを感じるようになります。<br />喉の粘膜には繊毛があり、細菌やウィルス・外気の汚れから喉を守る働きをしているのですが、粘膜は乾燥すると繊毛が固くなり働きが鈍くなってしまい、ウィルスに感染しやすくなるので注意が必要です！</p>

<h2 class="sideBlueBorder_blueText02 marginBottom10px">乾燥は喉の大敵！マスクを着用しよう！</h2>
<img src="/public/images/ccwork/077/sub_002.jpg" class="imagemargin_T10B30" alt="乾燥は喉の大敵！マスクを着用しよう！" />
<p class="marginBottom10px">大道のマスクですが乾燥防止に対する効果があなどれません！<br />マスクの内側で吐く息によって湿度が保持されるため、喉を潤った状態を維持することができます。<br />また、マスクをすることで口呼吸になることを防ぐことができます。口呼吸をするとどうしても体内の水分を逃がしてしまったり、口から侵入してくるウィルスを防ぐことができなくなったりしてまいますよね。夜寝るときにマスクをしておけば、無意識のうちに口呼吸になることも防ぐことができるのでオススメです！<br />更に、マスクの中でもおすすめしたいのが</p>

<p class="marginBottom20px Blue_Btextbold">濡れマスク</p>

<p class="marginBottom10px">口元を加湿することにより、潤いが持続します。濡れマスクによって湿気を与えられた空気が、軌道全体に行き渡るため、喉を優しく守ってくれます。<span class="Blue_text">ウィルスは高い湿度に弱い</span>ことは、ご存知でしょうか？</p>
<p class="marginBottom10px">例えばインフルエンザウィルスだと、温度２１～２４度で湿度２０％を保った時、６時間後のウィルス生存率は６０％に対し、同温度で湿度を５０％に保つと、生存率は３～５％にまで減るそうです！湿度、風邪やウィルス予防には非常に大切ということがおわかりいただけたかと思います！</p>
<p class="marginBottom10px">薬局では、沢山の濡れマスクが販売されていますよね！<br />マスクに専用の保水液をかけるタイプのものや、加湿のフィルターをマスクの内ポケットに入れるものなど、様々です。あなたに合ったタイプのマスクを探してみてくださいね♪<br />お値段は３枚入り３、４００円位からです。<br />普通のマスクよりお値段はしますが、１０時間加湿が続きます！と謳っている商品もあるので、試してみる価値はありそうです。</p>

<div class="freebox01_blueBG_wrapper marginBottom40px">
<p class="freebox01_blueBG_title01">ワンポイントアドバイス</p>
<p class="freebox01_blueBG_title02">濡れマスクを自分で作ることもできます！</p>
<p class="marginBottom_none">①マスクを水でぬらして軽く絞ります。<span class="text_small">※冬はお湯を使うといいそうです！</span><br />②マスクの上から1/3の部分を外側へ折り返します。<br />これで塗れマスクの出来上がり！すごく簡単ですよね！<br />綿100％のものだと、付け心地も優しく、アレルギーをお持ちの方にも安心です。</p></div>

<h2 class="sideBlueBorder_blueText02 marginBottom10px">喉が痛くなってしまった時に食べたい食材</h2>
<img src="/public/images/ccwork/077/sub_003.jpg" class="imagemargin_T10B30" alt="喉が痛くなってしまった時に食べたい食材？" />
<ul class="dot_bluetext">
<li><span>1</span>はちみつ</li>
</li>
</ul>
<p class="marginBottom20px">はちみつには<span class="Blue_text">殺菌作用と炎症を抑える作用</span>があります。<br />はちみつの中の物質「グルコースオキシターゼ」という酵素が働き、強力な殺菌作用を発揮してくれるのです。<br />炎症効果でいうと、荒れた粘膜をはちみつが優しくなだめてくれて、痛みがとれるらしいですよ。<br />直接食べてももちろん効果はありますが、ぬるま湯+はちみつではちみつ水を作ってうがいをするもの効果てきめんですので、試してみてくださいね♪</p>

<ul class="dot_bluetext">
<li><span>2</span>はちみつ大根</li>
</li>
</ul>
<p class="marginBottom20px">はちみつの効果は既にご理解いただけたかと思います！良く耳にするはちみつ大根ですが、はちみつに大根を加えたら効果はどうなるのでしょうか？<br />大根には３つの優秀な成分が含まれています。</p>
<div class="freebox01_blueBG_wrapper marginBottom40px">
<p class="marginBottom_none"><span class="Blue_text">●</span>ジアスターゼ<br/>咳を止めたり、痰を出しやすくしたりしてくれる働きもあるそう。抗炎症作用もあり、炎症を軽減させてくれます。<br /><span class="Blue_text">●</span>イソチオイアネート<br/>大根おろしが辛い！と感じる成分の基。細菌と戦う力を強めてくれる成分です。<br /><span class="Blue_text">●</span>アリルスルフィト<br/>もう一つの大根の辛みの成分の名前です。抗菌作用があり、最近から喉を守ってくれるとか！</p></div>
<p class="marginBottom20px">これらの成分が、はちみつの「グルコースオキシターゼ」と組み合わされば、もう無敵！<br />はちみつに大根の成分がしみこみ、<span class="Blue_text">ダブル</span>で喉をいたわってくれます。</p>

<ul class="dot_bluetext">
<li><span>3</span>マシュマロ</li>
</li>
</ul>
<p class="marginBottom20px">え！？マシュマロ！？と思った方も多いのではないでしょうか？私も初めは驚きました。<br />マシュマロに特に喉に良い成分は入っていないのですが、マシュマロの主な成分である「ゼラチン」が喉を優しく保護して、空気の汚れや乾燥から喉を守ってくれるのです。炎症を持っている喉の痛みに効果的、ということになりますね！</p>

<h2 class="sideBlueBorder_blueText02 marginBottom10px">その他</h2>
<p class="marginBottom20px Blue_Btextbold">喉を温める</p>
<p class="marginBottom20px sectborder1">これはかなり効きます。私も喉から風邪を引いてしまいそうだった時に、ダメ元で試してみたのですが、<span class="Blue_text">厚めのスカーフ</span>を巻いていると、本当に喉の痛みが軽減しました！<br />ウィルスは低温の環境で元気に活動をするらしく、温度が上がると働きが弱まるそうなのです！<br />また温めて体温を保つことにより、喉も乾燥がしにくくなります。<br />簡単にできる対処法なので、喉が痛いかも！？と感じたときは、早めに試してみてください！</p>

<p class="marginBottom20px sectborder1">いかがでしたでしょうか。<br /><span class="Blue_text">喉が痛いかも！？</span>と感じたときは上記のことに加えて、食事や睡眠も見直してみてくださいね。栄養をしっかり取って、いつもよりゆっくり睡眠を取るだけで、治ってしまう場合もあります。<br />健康管理、気を付けていきましょうね♪</p>
</section>

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター働く人の声」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail32">どうするクレーム？しっかり対応するための魔法の４ステップ！</a></li>
  <li><a href="/ccwork/detail51">営業経験が全くないのですが、コールセンターの仕事はできますか？</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  <aside class="tabNav_ccwork">
  <ul id="js-tab01">
  <li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_a.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_a.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li>
  </ul>
  </aside><!-- / tabNav -->
  
  
</section>
