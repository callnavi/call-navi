<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">知っ得情報</span></div>
  <h1>冷房病にご注意！エアコンの効いたコールセンターで働く際の健康管理とは？</h1>
  </header>
  
  <section class="sect01">
  <img src="/public/images/ccwork/059/main001.jpg" class="imagemargin_T10B10" alt="冷房病にご注意！エアコンの効いたコールセンター働く際の健康管理とは？" />
  <p class="marginBottom10px">暑い夏場、職場のつめた～いエアコンにお悩みの方は多いはず。体が冷えてなんとなくダルい…。頭痛がする…。喉が痛い…。でも、大勢の人が一緒に働いている職場では、温度設定を気軽に変更できない…。特にコールセンターで働くのなら、喉の状態をキープするのも仕事のうち！<br />いまやエアコンは夏の必需品。使わなければ使わないで、熱中症になってしまう恐れもあります。しっかりとしたエアコン対策をとって、夏の冷房と上手にお付き合いしていきましょう。</p>
</section>

  <section class="sect02">
  <h2 class="sideBlueBorder_blueText02 marginBottom10px">そもそも、どういう仕組みで部屋は涼しくなるの？</h2>
  <img src="/public/images/ccwork/059/sub_001.jpg" class="imagemargin_T10B10" alt="そもそも、どういう仕組みで部屋は涼しくなるの？" />
  <p class="marginBottom20px">夏場はエアコンの冷房、もしくは除湿機能を使って部屋を涼しくしますよね。そもそもどういう仕組みで涼しくなっているのでしょう？</p>
<ul class="fukidasi02_graybox">
<li><p class="text_bold_b">【1】冷房</p></li>
<li><p class="marginBottom10px">冷房は温度を下げることが目的の機能です。<br />室内の空気を取り込んで熱を取り除き、冷たくなった空気を室内に戻しています。</p></li>
<li><p class="text_bold_b">【2】除湿</p></li>
<li><p class="marginBottom10px">除湿は湿度を下げることが目的の機能です。室内の空気を取り込んで水分を取り除き、その空気を室内に戻しています。ちなみに除湿には弱冷房除湿と再熱除湿があり、前者は水分を集めるために温度を下げた空気をそのまま室内に戻すのに対し、後者は温度を下げた空気を少し温めてちょうどいい温度で室内に戻します。</p></li>
</ul>
  <p class="marginBottom20px">つまり、室内のひんやりした空気は一度エアコンを通って室内へ戻っているんです。そもそもこのエアコンがカビやばい菌で汚れていたりするとそれらをまき散らしているようなもの。体調不良の原因にもなるので定期的に清掃メンテナンスをしましょう。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">なぜ、冷房で肌や喉の調子が悪くなるの？</h2>
  <img src="/public/images/ccwork/059/sub_002.jpg" class="imagemargin_T10B10" alt="なぜ、冷房で肌や喉の調子が悪くなるの？" />
  <p class="marginBottom10px">さて、冷房によって身体が冷えてしまうのはもちろん、肌が荒れてしまったり、喉の調子が悪くなったりという方も多いですよね。その原因はエアコンによる乾燥なんです。冬場は暖房に加湿器が欠かせない！というのはもう当たり前ですが、夏の冷房でも空気は乾燥してるんです！</p>
  <p class="marginBottom20px">先程説明した通り、冷房・除湿では「室内の空気を取り込んで熱・水分を取り除く」ので、喉や肌を健康に保つ湿度を下回ってしまうのです。</p>

  <div class="freebox01_blueBG_wrapper">
  <p class="freebox01_blueBG_title01">POINT1</p>
  <p class="marginBottom_none">冷房で室内が冷えたタイミングで加湿器を使いましょう！それが難しいのであれば</p>
  <p class="marginBottom_none"><span class="text_bold">①マスクをつける</span></p>
  <p class="marginBottom_none"><span class="text_bold">②のど飴をなめる</span></p>
  <p class="marginBottom_none"><span class="text_bold">③デスクや部屋に水を入れたコップや濡らしたハンカチなどを置いておく</span></p>
  <p class="marginBottom10px">などなど、冬の乾燥対策を思い出して実践しましょう。</p>

  <p class="freebox01_blueBG_title01">POINT2</p>
  <p class="marginBottom_none">夏だからこそお肌の乾燥対策を怠らないこと！乳液や化粧水はもちろん、それにプラスしてウォーターミストを携帯しておくなどして、夏のお肌を労わりましょう。</p>
  </div>
</section>

  <section class="sect03">
  <h2 class="sideBlueBorder_blueText02 marginBottom10px">なんとなく体がダルい…それって冷房病かも</h2>
  <img src="/public/images/ccwork/059/sub_003.jpg" class="imagemargin_B10" alt="個人情報を取り扱うルール！個人情報保護法とは？" />
  <p class="marginBottom10px">夏なのに手足が冷える、なんとなく体がダルい、肩こりや腰痛がひどい…などの体の不調。もしかしたら冷房病かもしれません。冷房病とは正式な病名ではなく、主に冷房に起因する「自律神経の乱れた状態」のことを言います。</p>
  <p class="marginBottom20px">暑い屋外と、エアコンの効いた室内との極端な寒暖の差を繰り返すことで自律神経のバランスがくずれてしまうのです。そうなると汗をかきにくくなり、疲労物質が体内にたまり、体がダルく、疲れやすくなって体調不良の原因になるのです。</p>

  <div class="freebox01_blueBG_wrapper">
  <p class="freebox01_blueBG_title01">POINT1</p>
  <p class="marginBottom_none">まずは身体を冷やさないことが基本です！特に首、腹部、足首は冷やさないようにストールを巻いたり靴下を履いたり、腹巻をするなどして冷房による冷えから身体を守りましょう。屋外では汗はこまめにふくことを意識しましょう。</p>
  <p class="freebox01_blueBG_title01">POINT2</p>
  <p class="marginBottom_none">身体の内側からの予防として、</p>
  <p class="marginBottom_none"><span class="text_bold">① 冷房の効いた室内では、たまに歩いたり屈伸したりして、足の血行を刺激してあげましょう</span></p>
  <p class="marginBottom_none"><span class="text_bold">②生姜やニンニクなど、身体の温まるものを食べましょう</span></p>
  <p class="marginBottom_none"><span class="text_bold">③夏はシャワーで済ませてしまいがちですが、なるべくぬるめの湯船につかるようにしましょう</span></p>
  </div>
</section>

<aside>
  <dl>
  <dt class="headingG01">「コールセンター知っ得情報」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail18">学歴なんて関係ない！コールセンターなら、実力次第で高収入が目指せる</a></li>
  <li><a href="/ccwork/detail39">初心者大歓迎！高時給バイトなら、コールセンターが狙い目</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  
  <aside class="tabNav_ccwork">
  <ul>
  <li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_a.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_a.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li>
<li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li>
  </ul>
  </aside><!-- / tabNav -->
  
  
</section>
