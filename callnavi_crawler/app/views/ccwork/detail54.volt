<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">働く人の声</span></div>
  <h1>【コールセンター体験談！】<br />コールセンターで実際に研修を受けてみた！</h1>
  </header>
  
<!--- 段落１ 開始 --->  
  <section class="sect01">
    <img src="/public/images/ccwork/054/main001.jpg" class="imagemargin_T10B10" alt="【コールセンター体験談！】コールセンターで実際に研修を受けてみた！" />
  <p class="marginBottom10px">コールセンターでのお仕事を始めるに辺り、どのような研修があなたを待ちわびているか気になりますよね。本日は、実際に私がコールセンターで働き始める前に受けた、<span class="Blue_text">2日間の新人研修体験談をご紹介</span>します。</p>
  <p class="marginBottom20px">もちろんコールセンターによって、研修内容や研修の総時間は変わってくると思いますので、一例として参考にしてみてください。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">1日目／2日間研修</h2>
  <p class="marginBottom10px">さぁ座学研修のスタートです！<br />この研修はアルバイト、契約・派遣社員、社員など雇用形態に関係なく、お客様対応を始める前に受講をする研修です。</p>
    <h3 class="blueblockBG">商品やサービスの説明（半日間）</h3>
  <p class="marginBottom10px">まずは会社の基礎知識、そしてこれからコールセンター業務で扱う商品についての知識を、半日かけて研修しました。ここで徹底的に「基礎的な商品知識」をインプットします。</p>
  <p class="marginBottom10px">この半日の商材研修だけで、全てのお客様の質問に答えられるようになる、とまではいきませんが、一通りお客様対応に困らない程の知識を身に付けることができます。</p>
  <p class="marginBottom20px">お客様対応を開始後は、この知識を基に更に勉強を重ね、あらゆるお客様に対応が可能な「プロフェッショナル」へと成長していきます。</p>


    <h3 class="blueblockBG">お客様を魅了する声作りの基本！発声練習</h3>
  <p class="marginBottom10px">コールセンターでは、声が命です。まずは、聞き取りやすい声をお腹から自然に出せるようになるよう、<span class="Blue_text">発声練習</span>を行いました。</p>
  <p class="marginBottom10px">普段の生活で、発声練習をすることってあまりないですよね。<span class="Blue_text">研修メンバー全員で、お腹に手を当て「あ・え・い・う・え・お・あ・お」「か・け・き・く・け・こ・か・こ…」と練習を重ねていきます。</span></p>
  <p class="marginBottom20px">私にとって発生練習は、は中学校の合唱祭以来のことでした。久しぶりだったので、最初は少し照れくさい気持ちもあったのですが、インストラクターのためらいが1mmもない様子に、恥ずかしがっている方がはずかしい！という気分になりました。そして、最後は気分良く発声練習に取り組むことができました。</p>

    <h3 class="blueblockBG">スクリプト（お客様対応時の台本）の配布</h3>
  <p class="marginBottom10px">お待ちかねのスクリプトが配られます。<span class="Blue_text">スクリプトとはお客様対応をする時の台本のようなもので、対応の手順、セリフ等が書かれています。</span>各コールセンターによって内容は違いますが、営業担当、受電専門など、各ポジションに応じてそれぞれのスクリプトが用意されています。</p>
  <p class="marginBottom20px">まずは、インストラクターが見本でスクリプトを読んでくれます。当たり前ですが、これがまた上手！読む上でのポイントや強弱の付け方、間の取り方など、大事な部分を解説してくれました。<br /><span class="Blue_text">＊このスクリプトは企業秘密になるので、社外への持ち出しは禁止だそうです！</span></p>

    <h3 class="blueblockBG">スクリプトを30回音読する！</h3>
  <p class="marginBottom10px">スクリプトを読む上での重要ポイントやコツを習ったら、次はスクリプトの読み込みです！</p>
  <div class="freebox01_blueBG_wrapper">
  <p class="freebox01_blueBG_title01">ポイント</p>
  <p class="marginBottom10px"><span class="Blue_text">1人でぶつぶつと10分間で30回音読しました！</span><br />ここでは「音読」というのがポイントです！音読は、黙読よりも脳を活性化させることができ、読んだ商材の記憶が、黙読時より20％アップするらしいのです！</p>
  <p class="marginBottom10px">また音読は単調運動なので、脳内にセロトニンという神経伝達物質が分泌されます。このセロトニンはやる気を生み出す物質。研修時間が長くなってくると、どうしても飽きてきたり、眠くなってしまったり…しますよね。そんな時に音読を利用すると、記憶力が増して、やる気も取り戻すことができるので、研修の中盤に効果的なワークです。</p>
  </div>

    <h3 class="blueblockBG">2人1組で音読の成果発表！</h3>
  <p class="marginBottom10px">30回の音読が終わったら、次は隣の研修生とペアを組み、１人ずつスクリプトを読んでいきました。</p>
  <p class="marginBottom20px"><span class="Blue_text">良かった所や改善点を教え合い、お互いのスキルの向上を目指します。</span>「人のふり見て我がふり直せ」とあるように、ほかの人の音読を聞いて学ぶことが非常に沢山あります！特に相手の改善点を発見すると「自分は次ここの声のトーンを上げて読んでみようかな！」など、自分の音読の改善に役に立ちました。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">2日目／2日間研修（最終日）</h2>
    <h3 class="blueblockBG">効果的なあいずち、語尾をあげる、クッション言葉、正しい敬語研修</h3>

    <ul class="dot_bluetext">
    <li><span>1</span>効果的なあいづち</li>
    </ul>
  <p class="marginBottom10px">お客様の話をきちんと聞いています！という意思表示となるあいづち。相手に表情が伝わらないテレアポのお仕事で、あいづちは非常に大切な役割をもちます。</p>
  <p class="marginBottom10px"><span class="Blue_text">あいづちによって「同意・共感」を示すこともできます。人間は、相手に共感してもらえた！と感じると、心を開きおしゃべりになるもの。</span></p>
  <p class="marginBottom30px">適切なタイミング、ニュアンスであいづちを打ち、お客様の信頼をゲットしましょう！</p>

    <ul class="dot_bluetext">
    <li><span>2</span>語尾を上げて話す</li>
    </ul>
  <p class="marginBottom10px">通称「語尾上げ」と言われ、「もしもし○○です！」と言うときに、最後の部分（です！）を上げて話すことを言います。私の場合、この語尾上げに慣れるまで、少し時間がかかりました。</p>
  <p class="marginBottom30px">前職では「語尾を下げる」ことを意識して電話対応をしていたので、なおさら難しかったです。語尾を上げると、お客様に親しみやすく、明るい印象を持ってもらうことができるので、私が所属していたコールセンターでは、必要不可欠な話し方となっていました。</p>

    <ul class="dot_bluetext">
    <li><span>3</span>クッション言葉</li>
    </ul>
  <p class="marginBottom10px">その名のとおり、<span class="Blue_text">こちらからの言葉を相手にとって受け止めやすくする「クッション」の役目を果たす言葉</span>です。</p>
  <p class="marginBottom10px">「おそれ入りますが…・大変申し訳ございませんが…・失礼ですが…」などがあります。</p>
  <p class="marginBottom10px">この魔法の言葉を挟むことで、こちらからの依頼や質問が、お客様によりすんなり受け入れてもらえるようになります。</p>
  <p class="marginBottom30px">クッション言葉は、最初は意識しないと会話に組み込むことができませんでした。しかし、研修で練習をしていくにつれて、自然に使えるようになっていきました。</p>
  
    <ul class="dot_bluetext">
    <li><span>4</span>正しい敬語</li>
    </ul>
  <p class="marginBottom10px">今までこれらの敬語が間違いだったなんて、全く思っていませんでした。なんとも、お恥ずかしい限りですが…</p>
<ul class="fukidasi02_graybox">
<li>誤）「了解しました。」</li>
<li>正）「承知しました。」「かしこまりました。」</li>
</ul>

<ul class="fukidasi02_graybox">
<li>誤）「させていただいております。」</li>
<li>正）「しております」</li>
</ul>

  <p class="marginBottom10px">例えば、<span class="Blue_text">「値下げさせていただいております。」の場合</span>、相手が頼んだわけでもないのに値下げしたのなら、<span class="Blue_text">「値下げしております。」が適切</span>な表現だそうです。</p>
  <p class="marginBottom20px">敬語研修では、このほかにも沢山の間違い敬語を指摘され、ビジネスマナーに少し強くなれた気がします。</p>

    <h3 class="blueblockBG">クレーム研修</h3>
  <p class="marginBottom10px">こちらはお客様からクレームを受けた際の、対応方法を学びました。私がコールセンターで業務をするに辺り、一番心配に思っていたのがクレームについてでした。</p>
  <p class="marginBottom20px">しかし、私が所属するコールセンターでは非常にしっかりとしたクレーム対応の研修を行ってくれたので、すごくホッとしました。企業側もやはりこちらの心配ごとはお見通しなのですね。</p>
<ul class="fukidasi02_graybox">
<li>1）どんな種類のクレームがあるか</li>
<li>2）それぞれのクレームについての対処法</li>
<li>3）実際にお客様からのクレームの録音を聞いてみる</li>
<li>4）インストラクターがクレームを言うお客様になり、対応のモデル練習を行う</li>
</ul>
  <p class="marginBottom20px">こちらのクレーム研修に２時間程費やした記憶があります。とにかく、きめ細かく私たち研修生の不安をぬぐうが如く、対処法を教えてくれるので安心です。中でも、<span class="Blue_text">一番安心感を覚えたのは、「クレームは上司の方で処理するので、早い段階で上司に電話を代わってください！私たちが対応します。」と言われたことでした。</span>心強い上司がいれば、クレームも怖くない！と思えた瞬間でした。ゆくゆくは、自分である程度のクレームも対応できるようになれるよう、知識を身に付けていきたいと思います。</p>

    <h3 class="blueblockBG">個別訓練</h3>
  <p class="marginBottom10px">１人ひとりインストラクターと１対１で最終特訓を受けました。個人の持つ癖や、改善点を指摘して頂き、一緒に練習をしてくれました。</p>
  <p class="marginBottom10px">最後に<span class="Blue_text">自分のスクリプト音読をレコーダーに録音して、実際に自分の音読を聞くという作業をします。</span></p>
  <p class="marginBottom20px sectborder1"><span class="Blue_text">自分でできているつもりだったことが、実際はできていなかったり、思っている以上に上手な部分があったり、新たな発見が沢山あります。</span>自分の声を聞くのは恥ずかしいですが、一番勉強になった研修のひとつかもしれません。</p>
  <p class="marginBottom10px">以上が私の受けた２日間の座学研修です。</p>
  <p class="marginBottom40px">結構盛りだくさんの内容ですよね！もしコールセンターで働くことに、少し不安を覚えているようであれば、研修制度がしっかりしているところを探してみてはどうでしょうか？<br />研修をみっちり受け、自信を持ってお客様対応に臨みましょう！</p>
</section>
<!--- 段落１ 終了 ---> 
  

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター働く人の声」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail32">どうするクレーム？しっかり対応するための魔法の４ステップ！</a></li>
  <li><a href="/ccwork/detail45">テレアポ、テレオペ、テレマの違いって？自分にピッタリの仕事を見つける！</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  
  <aside class="tabNav_ccwork">
  <ul id="js-tab01">
  <li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_a.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_a.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li>
  </ul>
  </aside><!-- / tabNav -->
  
  
</section>
