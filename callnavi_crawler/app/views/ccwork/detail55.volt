<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">知っ得情報</span></div>
  <h1>取扱いにご注意！個人情報とコールセンター</h1>
  </header>
  
  <section class="sect01">
  <img src="/public/images/ccwork/055/main001.jpg" class="imagemargin_T10B10" alt="取扱いにご注意！個人情報とコールセンター" />
  <p class="marginBottom10px"><span class="Blue_text">コールセンターでは、お客様の個人情報を取り扱っています。</span>しかし、ちょっとしたことでその情報が漏れてしまえば、たちまちその企業は営業停止になってしまうことも。顧客情報の流出はどんな企業でも気を張るポイントですが、コールセンターではその情報を利用してお客様とのやりとりをするので一層気が抜けません。それでは、<span class="Blue_text">そもそも個人情報ってなに？</span>というところから一度確認しておきましょう！</p>
</section>

  <section class="sect02">
  <h2 class="sideBlueBorder_blueText02 marginBottom10px">そもそも個人情報ってなに？</h2>
  <img src="/public/images/ccwork/055/sub_001.jpg" class="imagemargin_T10B10" alt="そもそも個人情報ってなに？" />
  <div class="freebox01_blueBG_wrapper">
  <p class="freebox01_blueBG_title01">個人情報とは</p>
  <p class="marginBottom10px">その情報に含まれる氏名や生年月日その他の記述により、特定の個人を識別することができるものをいいます。</p>
  </div>

  <div class="freebox01_pinkBG_wrapper">
  <p class="freebox01_pinkBG_title01">個人データとは</p>
  <p class="marginBottom10px">個人情報の集合物である個人情報データベース等を構成する個人情報をいいます。</p>
  </div>

  <p class="marginBottom10px">ちなみに、コンピューターで処理されたデータベースだけでなく、紙に記録されたものであっても個人情報を容易に検索できるものなどは個人情報データベース等に該当します。</p>
</section>

  <section class="sect03">
  <h2 class="sideBlueBorder_blueText02 marginBottom10px">個人情報を取り扱うルール！個人情報保護法とは？</h2>
  <img src="/public/images/ccwork/055/sub_002.jpg" class="imagemargin_B10" alt="個人情報を取り扱うルール！個人情報保護法とは？" />
  <p class="marginBottom10px">よく耳にする、個人情報保護法とは一体何なのでしょう？消費者庁では下記のようにまとめられていました。</p>
<ul class="fukidasi02_graybox">
  <p class="marginBottom10px">だれもが安心してIT社会の便益を享受するための制度的基盤として、平成１５年５月に成立し、公布され、１７年４月に全面施行されました。<br />この法律は、個人情報の有用性に配慮しながら、個人の権利利益を保護することを目的として、民間事業者の皆様が、個人情報を取り扱う上でのルールを定めています。<br /><span class="text_small">(参照：消費者庁 http://www.caa.go.jp/planning/kojin/)</p>
</ul>

  <p class="marginBottom20px">つまり、<span class="Blue_text">事業者が個人情報を取り扱うときのルールが個人情報保護法ということになります。</span>ルールを定めたことによって、消費者は事業者による個人情報の取り扱いに不安を感じれば、自分に関する情報の開示や訂正、利用停止などをその事業者に求めることができるようになりました。<br />その際、<span class="Blue_text">個人情報を保有している全ての事業者が対象というわけではありません</span>のでご注意ください。事業用に保有している個人データが、過去6ヵ月において5,000人分を越えた場合、その事業者は個人情報取扱事業者となり、個人情報保護法の義務の対象となります。</p>
</section>

  <section class="sect04">
  <h2 class="sideBlueBorder_blueText02 marginBottom20px">コールセンターではどんな対策がとられているの？</h2>
  <p class="marginBottom10px">多くのコールセンターでは個人情報を扱っているので、さまざまな情報漏えいの対策をとっています。例えば<span class="Blue_text">コールセンター内における携帯電話の使用を禁止</span>していたり、<span class="Blue_text">ちょっとした紙切れに書いたメモであってもコールセンター外への資料持ち出しは全て禁止</span>としたりしているところもあります。また、スタッフが業務に不必要な個人情報を取得できないよう、対応している顧客以外の個人情報は検索できない仕組みになっているところがほとんどです。もし検索できても、基本的に検索履歴が残るようになっています。</p>
</section>

  <section class="sect05">
  <h2 class="sideBlueBorder_blueText02 marginBottom20px">コールセンターで働くにあたって気をつけなくてはいけないこと</h2>
  <img src="/public/images/ccwork/055/sub_003.jpg" class="imagemargin_B10" alt="コールセンターで働くにあたって気をつけなくてはいけないこと" />
  <p class="marginBottom40px">個人情報の取り扱いに関しては、コールセンターごとに対策マニュアルなどが準備されていると思いますので、そちらで確認してください。この場では皆さんがコールセンターで働くにおいて、心がけてほしいことをお伝えしておきます。</p>
  <p class="marginBottom10px">それは…<span class="Blue_text">「コールセンター内で見聞きしたことを、基本的に外部に漏らさない」</span>ということです。</p>
  <p class="marginBottom40px">お客様の情報はもちろんですが、業務内容の情報公開もNGのところがあります。アルバイトスタッフが<span class="Blue_text">フェイスブックやツイッターといったSNSで安易につぶやいた内容がきっかけで、コールセンターの操業が停止したという事例もありますので要注意</span>です。コールセンター内では当たり前のことでも、企業にとっては秘密事項であったりします。そのあたりの判断ができないうちは、<span class="Blue_text">仕事内容に関することはできるだけ口外しない</span>でおきましょう。<br />そこだけ聞くとクローズドな印象を持たれるかもしれませんが、コールセンターは情報を扱っている最前線の現場ですので、その意識だけは持っておきましょう！</p>
  </section>


<aside>
  <dl>
  <dt class="headingG01">「コールセンター知っ得情報」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail18">学歴なんて関係ない！コールセンターなら、実力次第で高収入が目指せる</a></li>
  <li><a href="/ccwork/detail39">初心者大歓迎！高時給バイトなら、コールセンターが狙い目</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  
  <aside class="tabNav_ccwork">
  <ul>
  <li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_a.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_a.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li>
<li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li>
  </ul>
  </aside><!-- / tabNav -->
  
  
</section>
