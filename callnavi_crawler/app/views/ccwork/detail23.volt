<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">基礎知識</span></div>
  <h1>日本語を正しく使えていますか？<br />コールセンター業務に必要な丁寧語、謙譲語、尊敬語</h1>
  </header>
  
  <section class="sect01">
  <img src="/public/images/ccwork/023/main001.jpg" alt="説明上手になれる？！その秘訣は･･･"  class="imagemargin_T10B10" />
  <p class="marginBottom5px">このお仕事では、お客様とのやりとりは、基本的に電話越しです。</p>
  <p class="marginBottom30px">ということは、声のトーンや言葉遣いのひとつひとつが、お客様にとって相手を信用する基準となります。アウトバウント（架電、電話をかける）でもインバウンド（受電、電話を受ける）でも、どんなお仕事でもまずはお客様に信頼していただかないと始まりません！あらためて基本に立ち返り、敬語とは何か確認しておきましょう！ちなみに敬語とは丁寧語・謙譲語・尊敬語の総称です。</p>

</section>
  
  <section class="sect03">
  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/030/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />ポイント</p>
  <p class="yellowmiddlecircle_titlenumber">1</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">その名の通り、<br />丁寧な言い回しの丁寧語</p>
  </div>  
  <p class="marginBottom10px">丁寧語とは、その名の通り丁寧な言い回しの表現です。語尾に「です」「ます」や「ございます」を付けることによって、相手や内容を問わず使えます。</p>
 <img src="/public/images/ccwork/023/sub_001.jpg" alt="箇条書きにする" />
 <ul class="fukidasi02_graybox">
<li>例：そう→そうです／そうでございます</li>
<li>例：弁当→お弁当　　天気→お天気</li>
<li>例文：そう、だからこの商品はあなたにオススメだよ。</li>
<li>→そうです、ですからこちらの商品はお客様にオススメでございます。</li>
</ul>
  <p class="marginBottom30px">相槌のひとつでも、「そう」「そうですか」と「左様でございますか」ではだいぶ印象が違いますよね。できるだけ丁寧な言い回しをするのか、もしくは「そうですよね」と親身さを出すか。このあたりのバランスは難しいところです。</p>

 
  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/030/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />ポイント</p>
  <p class="yellowmiddlecircle_titlenumber">2</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">自分をへりくだって<br />相手を立てる謙譲語</p>
  </div>
  <p class="marginBottom10px">目上の方に対し、自分をへりくだって表現することによって相手を立てる表現です。</p>
 <img src="/public/images/ccwork/023/sub_002.jpg" alt="箇条書きにする" />
 <ul class="fukidasi02_graybox">
<li>例：行きます→参ります／伺います</li>
<li>例：言います→申します</li>
<li>例文：じゃあ、これから電話番号を言うね。</li>
<li>→それでは、これから電話番号を申し上げます。</li>
</ul>
  <p class="marginBottom30px">おそらく、営業などで一番使われる謙譲語は「～いただく」ではないでしょうか。「私が～させていただきますのでご安心ください」「～していただきたいのですが」など、慣れていないと噛んでしまいそうですが大事な言い回しです。</p>

 
  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/030/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />ポイント</p>
  <p class="yellowmiddlecircle_titlenumber">3</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">目上の方の行動を高める尊敬語</p>
  </div>  
  <p class="marginBottom10px">目上の方に対し、自分をへりくだって表現することによって相手を立てる表現です。</p>
 <img src="/public/images/ccwork/023/sub_003.jpg" alt="箇条書きにする" />
 <ul class="fukidasi02_graybox">
<li>例：行く→いらっしゃる</li>
<li>例：見る→ご覧になる</li>
<li>例文：一度見てみたら、絶対気に入ると思うよ。</li>
<li>→一度ご覧になれば、必ずお気に召すかと存じます。</li>
</ul>
  <p class="marginBottom30px">お客様とのお話の中でよく使うのが「～なさる／くださる」という言い回しです。「ご購入なさいますか？」「ご検討ください」など、お客様の意志確認のさいなどの重要な場面では、丁寧な言葉遣いは必須です。</p>

    <h3 class="blueblockBG">最後に</h3>
 <img src="/public/images/ccwork/023/sub_004.jpg" alt="箇条書きにする" />
  <p class="marginBottom40px">丁寧な言葉遣いも大事ですが、そればかりを意識して、お客様との会話がちぐはぐになってしまっては元も子もありません。少しずつ、不自然にならないよう使っていきましょう。また、堅苦しい言葉遣いよりも、元気で爽やかでテンポのよい会話を好まれるお客様もいらっしゃいます。このあたりはお客様の雰囲気にあわせて臨機応変に対応しましょう！</p>
  </section>
  
  <aside>
  <dl>
  <dt class="headingG01">「コールセンター基礎知識」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail28">「ファミコン敬語」使っていませんか？以外と知らない？！正しい「ビジネス敬語」</a></li>
  <li><a href="/ccwork/detail30">説明上手になれる?!その秘訣は･･･</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  
<aside class="tabNav_ccwork">
<ul><li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_a.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li><li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_a.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li><li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li></ul>
</aside><!-- / tabNav -->
  
  
</section>
