<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">働く人の声</span></div>
  <h1>眠たいとき、どうする？ <br />理想の睡眠時間と業務効率について</h1>
  </header>
  
  <section class="sect01">
    <p class="marginBottom10px">仕事や勉強の最中に猛烈な眠気が襲ってくる…。こんな経験ありませんか？<br />特にデスクワークの多い人は、お昼休憩を挟んだら急に眠くなった、なんてこと多いのではないのでしょうか。</p>
    <p class="marginBottom20px">眠気対策には、ガムを噛む・ツボを刺激する・コーヒーをたくさん飲んでカフェインを摂取する・軽い運動をするなど方法はたくさんありますが、職場環境によっては、なかなかできなかったりしますよね。やはり昼間の眠気を解消して仕事の効率を上げていくためには、<span class="Blue_text">夜の睡眠の取り方を工夫するのが効果的</span>と言われています。<br />そこで今回は、これを知っておけば、仕事の効率も３倍アップ！？<span class="Blue_text">睡眠の質を改善して毎日気持ちよく過ごせる方法</span>をご紹介します。</p>
    <img src="/public/images/ccwork/063/main001.jpg" class="imagemargin_T10B10" alt="睡眠の質を改善して毎日気持ちよく過ごせる方法をご紹介" />

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">睡眠時間が短い人必見!!<br />気持ちよく起きられるルール</h2>
    <p class="marginBottom10px">日本の社会人の1日の平均睡眠時間は、<span class="Blue_text">６時間から７時間</span>と言われています。しかし、中には忙しくてそんなに睡眠時間が取れないという人、逆に、それ以上睡眠時間を取っているのに、なかなか朝起きられないという悩みを抱えている方もいるようです。<br /><span class="Blue_text">問題は睡眠時間にあるのでしょうか。</span>実は、ただ睡眠時間を伸ばせば良いかというとそうでもなく、人間にはすっきり起きられる時間帯があるのをご存じでしたか？</p>
    <p class="marginBottom20px">人間が朝すっきりと目を覚ます時間は入眠時間から数えて、<span class="Blue_text">90分ごと</span>だと言われているのです。ただし、入眠後3時間は成長ホルモンを分泌したり、疲れを取ったりする時間なので最低3時間は確保するようにしましょう。3時間から数えて90分ずつあとの4時間30分、6時間、7時間30分・・・と計算して目覚ましをかけてみてはいかがでしょうか。</p>

    <h3 class="blueblockBG">ベストな時間帯は0時～6時</h3>
  <p class="marginBottom20px">睡眠には深い眠りのノンレム睡眠と浅い眠りで夢を見るレム睡眠の2種類があると言われており、深い眠りのノンレム睡眠は入眠から3時間に集中的に出やすいと言われています。<br />一方、レム睡眠が出やすいと言われている時間帯は朝方午前３時から午前６時と言われています。そこで自分の体内のレム睡眠をこの時間帯に合わせる、つまり3時間以上の睡眠を取っていることが望ましいわけです。<br />したがって、深夜0時から午前6時まで寝た6時間と、レム睡眠が出やすい時間帯の午前3時に寝て午前9時に起きる6時間では睡眠の質が変わってしまいます。0時から6時ぴったりは無理でも大きくずれないようにすると良いでしょう。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">睡眠時間が足りないと起きてくる<br />身体の不調あれこれ</h2>
    <img src="/public/images/ccwork/063/sub_001.jpg" class="imagemargin_T10B10" alt="睡眠時間が足りないと起きてくる身体の不調あれこれ" />
  <p class="marginBottom10px">睡眠時間が足りないと体のあちこちに不調が現れてきます。<br />まず一つ目は、<span class="Blue_text">肥満</span>になりやすいということです。睡眠時間8時間以上の人と5時間に満たない人では、後者の方が食べる量が多いというデータもあり、<span class="Blue_text">睡眠時間が少ないと満腹中枢が正常な働きができなくなる</span>とも言われています。</p>
  <p class="marginBottom10px">また、睡眠が少ない人は心拍数が上昇し、<span class="Blue_text">高血圧</span>になりやすいとも言われています。そして、一番弊害をもたらすのが<span class="Blue_text">集中力</span>だと言われています。仕事や勉強時に限らず、運転中など日常生活で思わぬ事故にもつながるので気をつけましょう。<br />また、<span class="Blue_text">睡眠不足には86もの病気に繋がる恐れがある</span>とも言われています。身体的な症状だけでなく、気持ちの面でも落ち込みやすくなるなど様々な弊害がもたらされると言われています。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">睡眠の質をあげるポイント</h2>
  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg"/>ポイント</p>
  <p class="yellowmiddlecircle_titlenumber">1</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">起きる時間を一定にする</p>
  </div>  
  <p class="marginB20 sectborder">良い睡眠には、<span class="Blue_text">熟睡</span>がポイント。熟睡するためには、ただ睡眠時間を伸ばせば良いというわけではありません。睡眠の度合いを高めるには、<span class="Blue_text">一定の時間に起きる</span>ことが効果的だとも言われています。</p>

  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg"/>ポイント</p>
  <p class="yellowmiddlecircle_titlenumber">2</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">朝日を浴びる</p>
  </div>  
  <p class="marginB20">決まった時間に起きたら、<span class="Blue_text">さらに決まった時間に朝日を浴びる</span>と目覚めの効果も倍増です。太陽の光を浴びることは、体内にある様々な体内時計をリセットしてくれる働きがあると言われています。</p>
  <div class="freebox01_blueBG_wrapper">
  <p class="freebox01_blueBG_title01">ワンポイント・アドバイス</p>
 <p class="freebox01_blueBG_title02">どうしても眠いときは</p>
  <p>それでも眠くなってしまう…もう駄目だ！という緊急事態にオススメなのがこちら。<br /><span class="Blue_text">■15分の仮眠</span><br />思い切って寝てしまうのも方法のひとつです。ただし、時間は15分以内です。15分以上になると深く眠ってしまい、起きづらくなると言われています。浅い仮眠をとれば、頭も冴えて、すっきりした気分で仕事もはかどるかもしれません。</p>
  <p><span class="Blue_text">■冷たい飲み物やアイスクリームでリフレッシュ</span><br />人は体温が下がると眠気を感じるようになっています。夜、お風呂に入ってしばらくした後に眠くなるのは体が温まったあとに体温がだんだんと下がっていくためです。<br />その逆で、冷たい飲み物やアイスクリームを体内に入れると、脳は体温が下がったと思い、体温を上げようとします。体温の低下が止まれば眠気も和らぐということです。</p>
  </div>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">重要な仕事は午前中に片付けよう</h2>
  <p class="marginBottom10px">朝の時間帯は最も頭が働き、クリエイティブなアイデアも浮かびやすいと言われています。重要だと思う仕事は頭がすっきりしている<span class="Blue_text">午前中</span>に片付けると良いでしょう。</p>
    <img src="/public/images/ccwork/063/sub_002.jpg" class="imagemargin_T10B10" alt="重要な仕事は午前中に片付けよう" />
  <p class="marginBottom40px">毎日すっきりした気持ちで仕事に打ち込むためには、やはり睡眠や目覚めを改善することがポイントなのですね。体調も改善して、ストレスも減れば仕事だけでなく生活のあらゆる場面でうまくいくことが増えるかもしれないです！ぜひ、質の良い睡眠で昼間の眠気を撃退して集中力も仕事の効率も上げて行きましょう！</p>
</section>

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター働く人の声」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail41">まさかの逆効果に！？甘いものの罠。それリフレッシュになっていません！</a></li>
  <li><a href="/ccwork/detail49">肩が凝る？それ、座りっぱなし症候群ではないですか？</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  
  <aside class="tabNav_ccwork">
  <ul id="js-tab01">
  <li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_a.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_a.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li>
  </ul>
  </aside><!-- / tabNav -->
  
  
</section>
