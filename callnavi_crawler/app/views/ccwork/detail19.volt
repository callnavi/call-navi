<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">知っ得情報</span></div>
  <h1>ゲーム以上に仕事が楽しい！？<br />楽しく仕事をする４つのポイント</h1>
  </header>
  
  <section class="sect01">
  <img src="/public/images/ccwork/019/main001.jpg" alt="「ストレスに負けない！」コールセンターバイト" class="marginBottom20px" />
  <p class="marginBottom20px">休みが終わってしまう日曜日の夜…。「また1週間が始まる…」と思うと会社に行くのが、おっくうになったりする方も多いのではないでしょうか。<span class="Blue_text">ちょっとした工夫で、働くことが楽しくなります。</span>今回は、いくつかの方法を紹介します。</p>
</section>
  
  <section class="sect02">
  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />ポイント</p>
  <p class="yellowmiddlecircle_titlenumber">1</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">マイナス発言をしない</p>
  </div>  
  <p class="marginB20 sectborder">現状、仕事がつまらないのに、マイナスな発言をしない！というのは難しいかもしれません。人生のうち多くの時間を過ごしているのは職場です。そんな多くの時間を費やしているのに、マイナスなことばかりでは、心のゆとりもなくなっていきます。<span class="Blue_text">マイナスな言葉を使わないようにするだけでも、マイナスに浸ることが減っていきます。</span>たとえ自信がなかったとしても「きっと大丈夫！」と思えば、物事がうまくいきやすくなったり、成功しやすくなったりすることもあります。</p>

  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />ポイント</p>
  <p class="yellowmiddlecircle_titlenumber">2</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">ゲーム感覚で楽しんでみる</p>
  </div>  
<p class="marginB20 sectborder">小さな目標であったとしても、「どのようにしたら、時間短縮し、完了させることができるかだろか？」と考えてみましょう。<span class="Blue_text">ゲームの手法や仕組みを使い、問題解決を行なっていくことを「ゲーミフィケーション」と言います。</span>簡単に言うと「仕事のゲーム化」ということですが、ただ遊ぶということではないのがポイントです。ゲームには、基本的に、目的がしっかりあって、それに向かって先に進んでいく。だからこそ、ゲームは楽しい。<span class="Blue_text">これを仕事に置き換えてみましょう。まずはゲームでいう目的と同じように仕事に目標をたてます。目標をたてて、達成したら、自分にご褒美をあげます。</span>例えば、ちょっと高級なお店で料理を食べるなど、自分が幸せだなと思えることを自分にしてあげましょう。</p>

  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />ポイント</p>
  <p class="yellowmiddlecircle_titlenumber">3</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">新しいことに挑戦する勇気</p>
  </div>  
  <p class="marginB20">いきなり大きなことから始めようとすると、不安になり、なかなか勇気がでないかも知れません。新しいことを学ぶよりも、今持っているもの、今知っていることを使って始めてみましょう。それこそが、新しいことを始めるための最短ルートです。</p>
<p class="marginB20">何もない中で、新しいことを始めるのは不安なものです。本当に結果が出るのか、挫折してしまうかもしれない、そんな恐怖が、行動にできない原因です。</p>
<p class="marginB20 sectborder"><span class="Blue_text">その他のことは、後からどうにでもなることもあります。いずれ結果もついてきます。まずは、「ただ始める」からやってみましょう。</span></p>

  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />ポイント</p>
  <p class="yellowmiddlecircle_titlenumber">4</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">考えて工夫をしてみる</p>
  </div>  
  <p class="marginB20">「作業」として働いてはいませんか？事務的な処理など、面倒でうんざりするような業務もありますが、それは決められたルールに従うだけの「作業」だからです。<span class="Blue_text">「どんな風にすれば効率よくできるだろう」「自分にあったやり方はなんだろう」など創意工夫してみることで仕事は楽しくなります。</span>さらに、このように考えることで、やりがいにつながることもあります。</p>
</section>

  <section class="sect08">
    <h3 class="blueblockBG">最後に</h3>
  <p class="marginBottom40px">楽しい仕事と、つまらない仕事があるわけではありません。すべての仕事は、気持ちの持ちようによって楽しくもなるし、つまらなくもなります。少しでも仕事が楽しくなるよう、今回紹介した内容からチャレンジしてみてはいかがでしょうか。</p>
  </section>

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター知っ得情報」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail31">仕事とプライベートの両立！シフト制は、スケジュール管理の魔法の杖となるのか？</a></li>
  <li><a href="/ccwork/detail24">声で人生が変わる！？　コールセンターで好印象な声を会得する方法</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
<aside class="tabNav_ccwork">
<ul><li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li><li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_a.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_a.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li><li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li></ul>
</aside><!-- / tabNav -->
  
</section>

