<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">知っ得情報</span></div>
  <h1>どうするクレーム？<br />しっかり対応するための魔法の４ステップ！</h1>
  </header>
  
<!--- 段落１ 開始 --->  
  <section class="sect01">
  <img src="/public/images/ccwork/032/main001.jpg" class="imagemargin_T10B10" alt="隠されたドラマがある！？コールセンターの舞台裏！" />
  <p class="marginBottom10px">「大変！クレームになってしまった！お客様が怒っている！」</p>
  <p class="marginBottom10px">まず一番初めに湧き起ってくる感情は「焦りや恐怖」だと思います。今のご時世、どんな業界や職種でも、社会で働く以上クレーム対応は避けて通れない道。できることならば、「クレーム」には関わりたくない…というのが本音なのはわかります。しかしクレームの対応一つで、企業の存続が危うくなってしまう、なんてこともよくある話。</p>
  <p class="marginBottom10px">いざクレームを受けてしまった時にも、自信を持って的確な対応ができるよう、<span class="Blue_text">クレーム対応の正しい知識を身につけておくことが大切</span>と言われています。ここでは、<span class="Blue_text">クレームが発生した際に、一般的に、必要と言われている対処方法についてまとめていきます。</span></p>
  </section>
<!--- 段落１ 終了 --->


<!--- 段落２ 開始 --->    
  <section class="sect02">
  <p class="bluedot">クレームは必ず発生してしまう</p>
  <p class="marginBottom10px">クレームに対する対処法を学ぶ前に、知っておいて欲しいことがあります。まず、<span class="Blue_text">クレームとは「お客様の期待水準」を大きく下回ったときに発生</span>します。その期待を裏切ってしまった部分が、コールセンターのオペレーターによる「サービス」なのか、もしくは販売している「商品」なのかは、その時の状況によりますが、<span class="Blue_text">「期待が裏切られた！」と、お客様が感じたときに生じるのがクレーム</span>だということは、覚えておきましょう。</p>
  <p class="marginBottom10px">また、米国のある調査結果によると、どんな商品やサービスに対しても、購入された直後に40％のお客様が不満を抱き、そのうち4％のお客様が、不満をクレームとして表面化させると言われているそうです。お客様によって、感じ方はそれぞれなので、どんなにいい商品を扱っていても、万人に100％受け入れられるものは存在しない、ということですね。したがって、<span class="Blue_text">クレームを完全になくすということは、非常に難しいことも理解しておくといいかも知れません。</span></p>
</section>
<!--- 段落２ 終了 --->

<!--- 段落３ 開始 --->
  <section class="sect03">
  <h2 class="sideBlueBorder_blueText02 marginBottom10px">クレーム対応の「4 STEP」</h2>
  <img src="/public/images/ccwork/032/sub_001.jpg" class="imagemargin_B25" alt="隠されたドラマがある！？コールセンターの舞台裏！" />
  
  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />STEP</p>
  <p class="yellowmiddlecircle_titlenumber">1</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">お客様の心情の理解とお詫び</p>
  </div>  
  <p class="marginB20">クレームを受けるのは非常にストレスがかかり嫌なことです。しかし<span class="Blue_text">クレームを言うお客様も、購入した商品やサービスで「嫌な思い」をしたために、クレームとして問題点を指摘してきていることを、念頭においた対応がポイント</span>となるでしょう。クレームが言いたくなる<span class="Blue_text">お客様の心情を理解する</span>ことが大切ですね。</p>

    <h3 class="blueblockBG">対応のポイント</h3>
    <ul class="dot_bluetext">
    <li><span>1</span>まずは、お客様のお話を最後まで聞く</li>
    <li><span>2</span>お客様の話に同意をする</li>
    <li><span>3</span>聞き役に徹する</li>
    </ul>

  <p class="marginB20">誤解が生じていたり、きついことを言われたりすることもあるかも知れません。そんな時でもまずは、「さようでございましたか。それは大変不快な思いをさせてしまいまして、誠に申し訳ございませんでした。」と同意と心からのお詫びで、まずは受け止めます。間違っても、お客様の話を遮って「それは誤解です！」など、口を挟んではいけません。2次クレームに繋がってしまいます。ここはぐっと我慢の時です。あなたの忍耐力の訓練となります。<span class="Blue_text">お客様のお話に同意をして、聞き役に徹しましよう。</span></p>
  <p class="marginB20 sectborder">お客様が話したい事をすべて話終えて、内容が相手にしっかり伝わり受け入れてもらえたと感じると、自然とお客様の感情の高ぶりも収まってくるものです。誠意を持って傾聴していることを理解してもらえれば「あなたにこんなに怒ってもしょうがないんだけどね、ごめんね。」など、お詫びの言葉をもらえることも。ですので、まずは<span class="Blue_text">お客様の言い分すべてをしっかり受け止めてから、平常心に戻っていただくことが大切</span>だと言われています。</p>
  </section>
<!--- 段落３ 終了 --->

<!--- 段落４ 開始 --->
  <section class="sect04">
  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />STEP</p>
  <p class="yellowmiddlecircle_titlenumber">２</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">原因や事実の確認</p>
  </div>  
  <p class="marginB20">クレームを解決するには、「何が問題になっているのか、事実をしっかり確認することが大切です。</p>

    <h3 class="blueblockBG">対応のポイント</h3>
    <ul class="dot_bluetext">
    <li><span>1</span>必要なことは、すべてメモをとっておく</li>
    <li><span>2</span>事実関係を最大限把握する</li>
    </ul>
  <p class="blackdot">いつ、どこでトラブルが起こったか</p>
  <p class="blackdot">何が起こって、どんな不満があるのか</p>
  <p class="blackdot">誰が不満を感じているのか</p>
  <p class="blackdot">問題点は何か</p>
  <p class="blackdot marginBottom20px">どのような解決策を望んでいるのか</p>

  <p class="marginB20 sectborder">できるだけ細かく状況を把握しておく必要があります。このような詳細にお答えいただくときには、お客様の怒りが落ち着いた状態の方が好ましいです。そのためには<span class="Blue_text">１ステップ目で、お客様の心情をきちんと理解して、冷静な状態に戻っておいて頂くことがポイント</span>となります。</p>
  </section>
<!--- 段落４ 終了 --->

<!--- 段落５ 開始 --->
  <section class="sect05">
  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />STEP</p>
  <p class="yellowmiddlecircle_titlenumber">３</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">代替案・解決法の提案</p>
  </div>  
  <p class="marginB20 sectborder"><span class="Blue_text">こちらはコールセンターの上司（SV（スーパーバイザー）、リーダー、マネージャなど）に相談し、どんな代替案を提案できるか指示を仰ぎましょう。</span>自分で提案できる場合でも、クレームになっていることを一度上司に相談したほうがいいかと思います。考えていた案より、いい代替案の提案があったり、その他アドバイスをくれたりするかもしれません。クレーム時は迅速な報告が必須です。しかし、このとき長時間保留にして、お客様をお待たせしない方がいいでしょう。<span class="Blue_text">お客様をお待たせできるのは20秒！それ以上かかる場合は折り返しのご連絡にした方が良いと言われています。</span></p>
  </section>
<!--- 段落５ 終了 --->

<!--- 段落６ 開始 --->
  <section class="sect06">
  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />STEP</p>
  <p class="yellowmiddlecircle_titlenumber">４</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">再度のお詫びを</p>
  </div>  
  <p class="marginB20">お客様からの信頼を取り戻すべき、最後にもう一度誠意を持ってお詫びをします。<br /><span class="Blue_text">コールセンター勤務の場合、「声」が命綱。顔やしぐさなどは見えなくても、お客様に対する態度は声に乗って伝わるものです。</span>本当に申し訳ないと感じたら、自然と頭が下がってしまいますよね。それでいいのです。身振り手振りが声の感情を作ります。「声しか聞こえていないからいいや」ではなく、姿勢をきちんと正し、お辞儀をしながの対応が大切と言われています。</p>
  <p class="marginBottom30px">また「感謝」の気持ちを伝えることも大切です。<br />
いくら電話一本とはいえども、クレームをするのにも気力や体力を使います。また、<span class="Blue_text">お客様からのご指摘には、業務改善や商品改善に繋がるヒントがちりばめられていることも多い</span>です。<span class="Blue_text">クレームは必ずすべて内容を上司に伝え、会社の改善に繋がりそうな事柄があれば、社内に共有してもらいましょう。</span>あなたのクレーム対応が会社の未来の発展を創ることもあるのです。</p>
  </section>
<!--- 段落６ 終了 --->

<!--- 段落７ 開始 --->
  <section class="sect08">
    <h3 class="blueblockBG">最後に</h3>
  <p class="marginBottom20px">上記４点、クレーム対応の4stepをお伝え致しました。<br />
中には自分で対応し切れないクレームもあるかも知れません。<span class="Blue_text">これは私では対応できない！！と感じた場合場合は、早めに上司に相談しましょう。</span></p>
  <p class="marginBottom40px">コールセンターの上司は「お客様対応のプロ」ですから、過去にも様々なクレームの対応を行ってきた経験がある場合が多いです。まずは、<span class="Blue_text">きちんと相談して、どのような対応が望ましいのか、アドバイスを受けることが大切ですね。</span></p>
  </section>
<!--- 段落７ 終了 --->

<aside>
  <dl>
  <dt class="headingG01">「コールセンター知っ得情報」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail24">声で人生が変わる！？コールセンターで好印象な声を会得する方法</a></li>
  <li><a href="/ccwork/detail33">テレマから人事部へ！コールセンター業務の経験が、その後の昇進を決める！？</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  
  <aside class="tabNav_ccwork">
  <ul>
  <li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_a.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_a.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li>
<li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li>
  </ul>
  </aside><!-- / tabNav -->
  
  
</section>

