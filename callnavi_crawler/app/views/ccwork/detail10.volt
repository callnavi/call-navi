<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">知っ得情報</span></div>
  <h1>アイデアオフィスで疲れを吹き飛ばそう！</h1>
  </header>

<!--- 段落１ 開始 --->
  <section class="sect01">
  <p class="marginBottom_none">コールセンターは電話をかける設備があれば良い？いやいや、そんなことはありません。
  オペレーターさんが気持ちよく働けて、なおかつ業務効率や、売上を上げていくためには、
  レイアウトや環境づくりなど様々な工夫が大切なんです。
  そこで、仕事の効率もモチベーションもアップするオフィス環境とちょっとした工夫をまとめてみました。
</p>
</section>
<!--- 段落１ 終了 --->

<!--- 段落２ 開始 --->
    <section class="sect02">
  <img src="/public/images/ccwork/005/main001.jpg" alt="コールセンター風景01" class="imagemargin_T0B50" />
  <p class="yellowsmallcircle_Number">1</p>
  <p class="yellowsmallcircle_title">オペレーターにとって快適な環境であること</p>
  <p class="marginB20">まず、一番大切なのは、仕事がしやすい空間であることです。
部屋全体が明るく、開放感のある環境であれば気持ち良く働けますよね。
また、空調や湿度・照明にも配慮して長時間座っていても不快でない環境作りが大切です。
室温は個人差があるため調節が難しいですが、加湿器を使えば湿度とともに多少の温度調整もできますよ。
</p>
  <div class="freebox01_blueBG_wrapper">
  <p class="freebox01_blueBG_title01">ワンポイント・アドバイス</p>
  <p class="freebox01_blueBG_title02">ストレスが緩和できるアロマ法</p>
  <p class="marginBottom_none">特に受電がメインのコールセンターでは、
  お客様からのクレームを多く受けてしまうことも多く、ストレスも溜まりがちに…
  そこで、オフィスに心地よい香りを漂わせることでオペレーターがリラックスして
  仕事に臨むことができるよう工夫している会社もあるのだとか。</p>
  </div>
  </section>  
<!--- 段落２ 終了 --->

<!--- 段落３ 開始 --->  
  <section class="sect03">
  <p class="yellowsmallcircle_Number">2</p>
  <p class="yellowsmallcircle_title">機能性に優れていること</p>
  <p class="marginB20">効率よく仕事が進められるために、
  機能性に優れた作りであることも必須です。例えば、
  SV（スーパーバイザー）・責任者が自分の担当するチームを見渡せ、
  管理しやすいこと。また、必要な資料やちょっと確認したい事項が、
  誰でもすぐわかる場所に完備されていることも大切です。
  プリンタやFAXが自席から近く、交通の便が良いことが望ましいですね。
</p>
   <img src="/public/images/ccwork/005/main002.jpg" alt="コールセンター風景02" class="imagemargin_T0B50" />
  <p class="yellowsmallcircle_Number">3</p>
  <p class="yellowsmallcircle_title">コミュニケーションが取りやすい</p>
  <p class="marginB20">困った時に、ちょっと席を立って、
  他のスタッフやSVの意見を聞きやすいよう交通の便も工夫したレイアウトが
  良いでしょう。<br>
また、ちょっとした打ち合わせができるミーティングスペースがあると、
  研修もできて良いです。</p>
  </section>
<!--- 段落４ 終了 --->

<!--- 段落５ 開始 --->
  <section class="sect05">
  <p class="yellowsmallcircle_Number">4</p>
  <p class="yellowsmallcircle_title">コミュニケーションが取りやすい</p>
  <p class="marginB20">リフレッシュルームがあるのも大切ですね。
  快適な広さでオペレーターがくつろげる環境であることが必須です。
  大型液晶テレビ、リラックスできるソファ、全身マッサージ器具などを
  常備するセンターも増えてきているそうで、中には会員制のバーかクラブかと
  思わせるような間接照明を駆使したり、有線放送を入れてBGMを流したり
  雰囲気も工夫しているセンターが増えてきているそうですよ！
</p>
  </section>
<!--- 段落５ 終了 --->

<!--- 段落６ 開始 --->
  <section class="sect06">
  <p class="yellowsmallcircle_Number">5</p>
  <p class="yellowsmallcircle_title">ONとOFFの切り替えができる空間スペース</p>
  <p class="marginB20">遊び心があるオフィスだと出社するのが楽しくなりますよね。
  コールセンターに限らず、どの仕事でも大切です。
  GoogleやAirbnbのようにオフィスを単なる仕事場ではなく、
  アイデア創出の場と考えてデザインしている企業も増えてきました。
</p>
  <img src="/public/images/ccwork/005/main003.jpg" alt="" class="images_ctl" />
  <img src="/public/images/ccwork/005/main004.jpg" alt="" class="images_ctl" />
  <p class="textalign_right">画像出典元：<a href="http://wired.jp/2014/12/29/airbnb-invents-call-center/" target="_blank">WIRED（PHOTOS BY JEREMY BITTERMANN）</a></p>
  </section>
<!--- 段落６ 終了 --->

<!--- 段落７ 開始 --->
  <section class="sect07">
  <div class="freebox01_blueBG_wrapper">
  <p class="freebox01_blueBG_title01">ワンポイント・アドバイス</p>
  <p class="freebox01_blueBG_title02">自分のデスクをリラックス環境に</p>
  <p>オフィス全体は無理という職場であれば、自分のスペースだけでも
  工夫してみましょう。緊張や疲れをとる色は緑。デスクのスペースに余裕やあれば、
  小さな観葉植物を配置するだけでリラックス効果があり、気持ちが和らぎますよ。</p>
  </div>
  </section>
<!--- 段落７ 終了 --->

<!--- 段落８ 開始 --->
  <section class="sect08">
    <h3 class="blueblockBG">最後に</h3>
  <p class="marginBottom40px">自分の身の回りの環境を少し変えてみるだけでも、働きやすさは結構変化します。
  オペレーターさんやSVさんも、仕事の効率やモチベーションをアップするちょっとした
  工夫をオフィスに取り入れて、気持ち良く働きましょう。</p>
  </section>
<!--- 段落８ 終了 --->

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター知っ得情報」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail05">コールセンターの面接で失敗しないコツ？</a></li>
  <li><a href="/ccwork/detail06">「ストレスに負けない！」コールセンターバイト</a></li>
  <li><a href="/ccwork/detail09">健康の秘訣！知っておきたい体のケア</a></li>
  <li><a href="/ccwork/detail08">ライフスタイルに合った働き方を見つけよう！</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
<aside class="tabNav_ccwork">
<ul><li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li><li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_a.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_a.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li><li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li></ul>
</aside><!-- / tabNav -->
  
</section>