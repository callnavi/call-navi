<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">働く人の声</span></div>
  <h1>肩が凝る？<br />それ、座りっぱなし症候群ではないですか？</h1>
  </header>
  
<!--- 段落１ 開始 --->  
  <section class="sect01">
 <p class="marginBottom10px">コールセンター業務だけではなく、座り仕事に従事していると、気になってくるのが<span class="Blue_text">肩こりや腰の痛みなど体の凝り</span>ですよね。<br />
多くのオフィスワーカーの悩みの種の１つだと思います。</p>
    <img src="/public/images/ccwork/049/main001.jpg" class="imagemargin_T10B10" alt="ガムを噛め！仕事中のリフレッシュは、これに限る！" />

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">「座りっぱなし症候群」とは？</h2>
  <p class="marginBottom10px">突然ですが、あなたは「座りっぱなし症候群」になっていませんか？<br />「座りっぱなし症候群」は、日頃感じる頑固な肩こりや頭痛など、体の様々な不調の原因になっているそうです。</p>
  <p class="marginBottom10px">「座りっぱなし症候群」と聞くと、あまり馴染みがないですが、「エコノミー症候群」と言えば、聞き覚えがあるのではないでしょうか？実は、「座りっぱなし症候群」＝「エコノミー症候群」なのです。飛行機の中でしか、ならない！と錯覚しがちな「エコノミー症候群」ですが、実は普段の生活の中で、たった２時間座りっぱなしの状態にあるだけでなってしまう可能性がある、非常に身近な症状なのです。</p>
  <p class="marginBottom10px">私が以前アルバイトをしていたコールセンターでは、２時間に１度必ず休憩があり、体を動かすことができていました。たった２時間座りっぱなしなだけで、症状が出てくる可能性があるので、意識して「座りっぱなし症候群」の予防をしていきたいですね。</p>
  <p class="marginBottom20px">座りっぱなしで症候群で現れる体の不調とは…</p>
  <ul class="fukidasi02_graybox">
<p class="textCenter_bold">●肩こり　●頭痛　●腰痛　●冷え　●むくみ　●目の疲れ</p>
</ul> 
  <p class="marginBottom20px">複数の症状が思い当たる、という人も多いのではないでしょうか？<br />もしかしたら、その不調、座りっぱなしが原因かもしれません！</p>

  <div class="freebox01_blueBG_wrapper">
  <p class="Blue_textbold">【座りっぱなし症候群かどうか、セルフチェックしてみましょう！】</p>
  <p class="marginBottom_none">□　２時間以上連続で座っている</p>
  <p class="marginBottom_none">□　1日合計5時間以上座っている</p>
  <p class="marginBottom_none">□　1日を通してあまり体を動かさない</p>
  <p class="marginBottom_none">□　水分補給をあまりしていない</p>
  <p class="marginBottom_none">□　お茶やコーヒーをよく飲んでいる</p>
  <p class="marginBottom_none">□　あまりトイレに行かない</p>
  <p class="marginBottom_none">□　体を締め付けるような服装をしている</p>
  <p class="marginBottom_none">□　夕方になると足のむくみや靴がきつくなる</p>
  <p class="marginBottom_none">□　エアコン・暖房をつけている場所に長時間いる</p>
  <p class="marginBottom_none">□　空気の乾燥、肌や唇の乾燥が気になる</p>
  <p class="marginBottom_none">□　昼ごはんもデスクで食べる</p>
  </div>
  <p class="marginBottom10px"><span class="Blue_text">４つ以上当てはまる場合「座りっぱなし症候群」の可能性があるそうです！</span><br />あなたはいくつ該当しましたか？体の不調があると本領が発揮できなくて、つらいですよね。「座りっぱなし症候群」の原因と予防を一緒に探っていきましょう！</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">トイレに行く</h2>
    <h3 class="blueblockBG">血流の悪化</h3>
  <p class="marginBottom10px">長時間座り続け、ほとんど動かず同じ姿勢でいると、脚の静脈の血が流れにくくなり、膝の裏あたりの静脈に「血栓（血の塊）」ができる事が原因です。血栓ができるまでにはならなくても、脚の血流が悪くなることで、全身の血流が滞り、不調があらわれてきます。<br />また、<span class="Blue_text">脚を組む習慣のある方は要注意！</span>足を組んで、膝の裏の静脈を強く圧迫してしまうことで、血流が心臓に戻らず体の下部に溜まってしまいます。</p>

    <h3 class="blueblockBG">脱水</h3>
  <p class="marginBottom10px">座りっぱなしの状態でも、汗や蒸気によって体の水分は失われています。ほとんど動かない状態でいると、喉が渇いたことに気づきにくく、<span class="Blue_text">無意識のうちに脱水が起こっています。</span><br />人間は60％以上が水分で成り立っており、脱水は体の機能を低下させてしまいます。</p>
  <p class="marginBottom10px">上記２つの症状は、<span class="Blue_text">肩こり・腰痛・むくみ</span>などを引き起こしてしまうのです。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">座りっぱなし症候群の予防の方法は？</h2>
    <h3 class="blueblockBG">脱水症状を防ぐため、水分補給をしっかりと！</h3>
    <ul class="dot_bluetext">
    <li><span>1</span>お茶やコーヒーは水分補給にならない！？</li>
    </ul>
  <p class="marginBottom10px">コーヒーやお茶に多く含まれているカフェインは、腎臓の血管を拡張する働きがあります。腎臓は血液を濾過して老廃物や塩分を尿として体内に排出しますが、カフェインによって腎臓の血管が拡張することで血液量が増し、尿の生産も増やすことになります。<br />つまり、<span class="Blue_text">多くの水分が尿として排出されてしまうため、水分補給のための飲料としては適してない</span>のです。なんとも皮肉な結果ですが、オフィスでよく飲まれる飲料のＮＯ．１はお茶類とのことでした。私もオフィスではお茶派の１人です。</p>
  <p class="marginBottom10px sectborder">毎日オフィスにお茶を持参し、ちょっと一息つきたいときはコーヒーを飲んでいます。この行動が「座りっぱなし症候群」に拍車をかけていたのですね…。</p>

    <ul class="dot_bluetext">
    <li><span>2</span>効率よく水分補給のできる飲み物は！？</li>
    </ul>
  <p class="marginBottom10px">ズバリ！「電解質」という物質を含むイオン飲料水だそうです！ミネラルウォーターとイオン飲料水を比較したときに、イオン飲料の方が、尿として排出される量が少なく、長時間体に留まり続けることがわかっています。<br />また、血をサラサラに保つという面でも、イオン飲料が効果的という結果も残されています。ただし、イオン飲料は砂糖も多く入っているので、飲みすぎには注意が必要です！</p>

    <h3 class="blueblockBG">こまめに足を動かそう！</h3>
  <p class="marginBottom10px sectborder">コールセンターで電話をしているとき、オフィスワークでパソコンのキーボードを打ちながら、<span class="Blue_text">サッと簡単にできる脚のストレッチ</span>をご紹介します。こまめにストレッチをして、脚の血流を滞らせることのないよう、意識していきましょうね！<br />これで、「座りっぱなし症候群」とも、憎きむくみともおさらばです！</p>

    <ul class="dot_bluetext">
    <li><span>1</span>ふくらはぎの血流をよくするストレッチ</li>
    </ul>
  <p class="marginBottom10px">靴を脱ぐことが可能であればはだしで行うことが望ましいです。</p>
  <p class="text_bold">●まず両足を伸ばして、床と平行になる高さまで持ち上げます。</p>
  <p class="text_bold">●両足のつま先を天井に向け、そらします。ふくらはぎに心地よい刺激を感じながら、10秒間キープします。</p>
  <p class="text_bold">●次に足の付け根から足先までを一直線にするイメージで、つま先も床と平行に伸ばします。足の甲に刺激を感じながら、10秒間キープします。</p>
  <p class="text_bold sectborder">●両足同時に、足は床と平行を保ったまま、両つま先を時計回り、反時計回りに５回ずつぐるぐると回して、足先に溜まった血流を流していきます。</p>

    <ul class="dot_bluetext">
    <li><span>2</span>「逆ハの字」つま先ストレッチ</li>
    </ul>
  <p class="marginBottom10px">上記の①と少し似ているのですが、このストレッチは<span class="Blue_text">足全体の血流が良くなる</span>感じがするので、個人的には非常におススメです！</p>
  <p class="text_bold">●両足を前に伸ばして、つま先をハの字の逆にする（逆ハの字）そのまま3秒程キープ。</p>
  <p class="text_bold">●逆ハの字をハの字になるように戻して、３秒程キープ。</p>
  <p class="text_bold sectborder">●この作業を５回位、お好みの回数で繰り返す</p>

    <ul class="dot_bluetext">
    <li><span>3</span>足裏から血流を改善！テニスボールストレッチ！</li>
    </ul>
  <p class="marginBottom10px">青竹や足裏を刺激するグッツを購入するというのも、１つの手ではありますが、もっと安くて効果的なのが、<span class="Blue_text">テニスボール</span>を使ったストレッチです。こちらは座って行っても効果がありますますが、立って行った方がより血流を促進できます。</p>
  <p class="text_bold">●テニスボールを１つ、片方の足の裏で踏みつぶし、膝を伸ばして立つ。</p>
  <p class="text_bold">●足の裏で、ボールをまんべんなく転がし、かかとで踏みながら10～20秒キープ。</p>
  <p class="text_bold">●土踏まずで踏みながら、10～20秒キープ。</p>
  <p class="text_bold">●足指の付け根で踏みながら、10～20秒キープ</p>
  <p class="text_bold marginBottom40px">●もう一度足の裏をまんべんなく転がす</p>

  <p class="marginBottom40px">いかがでしたか？<br />適度なストレッチと細めな水分補給で、お仕事の快適度も変わってくるのですね！ちょっとした一工夫で、次の日の疲れもぐーんと減少するはず！<br />コールセンターでは、上記のことも考慮して、頻繁に休憩を挟む職場が多くなっています。オフィスで働いているあなたは、自分でコントロールしていかなくてはいけません。<br />仕事に夢中になるのも素敵ですが、ほどほどに休憩を取って、自身の体をいたわってあげましょうね♪</p>
</section>
<!--- 段落１ 終了 ---> 
  

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター働く人の声」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail32">どうするクレーム？しっかり対応するための魔法の４ステップ！</a></li>
  <li><a href="/ccwork/detail19">ゲーム以上に仕事が楽しい！？楽しく仕事をする４つのポイント</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  
  <aside class="tabNav_ccwork">
  <ul id="js-tab01">
  <li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_a.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_a.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li>
  </ul>
  </aside><!-- / tabNav -->
  
  
</section>

