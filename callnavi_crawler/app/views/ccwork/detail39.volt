<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">働く人の声</span></div>
  <h1>初心者大歓迎！<br />高時給バイトなら、コールセンターが狙い目</h1>
  </header>
  
<!--- 段落１ 開始 --->  
  <section class="sect01">
  <p class="marginBottom10px">アルバイトの中には、時給700円から800円程度の仕事から、時給2000円を超える仕事まで、色々なものが存在します。今回は、この中でも、高時給と言われる時給1500円以上のバイトについて、紹介していきます。</p>
  <h2 class="sideBlueBorder_blueText02 marginBottom10px">高時給が必要なわけ</h2>
  <p class="marginBottom10px">まず、なぜ高時給が必要なのでしょうか？もちろん、お給料は高いに越したことはないのですが、まずは高い時給（給料）が欲しい理由についてまとめました。</p>

    <img src="/public/images/ccwork/039/main001.jpg" class="imagemargin_T10B10" alt="初心者大歓迎！高時給バイトなら、コールセンターが狙い目" />
  </section>
<!--- 段落１ 終了 --->  

<!--- 段落２ 開始 --->  
  <section class="sect02">

  <p class="dot_yellowtext_Number">1</p>
  <p class="dot_yellowtext_title">お金が必要！</p>
  <p class="marginB20">これは、本当に切実な問題で、お金がすぐに必要な方がいらっしゃいます。正社員で働いていたけど、会社が倒産した、又はリストラなどで解雇されて、家族を養っていく生活費を稼がなければならない。あるいは、住宅や車、その他ローンを抱えていて、この返済をしなければならない、など理由は様々です。</p>
  <p class="marginB20">最近ですと、ひとり暮らしの学生さんの方でも、両親からの仕送りだけでは足りず、アルバイトで生活費をまかなっている方もいらっしゃるようです。学業の時間も確保するため、短時間で効率よく稼げる高時給のバイトに人気が集まるのもこんなところからです。</p>
  <p class="marginB20">他には、歌手・ミュージシャン・芸人・漫画家・弁護士など、夢を追いかけ中の方が、当面の生活費を賄うために、このようなバイトをされるケースも多いです。私の中学時代の塾の先生は、弁護士の国家試験にチャレンジしている方でした。</p>

  <p class="dot_yellowtext_Number">２</p>
  <p class="dot_yellowtext_title">お金が欲しい！</p>
  <p class="marginB20">ただ単にお金が欲しいから、という方もいらっしゃいます。海外旅行や留学に行きたいから・エステに行きたいから・おしゃれな洋服を買いたいからなど、動機は人それぞれのようです。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">高時給バイトって？</h2>
  <p class="marginBottom10px">時給が高いバイトを求人情報サイトなどで見てみると、<span class="Blue_text">体力が必要な仕事・不規則な時間や長時間勤務の仕事・特殊な仕事・経験や専門スキルが必要な仕事</span>そして、<span class="Blue_text">成果報酬やインセンティブがある仕事</span>に分類されます。</p>
    <img src="/public/images/ccwork/039/sub_001.jpg" class="imagemargin_T10B10" alt="初心者大歓迎！高時給バイトなら、コールセンターが狙い目" />

  <p class="dot_yellowtext_Number">1</p>
  <p class="dot_yellowtext_title">体力が必要な仕事</p>
  <p class="marginB20 sectborder">肉体労働がこれにあたります。建築現場などの工事現場や引越しなど、とにかく体力が必要な仕事です。運動部など体育会系の方に向いている仕事ですね。</p>

  <p class="dot_yellowtext_Number">２</p>
  <p class="dot_yellowtext_title">時間的に大変な仕事</p>
  <p class="marginB20 sectborder">夜中のガードマンや夜中のコンビニなど、通常は就寝していたり休んだりしている時間に働く仕事です。昼間時間を有効に使えるメリットがありますが、昼夜が逆転するなど体調を崩しやすくなってしまうので、健康管理には気をつけることが大切ですね。</p>

  <p class="dot_yellowtext_Number">３</p>
  <p class="dot_yellowtext_title">時間的に大変な仕事</p>
  <p class="marginB20 sectborder">ゴミ収集・清掃・パチンコ店員などの他に、水商売やフロアレディなどのナイト系の仕事があります。これは誰でも向いている仕事とは言い難いですが、このような仕事が楽しんでできる方には、向いているのかも知れませんね。</p>

  <p class="dot_yellowtext_Number">４</p>
  <p class="dot_yellowtext_title">経験・専門スキルが必要な仕事</p>
  <p class="marginB20 sectborder">家庭教師・パソコンインストラクターの他に、イベントコンパニオン・司会・ウグイス嬢・モデル・受付（レセプション）などがあります。大学生の方には家庭教師が人気ですね。あとは自分のスキルや、将来就きたい仕事に近いものがあれば、積極的にチャレンジしていくのもいいでしょう。私の学生時代の経験としては、KDDI国際電話の同時通訳という仕事をしていました。終電後までの勤務でしたが、アルバイトでありながら就業後はタクシーで送ってくれるなど、他にはないメリットがありました。英語が得意な方にはおすすめかも知れません。</p>

  <p class="dot_yellowtext_Number">５</p>
  <p class="dot_yellowtext_title">成果報酬・インセンティブがある仕事</p>
  <p class="marginB20">携帯電話の販売スタッフなど営業系の仕事が多いようです。基本的な時給はそんなに高くなくても、インセンティブや歩合制で高い報酬が得られる場合があります。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">なぜ高時給なの？</h2>
  <p class="marginBottom10px">時給が高い仕事については上に書いたとおりですが、これらの仕事でなぜ高い時給がもらえるのか、まとめてみましょう。</p>
    <img src="/public/images/ccwork/039/sub_002.jpg" class="imagemargin_T10B10" alt="初心者大歓迎！高時給バイトなら、コールセンターが狙い目" />

  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />ポイント</p>
  <p class="yellowmiddlecircle_titlenumber">1</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">客単価が高い</p>
  </div>  
  <p class="marginB20 sectborder">これは、非常に大事です。そのビジネス自体である程度以上のお金が回っていることが大切です。駄菓子屋の店員よりも、高級ブティックの店員さんのほうが高い給料がもらえるのは、このためです。<br />携帯電話の販売については少し特殊で、お客様から頂けるお金は少なくても、携帯キャリア会社から販売手数料がもらえる仕組みになっています。お客様にとっての負担が小さいながら、高報酬を狙える仕事として比較的入りやすい仕事ではないかと思われます。</p>

  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />ポイント</p>
  <p class="yellowmiddlecircle_titlenumber">2</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">誰もができる仕事ではない</p>
  </div>  
  <p class="marginB20 sectborder">難関大学の受験対策を行う家庭教師は誰でもができるわけではありません。結婚式などのイベントの司会をするにも、それ相当のスキルや経験・センスが必要になります。これは、アルバイト・社員・派遣には関係なく、一定以上の品質を求められる仕事についてまわるものですね。</p>

  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />ポイント</p>
  <p class="yellowmiddlecircle_titlenumber">3</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">誰もがやりたがる仕事ではない</p>
  </div>  
  <p class="marginB20 sectborder">そんなに難易度が高くなくても、体力的・時間的にハードだったり、人によっては精神的なストレスを感じたりするような仕事の場合があります。求人広告を出しても、応募してくる人が少な少なかったり、人手不足な場合などは時給が上がっていく傾向にあります。</p>

  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />ポイント</p>
  <p class="yellowmiddlecircle_titlenumber">４</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">インセンティブがある</p>
  </div>  
  <p class="marginB20 sectborder">これは、営業成績がいい！など、会社に貢献している人により多くの給料を払うように考えられたシステムで、結果をだせばその分だけボーナスのような形で跳ね返ってくるのが特徴です。固定時給や最低時給にこだわる方は別ですが、がんばり次第でたくさん稼げる方が良い方には向いている仕事と言えるでしょう。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">コールセンターが高時給なわけ</h2>
    <img src="/public/images/ccwork/039/sub_003.jpg" class="imagemargin_T10B10" alt="初心者大歓迎！高時給バイトなら、コールセンターが狙い目" />
  <p class="marginBottom10px">高時給について、一般的な話を書いてきましたがコールセンター業務で高時給がもらえる理由は、<span class="Blue_text">急にやめてしまう人も少なくないので、急ぎで人を集めるために時給を高く設定している（向き・不向きがある）</span>ことや、<span class="Blue_text">電話で聞いたことをパソコンに入力していく、ハイクオリティなタイピングを要求される（高いスキルが要求される）</span>ことなどでが考えられます。アウトバウンドの場合はそれプラス、上記の<span class="Blue_text">インセンティブ</span>があるからです。たくさん電話営業をしてたくさん成果を上げている方には、それだけ多くの給料を払う。テレアポでもテレマでも、アウトバウンドの場合もとにかく数をたくさんこなす仕事なので、他の仕事と比べて比較的成果を上げやすくなってくるのが特徴です。</p>
  <p class="marginBottom10px">また、コールセンターは外回りの営業と違って、エアコンが効いた環境で仕事をします。休憩スペースが充実していたり、自己申告制のシフトで自分のライフスタイルに合わせて働けたり、髪型や服装が自由など、お給料以外のメリットも大きいのが特徴です。人によってはブラインドタッチができるようになった方もいらっしゃるようです。
</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">コールセンター業務で高時給をもらうためには？</h2>
    <img src="/public/images/ccwork/039/sub_004.jpg" class="imagemargin_T10B10" alt="初心者大歓迎！高時給バイトなら、コールセンターが狙い目" />
  <p class="marginBottom10px">最後に、コールセンター業務で高時給をもらうためのポイントをまとめておきます。
各コールセンターには、マニュアルのようなものがあって成果を上げるための方法が確立されている場合が多いです。スクリプトと呼ばれる台本があったり、ロールプレイングと言われる会話やコミュニケーションの練習方法があったりと研修やスキルアップの環境も充実していることが多いです。ですからまずは、そのマニュアルや王道のやり方に従って真面目にやって行きましょう。最初からうまくいくことは難しいですが、結果を出している先輩社員やアルバイトの方にアドバイスを受けていけば大丈夫です！あとはとにかく数をたくさんこなすことです。コールセンターの仕事が普通の営業と違うのは徹底的に数をこなせることです。最初はうまく話せなくても、一日に何十人ものお客様とお話をしていけば自然と会話力は上達していくものです。</p>
  <p class="marginBottom10px">コールセンターの最大の魅力は、上記のように初心者の方であっても高時給を狙っていくための環境が整っていることですので、まずは、この部分を最大限に活用していきましょう！
</p>






</section>
<!--- 段落２ 終了 --->  
  

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター働く人の声」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail32">どうするクレーム？しっかり対応するための魔法の４ステップ！</a></li>
  <li><a href="/ccwork/detail51">営業経験が全くないのですが、コールセンターの仕事はできますか？</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  
  <aside class="tabNav_ccwork">
  <ul id="js-tab01">
  <li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_a.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_a.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li>
  </ul>
  </aside><!-- / tabNav -->
  
  
</section>

