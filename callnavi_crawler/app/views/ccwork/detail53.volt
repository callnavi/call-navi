<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">知っ得情報</span></div>
  <h1>コールセンターで生かせる資格「コン検」！</h1>
  </header>
  
  <section class="sect01">
  <p class="marginBottom10px">ビジネスマナー検定、パソコン検定、秘書検定・・・世の中には数えきれないほどの資格や検定が存在します。仕事をしていく上で、資格があると、心強いですし自分の自信にもつながりますよね！そんな中、コールセンターで働くアポインターさんにも資格があるのをご存じですか？今回は、アポインターさんの、お客様サービスのスキルを証明し、実践に役立てるための検定"コン検"について調べてみました。 </p>
  <img src="/public/images/ccwork/053/main001.jpg" class="imagemargin_T10B10" alt="コールセンターで生かせる資格「コン検」！" />
</section>

  <section class="sect02">
  <h2 class="sideBlueBorder_blueText02 marginBottom10px">「コン検」とは・・・</h2>
  <p class="marginBottom10px">コン検とは、<span class="Blue_text">「一般社団法人 日本コンタクトセンター教育検定協会」</span>が運営している、国内初の全国版コンタクトセンター資格認定制度です。ちなみに、”コン検”とは「コンタクトセンター検定」の略。お客様案内のサービスチャネルが増えていることから協会ではコンタクトという言葉を使っています。<br /><span class="text_small">※コンタクトセンターとコールセンターの違いは、<a href="/ccwork/detail11">こちらのページ</a>で紹介してますのでチェックして下さいね。</span></p>
  <p class="marginBottom10px">コン検受験のメリットとしては、「お客様サービスのスキルを証明できる」「高い成果を継続的に上げるための特性を学べるため、実践の現場で役立つ」など実務スキルを証明できることがあげられます。就職活動の自己PRにも役立つでしょう！</p>
</section>

  <section class="sect03">
  <h2 class="sideBlueBorder_blueText02 marginBottom10px">実際どんな問題が出るの？</h2>
  <p class="marginBottom10px">試験内容は、オペレーション資格の<span class="Blue_text">エントリー、オペレーター、スーパーバイザー。</span>プロフェッショナル資格の<span class="Blue_text">コンタクトセンターアーキテクチャ、オペレーションマネジメント、カスタマーサービス</span>の計 6 種に分かれています。</p>
  <p class="marginBottom10px"><span class="Blue_text">■</span><span class="text_bold">資格認定の仕組み</span></p>
  <img src="/public/images/ccwork/053/sub_001.jpg" class="imagemargin_B10" alt="資格認定の仕組み" />
  <p class="marginBottom10px">一番基礎的なものは、「エントリー資格」で、電話等を中心とした非対面のコミュニケーションに必要な知識・スキル、ビジネスマナーの習得を認定します。社会人、未就業者、学生など誰でも受験でき、業務経験を問わないので初めての方は、受けやすいでしょう。出題範囲としては、</p>
    <ul class="dot_bluetext">
    <li><span>1</span>コンタクトセンターについての基礎知識</li>
    <li><span>2</span>マナーと心構え</li>
    <li><span>3</span>お客様対応に必要なスキル</li>
    <li><span>4</span>お客様対応に必要な基礎知識</li>
    <li><span>5</span>お客様対応を支えるシステムとマネジメント</li>
    </ul>
  <p class="marginBottom10px">となっておいます。<span class="text_small">（平成27年5月現在）</span><br />こちらの試験はコンピューター上で実施するCBT（Computer Based Testing）形式で、全国の試験会場で随時実施しています。受験料は3,780円です。<span class="text_small">（平成27年5月現在）</span></p>
  <p class="marginBottom10px">「エントリー資格」より上のオペレーター資格になると業務経験が1年～3年程度、スーパーバイザー資格は３年以上など、業務経験が問われるものになってきます。また、プロフェッショナルレベルの資格は5年以上の業務経験が必要となります。プロフェッショナルレベルでは、コンタクトセンターの新規構築や新システム導入時の業務設計に必要な知識やスキルが習得できるコンタクトセンターアーキテクチャ資格など、細部にわたる管理能力を身に付けられる試験になっています。</p>
  <p class="marginBottom10px">こう見ると、一口に「コールセンター業務」と言っても、業務に関する知識も、管理・マネージメントの範囲も多岐に渡ることがわかり、追求すればするほど仕事の幅が広がっていくことがわかります！</p>
</section>

  <section class="sect04">
  <h2 class="sideBlueBorder_blueText02 marginBottom20px">勉強方法は？</h2>
  <img src="/public/images/ccwork/053/sub_002.jpg" class="imagemargin_B10" alt="勉強方法は？" />
  <p class="marginBottom10px">では、試験勉強はどのようにしたら良いの？まず、コン検用に日本コンタクトセンター教育検定協会から公式のテキストが発売されています。<br />また、同協会や一般社団法人日本コールセンター協会などの団体が主催しているセミナーやイベントに参加してみるのも良いでしょう。もちろん、すでにコールセンターで働いている人は今までの経験を活かせるでしょう。</p>
</section>

  <section class="sect05">
  <h2 class="sideBlueBorder_blueText02 marginBottom20px">ビジネスで生かせる資格いろいろ</h2>
  <img src="/public/images/ccwork/053/sub_003.jpg" class="imagemargin_B10" alt="ビジネスで生かせる資格いろいろ" />
  <p class="marginBottom40px">他にもコールセンターをはじめビジネスに生かせる資格は、たくさんあります。<br />文部科学省が後援しているビジネス系検定もその一種。<br />職場でのマナーやコミュニケーションスキルを証明するビジネス実務マナー検定や電話対応の基本、感じの良い電話対応術などが身に付くビジネス電話検定などがあります。資格を取得して、エキスパートとしての知識を身に付ければ、職場で、生かせるだけでなく、自信もつきますね！コールセンターで頑張る方も、これからコールセンターで働きたいとお考えの方もコン検を始め、業務に生かせる様々な資格にチャレンジしてみてはいかがでしょうか。</p>
  </section>


<aside>
  <dl>
  <dt class="headingG01">「コールセンター知っ得情報」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail18">学歴なんて関係ない！コールセンターなら、実力次第で高収入が目指せる</a></li>
  <li><a href="/ccwork/detail50">あなたはどっち？SV転向、アポインター継続</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  
  <aside class="tabNav_ccwork">
  <ul>
  <li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_a.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_a.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li>
<li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li>
  </ul>
  </aside><!-- / tabNav -->
  
  
</section>
