<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">知っ得情報</span></div>
  <h1>タダでディズニーランド！？<br />実際にあったコールセンターでの豪華賞品！</h1>
  </header>

<section class="sect01">
 <p class="marginBottom20px">コールセンターは高時給、高インセンティブで有名ですよね。実は、コールセンターのメリットは、報酬が良いだけではないのです。</p>
 <p class="marginBottom20px">
営業の成績によって「豪華賞品プレゼント！」を用意しているコールセンターが多数あります！こちらの賞品プレゼントのシステムは、「発信系（アウトバウンド）」のコールセンターで導入されていることが多いです。</p>
 <img src="/public/images/ccwork/070/main001.jpg" alt="タダでディズニーランド！？　実際にあったコールセンターでの豪華賞品！"  class="marginBottom20px"/>
<p class="marginBottom40px">本日は、実際に私が勤めていたコールセンターでプレゼントされていた豪華賞品をご紹介します！</p>
<h2 class="sideBlueBorder_blueText02 marginBottom20px">私の勤めていたコールセンターでプレゼントされて
いた商品一例</h2>
  <img src="/public/images/ccwork/070/sub_001.jpg" alt="タダでディズニーランド！？　実際にあったコールセンターでの豪華賞品！"  class="marginBottom20px"/>
 <p class="marginBottom20px">本日は、実際に私が勤めていたコールセンターでプレゼントされていた豪華賞品をご紹介します！</p>
 <ul class="fukidasi02_graybox">
<li><span class="Blue_Btextbold">●</span> くつ</li>
<li><span class="Blue_Btextbold">●</span>  ネクタイ（有名ブランド物）</li>
<li><span class="Blue_Btextbold">●</span>  名刺入れ（有名ブランド物）</li>
<li><span class="Blue_Btextbold">●</span>  松坂牛</li>
<li><span class="Blue_Btextbold">●</span>  マッサージ機</li>
<li><span class="Blue_Btextbold">●</span>  美顔器</li>
<li><span class="Blue_Btextbold">●</span>  ルンバ</li>
<li><span class="Blue_Btextbold">●</span>  商品券</li>
<li><span class="Blue_Btextbold">●</span>  ゲーム機</li>
<li><span class="Blue_Btextbold">●</span>  野球鑑賞券</li>
</ul>   
<p class="marginBottom20px">想像以上に豪勢ですよね！？私も入社当初はそのバラエティの大さに驚きました。これだけあればなにかしら欲しいものがありますよね。私の同僚は、冷蔵庫をもらっていた例もあります。

実際にどのようにして賞品をもらったのか、
コールセンター勤務で賞品をもらった経験のある３名に聞いてみました！</p>
<div class="freebox01_pinkBG_wrapper">
<p class="freebox01_pinkBG_title01">No.1</p><p class="freebox01_pinkBG_title02">女性（27歳）</p>
<p class="text_bold">賞品：パソコン（５万円程度）/ DS（１万５千円程度）</p>
<p>コールセンターに入社して1ヶ月目のころ、「新人」の中で１位の成績を納めました。その時に賞品としてプレゼントされたのが、ノートパソコンでした。おそらく５万円相当位のものだと思います。新人の中で１位といえども、まさかそんなに高価なものをもらえるとは思っていなかったので、非常にびっくりしたのを覚えています。１ヶ月目から、パソコンをもらった私は、モチベーションもＭＡＸに！次の月も好成績を残し、なんとＤＳをゲットしました！完全に物に釣られていますよね（笑）しかしこの制度が私には合っていたようで、初めは「来月もいい成績を残して賞品もらうぞ！」という感覚で仕事に励んでいたのですが、営業手法や、扱っている商材について学びを進めていくうちに、「営業」の奥深さにハマっていきました。賞品に釣られたのはいいきっかけだったなあと思います。もちろん賞品をもらえると嬉しいですが、現在は「営業」という仕事自体を楽しんで毎日の業務に当たっています！</p>
</div>
<div class="freebox01_blueBG_wrapper">
<p class="freebox01_blueBG_title01">No.2</p>
<p class="freebox01_blueBG_title02">男性（20歳）</p>
<p class="text_bold">賞品：プレイステ―ション（４万円程度）</p>
<p>こちらは月初めに「最速で●件取った人がもらえる！」というキャンペーンの賞品でした。この時はちょうどプレイステーションの新しいモデルが売り出されたばかりの頃だったんですよね。発売当時から「プレイステーションの新モデルが欲しい」と思っていた僕にとっては、願ってもないキャンペーンでした。50人程の部署の中から、１番初めに●件の契約を取らなくてはいけない。競合が多い中、「何が何でもプレイステーションをもらう！」という意思を固めて、毎日電話対応を頑張りました。そして見事、プレイステーションを手に入れることができました！目標を持って仕事に臨めたので、充実した１ヶ月でした。</p>
</div>
<div class="freebox01_blueBG_wrapper">
<p class="freebox01_blueBG_title01">No.3</p>
<p class="freebox01_blueBG_title02">男性（23歳）</p>
<p class="text_bold">賞品：オーダーメイドスーツ（５万円程度）</p>
<p>入社して１年の頃、数ヶ月連続で良い成績が取れたさいにいただきました。オーダースーツに憧れはありましたが、敷居が高くて、なかなか自分では手が出せませんよね！しかし、会社がくれるというなら話は別です。生地選びから、色の選定など、こだわりを持って作成しました！出来上がったオーダーメイドスーツは、既製品とは比べ物にならないほど体にフィットし、感動を覚えました。もう一着オーダーメイドスーツが欲しいので、今も連続でいい結果を残せるよう、日々精進しています。</p>
</div>
<div class="freebox01_pinkBG_wrapper">
<p class="freebox01_pinkBG_title01">No.4</p><p class="freebox01_pinkBG_title02">女性（19歳）</p>
<p class="text_bold">賞品：ディズニーランド・ペアチケット（１万５千円程度）</p>
<p>そんなにガツガツ向上心むき出しで頑張った訳ではないのですが、毎日お仕事を自分なりに頑張っていたら、ディズニーランドのペアチケットをもらうことができました。毎月、取れた契約数によってアルバイト先の福利厚生で「ポイント」が溜まっていくのですが、そのポイントが一定数を満たしたとき、このような賞品をもらえます。私のアルバイト先では、ディズニーのペアチケットの他にも、流行りの美顔器や商品券など、様々な賞品が準備されています。</p>
</div>
<p class="marginBottom40px sectborder2">いかがでしたか？<br />みなさん、賞品が良いモチベーションに繋がっているようですね！あなたもコールセンターで働いたら、賞品をもらえるかも！？<br />ビジネススキルやマナーも身に付くコールセンターのお仕事。ぜひ、一度お近くのコールセンターの求人を検索してみませんか？あなたのニーズに合ったコールセンターが見つかるはずです！</p>
</section>

<aside>
  <dl>
  <dt class="headingG01">「コールセンター知っ得情報」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail18">学歴なんて関係ない！コールセンターなら、実力次第で高収入が目指せる</a></li>
  <li><a href="/ccwork/detail39">初心者大歓迎！高時給バイトなら、コールセンターが狙い目</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  
  <aside class="tabNav_ccwork">
  <ul>
  <li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_a.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_a.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li>
<li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li>
  </ul>
  </aside><!-- / tabNav -->
  
  
</section>
