<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">知っ得情報</span></div>
  <h1>社員登用制度を利用して、<br />アルバイトから正社員へレベルアップ！</h1>
  </header>
  
  <section class="sect01">
  <img src="/public/images/ccwork/052/main001.jpg" class="imagemargin_T10B10" alt="社員登用制度を利用して、アルバイトから正社員へレベルアップ！" />
  <p class="marginBottom10px">多くのコールセンターでは、アルバイトからの社員への登用制度を積極的に行っています。その理由と実態について調べてみました。</p>
</section>

  <section class="sect02">
  <h2 class="sideBlueBorder_blueText02 marginBottom10px">実力があれば駆け抜けられる！オープンな登用システム</h2>
  <p class="marginBottom10px">コールセンターのアルバイト求人で、必ずと言っていいほどよく見かける<span class="Blue_text">「社員登用制度あり！」</span>の文字。実際、<span class="Blue_text">コールセンターではとてもよく使われている制度</span>です。その理由はどんなところにあるのでしょうか？</p>
  <img src="/public/images/ccwork/052/sub_001.jpg" class="imagemargin_B25" alt="実力があれば駆け抜けられる！オープンな登用システム" />

  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />Reason</p>
  <p class="yellowmiddlecircle_titlenumber">1</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">現場のノウハウが分かっている人材だから！</p>
  </div>  
  <p class="marginB20">コールセンターの社員は、オペレーター（実際に電話をかける人。アルバイト採用が多い）の管理をする立場であることが多いです。そのためには仕事のノウハウを理解していなければなりません。となると、新入社員にゼロから仕事を教えるよりも、<span class="Blue_text">現場のノウハウが分かっているアルバイトの方を、正社員として採用した方が効率的</span>ですよね。</p>

  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />Reason</p>
  <p class="yellowmiddlecircle_titlenumber">2</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">採用経費の軽減</p>
  </div>  
  <p class="marginB20">企業側からしてみれば、<span class="Blue_text">アルバイトからの登用を増やせば正社員の募集にかける経費が少なくて済む、というメリット</span>があります。アルバイトの採用の方が、履歴書の確認や面接の回数などを考えると、かかる経費は安く済みますしね。逆に<span class="Blue_text">応募する側からすれば、始めから正社員を目指すよりも、アルバイトから正社員への登用を目指す方が、門戸が広く採用されやすいというメリット</span>があります。</p>

</section>

  <section class="sect03">
  <h2 class="sideBlueBorder_blueText02 marginBottom10px">社員登用制度を利用するメリット</h2>
  <p class="marginBottom10px">さて、この制度を利用するメリットは他にどんな点があるのでしょうか？</p>
  <img src="/public/images/ccwork/052/sub_002.jpg" class="imagemargin_B10" alt="社員登用制度を利用するメリット" />
  <div class="freebox01_pinkBG_wrapper marginBottom20px">
  <p class="freebox01_pinkBG_title01">メリットについて</p>
  <p class="marginBottom10px"><span class="Pink_text">■現場を熟知してから、正社員になれる</span><br />いきなり正社員で採用されるより、まずはアルバイトとして入社したほうが、「自分には合わないな」と思った時に次に進みやすいですよね。また、もし社員になったときどのような仕事をしてどんなキャリアがつくれるのか、周りの社員を見てイメージできるという利点もあります。</p>
  <p class="marginBottom10px"><span class="Pink_text">■学歴など過去の経歴は関係なし！実力次第でキャリアがつくれる</span><br />コールセンターのお仕事では、学歴や過去の経歴（職歴）などが昇進に関係することがほとんどありません。高卒で責任者というのも珍しくなく、実力次第でキャリアアップできる可能性があるんです。</p>
  </div>
</section>

  <section class="sect04">
  <h2 class="sideBlueBorder_blueText02 marginBottom20px">実際にアルバイトから社員登用制度で正社員になったＲ・Ｋ(26)さんの場合</h2>
<div class="imageBlockB01">
<p class="image"><span class="bluefukidasi_bluetext">Ｒ・Ｋ(26)さんの場合</span>
      <img src="/public/images/ccwork/052/sub_003.jpg"alt="" class=" image_left" /></p>
<div class="contentsInner">
<div class="bluefukidasi">
  <p class="marginBottom10px">「フリーターとしてコールセンターで働きはじめた理由は、服装が自由ということと時給が良かったから。あくまで稼ぐための手段として働いていたので、キャリアアップしたいとかスキルを磨きたいという思いはなかったです。でも負けず嫌いの性格ということもあって目標をクリアするために日々挑戦していました。その甲斐あって成績もトップクラスに。正社員になってすぐ責任者に抜擢され、現在は部長補佐をさせていただいています。」</p>
</div><!-- / bluefukidasi -->
</div><!-- / contentsInner -->
</div><!-- / imageBlockB01 -->
</section>

  <section class="sect05">
  <p class="marginBottom10px sectborder">フリーターから正社員、責任者へと見事にキャリアアップしていったＲ・Ｋさん。仕事に対する意識が大きく変化し、いまではスタッフのマネジメントを常に考える立場になり、経営者目線で働いています。</p>
  <p class="marginBottom40px">最初のちょっとしたきっかけから、人生はどう進展するのかわかりません！あなたも一度、自分の実力を試してみては？</p>
  </section>


<aside>
  <dl>
  <dt class="headingG01">「コールセンター知っ得情報」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail18">学歴なんて関係ない！コールセンターなら、実力次第で高収入が目指せる</a></li>
  <li><a href="/ccwork/detail51">営業経験が全くないのですが、コールセンターの仕事はできますか？</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  
  <aside class="tabNav_ccwork">
  <ul>
  <li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_a.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_a.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li>
<li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li>
  </ul>
  </aside><!-- / tabNav -->
  
  
</section>

