<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">基礎知識</span></div>
  <h1>話すのが苦手な方へ、テレビショッピングの<br />コールセンターをおすすめする理由！</h1>
  </header>

<!--- 段落１ 開始 --->
  <section class="sect01">
  <img src="/public/images/ccwork/020/main001.jpg" alt="コールセンター風景01" class="imagemargin_T20B20" />
  <p class="marginBottom_none">コールセンターと一言でいっても、アウトバウンド(発信)・インバウンド(受信)の違いから始まり、カスタマーサポートや受注窓口、料金の督促、などさまざまな種類に分けられます。それぞれどんな仕事内容で、どんなお客様をご案内するのでしょうか？<br /><span class="Blue_text">今回はテレビショッピングのコールセンターについてご紹介いたします！</span></p>
</section>
<!--- 段落１ 終了 --->

<!--- 段落３ 開始 --->  
  <section class="sect02">
  <h2 class="sideBlueBorder_blueText02 marginBottom10px">主な業務内容とは？</h2>
  <img src="/public/images/ccwork/020/sub_001.jpg" alt="コールセンター風景01" class="imagemargin_B25" />
  <p class="marginB20">テレビショッピングのコールセンターでの主な業務は、テレビコマーシャルで紹介された商品の申し込み対応です。テレビショッピングの放送時間に合わせて、事前に電話対応や手順を確認し、コマーシャルの終了と同時に受電がスタート！一気にかなりの受電がくるので時間帯にあわせて、オペレーターを多めに待機させるのもテレビショッピングのコールセンターならでは。そのため、短時間勤務の求人募集が多くなります。受電ではお客様の個人情報や注文内容をどんどん聞き取ります。通常のコールセンターでは、パソコンを使ってデータ入力をするところが多いですが、コマーシャル放映後の受電スピードに対応するため、テレビショッピングのコールセンターは、現在でも手書き対応のところが多いそうです。仕事内容としては、比較的単純な作業なので専門知識は特に必要ありませんが、必要に応じて商品知識などを勉強していく必要があります。</p>
  </section>
<!--- 段落４ 終了 --->

<!--- 段落６ 開始 --->
  <section class="sect06">
  <img src="/public/images/ccwork/020/sub_002.jpg" alt="コールセンター風景01" class="imagemargin_B25" />
  <div class="freebox01_blueBG_wrapper marginBottom40px">
  <p class="freebox01_blueBG_title01">こんな方にオススメです！</p>
  <p><span class="Blue_text">★コールセンターのバイトが始めての方</span><br />お客様の情報を聞き取ることが主な業務ですので、「コールセンターで働くのが初めて」という方でも、比較的取組みやすいお仕事内容です。商品知識などが必要になる場合もありますが、「その商品を買いたい」と思って連絡してくださる方がほとんどなので、お客様の購買意欲を高めるというよりは、安心してご購入していただくための意味合いが強いです。</p>
  <p><span class="Blue_text">★聞き上手な方</span><br />テレビでのコマーシャルをご覧になって、電話を下さる方の中にはご高齢の方も多く、住所や電話番号などの個人情報がスムーズに出てこない方や、自分の喋りたいことを、一方的に喋りたがる方もいらっしゃるそう。そんなときには、ひたすら聞き手にまわりつつ、お客様の情報を聞き出すことが求められますので、話すのが得意！という方よりも聞き上手な方にオススメかもしれません。</p>
  </div>
  </section>
<!--- 段落７ 終了 --->

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター基礎知識」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail34">就活の秘訣！コールセンター経験者が就活を有利に進められる理由とは？</a></li>
  <li><a href="/ccwork/detail30">説明上手になれる?!その秘訣は･･･</a></li>
  </dd>
  </dl>
  </aside>
  
<aside class="tabNav_ccwork">
<ul><li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_a.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_a.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li><li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li><li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li></ul>
</aside><!-- / tabNav -->
  
</section>