<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">知っ得情報</span></div>
  <h1>声で人生が変わる！？<br />コールセンターで好印象な声を会得する方法</h1>
  </header>
  
  <section class="sect01">
     <p class="marginBottom20px">コールセンターで働く人にとって、声は命！　声の印象や話し方が良ければ、お客さんも安心して話を聞いてくれて、こちらの言いたいことも、しっかりと伝えることができます。また、普段生活している中でも、「この人の声素敵だな」「聞いていると落ち着くな」と感じる場面は多々ありますよね。心理学上でも、人の印象の4割は声である、と言われているほど声は重要なポイントなのです。また、声は表情以上に気持ちを表すとも言われています。魅力的な声になれば、相手と心地良い関係が作れ、恋もうまくいく！なんてことだって期待できるかも知れません。</p>
    <img src="/public/images/ccwork/024/main001.jpg" alt="" class="imagemargin_none" />
  </section>
  
  <section class="sect02">
  <h2 class="sideBlueBorder_blueText02">好印象を与える声とは？</h2>
     <p class="marginBottom_none">好印象を与える声には、4つのポイントがあります。まずは、「強さのある声」。生命力があって、よく通る声とも言われています。また、「澄んだ声」も好印象です。声には小さな雑音が入ることがあるのですが、声帯をうまく使うことで、澄んだきれいな声が出せるそうです。雰囲気も大事です。「優しさや明るさ、さわやかさを感じさせる声」も好印象を与えますよね。そのほか、落ち着いた余裕のある空気を漂わせる、「大人の色気を感じさせる声」などが、一般的に好まれる声だと言われています。</p>
    <img src="/public/images/ccwork/024/sub_001.jpg" alt="" class="imagemargin_T10B10" />
     <p class="marginBottom50px">「でも、声は生まれつきだから」とあきらめていた方必見！美しい声は、実は自分で作れるのです。それでは、モテ声を作る６つのポイントをお伝えします！</p>
<h3 class=" blueblockBG">その１、胸に手を当てて振動の練習</h3>
     <p class="marginBottom10px">相手に伝わる声というのはよく響く声。よく響く声が出ているときは胸の奥が振動しているのだそうです。まずは、胸に手をあてて低い声から出してみましょう。すると胸の奥が振動しているのがわかります。声が高くなるにつれて振動が減っていくのですが、声が高くなっても胸が振動するように練習してみましょう。</p>
    <img src="/public/images/ccwork/024/sub_002.jpg" alt="" class="imagemargin_T0B50" />
  </section>

  <section class="sect03">
<h3 class=" blueblockBG">その２、のどが乾かないようにする。</h3>
     <p class="marginBottom10px">のどが乾燥していると強い声が出せず、かすれた印象になってしまいます。飴をなめる、水を定期的に飲むなどして、のどをウェットな状態にしておきましょう。</p>
    <img src="/public/images/ccwork/024/sub_003.jpg" alt="" class="imagemargin_T0B50" />
  </section>

  <section class="sect04">
<h3 class=" blueblockBG">その３、体全体で声を出すイメージで。</h3>
     <p class="marginBottom10px">長時間話していると疲れてしまったり、のどが痛くなったり、声が変わってしまった経験はありませんか？これは、のどだけで声を出している証拠なのです。のどの調子が悪くなるのは、声帯を痛めてしまっている、ということなのです。良い声を出すには腹式呼吸が有効です。でも、いきなり腹式呼吸と言われても、意識しないと中々うまくできませんよね。ポイントは体全体で声を出すイメージで、長く呼吸することです。これによって、空気をたくさん含んだ、響きのある声を作りだすことができます。</p>
    <img src="/public/images/ccwork/024/sub_004.jpg" alt="" class="imagemargin_T0B50" />
  </section>

   <section class="sect05">
<h3 class=" blueblockBG">その４、笑顔で！</h3>
     <p class="marginBottom10px">笑顔になると、自然と口角が上がります。口角が上がると、声がワントーン高くなり明るい印象になります。笑っていると、声からも表情からも楽しさが伝わるので、魅力が増しますね。</p>
    <img src="/public/images/ccwork/024/sub_005.jpg" alt="" class="imagemargin_T0B50" />
  </section>

   <section class="sect06">
<h3 class=" blueblockBG">その５、相手を思いやったスピードで</h3>
     <p class="marginBottom10px">落ち着いてゆっくり話すと、相手も安心して聞いていられます。自分で思っている以上に早口になっている人は意外と多いはず。ちょっとゆっくりすぎるくらいのイメージで話すと、聞いている側からは、ちょうど良い早さだったりするんです。声の大きさも重要です！好感度も一気にアップですね。</p>
    <img src="/public/images/ccwork/024/sub_006.jpg" alt="" class="imagemargin_T0B50" />
     <p class="marginBottom40px">ちょっとした意識で、声の印象ってすごく変わるものなんです。印象の４割をになう声が魅力的になれば、コールセンターでのお仕事だけでなく、あなた自身の魅力も４割増しになるかも知れません！？<br />ぜひ、今すぐトライしてみてください。</p>
  </section>


  <aside>
  <dl>
  <dt class="headingG01">「コールセンター知っ得情報」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail18">学歴なんて関係ない！コールセンターなら、実力次第で高収入が目指せる</a></li>
  <li><a href="/ccwork/detail44">コールセンター心理学！これであなたもコミュニケーションの達人に！</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
<aside class="tabNav_ccwork">
<ul><li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li><li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_a.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_a.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li><li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li></ul>
</aside><!-- / tabNav -->
  
  
</section>

