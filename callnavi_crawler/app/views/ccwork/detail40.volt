<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">働く人の声</span></div>
  <h1>ミュージシャンは声で稼げ！<br />コールセンターで、バンドマンが重宝されるワケ</h1>
  </header>
  
<!--- 段落１ 開始 --->  
  <section class="sect01">
    <img src="/public/images/ccwork/040/main001.jpg" class="imagemargin_T10B10" alt="ミュージシャンは声で稼げ！コールセンターで、バンドマンが重宝されるワケ" />

  <p class="marginBottom10px">「よし！コールセンターで働こう！」そう思った時、一緒に働く仲間がどんな人達なのか気になりますよね。都内にある某コールセンターでは、約６割～７割が女性のスタッフとのこと。　</p>

  <img src="/public/images/ccwork/007/cont_002.jpg" width="90" height="90" alt="男性「バイトを辞めたものの、バイト代が貰えていない、、、」" class="fukidasi02_bg" />
<div class="bluefukidasi02">え、男性である僕は浮いてしまうかな･･･？</div>
<p class="fukidasi02_answer">そんなことはありません！<br />
確かに女性スタッフの割合が高いことが多いですが、上司は男性の場合が多いですし、決して「浮いてしまう！」なんてことはないのでご安心を！
また、コールセンターで働くメリットには次のようなこともあります。
</p>

    <h3 class="blueblockBG">代表的なメリット</h3>
    <ul class="dot_bluetext">
    <li><span>1</span>シフトが比較的自由が利く！</li>
    <li><span>2</span>髪型や洋服、ネイルなどに制限がない場合が多いので、オシャレを楽しめる！</li>
    </ul>

  <p class="marginBottom30px">そんなメリットから、こんなスタッフがコールセンターで働いています！！</p>

  <p class="dot_yellowtext_Number">1</p>
  <p class="dot_yellowtext_title">バンドマン（ミュージシャン）</p>
  <p class="marginB20">多忙極めるバンドマン。<span class="Blue_text">空いた時間でがっつり効率良くお金を稼ぎたい！</span>そんなバンドマンに人気があるのがコールセンターのお仕事。髪型自由、服装自由で、髪の毛が金色であっても紫であっても大丈夫！<br />
また、歌のうまい人は、電話対応も上手だといわれています。声でしか表情が伝えられないのがコールセンターのお仕事！バンドマンは音程を掴むのが上手なので、話し方の強弱やトーンなど、すぐにコツを掴むことができるようです。</p>

  <p class="dot_yellowtext_Number">２</p>
  <p class="dot_yellowtext_title">芸能人の卵</p>
  <p class="marginB20">コールセンターは、室内で行うお仕事です。日焼け厳禁の女優さんの卵や、タレントさん志望、元俳優さんが働いていたりもします。<span class="Blue_text">お客様に顔を露出することがないお仕事</span>でもあるので、その点でも安心して働いていけます。<br />
また、女優さんや俳優さんの卵の方々は、さすが電話対応もお上手な場合が多いです。コールセンターのお仕事は、スクリプト（お客様にお伝えする内容が記載されている「台本」のようなもの）が用意されていることがほとんどです。彼らは、その「台本」を見事に演じ、感じの良いお客様対応をしているようです。</p>

  <p class="dot_yellowtext_Number">３</p>
  <p class="dot_yellowtext_title">お笑い芸人</p>
  <p class="marginB20"><span class="Blue_text">隙間時間で稼ぎたい！</span>という理由から、お笑い芸人志望の方もいたりします。お客様と声だけでつながりがあるコールセンターのお仕事では、機転の良さや柔軟性も求められます。まさにその２点を持ち合わせているのが、お笑い芸人の方々です。頭の回転の速さが際立つ、テンポの良いトークで、お客様からも好印象を持たれることが多いようです。また、ノリの良い方が多いので、コールセンター内のムードメーカーにもなってくれることが多いみたいですよ。</p>

  <p class="dot_yellowtext_Number">４</p>
  <p class="dot_yellowtext_title">就活生・学生</p>
  <p class="marginB20 sectborder">コールセンターなら、<span class="Blue_text">就職活動の面接の後、スーツ姿のままでも出社OK！</span>スーツ姿のまま、電話対応している姿は、結構かっこいいかも知れませんよ！<br />１日３時間から就業可能なコールセンターが多いので、面接を受けた後の18時から働きたい！など、融通が利くのも助かりますよね。また電話対応を通して、基本的なビジネスマナーが自然に身につくので、面接前に焦って敬語を勉強する…なんてことも必要ないでしょう。なにより電話でのお客様対応は、<span class="Blue_text">伝えたいことを、シンプルにわかりやすく話す</span>ことの訓練のようなもの。企業との面接でも、要点をわかりやすくお伝えすることができるようになります。
また、学生生活を思いっきり楽しみたい、学生さんたちにも人気のコールセンターでは、学校帰りや、土日に働いているようです。
</p>

  <p class="marginBottom40px">いかがでしたでしょうか。<br />
上記では、コールセンターで働く方の一部を紹介致しました。コールセンターは<span class="Blue_text">自分らしいライフスタイルのまま</span>働くことができる職場です。そのため、上記の他にも、学生、主婦、フリ―ター、留学資金を貯めている人、起業している人、声優志望の人…などなど様々な人が働いています。
コールセンターに興味が湧いたあなた！早速求人をチェックしてみましょう！
</p>

</section>
<!--- 段落１ 終了 ---> 
  

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター働く人の声」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail32">どうするクレーム？しっかり対応するための魔法の４ステップ！</a></li>
  <li><a href="/ccwork/detail45">テレアポ、テレオペ、テレマの違いって？自分にピッタリの仕事を見つける！</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  
  <aside class="tabNav_ccwork">
  <ul id="js-tab01">
  <li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_a.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_a.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li>
  </ul>
  </aside><!-- / tabNav -->
  
  
</section>

