<section class="entryA01 ccwork_entry">
<header>
<div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">働く人の声</span></div>
<h1>【質問】コールセンターバイトでの休憩時間は、<br />仲間と積極的に話すべき！？<br />できれば一人でゆっくりしたいのですが...</h1>
</header>
  
<section class="sect01">
<img src="/public/images/ccwork/073/main001.jpg" class="imagemargin_T10B30" alt="コールセンター業務中の休憩時間の過ごし方" />
<p class="marginBottom20px">今回は、コールセンター業務中の休憩時間の過ごし方です。パート、バイト、派遣社員、正社員のどれでもいいのですが、休憩時間は、仲間ワイワイガヤガヤと楽しみたいですか、それとも一人で静かに過ごしたいですか？</p>

<h2 class="sideBlueBorder_blueText02 marginBottom10px">休憩時間も積極的に話をすべき！</h2>
<p class="marginBottom10px">休憩時間といえども、職場にいる間は仕事と考え、仲間と積極的に情報交換しよう！と言われることがあります。確かに、友達を作って、オペレーター同志仲良くなって、「さっきのトーク良かったよね！」「クレーム対応がとても丁寧ですね。勉強になりました。」などお互い成長していく方がいいと考える人もいます。</p>
<p class="marginBottom20px">他にも、休憩時間には、テレアポで営業成績を出しているオペレーターと積極的に話をして、自分と何が違うのか？　徹底的に研究するのがいい！とアドバイスされることもあります。</p>

<h2 class="sideBlueBorder_blueText02 marginBottom10px">なぜ、積極的に話すべきだと考えちゃうの？</h2>
<img src="/public/images/ccwork/073/sub_001.jpg" class="imagemargin_T10B30" alt="なぜ、積極的に話すべきだと考えちゃうの？" />
<p class="marginBottom20px">休み時間も、職場の同僚と積極的にコミュニケーションをとって、盗めるノウハウは盗むべき！　なんて考えちゃうことがあります。でも、なんでそう考えちゃうのでしょうか？入社説明会や、研修でそのような説明や助言を受けたから、成功哲学の本にそう書いてあったから、とにかく少しでも早く結果を出したい！と鼻息荒く、焦っているから。などなど、いろんな考え方があると思いますが、どこかで一度立ち止まって、<span class="Blue_text">なぜ自分はこのように（これが当たり前だと）考えるのだろう？というのを俯瞰する機会を持つことは、とても大切なこと</span>だと思います。</p>

<h2 class="sideBlueBorder_blueText02 marginBottom10px">なぜ、休憩時間に話す必要はなと考えちゃうの？</h2>
<p class="marginBottom10px">休憩なのだから、リフレッシュスペースでゆっくり休みたい。テレアポで散々しゃべっているのだから、静かにしたい。と考える人もいます。有名なお笑い芸人の中には、テレビに映っているとき以外でも、常にしゃべり続けている人と、テレビ以外ではほとんどしゃべらない人がいるようです。</p>
<p class="marginBottom20px">上記の他には、人見知りだったり、なかなか友達ができなかったりするため、休憩時間には、ひとりでいる場合もあります。</p>

<h2 class="sideBlueBorder_blueText02 marginBottom10px">結局のところ、どちらでもいい！</h2>
<img src="/public/images/ccwork/073/sub_002.jpg" class="imagemargin_T10B30" alt="結局のところ、どちらでもいい！" />
<p class="marginBottom10px">休憩時間も、ワイワイガヤガヤ話すのか、一人で静かにしているのか、結局のところ、どちらでもいいと思います。人間はみな同じではないので、自分にあったコールセンターの成功スタイルを見つけていくのがいいでしょう</p>
<p class="marginBottom10px">休憩時間の1分1秒も無駄にすることなく、先輩・リーダー・SVなど、結果を出している人にピッタリとくっついて、成功する為のポイントを教えてもらったり、自分との違いを見つけてそのスキルを埋めるたりするのもOK！</p>
<p class="marginBottom20px">自分の状態を整えて、ベストな状態で臨むために、一人静かに本を読んだり、仮眠をとったりするのもOKだと思います。その時の気分や体調によっても違ってくるし、話したい人がいれば話せばいいでしょう。</p>

<h2 class="sideBlueBorder_blueText02 marginBottom10px">ここは、気をつけよう！</h2>
<img src="/public/images/ccwork/073/sub_003.jpg" class="imagemargin_T10B30" alt="ここは、気をつけよう！" />
<p class="marginBottom10px">結局のところ、どちらでもいいのですが、自分にはこれがあっている！と思っても、他人にとってベストな方法かどうかは分かりません。無理に他人に押しつけないことが大切です。</p>
<p class="marginBottom10px">いつもみんなでワイワイガヤガヤしているからと言って、仕事の話をしているとは限りません。単なる雑談をしているだけかも知れません。仲間に属しているというだけで、自分が成長して、時給が上がって、インセンティブが貰えるわけではないので、注意しましょう。</p>
<p class="marginBottom20px">また、ひとりでいるのが好きな方でも、クレームの後や、気分の落ち込みが激しい時などは、できるだけ仲間とコミュニケーションをとりましょう。自分ひとりだと、ネガティブなループにぐるぐると入ってしまい、次のコール（電話）にも影響してしまうかも知れませんが、<span class="Blue_text">仲間と話すことにより、上手にリフレッシュできる場合があります。</span></p>

<h3 class="blueblockBG">最後に</h3>
<p class="marginBottom10px">休憩時間にどうするか？　結局のところ、どちらでもOKです。最初のうちは、いろいろなパターンを試して、どれが一番自分にあっているか、チェックしてみましょう。</p>
<p class="marginBottom10px">自分ではあまり気が進まないのに、無理してコミュニケーションをとろうとしたり、会話に入り込もうとしたりしない方がいいかも知れません。そんな時は、焦っているかも！？と自分の状態を俯瞰して、自然体でいられるよう、こころがけましょう。</p>
<p class="marginBottom40px">コールセンターの面白いところは、いろんな成功者がいることです。是非とも<span class="Blue_text">自分と似たタイプの成功者を見つけて、その人の行動パターンを真似していきましょう。</span></p>






</section>

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター働く人の声」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail58">コールセンターバイトで、就職活動を有利に！電話対応、コミュ力、ビジネスマナーは最大の武器！</a></li>
  <li><a href="/ccwork/detail67">裏技教えます！本当は教えたくない受注が取れるコツ</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  <aside class="tabNav_ccwork">
  <ul id="js-tab01">
  <li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_a.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_a.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li>
  </ul>
  </aside><!-- / tabNav -->
  
  
</section>
