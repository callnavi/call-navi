<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">知っ得情報</span></div>
  <h1>隠されたドラマがある！？<br />コールセンターの舞台裏！</h1>
  </header>
  
<!--- 段落１ 開始 --->  
  <section class="sect01">
  <img src="/public/images/ccwork/016/main001.jpg" class="imagemargin_T10B10" alt="隠されたドラマがある！？コールセンターの舞台裏！" />
  <p class="marginBottom10px">働く前になかなか見ることができないコールセンター。どんな人がどんな思いで働いているのかがわかりづらいところではありますよね。</p>
  <p class="marginBottom20px">そんなあなたにオススメする、コールセンターを題材にしたメディアをまとめてみました。</p>
  </section>
<!--- 段落１ 終了 --->


<!--- 段落２ 開始 --->    
  <section class="sect02">
  <h2 class="sideBlueBorder_blueText02">コールセンターエンターテイメント</h2>
  <img src="/public/images/ccwork/016/main002.jpg" class="imagemargin_T10B10" alt="コールセンターの業務内容とは？" />
  <p class="marginBottom20px"><span class="Blue_text">ドラマ：コールセンターの恋人</span><br />小泉孝太郎主演の2009年に放送されたドラマです。通販番組の商品企画を手がけていた主人公が、ひょんなことからコールセンターに移動させられることに。個性的すぎるクレーム対応係の面々と次々ふりかかるクレームに奮闘しながら対応します。あまり知られていないコールセンターの裏側を知ることもできるエンターテイメントドラマです。</p>

  <h2 class="sideBlueBorder_blueText02">作者の極限の下積み時代！</h2>
  <img src="/public/images/ccwork/016/sub_001.jpg" class="imagemargin_T10B10" alt="コールセンターの業務内容とは？" />
  <p class="marginBottom20px"><span class="Blue_text">漫画：かくかくしかじか／東村アキコ</span><br />マンガ大賞2015大賞に選ばれた。漫画家 東村アキコの自伝漫画「かくかくしかじか」。主人公のアキコが美大卒業後、故郷 宮崎に帰ってから働き始めたのがコールセンター。昼間はコールセンター、夜に画塾教室の先生、深夜に漫画を描いて投稿という極限状態の下積み時代が3〜4巻に描かれています。夢を目指しているアナタ、必読ですよ！</p>

  <h2 class="sideBlueBorder_blueText02">リストラ女子”電話対応日本一”を目指す！</h2>
<div class="imageBlockA02 marginB20">
<p class="image"><img src="/public/images/ccwork/016/sub_002.jpg" width="210" alt="リストラ女子”電話対応日本一”を目指す！" /></p>
<div class="contentsInner1">
<p class="marginBottom20px"><span class="Blue_text">小説：インバウンド／阿川大樹</span><br />
主人公の理美はリストラされてしまい、東京から故郷の沖縄へ帰りコールセンターで働くことに。いやいや面接に行ったが、コールセンターの近代的な設備に圧倒され、働くことを決意。「役を演じろ！」「ウィスキーの顔を作れ！」など研修でとまどう理美たち。そしてついに実務が始まるとさまざまなトラブルで更にてんやわんや！そんな理美が会社の代表として「電話対応コンクール」に出場することになり…！？コールセンター業務をディティールたっぷりに描いているお仕事小説です！</p>
</div>
</div>

  <h2 class="sideBlueBorder_blueText02 ">超ストレスフルな仕事の乗り越え方</h2>
<div class="imageBlockA02 marginBottom40px">
<p class="image"><img src="/public/images/ccwork/016/sub_003.jpg" width="210" alt="超ストレスフルな仕事の乗り越え方" /></p>
<div class="contentsInner1">
<p class="marginBottom20px"><span class="Blue_text">エッセイ：督促OL修行日記／榎本まみ</span><br />
新卒でカード会社に入社した新人督促OLの作者。人見知りで、人に強く言えない性格の作者が入金のお願いの電話をかける「督促」という仕事を始め、百戦錬磨の借金王の面々を相手に毎日格闘し、ついには2000億円の借金を回収する「スゴ腕 督促ＯＬ」に！「日本一ストレスフルな職場」で、仕事に誇りを見出していくまでの新人時代をエッセイと漫画でつづっています。おもしろおかしい内容だけでなく、全ての仕事に通じる処世術が読みやすくまとめられています！</p>
</div>
</div>
  </section>
<!--- 段落２ 終了 --->

<aside>
  <dl>
  <dt class="headingG01">「コールセンター知っ得情報」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail24">声で人生が変わる！？コールセンターで好印象な声を会得する方法</a></li>
  <li><a href="/ccwork/detail02">コールセンターで働くメリットってなに？</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  <aside class="tabNav_ccwork">
  <ul>
  <li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_a.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_a.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li>
<li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li>
  </ul>
  </aside><!-- / tabNav -->
  
</section>
