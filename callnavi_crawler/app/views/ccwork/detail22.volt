<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">基礎知識</span></div>
  <h1>やればやるだけ報酬UP！<br />だから成長できるインセンティブのひみつ</h1>
  </header>
  
<!--- 段落１ 開始 --->  
  <section class="sect01">
  <img src="/public/images/ccwork/022/main001.jpg" class="imagemargin_T10B10" alt="隠されたドラマがある！？コールセンターの舞台裏！" />
  <p class="marginBottom40px">コールセンターの仕事を探していると「基本給＋インセンティブ」という表記をよく目にしますよね。なんとなく分かるけど、結局どういう仕組みなの？という方も多いかと思います。また、「インセンティブがいっぱいある方が、いっぱい稼げるってことでしょ？」という方は、インセンティブのメリット・デメリットもきちんと確認しておくことが大事です。働き始めてから「こんなはずじゃ…」ではなく「こんなに稼げる！」となるように、前もってインセンティブの勉強をしておきましょう！</p>
  </section>
<!--- 段落１ 終了 --->


<!--- 段落２ 開始 --->    
  <section class="sect02">
  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />ヒミツ</p>
  <p class="yellowmiddlecircle_titlenumber">1</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">インセンティブとは？</p>
  </div>  
  <img src="/public/images/ccwork/022/sub_001.jpg" class="imagemargin_T10B10" alt="隠されたドラマがある！？コールセンターの舞台裏！" />
  <p class="marginBottom40px">インセンティブとは英語で “incentive”と書きます。名詞の意味としては「刺激・動機・奨学金」、形容詞の意味としては“意欲をかき立てる要因を広く指す語”となります。日本の意味としては、特に文脈の中で「動機付け」もしくは「見返り」に近いニュアンスで用いられることも多いようです。(参考：http://www.weblio.jp/content/incentiveより実用日本語表現辞典)<br />それでは、コールセンターで良く聞く「インセンティブ」とは、何を意味するのでしょうか？</p>

  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />ヒミツ</p>
  <p class="yellowmiddlecircle_titlenumber">2</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">コールセンターのインセンティブってどういう意味なの？</p>
  </div>  
  <img src="/public/images/ccwork/022/sub_002.jpg" class="imagemargin_T10B10" alt="隠されたドラマがある！？コールセンターの舞台裏！" />
  <p class="marginBottom40px">コールセンターにおけるインセンティブとは、ある決められた期間の中で、優秀な成績を収めたスタッフを対象に、特別な報酬や賞品を与えることです。基本給とは別に、こういった制度があることによって、スタッフのモチベーションの向上や競争意識を芽生えさせ、実績を上げるのが狙いです。</p>

  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />ヒミツ</p>
  <p class="yellowmiddlecircle_titlenumber">3</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">いくらくらい貰えるの？</p>
  </div>  
  <img src="/public/images/ccwork/022/sub_003.jpg" class="imagemargin_T10B10" alt="隠されたドラマがある！？コールセンターの舞台裏！" />
  <p class="marginBottom40px">インセンティブの基準はコールセンターによって様々です。１件ごとに数円単位で貰えるものや、売り上げの数％を貰えるもの、期間内に目標を達成したら貰えるもの、あるいはある一定の成績以上なら金一封が貰える…などコールセンターごとに、独自の制度があるところがほとんどです。一般的には、上記のような状況となりますので、一概に「いくらくらい貰える」という指標はなかなか出せませんが、人によってはインセンティブだけで数十万円稼ぐ方もいらっしゃると聞いたことがあります。ちなみに、インセンティブをたくさんもらって、稼ぎたい！　と思ったらアウトバンドのお仕事がオススメです。</p>

  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg"  class="yellowmiddlecircle_titleimg" />ヒミツ</p>
  <p class="yellowmiddlecircle_titlenumber">4</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">インセンティブのメリット・デメリット</p>
  </div>  
  <img src="/public/images/ccwork/022/sub_004.jpg" class="imagemargin_T10B10" alt="隠されたドラマがある！？コールセンターの舞台裏！" />
<p class="dot_bluestar_text"><span>メリット</span></p>
  <p class="marginBottom20px">件数を獲得することができれば、どんどん稼げますので、やりがいと報酬を得たい方には大変おすすめです！自分のトークスキルや知識量が増えてくれば、やればやるだけ件数も獲れてくるようになるので、自分の成長＝報酬UPが味わえるのがインセンティブの醍醐味です。</p>

<p class="dot_bluestar_text"><span>デメリット</span></p>
  <p class="marginBottom40px">インセンティブの報酬に気を取られてお仕事を決めてしまうと、成績がなかなか上がらないと辛い思いをしてしまうかも！？コールセンター自体が高時給のお仕事なので、あくまでインセンティブは基本給についてくる＋αのボーナス的な感覚だと思っておくと、丁度よいかもしれません。</p>

  </section>
<!--- 段落２ 終了 --->

<aside>
<dl>
<dt class="headingG01">「コールセンター基礎知識」オススメのページ</dt>
<dd>
<ul class="linkListA01">
<li><a href="/ccwork/detail03">全国調査！コールセンターの時給比較</a></li>
<li><a href="/ccwork/detail18">学歴なんて関係ない！コールセンターなら、実力次第で高収入が目指せる</a></li>
</ul>
</dd>
</dl>
</aside>


<aside class="tabNav_ccwork">
<ul><li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_a.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_a.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li><li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li><li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li></ul>
</aside><!-- / tabNav -->
  
  
</section>

