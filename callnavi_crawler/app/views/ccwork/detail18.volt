<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">知っ得情報</span></div>
  <h1>学歴なんて関係ない！コールセンターなら、<br />実力次第で高収入が目指せる</h1>
  </header>
  
<!--- 段落１ 開始 --->  
  <section class="sect01">
  <p class="marginBottom10px">自分にぴったりのコールセンター求人を見つけていざ履歴書を作成しようとしても、学歴がないから、職歴が十分じゃないから･･･と諦めてはいませんか？<br />今回は、コールセンターと学歴・職歴についてまとめました。</p>
  <img src="/public/images/ccwork/018/main001.jpg" class="imagemargin_T10B10" alt="学歴なんて関係ない！コールセンターなら、<br />実力次第で高収入が目指せる" />
  </section>
<!--- 段落１ 終了 --->

<!--- 段落２ 開始 --->    
  <section class="sect02">
  <h2 class="sideBlueBorder_blueText02 marginBottom10px">学歴・職歴で何が分かるのか？</h2>
  <p class="marginBottom10px">一般的には学歴・職歴が重視されています。人物的には性格や人間性が良くても、学歴や職歴が十分じゃないと、就職活動は不利になるケースが多いようです。<br />ではコールセンターの場合、どのような点が見られているのでしょうか。</p>

<ul class="fukidasi02_graybox">
<li class="marginBottom10px">●学歴…小・中学校、高校、大学、大学院、短大、専門学校など、今までの経歴・専門分野や、卒業年度からの社会人歴を見ています。</li>
<li class="marginBottom10px">●職歴…今までどんな仕事に就いていたのか？コールセンターや、オペレータ・責任者としての経験があるのか？即戦力になるのか？メンバーやスタッフとしてどういう仕事をしていたのかを見ています。コールセンターでのキャリアがない人は、事務仕事、電話対応、サポート業務などの経験があるかどうかを見ています。</li>
</ul>
  <p class="marginBottom10px">学歴・職歴は、その人がどんなことができそうなのか？といったおおよその目安でしかないので、実際のところは面接で判断されています。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">なぜ企業は学歴・職歴を見るの？</h2>
  <p class="marginBottom10px">まず必要最低限のスキルを有する人を集めておき、その中から、会社のビジョンに共鳴してくれたり、会社のメンバーとして一緒に頑張ってくれそうな人を採用していく方法が効率的だと考えられているからです。<br />企業は、学歴から学生時代に学んでいたこと、サークル活動の様子などから、専門性や経験値を、職歴からは以前の職場の業種や職種から、どんなアウトプットを出してくれそうか？即戦力やリーダーとして活躍してくれそうなのかを見ています。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">学歴、職歴で分からないことは？</h2>
  <p class="marginBottom10px">まずは<span class="Blue_text">未経験分野のポテンシャル</span>です。ある程度、似たような分野で業務を行った経験があれば、就職後にどのくらい活躍してくれるか予想がつきそうですが、全く別の業界となると、判断するのは難しくなります。そして、<span class="Blue_text">本人の性格やキャラクター</span>に関しても、学歴や職歴からは分かりにくいものです。<br />これらは、履歴書などの書類審査ではなく、面接（場合によっては、グループ面談なども）によって、コミュニケーションスキル、リーダーシップ、チームとしての協調性などを見ていく必要があります。<br />また、営業のスキルは、やってみないと分からないことが多く、テレアポやテレマの分野でも同じことが言えます。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">学歴、職歴の重要性は？</h2>
  <p class="marginBottom10px">では、コールセンターにおける、学歴、職歴の重要性は何でしょうか？<br />まずは、コールセンターでの経験や電話業務、営業経験などのキャリアやスキルが重要です。しかしながら、学歴としては、<span class="Blue_text">条件となるようなものはほとんどなく、敷居は低い</span>と言えます。その分、<span class="Blue_text">面接での対応が重要</span>となり、ここを上手にクリアすることが、コールセンター業務に就くためのポイントとなります。</p>
<p class="marginBottom10px">ハキハキとしたコミュニケーションや、電話対応のマナーや聞き取りやすさ、そして、この人と一緒に仕事をしたいと思ってもらえるか、といったキャラクターも大切になってきます。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">コールセンターで結果を出している人は？</h2>
  <p class="marginBottom10px">最後に、コールセンター業務で結果を出している人を見てみましょう。<br />それは、必ずしも学歴がある人、職歴がある人ではないようです。<br />未経験でアルバイト入社をして、スクリプトを一生懸命に覚えながら、先輩のアドバイスを真摯に受け止め、<span class="Blue_text">日々改善していった方</span>が多いと聞きます。<br />あとは、<span class="Blue_text">コールセンターの仕事を楽しんでいる人</span>です。<br />アルバイト、派遣、正社員のどれでもいいですが、テレアポやテレマなど、コールセンター業務に関わることがありましたら、まずは、社内のマニュアルに従って、日々努力をしてみては、いかがでしょうか。</p>
  <p class="marginBottom40px">結果を出している先輩や上司、リーダーにアドバイスを頂きながら、軌道修正をしていけば、ひょっとしたら、普通のサラリーマンよりも高収入な生活が待っているかも知れませんよ！</p>
  </section>
<!--- 段落２ 終了 --->

<aside>
  <dl>
  <dt class="headingG01">「コールセンター知っ得情報」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail31">仕事とプライベートの両立！シフト制は、スケジュール管理の魔法の杖となるのか？</a></li>
    <li><a href="/ccwork/detail34">就活の秘訣！コールセンター経験者が就活を有利に進められる理由とは？</a></li>
</ul>
  </dd>
  </dl>
  </aside>
  
  <aside class="tabNav_ccwork">
  <ul>
  <li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_a.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_a.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li>
<li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li>
  </ul>
  </aside><!-- / tabNav -->
  
</section>

