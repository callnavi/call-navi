<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">働く人の声</span></div>
  <h1>コールセンターバイトで、就職活動を有利に！<br />電話対応、コミュ力、ビジネスマナーは最大の武器！</h1>
  </header>
  
  <section class="sect01">
    <img src="/public/images/ccwork/058/main001.jpg" class="imagemargin_T10B10" alt="コールセンターバイトで、就職活動を有利に！電話対応、コミュ力、ビジネスマナーは最大の武器！" />
  <p class="marginBottom20px">大学生、専門学校生など、学生さんのアルバイトは、何を基準に選んでいますか？アルバイトを選ぶ基準と、選んだアルバイトが今後の人生で、どのように役立っていくかについて紹介していきます。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">何を目的に、アルバイトを選ぶ？</h2>
      <img src="/public/images/ccwork/058/sub_003.jpg" class="marginBottom_none" alt="ズバリ、アルバイトをする目的は何ですか？" />
    <img src="/public/images/ccwork/058/sub_004.jpg" class="marginBottom20px" alt="もちろん、お金です。" />
  <p class="marginBottom10px">海外旅行に行きたい！おしゃれな洋服が欲しい！車やバイクのため、なかには学費を稼ぐためなど、お金が目的でアルバイトをすると思いますが、どうせなら就活に役立つスキルを身につけながら働く方が良くありませんか？</p>
<ul class="fukidasi02_graybox">
<li><p class="dot_bluecircle_text">夏休みや冬休みなど、あいた時間を有効活用したい</p></li>
<li><p class="dot_bluecircle_text">将来の仕事の予行演習をしたい</p></li>
<li><p class="dot_bluecircle_text">社会勉強のため</p></li>
<li><p class="dot_bluecircle_text">新しい体験・スキルアップ、人間関係を求めて</p></li>
<li><p class="dot_bluecircle_text"> 友人や仲間に誘われて</p></li>
</ul>
  <p class="marginBottom20px">などなど、アルバイトをする目的やきっかけには、いろいろなパターンがあります。せっかくの機会ですから、<span class="Blue_text">お金も稼げて、社会人になってからも役立つアルバイトをしてみてはいかがでしょうか。このようなバイトは、就職活動でもきっと役に立つでしょう。</span></p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">就職活動で必要とされていること（スキルなど）</h2>
  <p class="marginBottom10px">就職活動の際、面接官（会社や企業の人事担当者）は、応募者のどのようなところを見ているのでしょうか？</p>
<ul class="fukidasi02_graybox">
<li><p class="dot_bluecircle_text">専門分野のスキルや経験</p></li>
<li><p class="dot_bluecircle_text">第一印象（身だしなみ、さわやかさ　など）</p></li>
<li><p class="dot_bluecircle_text">コミュニケーション力</p></li>
<li><p class="dot_bluecircle_text">プレゼンテーション能力</p></li>
<li><p class="dot_bluecircle_text">ビジネスマナー</p></li>
<li><p class="dot_bluecircle_text">質問力</p></li>
</ul>
  <p class="marginBottom20px">これらは仕事によって、変わってくることもありますが、おおむね上記のとおりです。この中で、コミュニケーション力やビジネスマナーは大切ですが、実務経験がないと、なかなか身につかないですよね。ここで、アルバイト経験が生きてきます。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">就職に役立つバイト経験</h2>
    <img src="/public/images/ccwork/058/sub_001.jpg" class="imagemargin_T10B10" alt="就職に役立つバイト経験" />
  <p class="marginBottom20px">アルバイト時代の経験が就職に役立つことは、結構あると思います。レストランだったら、接客スキルだけでなく、中華料理など食事のマナーが身についたり、家庭教師や塾講師だと説明が上手になったり、ガソリンスタンドだと運転が上手になったり、コンビニだと流通の仕組みが理解できたり、といったことがあります。<br />どれも、自分で勉強しようと思うと結構体験ですが、バイトの先輩や、店長さんが教えてくれる場合が多いです。他にも、引越しのバイトで、大金持ちの家がどんな風になっているか見られたり、掃除のバイトでクリーニングが上手になったり、などなど、アルバイトから得られる貴重な経験はたくさんあります。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">コールセンターの仕事が、なぜ役に立つのか？</h2>
    <img src="/public/images/ccwork/058/sub_002.jpg" class="imagemargin_T10B10" alt="コールセンターの仕事が、なぜ役に立つのか？" />
  <p class="marginBottom20px">ここで、コールセンターの仕事は、受信（インバウンド）と架電（アウトバウンド）がありますが、どちらも、一度もあったことがないお客様に電話で要件を伝える（又は受ける）のが仕事です。<br />コールセンター、ヘルプデスク、テクニカルサポートなど言い方は色々ありますが、<span class="Blue_text">ビジネスマナーとコミュニケーション能力が磨かれる仕事となります。</span><br />テレオペでは、ある程度のマニュアルがあり、それに伴って対応すればOKのところが多いですが、<span class="Blue_text">テレアポやテレマなど電話営業となると、コミュニケーションスキルや、プレゼンテーション能力が大切</span>になります。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">コールセンターバイトで、役立つ経験</h2>
  <p class="marginBottom20px">それでは、コールセンターでアルバイトをしていると、どんな経験ができて、どのように就職活動に役立つのでしょうか。</p>
<ul class="fukidasi02_graybox">
<li><p class="dot_bluecircle_text">コミュニケーション能力が上がる！</p></li>
<li><p class="dot_bluecircle_text">ビジネスマナーが身につく（正しい敬語、話し方）</p></li>
<li><p class="dot_bluecircle_text">問題なく、電話対応ができる</p></li>
<li><p class="dot_bluecircle_text">対応する商品やサービスの知識が身につく</p></li>
<li><p class="dot_bluecircle_text">パソコン操作が苦にならない</p></li>
<li><p class="dot_bluecircle_text">タッチタイピング（ブラインドタッチ）ができる</p></li>
<li><p class="dot_bluecircle_text">様々な年代の方と、仲良く話しができる</p></li>
<li><p class="dot_bluecircle_text">色々な人生経験、夢を持った友達ができる</p></li>
<li><p class="dot_bluecircle_text">精神的にタフになった</p></li>
<li><p class="dot_bluecircle_text">自己PRのネタになった</p></li>
</ul>
  <p class="marginBottom20px"><span class="Blue_text">一番大きいのは、コミュニケーション能力の向上と、ビジネスマナーの習得</span>です。顔が見えないお客様と話すので、丁寧な話し方でありながら、しっかりと意思疎通をはかっていく必要があります。<br />その際、通話の記録をメモにとっていくのですが、紙とペンを使う場合よりも、そのままパソコンに入力していく場合が多いので、パソコン操作が苦にならなくなるでしょう。<br />あとは、<span class="Blue_text">コールセンターの仕事は、コンビニやファミレスなどと比べて、一緒に働いている人の数も多いです。幅広い年代や、バックグラウンドを持った方と話ができるのもコールセンターの魅力</span>です。<br />ひょっとしたら、芸人やアーティスト志望の方が、今の生活費を稼ぐためにコールセンターにいるのかも知れません。テレビで活躍している芸人さんが、昔一緒に働いていた仲間だったりすると、ちょっとした自慢話にもなりますからね。</p>

  <h2 class="sideBlueBorder_blueText02 marginBottom10px">コールセンターでの体験談</h2>
<div class="imageBlockB01">
<p class="image"><span class="bluefukidasi_bluetext">コールセンター体験者</span><img src="/public/images/ccwork/004/sub001.jpg"alt="" class=" image_left" /></p>
<div class="contentsInner">
<div class="bluefukidasi">
  <p class="marginBottom_none">就職活動が始まって、多くの企業に履歴書を送って、会社説明会に参加して、面接もたくさん受けました。その為、スケジュールも結構厳しく、前の会社での面接が遅れてしまうと、次の面接がギリギリになってしまうこともありました。そんなときでも、電話を必ず入れるようにしていたのですが、友人などは、急いでいると敬語とかちゃんと使えない場合も多くなって、ボロがでて落ちてしまったこともあったようです。でも、私の場合、電話対応は問題なくできます！　どんなに急いでいても、自分の体調が悪くても、敬語が使えて、相手の方にきちんと伝えることができました。そのおかげもあって、無事内定を頂くことができました。これもコールセンターでのバイト経験が役立ったと思います。</p>
</div><!-- / bluefukidasi -->
</div><!-- / contentsInner -->
</div><!-- / imageBlockB01 -->

    <h3 class="blueblockBG">最後に</h3>
  <p class="marginBottom20px">いかがでしたか？　アルバイトを選ぶとき、色々な理由があると思いますが、コールセンターでのバイト経験が就職活動にも有利に働くことが分かって頂けたと思います。もちろん、全ての業界で有利に働くわけではありませんが、コミュニケーション力、ビジネスマナーは、社会人の基本とも言えるスキルですので、友人よりも一足先に身につけてみてはいかがでしょうか。</p>
</section>

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター働く人の声」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail32">どうするクレーム？しっかり対応するための魔法の４ステップ！</a></li>
  <li><a href="/ccwork/detail51">営業経験が全くないのですが、コールセンターの仕事はできますか？</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
  
  <aside class="tabNav_ccwork">
  <ul id="js-tab01">
  <li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li>
  <li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_a.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_a.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li>
  </ul>
  </aside><!-- / tabNav -->
  
  
</section>
