<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">知っ得情報</span></div>
  <h1>コールセンターの困ったお客様</h1>
  </header>
  
  <section class="sect01">
     <p class="marginBottom20px">コールセンターで働いていると、たまに対応に困るお客様に遭遇します。特に24時間営業をしているコールセンターでは、そんな電話も多いのだとか。お客様に失礼があってはいけませんが、どう対応するか見極めが難しいところです。どんな電話がかかってくるのか、今回は代表的なケースをいくつかご紹介します。</p>
  </section>
  
  <section class="sect02">
  <h2 class="sideBlueBorder_blueText02">どんな電話がかかってくるの？</h2>
<h3 class="yellowBlockBG">ケースA　酒酔電話</h3>
    <img src="/public/images/ccwork/025/main001.jpg" alt="" class="imagemargin_B10" />
  </section>

  <section class="sect03">
  <p class="dot_yellowtext_Number">1</p>
  <p class="dot_yellowtext_title">クレームを言いたくてお酒で勢いをつけてくる</p>
  <p class="marginBottom20px">ろれつが回っていない人も多く、人間の言語とは思えない勢いで聞きとれないことがほとんどだそうです。しかし、相手はとんでもない（お酒の）勢いで怒りをぶつけて来て、こちらの話は全く聞いてくれない人ばかりなのだとか。 </p>
  <p class="dot_yellowtext_Number">2</p>
  <p class="dot_yellowtext_title">上機嫌だけど、話にならない</p>
     <p class="marginBottom20px">お酒の効果でとっても上機嫌。でも、こっちの話は全然聞いてない！！自分の話したいことだけをずっと話してくるそうです。しかも同じことを何回も…。もし、なんとか話が進んでも、後で「覚えていない」なんて言われることも多いのだとか。相手がお酒を飲んでいれば、『きちんと対応した』ことにはならないですからね…。
ただ、お酒を飲んでいなくても、お酒を飲んだようなテンションの人も中にはいるので、きちんと確認したいところです。<br /><span class="text_small">※お客様批判にならないように、注意！</span></p>

<h3 class="yellowBlockBG">ケースB　無言電話</h3>
    <img src="/public/images/ccwork/025/sub_001.jpg" alt="" class="imagemargin_B25" />

     <p class="marginBottom20px">電話の相手が応答しない、なんてことありますよね？全くの無音の場合や、何か雑音のような音はするのに人の声が聞こえない、なんてこともありませんか？そう、無言電話です。こっちも黙っているとその状態で数分経過する、なんてこともあるそうです。でも、<span class="Blue_text">単なる悪意の嫌がらせ</span>なのか、<span class="Blue_text">電話器の不具合</span>なのかは、調べてみないとわからないこともあるようです。悪意の嫌がらせでの無言電話は大抵非通知でかかってくることが多いそうで、一日に数百回の無言電話で営業妨害にあたり、賠償問題に発展したケースも実際あるそうです。</p>

<h3 class="yellowBlockBG">ケースC　変態電話</h3>
    <img src="/public/images/ccwork/025/sub_002.jpg" alt="" class="imagemargin_B25" />

     <p class="marginBottom20px">何を話しているかわからない程度の小さな声で「ごにょごにょ…」と話をしてきて、よくよく聞いたらそれが猥褻な言葉だったりすることがあるそうです。内容は下着の色を聞いていたり、バストのサイズを聞いてきたり、それよりも過激な内容を言ってきたり…様々なのだとか。でも、こういった電話も最近のコールセンターでは稀なケースだそうです。現代のコールセンターは、非通知着信を受け付けないようにしていたり、昔よりも男性のオペレーターが増えているからかもしれませんね。</p>


  </section>


  <section class="sect08">
    <h3 class="blueblockBG">最後に</h3>
  <p class="marginBottom40px">コールセンターで働いていると一日に何十・何百人の人と会話をすることになるので、その中で、変な電話をとってしまう確率は、残念ながら、コールセンターで働いていない人よりも格段に上がります。どんな変なことを言われても、嫌なことを言われても、電話口の人にはあなたの電話が会社の印象を決めることを忘れずに、ガチャ切りや暴言は控えましょう。また、勤めている会社で対応方法がきちんとあるはずなので、マニュアルや上長に確認をして対応するようにしましょう。 
ただ、「自分ではどうにも出来ない！」と思ったときはSVや上長に電話を代わってもらいましょう。アルバイトが対応しなければならない訳ではないので、無理は禁物です。 </p>
  </section>

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター知っ得情報」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail32">どうするクレーム対応？しっかり対応するための魔法の４ステップ！</a></li>
  <li><a href="/ccwork/detail24">声で変わる！？コールセンターで印象的な声を会得する方法</a></li>
  </ul>
  </dd>
  </dl>
  </aside>
  
<aside class="tabNav_ccwork">
<ul><li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_n.png" width="192" height="" alt="基礎知識" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_01_03_n.png" width="286" height="" alt="基礎知識" class="mediaSP"></a></li><li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_a.png" width="192" height="" alt="知っ得情報" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_02_03_a.png" width="286" height="" alt="知っ得情報" class="mediaSP"></a></li><li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" width="192" height="" alt="働く人の声" class="mediaPC"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" width="286" height="" alt="働く人の声" class="mediaSP"></a></li></ul>
</aside><!-- / tabNav -->
  
  
</section>

