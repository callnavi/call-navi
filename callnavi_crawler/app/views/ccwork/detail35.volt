<section class="entryA01 ccwork_entry">
  <header>
  <div class="sub_title01">コールセンターの仕事とは？<span class="sub_title02">基礎知識</span></div>
  <h1>初心者でも安心！<br>コールセンターの研修や教育でやっていること</h1>
  </header>
  
  <section class="sect01">
  <p class="marginBottom10px">コールセンター初心者にとっては、商材の知識、クレーム対応など、会社の窓口としてサービスをご案内するにあたって、不安なことも多いのではないでしょうか。</p>
  <p class="marginBottom10px">
「初心者なんだから、失敗するのは当たり前！最初は怒られたって平気！」。</p>
  <p class="marginBottom10px">
確かにそうなのですが、そこであきらめずに、事前にスキルを身につけて、プロとしての自覚を少しでも持った状態で業務に当たれるのが望ましいですよね。また、商材の知識を身につければすぐに仕事ができるかというと、そうでもないのです。</p>
  <p class="marginBottom20px">
そこで今回は、伸びるアポインターさんを育てるための、コールセンターで行われている研修や教育についてご紹介します。</p>
  <img src="/public/images/ccwork/035/main001.jpg" alt="「ストレスに負けない！」コールセンターバイト" class="marginBottom25px">
</section>
  
  <section class="sect02">
  <h2 class="sideBlueBorder_blueText02 marginBottom10px">教育や研修で学べること</h2>
  <p class="marginBottom20px">コールセンターによって多少異なりますが、以下のような基準に沿って教育を行っています。</p>

  <p class="blackdot">商材の知識　基本的なサービス案内や業務のスキルなどを理解しているか。</p>
  <p class="blackdot">電話対応のスキル　声の明るさ、言葉づかい、ヒアリングの能力などを理解しているか。</p>
  <p class="blackdot marginBottom20px">対応後のフロー　情報の入力、アフターフォローなどを理解しているか。</p>
  <p class="marginBottom40px">上記を踏まえ、座学やロールプレイング（アポインター、お客様の役割に分かれて、実際にトークの練習）の研修を行っていきます。<br>では、実際にどのように研修が進められていくか、多くのコールセンターで行われている研修の内容をまとめました。</p>
  </section>
  
  <section class="sect03">
  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg" class="yellowmiddlecircle_titleimg">STEP</p>
  <p class="yellowmiddlecircle_titlenumber">1</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">座学研修</p>
  </div>  
  <img src="/public/images/ccwork/035/sub_001.jpg" class="imagemargin_B10" alt="隠されたドラマがある！？コールセンターの舞台裏！">
  <p class="marginB20">入社して、最初に行われるのが、座学研修です。ここでサービスの内容や基礎知識を身につけていきます。コールセンターによって多少異なりますが、だいたい、アウトバウンドで２～３日、インバウンドで２～３週間行われます。</p>
  </section>

  <section class="sect04">
  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg" class="yellowmiddlecircle_titleimg">STEP</p>
  <p class="yellowmiddlecircle_titlenumber">2</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">実務研修</p>
  </div>  
  <img src="/public/images/ccwork/035/sub_002.jpg" class="imagemargin_B10" alt="隠されたドラマがある！？コールセンターの舞台裏！">
  <p class="marginB20">座学で基礎知識を身に付けたあとは、実務研修に入ります。実際にお客様に電話をかけたり、受電したりします。となりで研修トレーナーが対応のチェックやフォローをしながら独り立ちを目指していきます。<br>だいたいアウトバウンドで2週間、インバウンドで1ケ月ほど行われることが多いです。</p>
  <p class="marginB20">初めてのお客様対応で緊張したり、きついと感じたりする期間でもありますが、ここを乗り越えると晴れてオペレーターとして成績アップを目指していけるので、頑張っていきましょう！</p>

  <img src="/public/images/ccwork/035/sub_003.jpg" class="imagemargin_B10" alt="隠されたドラマがある！？コールセンターの舞台裏！">
  <p class="marginB20">研修が終わったら、いよいよオペレーターデビュー。長い研修を乗り切って、対応力もアップしたので自信もつくはず！</p>
  <p class="marginBottom20px">また、研修期間中は時給が低いところもありますが、ここから正規のコールセンター勤務として時給もアップ、さらに成績を上げてインセンティブの獲得も目指していけるのでモチベーションも高まりますね。</p>
  </section>

  <section class="sect05">
  <h2 class="sideBlueBorder_blueText02 marginBottom10px">まだまだある！<br>コールセンター研修で学べる大事なこと</h2>
  <p class="marginBottom20px">研修で商材の知識やスキルを身につけるだけでは優秀なオペレーターは育ちません。そこで、コールセンターでスタッフの教育に当たっている担当者から、研修で教えている大切な心得について聞いてきました。</p>

  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg" class="yellowmiddlecircle_titleimg">STEP</p>
  <p class="yellowmiddlecircle_titlenumber">1</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">謙虚な気持ち</p>
  </div>  
  <img src="/public/images/ccwork/035/sub_004.jpg" class="imagemargin_B10" alt="隠されたドラマがある！？コールセンターの舞台裏！">
  <p class="marginBottom10px">お電話をかけるときは、お客様の大切な時間をいただいて聞いてもらっているということを忘れないようにしましょう。</p>
  <p class="marginBottom10px">お客様の質問や意見に対して否定的な言い方で返したり、質問に声をかぶせて返したりするのはNG。しっかりとヒアリングしてお客様にとってのメリットをわかりやすく伝えます。</p>
  <p class="marginBottom20px">感情は声で伝わりますから、感謝の気持ちを忘れず、お客様満足度の向上を念頭において業務を遂行することがポイントです。</p>
  </section>

  <section class="sect06">
  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg" class="yellowmiddlecircle_titleimg">STEP</p>
  <p class="yellowmiddlecircle_titlenumber">2</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">大きな声でハキハキと！</p>
  </div>  
  <img src="/public/images/ccwork/035/sub_005.jpg" class="imagemargin_B10" alt="隠されたドラマがある！？コールセンターの舞台裏！">
  <p class="marginBottom20px">小さな声でぼそぼそとしゃべっていては、要点が伝わりにくく、聞いている側にもストレスを与えてしまいます。大切なのは自信を持ってお伝えすることです。<br>大きな声ではっきりと、主旨を伝えることを心がけましょう。</p>
  </section>

  <section class="sect07">
  <div class="yellowmiddlecircle">
  <p class="yellowmiddlecircle_title"><img src="/public/images/ccwork/006/circle_bg-sp.jpg" class="yellowmiddlecircle_titleimg">STEP</p>
  <p class="yellowmiddlecircle_titlenumber">3</p>
  </div>
  <div class="yellowmiddlecircle_titlebox">
  <p class="yellowmiddlecircle_titletextblue">元気よく！</p>
  </div>  
  <img src="/public/images/ccwork/035/sub_006.jpg" class="imagemargin_B10" alt="隠されたドラマがある！？コールセンターの舞台裏！">
  <p class="marginBottom10px">ときには、クレームを受けたり、怒られてしまったり、落ち込むこともあるでしょう。でも、気を取り直してすぐ次に進めるメンタル力を身につけましょう。</p>
  <p class="marginBottom20px">大切なことは、「スキルよりもモチベーション」。強い心で元気良く稼働することが、伸びていくポイントかもしれません。</p>
  </section>

  <section class="sect08">
    <h3 class="blueblockBG">最後に</h3>
  <p class="marginBottom10px">オペレーターの育成はただスキルを教えれば良いというわけではなく、様々な状況に対応でき、問題解決をしていく力を備えた、優秀な人材を育てることが最終目的です。</p>
  <p class="marginBottom10px">スーパーバイザー（SV）と言われる責任者が新人に教えていることが多いですがコールセンターによっては、専門の教育担当を設けているところもあります。</p>
  <p class="marginBottom40px">「覚えることが多くて大変！」なんてこともあるかも知れませんが、オペレーターの皆さんは、困ったことがあったらスーパーバイザーや教育担当に気軽に質問して、問題解決能力を高めていけるように心がけましょう！</p>
  </section>

  <aside>
  <dl>
  <dt class="headingG01">「コールセンター基礎知識」オススメのページ</dt>
  <dd>
  <ul class="linkListA01">
  <li><a href="/ccwork/detail13">NGワードとクッション言葉に気をつけよう！</a></li>
  <li><a href="/ccwork/detail23">日本語を正しく使えていますか？コールセンター業務に必要な丁寧語、謙譲語、尊敬語</a></li>
  </ul></dd>
  </dl>
  </aside>
  
<aside class="tabNav_ccwork">
<ul><li><a href="/ccwork#tabContents01"><img src="/public/images/ccwork/nav/tabnav_01_a.png" alt="基礎知識" class="mediaPC" height="" width="192"><img src="/public/images/ccwork/nav/tabnav_01_03_a.png" alt="基礎知識" class="mediaSP" height="" width="286"></a></li><li><a href="/ccwork#tabContents02"><img src="/public/images/ccwork/nav/tabnav_02_n.png" alt="知っ得情報" class="mediaPC" height="" width="192"><img src="/public/images/ccwork/nav/tabnav_02_03_n.png" alt="知っ得情報" class="mediaSP" height="" width="286"></a></li><li><a href="/ccwork#tabContents03"><img src="/public/images/ccwork/nav/tabnav_03_n.png" alt="働く人の声" class="mediaPC" height="" width="192"><img src="/public/images/ccwork/nav/tabnav_03_03_n.png" alt="働く人の声" class="mediaSP" height="" width="286"></a></li></ul>
</aside><!-- / tabNav -->
  
</section>


