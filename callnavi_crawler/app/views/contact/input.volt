
<section class="unitA01">
<h2 class="headingC02">お問い合わせ</h2>
<p class="marginB30">以下のフォームよりお問い合わせください。担当者から料金やサービスのご案内を差し上げます。通常2営業日以内に対応致しますが、内容によっては返信にお時間をいただく場合がございます。</p>

<form>
<section>
<ol class="stepA01">
<li class="current">STEP1　<br class="mediaSP">情報入力</li>
<li>STEP2　<br class="mediaSP">内容確認</li>
<li>STEP3　<br class="mediaSP">送信完了</li>
</ol>
<div class="generalBlockA01">
<table class="formA01 marginB30">
<tr>
<th>氏名<span class="required">必須</span></th>
<td><em class="error">氏名が入力されていません</em><input type="text"></td>
</tr>
<tr>
<th>メールアドレス<span class="required">必須</span></th>
<td><input type="text"></td>
</tr>
<tr>
<th>問い合わせ種別<span class="required">必須</span></th>
<td>
<ul class="inputList">
<li><label><input type="radio" name="type">求人について</label></li>
<li><label><input type="radio" name="type">Call Naviについて</label></li>
</ul>
</td>
</tr>
<tr>
<th>お問い合わせ内容<span class="required">必須</span></th>
<td><textarea></textarea></td>
</tr>
</table>
<ul class="btnListA01 alignC">
<li><a href="/contact/confirm" class="btnB02"><span>確認画面へ</span></a></li>
</ul>
</div><!-- / generalBlockA01 -->
</section>
</form>
</section>