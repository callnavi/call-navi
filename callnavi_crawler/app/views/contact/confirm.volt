
<section class="unitA01">
    <h2 class="headingC02">お問い合わせ</h2>
    <p class="marginB30">以下のフォームよりお問い合わせください。担当者から料金やサービスのご案内を差し上げます。通常2営業日以内に対応致しますが、内容によっては返信にお時間をいただく場合がございます。</p>

    <form>
        <section>
            <ol class="stepA01">
                <li>STEP1　情<br class="mediaSP">報入力</li>
                <li class="current">STEP2　<br class="mediaSP">内容確認</li>
                <li>STEP3　<br class="mediaSP">送信完了</li>
            </ol>
            <div class="generalBlockA01">
                <table class="formA01 confirm marginB30">
                    <tr>
                        <th>氏名</th>
                        <td>山田　太郎</td>
                    </tr>
                    <tr>
                        <th>メールアドレス</th>
                        <td>yamada@yamada.co,jp</td>
                    </tr>
                    <tr>
                        <th>問い合わせ種別</th>
                        <td>求人掲載について</td>
                    </tr>
                    <tr>
                        <th>お問い合わせ内容</th>
                        <td>お問い合わせ内容お問い合わせ内容お問い合わせ内容お問い合わせ内容お問い合わせ内容お問い合わせ内容</td>
                    </tr>
                </table>
                <ul class="btnListB01 alignC">
                    <li><a href="/templates/tmp_contact_input.html" class="btnB01 left"><span>入力画面へ戻る</span></a></li>
                    <li><a href="/templates/tmp_contact_thanks.html" class="btnB02"><span>送信</span></a></li>
                </ul>
            </div><!-- / generalBlockA01 -->
        </section>
    </form>
</section>
