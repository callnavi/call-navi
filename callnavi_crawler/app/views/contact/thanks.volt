


<section class="unitA01">
<h2 class="headingC02">お問い合わせ</h2>
<p class="marginB30">以下のフォームよりお問い合わせください。担当者から料金やサービスのご案内を差し上げます。通常2営業日以内に対応致しますが、内容によっては返信にお時間をいただく場合がございます。</p>

<form>
<section>
<ol class="stepA01">
<li>STEP1　<br class="mediaSP">情報入力</li>
<li>STEP2　<br class="mediaSP">内容確認</li>
<li class="current">STEP3　<br class="mediaSP">送信完了</li>
</ol>
<div class="generalBlockA01">
<div class="generalBlockB01 uniqueContact01 marginB30">
<h2 class="headingB02 alignC">メッセージ送信完了</h2>
<p class="alignC textA01 fontSize30 message01">お問い合わせ<br class="mediaPC">ありがとうございました。</p>
<p class="alignC message02">内容を確認後、弊社担当者からご連絡させていただきます。<br class="mediaPC">ご入力されたメールアドレス宛に<br class="mediaPC">確認のメールを送信いたしましたのでご確認ください。<br class="mediaPC">尚、一両日経過してもメールが届かない場合には、<br class="mediaPC">ご入力時のメールアドレスが間違っている場合がありますので、<br class="mediaPC">まことに恐縮ですが再度のご連絡をよろしくお願いします。</p>
</div><!-- / generalBlockB01 -->
<ul class="btnListA01 alignC">
<li><a href="/" class="btnB01 left"><span>HOMEへ戻る</span></a></li>
</ul>
</div><!-- / generalBlockA01 -->

</section>
</form>
</section>