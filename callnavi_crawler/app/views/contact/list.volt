<div id="contents">
    <div class="contentsInner">
        <div id="contentsContainer">
            <div id="mainContents">
                <!-- InstanceBeginEditable name="mainContents" -->
                <ul class="fileList">
                    <li><a href="/templates/tmp_index.html" target="_blank">HOME</a></li>

                    <li><a href="/templates/tmp_search_result.html" target="_blank">検索結果</a></li>

                    <li><a href="/templates/tmp_detail.html" target="_blank">求人詳細</a></li>

                    <li><a href="/templates/tmp_about.html" target="_blank">call naviとは</a></li>

                    <li>お問い合わせ
                        <ul>
                            <li><a href="/templates/tmp_contact_input.html" target="_blank">入力</a></li>
                            <li><a href="/templates/tmp_contact_confirm.html" target="_blank">確認</a></li>
                            <li><a href="/templates/tmp_contact_thanks.html" target="_blank">完了</a></li>
                        </ul>
                    </li>

                    <li>求人情報掲載について</li>

                    <li><a href="/templates/tmp_company.html">運営会社</a></li>

                    <li><a href="/templates/tmp_term.html">利用規約</a></li>

                    <li><a href="/templates/module.html" target="_blank">モジュール一覧</a></li>
                </ul>
                <!-- InstanceEndEditable -->
            </div><!-- / mainContents -->

            <div id="subContents">
                <nav id="sideNavigation" class="mediaPC">
                    <ul>
                        <li><a href="#">HOME</a></li>
                        <li><a href="#">コールセンターの<br>仕事とは？</a></li>
                        <li><a href="#">おすすめ求人</a></li>
                    </ul>
                </nav>
                <section>
                    <h2 class="headingA01 topics">トピックス</h2>
                    <ul class="bnrListA01 alignC">
                        <li><a href="#" class="imgHover"><img src="/public/images/common/bnr_about-callnavi_01.png" width="200" height="170" alt="" class="mediaPC"/><img src="/public/images/common/bnr_about-callnavi_01.png" width="136" height="116" alt="" class="mediaSP"/></a></li>
                        <li><a href="#" class="imgHover"><img src="/public/images/common/bnr_melit_01.png" width="200" height="170" alt="" class="mediaPC"/><img src="/public/images/common/bnr_melit_01.png" width="136" height="116" alt="" class="mediaSP"/></a></li>
                        <li><a href="#" class="imgHover"><img src="/public/images/common/bnr_report_01.png" width="200" height="170" alt="" class="mediaPC"/><img src="/public/images/common/bnr_report_01.png" width="136" height="116" alt="" class="mediaSP"/></a></li>
                        <li><a href="#" class="imgHover"><img src="/public/images/common/bnr_help_01.png" width="200" height="170" alt="" class="mediaPC"/><img src="/public/images/common/bnr_help_01.png" width="136" height="116" alt="" class="mediaSP"/></a></li>
                    </ul>
                </section>
            </div><!-- / subContents -->
        </div><!-- / contentsContainer -->

        <div id="asideContents">
            {{ partial('/offer/rectangle')}}

            <section>
                <h2><img src="/public/images/common/ttl_pickup_01.gif" width="200" height="40" alt="Pickup求人" class="pickupTtl"/></h2>
                <article class="pickupB01">
                    <a href="#">
                        <p class="image"><img src="/templates/images/dummy_img_01.png" width="160" height="120" alt=""></p>
                        <h3>≪髪型・服装FREE♪≫月約35万円は普通に稼げるコールセンター♪</h3>
                        <p class="company">株式会社Bestエフォート</p>
                        <p class="summary">★未経験OK!!★週3～/1日4h～勤務OK!!時給＋インセンティブ15万円以上が稼げる環境です♪ノルマはありません。≪フリーターさん、Wワーカーさんも活躍中!!働きやすい職場環境・福利厚生が自慢です!!</p>
                        <table>
                            <tr>
                                <th>給与</th>
                                <td><div>時給 1,500円～2,500円<br>交通費：一部支給<br>月1万円まで支給</div></td>
                            </tr>
                            <tr>
                                <th>勤務地</th>
                                <td><div>東京都新宿区高田馬場2-13-12 Primegate4F</div></td>
                            </tr>
                        </table>
                    </a>
                </article><!-- / pickupB02 -->
            </section>

            <section>
                <h2><img src="/public/images/common/ttl_pickup_01.gif" width="200" height="40" alt="Pickup求人" class="pickupTtl"/></h2>
                <article class="pickupB01">
                    <a href="#">
                        <p class="image"><img src="/templates/images/dummy_img_01.png" width="160" height="120" alt=""></p>
                        <h3>≪髪型・服装FREE♪≫月約35万円は普通に稼げるコールセンター♪</h3>
                        <p class="company">株式会社Bestエフォート</p>
                        <p class="summary">★未経験OK!!★週3～/1日4h～勤務OK!!時給＋インセンティブ15万円以上が稼げる環境です♪ノルマはありません。≪フリーターさん、Wワーカーさんも活躍中!!働きやすい職場環境・福利厚生が自慢です!!</p>
                        <table>
                            <tr>
                                <th>給与</th>
                                <td><div>時給 1,500円～2,500円<br>交通費：一部支給<br>月1万円まで支給</div></td>
                            </tr>
                            <tr>
                                <th>勤務地</th>
                                <td><div>東京都新宿区高田馬場2-13-12 Primegate4F</div></td>
                            </tr>
                        </table>
                    </a>
                </article><!-- / pickupB02 -->
            </section>

            {{ partial('/offer/pickup_logo_banners')}}

        </div><!-- / asideContents -->

        <div id="pageTop"><a href="#globalHeader" class="smoothScroll">globalHeader</a></div>
    </div><!-- / contentsInner -->
</div><!-- / contents -->