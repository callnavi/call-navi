<div is_mail_content="true">
━━━━━━━━━━━━━━━━
求人広告内容の非承認のお知らせ
━━━━━━━━━━━━━━━━
※本メールは『コールナビ』より自動送信されています。

{{account.representative}}様

この度は『コールナビ』をご利用いただき、
誠にありがとうございます。

大変申し訳ございませんが、
受付した求人広告について非承認とさせていただきました。

＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
広告名：{{offer.title}}

受付日時：{{date.year}}年{{date.month}}月{{date.day}}日　{{date.time}}

広告種別：{{offer_type}}広告
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝

【非承認理由】
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
{% if reason01 == 1 %}
事業内容・募集内容が法令に抵触するため
{% endif %}
{% if reason02 == 1 %}
均等な雇用機会を損なうおそれがあると認められるため
{% endif %}
{% if reason03 == 1 %}
社会倫理・社会秩序に反すると認められるため
{% endif %}
{% if reason04 == 1 %}
利用者に不利益を与えるため
{% endif %}
{% if reason05 == 1 %}
予め提供する意思のない労働条件を表示するため
{% endif %}
その他
<?php $comment = str_replace("¥n","\r\n",$comment); $comment = mb_convert_encoding($comment, "UTF-8", "auto"); ?>
{{ comment }}

＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝

ご確認の上、再度求人広告掲載システムより、
求人広告内容を変更していただけますよう、お願い申し上げます。
{{ link }} 


本メールについてご不明な点や心当たりが無い場合は、大変お手数ですが、
下記の『お問い合わせ先』までご連絡ください。
━━━━━━━━━━━━━━━━
全国のコールセンター転職・求人一括検索！
アルバイト・中途・新卒もおまかせ！
『コールナビ』
http://callnavi.jp/
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
【お問い合わせ先】
株式会社コールナビ
Eメール：info@callnavi.jp
━━━━━━━━━━━━━━━━
</div is_mail_content="true">