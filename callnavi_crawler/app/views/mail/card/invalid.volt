<div is_mail_content="true">
------------
{{account.representative}} 様

この度は『CALL Navi』をご利用いただき、
誠にありがとうございます。

ご登録のクレジットカードでは、セキュリティ認証ができませんでした。
再度セキュリティコードをご確認の上、クレジットカードの再登録をお願いいたします。

http://staging.callnavi.jp/credit/input

＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝
広告名：広告名
受付日時：2015年4月22日　18:20
広告種別：リスティング広告
＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝＝

------------
</div is_mail_content="true">