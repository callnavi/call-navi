<section>
    <article class="jobDetailA01">
        <header>
            <h3><a href="#" target="_blank">Webディレクター</a></h3>
            <p class="date">更新日：0000年00月00日</p>
        </header>
        <div class="articleBody">
            <div class="featureA01 js-slider">
                <a href="#">
                    <div class="slidesContainer">
                        <div class="crossfade">
                            <ul class="slides">
                                <li><img src="/images/dummy/dummy_slide_01.png" width="720" height="370" alt="" class="pc"><img src="/images/dummy/dummy_slide_01.png" alt="" class="sp"></li>
                                <li><img src="/images/dummy/dummy_slide_02.png" width="720" height="370" alt="" class="pc"><img src="/images/dummy/dummy_slide_02.png" alt="" class="sp"></li>
                                <li><img src="/images/dummy/dummy_slide_03.png" width="720" height="370" alt="" class="pc"><img src="/images/dummy/dummy_slide_03.png" alt="" class="sp"></li>
                            </ul>
                        </div>
                    </div>
                </a>

                <div class="slideControl">
                    <ul class="select">
                        <li><a href="javascript:;">1</a></li>
                        <li><a href="javascript:;">2</a></li>
                        <li><a href="javascript:;">3</a></li>
                    </ul>
                </div>
            </div><!-- /featureA01 -->
            <p class="company">株式会社メタフェイズ</p>
            <p class="summary">自己のスキルアップを目指す方の応募をぜひお待ちしております！</p>
            <div class="introduction">クライアントとコミュニケーションをとりながら成功するWebサイトへと導きます。<br>メタフェイズのWebディレクターの具体的な業務は以下となります。 <br><br>○プロジェクトの統括<br>○進捗管理<br>○品質管理<br>○Webサイトの構造設計・画面設計<br><br>入社後はまず先輩ディレクターについて仕事の流れなどを覚え、各自のスキルにあわせて徐々にプロジェクトを担当していきます。</div>
            <table class="tableA01 description">
                <col style="width:160px;">
                <col>
                <tr>
                    <th>募集職種</th>
                    <td>テキストテキストテキストテキスト</td>
                </tr>
                <tr>
                    <th>募集背景</th>
                    <td>テキストテキストテキストテキスト</td>
                </tr>
                <tr>
                    <th>仕事内容</th>
                    <td>テキストテキストテキストテキスト</td>
                </tr>
                <tr>
                    <th>雇用形態</th>
                    <td>テキストテキストテキストテキスト</td>
                </tr>
                <tr>
                    <th>タグ</th>
                    <td class="tagsContainer">
                        <ul class="tags">
                            <li>正社員</li>
                            <li>正社員</li>
                            <li>正社員</li>
                            <li>正社員</li>
                            <li>正社員</li>
                            <li>正社員</li>
                            <li>正社員</li>
                            <li>正社員</li>
                            <li>正社員</li>
                            <li>正社員</li>
                            <li>正社員</li>
                            <li>正社員</li>
                            <li>正社員</li>
                            <li>正社員</li>
                            <li>正社員</li>
                        </ul>
                    </td>
                </tr>
            </table>
            <!-- /detail -->
        </div><!-- /articleBody -->
        <footer>
            <p><a href="#" target="_blank" class="buttonA01"><span class="extarnal">求人詳細を見る</span></a></p>
        </footer>
    </article>
</section>

<div id="sideContents" class="mediaPC">

    <section>
        <h2 class="headingA01">広告</h2>
        <div><img src="/images/dummy/dummy_adsense_01.png" width="233" height="733" alt=""/></div>
    </section>

    <aside class="ad">
        <div><img src="/images/common/dummy_ad_234-60.gif" width="234" height="60" alt=""/></div>
        <div><img src="/images/common/dummy_ad_120-600.gif" width="120" height="600" alt=""/></div>
    </aside>

</div>
