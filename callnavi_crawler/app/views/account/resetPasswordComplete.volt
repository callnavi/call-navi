<div id="contents">
<div id="contentsContainer">
<!-- InstanceBeginEditable name="mainContents" -->
<form method="post" action="#">
<div class="unitA01">
<h2 class="headingA01">パスワードをお忘れの方</h2>

<p class="alignC marginB200">ご記入されたメールアドレスに<br>「パスワード再設定用のご案内」を送信いたしました。</p>
<p class="alignC marginB200">メールに記載されているURLをクリックして、<br>
パスワードを再設定してください。</p>
<p class="alignC"><a href="/register/login">アカウントログインへ</a></p>
</div><!-- /.unitA01 -->
</form>
<!-- InstanceEndEditable -->
</div><!-- / contentsContainer -->
</div><!-- / contents -->