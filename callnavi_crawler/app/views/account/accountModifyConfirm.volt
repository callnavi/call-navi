<div id="contents">
<div id="contentsContainer">
<!-- InstanceBeginEditable name="mainContents" -->

<div class="unitA01">
<h2 class="headingA01">登録情報の変更・解約</h2>
<div class="headingB01">
<h3>入力情報のご確認</h3>
</div>
<table class="tableB01 marginB30">
<tr>
<th class="ttl">会社名</th>
<td class="ipt">{{data['company_name']}}</td>
</tr>
<tr>
<th class="ttl">会社名フリガナ</th>
<td class="ipt">{{data['company_name_kana']}}</td>
</tr>
<tr>
<th class="ttl">郵便番号</th>
<td class="ipt">{{data['post_A']}}-{{data['post_B']}}</td>
</tr>
<tr>
<th class="ttl">住所</th>
<td class="ipt">{{data['address']}}</td>
</tr>
<tr>
<th class="ttl">電話番号</th>
<td class="ipt">{{data['tel_A']}}-{{data['tel_B']}}-{{data['tel_C']}}</td>
</tr>
<tr>
<th class="ttl">FAX番号</th>
<td class="ipt">{{data['fax_A']}}-{{data['fax_B']}}-{{data['fax_C']}}</td>
</tr>
<tr>
<th class="ttl">会社URL</th>
<td class="ipt">http://{{data['company_url']}}</td>
</tr>
<tr>
<th class="ttl">所属部署</th>
<td class="ipt">{{data['company_section']}}</td>
</tr>
<tr>
<th class="ttl">役職</th>
<td class="ipt">{{data['company_position']}}</td>
</tr>
<tr>
<th class="ttl">ご担当者名</th>
<td class="ipt">{{data['representative_family']}}{{data['representative_given']}}</td>
</tr>
<tr>
<th class="ttl">ご担当者名フリガナ</th>
<td class="ipt">{{data['representative_family_kana']}}{{data['representative_given_kana']}}</td>
</tr>
<tr>
<th class="ttl">Eメールアドレス<br>（ログインID）</th>
<td class="ipt">{{data['email']}}</td>
</tr>
<tr>
<th class="ttl">パスワード</th>
<td class="ipt"><div class="error" >※パスワード情報は非表示となっております。</div></td>
</tr>
<tr>
<th class="ttl">セキュリティ</th>
<td class="ipt">
<ul class="formNameArea">
<li class="w90">秘密の質問：</li>
<li>{{data['security_question_display']}}</li>
<li>答え：</li>
<li>{{data['security_question_answer']}}</li>
</ul>
</td>
</tr>

</table>

<div class="alignC">
<div style="display:inline-flex">
<form method="post" action="/account/accountModifyStart">
<input type="hidden" name="company_name" value="{{data['company_name']}}">
<input type="hidden" name="company_name_kana" value="{{data['company_name_kana']}}">
<input type="hidden" name="post_A" value="{{data['post_A']}}">
<input type="hidden" name="post_B" value="{{data['post_B']}}">
<input type="hidden" name="address" value="{{data['address']}}">
<input type="hidden" name="tel_A" value="{{data['tel_A']}}">
<input type="hidden" name="tel_B" value="{{data['tel_B']}}">
<input type="hidden" name="tel_C" value="{{data['tel_C']}}">
<input type="hidden" name="fax_A" value="{{data['fax_A']}}">
<input type="hidden" name="fax_B" value="{{data['fax_B']}}">
<input type="hidden" name="fax_C" value="{{data['fax_C']}}">
<input type="hidden" name="company_url" value="{{data['company_url']}}">
<input type="hidden" name="company_section" value="{{data['company_section']}}">
<input type="hidden" name="company_position" value="{{data['company_position']}}">
<input type="hidden" name="representative_family" value="{{data['representative_family']}}">
<input type="hidden" name="representative_given" value="{{data['representative_given']}}">
<input type="hidden" name="representative_family_kana" value="{{data['representative_family_kana']}}">
<input type="hidden" name="representative_given_kana" value="{{data['representative_given_kana']}}">
<input type="hidden" name="email" value="{{data['email']}}" />
<input type="hidden" name="password" value="{{data['password']}}">
<input type="hidden" name="security_question_id" value="{{data['security_question_id']}}">
<input type="hidden" name="security_question_answer" value="{{data['security_question_answer']}}">
<p class="alignC"><button class="btnA01">入力内容を修正する</button></p>
</form>

&nbsp;&emsp;<form method="post" action="/account/accountModifyComplete">
<input type="hidden" name="company_name" value="{{data['company_name']}}" />
<input type="hidden" name="company_name_kana" value="{{data['company_name_kana']}}" />
<input type="hidden" name="post_A" value="{{data['post_A']}}" />
<input type="hidden" name="post_B" value="{{data['post_B']}}" />
<input type="hidden" name="address" value="{{data['address']}}" />
<input type="hidden" name="tel_A" value="{{data['tel_A']}}" />
<input type="hidden" name="tel_B" value="{{data['tel_B']}}" />
<input type="hidden" name="tel_C" value="{{data['tel_C']}}" />
<input type="hidden" name="fax_A" value="{{data['fax_A']}}" />
<input type="hidden" name="fax_B" value="{{data['fax_B']}}" />
<input type="hidden" name="fax_C" value="{{data['fax_C']}}" />
<input type="hidden" name="company_url" value="{{data['company_url']}}" />
<input type="hidden" name="company_section" value="{{data['company_section']}}" />
<input type="hidden" name="company_position" value="{{data['company_position']}}" />
<input type="hidden" name="representative_family" value="{{data['representative_family']}}" />
<input type="hidden" name="representative_given" value="{{data['representative_given']}}" />
<input type="hidden" name="representative_family_kana" value="{{data['representative_family_kana']}}" />
<input type="hidden" name="representative_given_kana" value="{{data['representative_given_kana']}}" />
<input type="hidden" name="email" value="{{data['email']}}" />
<input type="hidden" name="password" value="{{data['password']}}" />
<input type="hidden" name="security_question_id" value="{{data['security_question_id']}}" />
<input type="hidden" name="security_question_answer" value="{{data['security_question_answer']}}" />

<input type="hidden" name="postal_code" value="{{data['post_A']}}-{{data['post_B']}}" />
<input type="hidden" name="telephone" value="{{data['tel_A']}}-{{data['tel_B']}}-{{data['tel_C']}}" />
<input type="hidden" name="fax_code" value="{{data['fax_A']}}-{{data['fax_B']}}-{{data['fax_C']}}" />
<input type="hidden" name="representative" value="{{data['representative_family']}}　{{data['representative_given']}}" />
<input type="hidden" name="representative_kana" value="{{data['representative_family_kana']}}　{{data['representative_given_kana']}}" />
<p class="alignC"><button class="btnA02">この内容で登録する</button></p>
</form>

</div>

</div><!-- /.unitA01 -->
<!-- InstanceEndEditable -->
</div><!-- / contentsContainer -->
</div><!-- / contents -->
