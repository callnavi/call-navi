<div id="contents">
<div id="contentsContainer">
<!-- InstanceBeginEditable name="mainContents" -->
<form method="post" id="account_delete_form" action="/account/accountModifyDeleteThanks">
<div class="unitA01">
<h2 class="headingA01">登録情報の確認・変更</h2>
<div class="headingB01">
<h3>アカウントの解約</h3>
</div>

<p class="alignC">解約された場合は、すべての情報が削除されます。<br>
掲載中の広告がある場合は、解約時点で強制的に掲載終了されますのでご注意ください。</p>
<p class="alignC marginB30">本当に解約してもよろしいでしょうか？</p>

<div class="alignC">

<input type="hidden" name="account_id" value="{{basic.id}}">
<button class="btnA04">このアカウントを解約する</button>

</div>

</div><!-- /.unitA01 -->
</form>
<!-- InstanceEndEditable -->
</div><!-- / contentsContainer -->
</div><!-- / contents -->

