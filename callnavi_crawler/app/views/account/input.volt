<div id="contents">
<div id="contentsContainer">
<!-- InstanceBeginEditable name="mainContents" -->
<form id="account_form" method="post" action="/account/confirm">
<div class="unitA01">
<h2 class="headingA01">新規アカウント作成</h2>
<div class="headingB01">
<h3>アカウント情報の入力</h3>
</div>
<span class="inputError"></span>
<table class="tableB01">
<tr class="company_name">
<th class="ttl">会社名</th>
<th class="nsc"><span class="must">必須</span></th>
<td class="ipt"><input type="text" name="company_name" class="inputA01 marginB05" value="{% if data['company_name'] is defined %}{{data['company_name']}}{% else %}{% endif %}" maxlength="30">
<span class="fontSize12">※30文字以内でご記入ください。</span>
</td>
</tr>
<tr class="company_name_kana">
<th class="ttl">会社名フリガナ</th>
<th class="nsc"><span class="must">必須</span></th>
<td class="ipt"><input type="text" name="company_name_kana" class="inputA01" value="{% if data['company_name_kana'] is defined %}{{data['company_name_kana']}}{% else %}{% endif %}"></td>
</tr>
<tr class="postal_code">
<th class="ttl">郵便番号</th>
<th class="nsc"><span class="must">必須</span></th>
<td class="ipt">
<ul class="formPostArea marginB05">
<li><input type="text" name="post_A" value="{% if data['post_A'] is defined %}{{data['post_A']}}{% else %}{% endif %}" maxlength="3"></li>
<li>-</li>
<li><input type="text" name="post_B" value="{% if data['post_B'] is defined %}{{data['post_B']}}{% else %}{% endif %}" maxlength="4"></li>
</ul>
<input type="hidden" name="postal_code"/>
<span class="fontSize12">※半角数字でご記入ください。</span>
</td>
</tr>
<tr class="address">
<th class="ttl">住所</th>
<th class="nsc"><span class="must">必須</span></th>
<td class="ipt"><input type="text" name="address" class="inputA01 marginB05" value="{% if data['address'] is defined %}{{data['address']}}{% else %}{% endif %}"><br>
<span class="fontSize12">※市区町村・番地・建物名までご記入ください。</span></td>
</tr>
<tr class="telephone">
<th class="ttl">電話番号</th>
<th class="nsc"><span class="must">必須</span></th>
<td class="ipt">
<ul class="formTelArea marginB05">
<li><input type="text" name="tel_A" value="{% if data['tel_A'] is defined %}{{data['tel_A']}}{% else %}{% endif %}" maxlength="4"></li>
<li>-</li>
<li><input type="text" name="tel_B" value="{% if data['tel_B'] is defined %}{{data['tel_B']}}{% else %}{% endif %}" maxlength="4"></li>
<li>-</li>
<li><input type="text" name="tel_C" value="{% if data['tel_C'] is defined %}{{data['tel_C']}}{% else %}{% endif %}" maxlength="4"></li>
</ul>
<input type="hidden" name="telephone"/>
<span class="fontSize12">※半角数字でご記入ください。</span>
</td>
</tr>
<tr class="fax_code">
<th class="ttl">FAX番号</th>
<th class="nsc"><span class="option">任意</span></th>
<td class="ipt">
<ul class="formTelArea marginB05">
<li><input type="text" name="fax_A" value="{% if data['fax_A'] is defined %}{{data['fax_A']}}{% else %}{% endif %}" maxlength="4"></li>
<li>-</li>
<li><input type="text" name="fax_B" value="{% if data['fax_B'] is defined %}{{data['fax_B']}}{% else %}{% endif %}" maxlength="4"></li>
<li>-</li>
<li><input type="text" name="fax_C" value="{% if data['fax_C'] is defined %}{{data['fax_C']}}{% else %}{% endif %}" maxlength="4"></li>
</ul>
<input type="hidden" name="fax_code"/>
<span class="fontSize12">※半角数字でご記入ください。</span>
</td>
</tr>
<tr class="company_url">
<th class="ttl">会社URL</th>
<th class="nsc"><span class="option">任意</span></th>
<td class="ipt">
<ul class="formUrlArea marginB05">
<li><input type="text" name="company_url" class="inputA01" value="{% if data['company_url'] is defined %}{{data['company_url']}}{% else %}{% endif %}"></li>
</ul>
<span class="fontSize12">※半角英数字でご記入ください。<br>※http://から記載してください。</span>
</td>
</tr>
<tr class="company_section">
<th class="ttl">所属部署</th>
<th class="nsc"><span class="option">任意</span></th>
<td class="ipt"><input type="text" name="company_section" class="inputA01" value="{% if data['company_section'] is defined %}{{data['company_section']}}{% else %}{% endif %}"></td>
</tr>
<tr class="company_position">
<th class="ttl">役職</th>
<th class="nsc"><span class="option">任意</span></th>
<td class="ipt"><input type="text" name="company_position" class="inputA01" value="{% if data['company_position'] is defined %}{{data['company_position']}}{% else %}{% endif %}"></td>
</tr>
<tr class="representative">
<th class="ttl">ご担当者名</th>
<th class="nsc"><span class="must">必須</span></th>
<td class="ipt">
<ul class="formNameArea">
<li>姓</li>
<li><input type="text" name="representative_family" value="{% if data['representative_family'] is defined %}{{data['representative_family']}}{% else %}{% endif %}"></li>
<li>名</li>
<li><input type="text" name="representative_given" value="{% if data['representative_given'] is defined %}{{data['representative_given']}}{% else %}{% endif %}"></li>
</ul>
<input type="hidden" name="representative"/>
</td>
</tr>
<tr class="representative_kana">
<th class="ttl">ご担当者名フリガナ</th>
<th class="nsc"><span class="must">必須</span></th>
<td class="ipt">
<ul class="formNameArea">
<li>セイ</li>
<li><input type="text" name="representative_family_kana" value="{% if data['representative_family_kana'] is defined %}{{data['representative_family_kana']}}{% else %}{% endif %}"></li>
<li>メイ</li>
<li><input type="text" name="representative_given_kana" value="{% if data['representative_given_kana'] is defined %}{{data['representative_given_kana']}}{% else %}{% endif %}"></li>
</ul>
<input type="hidden" name="representative_kana"/>
</td>
</tr>
<tr class="email">
<th class="ttl">Eメールアドレス<br>（ログインID）</th>
<th class="nsc"><span class="must">必須</span></th>
<td class="ipt"><input type="text" name="email" class="inputA01 marginB05" value="{% if data['email'] is defined %}{{data['email']}}{% else %}{% endif %}">
<span class="fontSize12">※半角英数字でご記入ください。</span>
</td>
</tr>
</table>
</div><!-- /.unitA01 -->

<div class="unitA01">
<div class="headingB01">
<h3>パスワードの入力</h3>
</div>
<table class="tableB01">
<tr class="password">
<th class="ttl">パスワード</th>
<th class="nsc"><span class="must">必須</span></th>
<td class="ipt"><input type="password" name="password" class="inputA02 marginB05" value="{% if data['password'] is defined %}{{data['password']}}{% endif %}"><br>
<span class="fontSize12">※半角英数字、6文字以上12文字未満</span></td>
</tr>
<tr class="password_confirm">
<th class="ttl">パスワード（確認用）</th>
<th class="nsc"><span class="must">必須</span></th>
<td class="ipt"><input type="password" name="password_confirm" class="inputA02" value="{% if data['password'] is defined %}{{data['password']}}{% endif %}"></td>
</tr>
<tr class="security">
<th class="ttl">セキュリティ</th>
<th class="nsc"><span class="must">必須</span></th>
<td class="ipt">
<ul class="formNameArea">
<li class="w90">秘密の質問</li>
<li>
<select name="security_question_id">
<option value="0"{% if data['security_question_id'] is defined and data['security_question_id']=="0" %}selected{% else %}{% endif %}>初めて飼ったペットの名前は？</option>
<option value="1"{% if data['security_question_id'] is defined and data['security_question_id']=="1" %}selected{% else %}{% endif %} >母親の旧姓は？</option>
<option value="2"{% if data['security_question_id'] is defined and data['security_question_id']=="2" %}selected{% else %}{% endif %}>子どものころのあだ名は？</option>
<option value="3"{% if data['security_question_id'] is defined and data['security_question_id']=="3" %}selected{% else %}{% endif %}>一番嫌いな食べ物は？</option>
<option value="4"{% if data['security_question_id'] is defined and data['security_question_id']=="4" %}selected{% else %}{% endif %}>初めて行った海外の国は？</option>
<option value="5"{% if data['security_question_id'] is defined and data['security_question_id']=="5" %}selected{% else %}{% endif %}>一番好きなアーティストは？</option>
</select>
</li>
</li>
<li>答え</li>
<li><input type="text" name="security_question_answer" value="{% if data['security_question_answer'] is defined %}{{data['security_question_answer']}}{% else %}{% endif %}"></li>
</ul><br>
<span class="fontSize12">※パスワードを忘れて再設定する際に利用します。</span>
</td>
</tr>
</table>
<p class="marginB10 alignC">下記の【利用規約・プライバシーポリシー】をご一読の上、同意いただけますようお願いいたします。</p>
<p class="marginB30 alignC"><a href="/customer/contract" target="_blank">利用規約・プライバシーポリシーを開く</a><br>（新しい画面で表示されます）</p>
<p class="alignC"><button type="submit" class="btnB01 fontSize20">利用規約・プライバシーポリシーに<br>
同意して『確認画面』へ進む</button></p>
</div><!-- /.unitA01 -->
</form>
<!-- InstanceEndEditable -->
</div><!-- / contentsContainer -->
</div><!-- / contents -->