
<div id="contents">
<div id="contentsContainer">
<!-- InstanceBeginEditable name="mainContents" -->
<div class="unitA01">
<h2 class="headingA01">登録情報の変更・解約</h2>

<p class="fontSize14 alignC marginB40">アカウントを解約してログアウトいたしました。<br>
ご利用ありがとうございました</p>
<p class="alignC"><a href="/account/index">トップページへ</a></p>
</div><!-- /.unitA01 -->
<!-- InstanceEndEditable -->
</div><!-- / contentsContainer -->
</div><!-- / contents -->