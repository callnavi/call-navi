<div id="contents">
<div id="contentsContainer">
<!-- InstanceBeginEditable name="mainContents" -->
<form method="post" id="account_modify_form" action="/account/accountModifyConfirm">
<div class="unitA01">

<h2 class="headingA01">登録情報の確認・変更</h2>
<div class="headingB01">
<h3>登録情報の変更</h3>
</div>
<span class="inputError"></span>
<table class="tableB01">
<tr class="company_name">
<th class="ttl">会社名</th>
<th class="nsc"><span class="must">必須</span></th>
<td class="ipt"><input type="text" name="company_name" class="inputA01" value="{% if data['company_kana'] is defined %}{{data['company_name']}}{% else %}{{basic.company_name}}{% endif %}"></td>
</tr>
<tr class="company_name_kana">
<th class="ttl">会社名フリガナ</th>
<th class="nsc"><span class="must">必須</span></th>
<td class="ipt"><input type="text" name="company_name_kana" class="inputA01" value="{% if data['company_name_kana'] is defined %}{{data['company_name_kana']}}{% else %}{{basic.company_name_kana}}{% endif %}"></td>
</tr>
<tr class="postcode">
<th class="ttl">郵便番号</th>
<th class="nsc"><span class="must">必須</span></th>
<td class="ipt">
<ul class="formPostArea">
<li><input type="text" name="post_A" value="{% if data['post_A'] is defined %}{{data['post_A']}}{% else %}{{basic.post_A}}{% endif %}"></li>
<li>-</li>
<li><input type="text" name="post_B" value="{% if data['post_B'] is defined %}{{data['post_B']}}{% else %}{{basic.post_B}}{% endif %}"></li>
</ul>
</td>
</tr>
<tr class="address">
<th class="ttl">住所</th>
<th class="nsc"><span class="must">必須</span></th>
<td class="ipt"><input type="text" name="address" class="inputA01 marginB05" value="{% if data['address'] is defined %}{{data['address']}}{% else %}{{basic.address}}{% endif %}"><br>
<span class="fontSize12">※市区町村・番地・建物名までご記入ください</span></td>
</tr>
<tr class="tel">
<th class="ttl">電話番号</th>
<th class="nsc"><span class="must">必須</span></th>
<td class="ipt">
<ul class="formTelArea">
<li><input type="text" name="tel_A" value="{% if data['tel_A'] is defined %}{{data['tel_A']}}{% else %}{{basic.tel_A}}{% endif %}"></li>
<li>-</li>
<li><input type="text" name="tel_B" value="{% if data['tel_B'] is defined %}{{data['tel_B']}}{% else %}{{basic.tel_B}}{% endif %}"></li>
<li>-</li>
<li><input type="text" name="tel_C" value="{% if data['tel_C'] is defined %}{{data['tel_C']}}{% else %}{{basic.tel_C}}{% endif %}"></li>
</ul>
</td>
</tr>
<tr class="fax">
<th class="ttl">FAX番号</th>
<th class="nsc"><span class="option">任意</span></th>
<td class="ipt">
<ul class="formTelArea">
<li><input type="text" name="fax_A" value="{% if data['fax_A'] is defined %}{{data['fax_A']}}{% else %}{{basic.fax_A}}{% endif %}"></li>
<li>-</li>
<li><input type="text" name="fax_B" value="{% if data['fax_B'] is defined %}{{data['fax_B']}}{% else %}{{basic.fax_B}}{% endif %}"></li>
<li>-</li>
<li><input type="text" name="fax_C" value="{% if data['fax_C'] is defined %}{{data['fax_C']}}{% else %}{{basic.fax_C}}{% endif %}"></li>
</ul>
</td>
</tr>
<tr class="company_url">
<th class="ttl">会社URL</th>
<th class="nsc"><span class="option">任意</span></th>
<td class="ipt">
<ul class="formUrlArea">
{#<li>http://</li>#}
<li><input type="text" name="company_url" class="inputA01" value="{% if data['company_url'] is defined %}{{data['company_url']}}{% else %}{{basic.company_url}}{% endif %}"></li>
</ul>
</td>
</tr>
<tr class="company_section">
<th class="ttl">所属部署</th>
<th class="nsc"><span class="option">任意</span></th>
<td class="ipt"><input type="text" name="company_section" class="inputA01" value="{% if data['company_section'] is defined %}{{data['company_section']}}{% else %}{{basic.company_section}}{% endif %}"></td>
</tr>
<tr class="company_position">
<th class="ttl">役職</th>
<th class="nsc"><span class="option">任意</span></th>
<td class="ipt"><input type="text" name="company_position" class="inputA01" value="{% if data['company_position'] is defined %}{{data['company_position']}}{% else %}{{basic.company_position}}{% endif %}"></td>
</tr>
<tr class="representative">
<th class="ttl">ご担当者名</th>
<th class="nsc"><span class="must">必須</span></th>
<td class="ipt">
<ul class="formNameArea">
<li>姓</li>
<li><input type="text" name="representative_family" value="{% if data['representative_family'] is defined %}{{data['representative_family']}}{% else %}{{basic.representative_family}}{% endif %}"></li>
<li>名</li>
<li><input type="text" name="representative_given" value="{% if data['representative_given'] is defined %}{{data['representative_given']}}{% else %}{{basic.representative_given}}{% endif %}"></li>
</ul>
</td>
</tr>
<tr class="representative_kana">
<th class="ttl">ご担当者名フリガナ</th>
<th class="nsc"><span class="must">必須</span></th>
<td class="ipt">
<ul class="formNameArea">
<li>セイ</li>
<li><input type="text" name="representative_family_kana" value="{% if data['representative_family_kana'] is defined %}{{data['representative_family_kana']}}{% else %}{{basic.representative_family_kana}}{% endif %}"></li>
<li>メイ</li>
<li><input type="text" name="representative_given_kana" value="{% if data['representative_given_kana'] is defined %}{{data['representative_given_kana']}}{% else %}{{basic.representative_given_kana}}{% endif %}"></li>
</ul>
</td>
</tr>
<tr class="email">
<th class="ttl">Eメールアドレス<br>（ログインID）</th>
<th class="nsc"><span class="must">必須</span></th>
<td class="ipt"><input type="text" name="email" class="inputA01" value="{% if data['email'] is defined %}{{data['email']}}{% else %}{{basic.email}}{% endif %}"></td>
</tr>
</table>
</div><!-- /.unitA01 -->

<div class="unitA01">
<div class="headingB01">
<h3>パスワードの変更</h3>
</div>
<table class="tableB01 marginB30">
<tr class="password">
<th class="ttl">パスワード</th>
<th class="nsc"><span class="must">必須</span></th>
<td class="ipt"><input type="password" name="password" class="inputA02 marginB05" value="{% if data['password'] is defined %}{{data['password']}}{% endif %}"><br>
<span class="fontSize12">※半角英数字、6文字以上12文字未満</span></td>
</tr>
<tr class="password_confirm">
<th class="ttl">パスワード（確認用）</th>
<th class="nsc"><span class="must">必須</span></th>
<td class="ipt"><input type="password" name="password_confirm" class="inputA02" value="{% if data['password'] is defined %}{{data['password']}}{% endif %}"></td>
</tr>
<tr class="security">
<th class="ttl">セキュリティ</th>
<th class="nsc"><span class="must">必須</span></th>
<td class="ipt">
<ul class="formNameArea">
<li class="w90">秘密の質問</li>

<li>
<select name="security_question_id">
<option value="0"{% if data['security_question_id'] is defined and data['security_question_id']=="0" %}selected{% elseif basic.security_question_id=="0" %}selected{% else %}{% endif %}>初めて飼ったペットの名前は？</option>
<option value="1"{% if data['security_question_id'] is defined and data['security_question_id']=="1" %}selected{% elseif  basic.security_question_id=="1" %}selected{% else %}{% endif %}>母親の旧姓は？</option>
<option value="2"{% if data['security_question_id'] is defined and data['security_question_id']=="2" %}selected{% elseif  basic.security_question_id=="2" %}selected{% else %}{% endif %}>子どものころのあだ名は？</option>
<option value="3"{% if data['security_question_id'] is defined and data['security_question_id']=="3" %}selected{% elseif  basic.security_question_id=="3" %}selected{% else %}{% endif %}>一番嫌いな食べ物は？</option>
<option value="4"{% if data['security_question_id'] is defined and data['security_question_id']=="4" %}selected{% elseif  basic.security_question_id=="4" %}selected{% else %}{% endif %}>初めて行った海外の国は？</option>
<option value="5"{% if data['security_question_id'] is defined and data['security_question_id']=="5" %}selected{% elseif basic.security_question_id=="5" %}selected{% else %}{% endif %}>一番好きなアーティストは？</option>
</select></li>
<li>答え</li>
<li><input type="text" name="security_question_answer" value="{% if data['security_question_answer'] is defined %}{{data['security_question_answer']}}{% else %}{{basic.security_question_answer}}{% endif %}"></li>
</ul><br>
<span class="fontSize12">※パスワードを忘れて再設定する際に利用します。</span>
<input type=hidden name="security_question_display"/>
</td>
</tr>
</table>
<div class="alignC">
<button class="btnA02">確認画面へ進む</button>
</div>
</div><!-- /.unitA01 -->
</form>
<!-- InstanceEndEditable -->
</div><!-- / contentsContainer -->
</div><!-- / contents -->
