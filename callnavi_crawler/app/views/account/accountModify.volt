<div id="contents">
<div id="contentsContainer">
<!-- InstanceBeginEditable name="mainContents" -->
<form method="post" action="#">
<div class="unitA01">
<h2 class="headingA01">登録情報の確認・変更</h2>
<div class="headingB01">
<h3>登録情報の確認</h3>
</div>
<table class="tableB01 marginB30">
<tr>
<th class="ttl">会社名</th>
<td class="ipt">{{basic.company_name}}</td>
</tr>
<tr>
<th class="ttl">会社名フリガナ</th>
<td class="ipt">{{basic.company_name_kana}}</td>
</tr>
<tr>
<th class="ttl">郵便番号</th>
<td class="ipt">{{basic.postal_code}}</td>
</tr>
<tr>
<th class="ttl">住所</th>
<td class="ipt">{{basic.address}}</td>
</tr>
<tr>
<th class="ttl">電話番号</th>
<td class="ipt">{{basic.telephone}}</td>
</tr>
<tr>
<th class="ttl">FAX番号</th>
<td class="ipt">{{basic.fax_code}}</td>
</tr>
<tr>
<th class="ttl">会社URL</th>
<td class="ipt">{#http://#}{{basic.company_url}}</td>
</tr>
<tr>
<th class="ttl">所属部署</th>
<td class="ipt">{{basic.company_section}}</td>
</tr>
<tr>
<th class="ttl">役職</th>
<td class="ipt">{{basic.company_position}}</td>
</tr>
<tr>
<th class="ttl">ご担当者名</th>
<td class="ipt">{{basic.representative}}</td>
</tr>
<tr>
<th class="ttl">ご担当者名フリガナ</th>
<td class="ipt">{{basic.representative_kana}}</td>
</tr>
<tr>
<th class="ttl">Eメールアドレス<br>（ログインID）</th>
<td class="ipt">{{basic.email}}</td>
</tr>
<tr>
<th class="ttl">パスワード</th>
<td class="ipt"><div class="error" >※パスワード情報は非表示となっております。</div></td>
</tr>
<tr>
<th class="ttl">セキュリティ</th>
<td class="ipt">
<ul class="formNameArea">
<li class="w90">秘密の質問：</li>
{% if basic.security_question_id=="0" %}
<li>初めて飼ったペットの名前は?</li>
{% elseif basic.security_question_id=="1" %}
<li>母親の旧姓は?</li>
{% elseif basic.security_question_id=="2" %}
<li>子供のころのあだ名は?</li>
{% elseif basic.security_question_id=="3" %}
<li>一番嫌いな食べ物は?</li>
{% elseif basic.security_question_id=="4" %}
<li>初めて行った海外の国は?</li>
{% elseif basic.security_question_id=="5" %}
<li>一番好きなアーティストは?</li>
{% endif %}
<li>答え：</li>
<li>{{basic.security_question_answer}}</li>
</ul>
</td>
</tr>
</table>

<div class="alignC">
<a href="/account/accountModifyStart" class="btnA02 marginL240">登録情報を変更する</a>
<a href="/account/accountModifyDelete" class="btnA04 floatR">アカウントを解約する</a>
</div>

</div><!-- /.unitA01 -->
</form>
<!-- InstanceEndEditable -->
</div><!-- / contentsContainer -->
</div><!-- / contents -->
