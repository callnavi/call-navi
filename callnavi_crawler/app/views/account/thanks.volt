<div id="contents">
  <div id="contentsContainer">
  <!-- InstanceBeginEditable name="mainContents" -->
    <form method="post" action="#">
      <div class="unitA01">
        <h2 class="headingA01">新規アカウント作成</h2>
        <p class="fontSize18 alignC marginB20">ご登録ありがとうございました。</p>
        <p class="alignC marginB40">求人広告の掲載はログイン後、<br>「求人広告掲載・管理ページ」より行ってください。</p>
        <p class="alignC"><a href="/login/index">アカウントログインへ進む</a></p>
      </div><!-- /.unitA01 -->
    </form>
  <!-- InstanceEndEditable -->
  </div><!-- / contentsContainer -->
</div><!-- / contents -->