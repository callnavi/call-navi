<div id="contents">
<div id="contentsContainer">
<!-- InstanceBeginEditable name="mainContents" -->
<form method="post" action="/rectangle/editConfirm" name="make_rectangle_form" enctype="multipart/form-data" id="make_rectangle_form">
<div class="unitA01">
<h2 class="headingA01">新規求人広告作成</h2>
<div class="headingB01">
<h3>レクタングル広告</h3>
<span class="hyoujiArea"><a href="/customer/displayArea#area02" class="blank">表示エリアについて</a></span>
</div>
<span class="inputError"></span>
<table class="tableB01">
<tr class="title">
<th class="ttl">広告名（内部管理用）</th>
<th class="nsc"><span class="must">必須</span></th>
<td class="ipt"><input type="text" name="title" class="inputA01 marginB05 w20em" value="{% if data.title is defined %}{{data.title}}{% endif %}" maxlength="20">
<ul class="alertA01">
<li>※20文字以内で記入してください。</li>
<li>※ユーザー側の画面には表示されません。</li>
</ul></td>
</tr>
<tr class="picture">
<th class="ttl">広告用サムネイル画像</th>
<th class="nsc"><span class="must">必須</span></th>
<td class="ipt">
<ul class="listB05">
{% if data.has_pic==1 %}
<div id="uploaded">
<div><img src="/public/images/rectangle/{{ data.id }}.png" width="200" height="" alt=""></div>
<br>
<label><input type="button" id="delete" class="inputA07" value="画像を削除する"></label>
<br>
</div>
<br>
{% endif %}
<div><li><input type="file" name="rectangle_picture" id="rectangle_picture" class="inputA08 marginL00" accept="image/png, image/gif, image/jpeg"></li></div>
</ul>
<ul class="alertA01">
<li>※ファイルの種類：gif、jpeg、png</li>
<li>※ファイルサイズ：1MBまで</li>
<li>※推奨画像サイズ：縦400pixel × 横400pixel（縦横比 1:1）</li>
</ul>
<input type="hidden" name="has_pic" value="{{data.has_pic}}"/>
</td>
</tr>
</table>
</div><!-- /.unitA01 -->

<div class="unitA01">
<table class="tableB01 marginB20">
<tr class="click_price">
<th class="ttl">クリック単価</th>
<th class="nsc"><span class="must">必須</span></th>
<td class="ipt"><input type="text" name="click_price" class="inputA02 marginR10 marginB05"value="{% if data.click_price is defined %}{{data.click_price}}{% endif %}">円　現在の平均相場：<em class="hiyou">{{ average_click_price }}</em>円
<ul class="alertA01">
<li>※半角数字で記入してください。</li>
<li>※最低30円以上で記入してください。</li>
</ul></td>
</tr>
<tr class="budget_max">
<th class="ttl">上限広告予算</th>
<th class="nsc"><span class="recommend">推奨</span></th>
<td class="ipt"><input type="text" name="budget_max" class="inputA02 marginR10 marginB05" value="{% if data.budget_max is defined %}{{data.budget_max}}{% endif %}">円／1日
<ul class="alertA01">
<li>※半角数字で記入してください。</li>
</ul>
</td>
</tr>
<tr class="detail_url">
<th class="ttl">リンク先URL</th>
<th class="nsc"><span class="must">必須</span></th>
<td class="ipt"><input type="text" name="detail_url" class="inputA01 marginB05" value="{% if data.detail_url is defined %}{{data.detail_url}}{% endif %}">
<ul class="alertA01">
<li>※半角英数字で記入してください。</li>
<li>※http://から記載してください。</li>
</ul></td>
</tr>
</table>
<input type="hidden" name="offer_id" value="{{ data.id }}">
<div class="alignC">
<a href="#" class="btnA01 marginR15">入力内容を保存する</a>
<button class="btnA02">保存して確認画面へ進む</button>
</div>
</div><!-- /.unitA01 -->
</form>
<!-- InstanceEndEditable -->
</div><!-- / contentsContainer -->
</div><!-- / contents -->
