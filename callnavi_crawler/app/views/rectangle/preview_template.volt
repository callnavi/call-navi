
<h2 class="headingA01">新規求人広告作成</h2>
<div class="headingB01">
<h3>レクタングル広告</h3>

<span class="hyoujiArea"><a href="/customer/displayArea#area02" class="blank">表示エリアについて</a></span>
</div>
<p>以下で表示された広告イメージは、全ページの『レクタングル広告エリア』に掲載される広告イメージです。</p>
<p class="fontTypeB01 fw">※広告をクリックするとご指定のURLが別ウインドウで表示されます。</p>
<div class="zabuton">
<p class="alignC">

<a {% if data.detail_url !== "" %}href="{{ data.detail_url }}" target="_blank"{% endif %}><img src={% if data.has_pic ==1 %}"/public/images/rectangle/{{ data.id }}.png" width="200" height="" alt=""{% else %}"/public/admin_images/no_upload/img_no_upload_02.png" width="200" height="" alt=""{% endif %}></a>

</p>
</div><!-- / zabuton -->
</div><!-- /.unitA01 -->

<div class="unitA01">
<table class="tableB01 marginB30">
<tr>
<th class="ttl">広告名</th>
<td class="ipt">{{data.title}}</td>
</tr>
<tr>
<th class="ttl">クリック単価</th>
{% if data.click_price==null %}
<td class="error">まだデータ入力がありません。</td>
{% else %}
<td class="ipt">{% if data.click_price>=1000 %}
{{(data.click_price-data.click_price%1000)/1000}},<!-- 
-->{% endif %}<!--
-->{% if data.click_price<1000 %}{{data.click_price}}<!--
-->{% elseif data.click_price%1000>=100 %}<!--
-->{{data.click_price%1000}}<!--
-->{% elseif data.click_price%1000>=10 %}<!--
-->0{{data.click_price%1000}}<!--
-->{% else %}<!--
-->00{{data.click_price%1000}}
{% endif %}円</td>
{% endif %}
</tr>
<tr>
<th class="ttl">上限広告予算</th>
<td class="ipt">{% if data.budget_max>=1000 %}
{{(data.budget_max-data.budget_max%1000)/1000}},<!-- 
-->{% endif %}<!--
-->{% if data.budget_max<1000 %}{{data.budget_max}}<!--
-->{% elseif data.budget_max%1000>=100 %}<!--
-->{{data.budget_max%1000}}<!--
-->{% elseif data.budget_max%1000>=10 %}<!--
-->0{{data.budget_max%1000}}<!--
-->{% else %}<!--
-->00{{data.budget_max%1000}}
{% endif %}　円／1日</td>
</tr>
<tr>
<th class="ttl">リンク先URL</th>
{% if data.detail_url=='' %}
<td class="error">まだデータ入力がありません。</td>
{% else %}
<td class="ipt"><a href="{{data.detail_url}}" target="_blank">{{data.detail_url}}</a><p class="fontTypeB01 marginT05 marginB00">※クリックして、リンク先が正しいかご確認ください。</p></td>
{% endif %}
</tr>
</table>
