<ul class="types">
<?php if (in_array("full_time", $hired_as)): ?><li><img src="/public/admin_images/hired_as/ico_full_time.png" width="" height="" alt="正社員" /></li><?php endif; ?>

<?php if (in_array("contract", $hired_as)): ?><li><img src="/public/admin_images/hired_as/ico_contract.png" width="" height="" alt="契約社員" /></li><?php endif; ?>

<?php if (in_array("temporary", $hired_as)): ?><li><img src="/public/admin_images/hired_as/ico_temporary.png" width="" height="" alt="派遣社員" /></li><?php endif; ?>

<?php if (in_array("part_time", $hired_as)): ?><li><img src="/public/admin_images/hired_as/ico_part_time.png" width="" height="" alt="パート" /></li><?php endif; ?>

<?php if (in_array("arbeit", $hired_as)): ?><li><img src="/public/admin_images/hired_as/ico_arbeit.png" width="" height="" alt="アルバイト" /></li><?php endif; ?>
</ul>
