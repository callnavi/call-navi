<ul class="listB07">
<li><input type="checkbox" name="features[]" id="a1201" value="1"<?php if (isset($features) and in_array("1", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1201">フリーター歓迎</label></li>
<li><input type="checkbox" name="features[]" id="a1202" value="2"<?php if (isset($features) and in_array("2", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1202">大学生歓迎</label></li>
<li><input type="checkbox" name="features[]" id="a1203" value="3"<?php if (isset($features) and in_array("3", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1203">二部学生歓迎</label></li>
<li><input type="checkbox" name="features[]" id="a1204" value="4"<?php if (isset($features) and in_array("4", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1204">主婦（夫）歓迎</label></li>
<li><input type="checkbox" name="features[]" id="a1205" value="5"<?php if (isset($features) and in_array("5", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1205">高校生不可</label></li>
<li><input type="checkbox" name="features[]" id="a1206" value="6"<?php if (isset($features) and in_array("6", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1206">シニアOK</label></li>
<li><input type="checkbox" name="features[]" id="a1207" value="7"<?php if (isset($features) and in_array("7", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1207">仕事ブランクOK</label></li>
<li><input type="checkbox" name="features[]" id="a1208" value="8"<?php if (isset($features) and in_array("8", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1208">未経験OK</label></li>
<li><input type="checkbox" name="features[]" id="a1209" value="9"<?php if (isset($features) and in_array("9", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1209">経験者優遇</label></li>
<li><input type="checkbox" name="features[]" id="a1210" value="10"<?php if (isset($features) and in_array("10", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1210">友達と応募歓迎</label></li>
<li><input type="checkbox" name="features[]" id="a1211" value="11"<?php if (isset($features) and in_array("11", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1211">Wワーク歓迎</label></li>
<li><input type="checkbox" name="features[]" id="a1212" value="12"<?php if (isset($features) and in_array("12", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1212">扶養控除内OK</label></li>
<li><input type="checkbox" name="features[]" id="a1213" value="13"<?php if (isset($features) and in_array("13", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1213">Iターン・UターンOK</label></li>
<li><input type="checkbox" name="features[]" id="a1214" value="14"<?php if (isset($features) and in_array("14", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1214">短期勤務（～1ヶ月）OK</label></li>
<li><input type="checkbox" name="features[]" id="a1215" value="15"<?php if (isset($features) and in_array("15", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1215">長期勤務歓迎</label></li>
<li><input type="checkbox" name="features[]" id="a1216" value="16"<?php if (isset($features) and in_array("16", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1216">週2～3日からOK</label></li>
<li><input type="checkbox" name="features[]" id="a1217" value="17"<?php if (isset($features) and in_array("17", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1217">1日 3hからOK</label></li>
<li><input type="checkbox" name="features[]" id="a1218" value="18"<?php if (isset($features) and in_array("18", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1218">平日のみOK</label></li>
<li><input type="checkbox" name="features[]" id="a1219" value="19"<?php if (isset($features) and in_array("19", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1219">土日のみOK</label></li>
<li><input type="checkbox" name="features[]" id="a1220" value="20"<?php if (isset($features) and in_array("20", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1220">即日勤務OK</label></li>
<li><input type="checkbox" name="features[]" id="a1221" value="21"<?php if (isset($features) and in_array("21", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1221">日・週払い可</label></li>
<li><input type="checkbox" name="features[]" id="a1222" value="22"<?php if (isset($features) and in_array("22", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1222">前払いOK</label></li>
<li><input type="checkbox" name="features[]" id="a1223" value="23"<?php if (isset($features) and in_array("23", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1223">シフト制</label></li>
<li><input type="checkbox" name="features[]" id="a1224" value="24"<?php if (isset($features) and in_array("24", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1224">髪型自由</label></li>
<li><input type="checkbox" name="features[]" id="a1225" value="25"<?php if (isset($features) and in_array("25", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1225">服装自由</label></li>
<li><input type="checkbox" name="features[]" id="a1226" value="26"<?php if (isset($features) and in_array("26", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1226">ネイル・ピアスOK</label></li>
<li><input type="checkbox" name="features[]" id="a1227" value="27"<?php if (isset($features) and in_array("27", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1227">学歴不問</label></li>
<li><input type="checkbox" name="features[]" id="a1228" value="28"<?php if (isset($features) and in_array("28", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1228">語学力を活かす</label></li>
<li><input type="checkbox" name="features[]" id="a1229" value="29"<?php if (isset($features) and in_array("29", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1229">オープニングスタッフ</label></li>
<li><input type="checkbox" name="features[]" id="a1230" value="30"<?php if (isset($features) and in_array("30", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1230">駅チカ・駅ナカ</label></li>
<li><input type="checkbox" name="features[]" id="a1231" value="31"<?php if (isset($features) and in_array("31", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1231">早朝・深夜（夜勤）</label></li>
<li><input type="checkbox" name="features[]" id="a1232" value="32"<?php if (isset($features) and in_array("32", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1232">研修制度あり</label></li>
<li><input type="checkbox" name="features[]" id="a1233" value="33"<?php if (isset($features) and in_array("33", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1233">資格支援制度あり</label></li>
<li><input type="checkbox" name="features[]" id="a1234" value="34"<?php if (isset($features) and in_array("34", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1234">社保完備</label></li>
<li><input type="checkbox" name="features[]" id="a1235" value="35"<?php if (isset($features) and in_array("35", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1235">履歴書不要</label></li>
<li><input type="checkbox" name="features[]" id="a1236" value="36"<?php if (isset($features) and in_array("36", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1236">社員登用あり（紹介予定派遣含む）</label></li>
<li><input type="checkbox" name="features[]" id="a1237" value="37"<?php if (isset($features) and in_array("37", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1237">交通費規定支給</label></li>
<li><input type="checkbox" name="features[]" id="a1238" value="38"<?php if (isset($features) and in_array("38", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1238">昇給あり</label></li>
<li><input type="checkbox" name="features[]" id="a1239" value="39"<?php if (isset($features) and in_array("39", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1239">大量募集</label></li>
<li><input type="checkbox" name="features[]" id="a1240" value="40"<?php if (isset($features) and in_array("40", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1240">高収入</label></li>
<li><input type="checkbox" name="features[]" id="a1241" value="41"<?php if (isset($features) and in_array("41", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1241">登録制</label></li>
<li><input type="checkbox" name="features[]" id="a1242" value="42"<?php if (isset($features) and in_array("42", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1242">制服貸与</label></li>
<li><input type="checkbox" name="features[]" id="a1243" value="43"<?php if (isset($features) and in_array("43", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1243">食堂・まかないあり</label></li>
<li><input type="checkbox" name="features[]" id="a1244" value="44"<?php if (isset($features) and in_array("44", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1244">託児所あり</label></li>
<li><input type="checkbox" name="features[]" id="a1245" value="45"<?php if (isset($features) and in_array("45", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1245">寮・社宅あり</label></li>
<li><input type="checkbox" name="features[]" id="a1246" value="46"<?php if (isset($features) and in_array("46", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1246">事務スキルUP</label></li>
<li><input type="checkbox" name="features[]" id="a1247" value="47"<?php if (isset($features) and in_array("47", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1247">福利厚生充実</label></li>
<li><input type="checkbox" name="features[]" id="a1248" value="48"<?php if (isset($features) and in_array("48", $features)): ?>checked<?php else: ?><?php endif; ?>/><label for="a1248">社販割引あり</label></li> 
</ul>


