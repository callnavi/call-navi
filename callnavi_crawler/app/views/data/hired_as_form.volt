<ul class="listB01">
<li><input type="checkbox" name="hired_as[]" id="full_time" value="full_time"<?php if (isset($hired_as) and in_array("full_time", $hired_as)): ?>checked<?php endif; ?>><label for="full_time">正社員</label></li>
<li><input type="checkbox" name="hired_as[]" id="contract" value="contract"<?php if (isset($hired_as) and in_array("contract", $hired_as)): ?>checked<?php endif; ?>><label for="contract">契約社員</label></li>
<li><input type="checkbox" name="hired_as[]" id="temporary" value="temporary"<?php if (isset($hired_as) and in_array("temporary", $hired_as)): ?>checked<?php endif; ?>><label for="temporary">派遣社員</label></li>
<li><input type="checkbox" name="hired_as[]" id="part_time" value="part_time"<?php if (isset($hired_as) and in_array("part_time", $hired_as)): ?>checked<?php endif; ?>><label for="part_time">パート</label></li>
<li><input type="checkbox" name="hired_as[]" id="temporary_future" value="arbeit"<?php if (isset($hired_as) and in_array("arbeit", $hired_as)): ?>checked<?php endif; ?>><label for="temporary_future">アルバイト</label></li>
</ul>