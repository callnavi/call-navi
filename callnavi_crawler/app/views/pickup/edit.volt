<script language="javascript">   
$(function() {
	$("#pickup_img_02 td").css("background-color","#fafafa");
});
</script>

<div id="contents">
    <div id="contentsContainer">
        
        <form id="make_pickup_form" method="post" action="/pickup/editConfirm" enctype="multipart/form-data" name="make_pickup_form">
        
            <div class="unitA01">
                <h2 class="headingA01">新規求人広告作成</h2>
                <div class="headingB01">
                    <h3>ピックアップ広告</h3>
                    <span class="hyoujiArea"><a href="/customer/displayArea#area03" class="blank">表示エリアについて</a></span>
                </div>
                
                <span class="inputError"></span>
                <table class="tableB01 ">
                <tr class="title">
                        <th class="ttl">広告名（内部管理用）</th>
                        <th class="nsc"><span class="must">必須</span></th>
                        <td class="ipt"><input type="text" name="title" class="inputA01 marginB05 w20em" value="{% if data.title is defined %}{{data.title}}{% endif %}" maxlength="20">
                            <ul class="alertA01">
                                <li>※20文字以内で記入してください。</li>
                                <li>※ユーザー側の画面には表示されません。</li>
                            </ul></td>
                    </tr>

                    <tr class="job_category">
                        <th class="ttl">募集職種</th>
                        <th class="nsc"><span class="must">必須</span></th>
                        <td class="ipt"><input type="text" name="job_category" class="inputA01 marginB05 w20em" value="{% if data.job_category is defined %}{{data.job_category}}{% endif %}" maxlength="20">
                        <ul class="alertA01">
                        <li>※20文字以内で記入してください。</li>
                        </ul>
                        </td>
                    </tr>
                    <tr class="hired_as">
                        <th class="ttl">雇用形態</th>
                        <th class="nsc"><span class="must">必須</span></th>
                        <td class="ipt">
                            {{ partial("/data/hired_as_form")}}
                            
                            <input type="hidden" name="hired_as_string"/>
                        </td>
                    </tr>
                    <tr class="company_name">
                        <th class="ttl">募集会社名</th>
                        <th class="nsc"><span class="must">必須</span></th>
                        <td class="ipt"><input type="text" name="company_name" class="inputA01 marginB05 w30em" value="{% if data.company_name is defined %}{{data.company_name}}{% endif %}" maxlength="30">
                        <ul class="alertA01">
                        <li>※30文字以内で記入してください。</li>
                        </ul>
                        </td>
                    </tr>
                    <tr class="salary">
                        <th class="ttl">給与</th>
                        <th class="nsc"><span class="must">必須</span></th>
                        <td class="ipt">
                            <ul class="listB03 marginB05">
                                <li><select name="salary_term"><option value="hour"{% if data.salary_term is defined and data.salary_term=="hour" %}selected{% else %}{% endif %}>時給</option><option value="day"{% if data.salary_term is defined and data.salary_term=="day" %}selected{% else %}{% endif %}>日給</option><option value="month"{% if data.salary_term is defined and data.salary_term=="month" %}selected{% else %}{% endif %}>月給</option><option value="year"{% if data.salary_term is defined and data.salary_term=="year" %}selected{% else %}{% endif %}>年収</option></select></li>
                                <li><input type="text" name="salary_value_min" class="inputA02 marginR10" value="{% if data.salary_value_min is defined  %}{{data.salary_value_min}}{% endif %}" maxlength="5"><select name="salary_unit_min"><option value="yen"{% if data.salary_unit_min is defined and data.salary_unit_min=="yen" %}selected{% else %}{% endif %}>円</option><option value="man_yen"{% if data.salary_unit_min is defined and data.salary_unit_min=="man_yen" %}selected{% else %}{% endif %}>万円</option><option value="dollar"{% if data.salary_unit_min is defined and data.salary_unit_min=="dollar" %}selected{% else %}{% endif %}>＄</option></select></li>
                                <li>〜</li>
                                <li><input type="text" name="salary_value_max" class="inputA02 marginR10" value="{% if data.salary_value_max is defined  %}{{data.salary_value_max}}{% endif %}" maxlength="5"><select name="salary_unit_max"><option value="yen"{% if data.salary_unit_min is defined and data.salary_unit_min=="yen" %}selected{% else %}{% endif %}>円</option><option value="man_yen"{% if data.salary_unit_min is defined and data.salary_unit_min=="man_yen" %}selected{% else %}{% endif %}>万円</option><option value="dollar"{% if data.salary_unit_min is defined and data.salary_unit_min=="dollar" %}selected{% else %}{% endif %}>＄</option></select></li>
                            </ul>
                            <input type="hidden" name="salary_term_display"/>
                            <input type="hidden" name="salary_unit_display"/>
                            <ul class="alertA01">
                            <li>※半角数字で記入してください。</li>
                            </ul>
                        </td>
                    </tr>
                    <tr class="workplace">
                        <th class="ttl">勤務地</th>
                        <th class="nsc"><span class="must">必須</span></th>
                        <td class="ipt">
                            <ul class="listB03 marginB10">
                                <li><span class="marginR10">都道府県</span><select name="workplace_prefecture" class="sfr"><option value="">選択してください</option><option value="北海道"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="北海道" %}selected{% else %}{% endif %}>北海道</option><option value="青森県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="青森県" %}selected{% else %}{% endif %}>青森県</option><option value="秋田県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="秋田県" %}selected{% else %}{% endif %}>秋田県</option><option value="岩手県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="岩手県" %}selected{% else %}{% endif %}>岩手県</option><option value="宮城県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="宮城県" %}selected{% else %}{% endif %}>宮城県</option><option value="山形県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="山形県" %}selected{% else %}{% endif %}>山形県</option><option value="福島県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="福島県" %}selected{% else %}{% endif %}>福島県</option><option value="茨城県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="茨城県" %}selected{% else %}{% endif %}>茨城県</option><option value="千葉県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="千葉県" %}selected{% else %}{% endif %}>千葉県</option><option value="栃木県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="栃木県" %}selected{% else %}{% endif %}>栃木県</option><option value="埼玉県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="埼玉県" %}selected{% else %}{% endif %}>埼玉県</option><option value="東京都"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="東京都" %}selected{% else %}{% endif %}>東京都</option><option value="群馬県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="群馬県" %}selected{% else %}{% endif %}>群馬県</option><option value="神奈川県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="神奈川県" %}selected{% else %}{% endif %}>神奈川県</option><option value="新潟県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="新潟県" %}selected{% else %}{% endif %}>新潟県</option><option value="富山県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="富山県" %}selected{% else %}{% endif %}>富山県</option><option value="石川県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="石川県" %}selected{% else %}{% endif %}>石川県</option><option value="福井県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="福井県" %}selected{% else %}{% endif %}>福井県</option><option value="山梨県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="山梨県" %}selected{% else %}{% endif %}>山梨県</option><option value="静岡県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="静岡県" %}selected{% else %}{% endif %}>静岡県</option><option value="長野県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="長野県" %}selected{% else %}{% endif %}>長野県</option><option value="愛知県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="愛知県" %}selected{% else %}{% endif %}>愛知県</option><option value="岐阜県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="岐阜県" %}selected{% else %}{% endif %}>岐阜県</option><option value="滋賀県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="滋賀県" %}selected{% else %}{% endif %}>滋賀県</option><option value="三重県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="三重県" %}selected{% else %}{% endif %}>三重県</option><option value="京都府"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="京都府" %}selected{% else %}{% endif %}>京都府</option><option value="奈良県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="奈良県" %}selected{% else %}{% endif %}>奈良県</option><option value="大阪府"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="大阪府" %}selected{% else %}{% endif %}>大阪府</option><option value="和歌山県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="和歌山県" %}selected{% else %}{% endif %}>和歌山県</option><option value="兵庫県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="兵庫県" %}selected{% else %}{% endif %}>兵庫県</option><option value="鳥取県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="鳥取県" %}selected{% else %}{% endif %}>鳥取県</option><option value="岡山県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="岡山県" %}selected{% else %}{% endif %}>岡山県</option><option value="島根県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="鳥取県" %}selected{% else %}{% endif %}>島根県</option><option value="広島県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="広島県" %}selected{% else %}{% endif %}>広島県</option><option value="山口県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="山口県" %}selected{% else %}{% endif %}>山口県</option><option value="香川県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="香川県" %}selected{% else %}{% endif %}>香川県</option><option value="徳島県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="徳島県" %}selected{% else %}{% endif %}>徳島県</option><option value="愛媛県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="愛媛県" %}selected{% else %}{% endif %}>愛媛県</option><option value="高知県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="高知県" %}selected{% else %}{% endif %}>高知県</option><option value="大分県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="大分県" %}selected{% else %}{% endif %}>大分県</option><option value="宮崎県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="宮崎県" %}selected{% else %}{% endif %}>宮崎県</option><option value="鹿児島県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="鹿児島県" %}selected{% else %}{% endif %}>鹿児島県</option><option value="福岡県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="福岡県" %}selected{% else %}{% endif %}>福岡県</option><option value="熊本県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="熊本県" %}selected{% else %}{% endif %}>熊本県</option><option value="佐賀県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="佐賀県" %}selected{% else %}{% endif %}>佐賀県</option><option value="長崎県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="長崎県" %}selected{% else %}{% endif %}>長崎県</option><option value="沖縄県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="沖縄県" %}selected{% else %}{% endif %}>沖縄県</option><option value="その他(海外など)"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="その他(海外など)" %}selected{% else %}{% endif %}>その他(海外など)</option></select></li>
                                <li><span class="marginR10">市区町村名</span><input type="text" name="workplace_area" class="inputA02" value="{% if data.workplace_area is defined %}{{data.workplace_area}}{% else %}{% endif %}"/></li>
                            </ul>
                            <ul class="listB04">
                                <li>番地・建物名</li>
                                <li class="alignL"><input type="text" name="workplace_address" class="inputA01 marginB05 w40em" value="{% if data.workplace_address is defined %}{{data.workplace_address}}{% else %}{% endif %}" maxlength="40" /></li>
                            </ul>
                            <ul class="alertA01">
                            <li>※番地・建物名は40文字以内で記入してください。</li>
                            </ul>
                        </td>
                    </tr>
                    <tr class="message">
                        <th class="ttl">キャッチコピー・応募者へのメッセージ<br>
                            （120文字以内）</th>
                        <th class="nsc"><span class="must">必須</span></th>
                        <td class="ipt"><textarea name="message" class="inputA03 marginB05 js-textCount">{% if data.message is defined %}{{data.message|striptags}}{% else %}{% endif %}</textarea>
                        <ul class="alertA01 clearfix">
                        <li class="floatL">※120文字以内で記入してください。</li>
                        <li class="floatR">残り文字数：<span class="js-textLimit">120</span></li>
                        </ul>
                        </td>
                    </tr>
                    <tr class="features">
                        <th class="ttl">こだわり条件（5つまで）</th>
                        <th class="nsc"><span class="recommend">推奨</span></th>
                        <td class="ipt">
                          {{ partial("/data/features_form")}}
                           
                            <input type="hidden" name="features_string"/>
                        </td>
                    </tr>
                     <tr id="pickup_img_01">
                        <th class="ttl">広告用サムネイル画像</th>
                        <th class="nsc"><span class="must">必須</span></th>
                        <td class="ipt">
                            <ul class="listB06">
                                {% if data.has_thum_pic==1 %}
                                <div id="uploaded[0]">
                                <div><img src="/public/images/pickup/thum/{{ data.id }}.png" width="300" height="" alt=""></div>
                                <br>
                                <label><input type="button" id="delete[0]" class="inputA07" value="画像を削除する"></label>
                                <br>
                                </div>
                                <br>
                                {% endif %}
                                <div><input type="file" name="file[0]" id="thum" class="inputA08 marginLOff" accept="image/png, image/gif, image/jpeg"></div>
                            </ul>
                            <ul class="alertA01">
                                <li>※ファイルの種類：gif、jpeg、png</li>
                                <li>※ファイルサイズ：1MBまで</li>
                                <li>※推奨画像サイズ：縦450pixel × 横600pixel（縦横比 3:4）</li>
                            </ul>
                        </td>
                    </tr>
                    
                   
                </table>
            </div><!-- /.unitA01 -->

            <div class="unitA01">
                <table class="tableB01">
                  <div id="before_publish">
                    <tr class="published">
                        <th class="ttl">掲載期間</th>
                        <th class="nsc"><span class="must">必須</span></th>
                        <td class="ipt">
                            <ul class="listB03 marginB10">
                                <li><select name="publish_week"><option value="0">選択してください</option><option value="1"{% if data.publish_week is defined and data.publish_week=="1" %}selected{% elseif data.has_published == 1 %}disabled{% endif %}>15,000円／1週間</option><option value="2"{% if data.publish_week is defined and data.publish_week=="2" %}selected{% elseif data.has_published == 1 and data.publish_week > 2 %}disabled{% endif %}>26,000円／2週間</option><option value="3"{% if data.publish_week is defined and data.publish_week=="3" %}selected{% elseif data.has_published == 1 and data.publish_week > 3 %}disabled{% endif %}>36,000円／3週間</option><option value="4"{% if data.publish_week is defined and data.publish_week=="4" %}selected{% elseif data.has_published == 1 and data.publish_week > 4 %}disabled{% endif %}>40,000円／4週間</option></select></li>
                                <li>費用：<em class="hiyou"></em>円</li>
                            </ul>
                        {% if data.has_published == 1 %}
                            <ul class="alertA01 marginT15 freq_1">
                            <li>※掲載期間が残っているため、延長以外選択できません。</li>
                            </ul>
                        {% endif %}
                        </td>
                    </tr>
                    <tr class="display_area">
                        <th class="ttl">新着表示エリア(オプション)</th>
                        <th class="nsc"><span class="must">必須</span></th>
                        <td class="ipt">
                            <div class="hyoujiAreaColumn">
                                <ul id="hyoujiAreaOption" class="listB01">
                                    <li><input type="radio" name="display_area" id="b0100" value="none" {% if data.display_area is defined and data.display_area == "none" %}checked{% elseif data.has_published == 1%}disabled{% endif %}><label for="b0100" >無し</label></li>
                                    <li><input type="radio" name="display_area" id="b0101" value="inner" {% if data.display_area is defined and data.display_area == "inner" %}checked{% elseif data.has_published == 1%}disabled{% endif %} ><label for="b0101" >インナーパネル</label></li>
                                    <li><input type="radio" name="display_area" id="b0102" value="inner_half" {% if data.display_area is defined and data.display_area == "inner_half" %}checked{% elseif data.has_published == 1 %}disabled{% endif %}><label for="b0102">インナーパネルハーフ</label></li>
                                    <li><input type="radio" name="display_area" id="b0103" value="right_column" {% if data.display_area is defined and data.display_area == "right_column" %}checked{% elseif data.has_published == 1%}disabled{% endif %}><label for="b0103">右カラム</label></li>
                                    
                                    <li>費用：<span></span></li>
                                </ul> 
                            {% if data.has_published == 1 %}
                                <ul class="alertA01 marginT15 freq_1">
                                <li>※掲載期間が残っているため、選択変更できません。<br>
                                 また、掲載期間中でも掲載開始から7日間を超えている場合は、トップページの新着エリアには表示されませんのでご注意ください。</li>
                                </ul>
                            {% endif %}
                            </div>
                            <input type="hidden" name="display_area_display" />
                            <input type="hidden" name="display_price" />
                      </div>
                       
                    </tr>
                    <tr>
                        <th class="ttl">求人詳細ページ</th>
                        <th class="nsc"><span class="must">必須</span></th>
                        <td class="ipt">
                            <ul class="listB01 marginB15">
                                <li><input type="radio" name="detail_content_type" id="detail_content_type01" value="none"checked/><label for="detail_content_type01">不要</li>
                                <li><input type="radio" name="detail_content_type" id="detail_content_type02" value="form"{% if data.detail_content_type is defined and data.detail_content_type=="form" %}checked{% endif %}><label for="detail_content_type02">必要（フォーム記入）</li>

{% if 1 == 0 %}
                              {% if basic.id == 0 %}
                                <li><input type="radio" name="detail_content_type" id="detail_content_type03" value="html"{% if data.detail_content_type is defined and data.detail_content_type=="html" %}checked{% endif %}><label for="detail_content_type03">必要（HTML記入）</li>
                              {% else %}
                                <li><input type="radio" name="detail_content_type" id="detail_content_type03" value="html"{% if data.detail_content_type is defined and data.detail_content_type=="html" %}checked{% endif %}><label for="detail_content_type03">必要（HTML記入）</li>
                              {% endif %}

{% endif %}
                            </ul>
                            <ul class="alertA01">
                                <li>※『コールナビ』内に任意で求人詳細ページを持つことが可能です。<br>
                                    自社で求人詳細ページをお持ちの場合は不要をクリックしてください。</li>
                            </ul>
                        </td>
                    </tr>
                </table>
            </div><!-- /.unitA01 -->

            <div class="unitA01" id="inputBlock-01">
                <table class="tableB01">

                    <tr class="job_description">
                        <th class="ttl">仕事内容</th>
                        <th class="nsc"><span class="must">必須</span></th>
                        <td class="ipt"><textarea name="job_description" class="inputA03 marginB05" >{% if detail.job_description is defined %}{{detail.job_description|striptags}}{% endif %}</textarea>
                        <ul class="alertA01">
                        <li>※1000文字以内で記入してください。</li>
                        </ul>
                        </td>
                    </tr>
                    <tr class="requirement">
                        <th class="ttl">応募資格</th>
                        <th class="nsc"><span class="must">必須</span></th>
                        <td class="ipt"><textarea name="requirement" class="inputA03 marginB05">{% if detail.requirement is defined %}{{detail.requirement|striptags}}{% endif %}</textarea>
                        <ul class="alertA01">
                        <li>※500文字以内で記入してください。</li>
                        </ul>
                        </td>
                    </tr>
                    <tr class="office_hours">
                        <th class="ttl">勤務時間</th>
                        <th class="nsc"><span class="must">必須</span></th>
                        <td class="ipt"><textarea name="office_hours" class="inputA03 marginB05" >{% if detail.office_hours is defined %}{{detail.office_hours|striptags}}{% endif %}</textarea>
                        <ul class="alertA01">
                        <li>※500文字以内で記入してください。</li>
                        </ul>
                        </td>
                    </tr>
                    <tr class="holidays">
                        <th class="ttl">休日／休暇</th>
                        <th class="nsc"><span class="must">必須</span></th>
                        <td class="ipt"><textarea name="holidays" class="inputA03 marginB05">{% if detail.holidays is defined %}{{detail.holidays|striptags}}{% endif %}</textarea>
                        <ul class="alertA01">
                        <li>※500文字以内で記入してください。</li>
                        </ul>
                        </td>
                    </tr>
                    <tr class="welfares">
                        <th class="ttl">待遇／福利厚生</th>
                        <th class="nsc"><span class="must">必須</span></th>
                        <td class="ipt"><textarea name="welfares" class="inputA03 marginB05">{% if detail.welfares is defined %}{{detail.welfares|striptags}}{% endif %}</textarea>
                        <ul class="alertA01">
                        <li>※500文字以内で記入してください。</li>
                        </ul>
                        </td>
                    </tr>
                    <tr class="procedures">
                        <th class="ttl">応募後の選考プロセス</th>
                        <th class="nsc"><span class="recommend">推奨</span></th>
                        <td class="ipt"><textarea name="procedures" class="inputA03 marginB05">{% if detail.procedures is defined %}{{detail.procedures|striptags}}{% endif %}</textarea>
                        <ul class="alertA01">
                        <li>※500文字以内で記入してください。</li>
                        </ul>
                        </td>
                    </tr>
                    <tr class="others">
                        <th class="ttl">その他・特記事項</th>
                        <th class="nsc"><span class="recommend">推奨</span></th>
                        <td class="ipt"><textarea name="others" class="inputA03 marginB05">{% if detail.others is defined %}{{detail.others|striptags}}{% endif %}</textarea>
                        <ul class="alertA01">
                        <li>※1000文字以内で記入してください。</li>
                        </ul>
                        </td>
                    </tr>
                    <tr class="picture">
                        <th class="ttl" rowspan="3">掲載写真データ（3つまで）</th>
                        <th class="nsc noBB"><span class="must">必須</span></th>
                        <td class="ipt">
                            <ul class="listB05">
                                <li>写真１</li>
                                {% if data.has_first_pic==1 %}
                                <div id="uploaded[2]">
                                <div><img src="/public/images/pickup/first/{{ data.id }}.png" width="300" height="" alt=""></div>
                                <br>
                                <label><input type="button" id="delete[2]" class="inputA07" value="画像を削除する"></label>
                                </div>
                                <br>
                                {% endif %}
                                <div><li><input type="file" name="file[1]" id="pickup_picture_A" class="inputA08" accept="image/png, image/gif, image/jpeg"></li><div>

                            </ul>
                            <ul class="alertA01">
                                <li>※ファイルの種類：gif、jpeg、png</li>
                                <li>※ファイルサイズ：1MBまで</li>
                                <li>※推奨画像サイズ：縦540pixel × 横720pixel（縦横比 3:4）</li>
                            </ul>
                            <input type="hidden" name="has_first_pic" id="has_pic[2]" value="{{data.has_first_pic}}"/>
                            <input type="hidden" name="has_second_pic" id="has_pic[3]" value="{{data.has_second_pic}}"/>
                            <input type="hidden" name="has_third_pic" id="has_pic[4]" value="{{data.has_third_pic}}"/>
                            <input type="hidden" name="has_thum_pic" id="has_pic[0]" value="{{data.has_thum_pic}}"/>
                             <input type="hidden" name="offer_id" value="{{ data.id }}"/>
                            </td>
                    </tr>
                    <tr>
                        <th class="nsc noBB"><span class="recommend">推奨</span></th>
                        <td class="ipt"><ul class="listB05">
                                <li>写真２</li>
                                {% if data.has_second_pic==1 %}

                                <div id="uploaded[3]">
                                <div><img src="/public/images/pickup/second/{{ data.id }}.png" width="300" height="" alt=""></div>

                                <br>
                                <label><input type="button" id="delete[3]" class="inputA07" value="画像を削除する"></label>
                                <br>
                                </div>
                                <br>
                                {% endif %}
                                <div><li><input type="file" name="file[2]" id="pickup_picture_B" class="inputA08" accept="image/png, image/gif, image/jpeg"></li></div>
                                
                            </ul>
                            <ul class="alertA01">
                                <li>※ファイルの種類：gif、jpeg、png</li>
                                <li>※ファイルサイズ：1MBまで</li>
                                <li>※推奨画像サイズ：縦540pixel × 横720pixel（縦横比 3:4）</li>
                            </ul></td>
                    </tr>
                    <tr>
                        <th class="nsc"><span class="recommend">推奨</span></th>
                        <td class="ipt"><ul class="listB05">
                                <li>写真３</li>
                                {% if data.has_third_pic==1 %}
                                <div id="uploaded[4]">
                                <div><img src="/public/images/pickup/third/{{ data.id }}.png" width="300" height="" alt=""></div>
                                <br>
                                <label><input type="button" id="delete[4]" class="inputA07" value="画像を削除する"></label>
                                </div>
                                <br>
                                {% endif %}
                                <div><li><input type="file" name="file[3]" id="pickup_picture_C" class="inputA08" accept="image/png, image/gif, image/jpeg"></li></div>
                                </div>
                            </ul>
                            <ul class="alertA01">
                                <li>※ファイルの種類：gif、jpeg、png</li>
                                <li>※ファイルサイズ：1MBまで</li>
                                <li>※推奨画像サイズ：縦540pixel × 横720pixel（縦横比 3:4）</li>
                            </ul></td>
                    </tr>

                </table>
            </div><!-- /.unitA01 -->

            <div class="unitA01">
                <table class="tableB01 marginB20" id="inputBlock-02">
                    <tr class="founded">
                        <th class="ttl">設立</th>
                        <th class="nsc"><span class="must">必須</span></th>
                        <td class="ipt"><input type="text" name="founded" class="inputA01" value="{% if detail.founded is defined %}{{detail.founded}}{% endif %}"></td>
                    </tr>
                    <tr class="president">
                        <th class="ttl">代表者</th>
                        <th class="nsc"><span class="must">必須</span></th>
                        <td class="ipt"><input type="text" name="president" class="inputA01" value="{% if detail.president is defined %}{{detail.president}}{% endif %}"></td>
                    </tr>
                    <tr class="capital">
                        <th class="ttl">資本金</th>
                        <th class="nsc"><span class="must">必須</span></th>
                        <td class="ipt"><input type="text" name="capital" class="inputA01" value="{% if detail.capital is defined %}{{detail.capital}}{% endif %}"></td>
                    </tr>
                    <tr class="employees">
                        <th class="ttl">従業員数</th>
                        <th class="nsc"><span class="must">必須</span></th>
                        <td class="ipt"><input type="text" name="employees" class="inputA01" value="{% if detail.employees is defined %}{{detail.employees}}{% endif %}"></td>
                    </tr>
                    <tr class="business_description">
                        <th class="ttl">事業内容</th>
                        <th class="nsc"><span class="must">必須</span></th>
                        <td class="ipt"><textarea name="business_description" class="inputA03">{% if detail.business_description is defined %}{{detail.business_description|striptags}}{% endif %}</textarea></td>
                    </tr>
                    <tr class="company_url">
                        <th class="ttl">会社URL</th>
                        <th class="nsc"><span class="must">必須</span></th>
                        <td class="ipt"><input type="text" name="company_url" class="inputA01 marginB10" value="{% if detail.company_url is defined %}{{detail.company_url}}{% endif %}">
                        <ul class="alertA01">
                        <li>※半角英数字で記入してください。</li>
                            <li>※http://から記載してください。</li>
                        </ul></td>
                    </tr>
                </table>
            </div><!-- /.unitA01 -->

            <div class="unitA01" id="inputBlock-03">
                <table class="tableB01">
                    <tr class="detail_url">
                        <th class="ttl">リンク先URL（応募フォームなど）</th>
                        <th class="nsc"><span class="must">必須</span></th>
                        <td class="ipt"><input type="text" name="detail_url" value="{% if data.detail_url is defined %}{{data.detail_url}}{% endif %}" class="inputA01 marginB10">
                            <ul class="alertA01">
                                <li>※『コールナビ』内には応募フォームはございませんので、お客様側でご用意ください。</li>
                                <li>※半角英数字で記入してください。</li>
                                <li>※http://から記載してください。</li>
                            </ul></td>
                    </tr>
                </table>
            </div><!-- /.unitA01 -->

            <div class="unitA01" id="inputBlock-04">
                <table class="tableB01">
                    <tr class="html">
                        <th class="ttl"><p class="marginB15">HTML入力欄</p>
                    <ul class="alertB01 marginOff">
                    <li>※HTMLタグの記入による自由レイアウトも可能です。<a href="/pickup/htmlArea" class="blank">HTMLの記述が可能なエリアについてはこちら</a></li>
                    <li>※CSSファイルは『コールナビ』の規定に準じます。<a href="/pickup/cssSample" class="blank">CSSのレイアウトサンプルはこちら</a></li>
                    <li>※HTML内のimage画像は『コールナビ』のサーバーにはアップロードできません。外部サーバーのimage画像へ絶対パスで記入してください。</li>
                    </ul>
                    </th>
                    <th class="nsc"><span class="must">必須</span></th>
                    <td class="ipt">
                        <p class="marginB15"><textarea id="inputHtml" name="html">{% if detail.html is defined %}{{detail.html}}{% else %}<section>
                    <h1></h1>
                    <p></p>
                    </section>{% endif %}</textarea></p>
                            <p class="alignC marginLOff"><input type="button" class="formA03" value="プレビュー（別ウィンドウで開きます）"></p></td>
                        </tr>
                    </table>
                </div><!-- /.unitA01 -->
                <input type="hidden" name="offer_id" value="{{ data.id }}"/>
                <div class="alignC marginB40">
                    <button class="btnA01 marginR15">入力内容を保存する</button>
                    <button class="btnA02">保存して確認画面へ進む</button>
                </div>
            </form>
            <script>
                var editor = CodeMirror.fromTextArea(document.getElementById("inputHtml"), {
                    lineNumbers: true,
                    mode: "text/html",
                    matchBrackets: true
                });
            </script>
            
        </div><!-- / contentsContainer -->
    </div><!-- / contents -->