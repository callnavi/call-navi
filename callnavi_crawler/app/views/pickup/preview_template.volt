 
 <h2 class="headingA01">新規求人広告作成</h2>
            <div class="headingB01">
                <h3>ピックアップ広告</h3>
                <span class="hyoujiArea"><a href="/customer/displayArea#area03" class="blank">表示エリアについて</a></span>
            </div>

        {% if data.display_area=="inner_half" %}
            <p>以下で表示された広告イメージは、トップページの『新着オプションエリア』に掲載される広告イメージです。<br>
また、ピックアップ求人一覧ページに掲載される際の広告イメージも下部に表示しています。</p>
            <p class="fontTypeB01 fw">※広告をクリックするとご指定のURLが別ウインドウで表示されます。</p>

            <div class="zabuton">
            		<div class="zabutonCaption">［トップページ（新着オプションエリア）］</div>
                <article class="pickupD01">
                     <a href={% if data.detail_url !=="" and data.detail_content_type == "none" %}"{{data.detail_url}}" target="_blank"{% elseif data.detail_content_type !== "none" %}"/public/index/pickupPreview?offer_id={{data.id}}" target="_blank"{% endif %}>
                        <h3><span class="new">NEW</span><span class="pickup">PICKUP</span><br>
                                {% if data.job_category=='' %}
                                    <div class="error">まだデータ入力がありません。</div>
                                {% else %}
                                    {{data.job_category}}
                                {% endif %}</h3>
                        <p class="image"><img src={% if data.has_thum_pic==0 %}"/public/admin_images/no_upload/img_no_upload_01.png"{% else %}"/public/images/pickup/thum/{{ data.id }}.png" {% endif %}"width="" height="" alt=""/></p>
                        <p class="company">{{data.company_name}}</p>
                        <p class="summary">
                            {% if data.message =='' %}<!--
                                  --><div class="error">まだデータ入力がありません。</div><!--
                                -->{% else %}<!--
                                  -->{{data.message}}<!--
                              -->{% endif %}
                            </p>
                      {{ partial("/data/hired_as_preview")}}  
                        <table>
                            <tr>        
                                <th>給　与</th>
                                <td>
                                    <div><!--
                                        -->{% if data.salary_term == "year" %}<!--
                                        -->年収<!--
                                        -->{% elseif data.salary_term == "month" %}<!--
                                        -->月給<!--
                                        -->{% elseif data.salary_term == "day" %}<!--
                                        -->日給<!--
                                        -->{% elseif data.salary_term == "hour" %}<!--
                                        -->時給<!--
                                        -->{% endif %}<!--
                                        -->{% if data.salary_unit_min !== "dollar" %}
                                            {% if data.salary_value_min=="" %}<div class="error previewErr02">まだデータ入力がありません</div>{% else %}{{ data.salary_value_min }}{% endif %}<!--
                                         -->{% if data.salary_unit_min == "man_yen" %}万円{% elseif data.salary_unit_min == "yen" %}円{% endif %}～{% if data.salary_value_max=="" %}<div class="error previewErr02">まだデータ入力がありません</div>{% else %}{{ data.salary_value_max }}{% endif %}<!--
                                           -->{% if data.salary_unit_max == "man_yen" %}万円{% elseif data.salary_unit_max == "yen" %}円{% endif %}
                                        {% else %}
                                            ${% if data.salary_value_min=="" %}<div class="error previewErr02">まだデータ入力がありません</div>{% else %}{{ data.salary_value_min }}{% endif %}～${% if data.salary_value_max=="" %}<div class="error previewErr02">まだデータ入力がありません</div>{% else %}{{ data.salary_value_max }}{% endif %}
                                        {% endif %}
                                </td>
                            </tr>
                            <tr>
                                <th>勤務地</th>
                                <td>
                                    <div>
                                           {% if data.workplace_prefecture=="" %}<!--
                                        --><div class="error previewErr02">まだデータ入力がありません</div><!--
                                        -->{% else %}<!--
                                        -->{{data.workplace_prefecture}}<!--
                                        -->{% endif %}<!--
                                        -->{% if data.workplace_area=="" %}<!--
                                        --><div class="error previewErr02">まだデータ入力がありません</div><!--
                                        -->{% else %}<!--
                                        -->{{data.workplace_area}}<!--
                                        -->{% endif %}<!--
                                        -->{% if data.workplace_address=="" %}<!--
                                        --><div class="error previewErr02">まだデータ入力がありません</div><!--
                                        -->{% else %}<!--
                                        -->{{data.workplace_address}}<!--
                                        -->{% endif %}
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </a>
                </article><!-- / pickupD01 -->
            </div><!-- / zabuton -->
        </div><!-- /.unitA01 -->
        
        {% elseif data.display_area=="right_column" %}
<p>以下で表示された広告イメージは、［トップページ（新着オプションエリア）］に掲載される広告イメージです。<br>
また、［ピックアップ求人一覧ページ］［検索結果一覧ページ］に掲載される際の広告イメージも下部に表示しています。</p>
<p class="fontTypeB01 fw">※広告をクリックするとご指定のURLが別ウインドウで表示されます。</p>

            <div class="zabuton">
            		<div class="zabutonCaption">［トップページ（新着オプションエリア）］</div>

                <article class="pickupB01">

                     <a href={% if data.detail_url !=="" and data.detail_content_type == "none" %}"{{data.detail_url}}" target="_blank"{% elseif data.detail_content_type !== "none" %}"/public/index/pickupPreview?offer_id={{data.id}}" target="_blank"{% endif %}> 
                        <h3><span class="new">NEW</span><span class="pickup">PICKUP</span><br>
                               {% if data.job_category=='' %}
                                    <div class="error">まだデータ入力がありません。</div>
                                {% else %}
                                    {{data.job_category}}
                                {% endif %}</h3>
                        <p class="image"><img src={% if data.has_thum_pic==0 %}"/public/admin_images/no_upload/img_no_upload_01.png"{% else %}"/public/images/pickup/thum/{{ data.id }}.png" {% endif %}width="" height="" alt=""></p>
                        
                        {% if data.company_name=='' %}
                            <p class="error">まだデータ入力がありません</p>
                        {% else %}
                            <p class="company">{{data.company_name}}</p>
                        {% endif %}
                        {% if data.message=="" %}
                            <p class="error">まだデータ入力がありません</p>
                        {% else %}
                            <p class="summary">{{ data.message }}</p>
                        {% endif %}
                       {{ partial("/data/hired_as_preview")}}
                        <table>
                            <tr>
                                <th>給与</th>
                                <td><div>{% if data.salary_term == "year" %}
                                        年収
                                        {% elseif data.salary_term == "month"  %}
                                            月給
                                            {% elseif data.salary_term == "day" %}
                                                日給
                                                {% elseif data.salary_term == "hour" %}
                                                    時給
                                                    {% endif %}
                                        {% if data.salary_unit_min !== "dollar" %}
                                            {% if data.salary_value_min=="" %}<div class="error previewErr02">まだデータ入力がありません</div>{% else %}{{ data.salary_value_min }}{% endif %}<!--
                                            -->{% if data.salary_unit_min == "man_yen" %}万円{% elseif data.salary_unit_min == "yen" %}円{% endif %}～{% if data.salary_value_max=="" %}<div class="error previewErr02">まだデータ入力がありません</div>{% else %}{{ data.salary_value_max }}{% endif %}<!--
                                            -->{% if data.salary_unit_max == "man_yen" %}万円{% elseif data.salary_unit_max == "yen" %}円{% endif %}
                                        {% else %}
                                            ${% if data.salary_value_min=="" %}<div class="error previewErr02">まだデータ入力がありません</div>{% else %}{{ data.salary_value_min }}{% endif %}～${% if data.salary_value_max=="" %}<div class="error previewErr02">まだデータ入力がありません</div>{% else %}{{ data.salary_value_max }}{% endif %}
                                        {% endif %}
                                                        
                                            </div></td>
                                            </tr>
                                            <tr>
                                                <th>勤務地</th>
                                                <td><div>{% if data.workplace_prefecture=="" %}<div class="error previewErr02">まだデータ入力がありません</div>{% else %}{{data.workplace_prefecture}}{% endif %}{% if data.workplace_area=="" %}<div class="error previewErr02">まだデータ入力がありません</div>{% else %}{{data.workplace_area}}{% endif %}{% if data.workplace_address=="" %}<div class="error previewErr02">まだデータ入力がありません</div>{% else %}{{data.workplace_address}}{% endif %}</div></td>
                                            </tr>
                                        </table>
                                    </a>
                                </article><!-- / pickupB02 -->
                            </div><!-- / zabuton -->
                        </div><!-- /.unitA01 -->




                        {% endif %}




{% if data.display_area!="inner_half" and data.display_area!="right_column" %}
<p>以下で表示された広告イメージは、［トップページ（新着オプションエリア）］［ピックアップ求人一覧ページ］［検索結果一覧ページ］に掲載される広告イメージです。</p>
<p class="fontTypeB01 fw">※広告をクリックするとご指定のURLが別ウインドウで表示されます。</p>
{% endif %}

                <div class="zabuton">
                		<div class="zabutonCaption">［ピックアップ求人一覧ページ］［検索結果一覧ページ］</div>

                    <article class="pickupA01">
                        <a href={% if data.detail_url !=="" and data.detail_content_type == "none" %}"{{data.detail_url}}" target="_blank"{% elseif data.detail_content_type !== "none" %}"/public/index/pickupPreview?offer_id={{data.id}}" target="_blank"{% endif %}>
                            <h3><span class="new">NEW</span><span class="pickup">PICKUP</span>
                                {% if data.job_category=='' %}
                                    <div class="error">まだデータ入力がありません。</div>
                                {% else %}
                                    {{data.job_category}}
                                {% endif %}</h3>
                            <div class="cInner">
                                <p class="image">
                                    <img src={% if data.has_thum_pic==0 %}"/public/admin_images/no_upload/img_no_upload_01.png"{% else %}"/public/images/pickup/thum/{{ data.id }}.png"{% endif%} width="" height="" alt="">
                                </p>
                                {% if data.company_name=='' %}

                                    <p class="error previewErr01">まだデータ入力がありません</p>

                                {% else %}
                                    <p class="company">{{data.company_name}}</p>
                                {% endif %}
                                {% if data.message=='' %}
                                    <p class="error previewErr01">まだデータ入力がありません</p>
                                {% else %}
                                    <p class="summary mediaPC">{{data.message}}</p>
                                {% endif %}
                            </div><!-- / cInner -->  
                            <div class="cInner">
                            
                              <div class="details">
                               {{ partial("/data/hired_as_preview")}}  
                                <table>
                                    <tr>
                                        <th>給　与</th>
                                        <td>
                                            <div><!--
                                                -->{% if data.salary_term == "year" %}<!--
                                                -->年収
                                            {% elseif data.salary_term == "month" %}<!--
                                                -->月給
                                            {% elseif data.salary_term == "day" %}<!--
                                                -->日給
                                            {% elseif data.salary_term == "hour" %}<!--
                                                -->時給
                                            {% endif %}<!--
                                     -->{% if data.salary_unit_min !== "dollar" %}
                                            {% if data.salary_value_min=="" %}<div class="error previewErr02">まだデータ入力がありません</div>{% else %}{{ data.salary_value_min }}{% endif %}<!--
                                            -->{% if data.salary_unit_min == "man_yen" %}万円{% elseif data.salary_unit_min == "yen" %}円{% endif %}～{% if data.salary_value_max=="" %}<div class="error previewErr02">まだデータ入力がありません</div>{% else %}{{ data.salary_value_max }}{% endif %}<!--
                                            -->{% if data.salary_unit_max == "man_yen" %}万円{% elseif data.salary_unit_max == "yen" %}円{% endif %}
                                        {% else %}
                                            ${% if data.salary_value_min=="" %}<div class="error previewErr02">まだデータ入力がありません</div>{% else %}{{ data.salary_value_min }}{% endif %}～${% if data.salary_value_max=="" %}<div class="error previewErr02">まだデータ入力がありません</div>{% else %}{{ data.salary_value_max }}{% endif %}
                                        {% endif %}
                                         </div> 
                                    </td>
                                </tr>
                                <tr>
                                    <th>勤務地</th>
                                    <td>
                                        <div>
                                               {% if data.workplace_prefecture=="" %}<!--
                                            --><div class="error previewErr02">まだデータ入力がありません</div><!--
                                            -->{% else %}<!--
                                            -->{{data.workplace_prefecture}}<!--
                                            -->{% endif %}<!--
                                            -->{% if data.workplace_area=="" %}<!--
                                            --><div class="error previewErr02">まだデータ入力がありません</div><!--
                                            -->{% else %}<!--
                                            -->{{data.workplace_area}}<!--
                                            -->{% endif %}<!--
                                            -->{% if data.workplace_address=="" %}<!--
                                            --><div class="error previewErr02">まだデータ入力がありません</div><!--
                                            -->{% else %}<!--
                                            -->{{data.workplace_address}}<!--
                                            -->{% endif %}
                                        </div>
                                    </td>
                                </tr>
                            </table>
                           </div>
                           <ul class="tags"> 
                          {{ partial("/data/features_php")}}
                           </ul>
                            
                         
                        </div><!-- / cInner -->
                        <!--<p class="source">スポンサー: {{data.company_name}}</p>-->
                    </a>
                </article><!-- / pickupA01 -->
            </div><!-- / zabuton -->
        </div><!-- /.unitA01 -->

                            



                            <div class="unitA01">
                                <table class="tableB01 marginB20">
                                    <tr>
                                        <th class="ttl">掲載期間</th>
                                        <td class="ipt"><span class="fontTypeB01">[{{published_from_display}}] </span>より{{ data.publish_week }}週間　費用：<span class="fontTypeB01">{{ data.expense1 }}</span>円</td>
                                    </tr>
                                    <tr>
                                        <th class="ttl">新着表示エリア(オプション)</th>
                                        <td class="ipt">
                                            {% if data.display_area == "inner" %}
                                                インナーパネル　費用：<span class="fontTypeB01">2,000</span>円

                                            {% elseif data.display_area == "inner_half" %}
                                                インナーパネルハーフ　費用：<span class="fontTypeB01">1,000</span>円

                                            {% elseif data.display_area == "right_column" %}
                                                右カラム　費用：<span class="fontTypeB01">500</span>円

                                            {% elseif data.display_area == "none" %}
                                                無し　費用：<span class="fontTypeB01">無料</span>
                                            {% endif %}
                                        </td>
                                    </tr>
                                    
                                    {% if data.detail_content_type!=="none" %}
                                    <tr>
                                        <th class="ttl">詳細ページ</th>
                                        <td class="ipt"><input type="button" class="inputA04" value="プレビュー" />別ウインドウ表示</td>
                                    </tr>
                                    {% endif %}
                                </table> 
 
