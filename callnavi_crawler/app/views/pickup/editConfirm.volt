<div id="contents">
<div id="contentsContainer">
<!-- InstanceBeginEditable name="mainContents" -->

<div class="unitA01">
{{ partial("/pickup/preview_template")}}
<input type="hidden" name="offer_id" value="{{data.id}}"/>
<div class="alignC">
<div style="display:inline-flex">
<a href="/pickup/edit?offer_id={{ data.id }}&account_id={{ data.account_id}}"><button class="btnA01" marginR15">入力内容を修正する</button></a>
&nbsp;&emsp;<a href="/pickup/editThanks?offer_id={{data.id}}&account_id={{ data.account_id }}"><button class="btnA02">この内容で掲載依頼する</button></a>
</div>
</div>
</div><!-- /.unitA01 -->

<!-- InstanceEndEditable -->
</div><!-- / contentsContainer -->
</div><!-- / contents -->