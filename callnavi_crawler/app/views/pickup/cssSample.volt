<div class="unitC01">
<div class="close"><a href="javascript:void(0)" onclick="window.close();"><img src="/public/admin_images/btn_close_n.png" alt="このウィンドウを閉じる"></a></div>
<h2 class="headingA01">CSSのレイアウトサンプル</h2>
<p class="marginB15">HTMLタグによる自由レイアウトが可能です。<br />CSSは『コールナビ』の規定に準じます。</p>
<ul class="alertB01 marginB30">
<li>※HTML内のimage画像は『コールナビ』のサーバーにはアップロードできません。外部サーバーのimage画像へ絶対パスで記入してください。</li>
<li>※外部サーバーからのcssやjsの読み込みは無効となります。style属性は使用可能です。</li>
</ul>

<section class="unitB03 documentA01">
<div class="headingB01">
<h3>ユニット</h3>
</div>
<div class="unitD01">
<iframe src="/pickupModule/moduleUnit" scrolling="no" seamless class="iframeA01" height="640"></iframe>
</div>
</section>

<section class="unitB03 documentA01">
<div class="headingB01">
<h3>見出し</h3>
</div>
<div class="unitD01">
<iframe src="/pickupModule/moduleHeading" scrolling="no" seamless class="iframeA01" height="1480"></iframe>
</div>
</section>

<section class="unitB03 documentA01">
<div class="headingB01">
<h3>本文</h3>
</div>
<div class="unitD01">
<iframe src="/pickupModule/moduleBlock" scrolling="no" seamless class="iframeA01" height="1530"></iframe>
</div>
</section>

<section class="unitB03 documentA01">
<div class="headingB01">
<h3>テーブル</h3>
</div>
<div class="unitD01">
<iframe src="/pickupModule/moduleTable" scrolling="no" seamless class="iframeA01" height="810"></iframe>
</div>
</section>

<section class="unitB03 documentA01">
<div class="headingB01">
<h3>リスト</h3>
</div>
<div class="unitD01">
<iframe src="/pickupModule/moduleList" scrolling="no" seamless class="iframeA01" height="1230"></iframe>
</div>
</section>

<section class="unitB03 documentA01">
<div class="headingB01">
<h3>ボタン</h3>
</div>
<div class="unitD01">
<iframe src="/pickupModule/moduleButton" scrolling="no" seamless class="iframeA01" height="1400"></iframe>
</div>
</section>

<section class="unitB03 documentA01">
<div class="headingB01">
<h3>その他</h3>
</div>
<div class="unitD01">
<iframe src="/pickupModule/moduleOther" scrolling="no" seamless class="iframeA01" height="840"></iframe>
</div>
</section>

</div><!--//.unitC01-->
