<div id="contents">
<div id="contentsContainer">
<!-- InstanceBeginEditable name="mainContents" -->
<div class="unitA01">
<h2 class="headingA01">新規求人広告作成</h2>
<div class="headingB01">
<h3>ピックアップ広告</h3>
<span class="hyoujiArea"><a href="/customer/displayArea#area03" class="blank">表示エリアについて</a></span>
</div>

<p class="alignC fontSize16 marginB30">掲載可能な新着表示エリアを予めご確認ください。</p>
<ul class="boxB01">
<li>インナーパネル</li>
<li><span class="price">2,000</span>円／1週</li>
</ul>
<!--<p class="fontTypeA01">掲載枠に空きがあるため、即日掲載可能です。</p>-->

<ul class="boxB01">
<li>インナーパネルハーフ</li>
<li><span class="price">1,000</span>円／1週</li>
</ul>
<!--<p class="fontTypeA02">掲載枠に空きがありません。<br>最短で［2014/03/01］からの掲載となります。</p>
<p class="fontTypeA02 fontSize12">※他の求人広告出稿の都合上、掲載期間が前後する可能性がございます。</p>-->

<ul class="boxB01">
<li>右カラム</li>
<li><span class="price">500</span>円／1週</li>
</ul>
<!--<p class="fontTypeA01">掲載枠に空きがあるため、即日掲載可能です。</p>-->

<ul class="boxB01">
<li>ロゴバナー</li>
<li><span class="price">無料</span>／1週</li>
</ul>
<!--<p class="fontTypeA01">掲載枠に空きがあるため、即日掲載可能です。</p>-->


<p class="alignC"><a href="/pickup/makeStart" class="btnA02 w350">ピックアップ広告の作成へ進む</a></p>
</div><!-- /.unitA01 -->
<!-- InstanceEndEditable -->
</div><!-- / contentsContainer -->
</div><!-- / contents -->