<div id="contents">
<div id="contentsContainer">
<!-- InstanceBeginEditable name="mainContents" -->

<div class="unitA01">
<h2 class="headingA01">新規求人広告作成</h2>
<div class="headingB01">
<h3>ピックアップ広告</h3>
<span class="hyoujiArea"><a href="/customer/displayArea#area03" class="blank">表示エリアについて</a></span>
</div>
<p>プレビュー表示</p>
<p class="fontTypeB01">※広告をクリックするとご指定のURLが別ウインドウで表示されます。</p>
<div class="zabuton">
<article class="pickupB01">
<a href="#">
<p class="image"><img src="/public/images/pickup/{{ data.id }}.png" width="160" height="120" alt=""></p>
<h3>{{data.message}}</h3>
<p class="company">{{data.company_name}}</p>
<p class="summary">{{data.message}}</p>
<table>
<tr>
    <th>給与</th>
    <td><div>{% if data.salary_term == "year" %}
        年収
    {% elseif data.salary_term == "month"  %}
        月収
    {% elseif data.salary_term == "day" %}
        日給
    {% elseif data.salary_term == "hour" %}
        時給
    {% endif %}
    {{ data.salary_value_min }}{% if data.salary_unit_min == "man_yen" %}万円{% elseif data.salary_unit_min == "yen" %}円{% elseif data.salary_unit_min == "doller" %}＄{% endif %}～{{ data.salary_value_max }}{% if data.salary_unit_max == "man_yen" %}万円{% elseif data.salary_unit_max == "yen" %}円{% elseif data.salary_unit_max == "doller" %}＄{% endif %}
    <br>交通費：一部支給<br>月1万円まで支給</div></td>
</tr>
<tr>
<th>勤務地</th>
<td><div>{{data.workplace_prefecture}}{{data.workplace_area}}{{data.workplace_address}}</div></td>
</tr>
</table>
</a>
</article><!-- / pickupB02 -->
</div><!-- / zabuton -->
</div><!-- /.unitA01 -->

<div class="unitA01">
<table class="tableB01 marginB20">
<tr>
<th class="ttl">表示エリア選択</th>
<td class="ipt">
{% if data.display_area == "inner" %}
    インナーパネル
{% elseif data.display_area == "inner_half" %}
    インナーパネルハーフ
{% elseif data.display_area == "right_column" %}
    右カラム
{% elseif data.display_area == "logo_banner" %}
    ロゴバナー
{% endif %}
</td>
</tr>
<tr>
<th class="ttl">掲載期間</th>
<td class="ipt"><span class="fontTypeB01">[{{ data.published_from }}] </span>より{{ data.publish_week }}週間　費用：<span class="fontTypeB01">{{ data.expense }}</span>円</td>
</tr>
<tr>
<th class="ttl">詳細ページ</th>
<td class="ipt">必要<a href="/pickup/confirmPreview?offer_id={{data.id}}" target="_blank"><input type="button" class="inputA04" value="プレビュー"/></a>別ウインドウ表示</td>
</tr>
</table>
<div class="alignC">
<div style="display:inline-flex">
<a href="/pickup/edit?offer_id={{ data.id }}&account_id={{ data.account_id }}"><button class="btnA01" marginR15">入力内容を修正する</button></a>
&nbsp;&emsp;<a href="/pickup/resumeThanks?offer_id={{ data.id }}&account_id={{ data.account_id }}"><button class="btnA02">この内容で掲載依頼する</button></a>
</div>
</div>
</div><!-- /.unitA01 -->

<!-- InstanceEndEditable -->
</div><!-- / contentsContainer -->
</div><!-- / contents -->