<!-- InstanceBeginEditable name="mainContents" -->

<div class="unitC01">
<div class="close"><a href="javascript:void(0)" onclick="window.close();"><img src="/public/admin_images/btn_close_n.png" alt="このウィンドウを閉じる"></a></div>
<h2 class="headingA01">HTML記述可能エリア</h2>
<p class="marginB40">HTMLタグによる自由レイアウトが可能なエリアは下記になります。<br />CSSは『コールナビ』の規定に準じます。<a href="/pickup/cssSample" class="blank">CSSのレイアウトサンプルはこちら</a></p>
<div class="alignC"><img src="/public/admin_images/area_editable.png" alt=""></div>
</div><!--//.unitC01-->
<!-- InstanceEndEditable -->
