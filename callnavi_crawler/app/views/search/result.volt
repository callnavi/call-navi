    {% if listing.id is defined %}
    <section>
        {{ partial("/offer/listing", ['listing': listing])}}
    </section>
    {% endif %}

<section>
    <div class="resultA01">
        {% if countResults is defined %}
            <header>
                <p class="keywords">{{params.keyword}}　{{params.area}}</p>
                <p class="num">検索結果{{countResults}}件</p>
            </header>
               {% if pickups is defined %}<!--
　　　　　　　　-->{% for pickup in pickups %}<!--
 　                 -->{{ partial("/offer/pickup_listing") }}
                 {% endfor %}
              {% endif %}
           {% if frees is defined %}
　　　　　　　　{% for free in frees %}
 　                 {{ partial("/offer/free") }}
                 {% endfor %}
              {% endif %} 
            
        
            {% if results is defined %}
                {% for result in results %}
                    {{ partial("/search/result_each", ['result': result]) }}


                {% endfor %}
            {% endif %}
            
            <footer>
                <dl>
                    <dt>検索結果ページ</dt>                                       
                    <dd>
                        <ul>
                            {% if page > 1  %}
                                <li class="prev">
                                    <a href="/search/result?page={{(page - 2)}}&area={{params.area}}&keyword={{params.keyword}}">前へ</a>
                                </li>
                                <li>
                                    <a href="/search/result?page={{(page - 2)}}&area={{params.area}}&keyword={{params.keyword}}">{{page-1}}</a>
                                </li>                                
                                <li>
                                    <a href="/search/result?page={{(page - 1)}}&area={{params.area}}&keyword={{params.keyword}}">{{page}}</a>
                                </li>                                
                            {% elseif page > 0 %}
                                <li class="prev">
                                    <a href="/search/result?page={{(page - 1)}}&area={{params.area}}&keyword={{params.keyword}}">前へ</a>                                    
                                </li>
                                <li>
                                    <a href="/search/result?page={{(page - 1)}}&area={{params.area}}&keyword={{params.keyword}}">{{page}}</a>
                                </li>                                
                            {% endif %}

                            <li>                                
                                <em>{{page+1}}</em>
                            </li>

                            {% if countNextPages > 1 %}
                                <li>
                                    <a href="/search/result?page={{(page + 1)}}&area={{params.area}}&keyword={{params.keyword}}">{{page+2}}</a>                                    
                                </li>
                                <li>
                                    <a href="/search/result?page={{(page + 2)}}&area={{params.area}}&keyword={{params.keyword}}">{{page+3}}</a>                                    
                                </li>
                                <li class="next">
                                    <a href="/search/result?page={{(page + 2)}}&area={{params.area}}&keyword={{params.keyword}}">次へ</a>                                    
                                </li>
                            {% elseif countNextPages > 0 %}
                                <li>
                                    <a href="/search/result?page={{(page + 1)}}&area={{params.area}}&keyword={{params.keyword}}">{{page+2}}</a>                                    
                                </li>
                                <li class="next">
                                    <a href="/search/result?page={{(page + 1)}}&area={{params.area}}&keyword={{params.keyword}}">次へ</a>                                    
                                </li>
                            {% endif %}
                        </ul>
                    </dd>
                </dl>
            </footer>
        {% endif %}                    
    </div><!-- / resultA01 -->
</section>
