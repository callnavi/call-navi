<article {% if result.site_id == "11" and result.unique_id is defined %}class="freeA01 impression_offer free_offer" offer_id="{{result.unique_id}}" offer_type="free"{% endif %}>
    <a href="{{result.url}}" target="_blank" rel="nofollow">
        <h2>{{result.title}}</h2>
        <div class="contentsInner">
            <p class="image"><img src="{{result.pic_url}}" width="" height="" alt=""></p>
            <p class="company">{{result.company}}</p>
            <p class="summary mediaPC">{{result.desc}}</p>
            
        </div><!-- / contentsInner -->
          
        <div class="contentsInner">
          <div class="details">
              {% if result.site_id == "11" and result.hired_as is defined %}
            <?php $hired_as = explode(':', $result->hired_as) ; ?>
                {{ partial("/data/hired_as_preview")}}
              {% endif %}
            <table>
                <tr>
                    <th>給　与</th>
                    <td><div>{{result.salary}}</div></td>
                </tr>
                <tr>
                    <th>勤務地</th>
                    <td><div>{{result.workplace}}</div></td>
                </tr>
            </table>
          </div>
            <ul class="tags">
              {% if result.site_id !== "11" %}
                {% if result.labels | length > 1 %}
                    {% for label in result.labels %}
                        <li>
                            {{label}}
                        </li>
                    {% endfor %}
                {% endif %}
              {% else %}
                {{ partial("/data/features_php" , ['features': result.labels])}}
              {% endif %}
            </ul>
        </div><!-- / contentsInner -->
        <p class="source">提供元 - {{result.site_name}}- {{result.update_before}}</p>
    </a>
</article>
