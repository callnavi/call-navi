<div id="contents">
<div id="contentsContainer">

<form id="make_listing_form" method="post" name="make_listing_form" action="/listing/makeConfirm" enctype="multipart/form-data" >
<div class="unitA01">
<h2 class="headingA01">新規求人広告作成</h2>
<div class="headingB01">
<h3>リスティング広告</h3>
<span class="hyoujiArea"><a href="/customer/displayArea#area01" class="blank">表示エリアについて</a></span>
</div>
<span class="inputError"></span>
<table class="tableB01">
<tr class="title">
<th class="ttl">広告名（内部管理用）</th>
<th class="nsc"><span class="must">必須</span></th>
<td class="ipt"><input type="text" name="title" class="inputA01 marginB05 w20em" value="{% if data.title is defined %}{{data.title}}{% else %}{% endif %}" maxlength="20">
<ul class="alertA01">
<li>※20文字以内で記入してください。</li>
<li>※ユーザー側の画面には表示されません。</li>
</ul></td>
</tr>
<tr class="job_category">
<th class="ttl">募集職種</th>
<th class="nsc"><span class="must">必須</span></th>
<td class="ipt"><input type="text" name="job_category" class="inputA01 marginB05 w20em" value="{% if data.job_category is defined %}{{data.job_category}}{% else %}{% endif %}" maxlength="20" />
<ul class="alertA01">
<li>※20文字以内で記入してください。</li>
</ul>
</td>
</tr>
<tr class="hired_as">
<th class="ttl">雇用形態</th>
<th class="nsc"><span class="must">必須</span></th>
<td class="ipt">
{{ partial("/data/hired_as_form")}}

<input type="hidden" name="hired_as_string" />
</td>
</tr>
<tr class="company_name">
<th class="ttl">募集会社名</th>
<th class="nsc"><span class="must">必須</span></th>
<td class="ipt"><input type="text" name="company_name" class="inputA01 marginB05 w30em" value="{% if data.company_name is defined %}{{data.company_name}}{% else %}{% endif %}" maxlength="30">
<ul class="alertA01">
<li>※30文字以内で記入してください。</li>
</ul>
</td>
</tr>
<tr class="salary">
<th class="ttl">給与</th>
<th class="nsc"><span class="must">必須</span></th>
<td class="ipt">
<ul class="listB03 marginB05">
<li><select name="salary_term"><option value="hour"{% if data.salary_term is defined and data.salary_term =="hour" %}selected{% else %}{% endif %}>時給</option><option value="day"{% if data.salary_term is defined and data.salary_term =="day" %}selected{% else %}{% endif %}>日給</option><option value="month"{% if data.salary_term is defined and data.salary_term =="month" %}selected{% else %}{% endif %}>月給</option><option value="year"{% if data.salary_term is defined and data.salary_term=="year" %}selected{% else %}{% endif %}>年収</option></select></li>
<li><input type="text" name="salary_value_min" class="inputA02 marginR10" value="{% if data.salary_value_min is defined %}{{data.salary_value_min}}{% endif %}" maxlength="5"><select name="salary_unit_min"><option value="yen"{% if data.salary_unit_min is defined and data.salary_unit_min =="yen" %}selected{% else %}{% endif %}>円</option><option value="man_yen"{% if data.salary_unit_min is defined and data.salary_unit_min =="man_yen" %}selected{% else %}{% endif %}>万円</option><option value="dollar"{% if data.salary_unit_min is defined and data.salary_unit_min =="dollar" %}selected{% else %}{% endif %}>＄</option></select></li>
<li>〜</li>
<li><input type="text" name="salary_value_max" class="inputA02 marginR10" value="{% if data.salary_value_max is defined %}{{data.salary_value_max}}{% endif %}" maxlength="5"><select name="salary_unit_max"><option value="yen"{% if data.salary_unit_min is defined and data.salary_unit_min=="yen" %}selected{% else %}{% endif %}>円</option><option value="dollar"{% if data.salary_unit_min is defined and data.salary_unit_min=="dollar" %}selected{% else %}{% endif %}>＄</option><option value="man_yen"{% if data.salary_unit_min is defined and data.salary_unit_min=="man_yen" %}selected{% else %}{% endif %}>万円</option></select></li>
</ul>
<ul class="alertA01">
<li>※半角数字で記入してください。</li>
</ul>
</td>
</tr>

<tr class="workplace">
<th class="ttl">勤務地</th>
<th class="nsc"><span class="must">必須</span></th>
<td class="ipt">
<ul class="listB03 marginB10">
<li><span class="marginR10">都道府県</span><select name="workplace_prefecture" class="sfr"><option value="">選択してください</option><option value="北海道"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="北海道" %}selected{% else %}{% endif %}>北海道</option><option value="青森県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="青森県" %}selected{% else %}{% endif %}>青森県</option><option value="秋田県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="秋田県" %}selected{% else %}{% endif %}>秋田県</option><option value="岩手県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="岩手県" %}selected{% else %}{% endif %}>岩手県</option><option value="宮城県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="宮城県" %}selected{% else %}{% endif %}>宮城県</option><option value="山形県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="山形県" %}selected{% else %}{% endif %}>山形県</option><option value="福島県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="福島県" %}selected{% else %}{% endif %}>福島県</option><option value="茨城県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="茨城県" %}selected{% else %}{% endif %}>茨城県</option><option value="千葉県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="千葉県" %}selected{% else %}{% endif %}>千葉県</option><option value="栃木県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="栃木県" %}selected{% else %}{% endif %}>栃木県</option><option value="埼玉県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="埼玉県" %}selected{% else %}{% endif %}>埼玉県</option><option value="東京都"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="東京都" %}selected{% else %}{% endif %}>東京都</option><option value="群馬県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="群馬県" %}selected{% else %}{% endif %}>群馬県</option><option value="神奈川県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="神奈川県" %}selected{% else %}{% endif %}>神奈川県</option><option value="新潟県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="新潟県" %}selected{% else %}{% endif %}>新潟県</option><option value="富山県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="富山県" %}selected{% else %}{% endif %}>富山県</option><option value="石川県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="石川県" %}selected{% else %}{% endif %}>石川県</option><option value="福井県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="福井県" %}selected{% else %}{% endif %}>福井県</option><option value="山梨県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="山梨県" %}selected{% else %}{% endif %}>山梨県</option><option value="静岡県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="静岡県" %}selected{% else %}{% endif %}>静岡県</option><option value="長野県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="長野県" %}selected{% else %}{% endif %}>長野県</option><option value="愛知県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="愛知県" %}selected{% else %}{% endif %}>愛知県</option><option value="岐阜県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="岐阜県" %}selected{% else %}{% endif %}>岐阜県</option><option value="滋賀県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="滋賀県" %}selected{% else %}{% endif %}>滋賀県</option><option value="三重県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="三重県" %}selected{% else %}{% endif %}>三重県</option><option value="京都府"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="京都府" %}selected{% else %}{% endif %}>京都府</option><option value="奈良県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="奈良県" %}selected{% else %}{% endif %}>奈良県</option><option value="大阪府"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="大阪府" %}selected{% else %}{% endif %}>大阪府</option><option value="和歌山県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="和歌山県" %}selected{% else %}{% endif %}>和歌山県</option><option value="兵庫県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="兵庫県" %}selected{% else %}{% endif %}>兵庫県</option><option value="鳥取県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="鳥取県" %}selected{% else %}{% endif %}>鳥取県</option><option value="岡山県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="岡山県" %}selected{% else %}{% endif %}>岡山県</option><option value="島根県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="鳥取県" %}selected{% else %}{% endif %}>島根県</option><option value="広島県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="広島県" %}selected{% else %}{% endif %}>広島県</option><option value="山口県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="山口県" %}selected{% else %}{% endif %}>山口県</option><option value="香川県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="香川県" %}selected{% else %}{% endif %}>香川県</option><option value="徳島県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="徳島県" %}selected{% else %}{% endif %}>徳島県</option><option value="愛媛県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="愛媛県" %}selected{% else %}{% endif %}>愛媛県</option><option value="高知県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="高知県" %}selected{% else %}{% endif %}>高知県</option><option value="大分県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="大分県" %}selected{% else %}{% endif %}>大分県</option><option value="宮崎県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="宮崎県" %}selected{% else %}{% endif %}>宮崎県</option><option value="鹿児島県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="鹿児島県" %}selected{% else %}{% endif %}>鹿児島県</option><option value="福岡県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="福岡県" %}selected{% else %}{% endif %}>福岡県</option><option value="熊本県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="熊本県" %}selected{% else %}{% endif %}>熊本県</option><option value="佐賀県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="佐賀県" %}selected{% else %}{% endif %}>佐賀県</option><option value="長崎県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="長崎県" %}selected{% else %}{% endif %}>長崎県</option><option value="沖縄県"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="沖縄県" %}selected{% else %}{% endif %}>沖縄県</option><option value="その他(海外など)"{% if data.workplace_prefecture is defined and data.workplace_prefecture=="その他(海外など)" %}selected{% else %}{% endif %}>その他(海外など)</option></select></li>
<li><span class="marginR10">市区町村名</span><input type="text" name="workplace_area" class="inputA02" value="{% if data.workplace_area is defined %}{{data.workplace_area}}{% else %}{% endif %}"/></li>
</ul>
<ul class="listB04">
<li>番地・建物名</li>
<li class="alignL"><input type="text" name="workplace_address" class="inputA01 marginB05 w40em" value="{% if data.workplace_address is defined %}{{data.workplace_address }}{% else %}{% endif %}" maxlength="40" /></li>	
</ul>
<ul class="alertA01">
<li>※番地・建物名は40文字以内で記入してください。</li>
</ul>
</td>
</tr>
<tr class="job_description">
<th class="ttl">仕事内容・応募者へのメッセージなど</th>
<th class="nsc"><span class="must">必須</span></th>
<td class="ipt"><textarea name="job_description" class="inputA03 marginB05 js-textCount">{% if data.job_description is defined %}{{data.job_description|striptags}}{% else %}{% endif %}</textarea>
<ul class="alertA01 clearfix">
<li class="floatL">※120文字以内で記入してください。</li>
<li class="floatR">残り文字数：<span class="js-textLimit">120</span></li>
</ul>
</td>
</tr>
<tr class="features">
<th class="ttl">こだわり条件（5つまで）</th>
<th class="nsc"><span class="recommend">推奨</span></th>
<td class="ipt">
{{ partial("/data/features_form")}}
<input type="hidden" name="features_string"/>
</td>
</tr>
<tr class="picture">  
<th class="ttl">広告用サムネイル画像</th>
<th class="nsc"><span class="must">必須</span></th>
<td class="ipt">
<ul class="listB05">
{% if data.has_pic is defined and data.has_pic==1 %}
<div id="uploaded">
<div><img src="/public/images/listing/{{ data.id }}.png" width="300" height="" alt=""></div>
<br>
<label><input type="button" id="delete" class="inputA07" value="画像を削除する"></label>
<br>
</div>
<br>
{% endif %}   
<div><li><input type="file" id="listing_picture" class="inputA08 marginL00" accept="image/png, image/gif, image/jpeg" name="listing_picture"></li></div>
</ul>
<ul class="alertA01">
<li>※ファイルの種類：gif、jpeg、png</li>
<li>※ファイルサイズ：1MBまで</li>
<li>※推奨画像サイズ：縦450pixel × 横600pixel（縦横比 3:4）</li>
</ul>
<input type="hidden" name="has_pic" value={% if data.has_pic is defined %}"{{data.has_pic}}"{% else %}"0"{% endif %}/>
<input type="hidden" name="offer_id_set" value={% if data.id is defined %}"{{data.id}}"{% else %}"-1"{% endif %}/>
<input type="hidden" name="post" value="{{post}}"/>
</td>
</tr>
</table>
</div><!-- /.unitA01 -->

<div class="unitA01">
<table class="tableB01 marginB30">
<tr class="keywords">
<th class="ttl">検索キーワード</th>
<th class="nsc"><span class="must">必須</span></th>
<td class="ipt"><textarea name="keywords" class="inputA03 marginB05">{% if keywords is defined %}{{keywords}}{% else %}{% endif %}</textarea>
<ul class="alertA01">
    
<li>※単独キーワード、複合キーワードともに1行ごとに分けて記入してください。</li>
<li>※複合キーワードは、1キーワードにつき最大で5つの語句を指定できます。</li>
<li>※単独キーワードと複合キーワードを合わせて、100個までキーワードを設定できます。</li>
<li>検索キーワードの入力例は<a href="/listing/keywordsSample" class="blank">こちら</a></li>

</ul>
<input type="hidden" name="allkeywords_string"/>
</td>
</tr>
<tr class="click_price">
<th class="ttl">クリック単価</th>
<th class="nsc"><span class="must">必須</span></th>
<td class="ipt"><input type="text" name="click_price" class="inputA02 marginR10 marginB05" value="{% if data.click_price is defined %}{{data.click_price}}{% endif %}"/>円　現在の平均相場：<em class="hiyou">{% if average_click_price>=1000 %}
{{(average_click_price-average_click_price%1000)/1000}},<!--
-->{% endif %}<!--
-->{% if average_click_price<1000 %}{{average_click_price}}<!--
-->{% elseif average_click_price%1000>=100 %}<!--
-->{{average_click_price%1000}}<!--
-->{% elseif average_click_price%1000>=10 %}<!--
-->0{{average_click_price%1000}}<!--
-->{% else %}<!--
-->00{{average_click_price%1000}}
{% endif %}</em>円
<ul class="alertA01">
<li>※半角数字で記入してください。</li>
<li>※最低10円以上で記入してください。</li>
</ul></td>
</tr>
<tr class="budget_max">
<th class="ttl">上限広告予算</th>
<th class="nsc"><span class="recommend">推奨</span></th>
<td class="ipt"><input type="text" name="budget_max" class="inputA02 marginR10 marginB05" value="{% if data.budget_max is defined %}{{data.budget_max}}{% endif %}">円／1日
<ul class="alertA01">
<li>※半角数字で記入してください。</li>
</ul>
</td>
</tr>
<tr class="detail_url">
<th class="ttl">リンク先URL<br>
（求人詳細ページ・応募フォームなど）</th>
<th class="nsc"><span class="must">必須</span></th>
<td class="ipt"><input type="text" name="detail_url" class="inputA01 marginB05" value="{% if data.detail_url is defined %}{{data.detail_url}}{% endif %}"/>
<ul class="alertA01">
<li>※『コールナビ』内には応募フォームはございませんので、お客様側でご用意ください。</li>
<li>※半角英数字で記入してください。</li>
<li>※http://から記載してください。</li>
</ul></td>
</tr>
</table>
<div class="alignC">
<button class="btnA01 marginR15">入力内容を保存する</button>

<button class="btnA02">保存して確認画面へ進む</button>

</div>
</div><!-- /.unitA01 -->
</form>

</div><!-- / contentsContainer -->
</div><!-- / contents -->