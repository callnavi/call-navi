<div class="unitC01">
<div class="close"><a href="javascript:void(0)" onclick="window.close();"><img src="/public/admin_images/btn_close_n.png" alt="このウィンドウを閉じる"></a></div>
<h2 class="headingA01">検索キーワード入力例</h2>
<p class="marginB40">単独キーワード、複合キーワードともに1キーワードごとに改行してください。<br>
複合キーワードは、1キーワードにつき最大で5つの語句を指定できます。<br>
単独キーワードと複合キーワードを合わせて、100個までキーワードを設定できます。</p>
<section class="unitB03 alignC">
<img src="/public/admin_images/sample_listing_words.gif" alt="" />
</section>
</div><!--//.unitC01-->


