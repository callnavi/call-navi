  <div id="contents"> 
    <div id="contentsContainer">
        <!-- InstanceBeginEditable name="mainContents" -->
                <div class="unitA01">
                    <h2 class="headingA01">求人広告管理</h2>
                    <div class="headingB01">
                    <h3>リスティング広告</h3>
                    <span class="hyoujiArea"><a href="/customer/displayArea#area01" class="blank">表示エリアについて</a></span>
                </div>
                <div class="clearfix">
                    <ul class="formBlockA01">
                        <li>対象期間</li>
                        <li><select class="formA01 sfr" name="keyword_scope">
                            <option value="today">今日</option>
                            <option value="yesterday">昨日</option>
                            <option value="week">今週（月～）</option>
                            <option value="seven_days">過去7日間：{{ one_week_ago }} 〜 {{ today }}</option>
                            <option value="last_week">先週（月～日）</option>
                            <option value="month">今月</option>
                            <option value="thirty_days">過去30日間</option>
                            <option value="last_month">先月</option>
                            <option value="all">全期間</option>
                            </select>   
                        </li>
                        <li><input type="button" class="formA01" value="適用" id="change_keyword_scope" data-offer_id="{{ offer_id }}"></li>
                    </ul>
                </div>
                <div class="boxC01">{{ job_category }}</div>
                <p class="alignR marginB15"><input type="button" class="formA01" value="広告内容・検索キーワードの編集" onClick="javascript:location.href='/listing/edit?offer_id={{ offer_id }}&account_id={{ account_id }}';"></p>
                <table class="tableA01">
                    <tr>
                        <th>操作</th>
                        <th class="alignL">検索キーワード</th>
                        <th>表示回数</th>
                        <th>クリック数</th>
                        <th>クリック率</th>
                        <th>クリック単価</th>
                        <th>費用</th>
                    </tr>
                    {% if keywords_count > 0 %}
                        {% for keyword in keywords %}
                                <tr id="keyword_{{ keyword.id }}" data-keywords="{{ keyword.keywords }}">
                                    <td>
                                        <div class="status before_change" id="keyword_display_{{ keyword.id }}">
                                            {% if keyword.status == "judging" %}
                                                <span class="ttl"><img src="/public/admin_images/table_select_00_1.png" width="44" height="28" alt=""></span>
                                                <ul>                                                    
                                                    <li id="keyword_delete_{{ keyword.id }}" data-keyword_id="{{ keyword.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                                </ul>
                                            {% elseif keyword.status == "publishing" %}
                                                <span class="ttl"><img src="/public/admin_images/table_select_01_1.png" width="44" height="28" alt=""></span>
                                                <ul>
                                                    <li class="status_02"><a href="/listing/edit?offer_id={{ offer_id }}&account_id={{ account_id }}" class="st2">編集</a></li>
                                                    <li id="keyword_cancel_{{ keyword.id }}" data-keyword_id="{{ keyword.id }}" class="status_03"><a href="javascript:void(0);" class="st3">一時停止</a></li>                                        
                                                    <li id="keyword_delete_{{ keyword.id }}" data-keyword_id="{{ keyword.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                                </ul>
                                            {% elseif keyword.status == "will_publish" %}
                                                <span class="ttl"><img src="/public/admin_images/table_select_00_1.png" width="44" height="28" alt=""></span>
                                                <ul>
                                                    <li class="status_02"><a href="/listing/edit?offer_id={{ offer_id }}&account_id={{ account_id }}" class="st2">編集</a></li>
                                                    <li id="keyword_delete_{{ keyword.id }}" data-keyword_id="{{ keyword.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                                </ul>
                                            {% elseif keyword.status == "not_allowed" %}
                                                <span class="ttl"><img src="/public/admin_images/table_select_00_1.png" width="44" height="28" alt=""></span>
                                                <ul>
                                                    <li class="status_02"><a href="/listing/edit?offer_id={{ offer_id }}&account_id={{ account_id }}" class="st2">編集</a></li>
                                                    <li id="keyword_delete_{{ keyword.id }}" data-keyword_id="{{ keyword.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                                </ul>
                                          {% elseif keyword.status == "waiting_card" %}
                                                <span class="ttl"><img src="/public/admin_images/table_select_00_1.png" width="44" height="28" alt=""></span>
                                                <ul>
                                                    <li class="status_02"><a href="/listing/edit?offer_id={{ offer_id }}&account_id={{ account_id }}" class="st2">編集</a></li>
                                                    <li id="keyword_delete_{{ keyword.id }}" data-keyword_id="{{ keyword.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                                </ul>
                                          {% elseif keyword.status == "editing" %}
                                                <span class="ttl"><img src="/public/admin_images/table_select_02_1.png" width="44" height="28" alt=""></span>
                                                <ul>
                                                    <li class="status_02"><a href="/listing/edit?offer_id={{ offer_id }}&account_id={{ account_id }}" class="st2">編集</a></li>
                                                    <li id="keyword_delete_{{ keyword.id }}" data-keyword_id="{{ keyword.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                                </ul>
                                            {% elseif keyword.status == "canceled" %}
                                                <span class="ttl"><img src="/public/admin_images/table_select_03_1.png" width="44" height="28" alt=""></span>
                                                <ul>
                                                    <li id="keyword_effect_{{ keyword.id }}" data-keyword_id="{{ keyword.id }}" class="status_01"><a href="javascript:void(0);" class="st1">有効</a></li>                                       
                                                    <li class="status_02"><a href="/listing/edit?offer_id={{ offer_id }}&account_id={{ account_id }}" class="st2">編集</a></li>
                                                    <li id="keyword_delete_{{ keyword.id }}" data-keyword_id="{{ keyword.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                                </ul>
                                            {% endif %}
                                        </div>
                                        <div class="status publishing" id="keyword_publishing_{{ keyword.id }}" style="display :none;">
                                            <span class="ttl"><img src="/public/admin_images/table_select_01_1.png" width="44" height="28" alt=""></span>
                                            <ul>
                                                <li class="status_02"><a href="/listing/edit?offer_id={{ offer_id }}&account_id={{ account_id }}" class="st2">編集</a></li>
                                                <li id="keyword_cancel_{{ keyword.id }}" data-keyword_id="{{ keyword.id }}" class="status_03"><a href="javascript:void(0);" class="st3">一時停止</a></li>                                        
                                                <li id="keyword_delete_{{ keyword.id }}" data-keyword_id="{{ keyword.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                            </ul>
                                        </div>
                                        <div class="status waiting_card" id="keyword_waiting_card_{{ keyword.id }}" style="display :none;">
                                            <span class="ttl"><img src="/public/admin_images/table_select_00_1.png" width="44" height="28" alt=""></span>
                                            <ul>
                                                <li class="status_02"><a href="/listing/edit?offer_id={{ offer_id }}&account_id={{ account_id }}" class="st2">編集</a></li>
                                                <li id="keyword_delete_{{ keyword.id }}" data-keyword_id="{{ keyword.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                            </ul>
                                        </div>
                                        <div class="status canceled" id="keyword_canceled_{{ keyword.id }}" style="display: none;"> 
                                           <span class="ttl"><img src="/public/admin_images/table_select_03_1.png" width="44" height="28" alt=""></span>
                                           <ul>
                                               <li id="keyword_effect_{{ keyword.id }}" data-keyword_id="{{ keyword.id }}" class="status_01"><a href="javascript:void(0);" class="st1">有効</a></li>                                     
                                               <li class="status_02"><a href="/listing/edit?offer_id={{ offer_id }}&account_id={{ account_id }}" class="st2">編集</a></li>
                                               <li id="keyword_delete_{{ keyword.id }}" data-keyword_id="{{ keyword.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                           </ul>
                                        </div>
                                    </td>
                                    <td class="alignL">{{ keyword.keywords }}</td>
                                    <td id="keyword_impression_log_{{ keyword.id }}">{{ keyword.impression_log }}</td>
                                    <td id="keyword_click_log_{{ keyword.id }}">{{ keyword.click_log }}</td>
                                    <td>
                                    {% if keyword.click_ratio === false  %}
                                        <span id="keyword_click_ratio_{{ keyword.id }}">-</span>%
                                    {% else %}
                                        <span id="keyword_click_ratio_{{ keyword.id }}">{{ keyword.click_ratio }}</span>%
                                    {% endif %}
                                    </td>
                                    {% if without_card == 0 %}
                                    <td>&yen;{{ keyword.click_price }}</td>
                                    {% else %}
                                    <td><font color="#ff0000">&yen;{{ keyword.click_price }}</front></td>
                                    {% endif %}
                                    {% if without_card == 0 %}
                                    <td>&yen;<span id="keyword_expense_{{ keyword.id }}">{{ keyword.expense }}</span></td>
                                    {% else %}
                                    <td><font color="#ff0000">&yen;<span id="keyword_expense_{{ keyword.id }}">{{ keyword.expense }}</span></front></td>
                                    {% endif %}
                                </tr>
                        {% endfor %}
<!--
                                <tr class="lastCol"> 
                                    {% if without_card == 0 %}
                                    <td colspan="7" class="lastCell pr20" id="keyword_lastCol">対象期間合計  &yen;<span id="keyword_expense_sum">{{ expense_sum }}</span></td>
                                    {% else %} 
                                    <td colspan="7" class="lastCell pr20" id="keyword_lastCol"> <font color="#ff0000">カード無し決済合計 &yen;<span id="keyword_expense_sum">{{ expense_sum }}</span></font></td>
                                    {% endif %} 
                                </tr>
-->
                    {% else %}
                        <tr class="lastCol">
                        <td>
                        <div class="status"><span class="ttl"></span></div>
                        </td>
                        <td class="alignL">登録されているキーワードはありません。</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        </tr> 
                    {% endif %}
                        <tr class="lastCol" id="keyword_deleted_all" style="display: none;">
                        <td>
                        <div class="status"><span class="ttl"></span></div>
                        </td>
                        <td class="alignL">登録されているキーワードはありません。</td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        <td></td>
                        </tr> 
                </table>
            </div><!-- /.unitA01 -->
        <!-- InstanceEndEditable -->
    </div><!-- / contentsContainer -->
</div><!-- / contents -->