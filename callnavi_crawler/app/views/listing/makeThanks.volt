<div id="contents">
    <div id="contentsContainer">
        <!-- InstanceBeginEditable name="mainContents" -->
        <div class="unitA01">
            <h2 class="headingA01">新規求人広告作成</h2>
            <p class="alignC fontSize20 marginB20">ありがとうございました。<br>
                広告掲載の依頼を受付いたしました。</p>
            <div class="boxD01 marginB20">
                <p class="marginB15">【ご注意】</p>
                <p class="marginB15">以下の場合、掲載をお断りする場合があります。</p>
                <p class="marginB15">事業内容・募集内容が法令に抵触するもの<br>
                    均等な雇用機会を損なうおそれがあると認められるもの<br>
                    社会倫理・社会秩序に反すると認められるもの<br>
                    利用者に不利益を与えるもの<br>
                    予め提供する意思のない労働条件を表示するもの</p>
                <p>その他、弊社の掲載基準により、掲載をお断りする場合があります。</p>
            </div><!-- /boxD01 -->
            <p class="alignC marginB30">広告内容を審査後、掲載を開始いたします。</p>
            <p class="alignC"><a href="/customer/index">求人広告管理（HOME）へ戻る</a></p>
        </div><!-- /.unitA01 -->
        <!-- InstanceEndEditable -->
    </div><!-- / contentsContainer -->
</div><!-- / contents -->
