<h2 class="headingA01">新規求人広告作成</h2>
<div class="headingB01">
<h3>リスティング広告</h3>
<span class="hyoujiArea"><a href="/customer/displayArea#area01" class="blank">表示エリアについて</a></span>
</div>
<p>以下で表示された広告イメージは、検索結果ページの『リスティング広告エリア』に掲載される広告イメージです。</p>
<p class="fontTypeB01 fw">※広告をクリックするとご指定のURLが別ウインドウで表示されます。</p>
<div class="zabuton"> 
<article class="pickupA01"> 


<a {% if data.detail_url!=='' %}href="{{ data.detail_url }}" target="_blank"{% endif %}>
<h3><span>PR</span>{% if data.job_category=="" %}<div class="error">まだデータ入力がありません。</div>{% else %}{{data.job_category}}{% endif %}</h3>

<div class="cInner">
<p class="image noPkup"><img src={% if data.has_pic==0 %}"/public/admin_images/no_upload/img_no_upload_01.png"width="" height="" alt=""{% else %}"/public/images/listing/{{ data.id }}.png" width="" height="" alt=""{% endif %}></p>
{% if data.company_name=='' %}
<p class="error previewErr01">まだデータ入力がありません</p>
{% else %}
<p class="company">{{data.company_name}}</p>
{% endif %}

{% if data.job_description=='' %}
<p class="error previewErr01">まだデータ入力がありません</p>
{% else %}
<p class="summary mediaPC">{{data.job_description}}</p>
{% endif %}
</div><!-- / cInner -->
<div class="cInner">
<div class="details">
{{ partial("/data/hired_as_preview")}}  
<table>
<tr>
<th>給　与</th>
<td>    
<div>
{% if data.salary_term == "year" %}
年収
{% elseif data.salary_term == "month" %}
月給
{% elseif data.salary_term == "day" %}
日給
{% elseif data.salary_term == "hour" %}
時給

{% endif %}

{% if data.salary_unit_min !== "dollar" %}
    {% if data.salary_value_min=="" %}<div class="error previewErr02">まだデータ入力がありません</div>{% else %}{{ data.salary_value_min }}{% endif %}<!--
    -->{% if data.salary_unit_min == "man_yen" %}万円{% elseif data.salary_unit_min == "yen" %}円{% endif %}～{% if data.salary_value_max=="" %}<div class="error previewErr02">まだデータ入力がありません</div>{% else %}{{ data.salary_value_max }}{% endif %}<!--
    -->{% if data.salary_unit_max == "man_yen" %}万円{% elseif data.salary_unit_max == "yen" %}円{% endif %}
{% else %}
    ${% if data.salary_value_min=="" %}<div class="error previewErr02">まだデータ入力がありません</div>{% else %}{{ data.salary_value_min }}{% endif %}～${% if data.salary_value_max=="" %}<div class="error previewErr02">まだデータ入力がありません</div>{% else %}{{ data.salary_value_max }}{% endif %}
{% endif %}

</div>
</td>
</tr>
<tr>
<th>勤務地</th>
<td><div>{% if data.workplace_prefecture=="" %}<div class="error previewErr02">まだデータ入力がありません</div>{% else %}{{data.workplace_prefecture}}{% endif %}{% if data.workplace_area=="" %}<div class="error previewErr02">まだデータ入力がありません</div>{% else %}{{data.workplace_area}}{% endif %}{% if data.workplace_address=="" %}<div class="error previewErr02">まだデータ入力がありません</div>{% else %}{{data.workplace_address}}{% endif %}</div></td>
</tr>
</table>
</div>
<ul class="tags">
{{ partial("/data/features_php")}}
</ul>
</div><!-- / cInner -->

</a>
</article><!-- / pickupA01 -->
</div><!-- / zabuton -->
</div><!-- /.unitA01 -->

<div class="unitA01">
<table class="tableB01 marginB30">
<tr>
<th class="ttl">検索キーワード</th>
{% if keywords_flag==0 %}
<td class="error">まだデータ入力がありません。</td>
{% else %}
<td class="ipt"><p class="lh180">{{keywords_display}}</p></td>
{% endif %}
</tr>
<tr>
<th class="ttl">クリック単価</th>
{% if data.click_price==null %}
<td class="error">まだデータ入力がありません。</td>
{% else %}
<td class="ipt">{% if data.click_price>=1000 %}
{{(data.click_price-data.click_price%1000)/1000}},<!-- 
-->{% endif %}<!--
-->{% if data.click_price<1000 %}{{data.click_price}}<!--
-->{% elseif data.click_price%1000>=100 %}<!--
-->{{data.click_price%1000}}<!--
-->{% elseif data.click_price%1000>=10 %}<!--
-->0{{data.click_price%1000}}<!--
-->{% else %}<!--
-->00{{data.click_price%1000}}
{% endif %}円</td>
{% endif %}
</tr>
<tr>
<th class="ttl">上限広告予算</th>
<td class="ipt">{% if data.budget_max>=1000 %}
{{(data.budget_max-data.budget_max%1000)/1000}},<!-- 
-->{% endif %}<!--
-->{% if data.budget_max<1000 %}{{data.budget_max}}<!--
-->{% elseif data.budget_max%1000>=100 %}<!--
-->{{data.budget_max%1000}}<!--
-->{% elseif data.budget_max%1000>=10 %}<!--
-->0{{data.budget_max%1000}}<!--
-->{% else %}<!--
-->00{{data.budget_max%1000}}
{% endif %}　円／1日</td>
</tr>
<tr>
<th class="ttl">リンク先URL</th>
<td class="ipt">{% if data.detail_url !== "" %}<a href="{{data.detail_url}}" target="_blank">{{data.detail_url}}</a><p class="fontTypeB01 marginT05 marginB00">※クリックして、リンク先が正しいかご確認ください。</p>{% else %}<div class="error">まだデータ入力がありません。</div>{% endif %}</td>
</tr>
</table>