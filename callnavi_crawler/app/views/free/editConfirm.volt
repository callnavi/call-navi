<div id="contents">
    <div id="contentsContainer">
        <!-- InstanceBeginEditable name="mainContents" -->

        <div class="unitA01">
           
                    {{ partial("/free/preview_template")}}

                    <div class="alignC">
                        <div style="display:inline-flex">
                            <a href="/free/edit?offer_id={{ data.id }}&account_id={{ data.account_id}}"><button class="btnA01 marginR15">入力内容を修正する</button></a>
                            &nbsp;
                            <a href="/free/editThanks?offer_id={{ data.id}}&account_id={{ data.account_id }}"><button class="btnA02">この内容で掲載依頼する</button></a>
                        </div>
                    </div>
                </div><!-- /.unitA01 -->

                <!-- InstanceEndEditable -->
            </div><!-- / contentsContainer -->
        </div><!-- / contents -->

