<div id="contents">

  <div id="contentsContainer">
  <!-- InstanceBeginEditable name="mainContents" -->

    <form method="post" action="/login/resetPasswordProcedure" id="reset_password_start_form">

      <div class="unitA01">

        <h2 class="headingA01">パスワードの再設定</h2>
        <p class="alignC marginB15">パスワードを再設定いたします。<br>
        秘密の質問の答えと新しいパスワードをご入力ください。</p>

        <div class="boxA02 marginB15">

          <table class="tableC01 marginB20">
            <tr>
              <td colspan="2">秘密の質問</td>
              <td>
                {% if security_question_id == '0' %}
                  初めて飼ったペットの名前は？
                {% elseif security_question_id == '1' %}
                  母親の旧姓は？
                {% elseif security_question_id == '2' %}
                  子どものころのあだ名は？
                {% elseif security_question_id == '3' %}
                  一番嫌いな食べ物は？
                {% elseif security_question_id == '4' %}
                  初めて行った海外の国は？
                {% elseif security_question_id == '5' %}
                  一番好きなアーティストは？
                {% endif %}
              </td>
            </tr>
            <tr class="security_question_answer">
              <th>答え</th>
              <td><span class="must">必須</span></td>
              <td><input type="text" name="security_question_answer" class="inputA06"></td>
            </tr>
            <tr class="new_password">
              <th>新しいパスワード</th>
              <td><span class="must">必須</span></td>
              <td><input type="password" name="new_password" class="inputA06"></td>
            </tr>
            <tr class="new_password_confirmed">
              <th>パスワード（確認用）</th>
              <td><span class="must">必須</span></td>
              <td><input type="password" name="new_password_confirmed" class="inputA06"></td>
            </tr>
          </table>

          <input type="hidden" name="account_id" id="account_id" value="{{ account_id }}">

          <div class="alignC">
            <button class="btnA02">再設定</button>
          </div>

        </div>

      </div><!-- /.unitA01 -->

    </form>

  <!-- InstanceEndEditable -->
  </div><!-- / contentsContainer -->

</div><!-- / contents -->
