<div id="contents">
<div id="contentsContainer">
<!-- InstanceBeginEditable name="mainContents" -->
<form id="login_form" method="post" action="/login/checkIsUser">
<div class="unitA01">
<h2 class="headingA01">アカウントログイン</h2>

<div class="boxA02 marginB15">
<table class="tableC01 marginB20">
<tr class="email">
<th>メールアドレス</th>
<td><span class="must">必須</span></td>
<td><input type="text" name="email" class="inputA06"></td>
</tr>
<tr class="password">
<th>パスワード</th>
<td><span class="must">必須</span></td>
<td><input type="password" name="password" class="inputA06"></td>
</tr>
</table>
<div class="alignC">
<button class="btnA02" id="btnLogin">ログイン</button>
</div>
</div>

<p class="alignC"><a href="/login/resetPassword">パスワードをお忘れの方はこちら</a></p>
</div><!-- /.unitA01 -->
</form>
<!-- InstanceEndEditable -->
</div><!-- / contentsContainer -->
</div><!-- / contents -->
