<div id="contents">

  <div id="contentsContainer">
  <!-- InstanceBeginEditable name="mainContents" -->

    <form method="post" action="#">

      <div class="unitA01">

      <h2 class="headingA01">パスワードの再設定</h2>
      <p class="alignC marginB40">ありがとうございました。<br>パスワードを再設定いたしました。</p>
      <p class="alignC"><a href="/login/index">アカウントログインへ進む</a></p>

      </div><!-- /.unitA01 -->

    </form>

  <!-- InstanceEndEditable -->
  </div><!-- / contentsContainer -->

</div><!-- / contents -->
