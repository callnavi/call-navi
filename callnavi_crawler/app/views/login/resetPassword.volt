<div id="contents">

  <div id="contentsContainer">
  <!-- InstanceBeginEditable name="mainContents" -->

    <form method="post" action="#">

      <div class="unitA01">

        <h2 class="headingA01">パスワードをお忘れの方</h2>
        <p class="alignC marginB15">パスワードを再設定いたします。<br>ご登録されているメールアドレスをご記入して「送信する」ボタンをクリックしてください。</p>

        <div class="boxA02 marginB15">

          <table class="tableC01 marginB20">
            <tr>
              <th>メールアドレス</th>
              <td><span class="must">必須</span></td>
              <td><input type="text" name="mail" id="mail" class="inputA06"></td>
            </tr>
          </table>

        <div class="alignC">

        <a href="/login/resetPasswordSent" id="mail_submit" class="btnA02">送信する</a>

        </div>

      </div>

      </div><!-- /.unitA01 -->

    </form>

  <!-- InstanceEndEditable -->
  </div><!-- / contentsContainer -->

</div><!-- / contents -->
