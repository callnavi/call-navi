<header id="globalHeader">
<div id="floatBlockConteiner">
<div id="floatingHeader">
<h1 id="ci"><a href="/" class="js-baseurl">コールナビ</a></h1>
<label for="gnavCheckbox" id="gnavIcon"></label>
</div>
</div><!-- / floatBlockConteiner -->
<p id="shoulder">全国のコールセンター転職・求人一括検索！<br class="mediaSP">アルバイト・中途・新卒もおまかせ！<span class="num"><br>コールセンター求人数<em>00,000</em>件（毎日更新!）</span></p>

<nav id="globalNav">
<ul>
<li class="mediaSP"><a href="#">HOME</a></li>
<li class="about"><a href="#">コールナビとは？</a></li>
<li class="mediaSP"><a href="#">コールセンターの仕事とは？</a></li>
<li class="mediaSP"><a href="#">おすすめ求人</a></li>
<li class="contact"><a href="#">お問い合わせ</a></li>
</ul>
</nav>
</header>