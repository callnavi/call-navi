<div class="contentsInner">
<div id="mainContents">
<section>
<article class="jobDetailA01">
<header>
<h3><a href="#" target="_blank">Webデザイナー（アートディレクター候補）</a></h3>
<p class="date">更新日：0000年00月00日</p>
</header>
<div class="articleBody">
<div class="featureA01 js-slider">
<a href="#">
<div class="slidesContainer">
<div class="crossfade">
<ul class="slides">
<li><img src="/images/job_detail/metaphase_img_01.png" width="680" alt=""></li>
<li><img src="/images/job_detail/metaphase_img_02.png" width="680" alt=""></li>
<li><img src="/images/job_detail/metaphase_img_03.png" width="680" alt=""></li>
</ul>
</div>
</div>
</a>
<div class="slideControl">
<ul class="select">
<li><a href="javascript:;">1</a></li>
<li><a href="javascript:;">2</a></li>
<li><a href="javascript:;">3</a></li>
</ul>
</div>
</div><!-- /featureA01 -->
<p class="company">株式会社メタフェイズ</p>
<p class="summary">ワンランク上のWebデザイナーに！</p>
<div class="introduction">
<p>とにかくデザインが好きだという方、<br>デザイン以外の領域も積極的に学んでみたいという方。<br>当社でワンランク上のWebデザイナーを目指しませんか？</p>
<p>当社のWebデザイナーはデザイン制作はもとより、<br>クライアントへの企画提案やコンテンツ企画、情報設計などにも積極的に参加して頂きます。</p>
<p>大好きなデザインをしながら企画力やプレゼン力なども身につけ<br>立派なアートディレクターへと成長してみませんか？</p>
<p>クライアントも大手が多数です！<br>当社HPに実績ページもございます。ぜひご覧ください。</p>
<p><a href="http://www.metaphase.co.jp/" target="_blank">http://www.metaphase.co.jp/</a></p>
</div>
<table class="tableA01 description">
<col style="width:160px;">
<col>
<tr>
<th>応募要項</th>
<td>【必須スキル】<br>★Webデザイナーとして2年以上の実務経験のある方（またはそれに相当する実績のある方）<br>★Photoshop等基本的なソフトが使える方<br><br>【歓迎するスキル】<br>★企画・プレゼンなどにも参加し、アートディレクターを目指したい方<br>★情報設計やいろいろなUIに興味のある方<br>★jQuery、JavaScript、HTML5、css3を用いたビジュアル表現のできる方<br>★スマートホンサイトや大規模サイトの構築経験<br><br>※ポートフォリオとして、ご自身で制作された作品のURLをお送りください。Webに限らず、グラフィック作品やイラスト等もぜひお送りください。</td>
</tr>
<tr>
<th>雇用区分</th>
<td>正社員（中途）</td>
</tr>
<tr>
<th>勤務地</th>
<td>東京都</td>
</tr>
<tr>
<th>勤務先所在地</th>
<td>東京都新宿区新宿5-17-5 ラウンドクロス新宿5丁目ビル6F</td>
</tr>
<tr>
<th>アクセス方法</th>
<td>★新宿三丁目駅E2出口直結！雨の日も安心！★<br><br>東京メトロ丸ノ内線「新宿三丁目駅」より徒歩3分<br>東京メトロ副都心線「新宿三丁目駅」より徒歩5分<br>都営新宿線「新宿三丁目駅」より徒歩5分<br>JR新宿駅より徒歩10分<br><br>各駅E2出口から直結なので雨にも濡れません！<br><br>※詳細は下記Webサイトをご参照ください。<br><a href="http://www.metaphase.co.jp/company/access.php" target="_blank">http://www.metaphase.co.jp/company/access.php</a></td>
</tr>
<tr>
<th>年収（目安）</th>
<td>270万円～600万円<br>※能力・経験等を考慮し、当社規定により優遇します。</td>
</tr>
<tr>
<th>勤務時間</th>
<td>10:00～19:00</td>
</tr>
<tr>
<th>フレックス</th>
<td>あり（00:00～00:00）</td>
</tr>
<tr>
<th>休日</th>
<td>年間休日124日 ！！<br>◇完全週休2日制（土・日）<br>◇祝日<br>◇有給休暇<br>◇年末年始休暇<br>◇夏期休暇<br><br>※上記のほか、慶弔休暇など各種休暇制度あり。</td>
</tr>
<tr>
<th>通勤交通費</th>
<td>あり</td>
</tr>
<tr>
<th>保険</th>
<td>社会保険完備</td>
</tr>
<tr>
<th>諸手当</th>
<td></td>
</tr>
<tr>
<th>待遇・福利厚生</th>
<td>■交通費支給（3万円迄／月）<br>■社会保険完備<br>■管理職手当<br>■四半期ごとのインセンティブ制度<br>■OJT・各種セミナーへの参加支援<br><br>社内イベントも盛りだくさんです！<br><br>◇社員旅行<br>◇クリエイティブチームの勉強会<br>◇女子会<br>◇部活動<br>◇納会<br>◇BBQ</td>
</tr>
<tr>
<th>選考プロセス</th>
<td>▼MOREWORKS応募フォームによる書類選考<br>お送りいただいた応募データをもとに書類選考いたします。<br>選考後、追って面接日のご連絡を差し上げます。<br><br>▼ 1次面接<br>採用担当者による面接を行います。<br>※面接日は相談に応じます。<br><br>▼ 2次面接<br>役員・配属先担当者による面接を行います。<br>★1次で内定する場合もあります。<br><br>▼ 内定<br>入社日は相談に応じます。在職中の方も気兼ねなくご応募ください！</td>
</tr>
<tr>
<th>備考</th>
<td>裁量労働制を採用</td>
</tr>
<tr>
<th>タグ</th>
<td class="tagsContainer">
<ul class="tags">
<li>転勤なし</li>
<li>学歴不問</li>
<li>服装自由</li>
<li>自社サービスあり</li>
<li>オフィスがきれい</li>
</ul>
</td>
</tr>
</table>
<!-- /detail -->
</div><!-- /articleBody -->
<footer>
<p><a href="http://www.metaphase.co.jp/recruit/" target="_blank" class="buttonA01"><span class="extarnal">求人詳細を見る</span></a></p>
</footer>
</article>
</section>
</div><!-- /mainContents -->


<div id="sideContents" class="mediaPC">

<section>
<h2 class="headingA02">広告</h2>
<div><img src="/images/dummy/dummy_adsense_01.png" width="233" height="733" alt=""/></div>
</section>

<aside class="ad">
<div><img src="/images/common/dummy_ad_234-60.gif" width="234" height="60" alt=""/></div>
<div><img src="/images/common/dummy_ad_120-600.gif" width="120" height="600" alt=""/></div>
</aside>
</div>

</div><!-- / contentsInner -->