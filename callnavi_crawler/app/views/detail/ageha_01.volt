<div class="contentsInner">
<div id="mainContents">
<section>
<article class="jobDetailA01">
<header>
<h3><a href="#" target="_blank">クリエイティブディレクター</a></h3>
<p class="date">更新日：0000年00月00日</p>
</header>
<div class="articleBody">
<div class="featureA01 js-slider">
<a href="#">
<div class="slidesContainer">
<div class="crossfade">
<ul class="slides">
<li><img src="/public/images/job_detail/mod_no-img_01.png" width="680" alt=""></li>
<li><img src="/public/images/job_detail/mod_no-img_01.png" width="680" alt=""></li>
<li><img src="/public/images/job_detail/mod_no-img_01.png" width="680" alt=""></li>
</ul>
</div>
</div>
</a>
<div class="slideControl">
<ul class="select">
<li><a href="javascript:;">1</a></li>
<li><a href="javascript:;">2</a></li>
<li><a href="javascript:;">3</a></li>
</ul>
</div>
</div><!-- /featureA01 -->
<p class="company">株式会社揚羽</p>
<div class="introduction">
<p>パンフレット制作、Webサイト構築のクリエイティブディレクション及びプロジェクト管理</p>
<p>営業担当よりコンペ依頼が来ますので、ヒアリングから、プランニング、企画書作成、プレゼンテーションまで担当していただきます。また、受注後は、クリエイティブディレクション、プロジェクト管理もお任せします。</p>
<p>※取引先企業は、大手企業やメガベンチャーが多いです。<br>※取引の9割は、直取引です。最近は、代理店さんからの依頼も増えていますが、直取引も伸ばしていきたいと考えています</p>
</div>
<table class="tableA01 description">
<col style="width:160px;">
<col>
<tr>
<th>応募資格</th>
<td>パンフレット、Webサイトのクリエイティブディレクション経験２年以上<br>その他、下記経験があれば尚可<br>●企画立案、プレゼンテーションなど上流工程の経験<br>●取材撮影のディレクション経験<br>●コピーライティング経験<br>●グラフィックデザイン経験<br>●Webデザイン経験、コーディング経験</td>
</tr>
<tr>
<th>給与</th>
<td>月給240,000円以上（職務手当、みなし残業手当含む）　給与例/30歳　30万円（一律手当含む）</td>
</tr>
<tr>
<th>諸手当</th>
<td>通勤交通費全額支給</td>
</tr>
<tr>
<th>福利厚生</th>
<td>日本経済新聞購読手当、映画鑑賞・芸術鑑賞手当、早起きは三文の得手当、インセンティブ表彰制度有（目標達成、社内クリエイティブコンテストなど）、ウォーターサーバー有、iPhone貸与、社内旅行（不定期）</td>
</tr>
<tr>
<th>昇給</th>
<td>年2回（6ヵ月毎に査定の上、昇給・降給あり）</td>
</tr>
<tr>
<th>賞与</th>
<td>業績による決算賞与有（10月）</td>
</tr>
<tr>
<th>勤務地</th>
<td>銀座</td>
</tr>
<tr>
<th>交通</th>
<td>●銀座駅徒歩2分　●有楽町駅徒歩8分　●新橋駅徒歩8分</td>
</tr>
<tr>
<th>勤務時間</th>
<td>裁量労働制（１日8時間、労働基準時間10：00～19：00）　※毎週月曜は会議の為、9：00出社</td>
</tr>
<tr>
<th>休日休暇</th>
<td>完全週休2日制（土・日） 祝日、GW、夏季、年末年始、慶弔、有給</td>
</tr>
<tr>
<th>保険</th>
<td>社会保険完備（労災保険、雇用保険、健康保険、厚生年金）</td>
</tr>
<tr>
<th>教育・研修制度</th>
<td>新人研修（社内研修、公開研修、ＯＪＴ）、各種ビジネス研修、各種制作技術研修</td>
</tr>
</table>
<!-- /detail -->
</div><!-- /articleBody -->
<footer>
<p><a href="http://www.ageha.tv/recruit/information/" target="_blank" class="buttonA01"><span class="extarnal">求人詳細を見る</span></a></p>
</footer>
</article>
</section>
</div><!-- /mainContents -->


<div id="sideContents" class="mediaPC">

<section>
<h2 class="headingA02">広告</h2>
<div><img src="/public/images/dummy/dummy_adsense_01.png" width="233" height="733" alt=""/></div>
</section>

<aside class="ad">
<div><img src="/public/images/common/dummy_ad_234-60.gif" width="234" height="60" alt=""/></div>
<div><img src="/public/images/common/dummy_ad_120-600.gif" width="120" height="600" alt=""/></div>
</aside>
</div>

</div>