<div class="contentsInner">
<div id="mainContents">
<section>
<article class="jobDetailA01">
<header>
<h3><a href="#" target="_blank">Webアプリケーションエンジニア</a></h3>
<p class="date">更新日：0000年00月00日</p>
</header>
<div class="articleBody">
<div class="featureA01 js-slider">
<a href="#">
<div class="slidesContainer">
<div class="crossfade">
<ul class="slides">
<li><img src="/public/images/job_detail/borders_img_01.png" width="680" alt=""></li>
<li><img src="/public/images/job_detail/borders_img_02.png" width="680" alt=""></li>
<li><img src="/public/images/job_detail/borders_img_03.png" width="680" alt=""></li>
</ul>
</div>
</div>
</a>
<div class="slideControl">
<ul class="select">
<li><a href="javascript:;">1</a></li>
<li><a href="javascript:;">2</a></li>
<li><a href="javascript:;">3</a></li>
</ul>
</div>
</div><!-- /featureA01 -->
<p class="company">株式会社 ボーダーズ</p>
<p class="summary">【Webアプリケーションエンジニア】自社開発◎自社オンラインリサーチサービスの企画・設計・開発を手掛ける！LAMP環境経験者歓迎！</p>
<div class="introduction">
<p>オンライン市場調査システム「POST（Professional Online Survey Tool）」の開発をはじめ、受託マーケティングリサーチや完全セルフ型ネットリサーチＡＳＰの開発など、マーケティングリサーチ関連の様々な事業を展開する株式会社ボーダーズ。</p>
<p>現在最も力を入れる「POST」は、国内唯一の市場調査会社向けのネットリサーチソリューションプラットフォームである。</p>
<p>新しいサービスで市場を開拓していきたい、というチャレンジ精神溢れる人材の参画を待ち望んでいる。</p>
</div>
<table class="tableA01 description">
<col style="width:160px;">
<col>
<tr>
<th>仕事内容</th>
<td>【仕事内容】<br>当社主力アンケートシステム「POST」他、アンケートモニタ管理システム、集計システム等、当社のビジネスを担うシステム群を開発・メンテナンスしていただく業務です。<br><br>・お客様のご要望に応じて、または当社のシステム戦略に従って既存システムの改修や新規システムの開発を行う。<br><br>・常に新しい調査手法、開発手法を目指し、複雑な調査、新しい調査手法を実装する。<br><br>IT系ベンキャー企業のビジネスのスピードを体験し、実践力を身につけることができる業務です。リサーチITベンチャーである当社の成長のキーパーソンとなっていただくポストです。<br><br>【仕事の魅力・やりがい】<br>■リサーチITベンチャーである当社の成長の主力となっていただくポストです。<br>■全て自社内の開発になりますので、腰を据えて開発に専念することが可能です。<br>■自社サービスに携わるので、顧客の声やご自身のアイディアを反映することが可能です。<br>■企画から携わるので、一つひとつを作り上げる達成感を感じることができます。<br>■一人ひとりの裁量は大きいです。やりたいと言ったことを必ずやらせてもらえる環境が整っています。<br><br>【社内の雰囲気】<br>非常にフラットな社風です。年齢や社歴問わず、経営陣に対して直接提案をすることも可能です。採用されれば、即実行ということもあります。実際にも、新入社員が提案したシステムが採用され、売れ筋の商品になっています。</td>
</tr>
<tr>
<th>応募資格</th>
<td>【必須要件】<br>・Webアプリケーション、特にLinux/Apache/PostgreSQL/PHP環境での企画・開発経験をお持ちの方。<br><br>・常に技術トレンドをキャッチし、実践していける方<br><br>【歓迎要件】<br>■統計解析（検定、多変量解析、データマイニング等）の知識<br><br>【求める人物像】<br>■常に技術トレンドをキャッチし、実践していける方<br>■責任感を持って行動できる方<br>■自らの担当以外にも興味を持って挑戦できる方<br>■自主的に動くことができる方<br><br>【学歴】不問 </td>
</tr>
<tr>
<th>雇用区分</th>
<td>正社員</td>
</tr>
<tr>
<th>想定年収<br>（給与詳細）</th>
<td>～600万円<br>※職務経験や能力を考慮します。<br><br>【給与改定】年1回<br>【賞与】年4回<br>【給与例】<br>29歳（経験4年）：年収約450万円<br>（内訳：420万＋ボーナスで450万円）</td>
</tr>
<tr>
<th>勤務地</th>
<td>【勤務地詳細】<br>東京都新宿区新宿1-28-11　KOSUGIビル7階<br><br>【アクセス】<br>東京メトロ丸ノ内線「新宿御苑前」駅より徒歩約5分</td>
</tr>
<tr>
<th>採用人数</th>
<td>5名 </td>
</tr>
<tr>
<th>勤務時間</th>
<td>10:00～19:00</td> 
</tr>
<tr>
<th>待遇・福利厚生</th>
<td>【保険】<br>■各種保険完備<br><br>【諸手当】<br>■交通費全額支給<br>■都市生活手当<br><br>【福利厚生】<br><br>■学習補助<br>■資格取得支援<br>■セミナー・研修補助<br>■書籍購入支援<br>■永年勤続者（3年・5年・10年）対象の休暇・賞与付与 <br>■退職金制度<br>■MVP表彰（年1回）<br>■イベント支援（会社負担・例：BBQ大会、合宿etc）<br>■シャッフルランチ（部門を超えて月に１度様々なメンバーでランチに行きます。勿論、会社負担です）<br>■懇親会費用支援（チームの一体感や慰労を目的とした懇親会を会社負担で開催）<br>■パワードリンク（毎週水曜日の19時以降、社内にてビール解禁）<br>■部活支援制度</td>
</tr>
<tr>
<th>休日/休暇</th>
<td>完全週休2日制（土・日）、祝日、有給休暇、特別休暇、年末年始休暇ほか</td>
</tr>
<tr>
<th>選考プロセス</th>
<td>書類選考<br>　　↓<br>面接（数回）<br>　　↓<br>　 内定</td>
</tr>
</table>
<!-- /detail -->
</div><!-- /articleBody -->
<footer>
<p><a href="https://borders.jp/recruit/" target="_blank" class="buttonA01"><span class="extarnal">求人詳細を見る</span></a></p>
</footer>
</article>
</section>
</div><!-- /mainContents -->


<div id="sideContents" class="mediaPC">

<section>
<h2 class="headingA02">広告</h2>
<div><img src="/public/images/dummy/dummy_adsense_01.png" width="233" height="733" alt=""/></div>
</section>

<aside class="ad">
<div><img src="/public/images/common/dummy_ad_234-60.gif" width="234" height="60" alt=""/></div>
<div><img src="/public/images/common/dummy_ad_120-600.gif" width="120" height="600" alt=""/></div>
</aside>
</div>

</div>