<div class="contentsInner">
<div id="mainContents">
<section>
<article class="jobDetailA01">
<header>
<h3><a href="#" target="_blank">自社Webサービスのディレクター </a></h3>
<p class="date">更新日：0000年00月00日</p>
</header>
<div class="articleBody">
<div class="featureA01 js-slider">
<a href="#">
<div class="slidesContainer">
<div class="crossfade">
<ul class="slides">
<li><img src="/public/images/job_detail/vogaro_img_01.png" width="680" alt=""></li>
<li><img src="/public/images/job_detail/vogaro_img_02.png" width="680" alt=""></li>
<li><img src="/public/images/job_detail/vogaro_img_03.png" width="680" alt=""></li>
</ul>
</div>
</div>
</a>
<div class="slideControl">
<ul class="select">
<li><a href="javascript:;">1</a></li>
<li><a href="javascript:;">2</a></li>
<li><a href="javascript:;">3</a></li>
</ul>
</div>
</div><!-- /featureA01 -->
<p class="company">Vogaro 株式会社</p>
<p class="summary">Webサービスの戦略構築から開発、企画営業まで。一貫して担当できるチャンスがあります</p>
<div class="introduction">
<p>Design×Technology×Planning=Vogaro　高いデザイン力を軸に、クリエイティブな”流行を生み出し続ける“Webプロダクション</p>
<p>■Vogaroとは</p>
<p>大手企業を中心に、Web戦略からデザイン／開発までを手掛けている当社。<br>高いデザイン力と高度な技術によるクリエイティブワークが認められ 数々の賞を受賞してきました。</p>
<p>■お任せしたいのは最先端のクリエイティブ・ソリューションの事業展開</p>
<p>2013年10月には、博報堂プロダクツと共同開発で、020アプリのプラットフォーム「CROSSH」をリリース。また、サイバーエージェントとの共同開発では、スマートフォンに特化した多機能EFOパッケージ「スマートUPフォーム」なども開発しました。<br>2008年にリリースしたWebソリューションツール「LACNE」は1000サイト以上の導入実績を誇るなど、ユーザー視点にこだわり、操作面や使いやすさを徹底的に追求したモノづくりが評価されています。</p>
<p>これらのソリューションサービスの専任担当として、次なる展開をつくりだしていただける方を求めています。</p>
<p>■事業展開の戦略構築から、 実際の案件獲得まで。サービスを一貫して担当していただきます。</p>
<p>ターゲットとすべきマーケットの設定や、販売戦略として大手広告代理店とのパートナーシップの構築といった戦略から、実際にクライアントへの提案・成約にいたるまで、サービスを一貫して担当していただけます。もちろん、サービスそのものの開発やバージョンアップも、社内の一流と呼ばれるクリエイターたちが強力にサポートします。</p>
<p>当社のWebサービス事業をあなたの力で飛躍させてください。</p>
</div>
<table class="tableA01 description">
<col style="width:160px;">
<col>
<tr>
<th>募集背景</th>
<td>Webサービス事業の更なる充実を図るため、これまで兼任体制を脱し、初の専任担当としての募集となります。<br>自由度の高い環境に身をおき、ご自身の強みを最大限に活かしてください。<br>仕事内容	自社開発および大手広告代理店との共同開発により誕生したWebサービスのマーケティング・販売戦略の構築、提案営業と市場への情報発信をお任せします。<br><br>既に博報堂プロダクツやサイバーエージェントとの共同開発によるWebソリューションツールもリリース。これらを含む自社Webサービスの全般的な販売戦略の立案を担っていただきます。<br><br>〈具体的には〉<br>◎クライアントへのサービス提案<br>◎自社商品のリリースに関する統括<br>◎バージョンアップの際の進捗管理<br>◎新商品を対象にしたセミナーやイベントの企画・運営<br>◎広報企画</td>
</tr>
<tr>
<th>応募資格</th>
<td>以下のいずれかないし複数に当てはまる方を歓迎します<br><br>◆顧客の視点に立ったサービスの発信方法を考えられる方<br>（必ずしもIT業界経験がなくても構いません）<br><br>◆媒体や代理店の営業経験などがある方 <br><br>◆Webディレクターとしての経験があり、今後仕事の幅を広げていきたいと考えている方<br><br>◆目標達成のために、試行錯誤できる方<br><br>〈求める人物像〉 <br>◎社内外で積極的にコミュニケーションを取れる方 <br>◎ビジネスを生み出す意欲のある方 <br>◎世の中の新しいことやトレンドに敏感な方 <br>◎論理的思考のある方 <br>◎リリースなどドキュメント作成ができる方</td>
<tr>
<td>雇用区分</td>
<td>正社員・契約社員</td>
</tr>
<tr>
<th>想定年収<br>（給与詳細）</th>
<td>500万円～800万円<br>スキル・経験等に応じて相談。四半期毎に給与の査定がありますので、成果に応じて随時昇給有り<br><br>【 研修・試用期間 】<br>研修・試用期間2ヶ月 (本給与の約90％ 経験・スキルにより応相談。前職給考慮します)</td>
</tr>
<tr>
<th>勤務地</th>
<td>【勤務地詳細】<br>東京支社：東京都渋谷区恵比寿西2-20-17 代官山サンライトビル3F <br><br>【アクセス】<br>最寄駅<br>【東京支社】東急東横線　代官山駅</td>
</tr>
<tr>
<th>待遇・福利厚生</th>
<td>交通費支給（上限あり）<br>業務成果により昇給有り、賞与年2回、各種社会保険完備<br><br>【質の高い教育環境】 <br>必要に応じて、随時、研修・教育を行います。 <br>また、隔週開催されるマーケティング・デザイン・ソリューション勉強会、毎月開催される案件発表会など、様々な視点で質の高い学びを得る機会がたくさんあります。不定期ではありますが、業界の著名人も招待しワークショップや交流をする事あります。</td>
</tr>
<tr>
<th>休日/休暇</th>
<td>週休2日(土日)、祝日、夏季・年末年始休暇、慶弔休暇 </td>
</tr>
<tr>
<th>選考プロセス</th>
<td>◎ ご応募から内定までは、2週間程度とお考えください。 ◎<br><br>【STEP1】書類選考 <br>▼ <br>【STEP2】配属先担当者との面接 <br>▼ <br>【STEP3】内定 <br><br>※選考においてはなるべくスピーディーに行います。<br> ※面接日等は考慮しますので、ご相談ください。</td>
</tr>
</table>
<!-- /detail -->
</div><!-- /articleBody -->
<footer>
<p><a href="http://www.vogaro.co.jp/recruit/" target="_blank" class="buttonA01"><span class="extarnal">求人詳細を見る</span></a></p>
</footer>
</article>
</section>
</div><!-- /mainContents -->


<div id="sideContents" class="mediaPC">

<section>
<h2 class="headingA02">広告</h2>
<div><img src="/public/images/dummy/dummy_adsense_01.png" width="233" height="733" alt=""/></div>
</section>

<aside class="ad">
<div><img src="/public/images/common/dummy_ad_234-60.gif" width="234" height="60" alt=""/></div>
<div><img src="/public/images/common/dummy_ad_120-600.gif" width="120" height="600" alt=""/></div>
</aside>
</div>

</div><!-- / contentsInner -->