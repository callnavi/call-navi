<div id="contents">
    <div class="contentsInner">
        <div id="contentsContainer">
            <div id="mainContents">
                <!-- InstanceBeginEditable name="mainContents" -->
                <section class="mediaPC">
                    <p><img src="/public/images/index/index_img_01.png" width="720" height="156" alt="全国のコールセンター求人を一括検索。簡単検索であなたにピッタリのコールセンターがきっと見つかる！"/></p>
                    <div class="searchMapA01">
                        <form class="searchArea">
                            <h2>《簡単求人一括検索》</h2>
                            <p><input type="text" placeholder="キーワード（時給・服装自由など）"><br>例：給与30万円以上、週3日以上勤務、駅近、時短勤務可</p>
                            <p><input type="text" placeholder="勤務地（都道府県名または市区町村名など）"><br>例：東京都渋谷区、池袋駅、大阪市北区</p>
                            <p class="search"><button type="submit">検索</button></p>
                        </form><!-- / searchArea -->
                        <ul class="area">
                            <li class="area01"><a href="#">北海道</a></li>
                            <li class="area02"><a href="#">東北</a></li>
                            <li class="area03"><a href="#">関東</a></li>
                            <li class="area04"><a href="#">中部</a></li>
                            <li class="area05"><a href="#">近畿</a></li>
                            <li class="area06"><a href="#">中国</a></li>
                            <li class="area07"><a href="#">四国</a></li>
                            <li class="area08"><a href="#">九州</a></li>
                            <li class="area09"><a href="#">沖縄</a></li>
                        </ul>
                    </div><!-- / searchMapA01 -->
                </section>

                <section>
                    <article class="pickupA01">
                        <a href="#">
                            <h2><img src="/public/images/common/pickup-a01_ttl_01.png" width="" height="" alt="Pikcup求人" class="mediaPC"/></h2>
                            <h3>★新メンバー募集★≪週2,3～&amp;1日3h～≫コールセ…</h3>
                            <div class="contentsInner">
                                <p class="image"><img src="/templates/images/dummy_img_02.png" width="298" height="214" alt=""></p>
                                <p class="company">株式会社Wiz（ワイズ）「新大塚コールセンター」</p>
                                <p class="summary mediaPC">★キレイなビルのキレイなオフィス♪★<br><br>大手通信回線サービス(ブロードバンド)を利用中のお客様にお電話し,<br>プランや料金などの簡単な確認(ヒヤリング)をするのが主なお仕事!<br>私達がご案内するサービスの方がメリットがある場合があります。<br>使って頂けるかのヒヤリングもお願いします♪<br><br>必ず近くに社員もいるのでサポート体制も万全!<br>分からない事はすぐに質問できる環境です。<br><br>★商材はCMでも流れている誰でも知っているサービスだから内容も覚えやすくて安心です。<br>[コールセンター経験がない][サービスの知識がない]という方も問題なし!<br>商品マニュアルや,丁寧な研修もあり♪</p>
                            </div><!-- / contentsInner -->
                            <div class="contentsInner">
                                <table>
                                    <tr>
                                        <th>給　与</th>
                                        <td><div>時給 1,400円～2,000円<br>＋成績により月次「インセンティブ」あり!!(最大インセンティブ：30万円/月) / ※昇格・昇給(査定毎月)あり / ※研修期間は1000円～、研修終了後は1400円～2000円<br>交通費：一部支給<br>◆上限2万円（1日上限往復1000円） ※自転車・徒歩で通勤される方：交通費の代わりに、手当1日200円支給♪</div></td>
                                    </tr>
                                    <tr>
                                        <th>勤務地</th>
                                        <td><div>東京都豊島区南大塚2-25-15　South新大塚ビル （新大塚オフィス） ≪同時募集!!≫ 【高田馬場コールセンター】 ◆高田馬場2-13-2 Primegate4F ★新スタッフ30名大募集です♪★ 1月にオープンしたばかりのキレイで広いオフィスです♪	</div></td>
                                    </tr>
                                </table>
                                <ul class="tags">
                                    <li>高校生歓迎</li>
                                    <li>学生歓迎</li>
                                    <li>フリーター歓迎</li>
                                    <li>未経験・初心者歓迎</li>
                                    <li>交通費支給</li>
                                    <li>シフト自由・選べる</li>
                                    <li>高収入・高額・高給</li>
                                </ul>
                            </div><!-- / contentsInner -->
                            <p class="source">スポンサー: バイトル- 8時間前</p>
                        </a>
                    </article><!-- / pickupA01 -->
                </section>
                <!-- InstanceEndEditable -->
            </div><!-- / mainContents -->

            <div id="subContents">
                <nav id="sideNavigation" class="mediaPC">
                    <ul>
                        <li><a href="#">HOME</a></li>
                        <li><a href="#">コールセンターの<br>仕事とは？</a></li>
                        <li><a href="#">おすすめ求人</a></li>
                    </ul>
                </nav>
                <section>
                    <h2 class="headingA01 topics">トピックス</h2>
                    <ul class="bnrListA01 alignC">
                        <li><a href="#" class="imgHover"><img src="/public/images/common/bnr_about-callnavi_01.png" width="200" height="170" alt="" class="mediaPC"/><img src="/public/images/common/bnr_about-callnavi_01.png" width="136" height="116" alt="" class="mediaSP"/></a></li>
                        <li><a href="#" class="imgHover"><img src="/public/images/common/bnr_melit_01.png" width="200" height="170" alt="" class="mediaPC"/><img src="/public/images/common/bnr_melit_01.png" width="136" height="116" alt="" class="mediaSP"/></a></li>
                        <li><a href="#" class="imgHover"><img src="/public/images/common/bnr_report_01.png" width="200" height="170" alt="" class="mediaPC"/><img src="/public/images/common/bnr_report_01.png" width="136" height="116" alt="" class="mediaSP"/></a></li>
                        <li><a href="#" class="imgHover"><img src="/public/images/common/bnr_help_01.png" width="200" height="170" alt="" class="mediaPC"/><img src="/public/images/common/bnr_help_01.png" width="136" height="116" alt="" class="mediaSP"/></a></li>
                    </ul>
                </section>
            </div><!-- / subContents -->
        </div><!-- / contentsContainer -->

        <div id="asideContents" class="mediaPC">
            <section class="rectangleBnr">
                <p><img src="images/dummy_bnr_01.png" width="200" height="200" alt=""></p>
            </section>

            <section>
                <h2><img src="/public/images/common/ttl_pickup_01.gif" width="200" height="40" alt="Pickup求人"/></h2>
                <article class="pickupB01">
                    <a href="#">
                        <p class="image"><img src="/templates/images/dummy_img_01.png" width="160" height="120" alt=""></p>
                        <h3>≪髪型・服装FREE♪≫月約35万円は普通に稼げるコールセンター♪</h3>
                        <p class="company">株式会社Bestエフォート</p>
                        <p class="summary">★未経験OK!!★週3～/1日4h～勤務OK!!時給＋インセンティブ15万円以上が稼げる環境です♪ノルマはありません。≪フリーターさん、Wワーカーさんも活躍中!!働きやすい職場環境・福利厚生が自慢です!!</p>
                        <table>
                            <tr>
                                <th>給与</th>
                                <td><div>時給 1,500円～2,500円<br>交通費：一部支給<br>月1万円まで支給</div></td>
                            </tr>
                            <tr>
                                <th>勤務地</th>
                                <td><div>東京都新宿区高田馬場2-13-12 Primegate4F</div></td>
                            </tr>
                        </table>
                        <ul class="tags">
                            <li>フリーター歓迎</li>
                            <li>主婦（ママ）・主夫歓迎</li>
                            <li>未経験・初心者歓迎</li>
                            <li>交通費支給</li>
                            <li>高収入・高額・高級</li>
                        </ul>
                    </a>
                </article><!-- / pickupB02 -->
            </section>

            <section>
                <h2><img src="/public/images/common/ttl_pickup_01.gif" width="200" height="40" alt="Pickup求人"/></h2>
                <article class="pickupB01">
                    <a href="#">
                        <p class="image"><img src="/templates/images/dummy_img_01.png" width="160" height="120" alt=""></p>
                        <h3>≪髪型・服装FREE♪≫月約35万円は普通に稼げるコールセンター♪</h3>
                        <p class="company">株式会社Bestエフォート</p>
                        <p class="summary">★未経験OK!!★週3～/1日4h～勤務OK!!時給＋インセンティブ15万円以上が稼げる環境です♪ノルマはありません。≪フリーターさん、Wワーカーさんも活躍中!!働きやすい職場環境・福利厚生が自慢です!!</p>
                        <table>
                            <tr>
                                <th>給与</th>
                                <td><div>時給 1,500円～2,500円<br>交通費：一部支給<br>月1万円まで支給</div></td>
                            </tr>
                            <tr>
                                <th>勤務地</th>
                                <td><div>東京都新宿区高田馬場2-13-12 Primegate4F</div></td>
                            </tr>
                        </table>
                        <ul class="tags">
                            <li>フリーター歓迎</li>
                            <li>主婦（ママ）・主夫歓迎</li>
                            <li>未経験・初心者歓迎</li>
                            <li>交通費支給</li>
                            <li>高収入・高額・高級</li>
                        </ul>
                    </a>
                </article><!-- / pickupB02 -->
            </section>

            <section class="mediaPC">
                <h2 class="headingA01 recommend marginOff">おすすめ求人企業</h2>
                <ul class="bnrListB01">
                    <li><a href="/http://besteffort-p.com/" target="_blank" class="imgHover"><img src="/public/images/common/logo_besteffort_01.png" width="200" height="74" alt=""/><span>あいうえおかきくけこさしすせそ</span></a></li>
                    <li><a href="/http://012grp.co.jp/" target="_blank" class="imgHover"><img src="/public/images/common/logo_wiz_01.png" width="200" height="74" alt=""/><span>あいうえおかきくけこさしすせそ</span></a></li>
                    <li><a href="/http://012grp.co.jp/" target="_blank" class="imgHover"><img src="/public/images/common/logo_wiz_01.png" width="200" height="74" alt=""/><span>あいうえおかきくけこさしすせそ</span></a></li>
                    <li><a href="/http://012grp.co.jp/" target="_blank" class="imgHover"><img src="/public/images/common/logo_wiz_01.png" width="200" height="74" alt=""/><span>あいうえおかきくけこさしすせそ</span></a></li>
                    <li><a href="/http://012grp.co.jp/" target="_blank" class="imgHover"><img src="/public/images/common/logo_wiz_01.png" width="200" height="74" alt=""/><span>あいうえおかきくけこさしすせそ</span></a></li>
                </ul>
            </section>
        </div><!-- / asideContents -->

        <div id="pageTop"><a href="#globalHeader" class="smoothScroll">globalHeader</a></div>
    </div><!-- / contentsInner -->
</div><!-- / contents -->