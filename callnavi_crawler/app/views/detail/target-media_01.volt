<div class="contentsInner">
<div id="mainContents">
<section>
<article class="jobDetailA01">
<header>
<h3><a href="#" target="_blank">企画・営業（アカウントプランナー）</a></h3>
<p class="date">更新日：0000年00月00日</p>
</header>
<div class="articleBody">
<div class="featureA01 js-slider">
<a href="#">
<div class="slidesContainer">
<div class="crossfade">
<ul class="slides">
<li><img src="/public/images/job_detail/mod_no-img_01.png" width="680" alt=""></li>
<li><img src="/public/images/job_detail/mod_no-img_01.png" width="680" alt=""></li>
<li><img src="/public/images/job_detail/mod_no-img_01.png" width="680" alt=""></li>
</ul>
</div>
</div>
</a>
<div class="slideControl">
<ul class="select">
<li><a href="javascript:;">1</a></li>
<li><a href="javascript:;">2</a></li>
<li><a href="javascript:;">3</a></li>
</ul>
</div>
</div><!-- /featureA01 -->
<p class="company">ターゲットメディア株式会社</p>
<p class="summary">クライアントの新規顧客獲得におけるニーズ・課題を引き出し、<br>最適な手法を提案すること。</p>
<div class="introduction">
<p>アカウントプランナーのミッションは２つあります。</p>
<p>一つは、利益の源泉となる顧客を開拓すること。<br>一つは、顧客のニーズをつかみ社内でシェアすること。</p>
<p>ターゲットメディアが成長し続けるための大切な「仕入れ」を担う仕事です。<br>どんなに開発力をもつメーカーも材料がなければ商品はつくれません。</p>
<p>そんなビジネスの「起点」を担う仕事です。</p>
<p>お客様となるのは、広告業界、マーケティング業界、IT業界の企業の経営者や宣伝担当者、 <br>マーケティング担当者になります。<br>新規開拓営業と自身が開拓した既存顧客のフォロー営業と両方を担当してもらいます。</p>
<p>クライアントの業種内容や現在抱えている問題を理解した上で<br>最適な広告企画を立案し、実行～結果報告までを行います。</p>
</div>
<table class="tableA01 description">
<col style="width:160px;">
<col>
<tr>
<th>雇用形態</th>
<td>正社員</td>
</tr>
<tr>
<th>勤務地</th>
<td>〒160-0004　東京都新宿区四谷4-1 細井ビル2F</td>
</tr>
<tr>
<th>給与</th>
<td>新卒採用の方:210,000円（2012年度実績）<br>中途採用の方:経験・能力を考慮の上、当社規定により決定致します。</td>
</tr>
<tr>
<th>勤務時間</th>
<td>9時30分～18時30分</td>
</tr>
<tr>
<th>休日/休暇</th>
<td>週休2日制（土曜・日曜）、祝日、夏期休暇、年末年始休暇<br>※年に数日、研修のため土日出社の可能性があります。（年間休日120日以上）</td>
</tr>
<tr>
<th>福利厚生</th>
<td>健康保険（関東ITソフトウェア健康保険組合加入）、雇用保険、労災保険、厚生年金保険</td>
</tr>
<tr>
<th>選考方法</th>
<td>書類選考、面接(2～3回)</td>
</tr>
<tr>
<th>経験/スキル<br>（中途採用の方）</th>
<td>【必須スキル】<br>■法人営業経験<br>■PCスキル（パワーポイント、ワード、エクセル）<br><br>【歓迎スキル】<br>■IT業界、Web業界、広告・マーケティング業界の営業経験者歓迎</td>
</tr>
</table>
<!-- /detail -->
</div><!-- /articleBody -->
<footer>
<p><a href="http://www.tmedia.co.jp/recruit/" target="_blank" class="buttonA01"><span class="extarnal">求人詳細を見る</span></a></p>
</footer>
</article>
</section>
</div><!-- /mainContents -->


<div id="sideContents" class="mediaPC">

<section>
<h2 class="headingA02">広告</h2>
<div><img src="/public/images/dummy/dummy_adsense_01.png" width="233" height="733" alt=""/></div>
</section>

<aside class="ad">
<div><img src="/public/images/common/dummy_ad_234-60.gif" width="234" height="60" alt=""/></div>
<div><img src="/public/images/common/dummy_ad_120-600.gif" width="120" height="600" alt=""/></div>
</aside>
</div>

</div>