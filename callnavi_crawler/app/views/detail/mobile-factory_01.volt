<div class="contentsInner">
<div id="mainContents">
<section>
<article class="jobDetailA01">
<header>
<h3><a href="#" target="_blank">iOSエンジニア</a></h3>
<p class="date">更新日：0000年00月00日</p>
</header>
<div class="articleBody">
<div class="featureA01 js-slider">
<a href="#">
<div class="slidesContainer">
<div class="crossfade">
<ul class="slides">
<li><img src="/public/images/job_detail/borders_img_01.png" width="680" alt=""></li>
<li><img src="/public/images/job_detail/borders_img_02.png" width="680" alt=""></li>
<li><img src="/public/images/job_detail/borders_img_03.png" width="680" alt=""></li>
</ul>
</div>
</div>
</a>
<div class="slideControl">
<ul class="select">
<li><a href="javascript:;">1</a></li>
<li><a href="javascript:;">2</a></li>
<li><a href="javascript:;">3</a></li>
</ul>
</div>
</div><!-- /featureA01 -->
<p class="company">株式会社 モバイルファクトリー</p>
<p class="summary">iOSエンジニア求む！！！</p>
<div class="introduction">
<p>モバイルファクトリーは2001年10月に設立されて以来、様々なモバイルサービスを提供してきたIT系ベンチャー企業です。</p>
<p>ソーシャルゲーム事業では複数カテゴリのソーシャルゲームを展開。<br>位置情報機能を使って全ユーザーで日本全国の駅を奪い合う「駅奪取」、メイド喫茶をテーマにした男性向け恋愛シュミレーションゲーム「おかえりなさいご主人様!!」、戦略性の高いバトルが楽しめるシナリオRPG「漆黒のレガリア」など、オリジナルコンテンツを提供。</p>
<p>モバイルコンテンツ事業ではゲーム以外のエンターテイメントコンテンツを提供中。スマートフォンの普及・拡大に伴ってユーザー数も拡大中しています。</p>
<p>安定した事業収益の基盤を固めながら、ゲーム以外の新たなサービス展開に取り組んでいます。</p>
</div>
<table class="tableA01 description">
<col style="width:160px;">
<col>
<tr>
<th>配属部署</th>
<td>ソーシャルアプリ事業部</td>
</tr>
<tr>
<th>求人の特徴</th>
<td>
<ul class="tags">
<li>転勤なし</li>
<li>学歴不問</li>
<li>服装自由</li>
</ul>
</td>
</tr>
<tr>
<th>募集背景</th>
<td>ソーシャルアプリのヒットに伴ってエンジニアリソースを拡充するために、新たにエンジニアを募集します。<br>既存のタイトルのスマートフォン対応だけでなく、今後は新規タイトルの企画・リリースにも携わっていただきます。<br><br>スキル重視、経験は問いません。プログラミングが大好きなエンジニアを求めています！</td>
</tr>
<tr>
<th>仕事内容</th>
<td>既存タイトルのネイティブ対応、新規タイトルの企画・リリースに携わっていただきます。<br><br>開発環境・使用技術：<br>Linux/ iOS/ Android/ Apache/ MySQL/ Perl/ Java/ Objective-C/ HTML5/ Perlbal/Amon2/ lighttpd/ Gearman/ MogileFS/ memcached/ qmail/ rsync/ git/ screen/ ssh/ vim/ zsh/ redmine など</td>
</tr>
<tr>
<th>応募資格</th>
<td>【ＭＵＳＴ】<br>◆コンピュータ・プログラミングが大好きな方<br>◆iOSアプリケーションの開発経験<br>※iOS開発経験は必ずしも業務経験でなくても良いです。<br>　自作アプリの経験をお持ちの方もお待ちしております。<br><br>【あると望ましい経験・能力】<br>◆Webアプリケーションの知識<br>◆HTML5+CSS3+JavaScriptの知識<br>◆Perlを習得している<br>◆複数のプログラミング言語を習得している<br>◆LAMP環境での業務経験<br>◆ゲーム開発経験</td>
</tr>
<tr>
<th>この仕事で得られるもの</th>
<td>「エンジニアに最大限の能力を発揮して欲しい。そして“成長”を実感して欲しい」<br>私たちは本気で思っています。<br>弊社ではエンジニアたちが好奇心を持ち、楽しみ、探究し続けなければ<br>新しいものは生まれないと考えています。<br><br>そのための環境を作ることは私たちの仕事の一つ。<br><br>OJTの他、業務時間内に勉強会を行うなど、当社には充実した教育制度があります。<br><br>技術者同士、各々が書いたコードを見せてレビューし合ったり、<br>試してみた新しい技術を教え合あったり、インフラまわりの情報共有を行うことで、<br>高い技術に触れ、技術者としての成長スピードは本人の意志次第で飛躍的に高まります。</td>
</tr>
<tr>
<th>雇用区分</th>
<td>正社員</td>
</tr>
<tr>
<th>想定年収<br>（給与詳細）</th>
<td>450万円～700万円<br>　※経験、能力に基づき決定いたします<br>■昇給：年2回（2月、8月）※半年毎の目標管理制度による給与見直し<br>■賞与：年2回（2月、8月）※会社の業績及び個人の成果により変動</td>
</tr>
<tr>
<th>勤務地</th>
<td>【勤務地詳細】<br>■東京都千代田区四番町6番 東急番町ビル <br>※10月より五反田に移転いたします。 <br><br>【アクセス】<br>■JR中央線・総武線、東京メトロ有楽町線・南北線、都営新宿線 『市ケ谷駅』 徒歩3分<br>※移転後は五反田から徒歩5分程度</td>
</tr>
<tr>
<th>採用人数</th>
<td>1名</td>
</tr>
<tr>
<th>勤務時間</th>
<td>9:30～18:30</td>
</tr>
<tr>
<th>待遇・福利厚生</th>
<td>【基本制度】<br>■社会保険完備<br>■関東IT健保（全国の宿泊施設、スポーツ施設、レストランなどの優待利用）<br>■通勤交通費（5万円/月迄）<br>【独自制度】<br>■結婚お祝い金（Happy Celebration）<br>　：結婚すると、お祝い金5万円が支給<br>■出産お祝い金（Birth Celebration）<br>　：出産すると、以下条件にてお祝い金支給<br>　（1人目5万円／2人目10万円／3人目20万円／4人目40万円／5人目100万円）<br>■育児時時短制度（papa mama サポートタイム）<br>　：子供が小学校3年生まで、最大3時間(1日)、小学校4年生～6年生までは、最大2時間(1日)時短可能<br>■妊娠時時短制度（Given Life サポートタイム）<br>　：妊娠してから産休に入るまで、最大3時間(1日)の時間短縮が可能<br>■住宅補助制度（SWEET HOME制度）<br>　：会社から半径3kmを目安に会社指定地域の居住者に対し、月額2万円の家賃補助<br>■MF Dream Support Plan（年間最大15,000円分）<br>　：自己啓発にかかる費用や会社が指定する福利厚生サービスを利用した際の費用を補助  <br>休日/休暇	【休日】<br>■完全週休2日制（土・日）、祝日<br>【休暇】<br>■有給休暇（初年度最大10日間）、年末年始休暇<br>　産前産後休業、育児休業、介護休業、生理休暇、慶弔休暇<br>【特別休暇】<br>■子の看護休暇<br>■母性健康管理のための休暇<br>■介護休暇<br>■裁判員休暇<br>■カフェテリア休暇：年間6日間取得可能</td>
</tr>
<tr>
<th>選考プロセス</th>
<td>■適性試験　当社専用のエントリーシート（簡単なプログラミング課題を含む）をご提出いただきます。<br>　　▼<br>■個別面接　数回実施することもあります。<br>　　▼<br>■役員面接　<br>　　▼<br>■社長面接<br>　▼<br>■内定<br>※面接日・入社日はご相談下さい。</td>
</tr>
<tr>
<th>すべてはユーザーのために。だから遊ぶ。やらされ仕事は禁止です。</th>
<td>業務時間外に発想したことや勉強したことも業務に反映する。<br>このサイクルが技術力を向上させるのだと考えています。<br><br>遊ぶエンジニアほどよく育つ。<br>逆に言うと、24時間365日プログラミングに集中しているとも言えます。<br><br>モバイルファクトリーには、そんな強者エンジニアばかりです。<br>向上心の高いエンジニアにとっては魅力的な環境であると言えます。<br><br>また、モバイルファクトリーの社内では、コミュニケーションが活発に行われています。<br>人の心を動かして楽しませることをビジネスとしているからです。<br>どうやったら楽しんでもらえるのか、どうやったら心を動かせるのか。<br>そのヒントを探して会話をしています。そして、率先して楽しむことも忘れない。<br>自分たちが楽しまなくちゃ、人を楽しませることはできません。<br><br>だから、やらされ仕事は禁止です。<br><br>やりたい仕事を自分たちでつくる。<br><br>これがモバイルファクトリーのスタンスです。</td>
</tr>
<tr>
<th>ユニークな福利厚生！</th>
<td>ベンチャーでありながら当社は、福利厚生に特徴があり社員が働きやすい環境づくりに積極的に取り組んでいます。<br><br>特に、社員の働きやすさを目指した、SweetHome制度（会社指定の地域（目安：半径3km以内）に住むと月2万円が支給）や、有給と組み合わせて長期休暇が可能なカフェテリア休暇、妊娠時時短制度、育児時時短制度などは社員にも好評です。<br><br>また、社員同士のコミュニケーションを大切にし、仕事への活力とするため部活動を積極的に行うなど他部署間の交流を大切にしています。<br><br>このようにモバイルファクトリーでは、長期的に働きやすい会社であることを目指し、プライベートも充実できるように積極的に取り組みを行っています。</td>
</tr>
</table>
<!-- /detail -->
</div><!-- /articleBody -->
<footer>
<p><a href="http://www.mobilefactory.jp/recruit/" target="_blank" class="buttonA01"><span class="extarnal">求人詳細を見る</span></a></p>
</footer>
</article>
</section>
</div><!-- /mainContents -->


<div id="sideContents" class="mediaPC">

<section>
<h2 class="headingA02">広告</h2>
<div><img src="/public/images/dummy/dummy_adsense_01.png" width="233" height="733" alt=""/></div>
</section>

<aside class="ad">
<div><img src="/public/images/common/dummy_ad_234-60.gif" width="234" height="60" alt=""/></div>
<div><img src="/public/images/common/dummy_ad_120-600.gif" width="120" height="600" alt=""/></div>
</aside>
</div>

</div>