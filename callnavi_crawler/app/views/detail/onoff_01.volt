<div class="contentsInner">
<div id="mainContents">
<section>
<article class="jobDetailA01">
<header>
<h3><a href="#" target="_blank">マークアップエンジニア</a></h3>
<p class="date">更新日：0000年00月00日</p>
</header>
<div class="articleBody">
<div class="featureA01 js-slider">
<a href="#">
<div class="slidesContainer">
<div class="crossfade">
<ul class="slides">
<li><img src="/public/images/job_detail/ononff_img_01.png" width="680" alt=""></li>
<li><img src="/public/images/job_detail/mod_no-img_01.png" width="680" alt=""></li>
<li><img src="/public/images/job_detail/mod_no-img_01.png" width="680" alt=""></li>
</ul>
</div>
</div>
</a>
<div class="slideControl">
<ul class="select">
<li><a href="javascript:;">1</a></li>
<li><a href="javascript:;">2</a></li>
<li><a href="javascript:;">3</a></li>
</ul>
</div>
</div><!-- /featureA01 -->
<p class="company">株式会社オノフ</p>
<p class="summary">クライアントの新規顧客獲得におけるニーズ・課題を引き出し、<br>最適な手法を提案すること。</p>
<div class="introduction">
<p>[仕事内容]<br>新規キャンペーンページ、運用案件などのHTML、CSSのコーディングをご担当いただきます。スマートフォン、iPad対応サイト(HTML5)などのコーディングや既存サイトの運用更新作業も行って頂きます。</p>
</div>
<table class="tableA01 description">
<col style="width:160px;">
<col>
<tr>
<th>応募要項</th>
<td>[応募資格]<br>■必須スキル<br>・実務経験 2年以上<br>・HTML/CSSでのコーディングの実装経験<br>・外部APIやサーバサイドプログラムとの連携の実装経験<br>・Javascriptを理解している／Jqueryなどのライブラリを利用できる<br><br>□歓迎スキル<br>・中規模以上のサイト新規構築経験<br>・運用更新作業の経験<br>・HTML5+CSS3<br>・PHPなど各種プログラミング言語<br>・CMSなどシステム連携したページ構築経験<br>・簡単な画像補正、作成<br><br>□歓迎する人物像<br>・チームでの作業が得意な方<br>・常に新しい技術を身につけることが楽しい方<br><br>※ご応募時に必ず実績の分かるポートフォリオ(URL可)をご提示下さい。</td>
</tr>
<tr>
<th>雇用区分</th>
<td>正社員（中途）</td>
</tr>
<tr>
<th>勤務地</th>
<td>東京都、大阪府<br>勤務先所在地<br>【Tokyo Head Office】<br>東京都渋谷区東2-26-16渋谷HANAビル4F<br><br>【Osaka Creative Office】<br>大阪府大阪市西区新町1-6-23四ツ橋大川ビル9F</td>
</tr>
<tr>
<th>アクセス方法</th>
<td>【Tokyo Head Office】<br>各線恵比寿駅より徒歩7分<br>各線渋谷駅より徒歩8分<br><br>【Osaka Creative Office】<br>地下鉄四つ橋線四ツ橋駅より徒歩1分<br>地下鉄御堂筋線心斎橋駅より徒歩7分</td>
</tr>
<tr>
<th>年収（目安）</th>
<td>300万円～720万円<br>経験、能力、前職の給与を考慮し、面談の上決定いたします。<br><br>給与改定年2回（当社人事評価制度・上長との面談による） ※4月、10月<br>賞与年1回（決算賞与・業績によって支給）</td>
</tr>
<tr>
<th>勤務時間</th>
<td>10:00～19:00</td>
</tr>
<tr>
<th>フレックス</th>
<td>なし</td>
</tr>
<tr>
<th>休日</th>
<td>完全週休二日制（土・日）、祝日<br>有給休暇、GW、夏期休暇、年末年始休暇、慶弔休暇<br>※業務状況によっては出勤となる場合もあります（代休取得可）</td>
</tr>
<tr>
<th>通勤交通費</th>
<td>あり</td>
</tr>
<tr>
<th>保険</th>
<td>社会保険完備</td>
</tr>
<tr>
<th>諸手当</th>
<td>待遇・福利厚生</td>
</tr>
<tr>
<th>選考プロセス</th>
<td>▼書類選考<br>書類選考に合格された方には、面接の日程をご連絡します。<br><br>▼一次面接／配属部署リーダー<br>弊社の事業内容、あなたに担っていただきたい仕事内容を具体的にお話しいたします。<br><br>▼二次面接／役員<br>一次面接に合格された方の面接となります。<br><br>▼三次面接／代表取締役<br>二次面接に合格された方の最終面接となります。<br><br>▼内定通知<br>最終面接に合格された方は内定となります。※ご応募から内定までは、2～3週間となります。</td>
</tr>
<tr>
<th>備考</th>
<td>■Tokyo Head Office<br>役員1名<br>プロデュース部　4名（アートディレクター、ディレクター）<br>制作部　今回新規採用<br>マネジメント部　1名（人事、広報企画、経理）<br>(2014年4月15日時点）<br><br>制作は大阪オフィスで行っていますが、案件数拡大につき一層の制作力強化のため東京オフィスにも制作チームを設けることとなりました。<br><br>■Osaka Creative Office<br>役員　1名<br>ディレクション部　3名<br>ディベロップメント部　6名<br>クリエイティブ部　10名<br>マネジメント部　1名<br>(2014年4月15日時点）<br><br>その他海外スタッフ</td>
</tr>
<tr>
<th>タグ</th>
<td>
<ul class="tags">
<li>服装自由</li>
<li>学歴不問</li>
<li>自社サービスあり</li>
<li>平均年齢20代</li>
</ul>
</td>
</tr>
</table>
<!-- /detail -->
</div><!-- /articleBody -->
<footer>
<p><a href="http://www.onoff.ne.jp/#recruit" target="_blank" class="buttonA01"><span class="extarnal">求人詳細を見る</span></a></p>
</footer>
</article>
</section>
</div><!-- /mainContents -->


<div id="sideContents" class="mediaPC">

<section>
<h2 class="headingA02">広告</h2>
<div><img src="/public/images/dummy/dummy_adsense_01.png" width="233" height="733" alt=""/></div>
</section>

<aside class="ad">
<div><img src="/public/images/common/dummy_ad_234-60.gif" width="234" height="60" alt=""/></div>
<div><img src="/public/images/common/dummy_ad_120-600.gif" width="120" height="600" alt=""/></div>
</aside>
</div>

</div>