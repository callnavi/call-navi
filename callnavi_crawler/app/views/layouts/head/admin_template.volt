<!DOCTYPE HTML>
<html lang="ja"><!-- InstanceBegin template="/Templates/admin_not.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<script type="text/javascript"><!--
// 配列や連想配列、オブジェクトなどの中身を視覚的に表示する関数
function vardump(arr,lv,key) {
    var dumptxt = "",
        lv_idt = "",
        type = Object.prototype.toString.call(arr).slice(8, -1);
    if(!lv) lv = 0;
    for(var i=0;i<lv;i++) lv_idt += "    ";
    if(key) dumptxt += lv_idt + "[" + key + "] => ";
    
    if(arr == null || arr == undefined){
        dumptxt += arr + '\n';
    } else if(type == "Array" || type == "Object"){
        dumptxt += type + "...{\n";
        for(var item in arr) dumptxt += vardump(arr[item],lv+1,item);
        dumptxt += lv_idt + "}\n";
    } else if(type == "String"){
        dumptxt += '"' + arr + '" ('+ type +')\n';
    }  else if(type == "Number"){
        dumptxt += arr + " (" + type + ")\n";
    } else {
        dumptxt += arr + " (" + type + ")\n";
    }
    return dumptxt;
}
--></script>
<meta charset="utf-8">
<meta name="author" content="" />
<meta name="copyright" content="" />
<meta name="viewport" content="width=1200" />
<!-- InstanceBeginEditable name="doctitle" --><title>コールナビ</title><!-- InstanceEndEditable -->
<link rel="stylesheet" href="/public/admin_css/import.css" media="screen, print" /> 
<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<!--[if lt IE 9]> <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script> <![endif]-->
<script src="/public/scripts/smooth-scroll.js"></script>
<!-- InstanceBeginEditable name="head" --><!-- InstanceEndEditable -->
<!-- InstanceParam name="page_id" type="text" value="" -->
<script>
$(function(){
	$('.blank').click(function(){
		window.open(this.href, '_blank');
		return false;
	});
});
</script>
<?php if (! isset($this->session->get('auth')['id'])): ?>

<?php else: ?>

<!-- InstanceBeginEditable name="head" -->
<script type="text/javascript" src="/public/admin_scripts/dropdown.js"></script>
<script type="text/javascript" src="/public/admin_scripts/rollover.js"></script>

<!-- InstanceEndEditable -->
<!-- InstanceParam name="page_id" type="text" value="home" -->

<?php endif; ?>
<?php $this->assets->outputCss() ?>
</head>

{{ partial('/layouts/head/analyticstracking')}}
{{ content() }}
<?php $this->assets->outputJs() ?>

</html>
