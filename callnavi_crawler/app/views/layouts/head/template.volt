<!DOCTYPE HTML>
<html lang="ja"><!-- InstanceBegin template="/Templates/base.dwt" codeOutsideHTMLIsLocked="false" -->
    <head>

        <?php if(stristr($_SERVER['REQUEST_URI'], '/search/result')): ?>
        {{ partial('/layouts/head/meta_tags_result')}}
        <?php else: ?>
        {{ partial('/layouts/head/meta_tags_static')}}
        <?php endif; ?>

	<meta property="og:title" content="{{head.title}}" />
	<meta property="og:type" content="website" />
	<meta property="og:description" content="{{head.description}}" />
	<meta property="og:url" content="http://callnavi.jp/" />
	<meta property="og:image" content="http://callnavi.jp/ogp.png" />
	<meta property="og:site_name" content="全国のコールセンター転職・求人一括検索サイト CALL Navi（コールナビ）" />
	<meta property="og:locale" content="ja_JP" />
	<link rel="shortcut icon" href="/favicon.ico" />
	<link rel="apple-touch-icon-precomposed" href="/apple-touch-icon-precomposed.png" />

        <link rel="stylesheet" href="/public/css/import.css" media="screen, print" />
        <script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
        <!--[if lt IE 9]> <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script> <![endif]-->
        <script src="/public/scripts/smooth-scroll.js"></script>
        <!-- InstanceBeginEditable name="head" -->
        <script src="/public/scripts/common.js"></script>
        <script src="/public/scripts/offer_log.js"></script>
        <script src="/public/scripts/touch-event-callback.js"></script>	
        <script src="/public/scripts/slider.js"></script>
	<script src="/public/scripts/tab-control.js"></script>
        <script>
            $(function() {
                /* ===== tabControl ====== */
                $('#js-tab01').tabControl({
                    addAnchorTabs: ['#js-tab02'],
                    animation: true
                });
            });
        </script>
        <!-- InstanceEndEditable -->
        <!-- InstanceParam name="page_id" type="text" value="" -->
        <?php $this->assets->outputCss() ?>
    </head>

    <body>
        {{ partial('/layouts/head/analyticstracking')}}
        {{ content() }}
    </body>
</html>