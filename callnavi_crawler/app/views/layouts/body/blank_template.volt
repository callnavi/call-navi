<html lang="ja"><!-- InstanceBegin template="/Templates/admin_blank.dwt" codeOutsideHTMLIsLocked="false" -->
<head>
<meta charset="utf-8">
<meta name="author" content="" />
<meta name="copyright" content="" />
<meta name="viewport" content="width=1200" />
<meta name="format-detection" content="telephone=no" />
<!-- InstanceBeginEditable name="doctitle" --><title>コールナビ</title><!-- InstanceEndEditable -->
<link rel="stylesheet" href="/public/admin_css/import.css" media="screen, print" />
<script src="//code.jquery.com/jquery-1.11.0.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<!--[if lt IE 9]> <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script> <![endif]-->
<script src="/public/scripts/smooth-scroll.js"></script>
<!-- InstanceBeginEditable name="head" -->
<script src="/public/admin_scripts/rollover.js"></script>
<script type="text/javascript" src="/public/scripts/tab-control.js"></script>
<script>
$(function() {
	$('#js-tab01').tabControl({animation: true});
	$('html,body').animate({ scrollTop: 0 },1);
});
</script>
<!-- InstanceEndEditable -->
<!-- InstanceParam name="page_id" type="text" value="" -->
</head>

                {{ content()}}

