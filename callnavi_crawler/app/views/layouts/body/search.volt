<div id="conditionsSearch">
    <div class="contentsInner">
        <form action="/search/result" method="get">
            <div class="area"><input type="text" name="area"  value="{% if params.area is defined %}{{params.area}}{% endif %}" placeholder="勤務地（例：東京都渋谷区、池袋駅、大阪市北区）"></div>
            <div class="keyword"><input type="text" name="keyword" value="{% if params.keyword is defined %}{{params.keyword}}{% endif %}" placeholder="キーワード（例：長期、高時給、短期、派遣、日払い、通信、保険、不動産）"></div>
            <div class="search"><button type="submit">検索</button></div>
        </form>

        {{ partial('/layouts/body/recommend_keywords')}}
        
    </div><!-- / contentsInner -->
</div><!-- / conditionsSearch -->
