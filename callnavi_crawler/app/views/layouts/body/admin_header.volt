<?php if(stristr($_SERVER['REQUEST_URI'], '/account/index') || stristr($_SERVER['REQUEST_URI'], '/account/input') || stristr($_SERVER['REQUEST_URI'], '/account/confirm') || stristr($_SERVER['REQUEST_URI'], '/account/thanks') || stristr($_SERVER['REQUEST_URI'], '/account/accountModifyDeleteThanks') || stristr($_SERVER['REQUEST_URI'], '/account/law') || stristr($_SERVER['REQUEST_URI'], '/login') || stristr($_SERVER['REQUEST_URI'], '/login/resetPassword')): ?>
 <body> 
    <header id="globalHeader">
        <div class="globalHeaderInner">
            <nav id="funcNav">
                <ul>
                    <li class="bfr"><a href="/account/input">新規アカウント作成</a></li>
                    <li><a href="/login/index">ログイン</a></li>
                </ul>
            </nav>

            <h1><a href="/customer/"><img src="/public/admin_images/header_logo_02.png" width="138" height="40"></a><span>求人広告の掲載について</span></h1>

        </div>
    </header>
<?php elseif(stristr($_SERVER['REQUEST_URI'], '/admin') || (isset($basic->is_admin) && $basic->is_admin==1 && (stristr($_SERVER['REQUEST_URI'], '/listing/edit') || stristr($_SERVER['REQUEST_URI'], '/rectangle/edit') || stristr($_SERVER['REQUEST_URI'], '/pickup/edit')))) : ?>
<body id="admin">
    <header id="globalHeader">
        <div class="globalHeaderInner">
            <nav id="funcNav">
                <ul>
                    <li><a href="/login/logout">ログアウト</a></li>
                </ul>
            </nav>


            <h1><a href="/admin/index"><img src="/public/admin_images/header_logo_03.png" width="138" height="40"></a><span>求人広告審査システム</span></h1>


            <p class="companyAndName"><span>株式会社コールナビ</span><em>広告審査担当者</em>様</p>
        </div>
        <nav id="globalNav" class="meta">
            <ul>

                <li <?php if(stristr($_SERVER['REQUEST_URI'], '/admin/index')): ?> class="here" <?php endif; ?>><a href="/admin/index"><span>求人広告審査（HOME）</span></a></li>
                <li<?php if(stristr($_SERVER['REQUEST_URI'], '/admin/allowedOffers')): ?> class="here" <?php endif; ?>><a href="/admin/allowedOffers"><span>審査済み広告一覧</span></a></li>

                <li<?php if(stristr($_SERVER['REQUEST_URI'], '/admin/accountLists')): ?> class="here" <?php endif; ?>><a href="/admin/accountLists"><span>アカウント一覧<br>（求人広告掲載状況）</span></a></li>
                <li<?php if(stristr($_SERVER['REQUEST_URI'], '/admin/credit')): ?> class="here" <?php endif; ?>> <a href="/admin/credit"><span>クレジットカード決済情報</span></a></li>

            </ul>
        </nav>
    </header>

<?php elseif(!stristr($_SERVER['REQUEST_URI'], '/customer/contract') && !stristr($_SERVER['REQUEST_URI'], '/pickup/confirmPreview') && !stristr($_SERVER['REQUEST_URI'], 'preview') && !stristr($_SERVER['REQUEST_URI'], '/pickup/cssSample') && !stristr($_SERVER['REQUEST_URI'], '/pickupModule') && !stristr($_SERVER['REQUEST_URI'], '/pickup/htmlArea') && !stristr($_SERVER['REQUEST_URI'], '/listing/keywordsSample') && isset($basic->id)) : ?>
 <body>
    <header id="globalHeader">
        <div class="globalHeaderInner">
            <nav id="funcNav">
                <ul>
                    <li><a href="/customer/contact">お問い合わせ</a></li>
                    <li><a href="/login/logout">ログアウト</a></li>
                </ul>
            </nav>


            <h1><a href="/customer/index"><img src="/public/admin_images/header_logo_02.png" width="138" height="40"></a><span>求人広告掲載システム</span></h1>


            <p class="companyAndName"><span>{{basic.company_name}}</span><em>{{basic.representative}}</em>様</p>
        </div>
        <nav id="globalNav">

            <ul>

                <li <?php if(stristr($_SERVER['REQUEST_URI'], '/customer/index')): ?> class="here" <?php endif; ?>><a href="/customer/index"><span>求人広告管理（HOME）</span></a></li>

                <li<?php if(stristr($_SERVER['REQUEST_URI'], '/customer/make') || stristr($_SERVER['REQUEST_URI'], '/listing/') || stristr($_SERVER['REQUEST_URI'], '/rectangle') || stristr($_SERVER['REQUEST_URI'], '/pickup')|| stristr($_SERVER['REQUEST_URI'], '/free/')): ?> class="here" <?php endif; ?>><a href="/customer/make"><span>新規求人広告作成</span></a></li>


                <li<?php if(stristr($_SERVER['REQUEST_URI'], '/customer/charge')): ?> class="here" <?php endif; ?>><a href="/customer/charge"><span>ご請求情報</span></a></li>



                <li<?php if(stristr($_SERVER['REQUEST_URI'], '/account/accountModify')): ?> class="here" <?php endif; ?>><a href="/account/accountModify"><span>登録情報の確認・変更</span></a></li>



                <li<?php if(stristr($_SERVER['REQUEST_URI'], '/credit/input')): ?> class="here" <?php endif; ?>><a href="/credit/input"><span>クレジットカード<br>情報の登録・変更</span></a></li>

            </ul>
        </nav>
    </header>
<?php else: ?>
<?php endif; ?>

