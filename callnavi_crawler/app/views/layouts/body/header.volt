
<input id="gnavCheckbox" type="checkbox">
<header id="globalHeader">
    <div id="floatBlockConteiner">
        <div id="floatingHeader">
            <h1 id="ci"><a href="/" class="js-baseurl">コールナビ</a></h1>
            <label for="gnavCheckbox" id="gnavIcon"></label>  
        </div>
    </div><!-- / floatBlockConteiner -->
    <p id="shoulder">全国のコールセンター転職・求人一括検索！<br class="mediaSP">アルバイト・中途・新卒もおまかせ！<span class="num"><br>コールセンター求人数
            <em>
                {% if alloffers>=1000 %}
                    {{(alloffers-alloffers%1000)/1000}},<!-- 
                    -->{% endif %}<!--
                    -->{% if alloffers<1000 %}{{alloffers}}<!--
                    -->{% elseif alloffers%1000>=100 %}<!--
                    -->{{alloffers%1000}}<!--
                    -->{% elseif alloffers%1000>=10 %}<!--
                    -->0{{alloffers%1000}}<!--
                    -->{% else %}<!--
                    -->00{{alloffers%1000}}
                {% endif %}                                  
            </em>件 <span class="update">（<span>{{month}}</span>月<span>{{day}}</span>日&nbsp;<span>{{time}}</span>更新）</span></span></p>

    <nav id="globalNav">
        <ul>
            <li class="mediaSP"><a href="/">HOME</a></li>
            <li class="about"><a href="/about">コールナビとは？</a></li>
            <li class="mediaSP"><a href="/ccwork">コールセンターの仕事とは？</a></li>
            <li class="mediaSP"><a href="/ccwork/detail12">全国コールセンターまとめ</a></li>
            <li class="contact"><a href="https://callnavi.jp/contact/input.html">お問い合わせ</a></li>
        </ul>
    </nav>
</header>
