<div id="contents">
  <div id="contentsContainer">
    <!-- InstanceBeginEditable name="mainContents" -->
    <div class="unitA01">

      <h2 class="headingA01">アカウント一覧</h2>

      <div class="headingB01">
        <h3>アカウント一覧</h3>
      </div>

      <ul class="formBlockA01">
        <li id="search_desc">アカウント名検索</li>
        <li><input type="text" id="search_term" name="a01" class="inputA06"></li>
        <li><input type="button" id="search_button" class="formA01" value="検索"></li>
      </ul>

      <table id="accounts_table" class="tableA02 tableA03">

        <tr>
          <th class="alignL">会社名</th>
          <th class="alignL">ご担当者名</th>
          <th class="alignL">ご担当者メールアドレス</th>
          <th class="alignL">電話番号</th>
          <th>&nbsp;</th>
        </tr>

        {% if account_count > 0 %}
          {% for account in accounts %}

              <tr id="account_{{ account.id }}" data-account_id="{{ account.id }}">
                <td class="alignL">{{ account.company_name }}</td>
                <td class="alignL">{{ account.representative }}</td>
                <td class="alignL">{{ account.email }}</td>
                <td class="alignL">{{ account.telephone }}</td>
                <td><a href="/admin/accountListsContent?account_id={{ account.id }}"><img src="/public/admin_images/form_bg_search_n.png" alt=詳細情報の確認"></a></td>
              </tr>

          {% endfor %}
        {% else %}

        <tr>
          <td class="alignL" colspan="5">登録されているアカウントはありません。</td>
        </tr>

        {% endif %}

        <!-- MockUp
        <tr>
          <td class="alignL">株式会社メタフェイズ</td>
          <td class="alignL">城野 誠大</td>
          <td class="alignL">shirono@metaphase.co.jp</td>
          <td class="alignL">03-2222-3333</td>
          <td><input type="button" class="formA01" value="詳細情報の確認" onClick="javascript:location.href='/admin/accountListsContent';"></td>
        </tr>
        -->

      </table>

    </div><!-- /.unitA01 -->
    <!-- InstanceEndEditable -->
  </div><!-- / contentsContainer -->
</div><!-- / contents -->