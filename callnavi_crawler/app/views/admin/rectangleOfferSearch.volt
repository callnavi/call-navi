<table id="rectangle_offers_table" class="tableA02">

  <tbody>

    <tr>
      <th>承認日時</th>
      <th>ステータス</th>
      <th class="alignL pl05">広告名</th>
      <th>表示回数</th>
      <th>表示頻度</th>
      <th>クリック数</th>
      <th>クリック率</th>
      <th>クリック単価</th>
      <th>上限予算</th>
      <th>費用</th>
      <th>プレビュー</th>
    </tr>

    {% if rectangle_count > 0 %}
      {% for rectangle_offer in rectangle_offers %}
    
 
          <tr id="rectangle_offer_{{ rectangle_offer.id }}" data-offer_title="{{ rectangle_offer.title }}">
           <td>{{ rectangle_offer.allowed }}</td>
            <td class="alignC">
              <span class="{% if rectangle_offer.deleted == 1 %}unauthorized{% elseif rectangle_offer.status == 'judging' %}judge{% elseif rectangle_offer.status == 'publishing' %}{% if rectangle_offer.is_available ==0 %}unauthorized{% else %}publish{% endif %}{% elseif rectangle_offer.status == 'not_allowed' or rectangle_offer.deleted == 1%}unauthorized{% elseif rectangle_offer.status == 'waiting_card' %}wait{% elseif rectangle_offer.status == 'completed' %}stop{% elseif rectangle_offer.status == 'editing' %}edit{% elseif rectangle_offer.status == 'canceled' %}stop{% endif %}" id="rectangle_offer_status_{{ rectangle_offer.id }}">
                {% if rectangle_offer.deleted == 1 %}
                  ユーザ削除
                {% elseif rectangle_offer.status == "judging" %}
                  審査中
                {% elseif rectangle_offer.status == "publishing" %}
                      {% if rectangle_offer.is_available == 0 %}
                       上限予算到達
　　　　　　　　　　　　　　{% else %}　
                  　　　　　掲載中
                       {% endif %}
                {% elseif rectangle_offer.status == "not_allowed" %}
                  非承認
                {% elseif rectangle_offer.status == "waiting_card" %}
                  カード登録待ち
                {% elseif rectangle_offer.status == "completed" %}
                  掲載終了
                {% elseif rectangle_offer.status == "editing" %}
                  編集中
                {% elseif rectangle_offer.status == "canceled" %}
                  一時停止中
                
                {% endif %}
              </span>
            </td>
            <td class="alignL">{{ rectangle_offer.title }}</td>
            <td id="rectangle_offer_impression_log_{{ rectangle_offer.id }}">{{ rectangle_offer.impression_log }}</td>
            <td><span class="freq_4">低</span></td>
            <td id="rectangle_offer_click_log_{{ rectangle_offer.id }}">{{ rectangle_offer.click_log }}</td>
            <td>
              {% if rectangle_offer.click_ratio === false %}
              <span id="rectangle_offer_click_ratio_{{ rectangle_offer.id }}">-</span>%
              {% else %}
              <span id="rectangle_offer_click_ratio_{{ rectangle_offer.id }}">{{ rectangle_offer.click_ratio }}</span>%
              {% endif %}
            </td>
            <td>&yen;{{ rectangle_offer.click_price }}</td>
            <td>&yen;{{ rectangle_offer.budget_max }}</td>
            <td>&yen;<span id="rectangle_offer_expense_{{ rectangle_offer.id }}">{{ rectangle_offer.expense }}</span></td>
            <td><a href="/rectangle/preview?offer_id={{ rectangle_offer.id }}&account_id={{ rectangle_offer.account_id }}" target="_blank"><img src="/public/admin_images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
          </tr>
        
       {% endfor %}

      <tr class="lastCol">
        <td colspan="10" class="lastCell">対象期間合計　&yen;<span id="rectangle_offer_expense_sum">{{ rectangle_expense_sum }}</span></td>
      </tr>

    {% else %}

      <tr class="lastCol">
        <td class="alignL" colspan="10">登録されているレクタングル広告はありません。</td>
      </tr>

    {% endif %}

  </tbody>
</table>