<div id="contents">
  <div id="contentsContainer">
    <!-- InstanceBeginEditable name="mainContents" -->
    <form method="post" action="/admin/allowedOffers#" enctype="multipart/form-data">
   
      <div class="unitA01">

        <h2 class="headingA01">審査済み広告一覧</h2>

        <div class="unitB01">
          <div class="tabNavA01">
            <ul id="js-tab01">
              <li class="active"><a href="#tabContents00">無料広告</a></li>
              <li ><a href="#tabContents01">リスティング広告</a></li>
              <li><a href="#tabContents02">レクタングル広告</a></li>
              <li><a href="#tabContents03">ピックアップ広告</a></li>
            </ul>
          </div>
          <ul class="formBlockA01 withTabNav">
            <li id="search_desc">検索</li>
            <li><input type="text" name="a00" id="search_term" class="inputA06"></li>
            <li><input type="button" id="search_button" class="formA01" value="検索"></li>
          </ul>
        </div>
        
         <div id="tabContents00">
    <div class="unitB02">
    <ul class="formBlockA01">
    <li>対象期間</li>
    <li><select name="free_scope" class="formA01 sfr">
        <option value="today">今日</option>
        <option value="yesterday">昨日</option>
        <option value="week">今週（月～）</option>
        <option value="seven_days">過去7日間：{{ one_week_ago }} 〜 {{ today }}</option>
        <option value="last_week">先週（月～日）</option>
        <option value="month">今月</option>
        <option value="thirty_days">過去30日間</option>
        <option value="last_month">先月</option>
        <option value="all">全期間</option>
    </select></li>
    <li><input type="button" class="formA01" id="change_free_scope_for_admin" value="適用"></li>
    </ul>
    </div>
         <table id="free_offers_table" class="tableA02">
            <tr>
            <th>承認日時</th>
            <th>ステータス</th>
            <th class="alignL pl05">募集職種</th>
            <th class="alignL pl05">募集会社名</th>
            <th>表示回数</th>
            <th>クリック数</th>
            <th>クリック率</th>
            <th>プレビュー</th>
            </tr>
                {% if free_count > 0 %}
                  {% for free_offer in free_offers %}

                      <tr id="free_offer_{{ free_offer.id }}" data-offer_title="{{ free_offer.title }}">
                        <td>{% if free_offer.allowed !== "0000-00-00 00:00:00" %}{{ free_offer.allowed }}{% else %}-{% endif %}</td>
                        <td class="alignC">
                          <span class="{% if free_offer.deleted == 1 %}unauthorized{% elseif free_offer.status == "judging" %}judge{% elseif free_offer.status == "publishing" %}{% if free_offer.allowed >= six_month_ago %}publish{% else %}stop{% endif %}{% elseif free_offer.status == "judging" %}judge{% elseif free_offer.status == "publishing" %}{% if free_offer.allowed >= six_month_ago %}publish{% else %}stop{% endif %}{% elseif free_offer.status == 'not_allowed' %}unauthorized{% elseif free_offer.status == 'editing' %}edit{% elseif free_offer.status == 'canceled' %}stop{% endif %}" id="free_offer_status_{{ free_offer.id }}">
                            {% if free_offer.deleted == 1 %}
                              ユーザー削除
                            {% elseif free_offer.status == "publishing" %}
                                {% if free_offer.allowed >= six_month_ago %}
                                   掲載中
                                {% else %} 
                                   掲載終了
                                {% endif %}
                            {% elseif free_offer.status == "not_allowed" %}
                              非承認
                            
                            
                            {% elseif free_offer.status == "canceled" %}
                              一時停止中
                            {% elseif free_offer.status == "editing" %}
                              編集中
                            {% endif %}
                          </span>
                        </td>
                        <td class="alignL">{{ free_offer.job_category }}</td>
                        <td class="alignL">{{ free_offer.company_name }}</td>
                        
                        <td id="free_offer_impression_log_{{ free_offer.id }}">{{ free_offer.impression_log }}</td>
                        <td id="free_offer_click_log_{{ free_offer.id }}">{{ free_offer.click_log }}</td>
                        <td>
                          {% if free_offer.click_ratio === false  %}
                            <span id="free_offer_click_ratio_{{ free_offer.id }}">-</span>%
                          {% else %}
                            <span id="free_offer_click_ratio_{{ free_offer.id }}">{{ free_offer.click_ratio }}</span>%
                          {% endif %}
                        </td>

                        
                        
                        <td><a href="/free/preview?offer_id={{ free_offer.id }}" target="_blank"><img src="/public/admin_images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
                      </tr>

                  {% endfor %}
                  
                  

                {% else %}

                  <tr>
                    <!--<td><span class="wait"></span></td>-->
                    <td class="alignL" colspan="13">登録されている無料広告はありません。</td>
                  </tr>

                {% endif %}
  </table>
</div>

          <div id="tabContents01" style="display: block;">

            <div class="unitB02">
              <ul class="formBlockA01">
                <li>対象期間</li>
                <li>
                  <select class="formA01 sfr" name="listing_scope">
                    <option value="today">今日</option>
                    <option value="yesterday">昨日</option>
                    <option value="week">今週（月～）</option>
                    <option value="seven_days">過去7日間：{{ one_week_ago }} 〜 {{ today }}</option>
                    <option value="last_week">先週（月～日）</option>
                    <option value="month">今月</option>
                    <option value="thirty_days">過去30日間</option>
                    <option value="last_month">先月</option>
                    <option value="all">全期間</option>
                  </select>
                </li>
                <li><input type="button" class="formA01" value="適用" id="change_listing_scope_for_admin"></li>
              </ul>
            </div>

            <table  id="listing_offers_table" class="tableA02">

              <tbody>
                <tr>
                  <th>承認日時</th>
                  <th>ステータス</th>
                  <th class="alignL pl05">募集職種</th>
                  <th class="alignL pl05">募集会社名</th>
                  <th>検索ワード</th>
                  <th>表示回数</th>
                  <th>表示頻度</th>
                  <th>クリック数</th>
                  <th>クリック率</th>
                  <th>クリック単価</th>
                  <th>上限予算</th>
                  <th>費用</th>
                  <th>プレビュー</th>
                </tr>

                {% if listing_count > 0 %}
                  {% for listing_offer in listing_offers %}

                      <tr id="listing_offer_{{ listing_offer.id }}" data-offer_title="{{ listing_offer.title }}">
                        <td>{% if listing_offer.allowed !== "0000-00-00 00:00:00" %}{{ listing_offer.allowed }}{% else %}-{% endif %}</td>
                        <td class="alignC">
                          <span class="{% if listing_offer.deleted == 1 %}unauthorized{% elseif listing_offer.status == 'judging' %}judge{% elseif listing_offer.status == 'publishing' %}{% if listing_offer.is_available == 0 %}unauthorized{% else %}publish{% endif %}{% elseif listing_offer.status == 'not_allowed' %}unauthorized{% elseif listing_offer.status == 'waiting_card' %}{% if listing_offer.creditcard_id != "" and listing_offer.card_status == "before_approval" %}unauthorized{% else %}wait{% endif %}{% elseif listing_offer.status == 'completed' %}stop{% elseif listing_offer.status == 'editing' %}edit{% elseif listing_offer.status == 'canceled' %}stop{% endif %}" id="listing_offer_status_{{ listing_offer.id }}">
                            {% if listing_offer.deleted == 1 %}
                              ユーザー削除
                            
                            {% elseif listing_offer.status == "publishing" %}
                                 {% if listing_offer.is_available == 0 %}
                                   上限予算到達
                                  {% else %}     
                                      掲載中
                                  {% endif %}
                            {% elseif listing_offer.status == "not_allowed" %}
                              非承認
                            {% elseif listing_offer.status == "waiting_card" %}
                                {% if listing_offer.creditcard_id != "" and listing_offer.card_status == "before_approval" %}
                                    カード審査中
                                {% else %}
                                    カード登録待ち
                                {% endif %}
                            {% elseif listing_offer.status == "completed" %}
                              掲載終了
                            
                            {% elseif listing_offer.status == "canceled" %}
                              一時停止中
                            {% elseif listing_offer.status== "editing" %}
                               編集中
                            {% endif %}
                          </span>
                        </td>
                        <td class="alignL">{{ listing_offer.job_category }}</td>
                        <td class="alignL">{{ listing_offer.company_name }}</td>
                        <td class="noWrap">{{ listing_offer.keyword }}個 <a href="/admin/allowedListingKeywords?offer_id={{ listing_offer.id }}&account_id={{ listing_offer.account_id }}"><img src="/public/admin_images/form_bg_search_n.png" alt="" width="40" height="28"></a></td>
                        <td id="listing_offer_impression_log_{{ listing_offer.id }}">{{ listing_offer.impression_log }}</td>
                        <td class="listing_offer_impression_ratio_{{ listing_offer.id }}">
                            {% if listing_offer.impression_ratio == "error" %}
                                -
                            {% elseif listing_offer.impression_ratio >= 80 %}
                                <span class="freq_1">最高</span>
                            {% elseif listing_offer.impression_ratio >= 60 %}
                                <span class="freq_2">高</span>
                            {% elseif listing_offer.impression_ratio >= 40 %}
                                <span class="freq_3">普通</span>
                            {% elseif listing_offer.impression_ratio >= 0 %}
                                <span class="freq_4">低</span>                                
                            {% endif %}
                             
                        </td>
                        <td id="listing_offer_click_log_{{ listing_offer.id }}">{{ listing_offer.click_log }}</td>
                        <td>
                          {% if listing_offer.click_ratio === false  %}
                            <span id="listing_offer_click_ratio_{{ listing_offer.id }}">-</span>%
                          {% else %}
                            <span id="listing_offer_click_ratio_{{ listing_offer.id }}">{{ listing_offer.click_ratio }}</span>%
                          {% endif %}
                        </td>

                        <td>
                            {% if listing_offer.without_card == 1 %}
                                <font color="#ff0000">&yen;{{ listing_offer.click_price }}</font>
                            {% else %}
                                &yen;{{ listing_offer.click_price }}
                            {% endif %}
                        </td>
                        <td>
                            {% if listing_offer.without_card == 1 %}
                                <font color="#ff0000">&yen;{{ listing_offer.budget_max }}</font>
                            {% else %}
                                &yen;{{ listing_offer.budget_max }}
                            {% endif %}
                        </td>
                        <td>
                            {% if listing_offer.without_card == 1 %}
                              <font color="#ff0000">&yen;<span id="listing_offer_expense_{{ listing_offer.id }}">{{ listing_offer.expense }}</span></font>
                            {% else %}
                              &yen;<span id="listing_offer_expense_{{ listing_offer.id }}">{{ listing_offer.expense }}</span>
                            {% endif %}
                        </td>
                        
                        <td><a href="/listing/preview?offer_id={{ listing_offer.id }}&account_id={{ listing_offer.account_id }}" target="_blank"><img src="/public/admin_images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
                      </tr>

                  {% endfor %}
                  {% if listing_cardless_expense_sum !== 0 %}
                  <tr>
                   <td colspan="13" class="lastCell alertB01"> カードなし決済合計 &yen;<span id="listing_offer_cardless_expense_sum">{{ listing_cardless_expense_sum }}</span></td>
                 </tr>
                   {% endif %}
                  <tr>
                    <td colspan="13" class="lastCell">対象期間合計  &yen;<span id="listing_offer_expense_sum">{{ listing_expense_sum }}</span></td>
                  </tr>

                {% else %}

                  <tr>
                    <!--<td><span class="wait"></span></td>-->
                    <td class="alignL" colspan="13">登録されているリスティング広告はありません。</td>
                  </tr>

                {% endif %}

              </tbody>
            </table>
          </div>

          <div id="tabContents02" style="display: none;">

            <div class="unitB02">
              <ul class="formBlockA01">
                <li>対象期間</li>
                <li>
                  <select class="formA01 sfr" name="rectangle_scope">
                    <option value="today">今日</option>
                    <option value="yesterday">昨日</option>
                    <option value="week">今週（月～）</option>
                    <option value="seven_days">過去7日間：{{ one_week_ago }} 〜 {{ today }}</option>
                    <option value="last_week">先週（月～日）</option>
                    <option value="month">今月</option>
                    <option value="thirty_days">過去30日間</option>
                    <option value="last_month">先月</option>
                    <option value="all">全期間</option>
                  </select>
                <li><input type="button" class="formA01" value="適用" id="change_rectangle_scope_for_admin"></li>
              </ul>
            </div>

            <table id="rectangle_offers_table" class="tableA02">

              <tbody>

                <tr>
                  <th>承認日時</th> 
                  <th>ステータス</th>
                  <th class="alignL pl05">広告名</th>
                  <th>表示回数</th>
                  <th>表示頻度</th>
                  <th>クリック数</th>
                  <th>クリック率</th>
                  <th>クリック単価</th>
                  <th>上限予算</th>
                  <th>費用</th>
                  <th>プレビュー</th>
                </tr>

                {% if rectangle_count > 0 %}
                  {% for rectangle_offer in rectangle_offers %}

                      <tr id="rectangle_offer_{{ rectangle_offer.id }}" data-offer_title="{{ rectangle_offer.title }}">
                        <td>{% if rectangle_offer.allowed !== "0000-00-00 00:00:00" %}{{ rectangle_offer.allowed }}{% else %}-{% endif %}</td>
                        <td class="alignC">
                          <span class="{% if rectangle_offer.deleted == 1 %}unauthorized{% elseif rectangle_offer.status == 'judging' %}judge{% elseif rectangle_offer.status == 'publishing' %}{% if rectangle_offer.is_available ==0 %}unauthorized{% else %}publish{% endif %}{% elseif rectangle_offer.status == 'not_allowed' %}unauthorized{% elseif rectangle_offer.status == 'waiting_card' %}{% if listing_offer.creditcard_id != "" and listing_offer.card_status == "before_approval" %}unauthorized{% else %}wait{% endif %}{% elseif rectangle_offer.status == 'completed' %}stop{% elseif rectangle_offer.status == 'editing' %}edit{% elseif rectangle_offer.status == 'canceled' %}stop{% elseif rectangle_offer.status == 'run_out' %}unauthorized{% endif %}" id="rectangle_offer_status_{{ rectangle_offer.id }}">
                            {% if rectangle_offer.deleted == 1 %}
                              ユーザー削除
                            
                            {% elseif rectangle_offer.status == "publishing" %}
                                 {% if rectangle_offer.is_available == 0 %}
                                   上限予算到達
                                  {% else %}  
                                      掲載中
                                  {% endif %}
                            {% elseif rectangle_offer.status == "not_allowed" %}
                              非承認
                            {% elseif rectangle_offer.status == "waiting_card" %}
                                {% if rectangle_offer.creditcard_id != "" and rectangle_offer.card_status == "before_approval" %}
                                    カード審査中
                                {% else %}
                                    カード登録待ち
                                {% endif %}
                            {% elseif rectangle_offer.status == "completed" %}
                              掲載終了
                            
                            {% elseif rectangle_offer.status == "canceled" %}
                              一時停止中
                            {% elseif rectangle_offer.status == "editing" %}
                              編集中
                            {% endif %}
                          </span>
                        </td>
                        <td class="alignL">{{ rectangle_offer.title }}</td>
                        <td id="rectangle_offer_impression_log_{{ rectangle_offer.id }}">{{ rectangle_offer.impression_log }}</td>
                        <td class="rectangle_offer_impression_ratio_{{ rectangle_offer.id }}">
                            {% if rectangle_offer.impression_ratio == "error" %}
                                -
                            {% elseif rectangle_offer.impression_ratio >= 80 %}
                                <span class="freq_1">最高</span>
                            {% elseif rectangle_offer.impression_ratio >= 60 %}
                                <span class="freq_2">高</span>
                            {% elseif rectangle_offer.impression_ratio >= 40 %}
                                <span class="freq_3">普通</span>
                            {% elseif rectangle_offer.impression_ratio >= 0 %}
                                <span class="freq_4">低</span>                                
                            {% endif %}
                        </td>
                        <td id="rectangle_offer_click_log_{{ rectangle_offer.id }}">{{ rectangle_offer.click_log }}</td>
                        <td>
                          {% if rectangle_offer.click_ratio === false %}
                          <span id="rectangle_offer_click_ratio_{{ rectangle_offer.id }}">-</span>%
                          {% else %}
                          <span id="rectangle_offer_click_ratio_{{ rectangle_offer.id }}">{{ rectangle_offer.click_ratio }}</span>%
                          {% endif %}
                        </td>
                        <td>
                            {% if rectangle_offer.without_card == 1 %}
                                <font color="#ff0000">&yen;{{ rectangle_offer.click_price }}</font>
                            {% else %}
                                &yen;{{ rectangle_offer.click_price }}
                            {% endif %}
                        </td>
                        <td>
                            {% if rectangle_offer.without_card == 1 %}
                                <font color="#ff0000">&yen;{{ rectangle_offer.budget_max }}</font>
                            {% else %}
                                &yen;{{ rectangle_offer.budget_max }}
                            {% endif %}
                        </td>
                        <td>
                            {% if rectangle_offer.without_card == 1 %}
                              <font color="#ff0000">&yen;<span id="rectangle_offer_expense_{{ rectangle_offer.id }}">{{ rectangle_offer.expense }}</span></font>
                            {% else %}
                              &yen;<span id="rectangle_offer_expense_{{ rectangle_offer.id }}">{{ rectangle_offer.expense }}</span>
                            {% endif %}
                        </td>
                        <td><a href="/rectangle/preview?offer_id={{ rectangle_offer.id }}&account_id={{ rectangle_offer.account_id }}" target="_blank"><img src="/public/admin_images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
                      </tr>
                  {% endfor %}
                  {% if rectangle_cardless_expense_sum !== 0 %}
                  <tr>
                    <td colspan="11" class="lastCell alertB01"> カードなし決済合計 &yen;<span id="rectangle_offer_cardless_expense_sum">{{ rectangle_cardless_expense_sum }}</span></td>
                  </tr>
                  {% endif %}
                  <tr>
                    <td colspan="11" class="lastCell">対象期間合計　&yen;<span id="rectangle_offer_expense_sum">{{ rectangle_expense_sum }}</span></td>
                  </tr>

                {% else %}

                  <tr class="lastCol">
                    <td class="alignL" colspan="11">登録されているレクタングル広告はありません。</td>
                  </tr>

                {% endif %}

              </tbody>
            </table>
          </div>

          <div id="tabContents03" style="display: none;">

            <table id="pickup_offers_table" class="tableA02">

              <tbody>

                <tr>
                  <th>承認日時</th>
                  <th>ステータス</th>
                  <th class="alignL pl05">募集職種</th>
                  <th class="alignL pl05">募集会社名</th>
                  <th>新着</th>
                  <th>詳細ページ</th>
                  <th>クリック数</th>
                  <th>掲載期間</th>
                  <th>費用</th>
                  <th>プレビュー</th>
                </tr>

                {% if pickup_count > 0 %}
                  {% for pickup_offer in pickup_offers %}

                      <tr id="pickup_offer_{{ pickup_offer.id }}" data-offer_title="{{ pickup_offer.title }}">
                        <td>{% if pickup_offer.allowed !== "0000-00-00 00:00:00" %}{{ pickup_offer.allowed }}{% else %}-{% endif %}</td>
                        <td class="alignC">
                          <span class="{% if pickup_offer.deleted == 1 %}unauthorized{% elseif pickup_offer.status == 'judging' %}judge{% elseif pickup_offer.status == 'publishing' %}publish{% elseif pickup_offer.status == 'will_publish' %}wait{% elseif pickup_offer.status == 'not_allowed' %}unauthorized{% elseif pickup_offer.status == 'waiting_card' %}wait{% elseif pickup_offer.status == 'completed' %}stop{% elseif pickup_offer.status == 'editing' %}edit{% elseif pickup_offer.status == 'canceled' %}stop{% endif %}" id="pickup_offer_status_{{ pickup_offer.id }}">
                            {% if pickup_offer.deleted == 1 %}
                              ユーザー削除
                            
                            {% elseif pickup_offer.status == "publishing" %}
                              掲載中
                            {% elseif pickup_offer.status == "will_publish" %}
                              {{ pickup_offer.published_from }}より掲載
                            {% elseif pickup_offer.status == "not_allowed" %}
                              非承認
                            {% elseif pickup_offer.status == "waiting_card" %}
                                {% if pickup_offer.creditcard_id != "" and pickup_offer.card_status == "before_approval" %}
                                    カード審査中
                                {% else %}
                                    カード登録待ち
                                {% endif %}
                            {% elseif pickup_offer.status == "completed" %}
                              掲載終了
                            
                            {% elseif pickup_offer.status == "canceled" %}
                              一時停止中
                             {% elseif pickup_offer.status == "editing" %}
                              編集中
                            {% endif %}
                          </span>
                        </td>
                        <td class="alignL">{{ pickup_offer.job_category }}</td>
                        <td class="alignL">{{ pickup_offer.company_name }}</td>
                        <td>
                          {% if pickup_offer.display_area == "inner" %}
                          インナーパネル
                          {% elseif pickup_offer.display_area == "inner_half" %}
                          インナーパネルハーフ
                          {% elseif pickup_offer.display_area == "right_column" %}
                          右カラム
                          {% elseif pickup_offer.display_area == "none" %}
                           無し
                          {% endif %}
                        </td>
                        <td>{% if pickup_offer.detail_content_type == "none" %}<span class="freq_4">無</span>{% else %}<span class="freq_1">有</span>{% endif %}</td>
                        <td>{{ pickup_offer.click_log }}</td>
                        <td>
                            {{ pickup_offer.publish_week }}週 {% if pickup_offer.status == "publishing" or pickup_offer.status == "will_publish" or pickup_offer.status == "canceled" %}（{{ pickup_offer.published_from }}～{{ pickup_offer.published_to }})
                             {% else %}
                             {#（{{ pickup_offer.published_from }}～{{ pickup_offer.published_to }}) #} 
                            {% endif %}
                        </td>
                        <td>
                            {% if pickup_offer.without_card == 1 %}
                              <font color="#ff0000">&yen;<span id="pickup_offer_expense_{{ pickup_offer.id }}">{{ pickup_offer.expense }}</span></font>
                            {% else %}
                              &yen;<span id="pickup_offer_expense_{{ pickup_offer.id }}">{{ pickup_offer.expense }}</span>
                            {% endif %}
                        </td>
                        <td><a href="/pickup/preview?offer_id={{ pickup_offer.id }}&account_id={{ pickup_offer.account_id }}" target="_blank"><img src="/public/admin_images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
                      </tr>

                  {% endfor %}
                  {% if pickup_cardless_expense_sum !== 0 %}
                  <tr>
                    <td colspan="10" class="lastCell alertB01"> カードなし決済合計 &yen;{{ pickup_cardless_expense_sum }}</td>
                  </tr>
                  {% endif %}
                  
                  <tr>
                    <td colspan="10" class="lastCell">対象期間合計　&yen;{{ pickup_expense_sum }}</td>
                  </tr>

                {% else %}

                  <tr class="lastCol">
                    <td class="alignL" colspan="10">登録されているピックアップ広告はありません</td>
                  </tr>

                {% endif %}

              </tbody>
            </table>
          </div>
        </div><!--/.tabContents-->

      </div><!-- /.unitA01 -->
    
{#
    <div class="unitA01">
<h2 class="headingA01">審査済み広告一覧</h2>

<div class="unitB01">
<div class="tabNavA01">
<ul id="js-tab01">
<li class="active"><a href="#tabContents00">無料広告</a></li>
<li><a href="#tabContents01">リスティング広告</a></li>
<li><a href="#tabContents02">レクタングル広告</a></li>
<li><a href="#tabContents03">ピックアップ広告</a></li>
</ul>
</div>
<ul class="formBlockA01 withTabNav">
<li>検索</li>
<li><input type="text" name="a00" class="inputA06"></li>
<li><input type="button" class="formA01" value="検索"></li>
</ul>
</div>

<div class="tabContents">

<div id="tabContents00">
<div class="unitB02">
<ul class="formBlockA01">
<li>対象期間</li>
<li><select name="a01" class="formA01 sfr">
<option>今日</option>
<option>昨日</option>
<option>今週（月～）</option>
<option>過去7日間：2015/01/20 〜 2015/01/27</option>
<option>先週（月～日）</option>
<option>今月</option>
<option>過去30日間</option>
<option>先月</option>
<option>全期間</option>
</select></li>
<li><input type="button" class="formA01" value="適用"></li>
</ul>
</div>
<table class="tableA02">
<tr>
<th>承認日時</th>
<th>ステータス</th>
<th class="alignL pl05">募集職種</th>
<th class="alignL pl05">募集会社名</th>
<th>表示回数</th>
<th>表示頻度</th>
<th>クリック数</th>
<th>クリック率</th>
<th>プレビュー</th>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="stop">停止中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td class="alignL">ファシリテーションジャパン</td>
<td>999,999,999</td>
<td><span class="freq_1">最高</span></td>
<td>999,999,999</td>
<td>100.0%</td>
<td><a href="c-2-8-4.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="edit">編集中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td class="alignL">ファシリテーションジャパン</td>
<td>999,999,999</td>
<td><span class="freq_1">最高</span></td>
<td>999,999,999</td>
<td>100.0%</td>
<td><a href="c-2-8-4.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="stop">停止中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td class="alignL">ファシリテーションジャパン</td>
<td>999,999,999</td>
<td><span class="freq_3">普通</span></td>
<td>999,999,999</td>
<td>100.0%</td>
<td><a href="c-2-8-4.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="edit">編集中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td class="alignL">ファシリテーションジャパン</td>
<td>999,999,999</td>
<td><span class="freq_1">最高</span></td>
<td>999,999,999</td>
<td>100.0%</td>
<td><a href="c-2-8-4.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="stop">停止中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td class="alignL">ファシリテーションジャパン</td>
<td>999,999,999</td>
<td><span class="freq_1">最高</span></td>
<td>999,999,999</td>
<td>100.0%</td>
<td><a href="c-2-8-4.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="edit">編集中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td class="alignL">ファシリテーションジャパン</td>
<td>999,999,999</td>
<td><span class="freq_1">最高</span></td>
<td>999,999,999</td>
<td>100.0%</td>
<td><a href="c-2-8-4.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="stop">停止中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td class="alignL">ファシリテーションジャパン</td>
<td>999,999,999</td>
<td><span class="freq_3">普通</span></td>
<td>999,999,999</td>
<td>100.0%</td>
<td><a href="c-2-8-4.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="edit">編集中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td class="alignL">ファシリテーションジャパン</td>
<td>999,999,999</td>
<td><span class="freq_1">最高</span></td>
<td>999,999,999</td>
<td>100.0%</td>
<td><a href="c-2-8-4.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="stop">停止中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td class="alignL">ファシリテーションジャパン</td>
<td>999,999,999</td>
<td><span class="freq_1">最高</span></td>
<td>999,999,999</td>
<td>100.0%</td>
<td><a href="c-2-8-4.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="edit">編集中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td class="alignL">ファシリテーションジャパン</td>
<td>999,999,999</td>
<td><span class="freq_1">最高</span></td>
<td>999,999,999</td>
<td>100.0%</td>
<td><a href="c-2-8-4.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="stop">停止中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td class="alignL">ファシリテーションジャパン</td>
<td>999,999,999</td>
<td><span class="freq_3">普通</span></td>
<td>999,999,999</td>
<td>100.0%</td>
<td><a href="c-2-8-4.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="edit">編集中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td class="alignL">ファシリテーションジャパン</td>
<td>999,999,999</td>
<td><span class="freq_1">最高</span></td>
<td>999,999,999</td>
<td>100.0%</td>
<td><a href="c-2-8-4.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
</table>
</div>

<div id="tabContents01">
<div class="unitB02">
<ul class="formBlockA01">
<li>対象期間</li>
<li><select name="a01" class="formA01 sfr">
<option>今日</option>
<option>昨日</option>
<option>今週（月～）</option>
<option>過去7日間：2015/01/20 〜 2015/01/27</option>
<option>先週（月～日）</option>
<option>今月</option>
<option>過去30日間</option>
<option>先月</option>
<option>全期間</option>
</select></li>
<li><input type="button" class="formA01" value="適用"></li>
</ul>
</div>
<table class="tableA02">
<tr>
<th>承認日時</th>
<th>ステータス</th>
<th class="alignL pl05">募集職種</th>
<th class="alignL pl05">募集会社名</th>
<th>検索ワード</th>
<th>表示回数</th>
<th>表示頻度</th>
<th>クリック数</th>
<th>クリック率</th>
<th>クリック単価</th>
<th>上限予算</th>
<th>費用</th>
<th>プレビュー</th>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="stop">停止中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td class="alignL">ファシリテーションジャパン</td>
<td class="noWrap">999個 <a href="a-4-2.html"><img src="images/form_bg_search_n.png" alt="" width="40" height="28"></a></td>
<td>999,999,999</td>
<td><span class="freq_1">最高</span></td>
<td>999,999,999</td>
<td>100.0%</td>
<td><span class="alertB01">&yen;99,999</span></td>
<td><span class="alertB01">&yen;9,999,999</span></td>
<td><span class="alertB01">&yen;9,999,999</span></td>
<td><a href="c-2-1-4.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="publish">掲載中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td class="alignL">ファシリテーションジャパン</td>
<td class="noWrap">999個 <a href="a-4-2.html"><img src="images/form_bg_search_n.png" alt="" width="40" height="28"></a></td>
<td>999,999,999</td>
<td><span class="freq_2">高</span></td>
<td>999,999,999</td>
<td>100.0%</td>
<td>&yen;99,999</td>
<td>&yen;9,999,999</td>
<td>&yen;9,999,999</td>
<td><a href="c-2-1-4.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="edit">編集中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td class="alignL">ファシリテーションジャパン</td>
<td class="noWrap">999個 <a href="a-4-2.html"><img src="images/form_bg_search_n.png" alt="" width="40" height="28"></a></td>
<td>999,999,999</td>
<td><span class="freq_3">普通</span></td>
<td>999,999,999</td>
<td>100.0%</td>
<td>&yen;99,999</td>
<td>&yen;9,999,999</td>
<td>&yen;9,999,999</td>
<td><a href="c-2-1-4.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="unauthorized">非承認</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td class="alignL">ファシリテーションジャパン</td>
<td class="noWrap">999個 <a href="a-4-2.html"><img src="images/form_bg_search_n.png" alt="" width="40" height="28"></a></td>
<td>999,999,999</td>
<td><span class="freq_1">最高</span></td>
<td>999,999,999</td>
<td>100.0%</td>
<td>&yen;99,999</td>
<td>&yen;9,999,999</td>
<td>&yen;9,999,999</td>
<td><a href="c-2-1-4.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="stop">停止中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td class="alignL">ファシリテーションジャパン</td>
<td class="noWrap">999個 <a href="a-4-2.html"><img src="images/form_bg_search_n.png" alt="" width="40" height="28"></a></td>
<td>999,999,999</td>
<td><span class="freq_1">最高</span></td>
<td>999,999,999</td>
<td>100.0%</td>
<td>&yen;99,999</td>
<td>&yen;9,999,999</td>
<td>&yen;9,999,999</td>
<td><a href="c-2-1-4.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="publish">掲載中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td class="alignL">ファシリテーションジャパン</td>
<td class="noWrap">999個 <a href="a-4-2.html"><img src="images/form_bg_search_n.png" alt="" width="40" height="28"></a></td>
<td>999,999,999</td>
<td><span class="freq_2">高</span></td>
<td>999,999,999</td>
<td>100.0%</td>
<td>&yen;99,999</td>
<td>&yen;9,999,999</td>
<td>&yen;9,999,999</td>
<td><a href="c-2-1-4.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="edit">編集中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td class="alignL">ファシリテーションジャパン</td>
<td class="noWrap">999個 <a href="a-4-2.html"><img src="images/form_bg_search_n.png" alt="" width="40" height="28"></a></td>
<td>999,999,999</td>
<td><span class="freq_3">普通</span></td>
<td>999,999,999</td>
<td>100.0%</td>
<td>&yen;99,999</td>
<td>&yen;9,999,999</td>
<td>&yen;9,999,999</td>
<td><a href="c-2-1-4.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="stop">停止中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td class="alignL">ファシリテーションジャパン</td>
<td class="noWrap">999個 <a href="a-4-2.html"><img src="images/form_bg_search_n.png" alt="" width="40" height="28"></a></td>
<td>999,999,999</td>
<td><span class="freq_1">最高</span></td>
<td>999,999,999</td>
<td>100.0%</td>
<td>&yen;99,999</td>
<td>&yen;9,999,999</td>
<td>&yen;9,999,999</td>
<td><a href="c-2-1-4.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="stop">停止中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td class="alignL">ファシリテーションジャパン</td>
<td class="noWrap">999個 <a href="a-4-2.html"><img src="images/form_bg_search_n.png" alt="" width="40" height="28"></a></td>
<td>999,999,999</td>
<td><span class="freq_1">最高</span></td>
<td>999,999,999</td>
<td>100.0%</td>
<td>&yen;99,999</td>
<td>&yen;9,999,999</td>
<td>&yen;9,999,999</td>
<td><a href="c-2-1-4.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="publish">掲載中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td class="alignL">ファシリテーションジャパン</td>
<td class="noWrap">999個 <a href="a-4-2.html"><img src="images/form_bg_search_n.png" alt="" width="40" height="28"></a></td>
<td>999,999,999</td>
<td><span class="freq_2">高</span></td>
<td>999,999,999</td>
<td>100.0%</td>
<td>&yen;99,999</td>
<td>&yen;9,999,999</td>
<td>&yen;9,999,999</td>
<td><a href="c-2-1-4.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="edit">編集中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td class="alignL">ファシリテーションジャパン</td>
<td class="noWrap">999個 <a href="a-4-2.html"><img src="images/form_bg_search_n.png" alt="" width="40" height="28"></a></td>
<td>999,999,999</td>
<td><span class="freq_3">普通</span></td>
<td>999,999,999</td>
<td>100.0%</td>
<td>&yen;99,999</td>
<td>&yen;9,999,999</td>
<td>&yen;9,999,999</td>
<td><a href="c-2-1-4.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="stop">停止中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td class="alignL">ファシリテーションジャパン</td>
<td class="noWrap">999個 <a href="a-4-2.html"><img src="images/form_bg_search_n.png" alt="" width="40" height="28"></a></td>
<td>999,999,999</td>
<td><span class="freq_1">最高</span></td>
<td>999,999,999</td>
<td>100.0%</td>
<td>&yen;99,999</td>
<td>&yen;9,999,999</td>
<td>&yen;9,999,999</td>
<td><a href="c-2-1-4.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td colspan="13" class="lastCell"><span class="alertB01">カード無し決済合計　&yen;9,999,999</span></td>
</tr>
<tr>
<td colspan="13" class="lastCell">対象期間合計　&yen;9,999,999</td>
</tr>
</table>
</div>

<div id="tabContents02">
<div class="unitB02">
<ul class="formBlockA01">
<li>対象期間</li>
<li><select name="a01" class="formA01 sfr">
<option>今日</option>
<option>昨日</option>
<option>今週（月～）</option>
<option>過去7日間：2015/01/20 〜 2015/01/27</option>
<option>先週（月～日）</option>
<option>今月</option>
<option>過去30日間</option>
<option>先月</option>
<option>全期間</option>
</select></li>
<li><input type="button" class="formA01" value="適用"></li>
</ul>
</div>
<table class="tableA02">
<tr>
<th>承認日時</th>
<th>ステータス</th>
<th class="alignL pl05">広告名</th>
<th>表示回数</th>
<th>表示頻度</th>
<th>クリック数</th>
<th>クリック率</th>
<th>クリック単価</th>
<th>上限予算</th>
<th>費用</th>
<th>プレビュー</th>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="judge">審査中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td>999,999,999</td>
<td><span class="freq_4">低</span></td>
<td>999,999,999</td>
<td>100.0%</td>
<td><span class="alertB01">&yen;99,999</span></td>
<td><span class="alertB01">&yen;9,999,999</span></td>
<td><span class="alertB01">&yen;9,999,999</span></td>
<td><a href="c-2-2-4.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="stop">停止中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td>999,999,999</td>
<td><span class="freq_5">最低</span></td>
<td>999,999,999</td>
<td>100.0%</td>
<td>&yen;99,999</td>
<td>&yen;9,999,999</td>
<td>&yen;9,999,999</td>
<td><a href="c-2-2-4.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="unauthorized">非承認</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td>999,999,999</td>
<td><span class="freq_5">最低</span></td>
<td>999,999,999</td>
<td>100.0%</td>
<td>&yen;99,999</td>
<td>&yen;9,999,999</td>
<td>&yen;9,999,999</td>
<td><a href="c-2-2-4.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="judge">審査中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td>999,999,999</td>
<td><span class="freq_4">低</span></td>
<td>999,999,999</td>
<td>100.0%</td>
<td>&yen;99,999</td>
<td>&yen;9,999,999</td>
<td>&yen;9,999,999</td>
<td><a href="c-2-2-4.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="stop">停止中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td>999,999,999</td>
<td><span class="freq_5">最低</span></td>
<td>999,999,999</td>
<td>100.0%</td>
<td>&yen;99,999</td>
<td>&yen;9,999,999</td>
<td>&yen;9,999,999</td>
<td><a href="c-2-2-4.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="unauthorized">非承認</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td>999,999,999</td>
<td><span class="freq_5">最低</span></td>
<td>999,999,999</td>
<td>100.0%</td>
<td>&yen;99,999</td>
<td>&yen;9,999,999</td>
<td>&yen;9,999,999</td>
<td><a href="c-2-2-4.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="judge">審査中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td>999,999,999</td>
<td><span class="freq_4">低</span></td>
<td>999,999,999</td>
<td>100.0%</td>
<td>&yen;99,999</td>
<td>&yen;9,999,999</td>
<td>&yen;9,999,999</td>
<td><a href="c-2-2-4.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="stop">停止中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td>999,999,999</td>
<td><span class="freq_5">最低</span></td>
<td>999,999,999</td>
<td>100.0%</td>
<td>&yen;99,999</td>
<td>&yen;9,999,999</td>
<td>&yen;9,999,999</td>
<td><a href="c-2-2-4.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="unauthorized">非承認</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td>999,999,999</td>
<td><span class="freq_5">最低</span></td>
<td>999,999,999</td>
<td>100.0%</td>
<td>&yen;99,999</td>
<td>&yen;9,999,999</td>
<td>&yen;9,999,999</td>
<td><a href="c-2-2-4.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="judge">審査中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td>999,999,999</td>
<td><span class="freq_4">低</span></td>
<td>999,999,999</td>
<td>100.0%</td>
<td>&yen;99,999</td>
<td>&yen;9,999,999</td>
<td>&yen;9,999,999</td>
<td><a href="c-2-2-4.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="stop">停止中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td>999,999,999</td>
<td><span class="freq_5">最低</span></td>
<td>999,999,999</td>
<td>100.0%</td>
<td>&yen;99,999</td>
<td>&yen;9,999,999</td>
<td>&yen;9,999,999</td>
<td><a href="c-2-2-4.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="unauthorized">非承認</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td>999,999,999</td>
<td><span class="freq_5">最低</span></td>
<td>999,999,999</td>
<td>100.0%</td>
<td>&yen;99,999</td>
<td>&yen;9,999,999</td>
<td>&yen;9,999,999</td>
<td><a href="c-2-2-4.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td colspan="11" class="lastCell"><span class="alertB01">カード無し決済合計　&yen;9,999,999</span></td>
</tr>
<tr>
<td colspan="11" class="lastCell">対象期間合計　&yen;9,999,999</td>
</tr>
</table>
</div>

<div id="tabContents03">
<table class="tableA02">
<tr>
<th>承認日時</th>
<th>ステータス</th>
<th class="alignL pl05">募集職種</th>
<th class="alignL pl05">募集会社名</th>
<th>新着オプション</th>
<th>詳細ページ</th>
<th>クリック数</th>
<th>掲載期間</th>
<th>費用</th>
<th>プレビュー</th>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="unauthorized">ユーザー削除</span><!--<span class="wait">2014/03/01より掲載</span>--></td>
<td class="alignL">プロジェクトマネージャー</td>
<td class="alignL">ファシリテーションジャパン</td>
<td>インナーパネルハーフ</td>
<td><span class="freq_1">有</span></td>
<td>999,999,999</td>
<td>99週（01/08～02/08）</td>
<td><span class="alertB01">&yen;9,999,999</span></td>
<td><a href="c-2-4-2.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="publish">掲載中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td class="alignL">ファシリテーションジャパン</td>
<td>右カラム</td>
<td><span class="freq_1">有</span></td>
<td>999,999,999</td>
<td>99週（01/08～02/08）</td>
<td>&yen;9,999,999</td>
<td><a href="c-2-4-2.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="edit">編集中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td class="alignL">ファシリテーションジャパン</td>
<td>ロゴバナー</td>
<td><span class="freq_1">有</span></td>
<td>999,999,999</td>
<td>99週（01/08～02/08）</td>
<td>&yen;9,999,999</td>
<td><a href="c-2-4-2.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="unauthorized">非承認</span><!--<span class="wait">2014/03/01より掲載</span>--></td>
<td class="alignL">プロジェクトマネージャー</td>
<td class="alignL">ファシリテーションジャパン</td>
<td>インナーパネルハーフ</td>
<td><span class="freq_1">有</span></td>
<td>999,999,999</td>
<td>99週（01/08～02/08）</td>
<td>&yen;9,999,999</td>
<td><a href="c-2-4-2.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="publish">掲載中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td class="alignL">ファシリテーションジャパン</td>
<td>右カラム</td>
<td><span class="freq_1">有</span></td>
<td>999,999,999</td>
<td>99週（01/08～02/08）</td>
<td>&yen;9,999,999</td>
<td><a href="c-2-4-2.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="edit">編集中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td class="alignL">ファシリテーションジャパン</td>
<td>ロゴバナー</td>
<td><span class="freq_1">有</span></td>
<td>999,999,999</td>
<td>99週（01/08～02/08）</td>
<td>&yen;9,999,999</td>
<td><a href="c-2-4-2.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="unauthorized">ユーザー削除</span><!--<span class="wait">2014/03/01より掲載</span>--></td>
<td class="alignL">プロジェクトマネージャー</td>
<td class="alignL">ファシリテーションジャパン</td>
<td>インナーパネルハーフ</td>
<td><span class="freq_1">有</span></td>
<td>999,999,999</td>
<td>99週（01/08～02/08）</td>
<td>&yen;9,999,999</td>
<td><a href="c-2-4-2.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="publish">掲載中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td class="alignL">ファシリテーションジャパン</td>
<td>右カラム</td>
<td><span class="freq_1">有</span></td>
<td>999,999,999</td>
<td>99週（01/08～02/08）</td>
<td>&yen;9,999,999</td>
<td><a href="c-2-4-2.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="edit">編集中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td class="alignL">ファシリテーションジャパン</td>
<td>ロゴバナー</td>
<td><span class="freq_1">有</span></td>
<td>999,999,999</td>
<td>99週（01/08～02/08）</td>
<td>&yen;9,999,999</td>
<td><a href="c-2-4-2.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="unauthorized">ユーザー削除</span><!--<span class="wait">2014/03/01より掲載</span>--></td>
<td class="alignL">プロジェクトマネージャー</td>
<td class="alignL">ファシリテーションジャパン</td>
<td>インナーパネルハーフ</td>
<td><span class="freq_1">有</span></td>
<td>999,999,999</td>
<td>99週（01/08～02/08）</td>
<td>&yen;9,999,999</td>
<td><a href="c-2-4-2.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="publish">掲載中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td class="alignL">ファシリテーションジャパン</td>
<td>右カラム</td>
<td><span class="freq_1">有</span></td>
<td>999,999,999</td>
<td>99週（01/08～02/08）</td>
<td>&yen;9,999,999</td>
<td><a href="c-2-4-2.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td class="alignC">2015/06/15<br>16:45</td>
<td class="alignC"><span class="edit">編集中</span></td>
<td class="alignL">プロジェクトマネージャー</td>
<td class="alignL">ファシリテーションジャパン</td>
<td>ロゴバナー</td>
<td><span class="freq_1">有</span></td>
<td>999,999,999</td>
<td>99週（01/08～02/08）</td>
<td>&yen;9,999,999</td>
<td><a href="c-2-4-2.html" target="_blank"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
<tr>
<td colspan="10" class="lastCell"><span class="alertB01">カード無し決済合計　&yen;9,999,999</span></td>
</tr>
<tr>
<td colspan="10" class="lastCell">対象期間合計　&yen;9,999,999</td>
</tr>
</table>
</div>
</div><!--/.tabContents-->

</div><!-- /.unitA01 -->
#}
    </form>
    <!-- InstanceEndEditable -->
  </div><!-- / contentsContainer -->
</div><!-- / contents -->