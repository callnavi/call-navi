<div id="contents">
  <div id="contentsContainer">
    <!-- InstanceBeginEditable name="mainContents" -->
    <div class="unitA01">

      <h2 class="headingA01">アカウント一覧</h2>
      <input type="hidden" name="account_id" value="{{account.id}}" />
      <div class="headingB01">
        <h3>アカウント詳細</h3>
      </div>

      <table class="tableB01">
        <tr>
          <th class="ttl">登録日時</th>
          <td class="ipt">{{ account.created }}</td>
        </tr>
        <tr>
          <th class="ttl">会社名</th>
          <td class="ipt">{{ account.company_name }}</td>
        </tr>
        <tr>
          <th class="ttl">会社名フリガナ</th>
          <td class="ipt">{{ account.company_name_kana }}</td>
        </tr>
        <tr>
          <th class="ttl">郵便番号</th>
          <td class="ipt">{{ account.postal_code }}</td>
        </tr>
        <tr>
          <th class="ttl">住所</th>
          <td class="ipt">{{ account.address }}</td>
        </tr>
        <tr>
          <th class="ttl">電話番号</th>
          <td class="ipt">{{ account.telephone }}</td>
        </tr>
        <tr>
          <th class="ttl">FAX番号</th>
          <td class="ipt">{{ account.fax_code }}</td>
        </tr>
        <tr>
          <th class="ttl">会社URL</th>
          <td class="ipt">{{ account.company_url }}</td>
        </tr>
        <tr>
          <th class="ttl">所属部署</th>
          <td class="ipt">{{ account.company_section }}</td>
        </tr>
        <tr>
          <th class="ttl">役職</th>
          <td class="ipt">{{ account.company_position }}</td>
        </tr>
        <tr>
          <th class="ttl">ご担当者名</th>
          <td class="ipt">{{ account.representative }}</td>
        </tr>
        <tr>
          <th class="ttl">ご担当者名フリガナ</th>
          <td class="ipt">{{ account.representative_kana }}</td>
        </tr>
        <tr>
          <th class="ttl">Eメールアドレス<br>（ログインID）</th>
          <td class="ipt">{{ account.email }}</td>
        </tr>
      </table>
    </div><!-- /.unitA01 -->
    <div class="unitA01">

      <div class="headingB01">
        <h3>無料広告(オーガニック検索)状況</h3>
      </div>

      <ul class="formBlockA01">
        <li>対象期間</li>
        <li>
          <select class="formA01 sfr" name="free_scope">
            <option value="today">今日</option>
            <option value="yesterday">昨日</option>
            <option value="week">今週（月～）</option>
            <option value="seven_days">過去7日間：{{ one_week_ago }} 〜 {{ today }}</option>
            <option value="last_week">先週（月～日）</option>
            <option value="month">今月</option>
            <option value="thirty_days">過去30日間</option>
            <option value="last_month">先月</option>
            <option value="all">全期間</option>
          </select>
        </li>
        <li><input type="button" class="formA01" value="適用" id="change_free_scope_for_accounts"></li>
      </ul>

      <table class="tableA02">

        <tr>
          <th>ステータス</th>
          <th class="alignL">募集職種</th>
          <th class="alignL">募集会社名</th>
          
          <th>表示回数</th>
          <th>表示頻度</th>
          <th>クリック数</th>
          <th>クリック率</th>
          
          <th>プレビュー</th>
        </tr>

        {% if free_count > 0 %}
          {% for free_offer in free_offers %}
            {% if free_offer.deleted == 0 %}

              <tr id="free_offer_{{ free_offer.id }}" data-offer_title="{{ free_offer.title }}">
                <td class="alignC">
                  <span class="{% if free_offer.status == "not_allowed" %}unauthorized{% elseif free_offer.status == "judging" %}judge{% elseif free_offer.status == "publishing" %}{% if free_offer.allowed >= six_month_ago %}publish{% else %}stop{% endif %}{% elseif free_offer.status == "editing" %}edit{% elseif free_offer.status == "canceled" %}stop{% endif %}" id="free_offer_status_{{ free_offer.id }}">
                    {% if free_offer.status == "judging" %}
                      審査中
                    {% elseif free_offer.status == "publishing" %}
                         {% if free_offer.allowed >= six_month_ago %}
                                   掲載中
                                {% else %} 
                                   掲載終了
                                {% endif %}
                         {% elseif free_offer.status == "not_allowed" %}
                      非承認
                    
                    {% elseif free_offer.status == "editing" %}
                      編集中
                    {% elseif free_offer.status == "canceled" %}
                      停止中
               
                    {% endif %}
                  </span>
                </td>
                <td class="alignL">{{ free_offer.job_category }}</td>
                <td class="alignL">{{ free_offer.company_name }}</td>
               
                <td id="free_offer_impression_log_{{ free_offer.id }}">{{ free_offer.impression_log }}</td>
                <td class="free_offer_impression_ratio_{{ free_offer.id }}">
                            {% if free_offer.impression_ratio === "false" %}
                                -
                            {% elseif free_offer.impression_ratio >= 80 %}
                                <span class="freq_1">最高</span>
                            {% elseif free_offer.impression_ratio >= 60 %}
                                <span class="freq_2">高</span>
                            {% elseif free_offer.impression_ratio >= 40 %}
                                <span class="freq_3">普通</span>
                            {% elseif free_offer.impression_ratio >= 0 %}
                                <span class="freq_4">低</span>                                
                            {% endif %}
                    
                </td>
                <td id="free_offer_click_log_{{ free_offer.id }}">{{ free_offer.click_log }}</td>
                <td>
                  {% if free_offer.click_ratio === false  %}
                  <span id="free_offer_click_ratio_{{ free_offer.id }}">-</span>%
                  {% else %}
                  <span id="free_offer_click_ratio_{{ free_offer.id }}">{{ free_offer.click_ratio }}</span>%
                  {% endif %}
                </td>
                
                <td><a href="/free/preview?offer_id={{ free_offer.id }}" target="_blank"><img src="/public/admin_images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
              </tr>

            {% endif %}
          {% endfor %}
            
            

        {% else %}

          <tr class="lastCol">
             <td><span class="wait"></span></td>
            <td class="alignL" colspan="12">登録されている無料広告はありません。</td>
          </tr>

        {% endif %}

        

      </table>
    </div><!-- /.unitA01 -->
    
   <div class="unitA01">

      <div class="headingB01">
        <h3>リスティング広告状況</h3>
      </div>

      <ul class="formBlockA01">
        <li>対象期間</li>
        <li>
          <select class="formA01 sfr" name="listing_scope">
            <option value="today">今日</option>
            <option value="yesterday">昨日</option>
            <option value="week">今週（月～）</option>
            <option value="seven_days">過去7日間：{{ one_week_ago }} 〜 {{ today }}</option>
            <option value="last_week">先週（月～日）</option>
            <option value="month">今月</option>
            <option value="thirty_days">過去30日間</option>
            <option value="last_month">先月</option>
            <option value="all">全期間</option>
          </select>
        </li>
        <li><input type="button" class="formA01" value="適用" id="change_listing_scope_for_accounts"></li>
      </ul>

      <table class="tableA02">

        <tr>
          <th>ステータス</th>
          <th class="alignL">募集職種</th>
          <th class="alignL">募集会社名</th>
          <th>検索ワード</th>
          <th>表示回数</th>
          <th>表示頻度</th>
          <th>クリック数</th>
          <th>クリック率</th>
          <th>クリック単価</th>
          <th>上限予算</th>
          <th>費用</th>
          <th>プレビュー</th>
        </tr>

        {% if listing_count > 0 %}
          {% for listing_offer in listing_offers %}
            {% if listing_offer.deleted == 0 %}

              <tr id="listing_offer_{{ listing_offer.id }}" data-offer_title="{{ listing_offer.title }}">
                <td class="alignC">
                  <span class="{% if listing_offer.status == "not_allowed" %}unauthorized{% elseif listing_offer.status == "judging" %}judge{% elseif listing_offer.status == "publishing" %}{% if listing_offer.is_available == 0 %}unauthorized{% else %}publish{% endif %}{% elseif listing_offer.status == "completed" %}stop{% elseif listing_offer.status == "editing" %}edit{% elseif listing_offer.status == "canceled" %}stop{% elseif listing_offer.status == "waiting_card" %}wait{% endif %}" id="listing_offer_status_{{ listing_offer.id }}">
                    {% if listing_offer.status == "judging" %}
                      審査中
                    {% elseif listing_offer.status == "publishing" %}
                         {% if listing_offer.is_available == 0 %}
                            上限予算到達
                         {% else %}    
                            掲載中
                         {% endif %}
                    {% elseif listing_offer.status == "not_allowed" %}
                      非承認
                    {% elseif listing_offer.status == "completed" %}
                      掲載終了
                    {% elseif listing_offer.status == "editing" %}
                      編集中
                    {% elseif listing_offer.status == "canceled" %}
                      停止中
                    {% elseif listing_offer.status == "waiting_card"%}
                       {% if listing_offer.creditcard_id != "" and listing_offer.card_status == "before_approval" %}
                                    カード審査中
                        {% else %}
                                     カード登録待ち
                        {% endif %}
                    {% endif %}
                  </span>
                </td>
                <td class="alignL">{{ listing_offer.job_category }}</td>
                <td class="alignL">{{ listing_offer.company_name }}</td>
                <td class="noWrap">{{ listing_offer.keyword }}個 <a href="/admin/accountListingKeywords?offer_id={{ listing_offer.id }}&account_id={{ listing_offer.account_id }}" target="_blank"><img src="/public/admin_images/form_bg_search_n.png" alt="" width="40" height="28"></a></td>
                <td id="listing_offer_impression_log_{{ listing_offer.id }}">{{ listing_offer.impression_log }}</td>
                <td class="listing_offer_impression_ratio_{{ listing_offer.id }}">
                            {% if listing_offer.impression_ratio == "error" %}
                                -
                            {% elseif listing_offer.impression_ratio >= 80 %}
                                <span class="freq_1">最高</span>
                            {% elseif listing_offer.impression_ratio >= 60 %}
                                <span class="freq_2">高</span>
                            {% elseif listing_offer.impression_ratio >= 40 %}
                                <span class="freq_3">普通</span>
                            {% elseif listing_offer.impression_ratio >= 0 %}
                                <span class="freq_4">低</span>                                
                            {% endif %}
                            
                </td>
                <td id="listing_offer_click_log_{{ listing_offer.id }}">{{ listing_offer.click_log }}</td>
                <td>
                  {% if listing_offer.click_ratio === false  %}
                  <span id="listing_offer_click_ratio_{{ listing_offer.id }}">-</span>%
                  {% else %}
                  <span id="listing_offer_click_ratio_{{ listing_offer.id }}">{{ listing_offer.click_ratio }}</span>%
                  {% endif %}
                </td>
                <td>
                    {% if listing_offer.without_card == 1 %}
                        <font color="#ff0000">&yen;{{ listing_offer.click_price }}</font>
                    {% else %}
                        &yen;{{ listing_offer.click_price }}
                    {% endif %}
                </td>
                <td>
                    {% if listing_offer.without_card == 1 %}
                        <font color="#ff0000">&yen;{{ listing_offer.budget_max }}</font>
                    {% else %}
                        &yen;{{ listing_offer.budget_max }}
                    {% endif %}
                </td>
                <td>
                    {% if listing_offer.without_card == 1 %}
                      <font color="#ff0000">&yen;<span id="listing_offer_expense_{{ listing_offer.id }}">{{ listing_offer.expense }}</span></font>
                    {% else %}
                      &yen;<span id="listing_offer_expense_{{ listing_offer.id }}">{{ listing_offer.expense }}</span>
                    {% endif %}
                </td>
                <td><a href="/listing/preview?offer_id={{ listing_offer.id }}&account_id={{ listing_offer.account_id }}" target="_blank"><img src="/public/admin_images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
              </tr>

            {% endif %}
          {% endfor %}
           {% if listing_cardless_expense_sum !== 0 %}
            <tr>
              <td colspan="12" class="lastCell alertB01"> カードなし決済合計 &yen;<span id="listing_offer_cardless_expense_sum">{{ listing_cardless_expense_sum }}</span></td>
            </tr>
            {% endif %}
            <tr>
              <td colspan="12" class="lastCell">対象期間合計  &yen;<span id="listing_offer_expense_sum">{{ listing_expense_sum }}</span></td>
            </tr>

        {% else %}

          <tr class="lastCol">
             <td><span class="wait"></span></td>
            <td class="alignL" colspan="12">登録されているリスティング広告はありません。</td>
          </tr>

        {% endif %}

        

      </table>
    </div><!-- /.unitA01 -->
    

    

    <div class="unitA01">

      <div class="headingB01">
        <h3>レクタングル広告状況</h3>
      </div>

      <ul class="formBlockA01">
        <li>対象期間</li>
        <li>
          <select class="formA01 sfr" name="rectangle_scope">
            <option value="today">今日</option>
            <option value="yesterday">昨日</option>
            <option value="week">今週（月～）</option>
            <option value="seven_days">過去7日間：{{ one_week_ago }} 〜 {{ today }}</option>
            <option value="last_week">先週（月～日）</option>
            <option value="month">今月</option>
            <option value="thirty_days">過去30日間</option>
            <option value="last_month">先月</option>
            <option value="all">全期間</option>
          </select>
        <li><input type="button" class="formA01" value="適用" id="change_rectangle_scope_for_accounts"></li>
      </ul>

      <table class="tableA02">

        <tr>
          <th>ステータス</th>
          <th class="alignL">広告名</th>
          <th>表示回数</th>
          <th>表示頻度</th>
          <th>クリック数</th>
          <th>クリック率</th>
          <th>クリック単価</th>
          <th>上限予算</th>
          <th>費用</th>
          <th>プレビュー</th>
        </tr>

        {% if rectangle_count > 0 %}
          {% for rectangle_offer in rectangle_offers %}
            {% if rectangle_offer.deleted == 0 %}

              <tr id="rectangle_offer_{{ rectangle_offer.id }}" data-offer_title="{{ rectangle_offer.title }}">
                <td class="alignC">
                  <span class="{% if rectangle_offer.status == "not_allowed" %}unauthorized{% elseif rectangle_offer.status == "judging" %}judge{% elseif rectangle_offer.status == "publishing" %}{% if rectangle_offer.is_available == 0 %}unauthorized{% else %}publish{% endif %}{% elseif rectangle_offer.status == "completed" %}stop{% elseif rectangle_offer.status == "editing" %}edit{% elseif rectangle_offer.status == "canceled" %}stop{% elseif rectangle_offer.status == "waiting_card" %}wait{% endif %}" id="rectangle_offer_status_{{ rectangle_offer.id }}">
                    {% if rectangle_offer.status == "judging" %}
                      審査中
                    {% elseif rectangle_offer.status == "publishing" %}
                         {% if rectangle_offer.is_available == 0 %}
                              上限予算到達
                          {% else %}    
                             掲載中
                           {% endif %}  
                    {% elseif rectangle_offer.status == "not_allowed" %}
                      非承認
                    {% elseif rectangle_offer.status == "completed" %}
                      掲載終了
                    {% elseif rectangle_offer.status == "editing" %}
                      編集中
                    {% elseif rectangle_offer.status == "canceled" %}
                      停止中
                    {% elseif rectangle_offer.status == "waiting_card" %}
                      {% if rectangle_offer.creditcard_id != "" and rectangle_offer.card_status == "before_approval" %}
                        カード審査中
                      {% else %}
                        カード登録待ち
                      {% endif %}
                    {% endif %}
                  </span>
                </td>
                <td class="alignL">{{ rectangle_offer.title }}</td>
                <td id="rectangle_offer_impression_log_{{ rectangle_offer.id }}">{{ rectangle_offer.impression_log }}</td>
                <td class="rectangle_offer_impression_ratio_{{ rectangle_offer.id }}">
                            {% if rectangle_offer.impression_ratio == "error" %}
                                -
                            {% elseif rectangle_offer.impression_ratio >= 80 %}
                                <span class="freq_1">最高</span>
                            {% elseif rectangle_offer.impression_ratio >= 60 %}
                                <span class="freq_2">高</span>
                            {% elseif rectangle_offer.impression_ratio >= 40 %}
                                <span class="freq_3">普通</span>
                            {% elseif rectangle_offer.impression_ratio >= 0 %}
                                <span class="freq_4">低</span>                                
                            {% endif %}
                </td>
                <td id="rectangle_offer_click_log_{{ rectangle_offer.id }}">{{ rectangle_offer.click_log }}</td>
                <td>
                  {% if rectangle_offer.click_ratio === false %}
                  <span id="rectangle_offer_click_ratio_{{ rectangle_offer.id }}">-</span>%
                  {% else %}
                  <span id="rectangle_offer_click_ratio_{{ rectangle_offer.id }}">{{ rectangle_offer.click_ratio }}</span>%
                  {% endif %}
                </td>
                <td>
                    {% if rectangle_offer.without_card == 1 %}
                        <font color="#ff0000">&yen;{{ rectangle_offer.click_price }}</font>
                    {% else %}
                        &yen;{{ rectangle_offer.click_price }}    
                    {% endif %}
                </td>
                <td>
                    {% if rectangle_offer.without_card == 1 %}
                        <font color="#ff0000">&yen;{{ rectangle_offer.budget_max }}</font>
                    {% else %}
                        &yen;{{ rectangle_offer.budget_max }}
                    {% endif %}
                </td>
                 <td>
                    {% if rectangle_offer.without_card == 1 %}
                      <font color="#ff0000">&yen;<span id="rectangle_offer_expense_{{ rectangle_offer.id }}">{{ rectangle_offer.expense }}</span></font>
                    {% else %}
                      &yen;<span id="rectangle_offer_expense_{{ rectangle_offer.id }}">{{ rectangle_offer.expense }}</span>
                    {% endif %}
                </td>
                <td><a href="/rectangle/preview?offer_id={{ rectangle_offer.id }}&account_id={{ rectangle_offer.account_id }}" target="_blank"><img src="/public/admin_images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
              </tr>

            {% endif %}
          {% endfor %}
　　　　　　　{% if rectangle_cardless_expense_sum !== 0 %}
            <tr>
              <td colspan="10" class="lastCell alertB01"> カードなし決済合計 &yen;<span id="rectangle_offer_cardless_expense_sum">{{ rectangle_cardless_expense_sum }}</span></td>
            </tr>
            {% endif %}
            <tr>
               <td colspan="10" class="lastCell">対象期間合計　&yen;<span id="rectangle_offer_expense_sum">{{ rectangle_expense_sum }}</span></td>
            </tr>

        {% else %}

          <tr>
            <td><span class="judge"></span></td>
            <td class="alignL" colspan="10">登録されているレクタングル広告はありません。</td>
          </tr>

        {% endif %}

        <!--MockUp
        <tr>
          <td class="alignC"><span class="publish">掲載中</span></td>
          <td>xxxx求人サイトへの誘導</td>
          <td>999,999,999</td>
          <td><span class="freq_2">高</span></td>
          <td>999,999,999</td>
          <td>100.0%</td>
          <td>&yen;99,999</td>
          <td>&yen;9,999,999</td>
          <td>&yen;9,999,999</td>
          <td><a href="#"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
        </tr>
        <tr>
          <td class="alignC"><span class="publish">掲載中</span></td>
          <td>yyyyyy求人サイトへの誘導</td>
          <td>999,999,999</td>
          <td><span class="freq_5">無</span></td>
          <td>999,999,999</td>
          <td>100.0%</td>
          <td>&yen;99,999</td>
          <td>&yen;9,999,999</td>
          <td>&yen;9,999,999</td>
          <td><a href="#"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
        </tr>
        -->

      </table>
    </div><!-- /.unitA01 -->

    <div class="unitA01">

      <div class="headingB01">
        <h3>ピックアップ広告状況</h3>
      </div>

      <!--MockUp
      <ul class="formBlockA01">
        <li>対象期間</li>
        <li><select name="a01" class="formA01 sfr">
            <option>今日</option>
            <option>昨日</option>
            <option>今週（月～）</option>
            <option>過去7日間：2015/01/20 〜 2015/01/27</option>
            <option>先週（月～日）</option>
            <option>今月</option>
            <option>過去30日間</option>
            <option>先月</option>
            <option>全期間</option>
          </select></li>
        <li><input type="button" class="formA01" value="適用"></li>
      </ul>
      -->

      <table class="tableA02">

        <tr>
          <th>ステータス</th>
          <th class="alignL">募集職種</th>
          <th class="alignL">募集会社名</th>
          <th>新着</th>
          <th>詳細ページ</th>
          <th>クリック数</th>
          <th>掲載期間</th>
          <th>費用</th>
          <th>プレビュー</th>
        </tr>

        {% if pickup_count > 0 %}
          {% for pickup_offer in pickup_offers %}
            {%if pickup_offer.deleted == 0 %}

              <tr id="pickup_offer_{{ pickup_offer.id }}" data-offer_title="{{ pickup_offer.title }}">
                <td class="alignC">
                  <span class="{% if pickup_offer.status == "judging" %}judge{% elseif pickup_offer.status == "publishing" %}publish{% elseif pickup_offer.status == "will_publish" %}wait{% elseif pickup_offer.status == "not_allowed" %}unauthorized{% elseif pickup_offer.status == "completed" %}stop{% elseif pickup_offer.status == "editing" %}edit{% elseif pickup_offer.status == "canceled" %}stop{% elseif pickup_offer.status == "waiting_card" %}wait{% endif %}" id="pickup_offer_status_{{ pickup_offer.id }}">
                    {% if pickup_offer.status == "judging" %}
                      審査中
                    {% elseif pickup_offer.status == "publishing" %}
                      掲載中
                    {% elseif pickup_offer.status == "will_publish" %}
                      {{ pickup_offer.published_from }}より掲載
                    {% elseif pickup_offer.status == "not_allowed" %}
                      非承認
                    {% elseif pickup_offer.status == "completed" %}
                      掲載終了
                    {% elseif pickup_offer.status == "editing" %}
                      編集中
                    {% elseif pickup_offer.status == "canceled" %}
                      停止中
                    {% elseif pickup_offer.status == "waiting_card" %}
                        {% if pickup_offer.creditcard_id != "" and pickup_offer.card_status == "before_approval" %}
                                    カード審査中
                        {% else %}
                                    カード登録待ち
                        {% endif %}
                    {% endif %}
                  </span>
                </td>
                <td class="alignL">{{ pickup_offer.job_category }}</td>
                <td class="alignL">{{ pickup_offer.company_name }}</td>
                <td>
                  {% if pickup_offer.display_area == "inner" %}
                  インナーパネル
                  {% elseif pickup_offer.display_area == "inner_half" %}
                  インナーパネルハーフ
                  {% elseif pickup_offer.display_area == "right_column" %}
                  右カラム
                  {% elseif pickup_offer.display_area == "none" %}
                  無し
                  {% endif %}
                </td>
                <td><span class="freq_1">{% if pickup_offer.detail_content_type == "none" %}無{% else %}有{% endif %}</span></td>
                <td>{{ pickup_offer.click_log }}</td>
                <td>
                 {{ pickup_offer.publish_week }}週{% if pickup_offer.status == "publishing" or pickup_offer.status == "will_publish" or pickup_offer.status == "canceled" %}（{{ pickup_offer.published_from }}～{{ pickup_offer.published_to }}）
                  {% else %}
                  {#（{{ pickup_offer.published_from }}～{{ pickup_offer.published_to }})#}
                  {% endif %}
                </td>
                <td>
                    {% if pickup_offer.without_card == 1 %}
                      <font color="#ff0000">&yen;<span id="pickup_offer_expense_{{ pickup_offer.id }}">{{ pickup_offer.expense }}</span></font>
                    {% else %}
                      &yen;<span id="pickup_offer_expense_{{ pickup_offer.id }}">{{ pickup_offer.expense }}</span>
                    {% endif %}
                </td>
                <td><a href="/pickup/preview?offer_id={{ pickup_offer.id }}&account_id={{ pickup_offer.account_id }}" target="_blank"><img src="/public/admin_images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
              </tr>

            {% endif %}
          {% endfor %}
          {% if pickup_cardless_expense_sum !== 0 %}
          <tr>
            <td colspan="9" class="lastCell alertB01"> カードなし決済合計 &yen;{{ pickup_cardless_expense_sum }}</td>
          </tr>
          {% endif %}
          <tr>
            <td colspan="9" class="lastCell">対象期間合計　&yen;{{ pickup_expense_sum }}</td>
          </tr>
        {% else %}

          <tr>
            <td class="alignL" colspan="9">登録されているピックアップ広告はありません</td>
          </tr>

        {% endif %}

        <!--MockUp
        <tr>
          <td class="alignC"><span class="publish">掲載中</span></td>
          <td>プロジェクトマネージャー</td>
          <td>メタフェイズ</td>
          <td>上段（メイン）</td>
          <td><span class="fontTypeBlue">有</span></td>
          <td>999,999,999</td>
          <td>99週（01/08〜02/08）</td>
          <td>&yen;9,999,999</td>
          <td><a href="#"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
        </tr>
        <tr>
          <td class="alignC"><span class="publish">掲載中</span></td>
          <td>プロジェクトマネージャー</td>
          <td>メタフェイズ</td>
          <td>中段（サブ）</td>
          <td><span class="fontTypeOrange">無</span></td>
          <td>999,999,999</td>
          <td>99週（01/08〜02/08）</td>
          <td>&yen;9,999,999</td>
          <td><a href="#"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
        </tr>
        -->

      </table>
    </div><!-- /.unitA01 -->

    <div class="unitA01">

      <div class="headingB01">
        <h3>クレジットカード決済情報</h3>
      </div>

      <ul class="formBlockA01">
        <li>対象期間</li>
        <li>
          <select name="ago" class="formA01">
            {% for month in months %}
            {% set ago = 0 - loop.index %}
            <option value="{{ ago }}">{{ month }}分</option>
            {% endfor %}
          </select>
        </li>
        <li><input type="button" class="formA01" value="適用" id="change_month" data-account_id="{{ account_id }}"></li>
      </ul>

      <!--MockUp
      <ul class="formBlockA01">
        <li>対象期間</li>
        <li><select name="a01" class="formA01 sfr">
            <option>2015年4月分</option>
            <option>2015年3月分</option>
            <option>2015年2月分</option>
            <option>2015年1月分</option>
          </select></li>
        <li><input type="button" class="formA01" value="適用"></li>
      </ul>
      -->

      <?php $this->partial("admin/creditcardInfo") ?>

    </div><!-- /.unitA01 -->

    <!-- InstanceEndEditable -->
  </div><!-- / contentsContainer -->
</div><!-- / contents -->