<table id="pickup_offers_table" class="tableA02">

  <tbody>

    <tr>
      <th>承認日時</th>
      <th>ステータス</th>
      <th class="alignL pl05">募集職種</th>
      <th class="alignL pl05">募集会社名</th>
      <th>表示エリア</th>
      <th>詳細ページ</th>
      <th>クリック数</th>
      <th>掲載期間</th>
      <th>費用</th>
      <th>プレビュー</th>
    </tr>

    {% if pickup_count > 0 %}
      {% for pickup_offer in pickup_offers %}
         
         <tr id="pickup_offer_{{ pickup_offer.id }}" data-offer_title="{{ pickup_offer.title }}">
           <td>{{pickup_offer.allowed}}</td> 
            <td class="alignC">
              <span class="{% if pickup_offer.deleted == "1" %}unauthorized{% elseif pickup_offer.status == 'judging' %}judge{% elseif pickup_offer.status == 'publishing' %}publish{% elseif pickup_offer.status == 'will_publish' %}wait{% elseif pickup_offer.status == 'not_allowed' %}unauthorized{% elseif pickup_offer.status == 'waiting_card' %}wait{% elseif pickup_offer.status == 'completed' %}stop{% elseif pickup_offer.status == 'editing' %}edit{% elseif pickup_offer.status == 'canceled' %}stop{% endif %}" id="pickup_offer_status_{{ pickup_offer.id }}">
                {% if pickup_offer.deleted == "1" %}
                  ユーザ削除
                {% elseif pickup_offer.status == "judging" %}
                  審査中
                {% elseif pickup_offer.status == "publishing" %}
                  掲載中
                {% elseif pickup_offer.status == "will_publish" %}
                  {{ pickup_offer.published_from }}より掲載
                {% elseif pickup_offer.status == "not_allowed" %}
                  非承認
                {% elseif pickup_offer.status == "waiting_card" %}
                  カード登録待ち
                {% elseif pickup_offer.status == "completed" %}
                  掲載終了
                {% elseif pickup_offer.status == "editing" %}
                  編集中
                {% elseif pickup_offer.status == "canceled" %}
                  一時停止中
                {% endif %}
              </span>
            </td>
            <td class="alignL">{{ pickup_offer.job_category }}</td>
            <td class="alignL">{{ pickup_offer.company_name }}</td>
            <td>
              {% if pickup_offer.display_area == "inner" %}
              インナーパネル
              {% elseif pickup_offer.display_area == "inner_half" %}
              インナーパネルハーフ
              {% elseif pickup_offer.display_area == "right_column" %}
              右カラム
              {% elseif pickup_offer.display_area == "logo_banner" %}
              ロゴバナー
              {% endif %}
            </td>
            <td><span class="freq_1">{% if pickup_offer.detail_content_type == "none" %}無{% else %}有{% endif %}</span></td>
            <td>{{ pickup_offer.click_log }}</td>
            <td>{{ pickup_offer.publish_week }}週（{{ pickup_offer.published_from }}～{{ pickup_offer.published_to }}）</td>
            <td>&yen;{{ pickup_offer.expense }}</td>
            <td><a href="/pickup/preview?offer_id={{ pickup_offer.id }}&account_id={{ pickup_offer.account_id }}" target="_blank"><img src="/public/admin_images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
          </tr>
        
   {% endfor %}

      <tr class="lastCol">
        <td colspan="9" class="lastCell">合計　&yen;{{ pickup_expense_sum }}</td>
      </tr>

    {% else %}

      <tr class="lastCol">
        <td class="alignL" colspan="9">登録されているピックアップ広告はありません</td>
      </tr>

    {% endif %}

  </tbody>
</table>