<table id="listing_offers_table" class="tableA02">

  <tbody>
    <tr>
　　　<th>承認日時</th>
      <th>ステータス</th>
      <th class="alignL pl05">募集職種</th>
      <th class="alignL pl05">募集会社名</th>
      <th>検索ワード</th>
      <th>表示回数</th>
      <th>表示頻度</th>
      <th>クリック数</th>
      <th>クリック率</th>
      <th>クリック単価</th>
      <th>上限予算</th>
      <th>費用</th>
      <th>プレビュー</th>
    </tr>

    {% if listing_count > 0 %}
      {% for listing_offer in listing_offers %}
        
          <tr id="listing_offer_{{ listing_offer.id }}" data-offer_title="{{ listing_offer.title }}">
            <td>{{ listing_offer.allowed }}</td>
            <td class="alignC">
              <span class="{% if listing_offer.deleted== 1 %}unauthorized{% elseif listing_offer.status == 'judging' %}judge{% elseif listing_offer.status == 'publishing' %}{% if listing_offer.is_available == 0 %}unauthorized{% else %}publish{% endif %}{% elseif listing_offer.status == 'not_allowed' or listing_offer.deleted == 1 %}unauthorized{% elseif listing_offer.status == 'waiting_card' %}wait{% elseif listing_offer.status == 'completed' %}stop{% elseif listing_offer.status == 'editing' %}edit{% elseif listing_offer.status == 'canceled' %}stop{% endif %}" id="listing_offer_status_{{ listing_offer.id }}">
                {% if listing_offer.deleted == "1" %}
                  ユーザ削除
                {% elseif listing_offer.status == "judging" %}
                  審査中
                {% elseif listing_offer.status == "publishing" %}
                    {% if listing_offer.is_available == 0 %}
                      上限予算到達
　　　　　　　　　　　　{% else %}
                      掲載中
                    {% endif %}
                {% elseif listing_offer.status == "not_allowed" %}
                  非承認
                {% elseif listing_offer.status == "waiting_card" %}
                  カード登録待ち
                {% elseif listing_offer.status == "completed" %}
                  掲載終了
                {% elseif listing_offer.status == "editing" %}
                  編集中
                {% elseif listing_offer.status == "canceled" %}
                  一時停止中
                
                {% endif %}
              </span>
            </td>
            <td class="alignL">{{ listing_offer.job_category }}</td>
            <td class="alignL">{{ listing_offer.company_name }}</td>
            <td class="noWrap">{{ listing_offer.keyword }}個 <a href="/admin/allowedListingKeywords?offer_id={{ listing_offer.id }}&account_id={{ listing_offer.account_id }}" target="_blank"><img src="/public/admin_images/form_bg_search_n.png" alt="" width="40" height="28"></a></td>
            <td id="listing_offer_impression_log_{{ listing_offer.id }}">{{ listing_offer.impression_log }}</td>
            <td class="listing_offer_impression_ratio_{{ listing_offer.id }}">
                            {% if listing_offer.impression_ratio == "error" %}
                                -
                            {% elseif listing_offer.impression_ratio >= 80 %}
                                <span class="freq_1">最高</span>
                            {% elseif listing_offer.impression_ratio >= 60 %}
                                <span class="freq_2">高</span>
                            {% elseif listing_offer.impression_ratio >= 40 %}
                                <span class="freq_3">普通</span>
                            {% elseif listing_offer.impression_ratio >= 0 %}
                                <span class="freq_4">低</span>                                
                            {% endif %}
            </td>
            <td id="listing_offer_click_log_{{ listing_offer.id }}">{{ listing_offer.click_log }}</td>
            <td>
              {% if listing_offer.click_ratio === false  %}
                <span id="listing_offer_click_ratio_{{ listing_offer.id }}">-</span>%
              {% else %}
                <span id="listing_offer_click_ratio_{{ listing_offer.id }}">{{ listing_offer.click_ratio }}</span>%
              {% endif %}
            </td>
            <td>&yen;{{ listing_offer.click_price }}</td>
            <td>&yen;{{ listing_offer.budget_max }}</td>
            <td>&yen;<span id="listing_offer_expense_{{ listing_offer.id }}">{{ listing_offer.expense }}</span></td>
            <td><a href="/listing/preview?offer_id={{ listing_offer.id }}&account_id={{ listing_offer.account_id }}" target="_blank"><img src="/public/admin_images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
          </tr>

        
      {% endfor %}

      <tr class="lastCol">
        <td colspan="12" class="lastCell">対象期間合計  &yen;<span id="listing_offer_expense_sum">{{ listing_expense_sum }}</span></td>
      </tr>

    {% else %}

      <tr class="lastCol">
        <!--<td><span class="wait"></span></td>-->
        <td class="alignL" colspan="12">登録されているリスティング広告はありません。</td>
      </tr>

    {% endif %}

  </tbody>
</table>