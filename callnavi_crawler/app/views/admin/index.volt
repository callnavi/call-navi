<div id="contents">
  <div id="contentsContainer">
    <!-- InstanceBeginEditable name="mainContents" -->
    <div class="unitA01">

      <h2 class="headingA01">求人広告審査</h2>

      <div class="headingB01">
        <h3>無料広告(オーガニック検索)</h3>
      </div>

      <table class="tableA01">

        <tr>
          <th>操作</th>
          <th class="alignL">審査受付日時</th>
          <th class="alignL">募集職種</th>
          <th class="alignL">募集会社名</th>
          <th class="alignL">アカウント(請求先)</th>
          <th>プレビュー</th>
        </tr>
        
        {% if free_count > 0 %}
          {% for free_offer in free_offers %}
            
            <tr id="free_offer_{{ free_offer.id }}" data-offer_title="{{ free_offer.title }}">
              <td>

                <div class="status before_change" id="free_offer_display_{{ free_offer.id }}">
                  <span class="ttl"><img src="/public/admin_images/table_select_00_1.png" width="44" height="28" alt=""></span>
                  <ul>
                    <li id="free_offer_allow_{{ free_offer.id }}" data-offer_id="{{ free_offer.id }}" class="status_01"><a href="javascript:void(0);" class="st1">承認</a></li>
                   
                    <li class="status_04"><a href="/admin/rejectMessage?account_id={{ free_offer.account_id }}&offer_type=free&offer_id={{ free_offer.id }}" class="st4">非承認</a></li>
                  </ul>
                </div>

              </td>
              <td class="alignL">{{ free_offer.accepted }}</td>
              <td class="alignL">{{free_offer.job_category}}</td>
              <td class="alignL">{{ free_offer.company_name }}</td>
              <td class="alignL">{{free_offer.account}}</td>
              <td><a href="/free/preview?offer_id={{ free_offer.id }}" target="_blank"><img src="/public/admin_images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
            </tr>
            


          {% endfor %}
           
            
            <tr id="no_offers" class="lastCol" style="display: none">
              <td>
                <div class="status">
                  <span class="ttl"></span>
                </div>
              </td>
              <td class="alignL" colspan="6">審査待ちの無料広告はありません。</td>
            </tr>

        {% else %}
          <tr class="lastCol">
            <td>
              <div class="status">
                <span class="ttl"></span>
              </div>
            </td>
            <td class="alignL" colspan="6">審査待ちの無料広告はありません。</td>
          </tr>
        {% endif %}
      
{#
<tr>
<td>
<div class="status">
<span class="ttl"><img src="images/table_select_00_1.png" width="44" height="28" alt=""></span>
<ul>
<li class="status_01"><a href="javascript:void(0);" class="st1">承認</a></li>
<li class="status_04"><a href="javascript:void(0);" class="st4">非承認</a></li>
</ul>
</div>
</td>
<td class="alignL">2015/06/10 12:10</td>
<td class="alignL">プロジェクトマネージャー</td>
<td class="alignL">株式会社メタフェイズ</td>
<td class="alignL">株式会社メタフェイズ</td>
<td><a href="#"><img src="images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
#}
      </table>
    </div><!-- /.unitA01 -->
 
    <div class="unitA01">

      <h2 class="headingA01">求人広告審査</h2>

      <div class="headingB01">
        <h3>審査待ちの求人広告一覧（リスティング広告）</h3>
      </div>

      <table class="tableA01">

        <tr>
          <th>操作</th>
          <th>審査受付日時</th>
          <th class="alignL">募集職種</th>
          <th class="alignL">募集会社名</th>
          <th class="alignL">アカウント（請求先）</th>
          <th>クリック単価</th>
          <th>上限予算</th>
          <th>プレビュー</th>
        </tr>

        {% if listing_count > 0 %}
          {% for listing_offer in listing_offers %}

            <tr id="listing_offer_{{ listing_offer.id }}" data-offer_title="{{ listing_offer.title }}">
              <td>

                <div class="status before_change" id="listing_offer_display_{{ listing_offer.id }}">
                  <span class="ttl"><img src="/public/admin_images/table_select_00_1.png" width="44" height="28" alt=""></span>
                  <ul>
                    <li id="listing_offer_allow_{{ listing_offer.id }}" data-offer_id="{{ listing_offer.id }}" class="status_01"><a href="javascript:void(0);" class="st1">承認</a></li>
                    <li id="listing_offer_wo_allow_{{ listing_offer.id }}" data-offer_id="{{ listing_offer.id }}" class="status_03"><a href="javascript:void(0);" class="st3">カードなし承認</a></li>
                    <li class="status_04"><a href="/admin/rejectMessage?account_id={{ listing_offer.account_id }}&offer_type=listing&offer_id={{ listing_offer.id }}" class="st4">非承認</a></li>
                  </ul>
                </div>

              </td>
              <td>{{ listing_offer.accepted }}</td>
              <td class="alignL">{{ listing_offer.job_category }}</td>
              <td class="alignL">{{ listing_offer.company_name }}</td>
              <td class="alignL">{{ listing_offer.account }}</td>
              <td>&yen;{{ listing_offer.click_price }}</td>
              <td>&yen;{{ listing_offer.budget_max }}</td>
              <td><a href="/listing/preview?offer_id={{ listing_offer.id }}&account_id={{ listing_offer.account_id }}" target="_blank"><img src="/public/admin_images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
            </tr>

          {% endfor %}

            <tr id="no_offers" class="lastCol" style="display: none">
              <td>
                <div class="status">
                  <span class="ttl"></span>
                </div>
              </td>
              <td class="alignL" colspan="8">審査待ちのリスティング広告はありません。</td>
            </tr>

        {% else %}
          <tr class="lastCol">
            <td>
              <div class="status">
                <span class="ttl"></span>
              </div>
            </td>
            <td class="alignL" colspan="8">審査待ちのリスティング広告はありません。</td>
          </tr>
        {% endif %}

      </table>
    </div><!-- /.unitA01 -->

    <div class="unitA01">

      <div class="headingB01">
        <h3>審査待ちの求人広告一覧（レクタングル広告）</h3>
      </div>

      <table class="tableA01">

        <tr>
          <th>操作</th>
          <th>審査受付日時</th>
          <th class="alignL">広告名</th>
          <th class="alignL">アカウント（請求先）</th>
          <th>クリック単価</th>
          <th>上限予算</th>
          <th>プレビュー</th>
        </tr>

        {% if rectangle_count > 0 %}
          {% for rectangle_offer in rectangle_offers %}

            <tr id="rectangle_offer_{{ rectangle_offer.id }}" data-offer_title="{{ rectangle_offer.title }}">
              <td>

                <div class="status before_change" id="rectangle_offer_display_{{ rectangle_offer.id }}">
                  <span class="ttl"><img src="/public/admin_images/table_select_00_1.png" width="44" height="28" alt=""></span>
                  <ul>
                    <li id="rectangle_offer_allow_{{ rectangle_offer.id }}" data-offer_id="{{ rectangle_offer.id }}" class="status_01"><a href="javascript:void(0);" class="st1">承認</a></li>
                    <li id="rectangle_offer_wo_allow_{{ rectangle_offer.id }}" data-offer_id="{{ rectangle_offer.id }}" class="status_03"><a href="javascript:void(0);" class="st3">カードなし承認</a></li>
                    <li class="status_04"><a href="/admin/rejectMessage?account_id={{ rectangle_offer.account_id }}&offer_type=rectangle&offer_id={{ rectangle_offer.id }}" class="st4">非承認</a></li>
                  </ul>
                </div>

                <!-- MockUp
                <div class="status">
                  <span class="ttl"><img src="/public/admin_images/table_select_00_1.png" width="44" height="28" alt=""></span>
                  <ul>
                    <li><a href="javascript:void(0);" class="st1">承認</a></li>
                    <li><a href="javascript:void(0);" class="st2">編集</a></li>
                    <li><a href="javascript:void(0);" class="st3">カード無し承認</a></li>
                    <li><a href="javascript:void(0);" class="st4">非承認</a></li>
                  </ul>
                </div>
                -->

              </td>
              <td>{{ rectangle_offer.accepted }}</td>
              <td class="alignL">{{ rectangle_offer.title }}</td>
              <td class="alignL">{{ rectangle_offer.account }}</td>
              <td>&yen;{{ rectangle_offer.click_price }}</td>
              <td>&yen;{{ rectangle_offer.budget_max }}</td>
              <td><a href="/rectangle/preview?offer_id={{ rectangle_offer.id }}&account_id={{ rectangle_offer.account_id }}" target="_blank"><img src="/public/admin_images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
            </tr>

          {% endfor %}
          <tr id="no_offers" class="lastCol" style="display: none">
            <td><span class="judge"></span></td>
            <td class="alignL" colspan="7">審査待ちのレクタングル広告はありません。</td>
          </tr>
        {% else %}

          <tr class="lastCol">
            <td><span class="judge"></span></td>
            <td class="alignL" colspan="7">審査待ちのレクタングル広告はありません。</td>
          </tr>

        {% endif %}

      </table>
    </div><!-- /.unitA01 -->

    <div class="unitA01">

      <div class="headingB01">
        <h3>審査待ちの求人広告一覧（ピックアップ広告）</h3>
      </div>

      <table class="tableA01">

        <tr>
          <th>操作</th>
          <th>審査受付日時</th>
          <th class="alignL">募集職種</th>
          <th class="alignL">募集会社名</th>
          <th class="alignL">アカウント（請求先）</th>
          <th>新着</th>
          <th>詳細ページ</th>
          <th>掲載期間</th>
          <th>費用</th>
          <th>プレビュー</th>
        </tr>

        {% if pickup_count > 0 %}
          {% for pickup_offer in pickup_offers %}

            <tr id="pickup_offer_{{ pickup_offer.id }}" data-offer_title="{{ pickup_offer.title }}">
              <td>

                <div class="status before_change" id="pickup_offer_display_{{ pickup_offer.id }}">
                  <span class="ttl"><img src="/public/admin_images/table_select_00_1.png" width="44" height="28" alt=""></span>
                  <ul>
                    <li id="pickup_offer_allow_{{ pickup_offer.id }}" data-offer_id="{{ pickup_offer.id }}" class="status_01"><a href="javascript:void(0);" class="st1">承認</a></li>
                    <li id="pickup_offer_wo_allow_{{ pickup_offer.id }}" data-offer_id="{{ pickup_offer.id }}" class="status_03"><a href="javascript:void(0);" class="st3">カードなし承認</a></li>
                    <li class="status_04"><a href="/admin/rejectMessage?account_id={{ pickup_offer.account_id }}&offer_type=pickup&offer_id={{ pickup_offer.id }}" class="st4">非承認</a></li>
                  </ul>
                </div>

              </td>
              <td>{{ pickup_offer.accepted }}</td>
              <td class="alignL">{{ pickup_offer.job_category }}</td>
              <td class="alignL">{{ pickup_offer.company_name }}</td>
              <td class="alignL">{{ pickup_offer.account }}</td>
              <td>
                {% if pickup_offer.display_area == "inner" %}
                  インナーパネル
                {% elseif pickup_offer.display_area == "inner_half" %}
                  インナーパネルハーフ
                {% elseif pickup_offer.display_area == "right_column" %}
                  右カラム
                {% elseif pickup_offer.display_area == "none" %}
                   無し
                {% endif %}
              </td>
              <td>{% if pickup_offer.detail_content_type == "none" %}<span class="freq_1">無</span>{% else %}<span class="freq_4">有</span>{% endif %}</td>
              <td>
              {{ pickup_offer.publish_week }}週 {#（{{ pickup_offer.published_from }}～{{ pickup_offer.published_to }}）#}

              </td>
              <td>&yen;{{ pickup_offer.expense }}</td>
              <td><a href="/pickup/preview?offer_id={{ pickup_offer.id }}&account_id={{ pickup_offer.account_id }}" target="_blank"><img src="/public/admin_images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
            </tr>

          {% endfor %}
          <tr id="no_offers" class="lastCol" style="display: none">
            <td><span class="judge"></span></td>
            <td class="alignL" colspan="10">審査待ちのピックアップ広告はありません。</td>
          </tr>
        {% else %}

          <tr class="lastCol">
            <td><span class="judge"></span></td>
            <td class="alignL" colspan="10">審査待ちのピックアップ広告はありません。</td>
          </tr>

        {% endif %}

      </table>
    </div><!-- /.unitA01 -->

    <!-- InstanceEndEditable -->
  </div><!-- / contentsContainer -->
</div><!-- / contents -->
