<div id="contents">
  <div id="contentsContainer">
    <!-- InstanceBeginEditable name="mainContents" -->
    <form method="post" action="#" enctype="multipart/form-data">
      <div class="unitA01">
        <h2 class="headingA01">クレジットカード決済情報</h2>
        <div class="headingB01">
          <h3>クレジットカード請求明細</h3>
        </div>
        <p class="alertB01 floatL marginB15">決済日時が末日集計のため、前月分までしか表示されません。<br>当月の請求予定金額は、<a href="/admin/accountLists">アカウント一覧のページ</a>をご覧ください。</p>

        <ul class="formBlockA01 marginT05">
          <li id="search_desc">アカウント名検索</li>
          <li><input id="search_term" type="text" name="a00" class="inputA09"></li>
          <li class="pr30"><input id="search_button" type="button" class="formA01" value="検索"></li>
          <li>対象期間</li>
          <li>
            <select name="ago" class="formA01">
              {% for month in months %}
              {% set ago = 0 - loop.index %}
              <option value="{{ ago }}">{{ month }}分</option>
              {% endfor %}
            </select>
          </li>
          <li><input type="button" class="formA01" value="適用" id="change_month"></li>
        </ul>
        <?php $this->partial("admin/creditcardInfo") ?>
      </div><!-- /.unitA01 -->
    </form>
    <!-- InstanceEndEditable -->
  </div><!-- / contentsContainer -->
</div><!-- / contents -->