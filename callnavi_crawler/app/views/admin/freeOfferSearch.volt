 <table id="free_offers_table" class="tableA02">
            <tr>
            <th>承認日時</th>
            <th>ステータス</th>
            <th class="alignL pl05">募集職種</th>
            <th class="alignL pl05">募集会社名</th>
            <th>表示回数</th>
            <th>表示頻度</th>
            <th>クリック数</th>
            <th>クリック率</th>
            <th>プレビュー</th>
            </tr>
                {% if free_count > 0 %}
                  {% for free_offer in free_offers %}

                      <tr id="free_offer_{{ free_offer.id }}" data-offer_title="{{ free_offer.title }}">
                        <td>{{ free_offer.allowed }}</td>
                        <td class="alignC">
                          <span class="{% if free_offer.deleted == 1 %}unauthorized{% elseif free_offer.status == 'judging' %}judge{% elseif free_offer.status == 'publishing' %}publish{% elseif free_offer.status == 'not_allowed' %}unauthorized{% elseif free_offer.status == 'editing' %}edit{% elseif free_offer.status == 'canceled' %}stop{% endif %}" id="free_offer_status_{{ free_offer.id }}">
                            {% if free_offer.deleted == 1 %}
                              ユーザー削除
                            
                            {% elseif free_offer.status == "publishing" %}

                                   掲載中
                                  
                            {% elseif free_offer.status == "not_allowed" %}
                              非承認
                           
                              
                            {% elseif free_offer.status == "canceled" %}
                              一時停止中
                            {% elseif free_offer.status == "editing" %}
                              編集中  
                            {% endif %}
                          </span>
                        </td>
                        <td class="alignL">{{ free_offer.job_category }}</td>
                        <td class="alignL">{{ free_offer.company_name }}</td>
                        
                        <td id="free_offer_impression_log_{{ free_offer.id }}">{{ free_offer.impression_log }}</td>
                        <td class="free_offer_impression_ratio_{{ free_offer.id }}">
                            {% if free_offer.impression_ratio == "error" %}
                                -
                            {% elseif free_offer.impression_ratio >= 80 %}
                                <span class="freq_1">最高</span>
                            {% elseif free_offer.impression_ratio >= 60 %}
                                <span class="freq_2">高</span>
                            {% elseif free_offer.impression_ratio >= 40 %}
                                <span class="freq_3">普通</span>
                            {% elseif free_offer.impression_ratio >= 0 %}
                                <span class="freq_4">低</span>                                
                            {% endif %}
                        </td>
                        <td id="free_offer_click_log_{{ free_offer.id }}">{{ free_offer.click_log }}</td>
                        <td>
                          {% if free_offer.click_ratio === false  %}
                            <span id="free_offer_click_ratio_{{ free_offer.id }}">-</span>%
                          {% else %}
                            <span id="free_offer_click_ratio_{{ free_offer.id }}">{{ free_offer.click_ratio }}</span>%
                          {% endif %}
                        </td>

                        
                        
                        <td><a href="/free/preview?offer_id={{ free_offer.id }}" target="_blank"><img src="/public/admin_images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
                      </tr>

                  {% endfor %}
                  
                  

                {% else %}

                  <tr>
                    <!--<td><span class="wait"></span></td>-->
                    <td class="alignL" colspan="13">登録されている無料広告はありません。</td>
                  </tr>

                {% endif %}
  </table>
