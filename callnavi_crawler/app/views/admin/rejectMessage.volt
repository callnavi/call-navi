<div id="contents">
  <div id="contentsContainer">
    <!-- InstanceBeginEditable name="mainContents" -->
    <div class="unitA01">
      <h2 class="headingA01">求人広告審査</h2>
      <p class="alignC marginB15">非承認理由を選択してください。</p>
      <form>
        <div class="boxA02 marginB15">
          <ul class="listD01">
            <li><input type="checkbox" id="reason01"><label for="reason01">事業内容・募集内容が法令に抵触するため</label></li>
            <li><input type="checkbox" id="reason02"><label for="reason02">均等な雇用機会を損なうおそれがあると認められるため</label></li>
            <li><input type="checkbox" id="reason03"><label for="reason03">社会倫理・社会秩序に反すると認められるため</label></li>
            <li><input type="checkbox" id="reason04"><label for="reason04">利用者に不利益を与えるため</label></li>
            <li><input type="checkbox" id="reason05"><label for="reason05">予め提供する意思のない労働条件を表示するため</label></li>
            <li><input type="checkbox" id="reason06"><label for="reason06">その他</label><br>
              <textarea id="comment"></textarea>
              <p class="alertB01 marginOff marginT05">※半角カタカナは使用しないでください。</p></li>
          </ul>
          <div class="alignC">
            <p id="allow_button" class="btnA05">非承認メールを送信</p>
            <input type="hidden" id="account_id" value="{{ account_id }}">
            <input type="hidden" id="offer_id" value="{{ offer_id }}">
            <input type="hidden" id="offer_type" value="{{ offer_type }}">
          </div>
        </div>
      </form>
    </div><!-- /.unitA01 -->

    <!-- InstanceEndEditable -->
  </div><!-- / contentsContainer -->
</div><!-- / contents -->