<div id="creditcardInfo" class="clearfix">
  <table id="credit_table" class="tableB02">

      <tr>
        <th>決済日時</th>
        <th>アカウント（請求先）</th>
        <th>広告形態</th>
        <th>募集職種</th>
        <th>募集会社名</th>
        <th>費用</th>
        <th></th>
      </tr>

    {% for offer in offers %}
      {% if offer.expense > 0 %}
        {% if offer.without_card == 0 %}
          <tr>
            <td><span class="pay_year">{{ pay_year }}</span>/<span class="pay_month">{{ pay_month }}</span>/31</td>
            <td>{{ offer.charge_to }}</td>
            <td>
              {% if offer.type == "listing" %}
                リスティング広告
              {% elseif offer.type == "rectangle" %}
                レクタングル広告
              {% elseif offer.type == "pickup" %}
                ピックアップ広告
              {% endif %}
            </td>
            <td>
              {% if offer.job_category != '' %}
                {{ offer.job_category }}
              {% else %}
                -
              {% endif %}
            </td>
            <td>
              {% if offer.company_name != '' %}
                {{ offer.company_name }}
              {% else %}
                -
              {% endif %}
            </td>
            <td>&yen;<span id="{{ offer.type }}_expense_{{ offer.id }}">{{ offer.expense }}</span></td>
            <td><a href="/admin/accountListsContent?account_id={{ offer.account_id }}"><img src="/public/admin_images/form_bg_search_n.png" alt=詳細情報の確認"></a></td>
          </tr>
        {% endif %}
      {% endif %}
    {% endfor %}
    <tr>
      <td colspan="7" class="lastCell">対象期間合計　&yen;<span id="expense_sum">{{ expense_sum }}</span></td>
    </tr>
    <tr>
      <td colspan="7" class="lastCell"><span class="fnor">消費税（8％）　&yen;{{ tax }}</span></td>
    </tr>
    <tr>
      <td colspan="7" class="lastCell">当月ご請求金額合計　&yen;{{ expense_sum + tax }}</td>
    </tr>

    <!--MockUp
    <tr>
      <td>2015/01/27</td>
      <td>株式会社メタフェイズ</td>
      <td>リスティング広告</td>
      <td>プロジェクトマネージャー</td>
      <td>メタフェイズ</td>
      <td>&yen;9,999,999</td>
    </tr>
    <tr>
      <td>2015/01/27</td>
      <td>株式会社メタフェイズ</td>
      <td>ピックアップ広告</td>
      <td>プロジェクトマネージャー</td>
      <td>メタフェイズ</td>
      <td>&yen;9,999,999</td>
    </tr>
    <tr>
      <td>2015/01/27</td>
      <td>株式会社メタフェイズ</td>
      <td>リスティング広告</td>
      <td>プロジェクトマネージャー</td>
      <td>転職TV</td>
      <td>&yen;9,999,999</td>
    </tr>
    <tr>
      <td colspan="6" class="lastCell">対象期間合計　&yen;9,999,999</td>
    </tr>
    -->

  </table>
  <script>
    $('.tableA01 tr, .tableA02 tr, .tableB02 tr').mouseover(function(){
                    $('img',this).each(function(){
                            if($(this).attr('src').indexOf('_n.')){
                                    $(this).attr('src',$(this).attr('src').replace("_n", "_r"));
                            }
                    });
            });
            $('.tableA01 tr, .tableA02 tr, .tableB02 tr').mouseout(function(){
                    $('img',this).each(function(){
                            if($(this).attr('src').indexOf('_r.')){
                                    $(this).attr('src',$(this).attr('src').replace("_r", "_n"));
                            }
                    });
            });
  </script>
</div>