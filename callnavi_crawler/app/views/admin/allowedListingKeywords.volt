<div id="contents">
  <div id="contentsContainer">
    <!-- InstanceBeginEditable name="mainContents" -->
    <div class="unitA01">
      <h2 class="headingA01">審査済み広告一覧</h2>
      <div class="headingB01">
        <h3>リスティング広告</h3>
      </div>
      <div class="clearfix">

        <ul class="formBlockA01">
          <li>対象期間</li>
          <li><select class="formA01 sfr" name="keyword_scope">
              <option value="today">今日</option>
              <option value="yesterday">昨日</option>
              <option value="week">今週（月～）</option>
              <option value="seven_days">過去7日間：{{ one_week_ago }} 〜 {{ today }}</option>
              <option value="last_week">先週（月～日）</option>
              <option value="month">今月</option>
              <option value="thirty_days">過去30日間</option>
              <option value="last_month">先月</option>
              <option value="all">全期間</option>
            </select>
          </li>
          <li><input type="button" class="formA01" value="適用" id="change_keyword_scope" data-offer_id="{{ offer_id }}"></li>
        </ul>

      </div>
      <div class="boxC01">{{ job_category }}</div>

      <table class="tableB02">

        <tr>
          <th class="alignL">検索キーワード</th>
          <th>表示回数</th>
          <th>クリック数</th>
          <th>クリック率</th>
          <th>クリック単価</th>
          <th>費用</th>
        </tr>

        {% if keywords_count > 0 %}
          {% for keyword in keywords %}
            {% if keyword.deleted == 0 %}
              <tr id="keyword_{{ keyword.id }}" data-keywords="{{ keyword.keywords }}">
                <td class="alignL">{{ keyword.keywords }}</td>
                <td id="keyword_impression_log_{{ keyword.id }}">{{ keyword.impression_log }}</td>
                <td id="keyword_click_log_{{ keyword.id }}">{{ keyword.click_log }}</td>
                <td>
                  {% if keyword.click_ratio === false  %}
                  <span id="keyword_click_ratio_{{ keyword.id }}">-</span>%
                  {% else %}
                  <span id="keyword_click_ratio_{{ keyword.id }}">{{ keyword.click_ratio }}</span>%
                  {% endif %}
                </td>
                  {% if without_card==0 %}
                  <td>&yen;{{ keyword.click_price }}</td>
                  {% else %}
                  <td><font color="#ff0000">&yen;{{ keyword.click_price }}</front></td>
                  {% endif %} 
                {% if without_card==0 %}
                <td>&yen;<span id="keyword_expense_{{ keyword.id }}">{{ keyword.expense }}</span></td>
                {% else %}
                <td><font color="#ff0000">&yen;<span id="keyword_expense_{{ keyword.id }}">{{ keyword.expense }}</span></front></td>
                {% endif %}
              </tr>
            {% endif %}
          {% endfor %}
<!--
          <tr class="lastCol">
            {% if without_card == 0 %}
            <td colspan="6" class="lastCell pr20">対象期間合計  &yen;<span id="keyword_expense_sum">{{ expense_sum }}</span></td>
            {% else %}
            <td colspan="6" class="lastCell pr20" id="keyword_lastCol"> <font color="#ff0000">カード無し決済合計 &yen;<span id="keyword_expense_sum">{{ expense_sum }}</span></font></td>
            {% endif %}
          </tr>
-->
        {% else %}
          <tr class="lastCol">
            <td colspan="6" class="alignL">登録されているキーワードはありません。</td>
          </tr>
        {% endif %}

        <!--
        <tr>
          <td>
            <div class="status">
              <span class="ttl"><img src="/public/admin_images/table_select_00_1.png" width="44" height="28" alt=""></span>
              <ul>
                <li><a href="javascript:void(0);" class="st1">掲載</a></li>
                <li><a href="javascript:void(0);" class="st2">編集</a></li>
                <li><a href="javascript:void(0);" class="st3">停止</a></li>
                <li><a href="javascript:void(0);" class="st4">削除</a></li>
              </ul>
            </div>
          </td>
          <td class="alignL">システムエンジニア　プログラマー　東京都　新宿区</td>
          <td>999,999,999</td>
          <td>9,999,999</td>
          <td>100.0%</td>
          <td>&yen;99,999</td>
          <td>&yen;9,999,999</td>
        </tr>
        <tr>
          <td>
            <div class="status">
              <span class="ttl"><img src="/public/admin_images/table_select_00_1.png" width="44" height="28" alt=""></span>
              <ul>
                <li><a href="javascript:void(0);" class="st1">掲載</a></li>
                <li><a href="javascript:void(0);" class="st2">編集</a></li>
                <li><a href="javascript:void(0);" class="st3">停止</a></li>
                <li><a href="javascript:void(0);" class="st4">削除</a></li>
              </ul>
            </div>
          </td>
          <td class="alignL">プログラマー　ゲーム　アプリ</td>
          <td>999,999,999</td>
          <td>9,999,999</td>
          <td>100.0%</td>
          <td>&yen;99,999</td>
          <td>&yen;9,999,999</td>
        </tr>
        <tr>
          <td>
            <div class="status">
              <span class="ttl"><img src="/public/admin_images/table_select_00_1.png" width="44" height="28" alt=""></span>
              <ul>
                <li><a href="javascript:void(0);" class="st1">掲載</a></li>
                <li><a href="javascript:void(0);" class="st2">編集</a></li>
                <li><a href="javascript:void(0);" class="st3">停止</a></li>
                <li><a href="javascript:void(0);" class="st4">削除</a></li>
              </ul>
            </div>
          </td>
          <td class="alignL">システムエンジニア　東京都　新宿区</td>
          <td>999,999,999</td>
          <td>9,999,999</td>
          <td>100.0%</td>
          <td>&yen;99,999</td>
          <td>&yen;9,999,999</td>
        </tr>
        <tr>
          <td colspan="7" class="lastCell pr20">対象期間合計　&yen;9,999,999</td>
        </tr>
        -->

      </table>

    </div><!-- /.unitA01 -->

    <!-- InstanceEndEditable -->
  </div><!-- / contentsContainer -->
</div><!-- / contents -->