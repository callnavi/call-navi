<div id="contents">
<div id="contentsContainer">
<!-- InstanceBeginEditable name="mainContents" -->
<form method="post" action="#">
<div class="unitA01">
<h2 class="headingA01">クレジットカード情報の登録・変更</h2>

<p class="alignC marginB20">ありがとうございました。</p>
<p class="alignC marginB20">クレジットカード情報を登録してセキュリティコードの認証を行っています。<br>
セキュリティコードの認証には10分～30分程度かかる見込みです。</p>
<p class="alignC marginB30">セキュリティコードが認証された場合、ステータスが『<span class="fontTypeB01">カード審査中</span>』の広告は<br>
自動的に掲載を開始する予定です。</p>

<p class="alignC"><a href="/customer/index">求人広告管理（HOME）へ戻る</a></p>

</div><!-- /.unitA01 -->
</form>
<!-- InstanceEndEditable -->
</div><!-- / contentsContainer -->
</div><!-- / contents -->
