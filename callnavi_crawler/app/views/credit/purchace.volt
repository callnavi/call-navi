<div id="contents">
<div id="contentsContainer">
<!-- InstanceBeginEditable name="mainContents" -->
<form method="post" action="#">
<div class="unitA01">
<h2 class="headingA01">クレジットカード情報の登録・変更</h2>

<p class="alignC marginB20">ありがとうございました。<br>
クレジットカード情報を登録いたしました。</p>
<p class="alignC marginB30">求人広告管理画面で『<span class="fontTypeB01">カード登録待ち</span>』のステイタスの場合は、<br>
自動的に掲載がスタートされます。</p>

<p class="alignC"><a href="/customer/index">求人広告管理（HOME）へ戻る</a></p>

</div><!-- /.unitA01 -->
</form>
<!-- InstanceEndEditable -->
</div><!-- / contentsContainer -->
</div><!-- / contents -->
