<div id="contents">
    <div id="contentsContainer">
        <!-- InstanceBeginEditable name="mainContents" -->
        <form method="post" action="#">
            <div class="unitA01">
                <h2 class="headingA01">クレジットカード情報の登録・変更</h2>
                <p>お客様の個人情報は、オンライン決済処理会社である<a href="https://webpay.jp/" target="_blank">ウェブペイ株式会社</a>が厳重に管理します。<br>
                    『コールナビ』へはお客様へのサービス提供に必要な情報のみが通知されます。<br>
                    お客様の情報は<a href="https://webpay.jp/security" target="_blank">ウェブペイ株式会社のセキュリティ対策</a>のもと管理され、あらかじめ通知、公表した範囲を超えて第三者に情報が開示されることはありません。<br>
                    安心してご利用ください。</p>
                <p>利用可能なカードは下記をご確認ください。</p>
                <div class="alignC marginB80"><img src="/public/admin_images/c00_img_01.png" width="502" height="122" alt=""></div>
                <div class="alignC marginB80">
                    {% if is_new == 1 %}
                    <form action="/credit/input" method="post">
                        <script src="https://checkout.webpay.jp/v2/" class="webpay-button" data-key="live_public_0ov99oako5bn5lSfo460zc15" data-lang="ja"></script>
                        <script>
                            
                            $(function() {
                                $('#WP_checkoutBox').find('input[type="button"]')
                                        .val('カード情報を登録する')
                                        .addClass('btnA02')
                                        .css('background-color', '#ff9105')
                                        .css('padding', '0px')
                                        .css('height', '38px')
                                        .css('font-size', '16px');
                              });
                              setInterval(function() {
                                $('#WP_sendButton')
                                    .css('background-image', 'none')
                                    .text('登録する');
                              }, 100);
                            
                        </script>
                    </form>
                    {% elseif is_new == 0 %}
                        <p class="alertB01 fontSize20 marginB40">お客様のクレジットカードは既に登録済みです。</p>
                        <form action="/credit/input" method="post">
                            <script src="https://checkout.webpay.jp/v2/" class="webpay-button" data-key="live_public_0ov99oako5bn5lSfo460zc15" data-lang="ja"></script>
                            <script>
                                $(function() {
                                    $('#WP_checkoutBox').find('input[type="button"]')
                                            .val('クレジットカードの変更')
                                            .addClass('btnA02 marginR15')
                                            .css('background-color', '#ff9105')
                                            .css('height', '38px')
                                            .css('width', '238px')
                                            .css('padding', '0')
                                            .css('positiion', 'relative')
                                            .css('bottom', '1.6px')
                                            .css('font-size', '16px')
                                    $('#WP_checkoutBox')
                                        .css('display', 'inline-block')
                                  });
                                  setInterval(function() {
                                    $('#WP_sendButton')
                                        .css('background-image', 'none')
                                        .text('変更する');
                                  }, 100);
                          </script>
                      </form>
                      <a href="#" class="btnA04">クレジットカード情報の削除</a>
                    {% endif %}
                </div>
                <div class="boxE01 clearfix">
                    <div class="floatL marginR15"><img src="/public/admin_images/pci-dss_logo.png" width="44" height="53" alt="PCI DSS"></div>
                    <p class="marginB00">PCI DSS（Payment Card Industry Data Security Standard）とは、加盟店・決済代行事業者が取扱うカード会員様のクレジットカード情報・お取り引き情報を安全に守るために、JCB、American Express、Discover、MasterCard、VISAの国際ペイメントブランド5社が共同で策定した、クレジット業界におけるグローバルセキュリティ基準です。</p>
                </div>

            </div><!-- /.unitA01 -->
        </form>
        <!-- InstanceEndEditable -->
    </div><!-- / contentsContainer -->
</div><!-- / contents -->