<div id="contents">
<div id="contentsContainer">
<!-- InstanceBeginEditable name="mainContents" -->
<form method="post" action="#">
<div class="unitA01">
<h2 class="headingA01">クレジットカード情報の登録・変更</h2>
<p>お客様の個人情報は、オンライン決済処理会社である<a href="https://webpay.jp/" target="_blank">ウェブペイ株式会社</a>が厳重に管理します。<br>
『コールナビ』へはお客様へのサービス提供に必要な情報のみが通知されます。<br>
お客様の情報は<a href="https://webpay.jp/security" target="_blank">ウェブペイ株式会社のセキュリティ対策</a>のもと管理され、あらかじめ通知、公表した範囲を超えて第三者に情報が開示されることはありません。<br>
安心してご利用ください。</p>
<p>利用可能なカードは下記をご確認ください。</p>
<div class="alignC marginB40"><img src="/public/admin_images/c00_img_01.png" width="502" height="122" alt=""></div>
<div class="alignC marginB80">
<a href="/credit/confirm" class="btnA02">カード情報を登録する</a>
</div>
<div class="boxE01 clearfix">
<div class="floatL marginR15"><img src="/public/admin_images/pci-dss_logo.png" width="44" height="53" alt="PCI DSS"></div>
<p class="marginB00">PCI DSS（Payment Card Industry Data Security Standard）とは、加盟店・決済代行事業者が取扱うカード会員様のクレジットカード情報・お取り引き情報を安全に守るために、JCB、American Express、Discover、MasterCard、VISAの国際ペイメントブランド5社が共同で策定した、クレジット業界におけるグローバルセキュリティ基準です。</p>
</div>

</div><!-- /.unitA01 -->
</form>
<!-- InstanceEndEditable -->
</div><!-- / contentsContainer -->
</div><!-- / contents -->

