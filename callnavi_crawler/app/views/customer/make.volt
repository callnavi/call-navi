{#
<script>
$(function(){
	$('ul.listC01 li').each(function(){
		var basis = $('li:nth-of-type(3)');
		var cell01H = $('.tableD01 td:eq(0)', basis).height();
		var cell02H = $('.tableD01 td:eq(1)', basis).height();
		$(basis).siblings().find('.tableD01 td:eq(0)').css('height', cell01H + 'px');
		$(basis).siblings().find('.tableD01 td:eq(1)').css('height', cell02H + 'px');
	});
});
</script>

<div id="contents">
<div id="contentsContainer">
<!-- InstanceBeginEditable name="mainContents" -->
<div class="unitA01">
<h2 class="headingA01">新規求人広告作成</h2>
<p class="marginB20">『コールナビ』内に掲載できる広告種別は以下の4つです。<br>表示エリアについての詳細は<a href="c-2-7-1.html" class="blank">こちら</a>をご確認ください。</p>

<div class="createAdsGuide">
<ul class="listG01 js-createAdsGuide01">
<li>
<div class="marginB20"><a href="/free/make" class="btnB04 add_free_offer">無料広告（オーガニック検索）を<br>作成する</a></div>
<table class="tableD01">
<tr>
<th>表示エリア</th>
<td><a href="/customer/displayArea" class="blank">検索結果ページ</a></td>
</tr>
<tr>
<th>掲載費</th>
<td><p class="fw alertB01">3つまで無料（6ヶ月間）</p><p class="textB01">※求人広告の内容と求職者の検索キーワードが一致した場合に表示されます。</p></td>
</tr>
</table>
</li>
</ul>

{#
<ul class="listC01">
<li>
<div class="marginB20"><a href="/listing/make" class="btnB02">リスティング広告を作成する</a></div>
<table class="tableD01">
<tr>
<th>表示エリア</th>
<td><a href="/customer/displayArea#area01" class="blank">検索結果ページ</a></td>
</tr>
<tr>
<th>掲載費</th>
<td><p class="fw">入札形式（1クリック：10円～）</p><p class="textB01">※入札単価が高い順に求人広告の<br>表示頻度が高くなります。</p></td>
</tr>
</table>
</li>
<li>
<div class="marginB20"><a href="/rectangle/make" class="btnB02">レクタングル広告を作成する</a></div>
<table class="tableD01">
<tr>
<th>表示エリア</th>
<td><a href="/customer/displayArea#area02" class="blank">全ページ</a></td>
</tr>
<tr>
<th>掲載費</th>
<td><p class="fw">入札形式（1クリック：30円～）</p><p class="textB01">※入札単価が高い順に求人広告の<br>表示頻度が高くなります。</p></td>
</tr>
</table>
</li>
<li>
<div class="marginB20"><a href="/pickup/make" class="btnB02">ピックアップ広告を作成する</a></div>
<table class="tableD01">
<tr>
<th>表示エリア</th>
<td><a href="/customer/displayArea#area03" class="blank">トップページ、検索結果ページ、<br>求人詳細ページ</a></td>
</tr>
<tr>
<th>掲載費</th>
<td>
<ul>
<li><span>インナーパネル</span><br>10,000円／1週（5社限定）</li>
<li><span>インナーパネルハーフ</span><br>6,000円／1週（4社限定）</li>
<li><span>右カラム</span><br>8,000円／1週（2社限定）</li>
<li><span>ロゴバナー</span><br>1,000円／1週（2社限定）</li>
</ul>
</td>
</tr>
</table>
</li>
</ul>


</div>
</div><!-- /.unitA01 -->
<!-- InstanceEndEditable -->
</div><!-- / contentsContainer -->
</div><!-- / contents -->
#}

<div id="contents">
<div id="contentsContainer">
<!-- InstanceBeginEditable name="mainContents" -->
<div class="unitA01">
<h2 class="headingA01">新規求人広告作成</h2>
<p class="marginB30">『コールナビ』内に掲載できる広告種別は以下の4つです。<br>表示エリアについての詳細は<a href="/customer/displayArea" class="blank">こちら</a>をご確認ください。</p>

<div class="createAdsGuide">
<ul class="listG01 js-createAdsGuide01">
<li>
<div class="marginB20"><button class="btnB04 add_free_offer">無料広告（オーガニック検索）を<br>作成する</button></div>
<table class="tableD01">
<tr>
<th>表示エリア</th>
<td><a href="/customer/displayArea" class="blank">検索結果ページ</a></td>
</tr>
<tr>
<th>掲載費</th>
<td><p class="fw alertB01">3つまで無料（6ヶ月間）</p><p class="textB01">※求人広告の内容と求職者の検索キーワードが一致した場合に表示されます。</p></td>
</tr>
</table>
</li>
</ul>

<ul class="listG01 js-createAdsGuide02">
<li>
<div class="marginB20"><a href="/listing/make" class="btnB03">リスティング広告を作成する</a></div>
<table class="tableD01">
<tr>
<th>表示エリア</th>
<td><a href="/customer/displayArea#area01" class="blank">検索結果ページ上位</a></td>
</tr>
<tr>
<th>掲載費</th>
<td><p class="fw">入札形式<br>（1クリック：10円～）</p><p class="textB01">※入札単価が高い順に求人広告の表示頻度が高くなります。</p></td>
</tr>
</table>
</li>
<li>
<div class="marginB20"><a href="/rectangle/make" class="btnB03">レクタングル広告を作成する</a></div>
<table class="tableD01">
<tr>
<th>表示エリア</th>
<td><a href="/customer/displayArea#area02" class="blank">全ページ</a></td>
</tr>
<tr>
<th>掲載費</th>
<td><p class="fw">入札形式<br>（1クリック：30円～）</p><p class="textB01">※入札単価が高い順に求人広告の表示頻度が高くなります。</p></td>
</tr>
</table>
</li>
<li>
<div class="marginB20"><a href="/pickup/makeStart" class="btnB03">ピックアップ広告を作成する</a></div>
<table class="tableD01">
<tr>
<th>表示エリア</th>
<td><a href="/customer/displayArea#area03" class="blank">検索結果ページ上位、<br>トップページ（新着表示オプション）</a></td>
</tr>
<tr>
<th>掲載費</th>
<td>
15,000円／1週間<br>
26,000円／2週間<br>
36,000円／3週間<br>
40,000円／4週間
</td></tr>
<tr>
<th>新着表示<br>オプション</th>
<td>
<ul class="marginB10">
<li><span>無し</span><br>無料</li>
<li><span>インナーパネル</span><br>2,000円／1週</li>
<li><span>インナーパネルハーフ</span><br>1,000円／1週</li>
<li><span>右カラム</span><br>500円／1週</li>
</ul>
<p class="textB01">※新着扱いでトップページに表示する期間は掲載開始から1週間のみとなっております。</p>
</td>
</tr>
</table>
</li>
</ul>

</div>
</div><!-- /.unitA01 -->
<!-- InstanceEndEditable -->
</div><!-- / contentsContainer -->
</div><!-- / contents -->


