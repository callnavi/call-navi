<!-- InstanceBeginEditable name="mainContents" -->

<div class="unitC01">
<div class="close"><a href="javascript:void(0)" onclick="window.close();"><img src="/public/admin_images/btn_close_n.png" alt="このウィンドウを閉じる"></a></div>
<h2 class="headingA01">求人広告表示エリア</h2>
<div class="tabNavA02 marginB15">
<ul id="js-tab01">
<li class="active"><a href="#area00">無料広告（オーガニック検索）</a></li>
<li><a href="#area01">リスティング広告</a></li>
<li><a href="#area02">レクタングル広告</a></li>
<li><a href="#area03">ピックアップ広告</a></li>
</ul>
</div>
<div>
<ul class="arealist">
<li id="area00">
	<p class="minH145">『コールナビ』上の検索結果表示エリア（オーガニック検索表示欄）に無料で求人広告を3つまで掲載できます。<br>
求職者が入力したキーワードが、御社の求人広告内容に含まれている場合に検索結果ページに表示されます。<br>
掲載期間は6ヶ月間となります。</p>
	<ul class="listE01 marginB40">
	<li>
	<dl class="ptn01">
	<dt>表示エリア</dt>
	<dd>検索結果ページ</dd>
	</dl>
	</li>
	<li>
	<dl>
	<dt>掲載費</dt>
	<dd><span class="alertB01">3つまで無料（6ヶ月間）</span><br>※求人広告の内容と求職者の検索キーワードが<br>一致した場合に表示されます。</dd>
	</dl>
	</li>
	</ul>
	<ul class="listH01">
	<li>
	<div class="headingC01"><h2>トップページ</h2></div>
	<div class="wide330"><img src="/public/admin_images/sample_organic_01.jpg" alt=""></div>
	</li>
	<li>
	<div class="headingC01"><h2>検索結果ページ</h2></div>
	<div class="wide"><img src="/public/admin_images/sample_organic_02.jpg" alt=""></div>
	</li>
	<li>
	<div class="headingC01"><h2>ピックアップ求人一覧</h2></div>
	<div class="wide"><img src="/public/admin_images/sample_organic_03.jpg" alt=""></div>
	</li>
	<li>
	<div class="headingC01"><h2>詳細ページ</h2></div>
	<div class="wide"><img src="/public/admin_images/sample_organic_04.jpg" alt=""></div>
	</li>
	</ul>
</li>
<li id="area01">
	<p class="minH145">求職者が入力する検索キーワードと連動して、指定したキーワードで検索された場合のみ、検索結果ページの上部に表示させる広告です。<br>
同じキーワードで複数社重複した場合は、入札形式によってクリック単価が高い広告が優先的に表示されます。<br>
広告からはご指定の求人媒体記事、サイトなどへのリンクを設定できます。</p>
	<ul class="listE01 marginB40">
	<li>
	<dl>
	<dt>表示エリア</dt>
	<dd>検索結果ページ上位</dd>
	</dl>
	</li>
	<li>
	<dl>
	<dt>掲載費</dt>
	<dd>入札形式（1クリック：10円～）<br>※入札単価が高い順に求人広告の表示頻度が高くなります。</dd>
	</dl>
	</li>
	</ul>
	<ul class="listH01">
	<li>
	<div class="headingC01"><h2>トップページ</h2></div>
	<div><img src="/public/admin_images/sample_listing_01.jpg" alt=""></div>
	</li>
	<li>
	<div class="headingC01"><h2>検索結果ページ</h2></div>
	<div class="wide"><img src="/public/admin_images/sample_listing_02.jpg" alt=""></div>
	</li>
	<li>
	<div class="headingC01"><h2>ピックアップ求人一覧</h2></div>
	<div class="wide"><img src="/public/admin_images/sample_listing_03.jpg" alt=""></div>
	</li>
	<li>
	<div class="headingC01"><h2>詳細ページ</h2></div>
	<div class="wide"><img src="/public/admin_images/sample_listing_04.jpg" alt=""></div>
	</li>
	</ul>
</li>
<li id="area02">
	<p class="minH145">検索キーワードとは連動せず、下層ページを含めた全ページに表示させる固定バナー広告です。<br>
	インプレッションはリスティング広告より多くなります。<br>
	複数社からの広告掲載希望がある場合は、入札形式によってクリック単価が高い広告が優先的に表示されます。<br>
	広告からはご指定の求人媒体記事、サイトなどへのリンクを設置できます。</p>
	<ul class="listE01 marginB40">
	<li>
	<dl>
	<dt>表示エリア</dt>
	<dd>全ページ</dd>
	</dl>
	</li>
	<li>
	<dl>
	<dt>掲載費</dt>
	<dd>入札形式（1クリック：30円～）<br>※入札単価が高い順に求人広告の表示頻度が高くなります。</dd>
	</dl>
	</li>
	</ul>
	<ul class="listH01">
	<li>
	<div class="headingC01"><h2>トップページ</h2></div>
	<div><img src="/public/admin_images/sample_rectangle_01.jpg" alt=""></div>
	</li>
	<li>
	<div class="headingC01"><h2>検索結果ページ</h2></div>
	<div class="wide"><img src="/public/admin_images/sample_rectangle_02.jpg" alt=""></div>
	</li>
	<li>
	<div class="headingC01"><h2>ピックアップ求人一覧</h2></div>
	<div class="wide"><img src="/public/admin_images/sample_rectangle_03.jpg" alt=""></div>
	</li>
	<li>
	<div class="headingC01"><h2>詳細ページ</h2></div>
	<div class="wide"><img src="/public/admin_images/sample_rectangle_04.jpg" alt=""></div>
	</li>
	</ul>
</li>
<li id="area03">
	<p class="minH145">固定で表示される期間限定（週単位）の広告です。<br>
	『コールナビ』サイト内で求人詳細ページを持つ事も可能です。</p>
	<ul class="listE01 marginB40">
		<li>
			<dl class="thin">
			<dt>表示エリア</dt>
			<dd>検索結果ページ上位、トップページ（新着表示オプション）</dd>
			</dl>
			<dl>
			<dt>掲載費</dt>
			<dd>15,000円／1週間<br>26,000円／2週間<br>36,000円／3週間<br>40,000円／4週間</dd>
			</dl>
			</li>
		</li>
		<li>
			<dl class="tall02">
			<dt>新着表示<br>オプション</dt>
			<dd>
			<table>
			<tr><th>無し</th><td>無料</td></tr>
			<tr><th>インナーパネル</th><td>2,000円／1週</td></tr>
			<tr><th>インナーパネルハーフ</th><td>1,000円／1週</td></tr>
			<tr><th>右カラム</th><td>500円／1週</td></tr>
			</table>
			※新着扱いでトップページに表示する期間は<br>掲載開始から1週間のみとなっております。
			</dd>
			</dl>
		</li>
	</ul>
	<ul class="listH01">
	<li>
	<div class="headingC01"><h2>トップページ</h2></div>
	<div class="wide330"><img src="/public/admin_images/sample_pickup_01.png" alt=""></div>
	</li>
	<li>
	<div class="headingC01"><h2>検索結果ページ</h2></div>
	<div class="wide"><img src="/public/admin_images/sample_pickup_02.jpg" alt=""></div>
	</li>
	<li>
	<div class="headingC01"><h2>ピックアップ求人一覧</h2></div>
	<div class="wide"><img src="/public/admin_images/sample_pickup_03.jpg" alt=""></div>
	</li>
	<li>
	<div class="headingC01"><h2>詳細ページ</h2></div>
	<div class="wide"><img src="/public/admin_images/sample_pickup_04.jpg" alt=""></div>
	</li>
	</ul>
</li>
</ul>
</div>
</div><!--//.unitC01-->
<!-- InstanceEndEditable -->
</body>

