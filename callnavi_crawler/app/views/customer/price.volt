<header id="globalHeader">
<div class="globalHeaderInner">
<nav id="funcNav">
<ul class="bfr">
<li class="bfr"><a href="/account/input">新規アカウント作成</a></li>
<li><a href="/login/index">ログイン</a></li>
</ul>
</nav>
<h1><img src="/public/images/customer/header_logo_02.png" width="138" height="40"><span>求人広告の掲載について</span></h1>
</div>
</header>

<div id="contents">
<div id="contentsContainer">
<!-- InstanceBeginEditable name="mainContents" -->
<div class="unitA01">
<h2 class="headingA01">求人広告の掲載について</h2>
<p class="marginB30">『コールナビ』では4種類の求人広告プランを用意しております。</p>
<p style="font-size: 18px;" class="marginB30 alignC fw alertB01">現在、求人広告は『無料広告』のみ出稿が可能です。<br />
その他の有料広告については準備中ですので、予めご了承ください。</p>
<div class="alignC marginB40"><a href="/public/pdf/document.pdf" target="_blank" class="btnC01">媒体資料をダウンロードする<span>PDF</span></a></div>

<div class="headingB01">
<h3>無料広告（オーガニック検索）</h3>
</div>
<p>『コールナビ』上の検索結果表示エリア（オーガニック検索表示欄）に無料で求人広告を3つまで掲載できます。<br>
求職者が入力したキーワードが、御社の求人広告内容に含まれている場合に検索結果ページに表示されます。<br>
掲載期間は6ヶ月間となります。表示エリアについては<a href="/customer/displayArea" class="blank">こちら</a>をご確認ください。</p>
<ul class="boxA01">
<li>掲載費</li>
<li><span class="price alertB01">3つまで無料（6ヶ月間）</span>
<p class="alert textB01 marginB00">※求人広告の内容と求職者の検索キーワードが一致した場合に表示されます。</p></li>
</ul>
</div>

<div class="unitA01">
<div class="headingB01">
<h3>リスティング広告</h3>
</div>
<p>求職者が入力する検索キーワードと連動して、指定したキーワードで検索された場合のみ、検索結果ページの上部に表示させる広告です。<br>
同じキーワードで複数社重複した場合は、入札形式によってクリック単価が高い広告が優先的に表示されます。<br>
広告からはご指定の求人媒体記事、サイトなどへのリンクを設定できます。<br>
表示エリアについては<a href="/customer/displayArea#area01" class="blank">こちら</a>をご確認ください。</p>
<ul class="boxA01">
<li>掲載費</li>
<li>入札形式（1クリック：<span class="price">10</span>円～）<br>
<span class="alert">※入札単価が高い順に求人広告の表示頻度が高くなります。</span></li>
</ul>
</div>


<div class="unitA01">
<div class="headingB01">
<h3>レクタングル広告</h3>
</div>
<p>検索キーワードとは連動せず、下層ページを含めた全ページに表示させる固定バナー広告です。<br>
インプレッションはリスティング広告より多くなります。<br>
複数社からの広告掲載希望がある場合は、入札形式によってクリック単価が高い広告が優先的に表示されます。<br>
広告からはご指定の求人媒体記事、サイトなどへのリンクを設置できます。<br>
表示エリアについては<a href="/customer/displayArea#area02" class="blank">こちら</a>をご確認ください。</p>
<ul class="boxA01">
<li>掲載費</li>
<li>入札形式（1クリック：<span class="price">30</span>円～）<br>
<span class="alert">※入札単価が高い順に求人広告の表示頻度が高くなります。</span></li>
</ul>
</div>

<div class="unitA01">
<div class="headingB01">
<h3>ピックアップ広告</h3>
</div>
<p>固定で表示される期間限定（週単位）の広告です。<br>
『コールナビ』サイト内で求人詳細ページを持つ事も可能です。<br>

表示エリアについては<a href="/customer/displayArea#area03" class="blank">こちら</a>をご確認ください。</p>
<ul class="boxA01">
<li>掲載費</li>
<li class="ltt">
<table>
<tr>
<td class="alignR"><span class="price">15,000</span>円</td>
<td class="alignL">&nbsp;／1週間</td>
</tr>
<tr>
<td class="alignR"><span class="price">26,000</span>円</td>
<td class="alignL">&nbsp;／2週間</td>
</tr>
<tr>
<td class="alignR"><span class="price">36,000</span>円</td>
<td class="alignL">&nbsp;／3週間</td>
</tr>
<tr>
<td class="alignR"><span class="price">40,000</span>円</td>
<td class="alignL">&nbsp;／4週間</td>
</tr>
</table>
</li>
</ul>
<div class="plusMark">＋</div>
<ul class="boxA01">
<li>トップページ<br>新着表示<br>（オプション）</li>
<li class="ltt">
<table>
<tr>
<td class="alignL pr10">無し</td>
<td class="alignR"><span class="price">無料</span></td>
<td class="alignL">&nbsp;</td>
</tr>
<tr>
<td class="alignL pr10">インナーパネル</td>
<td class="alignR"><span class="price">2,000</span>円</td>
<td class="alignL">&nbsp;／1週</td>
</tr>
<tr>
<td class="alignL pr10">インナーパネルハーフ</td>
<td class="alignR"><span class="price">1,000</span>円</td>
<td class="alignL">&nbsp;／1週</td>
</tr>
<tr>
<td class="alignL pr10">右カラム</td>
<td class="alignR"><span class="price">500</span>円</td>
<td class="alignL">&nbsp;／1週</td>
</tr>
</table>
<span class="alert">※新着扱いでトップページに表示する期間は、掲載開始から1週間のみとなっております。</span>
</li>
</ul>
</div>

<div class="unitA01">
<h2 class="headingA01">求人広告費用のお支払い方法</h2>
<p class="marginB10">お支払いはクレジットカード決済のみになります。<br>クレジットカード決済は『<a href="https://webpay.jp/" target="_blank">WebPay</a>』を利用しています。<br>クレジットカード情報漏洩防止の国際セキュリティ標準である<a href="https://ja.pcisecuritystandards.org/minisite/en/" target="_blank">PCI DSS</a>にも準拠しており、WebPayに預けたクレジットカード番号はしっかりと守られます。<br>利用可能なカードは下記をご確認ください。</p>
<div class="alignC"><img src="/public/images/customer/c00_img_01.png" width="502" height="122" alt=""></div>
</div>

<div class="unitA01">
<h2 class="headingA01">求人広告の作成方法</h2>
<p>新規アカウント作成後、管理画面から簡単に求人広告を作成することができます。</p>
<p class="alignC marginB20"><a href="/account/input" class="btnB01">今すぐ求人広告を作成する<br>
<span>（新規アカウント作成）</span></a></p>
<div class="alignC marginB40"><a href="/public/pdf/document.pdf" target="_blank" class="btnC01">媒体資料をダウンロードする<span>PDF</span></a></div>
</div>
<!-- InstanceEndEditable -->
</div><!-- / contentsContainer -->
</div><!-- / contents -->

<footer id="globalFooter">
<div class="globalFooterInner">
<ul id="footerNav">
<li><a href="terms" class="blank">利用規約・プライバシーポリシー</a></li>
<li><a href="trading" class="blank">特定商取引法に基づく表記</a></li>
</ul>
<div id="footerLogo2">
<p id="copyright">Copyright &copy; 2015 CALL Navi Inc.All Rights Reserved.</p>
</div>
</div><!-- / contentsInner -->
</footer>
