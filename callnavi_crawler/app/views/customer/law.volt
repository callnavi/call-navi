             <div id="contents">
<div id="contentsContainer">
<!-- InstanceBeginEditable name="mainContents" -->
<div class="unitA01">
<h2 class="headingA01">特定商取引法に基づく表記</h2>
<table class="tableB01 marginB30">
<tr>
<th class="ttl">運営団体名</th>
<td>株式会社コールナビ</td>
</tr>
<tr>
<th class="ttl">運営責任者名</th>
<td>代表取締役　山本 桂</td>
</tr>
<tr>
<th class="ttl">所在地</th>
<td>〒169-0075 東京都新宿区高田馬場2-13-2</td>
</tr>
<tr>
<th class="ttl">電話番号</th>
<td>03-6892-1796</td>
</tr>
<tr>
<th class="ttl">問合せ先メールアドレス</th>
<td><a href="mailto:info@callnavi.jp">info@callnavi.jp</a></td>
</tr>
<tr>
<th class="ttl">販売価格</th>
<td>各サービスごとに税抜きで表示されます。</td>
</tr>
<tr>
<th class="ttl">ご注文方法</th>
<td>Webフォーム</td>
</tr>
<tr>
<th class="ttl">お支払方法</th>
<td>お支払いはクレジットカード決済のみになります。<br />
クレジットカード決済は『<a href="https://webpay.jp/" target="_blank">WebPay</a>』を利用しております。<br />
クレジットカード情報漏洩防止の国際セキュリティ標準であるPCI DSSにも準拠しており、WebPayに預けたクレジットカード番号はしっかりと守られます。<br />
○利用可能カード：JCB、VISA、MasterCard、Diners Club、AMERICAN EXPRESS</td>
</tr>
<tr>
<th class="ttl">サービスのお渡し時期および<br />サービス代金のお支払時期</th>
<td>クレジットカード決済完了後、直ちに当有料サービスが利用可能となります。<br />
代金のお支払時期は各決済会社によって異なります。</td>
</tr>
<tr>
<th class="ttl">契約の更新</th>
<td>自動更新の停止手続きを行わない限り自動的に有料サービスが更新され、有料サービスを最初に購入した際に選択されたコースに応じて課金が発生いたします。</td>
</tr>
<tr>
<th class="ttl">返品について</th>
<td>サービスの性質上、返品はお受けできません。</td>
</tr>
</table>
</div>
<!-- InstanceEndEditable -->
</div><!-- / contentsContainer -->
</div><!-- / contents -->