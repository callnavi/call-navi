<div id="offers">
    <table class="tableB02">
        <tr>
            <th>決済日時</th>
            <th>広告形態</th>
            <th>広告名（内部管理用）</th>
            <th>募集職種</th>
            <th>募集会社名</th>
            <th>費用</th>
        </tr>
        {% for offer in offers %}
            {% if offer.expense > 0 %}
                {% if offer.without_card == 0  %}
                    <tr>
                        <td><span class="pay_year">{{ pay_year }}</span>/<span class="pay_month">{{ pay_month }}</span>/31</td>
                        <td>
                            {% if offer.type == "listing" %}
                                リスティング広告
                            {% elseif offer.type == "rectangle" %}
                                レクタングル広告
                            {% elseif offer.type == "pickup" %}
                                ピックアップ広告
                            {% endif %}
                        </td>
                        <td>{{ offer.title }}</td>
                        <td>{{ offer.job_category }}</td>
                        <td>{{ offer.company_name }}</td>
                        <td>&yen;<span id="{{ offer.type }}_expense_{{ offer.id }}">{{ offer.expense }}</span></td>
                    </tr>
                {% endif %}
            {% endif %}
        {% endfor %}
        <tr>
            <td colspan="6" class="lastCell">対象期間合計　&yen;<span id="expense_sum">{{ expense_sum }}</span></td>
        </tr>
        <tr>
            <td colspan="6" class="lastCell"><span class="fnor">消費税（8％）　&yen;{{ tax }}</span></td>
        </tr>
        <tr>
          <td colspan="6" class="lastCell">当月ご請求金額合計　&yen;{{ expense_sum + tax }}</td>
        </tr>
    </table>
</div>