<div id="contents">
<div id="contentsContainer">

<div class="unitA01">
<h2 class="headingA01">お問い合わせ</h2>
<p class="marginB20">コールナビをご利用いただきありがとうございます。サービス内容や広告出稿についてのご不明点は下記窓口にお問い合わせください。<br />
通常2営業日以内に対応いたしますが、内容によっては返信にお時間いただく場合がございます。 </p>
<ul class="listF01">
<li>
<dl class="boxF01">
<dt class="boxTtl">メールでのお問い合わせ</dt>
<dd class="boxCnts">
<p class="headingD01">info@callnavi.jp</p>
<p class="textB01">※土日祝のお問い合わせは、平日の営業時間内に改めてご連絡させていただきます。</p>
</dd>
</dl>
</li>
<li>
<dl class="boxF01">
<dt class="boxTtl">お電話でのお問い合わせ</dt>
<dd class="boxCnts">
<p class="headingD01">03-6892-1796</p>
<p>受付時間：午前9時〜午後18時（土日祝/年末年始を除きます）</p>
</dd>
</dl>
</li>
</ul>
</div><!-- /.unitA01 -->

</div><!-- / contentsContainer -->
</div><!-- / contents -->

