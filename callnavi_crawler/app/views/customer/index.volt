<div id="contents">
    <div id="contentsContainer">
    <!-- InstanceBeginEditable name="mainContents" -->
        <div class="unitA01">
            <h2 class="headingA01">求人広告管理</h2>
             <div class="headingB01">
<h3>無料広告（オーガニック検索）</h3>
<span class="hyoujiArea"><a href="/customer/displayArea" class="blank">表示エリアについて</a></span>
</div>
<ul class="formBlockA01">
<li>対象期間</li>
<li><select name="free_scope" class="formA01 sfr">
<option value="today">今日</option>
<option value="yesterday">昨日</option>
<option value="week">今週（月～）</option>
<option value="seven_days">過去7日間：{{ one_week_ago }} 〜 {{ today }}</option>
<option value="last_week">先週（月～日）</option>
<option value="month">今月</option>
<option value="thirty_days">過去30日間</option>
<option value="last_month">先月</option>
<option value="all">全期間</option>
</select></li>
<li><input type="button" class="formA01" value="適用" id="change_free_scope"></li>
</ul>
<table class="tableA01">
<tr>
<th>操作</th>
<th>ステータス</th>
<th class="alignL">募集職種</th>
<th class="alignL">募集会社名</th>
<th>表示回数</th>
<th>クリック数</th>
<th>クリック率</th>
<th>プレビュー</th>
</tr>
{% if free_count > 0 %}
    {% for free_offer in free_offers %}
<tr id="free_offer_{{ free_offer.id }}" data-offer_title="{{ free_offer.title }}">


  {% if free_offer.status == "judging" %}
                <td class="free_offer_display_{{ free_offer.id }}">
                    <div class="status before_change">
                        <span class="ttl"><img src="/public/admin_images/table_select_00_1.png" width="44" height="28" alt=""></span>
                        <ul>
                            <li id="free_offer_delete_{{ free_offer.id }}" data-offer_id="{{ free_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                        </ul>
                     </div>
                </td>
                <td class="free_offer_display_{{ free_offer.id }}">
                    <span class="judge">
                        審査中
                    </span>
                </td>
            {% elseif free_offer.status == "publishing" %}
              {% if free_offer.allowed >= six_month_ago %}
                    <td class="free_offer_display_{{ free_offer.id }}">
                        <div class="status before_change">
                            <span class="ttl"><img src="/public/admin_images/table_select_01_1.png" width="44" height="28" alt=""></span>
                            <ul>
                                <li class="status_02"><a href="/free/edit?offer_id={{ free_offer.id }}&account_id={{ free_offer.account_id }}" class="st2">編集</a></li>
                                <li id="free_offer_cancel_{{ free_offer.id }}" data-offer_id="{{ free_offer.id }}" class="status_03"><a href="javascript:void(0);" class="st3">一時停止</a></li>                                        
                                <li id="free_offer_delete_{{ free_offer.id }}" data-offer_id="{{ free_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                            </ul>
                        </div>
                    </td>
                    <td class="free_offer_display_{{ free_offer.id }}">
                        <span class="publish">
                            掲載中
                        </span>
                    </td>
              {% else %}
                    <td class="free_offer_display_{{ free_offer.id }}">
                    <div class="status before_change">
                        <span class="ttl"><img src="/public/admin_images/table_select_00_1.png" width="44" height="28" alt=""></span>
                        <ul>
                            <li class="status_01"><a href="/free/edit?offer_id={{ free_offer.id }}&account_id={{ free_offer.account_id }}" class="st1">再掲載</a></li>                                       
                            <li id="free_offer_delete_{{ free_offer.id }}" data-offer_id="{{ free_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                        </ul>
                    </div>
                    </td>    
                    <td class="free_offer_display_{{ free_offer.id }}">
                        <span class="stop">掲載終了</span>
                    </td>
              {% endif %}
              
            {% elseif free_offer.status == "not_allowed" %}
                <td class="free_offer_display_{{ free_offer.id }}">
                    <div class="status before_change">
                        <span class="ttl"><img src="/public/admin_images/table_select_00_1.png" width="44" height="28" alt=""></span>
                        <ul>
                            <li class="status_02"><a href="/free/edit?offer_id={{ free_offer.id }}&account_id={{ free_offer.account_id }}" class="st2">編集</a></li>
                            <li id="free_offer_delete_{{ free_offer.id }}" data-offer_id="{{ free_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                        </ul>
                    </div>
                </td>
                <td class="free_offer_display_{{ free_offer.id }}">
                    <span class="unauthorized">
                        非承認
                    </span>
                </td>
           
            {% elseif free_offer.status == "editing" %}
                <td class="free_offer_display_{{ free_offer.id }}">
                    <div class="status before_change">
                        <span class="ttl"><img src="/public/admin_images/table_select_02_1.png" width="44" height="28" alt=""></span>
                        <ul>
                            <li class="status_02"><a href="/free/edit?offer_id={{ free_offer.id }}&account_id={{ free_offer.account_id }}" class="st2">編集</a></li>
                            <li id="free_offer_delete_{{ free_offer.id }}" data-offer_id="{{ free_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                        </ul>
                    </div>
                </td>
                <td class="free_offer_display_{{ free_offer.id }}">
                    <span class="edit">
                        編集中
                    </span>
                </td>
            {% elseif free_offer.status == "canceled" %}
                <td class="free_offer_display_{{ free_offer.id }}">
                    <div class="status before_change">
                        <span class="ttl"><img src="/public/admin_images/table_select_03_1.png" width="44" height="28" alt=""></span>
                        <ul>
                            <li id="free_offer_effect_{{ free_offer.id }}" data-offer_id="{{ free_offer.id }}" class="status_01"><a href="javascript:void(0);" class="st1">有効</a></li>                                       
                            <li class="status_02"><a href="/free/edit?offer_id={{ free_offer.id }}&account_id={{ free_offer.account_id }}" class="st2">編集</a></li>
                            <li id="free_offer_delete_{{ free_offer.id }}" data-offer_id="{{ free_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                        </ul>
                    </div>
                </td>
                <td class="free_offer_display_{{ free_offer.id }}">
                    <span class="stop">
                        一時停止中
                    </span>
                </td>
            {% endif %}
                        <td class="free_offer_publishing_{{ free_offer.id }}" style="display: none;">
                            <div class="status publishing">
                                <span class="ttl"><img src="/public/admin_images/table_select_01_1.png" width="44" height="28" alt=""></span>
                                <ul>
                                    <li class="status_02"><a href="/free/edit?offer_id={{ free_offer.id }}&account_id={{ free_offer.account_id }}" class="st2">編集</a></li>
                                    <li id="free_offer_cancel_{{ free_offer.id }}" data-offer_id="{{ free_offer.id }}" class="status_03"><a href="javascript:void(0);" class="st3">一時停止</a></li>                                                                                     
                                    <li id="listing_offer_delete_{{ free_offer.id }}" data-offer_id="{{ free_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                </ul>
                            </div>
                        </td>
                        <td class="free_offer_publishing_{{ free_offer.id }}" style="display: none;">
                            <span class="publish">
                                掲載中
                            </span>
                        </td>
                        
                        <td class="free_offer_canceled_{{ free_offer.id }}" style="display: none;">
                            <div class="status canceled">
                                <span class="ttl"><img src="/public/admin_images/table_select_03_1.png" width="44" height="28" alt=""></span>
                                <ul>
                                    <li id="free_offer_effect_{{ free_offer.id }}" data-offer_id="{{ free_offer.id }}" class="status_01"><a href="javascript:void(0);" class="st1">有効</a></li>                                       
                                    <li class="status_02"><a href="/free/edit?offer_id={{ free_offer.id }}&account_id={{ free_offer.account_id }}" class="st2">編集</a></li>
                                    <li id="free_offer_delete_{{ free_offer.id }}" data-offer_id="{{ free_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                </ul>
                            </div>
                        </td>
                        <td class="free_offer_canceled_{{ free_offer.id }}" style="display: none;">
                            <span class="stop">
                                一時停止中
                            </span>
                        </td>

<td class="alignL">{{free_offer.job_category}}</td>
<td class="alignL">{{free_offer.company_name}}</td> 
<td id="free_offer_impression_log_{{ free_offer.id }}">{{ free_offer.impression_log }}</td>
<td id="free_offer_click_log_{{ free_offer.id }}">{{ free_offer.click_log }}</td>
     <td>
    {% if free_offer.click_ratio === false  %}
        <span id="free_offer_click_ratio_{{ free_offer.id }}">-</span>%
    {% else %}
        <span id="free_offer_click_ratio_{{ free_offer.id }}">{{ free_offer.click_ratio }}</span>%
    {% endif %}
    </td>
<td><a href="/free/preview?offer_id={{free_offer.id}}" target="_blank"><img src="/public/admin_images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
</tr>
  {% endfor %}
     {% else %}
        <tr class="lastCol">
                <td></td>
                <td></td>
                <td class="alignL">登録されている無料広告はありません。</td>
                <td class="alignL"></td>
                <td class="noWrap"></td>
                <td></td>
                <td></td>
                <td></td>
             </tr> 
     {% endif %} 
  <tr class="lastCol" id="free_deleted_all" style="display: none;">
                <td></td>
                <td></td>
                <td class="alignL">登録されている無料広告はありません。</td>
                <td class="alignL"></td>
                <td class="noWrap"></td>
                <td></td>
                <td></td>
                <td></td>
             </tr> 
</table>
<div class="clearfix">
<ul class="listA01">
<li><input type="button" class="formA02 add_free_offer" value="無料広告（オーガニック検索）の追加" {# onClick="javascript:location.href='/free/make';" #}></li>
</ul>
</div>
</div>


       <div class="unitA01">
            <div class="headingB01">
            <h3>リスティング広告</h3>
            <span class="hyoujiArea"><a href="/customer/displayArea#area01" class="blank">表示エリアについて</a></span>
        </div>
        <ul class="formBlockA01">
            <li>対象期間</li>
            <li>
                <select class="formA01 sfr" name="listing_scope">
                    <option value="today">今日</option>
                    <option value="yesterday">昨日</option>
                    <option value="week">今週（月～）</option>
                    <option value="seven_days">過去7日間：{{ one_week_ago }} 〜 {{ today }}</option>
                    <option value="last_week">先週（月～日）</option>
                    <option value="month">今月</option>
                    <option value="thirty_days">過去30日間</option>
                    <option value="last_month">先月</option>
                    <option value="all">全期間</option>
                </select>
            </li>
            <li><input type="button" class="formA01" value="適用" id="change_listing_scope" ></li>
        </ul>
        <table class="tableA01">
            <tr>
                <th>操作</th>
                <th>ステータス</th>
                <th class="alignL">募集職種</th>
                <th class="alignL">募集会社名</th>
                <th>検索ワード</th>
                <th>表示回数</th>
                <th>表示頻度</th>
                <th>クリック数</th>
                <th>クリック率</th>
                <th>クリック単価</th>
                <th>上限予算</th>
                <th>費用</th>
                <th>プレビュー</th>
            </tr>
            {% if listing_count > 0 %}
                {% for listing_offer in listing_offers %}
                    <tr id="listing_offer_{{ listing_offer.id }}" data-offer_title="{{ listing_offer.title }}">
                            {% if listing_offer.status == "judging" %}
                                <td class="listing_offer_display_{{ listing_offer.id }}">
                                    <div class="status before_change">
                                        <span class="ttl"><img src="/public/admin_images/table_select_00_1.png" width="44" height="28" alt=""></span>
                                        <ul>
                                            <li id="listing_offer_delete_{{ listing_offer.id }}" data-offer_id="{{ listing_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                        </ul>
                                     </div>
                                </td>
                                <td class="listing_offer_display_{{ listing_offer.id }}">
                                    <span class="judge">
                                        審査中
                                    </span>
                                </td>
                            {% elseif listing_offer.status == "publishing" %}
                                    {% if listing_offer.is_available == 0 %}
                                <td class="listing_offer_display_{{ listing_offer.id }}">
                                    <div class="status before_change">
                                        <span class="ttl"><img src="/public/admin_images/table_select_00_1.png" width="44" height="28" alt=""></span>
                                        <ul>
                                            <li class="status_02"><a href="/listing/edit?offer_id={{ listing_offer.id }}&account_id={{ listing_offer.account_id }}" class="st2">編集</a></li>
                                            <li id="listing_offer_delete_{{ listing_offer.id }}" data-offer_id="{{ listing_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                        </ul>
                                    </div>
                                </td>
                                <td class="listing_offer_display_{{ listing_offer.id }}">
                                    <span class="unauthorized">
                                        上限予算到達
                                    </span>
                                </td>
                                    {% else %}
                                <td class="listing_offer_display_{{ listing_offer.id }}">
                                    <div class="status before_change">
                                        <span class="ttl"><img src="/public/admin_images/table_select_01_1.png" width="44" height="28" alt=""></span>
                                        <ul>
                                            <li class="status_02"><a href="/listing/edit?offer_id={{ listing_offer.id }}&account_id={{ listing_offer.account_id }}" class="st2">編集</a></li>
                                            <li id="listing_offer_cancel_{{ listing_offer.id }}" data-offer_id="{{ listing_offer.id }}" class="status_03"><a href="javascript:void(0);" class="st3">一時停止</a></li>                                        
                                            <li id="listing_offer_delete_{{ listing_offer.id }}" data-offer_id="{{ listing_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                        </ul>
                                    </div>
                                </td>
                                <td class="listing_offer_display_{{ listing_offer.id }}">
                                    <span class="publish">
                                        掲載中
                                    </span>
                                </td>
                                    {% endif %}
                            {% elseif listing_offer.status == "not_allowed" %}
                                <td class="listing_offer_display_{{ listing_offer.id }}">
                                    <div class="status before_change">
                                        <span class="ttl"><img src="/public/admin_images/table_select_00_1.png" width="44" height="28" alt=""></span>
                                        <ul>
                                            <li class="status_02"><a href="/listing/edit?offer_id={{ listing_offer.id }}&account_id={{ listing_offer.account_id }}" class="st2">編集</a></li>
                                            <li id="listing_offer_delete_{{ listing_offer.id }}" data-offer_id="{{ listing_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                        </ul>
                                    </div>
                                </td>
                                <td class="listing_offer_display_{{ listing_offer.id }}">
                                    <span class="unauthorized">
                                        非承認
                                    </span>
                                </td>
                           {% elseif listing_offer.status == "waiting_card" %}
                               {% if basic.creditcard_id != "" and basic.card_status == "before_approval" %}
                                    <td class="listing_offer_display_{{ listing_offer.id }}">
                                        <div class="status before_change">
                                            <img src="/public/admin_images/table_select_00_1.png" width="44" height="28" alt="">
                                        </div>
                                    </td>
                                    <td class="listing_offer_display_{{ listing_offer.id }}">
                                        <span class="unauthorized">
                                            カード審査中
                                        </span>
                                </td>
                                {% else %}
                                    <td class="listing_offer_display_{{ listing_offer.id }}">
                                        <div class="status before_change">
                                            <span class="ttl"><img src="/public/admin_images/table_select_00_1.png" width="44" height="28" alt=""></span>
                                            <ul>
                                                <li class="status_02"><a href="/listing/edit?offer_id={{ listing_offer.id }}&account_id={{ listing_offer.account_id }}" class="st2">編集</a></li>
                                                <li class="status_06"><a href="/credit/input" class="st2">カード登録</a></li>
                                                <li id="listing_offer_delete_{{ listing_offer.id }}" data-offer_id="{{ listing_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                    <td class="listing_offer_display_{{ listing_offer.id }}">
                                        <span class="wait">
                                            カード登録待ち
                                        </span>
                                    </td>
                              {% endif %}
                            {% elseif listing_offer.status == "editing" %}
                                <td class="listing_offer_display_{{ listing_offer.id }}">
                                    <div class="status before_change">
                                        <span class="ttl"><img src="/public/admin_images/table_select_02_1.png" width="44" height="28" alt=""></span>
                                        <ul>
                                            <li class="status_02"><a href="/listing/edit?offer_id={{ listing_offer.id }}&account_id={{ listing_offer.account_id }}" class="st2">編集</a></li>
                                            <li id="listing_offer_delete_{{ listing_offer.id }}" data-offer_id="{{ listing_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                        </ul>
                                    </div>
                                </td>
                                <td class="listing_offer_display_{{ listing_offer.id }}">
                                    <span class="edit">
                                        編集中
                                    </span>
                                </td>
                            {% elseif listing_offer.status == "canceled" %}
                                <td class="listing_offer_display_{{ listing_offer.id }}">
                                    <div class="status before_change">
                                        <span class="ttl"><img src="/public/admin_images/table_select_03_1.png" width="44" height="28" alt=""></span>
                                        <ul>
                                            <li id="listing_offer_effect_{{ listing_offer.id }}" data-offer_id="{{ listing_offer.id }}" class="status_01"><a href="javascript:void(0);" class="st1">有効</a></li>                                       
                                            <li class="status_02"><a href="/listing/edit?offer_id={{ listing_offer.id }}&account_id={{ listing_offer.account_id }}" class="st2">編集</a></li>
                                            <li id="listing_offer_delete_{{ listing_offer.id }}" data-offer_id="{{ listing_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                        </ul>
                                    </div>
                                </td>
                                <td class="listing_offer_display_{{ listing_offer.id }}">
                                    <span class="stop">
                                        一時停止中
                                    </span>
                                </td>
                            {% endif %}
                        <td class="listing_offer_publishing_{{ listing_offer.id }}" style="display: none;">
                            <div class="status publishing">
                                <span class="ttl"><img src="/public/admin_images/table_select_01_1.png" width="44" height="28" alt=""></span>
                                <ul>
                                    <li class="status_02"><a href="/listing/edit?offer_id={{ listing_offer.id }}&account_id={{ listing_offer.account_id }}" class="st2">編集</a></li>
                                    <li id="listing_offer_cancel_{{ listing_offer.id }}" data-offer_id="{{ listing_offer.id }}" class="status_03"><a href="javascript:void(0);" class="st3">一時停止</a></li>                                                                                     
                                    <li id="listing_offer_delete_{{ listing_offer.id }}" data-offer_id="{{ listing_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                </ul>
                            </div>
                        </td>
                        <td class="listing_offer_publishing_{{ listing_offer.id }}" style="display: none;">
                            <span class="publish">
                                掲載中
                            </span>
                        </td>
                        <td class="listing_offer_waiting_card_{{ listing_offer.id }}" style="display :none;">
                            <div class="status waiting_card">
                                <span class="ttl"><img src="/public/admin_images/table_select_00_1.png" width="44" height="28" alt=""></span>
                                <ul>
                                    <li class="status_02"><a href="/listing/edit?offer_id={{ listing_offer.id }}&account_id={{ listing_offer.account_id }}" class="st2">編集</a></li>
                                    <li id="listing_offer_delete_{{ listing_offer.id }}" data-offer_id="{{ listing_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                </ul>
                            </div>
                        </td>
                        <td class="listing_offer_waiting_card_{{ listing_offer.id }}" style="display :none;">
                            <span class="wait">
                                カード登録待ち
                            </span>
                        </td>
                        <td class="listing_offer_canceled_{{ listing_offer.id }}" style="display: none;">
                            <div class="status canceled">
                                <span class="ttl"><img src="/public/admin_images/table_select_03_1.png" width="44" height="28" alt=""></span>
                                <ul>
                                    <li id="listing_offer_effect_{{ listing_offer.id }}" data-offer_id="{{ listing_offer.id }}" class="status_01"><a href="javascript:void(0);" class="st1">有効</a></li>                                       
                                    <li class="status_02"><a href="/listing/edit?offer_id={{ listing_offer.id }}&account_id={{ listing_offer.account_id }}" class="st2">編集</a></li>
                                    <li id="listing_offer_delete_{{ listing_offer.id }}" data-offer_id="{{ listing_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                </ul>
                            </div>
                        </td>
                        <td class="listing_offer_canceled_{{ listing_offer.id }}" style="display: none;">
                            <span class="stop">
                                一時停止中
                            </span>
                        </td>
                        <td class="alignL">{{ listing_offer.job_category }}</td>
                        <td class="alignL">{{ listing_offer.company_name }}</td>
                        <td class="noWrap">{{ listing_offer.keyword }}個 <a href="/listing/keyword?offer_id={{ listing_offer.id }}&account_id={{ listing_offer.account_id }}&offer_job_category={{ listing_offer.job_category }}"><img src="/public/admin_images/form_bg_search_n.png" alt="" width="40" height="28"></a></td>
                        <td id="listing_offer_impression_log_{{ listing_offer.id }}">{{ listing_offer.impression_log }}</td>
                        <td class="listing_offer_impression_ratio_{{ listing_offer.id }}">
                            {% if listing_offer.impression_ratio == "error" %}
                                -
                            {% elseif listing_offer.impression_ratio >= 80 %}
                                <span class="freq_1">最高</span>
                            {% elseif listing_offer.impression_ratio >= 60 %}
                                <span class="freq_2">高</span>
                            {% elseif listing_offer.impression_ratio >= 40 %}
                                <span class="freq_3">普通</span>
                            {% elseif listing_offer.impression_ratio >= 0 %}
                                <span class="freq_4">低</span>
                            {% endif %}
                        </td>
                        <td class="listing_offer_impression_ratio_{{ listing_offer.id }}" id="listing_offer_impression_ratio_highest_{{ listing_offer.id }}" style="display: none;">
                            <span class="freq_1">最高</span>
                        </td>
                        <td class="listing_offer_impression_ratio_{{ listing_offer.id }}" id="listing_offer_impression_ratio_high_{{ listing_offer.id }}" style="display: none;">
                            <span class="freq_2">高</span>
                        </td>
                        <td class="listing_offer_impression_ratio_{{ listing_offer.id }}" id="listing_offer_impression_ratio_middle_{{ listing_offer.id }}" style="display: none;">
                            <span class="freq_3">普通</span>
                        </td>
                        <td class="listing_offer_impression_ratio_{{ listing_offer.id }}" id="listing_offer_impression_ratio_low_{{ listing_offer.id }}" style="display: none;">
                            <span class="freq_4">低</span>
                        </td>
                        <td class="listing_offer_impression_ratio_{{ listing_offer.id }}" id="listing_offer_impression_ratio_error_{{ listing_offer.id }}" style="display: none;">
                            -
                        </td>
                        <td id="listing_offer_click_log_{{ listing_offer.id }}">{{ listing_offer.click_log }}</td>
                        <td>
                            {% if listing_offer.click_ratio === false  %}
                                <span id="listing_offer_click_ratio_{{ listing_offer.id }}">-</span>%
                            {% else %}
                                <span id="listing_offer_click_ratio_{{ listing_offer.id }}">{{ listing_offer.click_ratio }}</span>%
                            {% endif %}
                        </td>
                        <td>
                            {% if listing_offer.without_card == 1 %}
                                <font color="#ff0000">&yen;{{ listing_offer.click_price }}</font>
                            {% else %}
                                &yen;{{ listing_offer.click_price }}  
                            {% endif %}
                        </td>
                        <td>
                            {% if listing_offer.without_card == 1 %}
                                <font color="#ff0000">&yen;{{ listing_offer.budget_max }}</font>
                            {% else %}
                                &yen;{{ listing_offer.budget_max }}
                            {% endif %}
                        </td>
                        <td>
                            {% if listing_offer.without_card == 1 %}
                              <font color="#ff0000">&yen;<span id="listing_offer_expense_{{ listing_offer.id }}">{{ listing_offer.expense }}</span></font>
                            {% else %}
                              &yen;<span id="listing_offer_expense_{{ listing_offer.id }}">{{ listing_offer.expense }}</span>
                            {% endif %}
                        </td>
                        <td><a href="/listing/preview?offer_id={{ listing_offer.id }}&account_id={{ listing_offer.account_id }}" target="_blank"><img src="/public/admin_images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
                    </tr>
                {% endfor %}
                {% if listing_cardless_expense_sum !== 0 %}
                <tr id="listing_cardless_lastCol">
                  <td colspan="13" class="lastCell"> <font color="#ff0000">カードなし決済合計 &yen;<span id="listing_offer_cardless_expense_sum">{{ listing_cardless_expense_sum }}</span></font></td>
                </tr>
                {% endif %}
                <tr id="listing_lastCol">
                    <td colspan="13" class="lastCell">対象期間合計  &yen;<span id="listing_offer_expense_sum">{{ listing_expense_sum }}</span></td>
                </tr>
            {% else %}
            <tr class="lastCol">
                <td></td>
                <td></td>
                <td class="alignL">登録されているリスティング広告はありません。</td>
                <td class="alignL"></td>
                <td class="noWrap"></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
             </tr> 
            {% endif %}
            <tr class="lastCol" id="listing_deleted_all" style="display: none;">
                <td></td>
                <td></td>
                <td class="alignL">登録されているリスティング広告はありません。</td>
                <td class="alignL"></td>
                <td class="noWrap"></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
             </tr> 
        </table>
        <div class="clearfix">
            <ul class="listA01">
                <li><input type="button" class="formA02" value="リスティング広告の追加" onClick="javascript:location.href='/listing/make';"></li>
            </ul>
        </div>
    </div><!-- /.unitA01 -->

    <div class="unitA01">
        <div class="headingB01">
            <h3>レクタングル広告</h3>
            <span class="hyoujiArea"><a href="/customer/displayArea#area02" class="blank">表示エリアについて</a></span>
        </div>
        <ul class="formBlockA01">
            <li>対象期間</li>
            <li><select class="formA01 sfr" name="rectangle_scope">
                <option value="today">今日</option>
                <option value="yesterday">昨日</option>
                <option value="week">今週（月～）</option>
                <option value="seven_days">過去7日間：{{ one_week_ago }} 〜 {{ today }}</option>
                <option value="last_week">先週（月～日）</option>
                <option value="month">今月</option>
                <option value="thirty_days">過去30日間</option>
                <option value="last_month">先月</option>
                <option value="all">全期間</option>
                </select>
            <li><input type="button" class="formA01" value="適用" id="change_rectangle_scope"></li>
        </ul>
        <table class="tableA01">
            <tr>
                <th>操作</th>
                <th>ステータス</th>
                <th class="alignL">広告名</th>
                <th>表示回数</th>
                <th>表示頻度</th>
                <th>クリック数</th>
                <th>クリック率</th>
                <th>クリック単価</th>
                <th>上限予算</th>
                <th>費用</th>
                <th>プレビュー</th>
            </tr>
            {% if rectangle_count > 0 %}
                {% for rectangle_offer in rectangle_offers %}
                        <tr id="rectangle_offer_{{ rectangle_offer.id }}" data-offer_title="{{ rectangle_offer.title }}">
                            {% if rectangle_offer.status == "judging" %}
                                <td class="rectangle_offer_display_{{ rectangle_offer.id }}">
                                    <div class="status before_change">
                                        <span class="ttl"><img src="/public/admin_images/table_select_00_1.png" width="44" height="28" alt=""></span>
                                        <ul>
                                            <li id="rectangle_offer_delete_{{ rectangle_offer.id }}" data-offer_id="{{ rectangle_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                        </ul>
                                     </div>
                                </td>
                                <td class="rectangle_offer_display_{{ rectangle_offer.id }}">
                                    <span class="judge">
                                        審査中
                                    </span>
                                </td>
                            {% elseif rectangle_offer.status == "publishing" %}
                                    {% if rectangle_offer.is_available == 0 %}
                                <td class="rectangle_offer_display_{{ rectangle_offer.id }}">
                                    <div class="status before_change">
                                        <span class="ttl"><img src="/public/admin_images/table_select_00_1.png" width="44" height="28" alt=""></span>
                                        <ul>
                                            <li class="status_02"><a href="/rectangle/edit?offer_id={{ rectangle_offer.id }}&account_id={{ rectangle_offer.account_id }}" class="st2">編集</a></li>
                                            <li id="rectangle_offer_delete_{{ rectangle_offer.id }}" data-offer_id="{{ rectangle_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                        </ul>
                                    </div>
                                </td>
                                <td class="rectangle_offer_display_{{ rectangle_offer.id }}">
                                    <span class="unauthorized">
                                        上限予算到達
                                    </span>
                                </td>
                                    {% else %}
                                <td class="rectangle_offer_display_{{ rectangle_offer.id }}">
                                    <div class="status before_change">
                                        <span class="ttl"><img src="/public/admin_images/table_select_01_1.png" width="44" height="28" alt=""></span>
                                        <ul>
                                            <li class="status_02"><a href="/rectangle/edit?offer_id={{ rectangle_offer.id }}&account_id={{ rectangle_offer.account_id }}" class="st2">編集</a></li>
                                            <li id="rectangle_offer_cancel_{{ rectangle_offer.id }}" data-offer_id="{{ rectangle_offer.id }}" class="status_03"><a href="javascript:void(0);" class="st3">一時停止</a></li>                                        
                                            <li id="rectangle_offer_delete_{{ rectangle_offer.id }}" data-offer_id="{{ rectangle_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                        </ul>
                                    </div>
                                </td>
                                <td class="rectangle_offer_display_{{ rectangle_offer.id }}">
                                    <span class="publish">
                                        掲載中
                                    </span>
                                </td>
                                    {% endif %}
                            {% elseif rectangle_offer.status == "not_allowed" %}
                                <td class="rectangle_offer_display_{{ rectangle_offer.id }}">
                                    <div class="status before_change">
                                        <span class="ttl"><img src="/public/admin_images/table_select_00_1.png" width="44" height="28" alt=""></span>
                                        <ul>
                                            <li class="status_02"><a href="/rectangle/edit?offer_id={{ rectangle_offer.id }}&account_id={{ rectangle_offer.account_id }}" class="st2">編集</a></li>
                                            <li id="rectangle_offer_delete_{{ rectangle_offer.id }}" data-offer_id="{{ rectangle_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                        </ul>
                                    </div>
                                </td>
                                <td class="rectangle_offer_display_{{ rectangle_offer.id }}">
                                    <span class="unauthorized">
                                        非承認
                                    </span>
                                </td>
                           {% elseif rectangle_offer.status == "waiting_card" %}
                               {% if basic.creditcard_id != "" and basic.card_status == "before_approval" %}
                                    <td class="rectangle_offer_display_{{ rectangle_offer.id }}">
                                        <div class="status before_change">
                                            <img src="/public/admin_images/table_select_00_1.png" width="44" height="28" alt="">
                                        </div>
                                    </td>
                                    <td class="rectangle_offer_display_{{ rectangle_offer.id }}">
                                        <span class="unauthorized">
                                            カード審査中
                                        </span>
                                </td>
                                {% else %}
                                    <td class="rectangle_offer_display_{{ rectangle_offer.id }}">
                                        <div class="status before_change">
                                            <span class="ttl"><img src="/public/admin_images/table_select_00_1.png" width="44" height="28" alt=""></span>
                                            <ul>
                                                <li class="status_02"><a href="/rectangle/edit?offer_id={{ rectangle_offer.id }}&account_id={{ rectangle_offer.account_id }}" class="st2">編集</a></li>
                                                <li class="status_06"><a href="/credit/input" class="st2">カード登録</a></li>
                                                <li id="rectangle_offer_delete_{{ rectangle_offer.id }}" data-offer_id="{{ rectangle_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                    <td class="rectangle_offer_display_{{ rectangle_offer.id }}">
                                        <span class="wait">
                                            カード登録待ち
                                        </span>
                                    </td>
                                {% endif %}
                            {% elseif rectangle_offer.status == "editing" %}
                                <td class="rectangle_offer_display_{{ rectangle_offer.id }}">
                                    <div class="status before_change">
                                        <span class="ttl"><img src="/public/admin_images/table_select_02_1.png" width="44" height="28" alt=""></span>
                                        <ul>
                                            <li class="status_02"><a href="/rectangle/edit?offer_id={{ rectangle_offer.id }}&account_id={{ rectangle_offer.account_id }}" class="st2">編集</a></li>
                                            <li id="rectangle_offer_delete_{{ rectangle_offer.id }}" data-offer_id="{{ rectangle_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                        </ul>
                                    </div>
                                </td>
                                <td class="rectangle_offer_display_{{ rectangle_offer.id }}">
                                    <span class="edit">
                                        編集中
                                    </span>
                                </td>
                            {% elseif rectangle_offer.status == "canceled" %}
                                <td class="rectangle_offer_display_{{ rectangle_offer.id }}">
                                    <div class="status before_change">
                                        <span class="ttl"><img src="/public/admin_images/table_select_03_1.png" width="44" height="28" alt=""></span>
                                        <ul>
                                            <li id="rectangle_offer_effect_{{ rectangle_offer.id }}" data-offer_id="{{ rectangle_offer.id }}" class="status_01"><a href="javascript:void(0);" class="st1">有効</a></li>                                       
                                            <li class="status_02"><a href="/rectangle/edit?offer_id={{ rectangle_offer.id }}&account_id={{ rectangle_offer.account_id }}" class="st2">編集</a></li>
                                            <li id="rectangle_offer_delete_{{ rectangle_offer.id }}" data-offer_id="{{ rectangle_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                        </ul>
                                    </div>
                                </td>
                                <td class="rectangle_offer_display_{{ rectangle_offer.id }}">
                                    <span class="stop">
                                        一時停止中
                                    </span>
                                </td>
                             {% endif %}
                        <td class="rectangle_offer_publishing_{{ rectangle_offer.id }}" style="display: none;">
                            <div class="status publishing">
                                <span class="ttl"><img src="/public/admin_images/table_select_01_1.png" width="44" height="28" alt=""></span>
                                <ul>
                                    <li class="status_02"><a href="/rectangle/edit?offer_id={{ rectangle_offer.id }}&account_id={{ rectangle_offer.account_id }}" class="st2">編集</a></li>
                                    <li id="rectangle_offer_cancel_{{ rectangle_offer.id }}" data-offer_id="{{ rectangle_offer.id }}" class="status_03"><a href="javascript:void(0);" class="st3">一時停止</a></li>                                                                                     
                                    <li id="rectangle_offer_delete_{{ rectangle_offer.id }}" data-offer_id="{{ rectangle_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                </ul>
                            </div>
                        </td>
                        <td class="rectangle_offer_publishing_{{ rectangle_offer.id }}" style="display: none;">
                            <span class="publish">
                                掲載中
                            </span>
                        </td>
                        <td class="rectangle_offer_waiting_card_{{ rectangle_offer.id }}" style="display :none;">
                            <div class="status waiting_card">
                                <span class="ttl"><img src="/public/admin_images/table_select_00_1.png" width="44" height="28" alt=""></span>
                                <ul>
                                    <li class="status_02"><a href="/rectangle/edit?offer_id={{ rectangle_offer.id }}&account_id={{ rectangle_offer.account_id }}" class="st2">編集</a></li>
                                    <li id="rectangle_offer_delete_{{ rectangle_offer.id }}" data-offer_id="{{ rectangle_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                </ul>
                            </div>
                        </td>
                        <td class="rectangle_offer_waiting_card_{{ rectangle_offer.id }}" style="display :none;">
                            <span class="wait">
                                カード登録待ち
                            </span>
                        </td>
                        <td class="rectangle_offer_canceled_{{ rectangle_offer.id }}" style="display: none;">
                            <div class="status canceled">
                                <span class="ttl"><img src="/public/admin_images/table_select_03_1.png" width="44" height="28" alt=""></span>
                                <ul>
                                    <li id="rectangle_offer_effect_{{ rectangle_offer.id }}" data-offer_id="{{ rectangle_offer.id }}" class="status_01"><a href="javascript:void(0);" class="st1">有効</a></li>                                       
                                    <li class="status_02"><a href="/rectangle/edit?offer_id={{ rectangle_offer.id }}&account_id={{ rectangle_offer.account_id }}" class="st2">編集</a></li>
                                    <li id="rectangle_offer_delete_{{ rectangle_offer.id }}" data-offer_id="{{ rectangle_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                </ul>
                            </div>
                        </td>
                        <td class="rectangle_offer_canceled_{{ rectangle_offer.id }}" style="display: none;">
                            <span class="stop">
                                一時停止中
                            </span>
                        </td>
                        <td class="alignL">{{ rectangle_offer.title }}</td>
                        <td id="rectangle_offer_impression_log_{{ rectangle_offer.id }}">{{ rectangle_offer.impression_log }}</td>
                        <td class="rectangle_offer_impression_ratio_{{ rectangle_offer.id }}">
                            {% if rectangle_offer.impression_ratio == "error" %}
                                -
                            {% elseif rectangle_offer.impression_ratio >= 80 %}
                                <span class="freq_1">最高</span>
                            {% elseif rectangle_offer.impression_ratio >= 60 %}
                                <span class="freq_2">高</span>
                            {% elseif rectangle_offer.impression_ratio >= 40 %}
                                <span class="freq_3">普通</span>
                            {% elseif rectangle_offer.impression_ratio >= 0 %}
                                <span class="freq_4">低</span>                                
                            {% endif %}
                        </td>
                        <td class="rectangle_offer_impression_ratio_{{ rectangle_offer.id }}" id="rectangle_offer_impression_ratio_highest_{{ rectangle_offer.id }}" style="display: none;">
                            <span class="freq_1">最高</span>
                        </td>
                        <td class="rectangle_offer_impression_ratio_{{ rectangle_offer.id }}" id="rectangle_offer_impression_ratio_high_{{ rectangle_offer.id }}" style="display: none;">
                            <span class="freq_2">高</span>
                        </td>
                        <td class="rectangle_offer_impression_ratio_{{ rectangle_offer.id }}" id="rectangle_offer_impression_ratio_middle_{{ rectangle_offer.id }}" style="display: none;">
                            <span class="freq_3">普通</span>
                        </td>
                        <td class="rectangle_offer_impression_ratio_{{ rectangle_offer.id }}" id="rectangle_offer_impression_ratio_low_{{ rectangle_offer.id }}" style="display: none;">
                            <span class="freq_4">低</span>
                        </td>
                        <td class="rectangle_offer_impression_ratio_{{ rectangle_offer.id }}" id="rectangle_offer_impression_ratio_error_{{ rectangle_offer.id }}" style="display: none;">
                            -
                        </td>
                        <td id="rectangle_offer_click_log_{{ rectangle_offer.id }}">{{ rectangle_offer.click_log }}</td>
                        <td>
                            {% if rectangle_offer.click_ratio === false %}
                                <span id="rectangle_offer_click_ratio_{{ rectangle_offer.id }}">-</span>%
                            {% else %}
                                <span id="rectangle_offer_click_ratio_{{ rectangle_offer.id }}">{{ rectangle_offer.click_ratio }}</span>%
                            {% endif %}
                        </td>
                        <td>
                            {% if rectangle_offer.without_card == 1 %}
                                <font color="#ff0000">&yen;{{ rectangle_offer.click_price }}</font>
                            {% else %}
                                &yen;{{ rectangle_offer.click_price }}
                            {% endif %}
                        </td>
                        <td>
                            {% if rectangle_offer.without_card == 1 %}
                                <font color="#ff0000">&yen;{{ rectangle_offer.budget_max }}</font>
                            {% else %}
                                &yen;{{ rectangle_offer.budget_max }}
                            {% endif %}
                        </td>
                        <td>
                            {% if rectangle_offer.without_card == 1 %}
                              <font color="#ff0000">&yen;<span id="rectangle_offer_expense_{{ rectangle_offer.id }}">{{ rectangle_offer.expense }}</span></font>
                            {% else %}
                              &yen;<span id="rectangle_offer_expense_{{ rectangle_offer.id }}">{{ rectangle_offer.expense }}</span>
                            {% endif %}
                        </td>
                        <td><a href="/rectangle/preview?offer_id={{ rectangle_offer.id }}&account_id={{ rectangle_offer.account_id }}" target="_blank"><img src="/public/admin_images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
                    </tr>
                {% endfor %}
                {% if rectangle_cardless_expense_sum !== 0 %}
                <tr id="rectangle_cardless_lastCol">
                   <td colspan="11" class="lastCell"> <font color="#ff0000">カードなし決済合計 &yen;<span id="rectangle_offer_cardless_expense_sum">{{ rectangle_cardless_expense_sum }}</span></font></td>
                </tr>
                {% endif %}
                <tr id="rectangle_lastCol">
                    <td colspan="11" class="lastCell">対象期間合計　&yen;<span id="rectangle_offer_expense_sum">{{ rectangle_expense_sum }}</span></td>
                </tr>
            {% else %}
                <tr class="lastCol">
                    <td></td>
                    <td></td>
                    <td class="alignL">登録されているレクタングル広告はありません。</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            {% endif %}
                <tr class="lastCol" id="rectangle_deleted_all" style="display: none;">
                    <td></td>
                    <td></td>
                    <td class="alignL">登録されているレクタングル広告はありません。</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
        </table>
        <div class="clearfix">
            <ul class="listA01">
                <li><input type="button" class="formA02" value="レクタングル広告の追加" onClick="javascript:location.href='/rectangle/make';"></li>
            </ul>
        </div>
    </div><!-- /.unitA01 -->

    <div class="unitA01">
        <div class="headingB01">
            <h3>ピックアップ広告</h3>
            <span class="hyoujiArea"><a href="/customer/displayArea#area03" class="blank">表示エリアについて</a></span>
        </div>
        <table class="tableA01">
            <tr>
                <th>操作</th>
                <th>ステータス</th>
                <th class="alignL">募集職種</th>
                <th class="alignL">募集会社名</th>
                <th>新着オプション</th>
                <th>詳細ページ</th>
                <th>クリック数</th>
                <th>掲載期間</th>
                <th>費用</th>
                <th>プレビュー</th>
            </tr>
            {% if pickup_count > 0 %}
                {% for pickup_offer in pickup_offers %}
                        <tr id="pickup_offer_{{ pickup_offer.id }}" data-offer_title="{{ pickup_offer.title }}">
                            {% if pickup_offer.status == "judging" %}
                                <td class="pickup_offer_display_{{ pickup_offer.id }}">
                                    <div class="status before_change">
                                        <span class="ttl"><img src="/public/admin_images/table_select_00_1.png" width="44" height="28" alt=""></span>
                                        <ul>
                                            <li id="pickup_offer_delete_{{ pickup_offer.id }}" data-offer_id="{{ pickup_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                        </ul>
                                    </div>
                                </td>
                                <td class="pickup_offer_display_{{ pickup_offer.id }}">
                                   <span class="judge">審査中</span>
                                </td>
                            {% elseif pickup_offer.status == "publishing" %}
                                <td class="pickup_offer_display_{{ pickup_offer.id }}">
                                    <div class="status before_change">
                                        <span class="ttl"><img src="/public/admin_images/table_select_01_1.png" width="44" height="28" alt=""></span>
                                        <ul>
                                            <li class="status_02"><a href="/pickup/edit?offer_id={{ pickup_offer.id }}&account_id={{ pickup_offer.account_id }}" class="st2">編集</a></li>
                                            <li id="pickup_offer_cancel_{{ pickup_offer.id }}" data-offer_id="{{ pickup_offer.id }}" class="status_03"><a href="javascript:void(0);" class="st3">一時停止</a></li>
                                            <li id="pickup_offer_complete_{{ pickup_offer.id }}" data-offer_id="{{ pickup_offer.id }}" class="status_05"><a href="javascript:void(0);" class="st5">掲載終了</a></li>
                                            <li id="pickup_offer_delete_{{ pickup_offer.id }}" data-offer_id="{{ pickup_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                        </ul>
                                    </div>
                                </td>
                                <td class="pickup_offer_display_{{ pickup_offer.id }}">
                                    <span class="publish">掲載中</span>
                                </td>
                            {% elseif pickup_offer.status == "will_publish" %}
                                <td class="pickup_offer_display_{{ pickup_offer.id }}">
                                    <div class="status before_change">
                                        <span class="ttl"><img src="/public/admin_images/table_select_00_1.png" width="44" height="28" alt=""></span>
                                        <ul>
                                            <li class="status_02"><a href="/pickup/edit?offer_id={{ pickup_offer.id }}&account_id={{ pickup_offer.account_id }}" class="st2">編集</a></li>
                                            <li id="pickup_offer_delete_{{ pickup_offer.id }}" data-offer_id="{{ pickup_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                        </ul>
                                    </div>
                                </td>
                                <td class="pickup_offer_display_{{ pickup_offer.id }}">
                                    <span class="wait">{{ pickup_offer.published_from }}より掲載予定</span>
                                </td>
                            {% elseif pickup_offer.status == "not_allowed" %}
                                <td class="pickup_offer_display_{{ pickup_offer.id }}">
                                    <div class="status before_change">
                                        <span class="ttl"><img src="/public/admin_images/table_select_00_1.png" width="44" height="28" alt=""></span>
                                        <ul>
                                            <li class="status_02"><a href="/pickup/edit?offer_id={{ pickup_offer.id }}&account_id={{ pickup_offer.account_id }}" class="st2">編集</a></li>
                                            <li id="pickup_offer_delete_{{ pickup_offer.id }}" data-offer_id="{{ pickup_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                        </ul>
                                    </div>
                                </td>
                                <td class="pickup_offer_display_{{ pickup_offer.id }}">
                                    <span class="unauthorized">非承認</span>
                                </td>
                            {% elseif pickup_offer.status == "waiting_card" %}
                                {% if basic.creditcard_id != "" and basic.card_status == "before_approval" %}
                                    <td class="pickup_offer_display_{{ pickup_offer.id }}">
                                        <div class="status before_change">
                                            <img src="/public/admin_images/table_select_00_1.png" width="44" height="28" alt="">
                                        </div>
                                    </td>
                                    <td class="pickup_offer_display_{{ pickup_offer.id }}">
                                        <span class="unauthorized">
                                            カード審査中
                                        </span>
                                    </td>
                                {% else %}
                                    <td class="pickup_offer_display_{{ pickup_offer.id }}">
                                        <div class="status before_change">
                                            <span class="ttl"><img src="/public/admin_images/table_select_00_1.png" width="44" height="28" alt=""></span>
                                            <ul>
                                                <li class="status_02"><a href="/pickup/edit?offer_id={{ pickup_offer.id }}&account_id={{ pickup_offer.account_id }}" class="st2">編集</a></li>
                                                <li class="status_06"><a href="/credit/input" class="st2">カード登録</a></li>
                                                <li id="pickup_offer_delete_{{ pickup_offer.id }}" data-offer_id="{{ pickup_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                            </ul>
                                        </div>
                                    </td>    
                                    <td class="pickup_offer_display_{{ pickup_offer.id }}">
                                        <span class="wait">カード登録待ち</span>
                                    </td>
                                {% endif %}
                            {% elseif pickup_offer.status == "completed" %}
                                <td class="pickup_offer_display_{{ pickup_offer.id }}">
                                    <div class="status before_change">
                                        <span class="ttl"><img src="/public/admin_images/table_select_00_1.png" width="44" height="28" alt=""></span>
                                        <ul>
                                            <li class="status_01"><a href="/pickup/edit?offer_id={{ pickup_offer.id }}&account_id={{ pickup_offer.account_id }}" class="st1">再掲載</a></li>                                       
                                            <li id="pickup_offer_delete_{{ pickup_offer.id }}" data-offer_id="{{ pickup_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                        </ul>
                                    </div>
                                </td>    
                                <td class="pickup_offer_display_{{ pickup_offer.id }}">
                                    <span class="stop">掲載終了</span>
                                </td>
                            {% elseif pickup_offer.status == "editing" %}
                                <td class="pickup_offer_display_{{ pickup_offer.id }}">
                                    <div class="status before_change">
                                        <span class="ttl"><img src="/public/admin_images/table_select_02_1.png" width="44" height="28" alt=""></span>
                                        <ul>
                                            <li class="status_02"><a href="/pickup/edit?offer_id={{ pickup_offer.id }}&account_id={{ pickup_offer.account_id }}" class="st2">編集</a></li>
                                            <li id="pickup_offer_delete_{{ pickup_offer.id }}" data-offer_id="{{ pickup_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                        </ul>
                                    </div>
                                </td>    
                                <td class="pickup_offer_display_{{ pickup_offer.id }}">
                                    <span class="edit">編集中</span>
                                </td>
                            {% elseif pickup_offer.status == "canceled" %}   
                                <td class="pickup_offer_display_{{ pickup_offer.id }}">
                                    <div class="status before_change">
                                        <span class="ttl"><img src="/public/admin_images/table_select_03_1.png" width="44" height="28" alt=""></span>
                                        <ul>
                                            <li id="pickup_offer_effect_{{ pickup_offer.id }}" data-offer_id="{{ pickup_offer.id }}" class="status_01"><a href="javascript:void(0);" class="st1">再掲載</a></li>
                                            <li class="status_02"><a href="/pickup/edit?offer_id={{ pickup_offer.id }}&account_id={{ pickup_offer.account_id }}" class="st2">編集</a></li> 
                                            <li id="pickup_offer_delete_{{ pickup_offer.id }}" data-offer_id="{{ pickup_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                        </ul>
                                    </div>
                                </td>    
                                <td class="pickup_offer_display_{{ pickup_offer.id }}">
                                    <span class="stop">一時停止中</span>
                                </td>
                            {% endif %}
                            <td class="pickup_offer_publishing_{{ pickup_offer.id }}" style="display: none;">
                                <div class="status publishing">
                                    <span class="ttl"><img src="/public/admin_images/table_select_01_1.png" width="44" height="28" alt=""></span>
                                    <ul>
                                        <li class="status_02"><a href="/pickup/edit?offer_id={{ pickup_offer.id }}&account_id={{ pickup_offer.account_id }}" class="st2">編集</a></li>
                                        <li id="pickup_offer_cancel_{{ pickup_offer.id }}" data-offer_id="{{ pickup_offer.id }}" class="status_03"><a href="javascript:void(0);" class="st3">一時停止</a></li>
                                        <li id="pickup_offer_complete_{{ pickup_offer.id }}" data-offer_id="{{ pickup_offer.id }}" class="status_05"><a href="javascript:void(0);" class="st5">掲載終了</a></li>
                                        <li id="pickup_offer_delete_{{ pickup_offer.id }}" data-offer_id="{{ pickup_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                    </ul>
                                </div>
                            </td>
                            <td class="pickup_offer_publishing_{{ pickup_offer.id }}" style="display: none;">
                                <span class="publish">掲載中</span>
                            </td>
                            <td class="pickup_offer_waitng_card_{{ pickup_offer.id }}" style="display: none;">
                                <div class="status waiting_card">
                                    <span class="ttl"><img src="/public/admin_images/table_select_00_1.png" width="44" height="28" alt=""></span>
                                    <ul>
                                        <li class="status_02"><a href="/pickup/edit?offer_id={{ pickup_offer.id }}&account_id={{ pickup_offer.account_id }}" class="st2">編集</a></li>
                                        <li id="pickup_offer_delete_{{ pickup_offer.id }}" data-offer_id="{{ pickup_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                    </ul>
                                </div>
                            </td>    
                            <td class="pickup_offer_waiting_card_{{ pickup_offer.id }}" style="display: none;">
                                <span class="wait">カード登録待ち</span>
                            </td>
                            <td class="pickup_offer_canceled_{{ pickup_offer.id }}" style="display: none;">
                                <div class="status canceled">
                                    <span class="ttl"><img src="/public/admin_images/table_select_03_1.png" width="44" height="28" alt=""></span>
                                    <ul>
                                        <li id="pickup_offer_effect_{{ pickup_offer.id }}" data-offer_id="{{ pickup_offer.id }}" class="status_01"><a href="javascript:void(0);" class="st1">再掲載</a></li>
                                        <li class="status_02"><a href="/pickup/edit?offer_id={{ pickup_offer.id }}&account_id={{ pickup_offer.account_id }}" class="st2">編集</a></li> 
                                        <li id="pickup_offer_delete_{{ pickup_offer.id }}" data-offer_id="{{ pickup_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                    </ul>
                                </div>
                            </td>    
                            <td class="pickup_offer_canceled_{{ pickup_offer.id }}" style="display: none;">
                                <span class="stop">一時停止中</span>
                            </td>
                            <td class="pickup_offer_completed_{{ pickup_offer.id }}" style="display: none;">
                                <div class="status completed">
                                    <span class="ttl"><img src="/public/admin_images/table_select_00_1.png" width="44" height="28" alt=""></span>
                                    <ul>
                                        <li class="status_01"><a href="/pickup/edit?offer_id={{ pickup_offer.id }}&account_id={{ pickup_offer.account_id }}" class="st1">再掲載</a></li>                                       
                                        <li id="pickup_offer_delete_{{ pickup_offer.id }}" data-offer_id="{{ pickup_offer.id }}" class="status_04"><a href="javascript:void(0);" class="st4">削除</a></li>
                                    </ul>
                                </div>
                            </td>    
                            <td class="pickup_offer_completed_{{ pickup_offer.id }}" style="display: none;">
                                <span class="stop">掲載終了</span>
                            </td>
                            <td class="alignL">{{ pickup_offer.job_category }}</td>
                            <td class="alignL">{{ pickup_offer.company_name }}</td>
                            <td>
                                {% if pickup_offer.display_area == "inner" %}
                                    インナーパネル
                                {% elseif pickup_offer.display_area == "inner_half" %}
                                    インナーパネルハーフ
                                {% elseif pickup_offer.display_area == "right_column" %}
                                    右カラム
                                {% elseif pickup_offer.display_area == "none" %}
                                    無し
                                {% endif %}
                            </td>
                            <td>{% if pickup_offer.detail_content_type == "none" %}<span class="freq_4">無</span>{% else %}<span class="freq_1">有</span>{% endif %}</td>
                            <td>{{ pickup_offer.click_log }}</td>
                            <td>
                            
                            <span id="pickup_offer_publish_week_{{ pickup_offer.id }}">{{ pickup_offer.publish_week }}</span>週 {% if pickup_offer.status == "publishing" or pickup_offer.status == "will_publish" or pickup_offer.status == "canceled" or pickup_offer.status == "completed" %}
                             <span id="pickup_offer_published_to_{{ pickup_offer.id }}">（{{ pickup_offer.published_from }}～{{ pickup_offer.published_to}})</span>
                            {% else %}
                             {#（{{ pickup_offer.published_from }}～{{ pickup_offer.published_to}})#}
                            {% endif %}
                            
                            
                            </td>
                            <td>
                                {% if pickup_offer.without_card == 1 %}
                                  <font color="#ff0000">&yen;<span id="pickup_offer_expense_{{ pickup_offer.id }}">{{ pickup_offer.expense }}</span></font>
                                {% else %}
                                  &yen;<span id="pickup_offer_expense_{{ pickup_offer.id }}">{{ pickup_offer.expense }}</span>
                                {% endif %}
                            </td>
                            <td><a href="/pickup/preview?offer_id={{ pickup_offer.id }}&account_id={{ pickup_offer.account_id }}" target="_blank"><img src="/public/admin_images/form_bg_preview_n.png" width="40" height="28" alt=""></a></td>
                        </tr>
                {% endfor %}
                 {% if pickup_cardless_expense_sum !== 0 %}
                 <tr id="pickup_cardless_lastCol">
                   <td colspan="10" class="lastCell"> <font color="#ff0000">カードなし決済合計 &yen;<span id="pickup_offer_cardless_expense_sum">{{ pickup_cardless_expense_sum }}</span></font></td>
                </tr>
                 {% endif %}
                <tr id="pickup_lastCol">
                    <td colspan="10" class="lastCell">対象期間合計　&yen;<span id="pickup_offer_expense_sum">{{ pickup_expense_sum }}</span></td>
                </tr>
            {% else %}
                <tr class="lastCol">
                    <td>
                        <div class="status"></div>
                    </td>
                    <td><span class="wait"></span></td>
                    <td class="alignL">登録されているピックアップ広告はありません</td>
                    <td class="alignL"></td>
                    <td></td>
                    <td><span class="freq_1"></span></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            {% endif %}
                <tr class="lastCol" id="pickup_deleted_all" style="display: none;">
                    <td>
                        <div class="status"></div>
                    </td>
                    <td></td>
                    <td class="alignL">登録されているピックアップ広告はありません</td>
                    <td class="alignL"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
        </table>
        <div class="clearfix">
            <ul class="listA01">
                <li><input type="button" class="formA02" value="ピックアップ広告の追加" onClick="javascript:location.href='/pickup/makeStart';"></li>
            </ul>
        </div>
    </div><!-- /.unitA01 -->


<!-- InstanceEndEditable -->
</div><!-- / contentsContainer -->
</div><!-- / contents -->
