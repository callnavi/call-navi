<section class="unitA01">
<section>
<p class="uniqueAbout01"><img src="/public/images/about/index_img_01.gif" width="720" height="537" alt="" class="mediaPC" /><img src="/public/images/about/index_img_01_02.png" width="100%" height="" alt="" class="mediaSP" /></p>
<p>コールナビは様々な媒体の垣根を越えて、コールセンターの求人をまとめて検索できる<span class="textA01">『求人情報一括検索サービス』</span>です。<br>正社員でもアルバイトでも、働く場所や給与、時間帯など、出来る事なら自分の理想の環境で働きたいと思うのが自然です。しかし近年、求人媒体は増える一方。ついつい面倒で、大手の媒体だけで検索していませんか？</p>
</section>

<section>
<p>コールナビはWEB上のコールセンター求人だけをまとめて検索可能にすることで、手間なく、スピーディに、あなたにとってベストなお仕事との出会いを演出します。</p>
</section>

<section>
<h2 class="headingE01 uniqueAboutHeading"><img src="/public/images/about/index_ico_01.gif" width="54" height="54" alt="メリット1"/>さまざまな条件からぴったりの仕事が見つかる</h2>
<div class="imageBlockA01 uniqueAbout02 merit1">
<p class="image"><img src="/public/images/about/index_img_02.gif" width="145" height="139" alt="" class="mediaPC" /><img src="/public/images/about/index_img_02_02.png" width="132" height="" alt="" class="mediaSP" /></p>
<div class="contentsInner valignT">
<p>コールセンター業務といっても、扱う内容や勤務時間帯などによって細分化されていきます。コールナビは様々な雇用形態、勤務時間帯、商材のお仕事を通して、あなたにぴったりの仕事・アルバイトが見つかります。</p>
</div><!-- / contentsInner -->
</div><!-- / imageBlockA01 -->
</section>

<section>
<h2 class="headingE01 uniqueAboutHeading"><img src="/public/images/about/index_ico_02.gif" width="54" height="54" alt="メリット2"/>仕事選びの上での最重要項目を一覧化</h2>
<div class="imageBlockA01 uniqueAbout02 merit2">
<p class="image"><img src="/public/images/about/index_img_03.gif" width="118" height="125" alt="" class="mediaPC" /><img src="/public/images/about/index_img_03_02.png" width="112" height="" alt="" class="mediaSP" /></p>
<div class="contentsInner valignT">
<p>社名、アピールポイント、給与、勤務時間帯、勤務地を一目で把握できるように一覧化することで、よりスピーディな仕事探しをお手伝いします。</p>
</div><!-- / contentsInner -->
</div><!-- / imageBlockA01 -->
</section>

<section>
<h2 class="headingE01 uniqueAboutHeading"><img src="/public/images/about/index_ico_03.gif" width="54" height="54" alt="メリット3"/>会員登録不要、コールセンター求人のための「検索エンジン」</h2>
<div class="imageBlockA01 uniqueAbout02">
<p class="image"><img src="/public/images/about/index_img_04.gif" width="110" height="94" alt="" class="mediaPC" /><img src="/public/images/about/index_img_04_02.png" width="124" height="" alt="" class="mediaSP" /></p>
<div class="contentsInner valignT">
<p>コールナビは会員登録不要の「検索エンジン」なので、キーワードを打ち込んですぐに検索が可能です。もちろんメールや郵送物が届かないから、気軽に仕事探しが出来ます。</p>
</div><!-- / contentsInner -->
</div><!-- / imageBlockA01 -->
</section>
</section>
