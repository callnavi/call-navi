
<section class="unitA01">
<h2 class="headingC02">運営会社</h2>
<table class="tableB01">
<col style="width:150px">
<col>
<tr>
<th>商　号</th>
<td> 株式会社コールナビ［英文表記：CALL Navi Inc.］</td>
</tr>
<tr>
<th>所在地</th>
<td>東京都新宿区高田馬場2-13-2</td>
</tr>
<tr>
<th>設　立</th>
<td>平成27年4月1日</td>
</tr>
<tr>
<th>事業内容</th>
<td>メディアサイト運営</td>
</tr>
<tr>
<th>代表取締役</th>
<td>山本 桂</td>
</tr>
<tr>
<th>資本金</th>
<td>300万円</td>
</tr>
<tr>
<th>決算期</th>
<td>3月</td>
</tr>
<tr>
<th>HP</th>
<td><a href="http://corp.callnavi.jp/" target="_blank">http://corp.callnavi.jp/</a></td>
</tr>
</table>
</section>