<div id="module_view">
<div id="mainContents">
<p>※実際に使用する際には、各見出しの下に余白が付きます。<br />
※h2のほか、h1やh3以下のタグを使用しても同様のスタイルが適用されます。</p>

<section>
<div class="moduleTextB01">見出し [headingA01]</div>
<h2 class="headingA01 marginOff">テキストテキストテキスト</h2>
<textarea class="source singleLine" readonly>&lt;h2 class=&quot;headingA01&quot;&gt;&lt;/h2&gt;</textarea>

<div class="moduleTextB01">見出し [headingA02]</div>
<h2 class="headingA02 marginOff">テキストテキストテキスト</h2>
<textarea class="source singleLine" readonly>&lt;h2 class=&quot;headingA02&quot;&gt;&lt;/h2&gt;</textarea>

<div class="moduleTextB01">見出し [headingB01]</div>
<h2 class="headingB01 marginOff">テキストテキストテキスト</h2>
<textarea class="source singleLine" readonly>&lt;h2 class=&quot;headingB01&quot;&gt;&lt;/h2&gt;</textarea>

<div class="moduleTextB01">見出し [headingB02]</div>
<h2 class="headingB02 marginOff">テキストテキストテキスト</h2>
<textarea class="source singleLine" readonly>&lt;h2 class=&quot;headingB02&quot;&gt;&lt;/h2&gt;</textarea>

<div class="moduleTextB01">見出し [headingC01]</div>
<h2 class="headingC01 marginOff">テキストテキストテキスト</h2>
<textarea class="source singleLine" readonly>&lt;h2 class=&quot;headingC01&quot;&gt;&lt;/h2&gt;</textarea>

<div class="moduleTextB01">見出し [headingC02]</div>
<h2 class="headingC02 marginOff">テキストテキストテキスト</h2>
<textarea class="source singleLine" readonly>&lt;h2 class=&quot;headingC02&quot;&gt;&lt;/h2&gt;</textarea>

<div class="moduleTextB01">見出し [headingD01]</div>
<h2 class="headingD01 marginOff">テキストテキストテキスト</h2>
<textarea class="source singleLine" readonly>&lt;h2 class=&quot;headingD01&quot;&gt;&lt;/h2&gt;</textarea>

<div class="moduleTextB01">見出し（サムネイル付） [headingE01]</div>
<h2 class="headingE01 marginOff"><img src="/public/images/dummy/dummy_gray.gif" width="54" height="54" alt="">テキストテキストテキスト</h2>
<textarea class="source singleLine" readonly>&lt;h2 class=&quot;headingE01&quot;&gt;&lt;img src=&quot;&quot; width=&quot;&quot; height=&quot;&quot; alt=&quot;&quot;&gt;テキスト&lt;/h2&gt;</textarea>

<div class="moduleTextB01">見出し [headingF01]</div>
<h2 class="headingF01 marginOff">テキストテキストテキスト</h2>
<textarea class="source singleLine" readonly>&lt;h2 class=&quot;headingF01&quot;&gt;&lt;/h2&gt;</textarea>

<div class="moduleTextB01">見出し [headingG01]</div>
<h2 class="headingG01 marginOff">テキストテキストテキスト</h2>
<textarea class="source singleLine" readonly>&lt;h2 class=&quot;headingG01&quot;&gt;&lt;/h2&gt;</textarea>

</section>
</div><!-- / mainContents -->
</div>
