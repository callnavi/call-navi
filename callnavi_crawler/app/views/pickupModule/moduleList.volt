<div id="module_view">
<div id="mainContents">

<section>
<div class="moduleTextB01">リンクリスト [linkListA01]</div>
<ul class="linkListA01">
<li><a href="#">テキスト</a></li>
<li><a href="#">テキスト</a></li>
<li><a href="#">テキスト</a></li>
<li><a href="#">テキスト</a></li>
</ul>
<textarea class="source fourLines" readonly>&lt;ul class=&quot;linkListA01&quot;&gt;
&lt;li&gt;&lt;/li&gt;
&lt;li&gt;&lt;/li&gt;
&lt;/ul&gt;</textarea>

<div class="moduleTextB01">バナーリスト（余白有） [bnrListA01]</div>
<ul class="bnrListA01">
<li><img src="/public/images/dummy/dummy_gray.gif" width="200" height="50" alt=""/></li>
<li><img src="/public/images/dummy/dummy_gray.gif" width="200" height="50" alt=""/></li>
<li><img src="/public/images/dummy/dummy_gray.gif" width="200" height="50" alt=""/></li>
</ul>
<textarea class="source fourLines" readonly>&lt;ul class=&quot;bnrListA01&quot;&gt;
&lt;li&gt;&lt;img src=&quot;&quot; width=&quot;&quot; height=&quot;&quot; alt=&quot;&quot;&gt;&lt;/li&gt;
&lt;li&gt;&lt;img src=&quot;&quot; width=&quot;&quot; height=&quot;&quot; alt=&quot;&quot;&gt;&lt;/li&gt;
&lt;/ul&gt;</textarea>

<div class="moduleTextB01">バナーリスト（余白無） [bnrListB01]</div>
<ul class="bnrListB01">
<li><img src="/public/images/dummy/dummy_gray.gif" width="200" height="50" alt=""/></li>
<li><img src="/public/images/dummy/dummy_gray.gif" width="200" height="50" alt=""/></li>
<li><img src="/public/images/dummy/dummy_gray.gif" width="200" height="50" alt=""/></li>
</ul>
<textarea class="source fourLines" readonly>&lt;ul class=&quot;bnrListB01&quot;&gt;
&lt;li&gt;&lt;img src=&quot;&quot; width=&quot;&quot; height=&quot;&quot; alt=&quot;&quot;&gt;&lt;/li&gt;
&lt;li&gt;&lt;img src=&quot;&quot; width=&quot;&quot; height=&quot;&quot; alt=&quot;&quot;&gt;&lt;/li&gt;
&lt;/ul&gt;</textarea>

<div class="moduleTextB01">タグリスト [tagListA01]</div>
<ul class="tagListA01">
<li>テキスト</li>
<li>テキスト</li>
<li>テキスト</li>
</ul>
<textarea class="source fourLines" readonly>&lt;ul class=&quot;tagListA01&quot;&gt;
&lt;li&gt;&lt;/li&gt;
&lt;li&gt;&lt;/li&gt;
&lt;/ul&gt;</textarea>
</section>

</div><!-- / mainContents -->
</div>