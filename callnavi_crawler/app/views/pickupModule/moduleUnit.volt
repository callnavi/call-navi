<div id="module_view">

<div id="mainContents">

<section>
<div class="moduleTextB01">汎用ユニット（白背景）[.unitA01]</div>
<div class="unitA01" style="border: 1px solid red;">
<p>padding:30px 40px 20px</p>
<p class="">※わかりやすくするために赤い枠線を付けています。実際には枠線は付きません。</p>
</div>
<textarea class="source singleLine" readonly>&lt;div class=&quot;unitA01&quot;&gt;&lt;/div&gt;</textarea>

<div class="moduleTextB01">汎用ユニット（白背景・余白狭）[.unitA02]</div>
<div class="unitA02" style="border: 1px solid red;">
<p>padding:20px 20px 10px</p>
<p class="">※わかりやすくするために赤い枠線を付けています。実際には枠線は付きません。</p>
</div>
<textarea class="source singleLine" readonly>&lt;div class=&quot;unitA02&quot;&gt;&lt;/div&gt;</textarea>

<div class="moduleTextB01" >汎用ユニット（ブルー背景）[.unitB01]</div>
<div class="unitB01">
<p>padding:30px 40px 20px</p>
</div>
<textarea class="source singleLine" readonly>&lt;div class=&quot;unitB01&quot;&gt;&lt;/div&gt;</textarea>
</section>

</div><!-- / mainContents -->   
</div>