<div id="module_view">
<div id="mainContents">

<section>
<div class="moduleTextB01">ボタン [btnA01]</div>
<div><a href="#" class="btnA01">テキスト</a></div>
<textarea class="source singleLine" readonly>&lt;div&gt;&lt;a href=&quot;#&quot; class=&quot;btnA01&quot;&gt;&lt;/a&gt;&lt;/div&gt;</textarea>

<div class="moduleTextB01">ボタン [btnA02]</div>
<div><a href="#" class="btnA02"><span>テキスト</span></a></div>
<textarea class="source singleLine" readonly>&lt;div&gt;&lt;a href=&quot;#&quot; class=&quot;btnA02&quot;&gt;&lt;span&gt;&lt;/span&gt;&lt;/a&gt;&lt;/div&gt;</textarea>

<div class="moduleTextB01">ボタン [btnB01]</div>
<div><a href="#" class="btnB01"><span>テキスト</span></a></div>
<textarea class="source singleLine" readonly>&lt;div&gt;&lt;a href=&quot;#&quot; class=&quot;btnB01&quot;&gt;&lt;span&gt;&lt;/span&gt;&lt;/a&gt;&lt;/div&gt;</textarea>

<div class="moduleTextB01">ボタン [btnB02]</div>
<div><a href="#" class="btnB02"><span>テキスト</span></a></div>
<textarea class="source singleLine" readonly>&lt;div&gt;&lt;a href=&quot;#&quot; class=&quot;btnB02&quot;&gt;&lt;span&gt;&lt;/span&gt;&lt;/a&gt;&lt;/div&gt;</textarea>

<div class="moduleTextB01">ボタン [btnB* + left]</div>
<div><a href="#" class="btnB01 left"><span>テキスト</span></a></div>
<div><a href="#" class="btnB02 left"><span>テキスト</span></a></div>
<textarea class="source twoLines" readonly>&lt;div&gt;&lt;a href=&quot;#&quot; class=&quot;btnB01 left&quot;&gt;&lt;span&gt;&lt;/span&gt;&lt;/a&gt;&lt;/div&gt;
&lt;div&gt;&lt;a href=&quot;#&quot; class=&quot;btnB02 left&quot;&gt;&lt;span&gt;&lt;/span&gt;&lt;/a&gt;&lt;/div&gt;</textarea>

<div class="moduleTextB01">ボタンリスト [btnListA01]</div>
<ul class="btnListA01">
<li><a href="#" class="btnB01"><span>テキスト</span></a></li>
<li><a href="#" class="btnB01"><span>テキスト</span></a></li>
</ul>
<textarea class="source fourLines" readonly>&lt;ul class=&quot;btnListA01&quot;&gt;
&lt;li&gt;&lt;a href=&quot;#&quot; class=&quot;btnB01&quot;&gt;&lt;span&gt;&lt;/span&gt;&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;#&quot; class=&quot;btnB01&quot;&gt;&lt;span&gt;&lt;/span&gt;&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</textarea>

<div class="moduleTextB01">ボタンリスト [btnListB01]</div>
<ul class="btnListB01">
<li><a href="#" class="btnB01"><span>テキスト</span></a></li>
<li><a href="#" class="btnB01"><span>テキスト</span></a></li>
</ul>
<textarea class="source fourLines" readonly>&lt;ul class=&quot;btnListB01&quot;&gt;
&lt;li&gt;&lt;a href=&quot;#&quot; class=&quot;btnB01&quot;&gt;&lt;span&gt;&lt;/span&gt;&lt;/a&gt;&lt;/li&gt;
&lt;li&gt;&lt;a href=&quot;#&quot; class=&quot;btnB01&quot;&gt;&lt;span&gt;&lt;/span&gt;&lt;/a&gt;&lt;/li&gt;
&lt;/ul&gt;</textarea>
</section>

</div><!-- / mainContents -->
</div>