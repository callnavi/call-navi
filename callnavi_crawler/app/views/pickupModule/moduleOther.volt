<div id="module_view">
<div id="mainContents">

<section>
<div class="moduleTextB01">テキスト（ブルー） [textA01]</div>
<p class="textA01 marginOff">テキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
<textarea class="source singleLine" readonly>&lt;p class=&quot;textA01&quot;&gt;&lt;/p&gt;</textarea>

<div class="moduleTextB01">テキスト（太字） [fontWeightBold]</div>
<p class="fontWeightBold marginOff">テキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
<textarea class="source singleLine" readonly>&lt;p class=&quot;fontWeightBold&quot;&gt;&lt;/p&gt;</textarea>

<div class="moduleTextB01">フォントサイズ（px指定）　[fontSizeNormal（14px） / fontSize15・16・17…～30]</div>
<p class="fontSizeNormal marginOff">テキストテキストテキスト</p>
<p class="fontSize15 marginOff">テキストテキストテキスト</p>
<p class="fontSize30 marginOff">テキストテキストテキスト</p>
<textarea class="source threeLines" readonly>&lt;p class=&quot;fontSizeNormal&quot;&gt;&lt;/p&gt;
&lt;p class=&quot;fontSize15&quot;&gt;&lt;/p&gt;
&lt;p class=&quot;fontSize30&quot;&gt;&lt;/p&gt;</textarea>

<div class="moduleTextB01">margin-bottom（余白無し / 10px～50pxを付与）　[marginOff / marginB10・20・30・40・50]</div>
<textarea class="source threeLines" readonly>&lt;div class=&quot;marginOff&quot;&gt;&lt;/div&gt;
&lt;div class=&quot;marginB10&quot;&gt;&lt;/div&gt;
&lt;div class=&quot;marginB50&quot;&gt;&lt;/div&gt;</textarea>

<div class="moduleTextB01">マウスオーバー [imgHover]</div>
<p class="marginOff"><a href="#" class="imgHover"><img src="/public/images/dummy/dummy_gray.gif" width="300" height="100" alt=""></a></p>
<textarea class="source singleLine" readonly>&lt;a href=&quot;#&quot; class=&quot;imgHover&quot;&gt;&lt;img src=&quot;&quot; width=&quot;&quot; height=&quot;&quot; alt=&quot;&quot;&gt;&lt;/a&gt;</textarea>
</section>

</div><!-- / mainContents -->
</div>