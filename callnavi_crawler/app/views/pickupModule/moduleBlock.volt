<div id="module_view">
<div id="mainContents">
<p>※実際に使用する際には、各ブロックの下に余白が付きます。</p>

<section>
<div class="moduleTextB01">汎用ブロック（グレー背景） [generalBlockA01]</div>
<div class="generalBlockA01 marginOff">
<p>padding:15px 30px 5px;　テキストテキストテキストテキストテキストテキスト</p>
</div><!-- / generalBlockA01 -->
<textarea class="source singleLine" readonly>&lt;div class=&quot;generalBlockA01&quot;&gt;&lt;/div&gt;</textarea>

<div class="moduleTextB01">汎用ブロック（白背景） [generalBlockB01]</div>
<div class="generalBlockB01 marginOff" style="border: 1px solid red;">
<p>padding:15px 30px 5px;　テキストテキストテキストテキストテキストテキスト</p>
<p class="">※わかりやすくするために赤い枠線を付けています。実際には枠線は付きません。</p>
</div><!-- / generalBlockB01 -->
<textarea class="source singleLine" readonly>&lt;div class=&quot;generalBlockB01&quot;&gt;&lt;/div&gt;</textarea>

<div class="moduleTextB01">汎用ブロック（2カラム） [generalBlockC01]</div>
<div class="generalBlockC01" style="margin-bottom: 0;">
<div class="contentsInner">
<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
</div><!-- / contentsInner -->
<div class="contentsInner">
<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
</div><!-- / contentsInner -->
</div><!-- / generalBlockC01 -->
<textarea class="source fourLines" readonly>&lt;div class=&quot;generalBlockC01&quot;&gt;
&lt;div class=&quot;contentsInner&quot;&gt;&lt;/div&gt;
&lt;div class=&quot;contentsInner&quot;&gt;&lt;/div&gt;
&lt;/div&gt;</textarea>

<div class="moduleTextB01">画像カラム [imageBlockA01]</div>
<div class="imageBlockA01">
<div class="image" style="padding-bottom: 0;"><img src="/public/images/dummy/dummy_gray.gif" width="200" height="100" alt=""/></div>
<div class="contentsInner" style="padding-bottom: 0;">
<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
</div><!-- / contentsInner -->
</div><!-- / imageBlockA01 -->
<textarea class="source fourLines" readonly>&lt;div class=&quot;imageBlockA01&quot;&gt;
&lt;div class=&quot;image&quot;&gt;&lt;img src=&quot;&quot; width=&quot;&quot; height=&quot;&quot; alt=&quot;&quot;&gt;&lt;/div&gt;
&lt;div class=&quot;contentsInner&quot;&gt;&lt;/div&gt;
&lt;/div&gt;</textarea>

<div class="moduleTextB01">画像カラム [imageBlockB01]</div>
<div class="imageBlockB01">
<div class="image" style="padding-bottom: 0;"><img src="/public/images/dummy/dummy_gray.gif" width="200" height="100" alt=""/></div>
<div class="contentsInner" style="padding-bottom: 0;">
<p>テキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキストテキスト</p>
</div><!-- / contentsInner -->
</div><!-- / imageBlockB01 -->
<textarea class="source fourLines" readonly>&lt;div class=&quot;imageBlockB01&quot;&gt;
&lt;div class=&quot;image&quot;&gt;&lt;img src=&quot;&quot; width=&quot;&quot; height=&quot;&quot; alt=&quot;&quot;&gt;&lt;/div&gt;
&lt;div class=&quot;contentsInner&quot;&gt;&lt;/div&gt;
&lt;/div&gt;</textarea>

</section>

</div><!-- / mainContents -->
</div>