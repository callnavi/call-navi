<?php

trait ScopeCalculatable {
         /**
        * 先月から11ヶ月前までの「何年何月」というデータを配列に格納
        * @return array 先月から、11ヶ月前までの、
        *              「何月何日」というデータの配列
        */
    public function calMonths() {

        $months = [];
        $year = date("Y");
        $last_year = date("Y", strtotime("-1 year"));
        $this_month = date("m");
        
        
        for ($i = -1; $i > -12; $i--) {
            $month = date("m", strtotime("$i month"));
            $is_last_year = $this_month + $i;
            if ($is_last_year <= 0) {
                $each_output = $last_year . "年" . $month . "月";
            } else {
                $each_output = $year . "年" . $month . "月";
            }

            array_push($months, $each_output);
        }

        return $months;
    }
        /**
        * 先月から現在ログイン中のアカウントが作成された月までの「何年何月」というデータを配列に格納
        * @return array 先月から、現在ログイン中のアカウントが作成された月までの、
        *              「何月何日」というデータの配列
        */
    public function calMyMonths() {

        $months = [];
        $this_year = date("Y");
        $last_year = date("Y", strtotime("-1 year"));
        $this_month = date("m");
        $account = new Account();
        $my_account = $account->findFirstById($this->basic->id);
        $created = $my_account->created;
        $today = time();
        $created = strtotime($created);
        $today_month = date("Y",$today)*12+date("m",$today);
        $created_month = date("Y",$created)*12+date("m",$created);

        $diff = $created_month - $today_month - 1;
        
        for ($i = -1; $i > $diff; $i--) {
            $month = date("m", strtotime("$i month"));
            $year = date("y", strtotime("$i month"));
            $each_output = $year . "年" . $month . "月";
            array_push($months, $each_output);
        }
        //アカウントが作成された日付が今月の場合、今月分を格納
        if ($month == []) {
            array_push($months,  date("Y") . "年" . date("m") . "月");
            
        }
        return $months;
    }  
       /**
        * 先月から、idが＄account_idであるアカウントが作成された月までの「何年何月」というデータを配列に格納
        * @param int $account_id 特定のアカウントのID
        * @return array 先月から、$account_idにより特定されるアカウントが作成された月までの、
        *              「何月何日」というデータの配列
        */
    public function calHisMonths($account_id) {

        $months = [];
        $this_year = date("Y");
        $last_year = date("Y", strtotime("-1 year"));
        $this_month = date("m");
        $account = new Account();
        $his_account = $account->findFirstById($account_id);
        $created = $his_account->created;
        $today = time();
        $created = strtotime($created);
        $today_month = date("Y",$today)*12+date("m",$today);
        $created_month = date("Y",$created)*12+date("m",$created);

        $diff = $created_month - $today_month - 1;
        
        for ($i = -1; $i > $diff; $i--) {
            $month = date("m", strtotime("$i month"));
            $year = date("y", strtotime("$i month"));
            $each_output = $year . "年" . $month . "月";
            array_push($months, $each_output);
        }
        //アカウントが作成された日付が今月の場合、今月分を格納
        if ($month == []) {
            array_push($months,  date("Y") . "年" . date("m") . "月");
            
        }
        return $months;
    }   
    
        /**
        * 対象期間変更のプルダウンメニューに表示される期間に応じて、開始時と終了時をdatetimeで出力
        * @param string $scope 対象期間を表す文字列
        * @param $condition
        * @return array 開始時及び終了時のdatetimeを格納した配列
        */
    public function calScope($scope, $condition = 0) {

        $today = date("Y-m-d");
        $yesterday = date("Y-m-d", strtotime("-1 day"));
        $year = date("Y");
        $last_year = date("Y", strtotime("-1 year"));
        $month = date("m");
        $last_month = date("m", strtotime("-1 month"));
        $datetime = new DateTime($today);
        switch ($scope) {
            case "today":
                $start = $today . " 00:00:00";
                $end = $today . " 23:59:59";
                break;
            case "yesterday":
                $start = $yesterday . " 00:00:00";
                $end = $yesterday . " 23:59:59";
                break;
            case "week":
                $plus_days = $datetime->format('w');
                $minus_days = 1 - $plus_days;
                if ($plus_days == 0) {
                    $start = date("Y-m-d", strtotime("-6 day")) . " 00:00:00";
                    $end = $today . " 23:59:59";
                } else {
                    $start = date("Y-m-d", strtotime("$minus_days day")) . " 00:00:00";
                    $end = $today . " 23:59:59";
                }
                break;
            case "seven_days":
                $start = date("Y-m-d", strtotime("-6 day")) . " 00:00:00";
                $end = $today . " 23:59:59";
                break;
            case "last_week":
                $plus_days = -6 - $datetime->format('w');
                $minus_days = 0 - $datetime->format('w');
                if ($plus_days == -6) {
                    $start = date("Y-m-d", strtotime("-13 day")) . " 00:00:00";
                    $end = date("Y-m-d", strtotime("-7 day")) . " 23:59:59";
                } else {
                    $start = date("Y-m-d", strtotime("$plus_days day")) . " 00:00:00";
                    $end = date("Y-m-d", strtotime("$minus_days day")) . " 23:59:59";
                }
                break;
            case "month";
                $start = $year . "-" . $month . "-01 00:00:00";
                $end = $today . " 23:59:59";
                break;
            case "thirty_days":
                $start = date("Y-m-d", strtotime("-30 day")) . " 00:00:00";
                $end = $today . " 23:59:59";
                break;
            case "last_month":
                if ($month == 1) {
                    $start = $last_year . "-12-01 00:00:00";
                    $end = $last_year . "-12-31 23:59:59";
                } else {
                    $start = $year . "-" . $last_month . "-01 00:00:00";
                    $end = $year . "-" . $last_month . "-31 23:59:59";
                }
                break;
            case "all":
                $start = "1000-01-01 00:00:00";
                $end = "9999-12-31 23:59:59";
                break;
            case "admin":
                $start = $condition['keyword_scope_from'] . " 00:00:00";
                $end = $condition['keyword_scope_to'] . " 23:59:59";
                break;
        }

        return array('start' => $start, 'end' => $end);
    }
        /**
        * クレジットカード請求の対象期間プルダウメニュ-に表示される、「何月何日」というデータに応じて開始時と終了時をdatetimeで出力
        * @param int $ago 対象期間が今月から何ヶ月前かを表す
        * @return array 対象期間の開始時及び終了時のdatetime,及び年、月の配列
        */
    public function changeMonth($ago) {

        $year = date("Y", strtotime("$ago month"));
        $month = date("m", strtotime("$ago month"));
        $start = $year . "-" . $month . "-01 00:00:00";
        $end = $year . "-" . $month . "-31 23:59:59";
        $pay_year = date("Y", strtotime("$ago month"));
        $pay_month = date("m", strtotime("$ago month"));
        return array("year" => $year, "month" => $month, "pay_year" => $pay_year, "start" => $start, "end" => $end, "pay_month" => $pay_month);
    }
        /**
        * 該当広告の通常決済額合計額を出力
        * @param array $offers 該当広告オブジェクトの配列
        * @return int 上記広告オブジェクト全部における通常決済額合計
        */
    public function calExpenseSum($offers) {

        $expense_sum = 0;
        foreach ($offers as $offer) {
            if(property_exists($offer, 'without_card')) {
              if($offer->without_card == 0) {
                  $expense_sum += $offer->expense;
              }
            }
        }

        return $expense_sum;
    }
        
        /**
        * 該当広告のカードなし決済額合計額を出力
        * @param array $offers 該当広告オブジェクトの配列
        * @return int 上記広告オブジェクト全部におけるカードなし決済額合計額
        */
    public function calCardlessExpenseSum($offers) {

         $cardless_expense_sum =0;
         foreach ($offers as $offer) {

            if ($offer->without_card ==1) {
                 $cardless_expense_sum += $offer->expense;
            }
         }
         
         return $cardless_expense_sum;
    }
      
}
