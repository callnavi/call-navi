<?php

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller {

       
    public function initialize() {
         
        $this->__setServerSettings();
        $this->__setHeadParams();

        /**
         * recommend_keywords(タグクラウド)の取得  
         */
        $this->useModel('RecommendKeyword');
        $this->view->recommendKeywords = $this->RecommendKeyword->getKeywords();

        //求人数を変数に格納してviewに表示
        $this->view->alloffers = file_get_contents('http://crawler.callnavi.jp/public/offers/count_offers.txt');
        $header = get_headers('http://crawler.callnavi.jp/public/offers/count_offers.txt', 1);
        $updated = strtotime($header['Last-Modified']);
        
        $this->view->month = date('m', $updated);
        $this->view->day = date('d', $updated);
        $this->view->time = date('H:i', $updated);
        
        //ajaxアクションを前提にしているため、renderする場合は、LEVE_MAIN_LAYOUTにする必要がある
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_MAIN_LAYOUT);
    }

    private function __setServerSettings() {
        error_reporting(E_ALL);
        ini_set('display_errors', 1);
    }

    /**
     * headタグ内に埋め込むパラメータを指定
     */
    private function __setHeadParams() {
        $this->view->head = (object) null;
        $this->view->head->title = 'コールセンター求人まとめメディア｜コールナビ';
        $this->view->head->description = 'コールセンター求人おまとめサイトならコールナビ！コールセンターのアルバイト、転職、パートなどの求人情報はここでチェック！コールセンターで働く人に向けた記事コンテンツも充実！正社員、派遣、アルバイトを問わず、転職、就職、求職中の方は必見！ SV(スーパーバイザー)、高時給、高収入のバイトや、シフト自由の学生、フリーター、主婦の方に人気です！';
        $this->view->head->keywords = 'コールセンター 求人,テレアポ 募集,アルバイト 高時給,コールナビ,オペレーター,アウトバウンド';
    }

    /**
     * コントローラー内での単一モデルの読み込み関数
     * 
     * @param object $models
     * @return void
     */
    
    public function useModel($model) {
        $this->$model = new $model();
    }
    
}
