<?php

class CustomerController extends AdminControllerBase {
	
   public function indexAction() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
   }

   public function priceAction() {
//   	$this->view->disable();
	$this->view->pick("customer/price");
   }

   public function termsAction() {
	$this->view->pick("customer/terms");
   }

   public function tradingAction() {
	$this->view->pick("customer/trading");
   }
}
