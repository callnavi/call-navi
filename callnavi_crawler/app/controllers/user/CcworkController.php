<?php

class CcworkController extends UserControllerBase {

    public function indexAction() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
    }
    
    public function detail01Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = 'コールセンターの仕事内容 | コールセンター求人まとめメディア｜コールナビ';
    }
    public function detail02Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = 'コールセンターで働くメリットってなに？ | コールセンター求人まとめメディア｜コールナビ';
    }
    public function detail03Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '全国調査！コールセンターの時給比較 | コールセンター求人まとめメディア｜コールナビ';
    }
    public function detail04Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = 'コールセンターの雇用形態 | コールセンター求人まとめメディア｜コールナビ';
    }
    public function detail05Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = 'コールセンターの面接で失敗しないコツ？ | コールセンター求人まとめメディア｜コールナビ';
    }
    public function detail06Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '「ストレスに負けない!」コールセンターバイト | コールセンター求人まとめメディア｜コールナビ';
    }
    public function detail07Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = 'バイトを辞めるときはここに注意！ | コールセンター求人まとめメディア｜コールナビ';
    }
    public function detail08Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = 'ライフスタイルに合った働き方を見つけよう！ | コールセンター求人まとめメディア｜コールナビ';
    }
    public function detail09Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '健康の秘訣！知っておきたい、体のケア | コールセンター求人まとめメディア｜コールナビ';

    }
    public function detail10Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = 'アイデアオフィスで疲れを吹き飛ばそう！ | コールセンター求人まとめメディア｜コールナビ';
    }
    public function detail11Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = 'コールセンターの専門用語！！ | コールセンター求人まとめメディア｜コールナビ';
    }
    public function detail12Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '日本全国コールセンターまとめ | コールセンター求人まとめメディア｜コールナビ';
    }
    public function detail13Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = 'NGワードとクッション言葉に気をつけよう！ | コールセンター求人まとめメディア｜コールナビ';
    }
    public function detail14Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = 'コールセンター求人まとめメディア｜コールナビ';
    }
    public function detail15Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '性格まで明るくなっちゃう？コールセンターで充実ライフを手に入れよう！ | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail16Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '隠されたドラマがある！？コールセンターの舞台裏！ | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail17Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = 'おしゃれを楽しみたい！服装自由のコールセンターが多いわけ | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail18Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '学歴なんて関係ない！コールセンターなら、実力次第で高収入が目指せる | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail19Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = 'ゲーム以上に仕事が楽しい！？楽しく仕事をする４つのポイント | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail20Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '話すのが苦手な方へ、テレビショッピングのコールセンターをおすすめする理由！ | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail21Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '「コールセンターで働くあの子！どんな１日を過ごしているの？」 | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail22Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = 'やればやるだけ報酬UP！だから成長できるインセンティブのひみつ | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail23Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '日本語を正しく使えていますか？コールセンター業務に必要な丁寧語、謙譲語、尊敬語 | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail24Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '声で人生が変わる！？コールセンターで好印象な声を会得する方法 | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail25Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = 'コールセンターの困ったお客様 | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail26Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '始めての方必見！コールセンター応募、面接、採用、入社初日までの流れ | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail27Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = 'コールセンターの年齢事情 | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail28Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '「ファミコン敬語」使っていませんか？以外と知らない？！正しい「ビジネス敬語」 | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail29Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = 'これで受かる！コールセンター応募の志望動機 | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail30Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '説明上手になれる?!その秘訣は･･･ | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail31Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '仕事とプライベートの両立！シフト制は、スケジュール管理の魔法の杖となるのか？ | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail32Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = 'どうするクレーム？しっかり対応するための魔法の４ステップ！ | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail33Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = 'テレマから人事部へ！コールセンター業務の経験が、その後の昇進を決める！？ | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail34Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '就活の秘訣！コールセンター経験者が就活を有利に進められる理由とは？ | コールセンター求人まとめメディア｜コールナビ';

}
    public function detail35Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '初心者でも安心！コールセンターの研修や教育でやっていること | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail36Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '地方で働きたい！　コールセンターが地方で増える理由とメリット | コールセンター求人まとめメディア｜コールナビ';

}
    public function detail37Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '私のテレアポ営業奮闘記～テレアポは怖くない！ | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail38Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '短期バイトで稼ぎたい！それなら、コールセンターがおすすめです。 | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail39Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '初心者大歓迎！高時給バイトなら、コールセンターが狙い目 | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail40Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = 'ミュージシャンは声で稼げ！コールセンターで、バンドマンが重宝されるワケ | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail41Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = 'まさかの逆効果！？甘いものの罠。それリフレッシュになっていません。 | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail42Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = 'ガムを噛め！仕事中のリフレッシュは、これに限る！ | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail43Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '友達同士でバイトに応募！他にはない！？コールセンターのメリット | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail44Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = 'コールセンター心理学！これであなたもコミュニケーションの達人に！ | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail45Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = 'テレアポ・テレオペ・テレマの違いって？自分にピッタリの仕事を見つける！ | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail46Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = 'コールセンターの三種の神器！PC・ヘッドセット・管理ソフト！ | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail47Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '「集中力を大発揮！あなたのジンクスはなに！？」 | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail48Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '正社員登用制度を活用せよ！アルバイト入社でも実力次第で、正社員になれるコールセンター | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail49Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '肩が凝る？それ、座りっぱなし症候群ではないですか？ | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail50Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = 'あなたはどっち？ SV転向、アポインター継続 | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail51Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '営業経験が全くないのですが、コールセンターの仕事はできますか？ | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail52Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '社員登用制度を利用して、アルバイトから正社員へレベルアップ！ | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail53Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = 'コールセンターで生かせる資格「コン検」！ | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail54Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '【コールセンター体験談！】コールセンターで実際に研修を受けてみた！ | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail55Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '取扱いにご注意！個人情報とコールセンター | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail56Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = 'えっ、あの人も！？テレアポに転職した先輩の意外な過去とは？ | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail57Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '結婚・引越し・出産があっても大丈夫！コールセンターの魅力！ | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail58Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = 'コールセンターバイトで、就職活動を有利に！電話対応、コミュ力、ビジネスマナーは最大の武器！ | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail59Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '結婚・引越し・出産があっても大丈夫！コールセンターの魅力！ | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail60Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '色んな人と出会えるチャンス！コールセンターで友達＆恋人もゲットしちゃおう | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail61Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '大丈夫！？即日勤務OK！ 日払いOK！アルバイトとその実態 | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail62Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '睡魔を吹き飛ばせ！眠気対策には、これを知っておけば大丈夫 | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail63Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '眠たいとき、どうする？ 理想の睡眠時間と業務効率について | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail64Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '髪型自由は、本当に自由なの？髪型自由だとこんなメリットがある | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail65Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '副業のススメ～コールセンターで副業するときには、ここに気をつけよう！ | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail66Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '保険関係のコールセンターで行う４つの業務とは？ | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail67Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '裏技教えます！本当は教えたくない受注が取れるコツ | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail68Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = 'テレマーケティングできついこと〜アウトバウンド編〜 | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail69Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '教育スタッフに聞いた、新人がやりがちなミス5選 | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail70Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = 'タダでディズニーランド！？実際にあったコールセンターでの豪華賞品！ | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail71Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '実録インタビュー！実際にコールセンターで働いた印象 | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail72Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = 'テンションが高くない人必見！コールセンターバイトでは、無理にテンションを上げる必要はない！？ | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail73Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '【質問】コールセンターバイトでの休憩時間は、仲間と積極的に話すべき！？できれば一人でゆっくりしたいのですが... | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail74Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '高時給バイトのテレアポ体験談！苦情は意外と気にならない！？ | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail75Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '一度始めたら辞められない！？コールセンターバイトの５つの魅力！ | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail76Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = 'お笑い芸人の卵です！そんな方は、高時給・シフト制のコールセンターで、当面の生活費と将来の夢の両立を！ | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail77Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '喉が命！コールセンターバイトもしゃべりのプロ！喉のケア方法と喉にいい食材はこちら | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail78Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = '女性SVに聞く～コールセンターのお仕事あれこれ～ | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail79Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = 'カラオケ上手は、テレアポ上手！？意外な共通点と、心に刺さるトークのコツとは？ | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail80Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = 'コミュニケーションのコツはVAK！テレアポはもちろん、普段の会話にも劇的に変わります！ | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail81Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        $this->view->head->title = ' | コールセンター求人まとめメディア｜コールナビ';
}
    public function detail82Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
}
    public function detail83Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
}
    public function detail84Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
}
    public function detail85Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
}
    public function detail86Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
}
    public function detail87Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
}
    public function detail88Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
}
    public function detail89Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
}
    public function detail90Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
}
    public function detail91Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
}
    public function detail92Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
}
    public function detail93Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
}
    public function detail94Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
}
    public function detail95Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
}
    public function detail96Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
}
    public function detail97Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
}
    public function detail98Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
}
    public function detail99Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
}
    public function detail100Action() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
}
}
