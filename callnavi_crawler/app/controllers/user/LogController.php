<?php

class LogController extends UserControllerBase {

    public function initialize() {
        parent::initialize();
        session_set_cookie_params(24 * 60 * 60);
    }

    public function impressionRectangleAction() {
        $request = new \Phalcon\Http\Request();
        if ($request->isPost() == true) {
            $offer_id = $request->getPost('offer_id');
            $rectangle_offer = new RectangleOffer();
            $rectangle_offer->impression($offer_id);
        }

        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_MAIN_LAYOUT);
    }

    public function impressionListingAction() {
        $request = new \Phalcon\Http\Request();
        if ($request->isPost() == true) {
            $offer_id = $request->getPost('offer_id');
            $listing_keyword_id = $request->getPost('listing_keyword_id');

            $listing_offer = new ListingOffer();
            $listing_offer->impression($offer_id, $listing_keyword_id);
        }
    }

    public function impressionPickupAction() {
        $request = new \Phalcon\Http\Request();
        if ($request->isPost() == true) {
            $offer_id = $request->getPost('offer_id');

            $pickup_offer = new PickupOffer();
            $pickup_offer->impression($offer_id);
        }
    }

    public function impressionFreeAction() {
        $request = new \Phalcon\Http\Request();
        if ($request->isPost() == true) {
            $offer_id = $request->getPost('offer_id');

            $free_offer = new FreeOffer();
            $free_offer->impression($offer_id);
        }
    }

    public function clickRectangleAction() {
        $request = new \Phalcon\Http\Request();
        if ($request->isPost() == true) {
            $offer_id = $request->getPost('offer_id');

            if (!$this->session->has('rectangle_' . $offer_id)) {
                $rectangle_offer = new RectangleOffer();
                $rectangle_offer->click($offer_id);
                $rectangle_offer->checkIsAvailable($offer_id);

                $this->session->set('rectangle_' . $offer_id, 1);
            }
        }
    }



    public function clickListingAction() {
        $request = new \Phalcon\Http\Request();
        if ($request->isPost() == true) {
            $offer_id = $request->getPost('offer_id');
            $listing_keyword_id = $request->getPost('listing_keyword_id');

            if (!$this->session->has('listing_' . $offer_id)) {
                $listing_offer = new ListingOffer();
                $listing_offer->click($offer_id, $listing_keyword_id);
                $listing_offer->checkIsAvailable($offer_id);
                $this->session->set('listing_' . $offer_id, 1);
            }
        }
    }   

    public function clickPickupAction() {
        $request = new \Phalcon\Http\Request();
        if ($request->isPost() == true) {
            $offer_id = $request->getPost('offer_id');

            if (!$this->session->has('pickup_' . $offer_id)) {
                $pickup_offer = new PickupOffer();
                $pickup_offer->click($offer_id);
                $this->session->set('pickup_' . $offer_id, 1);
            }
        }
    }

    public function clickFreeAction() {
        $request = new \Phalcon\Http\Request();
        if ($request->isPost() == true) {
            $offer_id = $request->getPost('offer_id');

            if (!$this->session->has('free_' . $offer_id)) {
                $free_offer = new FreeOffer();
                $free_offer->click($offer_id);
                $this->session->set('free_' . $offer_id, 1);
            }
        }
    }

}
