<?php

class IndexController extends UserControllerBase {

    public function indexAction() {

        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/jquery.heightLine.js');  
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/index/index.js');
        
        $this->view->head->title = 'コールセンター求人まとめメディア｜コールナビ';
        $this->view->head->description = 'コールセンター求人おまとめサイトならコールナビ！コールセンターのアルバイト、転職、パートなどの求人情報はここでチェック！コールセンターで働く人に向けた記事コンテンツも充実！正社員、派遣、アルバイトを問わず、転職、就職、求職中の方は必見！ SV(スーパーバイザー)、高時給、高収入のバイトや、シフト自由の学生、フリーター、主婦の方に人気です！';
        $this->view->head->keywords = 'コールセンター 求人,テレアポ 募集,アルバイト 高時給,コールナビ,オペレーター,アウトバウンド';
        
        $this->useModel('PickupOffer');
        $this->view->pickup_inners = $this->PickupOffer->newInners();
        $this->view->pickup_inner_halfs = $this->PickupOffer->newInnerHalfs();
        $pickups = $this->PickupOffer->findPublishedOffers();
        $this->view->pickups = $pickups;
        $pickups_count = count($pickups);
        $this->view->pickups_count = $pickups_count;
        
    }
  
   public function pickupPreviewAction() {
         $this->assets->addCss('admin_css/error_red.css');
         $request = new \Phalcon\Http\Request();

        if ($request->isGet() == true) {
            $this->useModel('PickupOffer');
            $offer_id = intval($request->getQuery('offer_id'));

            
            
            $data = $this->PickupOffer->findOnConfirmPickupOfferById($offer_id);
            $this->view->data = $data;
            $features = explode(":", $data->features);
            
            $hired_as = explode(":", $data->hired_as);
            $this->view->features = $features;
            $this->view->hired_as = $hired_as;
          

            $hired_as_display = $this->PickupOffer->calHiredAsDisplay($hired_as);
              
            $this->view->hired_as_display = $hired_as_display;
   
            $this->useModel('PickupOfferDetail');
            if($data->detail_content_type!=="none") {
            $detail = $this->PickupOfferDetail->findPickupOfferDetailById($offer_id);

            $html = sprintf($detail->html);
            $this->view->detail = $detail; 
            $this->view->html = $html;

         }
            
            
     }
    
   }
   
    public function infoAction() {

    }
    
    public function pickupListAction() {
     $this->useModel('PickupOffer');
     $pickups = $this->PickupOffer->setPickupData($this->PickupOffer->findPublishedOffers());
     $pickups_count = count($pickups);
     
     $this->view->pickups_count = $pickups_count;
     $today = date('Y-m-d H:i:s');
     $this->view->recent_past = date('Y-m-d H:i:s', strtotime("$today - 7 day"));
     $request = new \Phalcon\Http\Request();
     $page = ($request->get('page')) ? intval($request->get('page')) : 0;
     $this->view->page = $page;
     $size = 20;
     $pickups_selected = $this->PickupOffer->findPickupLists($page, $size);
     
     $this->view->pickups = $this->PickupOffer->findPickupLists($page, $size);
     $this->view->countNextPages = ceil(($pickups_count - $size * ($page + 1)) / $size);
     
    }
}
