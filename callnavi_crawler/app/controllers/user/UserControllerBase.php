<?php
use Phalcon\Mvc\Controller;

    class UserControllerBase extends ControllerBase {

    public function initialize() {

        parent::initialize();
        /*
         * layoutの読み込み
         * 
         * setTemplateAfterで<head/>レベルを指定して、
         * setLayoutでheader,footerを指定する
         */

        
        $this->view->setLayout('body/template');
        $this->view->setTemplateAfter('head/template');
        
        //広告関連処理　80ms - 150ms
        $this->useModel('RectangleOffer');
        $this->view->rectangle = $this->RectangleOffer->choose();
              
        $this->useModel('PickupOffer');
        $this->view->pickup_right_columns = $this->PickupOffer->newColumns();
        
    }

}
