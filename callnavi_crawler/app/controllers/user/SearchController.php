<?php

class SearchController extends UserControllerBase {

    public function resultAction() {

        $request = new \Phalcon\Http\Request();
        if ($request->isGet() == true) {
            $area = $request->get('area');
            $keyword = $request->get('keyword');
            $page = ($request->get('page')) ? intval($request->get('page')) : 0;
            $site_id = $request->get('site_id');
            $this->assets->addJs('scripts/search/result.js');
            $this->assets->addJs('scripts/offer_log.js');
            $params = [
                'area' => $area,
                'keyword' => $keyword,
                'site_id' => $site_id,
            ];
            $this->useModel('CloudSearch');  

            $this->view->params = (object) $params;
            $size = 20;
            list($this->view->results, $this->view->countResults) = $this->CloudSearch->search($params, $page, $size);
            $this->view->page = $page;
            $this->view->countNextPages = ceil(($this->view->countResults - $size * ($page + 1)) / $size);  
            
            $this->useModel('ListingOffer');
            if(mb_strlen($keyword, 'UTF8')){
                $this->view->listing = $this->ListingOffer->choose($keyword);
            }elseif(mb_strlen($area, 'UTF8')){
                $this->view->listing = $this->ListingOffer->choose($area);              
            }else{
                $this->view->listing = null;
            }
           
            $this->useModel('PickupOffer');
            if(mb_strlen($keyword, 'UTF8')){
                $this->view->pickups = $this->PickupOffer->choose($keyword);
            }elseif(mb_strlen($area, 'UTF8')){
                $this->view->pickups = $this->PickupOffer->choose($area);              
            }else{
                $this->view->pickups = null;
            }
                       
            $today = date('Y-m-d H:i:s');
            $this->view->recent_past = date('Y-m-d H:i:s', strtotime("$today - 7 day"));

            
        }
    }

    public function insertQueryAction() {
        $request = new \Phalcon\Http\Request();
        if ($request->isPost() == true) {
            $query = $request->getPost('query');

            $this->useModel('SearchedQuery');
            $this->SearchedQuery->addQueries($query);

            $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
        }
    }
      
       /**
        * クラウドサーチからデータを削除(誤ったデータ削除用)
        * urlで、?cloudsearch=11-14 のように指定
        */
    public function deleteAction() {
        $request = new \Phalcon\Http\Request();
        if ($request->isGet() == true) {
            $cloudsearch_id = $request->getQuery('cloudsearch_id');

            $this->useModel('Cloudsearch');
            $this->Cloudsearch->deletetById($cloudsearch_id);
            exit;
            
        }
        
    }
}
