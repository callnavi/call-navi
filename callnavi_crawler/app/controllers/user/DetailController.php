<?php

class DetailController extends UserControllerBase {

    public function indexAction() {
        $this->assets->addJs('scripts/touch-event-callback.js');
        $this->assets->addJs('scripts/slider.js');
        $this->assets->addJs('scripts/detail/index.js');
        return $this->response->redirect('detail/wiz');
    }
    

}
