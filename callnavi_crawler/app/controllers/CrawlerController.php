<?php
      //ローカルテスト用
class CrawlerController extends UserControllerBase {
        
    public function indexAction() {
       
        $this->useModel('Offer');
        $this->Offer->truncate();
        
        $this->useModel('EaidemCrawler');
        $this->EaidemCrawler->crawl();
        $this->useModel('JobSenseCrawler');
        $this->JobSenseCrawler->crawl();
        $this->useModel('DodaCrawler');
        $this->DodaCrawler->crawl();
        $this->useModel('TenshokuMynaviCrawler');
        $this->TenshokuMynaviCrawler->crawl();
        $this->useModel('BaitoMynaviCrawler');
        $this->BaitoMynaviCrawler->crawl();
        $this->useModel('FromAnaviCrawler');
        $this->FromAnaviCrawler->crawl();
        $this->useModel('BaitoruCrawler');
        $this->BaitoruCrawler->crawl();
        $this->useModel('AnCrawler');
        $this->AnCrawler->crawl();
        
        $this->useModel('CallnaviCrawler');
         

        $this->CallnaviCrawler->crawl();
        
        //store data to aws
        $this->useModel('CloudSearch');
        $this->CloudSearch->refresh(11);
       
        //件数をクローラーサーバーに記録する
        $this->useModel('Offer');
        $this->Offer->recordOffersToFile();
        
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_NO_RENDER);
       
        
    }

    public function testSearchResultConditionAction($client = null) {

        if (!is_null($client)) {

            $clientModel = $client . 'Crawler';

            $this->useModel($clientModel);

            if (count($results = $this->{$clientModel}->getUrlsBySearch(1))) {

                echo 'たぶんOK！', '<br /><br />';

                foreach ($results as $result) {

                    echo '<a href="' . $result . '" target="_blank">' . $result, '</a>', '<br />';
                }
            } else {

                echo 'ひとつも引っかかってないので、どこかおかしいかも...', '<br /><br />';
                echo '次のプロパティ、メソッドの中身が適切でない可能性があります', '<br /.';
                echo $clientModel . '::_url', '<br />';
                echo $clientModel . '::_url_page', '<br />';
                echo $clientModel . '::_page', '<br />';
                echo $clientModel . '::getConditionForSearchResult', '<br />';
            }
        } else {

            echo '媒体をキャメル記法で入れてください';
        }
    }

    public function testGetElementConditionAction($client = null) {

        if (!is_null($client)) {

            $clientModel = $client . 'Crawler';

            $this->useModel($clientModel);

            $urls = $this->{$clientModel}->getUrlsBySearch(1);
            $url = $urls[0];

            $pageXml = $this->{$clientModel}->parseResource($url);

            try {

                $offer = $this->{$clientModel}->getElement($pageXml);

                if ($this->{$clientModel}->validateElement($offer)) {

                    echo 'たぶんOK！', '<br /><br />';
                } else {

                    echo 'validationが通りませんでした。', '<br /><br />';
                }

                echo 'title:', $offer->title, '<br />';
                echo 'company:', $offer->company, '<br />';
                echo 'pic_url:', $offer->pic_url, '<br />';
                echo 'desc:', $offer->desc, '<br />';
                echo 'salary:', $offer->salary, '<br />';
                echo 'workplace:', $offer->workplace, '<br />';
                echo 'labels:', $offer->labels, '<br />';
            } catch (Exception $e) {
                echo 'parseに失敗:' . $url, '<br /><br />';
            }
        } else {

            echo '媒体をキャメル記法で入れてください';
        }
    }

    public function crawlAction() {
        $params = array(0=>"callnavi");
        for ($i=0; $i < count($params); $i++) {
            Phalcon\DI::getDefault()->getLogger()->info("params[$i] : $params[$i]");
        }

        if (empty($params)) {
            Phalcon\DI::getDefault()->getLogger()->error("Params are required");
            exit(1);
        }

        if (!in_array($params[0], (array)$this->crawlerConfig->site)) {
            Phalcon\DI::getDefault()->getLogger()->error("invalid param : $params[0]");
            exit(1);
        }

        if (isset($params[1])) {
            Phalcon\DI::getDefault()->getLogger()->info("Target env : " . $params[1]);
            $this->targetEnv = strtolower($params[1]);
        }

        $crawlerConfig = $this->crawlerConfig->toArray();

        $siteNames = $crawlerConfig['siteNames'];

        $target = $params[0];
        $modelPrefix = ucfirst($target);
        $siteId = $crawlerConfig[$target]['siteId'];

        Phalcon\DI::getDefault()->getLogger()->info("Target : $target");

        $this->useModel('Offer');
        $this->Offer->changeToDelete($siteId);

        Phalcon\DI::getDefault()->set('targetCrawlerConfig', $this->crawlerConfig->$target);

        $targetModel = $modelPrefix . 'Crawler';
        $this->useModel($targetModel);
        $this->$targetModel->crawl();

        Phalcon\DI::getDefault()->getLogger()->info($siteNames[$siteId] . "の取得件数 : " . $this->$targetModel->saveElementCnt);

        if ($this->$targetModel->saveElementCnt > 0) {
            //store data to aws
            $this->useModel('CloudSearch');
            $this->CloudSearch->refresh($siteId);

            // deletedが1のものを削除する
            $this->Offer->deleteBySiteId($siteId);

            //件数をクローラーサーバーに記録する
            $this->useModel('Offer');
            $this->Offer->recordOffersToFile();
        } else {
            // 取得件数が0件の場合、メールで通報
            mb_language('ja');
            mb_internal_encoding('UTF-8');
            $to = $this->crawlerConfig->mail_addr_for_result_reporting;
            $mailTimeStamp = date( "Y/m/d (D) H:i:s", Utils::getMicrotimeFloat() );
            $subject = $siteNames[$siteId] ."のクローラーの取得件数が0件です。 ({$mailTimeStamp})";
            $message = "取得件数 : " . $this->$targetModel->saveElementCnt . "\nsite_id = " . $siteId;
            $additionalHeaders = "From:info@callnavi.jp\n";

            if (mb_send_mail($to, $subject, $message, $additionalHeaders)) {
                Phalcon\DI::getDefault()->getLogger()->info("sent to result mail");
            } else {
                Phalcon\DI::getDefault()->getLogger()->info("failed send to result mail");
            }

            Phalcon\DI::getDefault()->getLogger()->warning("*** site_id : $siteId // クローリングによる取得データがありません。 : " . $this->$targetModel->saveElementCnt . " ***");
        }
    }
      //ローカルテスト用
//    public function changeAction() {
//        $connection = new Phalcon\Db\Adapter\Pdo\Mysql(array(
//          'host' => 'localhost',
//          'username' => 'root',
//          'password' => 'pass',
//          'dbname' => 'callnavitest',
//         ));
//        $connection->connect();
//        
//        $resultset = $connection->query("SELECT * FROM free_offers WHERE deleted = ? AND status =?", array(0, 'publishing'));
//        
//        while ($result = $resultset->fetch()) {
//              echo $result['id'];
//        }
////        
////        $frees = $connection->fetchAll("SELECT * FROM free_offers WHERE deleted = :deleted",
////            Phalcon\Db::FETCH_ASSOC,
////            array('deleted' => 0)
////          );
////        
////          foreach ($frees as $result) {
////              
////          }
//     
//        exit;
//    }
//    public function normalAction() {
//     $this->useModel('FreeOffer');
//     $results = $this->FreeOffer->find();
//     var_dump($results[0]->title);
//     exit;
//    }
//    
//    public function callnaviCrawlAction() {
//     $this->useModel('CallnaviCrawler');
//     $this->CallnaviCrawler->crawl();
//     exit;
//    }
    
//    public function deleteTestAction() {
//       $this->useModel('CloudSearch');
//       $this->CloudSearch->deleteTest();
//       exit;
//        
//    }
    
//     public function testAction() {
//         $this->useModel('Account');
//        $accounts = $this->Account->getAccountsForApproval();
//        $this->useModel('Pay');
//        foreach ($accounts as $account) {
//            try {  $charge = $this->Pay->chargeForTest($account);
//                    // API リクエスト
//                   $account->card_status = "valid";
//                } catch (\WebPay\ErrorResponse\ErrorResponseException $e) {
//                    $error = $e->data->error;
//                    switch ($error->causedBy) {
//                        case 'buyer':
//                            echo 'buyer';
//                            // カードエラーなど、購入者に原因がある
//                            // エラーメッセージをそのまま表示するのがわかりやすい
//                            $account->card_status = "invalid";
//                            break;
//                        case 'insufficient':
//                            echo 'insufficient';
//                            // 実装ミスに起因する
//                            $account->card_status = 'before_approval';
//                            break;
//                        case 'missing':
//                            echo 'missing';
//                            // リクエスト対象のオブジェクトが存在しない
//                            $account->card_status ='before_approval';
//                            break;
//                        case 'service':
//                            echo 'service';
//                            // WebPayに起因cするエラー
//                            $account->card_status ='before_approval';
//                            break;
//                        default:
//                            // 未知のエラー
//                            echo 'unknown';
//                            $account->card_status ='before_approval';
//                            break;
//                    }
//                } catch (\WebPay\ApiException $e) {
//                    // APIからのレスポンスが受け取れない場合。接続エラーなど
//                    echo 'not_received';
//                    $account->card_status ='before_approval';
//                    break;
//                }
//                $account->updatecolumn();
//        }
//         exit;
//     }
}
