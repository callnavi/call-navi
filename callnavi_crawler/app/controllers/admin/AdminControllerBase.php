<?php

class AdminControllerBase extends ControllerBase {

    public function initialize() {

        parent::initialize();
        /*
         * layoutの読み込み
         * 
         * setTemplateAfterで<head/>レベルを指定して、
         * setTemplateでheader,footerを指定する
         */

        $this->checkLogin();

      
        $this->view->setLayout('body/admin_template');
        

        $this->view->setTemplateAfter('head/admin_template');

        $this->basic = (object) null;
      
        if($this->isLoggedIn()) {

            $register_id = $this->session->get('auth')['id'];
            
                /*
             * セッション変数に登録されたアカウントID($register_id)を用いてアカウントを検索し、
             * そのアカウント情報をbasicオブジェクトに登録
             *  
             */
            $this->useModel('Account');
            $account = $this->Account->findFirstById($register_id);

            $this->basic->id = $account->id;
            $this->basic->company_name = $account->company_name;
            $this->basic->company_name_kana = $account->company_name_kana;
            $this->basic->postal_code = $account->postal_code;

            $this->basic->address = $account->address;
            $this->basic->telephone = $account->telephone;

            $this->basic->fax_code = $account->fax_code;

            $this->basic->company_url = $account->company_url;
            $this->basic->company_section = $account->company_section;
            $this->basic->company_position = $account->company_position;
            $this->basic->representative = $account->representative;

            $this->basic->representative_kana = $account->representative_kana;

            $this->basic->email = $account->email;
            $this->basic->password = $account->password;
            $this->basic->security_question_id = $account->security_question_id;
            $this->basic->security_question_answer = $account->security_question_answer;
            $representative_full = explode("　", $this->basic->representative);
            $this->basic->representative_family = $representative_full[0];
            $this->basic->representative_given = $representative_full[1];
            $representative_kana_full = explode("　", $this->basic->representative_kana);
            $this->basic->representative_family_kana = $representative_kana_full[0];
            $this->basic->representative_given_kana = $representative_kana_full[1];
            $postal_code_full = explode("-", $this->basic->postal_code);
            $this->basic->post_A = $postal_code_full[0];
            $this->basic->post_B = $postal_code_full[1];
            $telephone_full = explode("-", $this->basic->telephone);
            $this->basic->tel_A = $telephone_full[0];
            $this->basic->tel_B = $telephone_full[1];
            $this->basic->tel_C = $telephone_full[2];
            $fax_code_full = explode("-", $this->basic->fax_code);
            $this->basic->fax_A = $fax_code_full[0];
            $this->basic->fax_B = $fax_code_full[1];
            $this->basic->fax_C = $fax_code_full[2];

            $this->basic->is_admin = $account->is_admin;
            $this->basic->creditcard_id = $account->creditcard_id; 
            $this->basic->card_status = $account->card_status;
            $this->view->basic = $this->basic;
        }

        $this->checkAdmin();
    }
       /**
        *  ログインセッションの有無を判定
        */
    public function isLoggedIn() {

        return isset($this->session->get('auth')['id']);
    }
        /**
        *  ログインしていなければ表示されるべきでないページにおいて、ログインセッションが存在しない場合、
         　login/indexへリダイレクトする
        */
    public function checkLogin() {

        $actionsForGuest = ['account/index',
            'account/input',
            'account/confirm',
            'account/thanks',
            'account/accountModifyDeleteThanks',
            'account/law',
            'login/index',
            'login/resetPassword',
            'login/resetPasswordComplete',
            'login/accountCheck',
            'login/checkDuplicateMailForInput',
            'login/checkDuplicateMailForAccountModify',
            'login/checkIsUser',
            'customer/contract',
            'customer/displayArea',
            'listing/preview',
            'rectangle/preview',
            'pickup/preview',
            'customer/index',
            'customer/make',
            'customer/charge',
            'customer/price',
            'customer/changeMonth',
            'customer/displayArea',
            'customer/contract',
            'customer/contact',
            'customer/chargeOffers',
            'customer/law',
            'customer/terms',
            'customer/trading'
            ];

        $uriArray = explode('?', $_SERVER['REQUEST_URI']);

        if(!$this->isLoggedIn()) {
            $hasUri = false;
            foreach($actionsForGuest as $action) {
                if(stristr($uriArray[0], $action)) {
                    $hasUri = true;
                }
            }
            if(!$hasUri) {
                return $this->response->redirect('login/index');
            }
        }
    }
        /**
        *  管理者アカウントでのみ表示されるべきページにおいて、ログインしているのが管理者アカウントでない場合、
         　customer/indexへリダイレクト
        */
    public function checkAdmin() {

        $is_admin_page = false;

        if(stristr($_SERVER['REQUEST_URI'], 'admin' )) {
            $is_admin_page = true;
        }

        if(stristr($_SERVER['REQUEST_URI'], 'customer')) {
            return true;
        }

        if($is_admin_page) {
            if($this->basic->is_admin != 1) {
                return $this->response->redirect('customer/index');
            } else {
                return true;
            }
        }
    }
}
