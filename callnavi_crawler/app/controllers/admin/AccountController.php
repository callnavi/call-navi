<?php

class AccountController extends AdminControllerBase {
    
    public function indexAction() {
        
    }
         /**
        * アカウントフォーム入力場面。
        *確認画面へ進む」ボタン押下により、account/confirmへ、postで値を送る
        * account/confirmから、「入力内容を修正する」で戻った場合には、postで値を受け取る事により入力内容が保持される
        */
    public function inputAction() {
        $this->assets->addJs('admin_scripts/account/input.js');
        $this->assets->addCss('admin_css/error_red.css');
        $request = new \Phalcon\Http\Request();
        if ($request->isPost() == true) {
            $this->view->data = $this->__setAccountData($request->getPost());

            $strDate = 'tomorrow'; //翌日の0時までキャッシュさせる場合
            $dt = new DateTime($strDate);
            header('Cache-Control: max-age=' . ($dt->format('U') - time()));

        }
    }
         /**
        * アカウントフォーム確認場面。
        * account/inputからpostで入力値を受け取り、画面に表示。この段階ではDBに保存せず、「この内容で登録する」押下により
        * account/thanksへpostで再度送る。
        *「入力内容を修正する」押下により、account/inputにpostで値を送り直す。 
        */
    public function confirmAction() {

        $this->assets->addCss('admin_css/error_red.css');
        $request = new \Phalcon\Http\Request();
        if ($request->isPost() == true) {

        $this->view->data = $this->__setAccountData($request->getPost());
        $strDate = 'tomorrow'; //翌日の0時までキャッシュさせる場合
        $dt = new DateTime($strDate);
        header('Cache-Control: max-age=' . ($dt->format('U') - time()));

     }
  }

            /**
        * アカウントフォーム登録完了場面。
        * account/confirmからpostで値を受け取り、DB(accounts)へ格納。
        * また、出向者管理者双方へアカウント登録完了の旨のメールを送信。
        */
    public function thanksAction() {

        $request = new \Phalcon\Http\Request();
        if ($request->isPost() == true) {
            $data = $this->__setAccountData($request->getPost());
            $this->useModel('Account');
            $password_changed = strlen($data['password']) == 0  ? $this->basic->password: $this->Account->encryptPassword($data['password']);
            $return_data = $this->Account->addAccount($data);

            $this->Account->sendThanksMail($return_data);
            $this->Account->sendThanksAdminMail($return_data->email);
            
            //$this->view->email = $return_data->email;

            $strDate = 'tomorrow'; //翌日の0時までキャッシュさせる場合
            $dt = new DateTime($strDate);
            header('Cache-Control: max-age=' . ($dt->format('U') - time()));
        }
    }
    
             /**
        * アカウント情報変更確認開始場面
        * DBに登録されたアカウント情報を画面に表示
        */
    
    public function accountModifyAction() {    
        $this->assets->addCss('admin_css/error_red.css');    

    }
         /**
         * アカウント情報変更確認フォーム入力場面
         * フォームには、DBで登録された情報が、パスワードを除き初期値として表示される。
         * account/accountModifyConfirmから、「入力内容を修正する」押下により、postで
         * 値を受け取る。
         */
    public function accountModifyStartAction() {
        $this->assets->addJs('admin_scripts/account/accountModifyStart.js');
        $this->assets->addCss('admin_css/error_red.css');
        
        $request = new \Phalcon\Http\Request();
        if ($request->isPost() == true) {
            $this->view->data = $this->__setAccountData($request->getPost());

            $strDate = 'tomorrow'; //翌日の0時までキャッシュさせる場合
            $dt = new DateTime($strDate);
            header('Cache-Control: max-age=' . ($dt->format('U') - time()));
        }
    }

    
        /**
        * アカウント情報変更確認フォーム確認場面
        * account/accountModifyStartから、「確認画面へ進む」押下により、postで
        * 値を受け取り、画面に表示
        * この内容で登録する」押下によりaccountModifyThanksへ、
        *「入力内容を修正する」押下によりacccount/accountModifyStartへ
        * それぞれ値を送る。
        */
    public function accountModifyConfirmAction() {

         $this->assets->addCss('admin_css/error_red.css');

        $request = new \Phalcon\Http\Request();
        if ($request->isPost() == true) {
            $this->view->data = $this->__setAccountData($request->getPost());

            $strDate = 'tomorrow'; //翌日の0時までキャッシュさせる場合
            $dt = new DateTime($strDate);
            header('Cache-Control: max-age=' . ($dt->format('U') - time()));
        }
    }
        /**
        * アカウント情報変更完了場面
        * account/accountModifyConfirmからpostで
        * 値を受け取り、DB(accounts)のデータを更新
        */
    public function accountModifyCompleteAction() {

        $request = new \Phalcon\Http\Request();

        if ($request->isPost() == true) {

            $data = $this->__setAccountData($request->getPost());
            $this->useModel('Account');

            $password_changed = strlen($data['password']) == 0  ? $this->basic->password: $this->Account->encryptPassword($data['password']);
            $this->Account->changeAccount($data, $this->basic->id, $password_changed);

            $strDate = 'tomorrow'; //翌日の0時までキャッシュさせる場合
            $dt = new DateTime($strDate);
            header('Cache-Control: max-age=' . ($dt->format('U') - time()));
        }
    }   

    //リファクタリング用のテストメソッド
    private function __setAccountData($inputData) {
        $outputData = [];

        $fields = ['company_name', 'company_name_kana', 'postal_code', 'post_A', 'post_B', 'address', 'telephone', 'tel_A', 'tel_B', 'tel_C', 'fax_code', 'fax_A', 'fax_B', 'fax_C', 'company_url', 'company_section', 'company_position', 'representative', 'representative_family', 'representative_given','representative_kana', 'representative_family_kana', 'representative_given_kana', 'email', 'password', 'security_question_id', 'security_question_display', 'security_question_answer'];
       
         foreach ($fields as $field) {
            if (isset($inputData[$field])) {
                $outputData[$field] = $inputData[$field];
            }
        }
        return $outputData;
    }
         /**
        *「このアカウントを解約する」押下により、confirmの後、account/accountModifyDeleteへ移る
        *この際、postでアカウントのIDを送る(セッションが破棄されるため)
        */
    public function accountModifyDeleteAction() {
        $this->assets->addJs('admin_scripts/account/accountModifyDelete.js');
        //viewのみ
    }
         /**
        *  account/accountModifyDeleteよりアカウントのIDをpostで受け取った上でセッション破棄
          　受け取ったアカウントIDのアカウントを論理削除(accountsのdeletedカラムを1に)
          　同時に、そのアカウントに紐付いたすべての広告データ(テーブルはlisting_offers, listing_keywords, rectangle_offers,pickup_offers,pickup_offer_details, free_ofers)
           について論理削除
        */
    public function accountModifyDeleteThanksAction() {
        $this->session->destroy();
        $request = new \Phalcon\Http\Request();
        if ($request->isPost() == true) {
            $account_id = $request->getPost('account_id');
        }
        $this->useModel('Account');
        $this->Account->deleteAccount($account_id);
   
        $this->useModel('ListingOffer');
        $listings = $this->ListingOffer->find("account_id = $account_id");
        foreach ($listings as $listing) {
           $listing->softDelete();
           $offer_id = $listing->id;
           $this->useModel('ListingKeyword');
           $this->ListingKeyword->softDeleteKeywords($offer_id);
        }

        $this->useModel('RectangleOffer');
        $rectangles = $this->RectangleOffer->find("account_id = $account_id");
        foreach ($rectangles as $rectangle) {
            $rectangle->softDelete();
        }

        $this->useModel('PickupOffer');
        $pickups = $this->PickupOffer->find("account_id = $account_id");
        foreach ($pickups as $pickup) {
            $pickup->softDelete();
            $this->PickupOffer->completePickupOffer($pickup);
        }
       
        
    }

    public function lawAction() {
        //viewのみ
    }
    

}
