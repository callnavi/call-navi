<?php

class FreeController extends AdminControllerBase {

use ImageEditable;

use Mailable;

use ScopeCalculatable;
        /**
        * 無料広告作成フォーム入力場面。
         「確認画面へ進む」ボタン押下により、free/makeConfirmへ、postで値を送る
           free/makeConfirmから、「入力内容を修正する」で戻った場合には、postで受け取ったfree_offersテーブルのidに基づきDB(free_offersテーブル)に格納されたデータを初期値として保持する事により入力内容が保持される
        */

    public function makeAction() {

        $this->assets->addJs('admin_scripts/free/make.js');
        $this->assets->addCss('admin_css/error_red.css');
        $this->useModel('FreeOffer');

        $request = new \Phalcon\Http\Request();
        //一度確認画面に進んだ後に広告作成フォームに戻った場合
        if ($request->isPost() == true) {

            $offer_id = $request->getPost('offer_id');
            $data = $this->FreeOffer->findOnConfirmFreeOfferById($offer_id);
            $hired_as = explode(":", $data->hired_as);
            $features = explode(":", $data->features);
            $data->features_array = $features;
            $this->view->features = $features;
            $this->view->hired_as = $hired_as;
            $this->view->data = $data;
            $this->view->post = 1;
        } else {
        //新規に広告作成フォームを開いた場合
            $this->view->post = 0;
        }
        
        $strDate = 'tomorrow'; //翌日の0時までキャッシュさせる場合
        $dt = new DateTime($strDate);
        header('Cache-Control: max-age=' . ($dt->format('U') - time()));
    }
         /**
        * 無料広告作成フォーム確認場面。
           free/makeからpostで入力値を受け取り、入力に内容に応じてデータをDB(free_offersテーブル)に格納、及び確認画面に表示。
           free/makeで添付された画像をpublic/images/freeに、「広告ID.png」という形式で保存及び確認画面に表示
          　データについて、statusカラムはediting(編集中)、is_confirmedカラム(作成完了か否かを判断)は1となる
           入力内容の保存処理により既にis_confirmedカラムが1であるデータが存在する場合には、データ新規作成処理ではなく
          　データ更新処理となる。この場合、is_confirmedカラムは1のまま。
        */
    public function makeConfirmAction() {

        $request = new \Phalcon\Http\Request();
        if ($request->isPost() == true) {
            $offer_id_set = $request->getPost('offer_id_set');
            //新たにデータ作成となる場合($offer_id_setは-1となっている)
            if ($offer_id_set == "-1") {
                $data = $this->__setFreeData($request->getPost());
                $data['account_id'] = $this->basic->id;
                $this->useModel('FreeOffer');
                $this->view->data = $newOffer = $this->FreeOffer->addFreeOffer($data);
            //既にあるデータの更新処理となる場合($offer_id_setは、更新されるべき広告レコードのIDとなっている)
            } else {
                $data = $this->__setFreeData($request->getPost());
                $this->useModel('FreeOffer');
                $data['id'] = intval($offer_id_set);
                


                $data['account_id'] = $this->basic->id;

                $this->view->data = $newOffer = $this->FreeOffer->updateFreeOffer($data, "editing");
                
            }

            $hired_as = explode(":", $newOffer->hired_as);
            $features = explode(":", $newOffer->features);
            $this->view->features = $features;
            $this->view->hired_as = $hired_as;
            
            $strDate = 'tomorrow'; //翌日の0時までキャッシュさせる場合
            $dt = new DateTime($strDate);
            header('Cache-Control: max-age=' . ($dt->format('U') - time()));



            if ($this->request->hasFiles() == true) {
               
                foreach ($this->request->getUploadedFiles() as $file) {
                    
                    $image = $this->getImage($file->getTempName(), $file->getType());

                    $image = $this->minimizeImage($image, 200);

                    $imageUrl = '/images/free/' . $newOffer->id . '.png';
                    $toPath = __DIR__ . '/../../../public' . $imageUrl;
                    if (file_exists($toPath)) {
                        unlink($toPath);
                    }
                    $this->locatePngImage($image, $toPath);

                }
            }

        }
        
    }
            /**
        * 無料広告作成完了場面。
           既に存在するデータについて、statusカラムをjudging(編集中)とする。。
           また、出向者管理者双方へ無料広告作成完了の旨のメールを送信。
        */
    public function makeThanksAction() {

        $request = new \Phalcon\Http\Request();

        if ($request->isPost() == true) {
    
            $this->useModel('FreeOffer');
            $offer_id = $request->getPost('offer_id');


            $this->FreeOffer->confirmFreeOffer($offer_id);

            $this->FreeOffer->sendThanksMail($offer_id);
            $this->FreeOffer->sendThanksAdminMail($offer_id);

            $strDate = 'tomorrow'; //翌日の0時までキャッシュさせる場合
            $dt = new DateTime($strDate);
            header('Cache-Control: max-age=' . ($dt->format('U') - time()));
        }
    }
    
            /**
        * ajax通信先
          無料広告保存場面。(free/makeにおいて、「入力内容を保存する」を押下した場合)
           free/makeからpostで入力値を受け取り、入力内容に応じてDB(free_offersテーブル)にデータを格納(ajax通信)
           free/makeで添付された画像をpublic/images/freeに、「広告ID.png」という形式で保存
           statusカラムはediting(編集中)、is_confirmedカラムは1となる。
           入力内容の保存処理が既に一度以上行われており既にis_confirmedカラムが1であるデータが存在する場合には、データ新規作成処理ではなく
          　データ更新処理となる。
        */
    public function saveAction() {

        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        $request = new \Phalcon\Http\Request();

        if ($request->isPost() == true) {

            $offer_id_set = $request->getPost('offer_id_set');
            if ($offer_id_set == "-1") {
                $data = $this->__setFreeData($request->getPost());
                $data['account_id'] = $this->basic->id;
                $this->useModel('FreeOffer');
                $newOffer = $this->FreeOffer->saveFreeOffer($data);
            } else {
                $data = $this->__setFreeData($request->getPost());
                $this->useModel('FreeOffer');
                $data['id'] = intval($offer_id_set);
                $data['account_id'] = $this->basic->id;
                $this->FreeOffer->updateFreeOffer($data, "editing");
                $newOffer = new stdClass;
                $newOffer->id = $data['id'];
            }
        }

        if ($this->request->hasFiles() == true) {
            foreach ($this->request->getUploadedFiles() as $file) {
                $image = $this->getImage($file->getTempName(), $file->getType());
                $image = $this->minimizeImage($image, 200);
                $imageUrl = '/images/free/' . $newOffer->id . '.png';
                $toPath = __DIR__ . '/../../../public' . $imageUrl;
                if (file_exists($toPath)) {
                    unlink($toPath);
                }
                $this->locatePngImage($image, $toPath);
            }
        }

        echo $newOffer->id;
    }   
        /**
        * 無料広告編集フォーム入力場面。
          free_offersテーブルのidの値をgetで受け取る事により、該当するデータの値をフォーム上に初期値として表示
         「確認画面へ進む」ボタン押下により、free/editConfirmへ、postで値を送る
           free/editConfirmから、「入力内容を修正する」で戻った場合には、DB(free_offersテーブル)に格納されたデータを初期値として保持する事により入力内容が保持される
        */
    public function editAction() {

        $this->assets->addJs('admin_scripts/free/edit.js');
        $this->assets->addCss('admin_css/error_red.css');

        $request = new \Phalcon\Http\Request();

        if ($request->isGet() == true) {
            if ($this->basic->id == $request->getQuery('account_id') or $this->basic->is_admin == 1) {
                $this->useModel('FreeOffer');
                $data = $this->FreeOffer->findFreeOfferById($request->getQuery('offer_id'));
                $this->view->data = $data;
                $hired_as = explode(":", $data->hired_as);
                $features = explode(":", $data->features);
         
                $this->view->features = $features;
                $this->view->hired_as = $hired_as;
            } else {
                $this->response->redirect('customer/index');
            }

            $strDate = 'tomorrow'; //翌日の0時までキャッシュさせる場合
            $dt = new DateTime($strDate);
            header('Cache-Control: max-age=' . ($dt->format('U') - time()));
        }
    }
       /**
        * 無料広告作成フォーム確認場面。
           free/editからpostで入力値を受け取り、入力に内容に応じて該当データを更新、及び確認画面に表示。
           free/makeで添付された画像をpublic/images/freeに、「広告ID.png」という形式で保存及び確認画面に表示
          　statusカラムはediting(編集中)、is_confirmedカラムは1のまま
        */
    public function editConfirmAction() {

        $request = new \Phalcon\Http\Request();


        if ($request->isPost() == true) {
            $data = $this->__setFreeData($request->getPost());
            $data['account_id'] = $this->basic->id;
            $data['id'] = $request->getPost('offer_id');
            $this->useModel('FreeOffer');

            
            
            
            $this->view->data = $newOffer = $this->FreeOffer->updateFreeOffer($data, 'editing');
            $hired_as = explode(":", $newOffer->hired_as);
            $features = explode(":", $newOffer->features);
            $this->view->features = $features;
            $this->view->hired_as = $hired_as;
            
            if ($this->request->hasFiles() == true) {
                foreach ($this->request->getUploadedFiles() as $file) {
                    $image = $this->getImage($file->getTempName(), $file->getType());
                    $image = $this->minimizeImage($image, 200);
                    $imageUrl = '/images/free/' . $data['id'] . '.png';
                    $toPath = __DIR__ . '/../../../public' . $imageUrl;
                    if (file_exists($toPath)) {
                        unlink($toPath);
                    }
                    $this->locatePngImage($image, $toPath);
             

                    $strDate = 'tomorrow'; //翌日の0時までキャッシュさせる場合
                    $dt = new DateTime($strDate);
                    header('Cache-Control: max-age=' . ($dt->format('U') - time()));
                }
            }
        }
        
    }
            /**
        * 無料広告編集完了場面。
          既に存在する該当データについて、statusカラムをediting(編集中)からjudging(審査中)に変更。
           また、出向者管理者双方へ無料広告作成完了の旨のメールを送信。
        */
    public function editThanksAction() {

        $request = new \Phalcon\Http\Request();

        if ($request->isGet() == true) {
            if ($request->getQuery('account_id') or $this->basic->is_admin == 1) {
                $this->useModel('FreeOffer');
                $offer_id = $request->getQuery('offer_id');
                $offer = $this->FreeOffer->findFreeOfferById($offer_id);
                $offer->status = 'judging';
                $offer->is_confirmed = 1;
                $date = new DateTime();
                $today = $date->format('Y-m-d H:i:s');
                $offer->accepted = $today;
                $offer->updateColumn();
                
                $this->FreeOffer->sendThanksMail($offer_id);
                $this->FreeOffer->sendThanksAdminMail($offer_id);
                

                $strDate = 'tomorrow'; //翌日の0時までキャッシュさせる場合
                $dt = new DateTime($strDate);
                header('Cache-Control: max-age=' . ($dt->format('U') - time()));
            } else {
                $this->response->redirect("customer/index");
            }
        }
    }
        /**
        *  ajax通信先
           無料広告作成フォーム保存場面。
           free/editからpostで入力値を受け取り、入力に内容に応じて該当データを更新(ajax通信)
          　statusカラムはediting(編集中)、is_confirmedカラムは1のまま
        */
    public function editSaveAction() {

        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        $request = new \Phalcon\Http\Request();

        if ($request->isPost() == true) {
            $data = $this->__setFreeData($request->getPost());
            $data['account_id'] = $this->basic->id;
            $data['id'] = $request->getPost('offer_id');
            $this->useModel('FreeOffer');
            $this->view->data = $this->FreeOffer->updateFreeOffer($data, 'editing');



            if ($this->request->hasFiles() == true) {
                foreach ($this->request->getUploadedFiles() as $file) {
                    $image = $this->getImage($file->getTempName(), $file->getType());
                    $image = $this->minimizeImage($image, 200);
                    $imageUrl = '/images/free/' . $data['id'] . '.png';
                    $toPath = __DIR__ . '/../../../public' . $imageUrl;
                    if (file_exists($toPath)) {
                        unlink($toPath);
                    }
                    $this->locatePngImage($image, $toPath);
                }
            }
        }
    }

    
    
        /**
        *  無料広告プレビュー表示画面(管理画面の広告一覧から、プレビューの下の目マークを押下した場合)
          getで受け取ったfree_offersのidに基づき、該当する無料広告のプレビューを表示
        */
    public function previewAction() {
        $this->assets->addCss('admin_css/error_red.css');
        $request = new \Phalcon\Http\Request();

        if ($request->isGet() == true) {

            $this->useModel('FreeOffer');
            
            $data = $this->FreeOffer->findFreeOfferById($request->getQuery('offer_id'));
            $hired_as = explode(":", $data->hired_as);
            $features = explode(":", $data->features);
            $this->view->data = $data;
            $this->view->features = $features;
            $this->view->hired_as = $hired_as;
        }
    }
        /**
        *  入力フォームに入力され、postで送られた各々のデータを一つの配列に格納するに際しての共通処理
        */
    private function __setFreeData($inputData) {

        $outputData = [];

        $fields = ['title', 'has_pic', 'detail_url', 'job_category', 'hired_as', 'hired_as_string', 'company_name', 'salary_term', 'salary_value_min', 'salary_value_max', 'salary_unit_min', 'salary_unt_max', 'workplace_prefecture', 'workplace_area', 'workplace_address', 'job_description', 'features', 'features_string', 'has_pic', 'click_price', 'budget_max', 'detail_url'];
        foreach ($fields as $field) {
            if (isset($inputData[$field])) {
                $outputData[$field] = $inputData[$field];
            }
        }
        return $outputData;
    }
        /**
        *  ajax通信先
           customer/indexの無料広告において対象期間を変更して「適用」を押下した場合において、しかるべきデータを表示
        */
    public function changeScopeAction() {

        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
                $free_scope = $this->calScope($request->getQuery('free_scope'));
                $this->useModel('FreeOffer');
                $offers = $this->FreeOffer->findFreeOffers($free_scope, $this->basic->id);
                
                $data = array("offers" => $offers);
                header('Content-Type:application/json');
                echo json_encode($data);
            }
        }
    }
       /**
        *  ajax通信先
           admin/allowedOffersの無料広告において対象期間を変更して「適用」を押下した場合において、しかるべきデータを表示
        */
    public function changeScopeForAdminAction() {

        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
                $free_scope = $this->calScope($request->getQuery('free_scope'));
                $this->useModel('FreeOffer');
                $offers = $this->FreeOffer->findFreeOffers($free_scope, 'all');
                
                $data = array("offers" => $offers);
                header('Content-Type:application/json');
                echo json_encode($data);
            }
        }
    }
       /**
        *  ajax通信先
           admin/accountListsContentの無料広告において対象期間を変更して「適用」を押下した場合において、しかるべきデータを表示
        */
    public function changeScopeForAccountsAction() {

        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
                $free_scope = $this->calScope($request->getQuery('free_scope'));
                $account_id = $request->getQuery('account_id');
                $this->useModel('FreeOffer');
                $offers = $this->FreeOffer->findFreeOffers($free_scope, $account_id);
                
                $data = array("offers" => $offers);
                header('Content-Type:application/json');
                echo json_encode($data);
            }
        }
    }   
        /**
        *  ajax通信先
           customer/indexにおいて無料広告を一時停止した場合、該当する無料広告のstatusカラムをcanceled(一時停止中)に変更
        */
    public function cancelOfferAction() {

        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
                $scope = $this->calScope($request->getQuery('scope'));
                $this->useModel('FreeOffer');
                $offer = $this->FreeOffer->findFreeOfferById($request->getQuery('offer_id'));
                $offer->status = "canceled";
                $offer->updateColumn();
                $offers = $this->FreeOffer->findFreeOffers($scope, $this->basic->id);
                $data = array("offer_id" => $offer->id, "offers" => $offers);
                header('Content-Type:application/json');
                echo json_encode($data);
            }
        }
    }
        /**
        *  ajax通信先
           customer/indexにおいて一時停止中の無料広告を有効にした場合、該当する無料広告のstatusカラムをpublishing(掲載中)に変更
        */
    public function effectOfferAction() {

        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
                $scope = $this->calScope($request->getQuery('scope'));
                $this->useModel('FreeOffer');
                $offer = $this->FreeOffer->findFreeOfferById($request->getQuery('offer_id'));
                $offer->status = "publishing";
                $offer->updateColumn();

                $offers = $this->FreeOffer->findFreeOffers($scope, $this->basic->id);
                $data = array("offer_id" => $offer->id, "offers" => $offers, "status" => $offer->status);
                header('Content-Type:application/json');
                echo json_encode($data);
            }
        }
    }
        /**
        *  ajax通信先
           customer/indexにおいて無料広告を削除した場合、該当する無料広告のdeletedカラムを1に変更
         　この場合、出稿者ページにおいては表示されなくなり、管理者ページにおいてはステータスが「ユーザー削除」として
         　表示されるようになる。
        */
    public function deleteOfferAction() {

        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
                $scope = $this->calScope($request->getQuery('scope'));
                $this->useModel('FreeOffer');
                $offer = $this->FreeOffer->findFreeOfferById($request->getQuery('offer_id'));
                $offer->softDelete();
                $offers = $this->FreeOffer->findFreeOffers($scope, $this->basic->id);
                $count = count($offers);
                
                $data = array("offer_id" => $offer->id, "offers" => $offers, "count" => $count);
                header('Content-Type:application/json');
                echo json_encode($data);
            }
        }
    }
        /**
        *  ajax通信先
         　free/make　及びfree/editにおいて、画像登録済みの場合のみ表示される「画像を削除する」ボタンを押下した際に、
         　該当する無料広告のhas_picカラムを1から0へと変更し、その広告データが画像未登録のものとして扱われるようにする
        */
    public function deletePictureAction() {
        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);
        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
                $this->useModel('FreeOffer');
                $offer_id = $request->getQuery('offer_id');
                $offer = $this->FreeOffer->findOnConfirmFreeOfferById($offer_id);
                $offer->has_pic = 0;
                $offer->updateColumn();
            }
        }
    }
       /**
        *  ajax通信先
         　無料広告の出稿数制限をチェックする
        */
    public function checkMaxAction() {
        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);
        
        if ($request->isGet() == true) {
            $this->useModel('FreeOffer');
            $account_id = $this->basic->id;
            $my_frees_now = $this->FreeOffer->countMyOffersNow($account_id);
            
            echo $my_frees_now > 2 ? 1 : 0;
         }
       

    }
      //ローカルテスト用
//    public function stopAction() {
//        $this->useModel('FreeOffer');
//        $offers = $this->FreeOffer->findStoppingOffers();
//        echo $offers[0]->title;
//        exit;
//    }
//    
//    public function dayAction() {
//        $today = date("Y/m/d");
//        $seven_day_after = date('Y/m/d', strtotime("$today + 7 day"));
//        $six_month_ago = date('Y/m/d', strtotime("$seven_day_after - 6 month"));
//        $six_month_ago_next = date('Y/m/d', strtotime("$six_month_ago + 1 day")); 
//        echo $today;
//        exit;
//    
//    }
    
}