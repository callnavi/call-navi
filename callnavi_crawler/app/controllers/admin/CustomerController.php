<?php

class CustomerController extends AdminControllerBase {

    use ScopeCalculatable;
           /**
        *  出向者側トップページ
         　出向者のアカウントで作成したそれぞれの広告を、承認日付(allowed)による降順で表示
        */
    public function indexAction() {

            $this->assets  
            ->addJs('admin_scripts/changeScope.js')
            ->addJs('admin_scripts/customer/effectOffer.js')
            ->addJs('admin_scripts/customer/cancelOffer.js')
            ->addJs('admin_scripts/customer/completeOffer.js')
            ->addJs('admin_scripts/customer/deleteOffer.js')
            ->addJs('admin_scripts/customer/freeLimit.js');
            
//            if (isset($this->basic->id)) {

                $scope = $this->calScope("today");
                $this->useModel('ListingOffer');
                $listing_offers = $this->ListingOffer->findAllowedListingOffers($scope, $this->basic->id);
                $listing_expense_sum = $this->calExpenseSum($listing_offers);
                $listing_cardless_expense_sum = $this->calCardlessExpenseSum($listing_offers);
                $this->view->listing_count = count($listing_offers);
                $this->view->listing_offers = $listing_offers;
                $this->view->listing_expense_sum = $listing_expense_sum;
                $this->view->listing_cardless_expense_sum = $listing_cardless_expense_sum;

                $this->useModel('ListingKeyword');

                $this->useModel('RectangleOffer');
                $rectangle_offers = $this->RectangleOffer->findAllowedRectangleOffers($scope, $this->basic->id);
                $rectangle_expense_sum = $this->calExpenseSum($rectangle_offers);
                $rectangle_cardless_expense_sum = $this->calCardlessExpenseSum($rectangle_offers);
                $this->view->rectangle_count = count($rectangle_offers);
                $this->view->rectangle_offers = $rectangle_offers;
                $this->view->rectangle_expense_sum = $rectangle_expense_sum;
                $this->view->rectangle_cardless_expense_sum = $rectangle_cardless_expense_sum;

                $pickup_scope = $this->calScope("all");
                $this->useModel('PickupOffer');
                $pickup_offers = $this->PickupOffer->findAllowedPickupOffers($pickup_scope, $this->basic->id);
                $pickup_expense_sum = $this->calExpenseSum($pickup_offers);
                $pickup_cardless_expense_sum = $this->calCardlessExpenseSum($pickup_offers);
                $this->view->pickup_count = count($pickup_offers);
                $this->view->pickup_offers = $pickup_offers;
                $this->view->pickup_expense_sum = $pickup_expense_sum;
                $this->view->pickup_cardless_expense_sum = $pickup_cardless_expense_sum;

                $this->useModel('FreeOffer');
                $free_offers = $this->FreeOffer->findAllowedFreeOffers($scope, $this->basic->id);
                $this->view->free_count = count($free_offers);
                $this->view->free_offers = $free_offers;
                $today = date('Y-m-d H:i:s');
                $six_month_ago = date('Y-m-d H:i:s', strtotime("$today - 6 month"));
                $this->view->six_month_ago = $six_month_ago;

                $this->view->today = date("Y/m/d");
                $this->view->one_week_ago = date("Y/m/d", strtotime("-6 day"));  
                
//        }

       // 20150901 �����[�X�Ή�
       $this->view->serverHost = $_SERVER["SERVER_NAME"];
}
         /**
        *  広告作成トップページ
         
        */
public function makeAction() {
       $this->assets->addJs('admin_scripts/customer/make.js');
       $this->assets->addJs('admin_scripts/customer/freeLimit.js');
       
       // 20150901 �����[�X�Ή�
       $this->view->serverHost = $_SERVER["SERVER_NAME"];
}
      /**
        *  クレジットカードの請求明細表示
       　　　対象期間プルダウンは、アカウントの登録日付から先月分まで
        */
public function chargeAction() {
    
        $this->assets->addJs('admin_scripts/customer/changeMonth.js');

//        if (isset($this->basic->id)) {
            $scope = $this->changeMonth(-1);

            $this->useModel('ListingOffer');
            $listing_offers = $this->ListingOffer->findListingOffersForCharge($scope, $this->basic->id);
            $listing_expense_sum = $this->calExpenseSum($listing_offers);

            $this->useModel('RectangleOffer');
            $rectangle_offers = $this->RectangleOffer->findRectangleOffersForCharge($scope, $this->basic->id);
            $rectangle_expense_sum = $this->calExpenseSum($rectangle_offers);

            $this->useModel('PickupOffer');
            $pickup_offers = $this->PickupOffer->findPickupOffersForCharge($this->basic->id, $scope);
            $pickup_expense_sum = $this->calExpenseSum($pickup_offers);

            $offers = array_merge($listing_offers, $rectangle_offers, $pickup_offers);
            $expense_sum = $listing_expense_sum + $rectangle_expense_sum + $pickup_expense_sum;
            $tax = floor( $expense_sum * 0.08 );

            $months = $this->calMyMonths();

            $this->view->months = $months;
            $this->view->offers = $offers;
            $this->view->expense_sum = $expense_sum;
            $this->view->tax = $tax;
            $this->view->pay_month = $scope['pay_month'];
            $this->view->pay_year = $scope['pay_year'];
            
            $this->view->a = 'c';
//    }
}
        /**
        * ajax通信先
          customer/indexにおいて、それぞれの広告について対象期間を変更して「適用」を押した場合に、
          しかるべきデータを表示
        */
public function changeMonthAction() {

        $request = new Phalcon\Http\Request;

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {

                $scope = $this->changeMonth($request->getQuery('ago'));

                $this->useModel('ListingOffer');
                $listing_offers = $this->ListingOffer->findListingOffersForCharge($scope, $this->basic->id);
                $listing_expense_sum = $this->calExpenseSum($listing_offers);

                $this->useModel('RectangleOffer');
                $rectangle_offers = $this->RectangleOffer->findRectangleOffersForCharge($scope, $this->basic->id);
                $rectangle_expense_sum = $this->calExpenseSum($rectangle_offers);

                $this->useModel('PickupOffer');
                $pickup_offers = $this->PickupOffer->findPickupOffersForCharge($this->basic->id, $scope);
                $pickup_expense_sum = $this->calExpenseSum($pickup_offers);

                $offers = array_merge($listing_offers, $rectangle_offers, $pickup_offers);
                $expense_sum = $listing_expense_sum + $rectangle_expense_sum + $pickup_expense_sum;
                $tax = floor( $expense_sum * 0.08 );
                $pay_month = $scope['pay_month'];
                $pay_year = $scope['pay_year'];
                $this->view->offers = $offers;
                $this->view->expense_sum = $expense_sum;
                $this->view->tax = $tax;
                $this->view->pay_month = $scope['pay_month'];
                $this->view->pay_year = $scope['pay_year'];
                $this->view->start();
                $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
                $this->view->render('customer', 'chargeOffers');
                $this->view->finish();
                
                $offers_data = $this->view->getContent();
                header('Content-Type:application/json');
                echo json_encode($offers_data);            

        }
    }
}

    public function displayAreaAction() {
           //viewのみ
        $this->view->setLayout('body/blank_template');
    }

    public function contractAction() {

        
    }
    
    public function contactAction() {
           //viewのみ
        
    }
    
    public function charge_offersAction() {
      
    }
   
    public function lawAction() {
           //viewのみ
    }
    
    public function priceAction() {
           //viewのみ
	   $this->view->pick("customer/price");
   	}

    public function termsAction() {
            //viewのみ
            $this->view->pick("customer/terms");
    }

    public function tradingAction() {
            //viewのみ
            $this->view->pick("customer/trading");
    }
}
