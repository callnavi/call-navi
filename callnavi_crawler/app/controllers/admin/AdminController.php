<?php

class AdminController extends AdminControllerBase {

    use ScopeCalculatable;
    use VariablesCalculatable;
    
         /**
        * 審査待ちの広告を、審査受け付け日昇順に表示
        */
    public function indexAction() {

        $this->assets
                ->addJs('admin_scripts/allowOffer.js')
                ->addJs('admin_scripts/woAllowOffer.js')
                ->addJs('admin_scripts/notAllowOffer.js');

        $scope = $this->calScope('all');  

        /* Listing Offer Data */
        $this->useModel('ListingOffer');
        //$listing_offers = $this->ListingOffer->findListingOffers($scope, 'admin');
        $listing_offers = $this->ListingOffer->findAcceptedListingOffers($scope, 'admin'); 
        $this->view->listing_count = count($listing_offers);
        $this->view->listing_offers = $listing_offers;

        /* Rectangle Offer Data */
        $this->useModel('RectangleOffer');
        //$rectangle_offers = $this->RectangleOffer->findRectangleOffers($scope, 'admin');
        $rectangle_offers = $this->RectangleOffer->findAcceptedRectangleOffers($scope, 'admin'); 
        $this->view->rectangle_count = count($rectangle_offers);
        
        $this->view->rectangle_offers = $rectangle_offers;

        /* Pickup Offer Data */
        $this->useModel('PickupOffer');
        //$pickup_offers = $this->PickupOffer->findPickupOffers($scope, 'admin');
        $pickup_offers = $this->PickupOffer->findAcceptedPickupOffers($scope, 'admin');
        $this->view->pickup_count = count($pickup_offers);
        $this->view->pickup_offers = $pickup_offers;
        
        $this->useModel('FreeOffer');
        $free_offers = $this->FreeOffer->findAcceptedFreeOffers($scope, 'admin');
        $this->view->free_count = count($free_offers);
        $this->view->free_offers = $free_offers;
    }
         /**
        * ajax通信先
          リスティング広告通常承認処理
          該当する審査待ちのリスティング広告のステータスを、カード登録承認処理が完了していれば「掲載中」(publishing)に、
          未了ならば「カード登録待ち」(waiting_card)へ変更。
          without_cardカラムには0を格納
        */
    public function allowListingOfferAction() {

        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {

                $offer_id = $request->getQuery('offer_id');

                $this->useModel('ListingOffer');
                $offer = $this->ListingOffer->findListingOfferById($offer_id);
                $date = new DateTime();
                $today = $date->format('Y-m-d H:i:s');
                $offer->allowed = $today;
                $offer->without_card = 0;
                $this->useModel('Account');
                $account = $this->Account->findAccountById($offer->account_id);

                $this->useModel('ListingKeyword');
                $this->useModel('AdminMailer');
                if($account->card_status == "valid") {
                    $offer->status = "publishing";
                    $this->ListingKeyword->publishKeywords($offer->id);
                    $is_waiting_card = '0';
                } else {
                    $offer->status = "waiting_card";
                    $this->ListingKeyword->waitKeywords($offer->id);
                    $is_waiting_card = '1';
                }
                $offer->save();
                header('Content-Type:application/json');
                echo json_encode($offer->id.','.$is_waiting_card);
            }
        }
    }
            /**
        * ajax通信先
          レクタングル広告通常承認処理
          該当する審査待ちのレクタングル広告のステータスを、カード登録承認処理が完了していれば「掲載中」(publishing)へ、
          未了ならば「カード登録待ち」(waiting_card)へ変更。
          without_cardカラムには0を格納
        */
    public function allowRectangleOfferAction() {

        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {

                $offer_id = $request->getQuery('offer_id');

                $this->useModel('RectangleOffer');
                $offer = $this->RectangleOffer->findRectangleOfferById($offer_id);
                $date = new DateTime();
                $today = $date->format('Y-m-d H:i:s');
                $offer->allowed = $today;
                $offer->without_card = 0;
                $this->useModel('Account');
                $account = $this->Account->findAccountById($offer->account_id);

                $this->useModel('AdminMailer');
                if($account->card_status == "valid") {
                    $offer->status = "publishing";
                    $is_waiting_card = '0';
                } else {
                    $offer->status = "waiting_card";
                    $is_waiting_card = '1';
                }
                $offer->save();
                header('Content-Type:application/json');
                echo json_encode($offer->id.','.$is_waiting_card);
            }
        }
    }
            /**
        * ajax通信先
          ピックアップ広告通常承認処理
          該当する審査待ちのピックアップ広告のステータスを、カード登録承認処理が完了していれば「掲載中」(publishing)に、
          未了ならば「カード登録待ち」(waiting_card)へ変更。
          without_cardカラムには0を格納
        */
    public function allowPickupOfferAction() {

        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {

                $offer_id = $request->getQuery('offer_id');

                $this->useModel('PickupOffer');
                $offer = $this->PickupOffer->findPickupOfferById($offer_id);
                /*
                $from = str_replace('/', '', $offer->published_from);
                $to = str_replace('/', '', $offer->published_to);
                $today = date('Ymd');
                
                if($today < $from) {
                    $offer->status = 'will_publish';
                } else {
                    if($today > $to) {
                        $offer->status = 'completed';
                    } else {
                        $offer->status = 'publishing';
                    }
                }
                */

                $date = new DateTime();
                $today = $date->format('Y-m-d H:i:s');
                
                $offer->allowed = $today;
                $display_area = $offer->display_area;
                $has_published =  $offer->has_published;
                if ($has_published == 0) {
                $offer->published_from = $this->PickupOffer->calPublishedFrom();
                 }
                $offer->published_to = $this->PickupOffer->calPublishedTo($offer->published_from, $offer->publish_week);
                $offer->without_card = 0;
                $this->useModel('Account');
                $account = $this->Account->findAccountById($offer->account_id);

                $this->useModel('AdminMailer');
                if ($account->card_status == "valid") {
                    $offer->status = "publishing";
                //pickup_paymentsへの、課金レコード作成処理(月次請求の際に参照)
                $pickup_payment = new PickupPayment;
                $pickup_payment->calPayment($offer);
                    $is_waiting_card = '0';
                    $offer->has_published = 1;
                } else {
                    $offer->status = "waiting_card";
                    $is_waiting_card = '1';
                    
                }
                
                
                $offer->save();
                header('Content-Type:application/json');
                echo json_encode($offer->id.','.$is_waiting_card);
            }
        }
    }
         /**
        * ajax通信先
          無料広告通常承認処理
          該当する審査待ちの無料広告のステータスを、「掲載中」(publishing)に変更
        */
    public function allowFreeOfferAction() {

        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {

                $offer_id = $request->getQuery('offer_id');

                $this->useModel('FreeOffer');
                $offer = $this->FreeOffer->findFreeOfferById($offer_id);
                $date = new DateTime();
                $today = $date->format('Y-m-d H:i:s');
                $offer->allowed = $today;
                $offer->status = "publishing";
                

              
                $this->useModel('AdminMailer');
                
                $offer->save();
                header('Content-Type:application/json');
                echo json_encode($offer->id);
            }
        }
    }      
        /**
        *  ajax通信先
        *　広告承認をした際に、広告の掲載が開始した旨、またはカード登録を要する旨のメールを
           出稿者に送信
          
        */
    public function sendAllowOfferMessageAction() {
       
        $request = new Phalcon\Http\Request;

        if ($request->isPost() == true) {
            if ($request->isAjax() == true) {

                $offer_id = $request->getPost('offer_id');
                $offer_type = $request->getPost('offer_type');
                $is_waiting_card = $request->getPost('is_waiting_card');

                $this->useModel('AdminMailer');
                echo $offer_type;
                if ($is_waiting_card == '0' or $offer_type == 'free') {
                    echo $this->AdminMailer->informPublishingMail($offer_id, $offer_type);
              
                } elseif($is_waiting_card == '1') {
                    echo $this->AdminMailer->informWaitingCreditMail($offer_id, $offer_type);
                } 
//                  else {
//                    echo $this->AdminMailer->informWaitingPublishingMail($offer_id, $offer_type);
//                }
            }
        }
    }
            /**
        * ajax通信先
          リスティング広告のカードなし承認処理
          該当する審査待ちのリスティング広告のステータスを、掲載中」(publishing)に変更。
          without_cardカラムには1を格納
        */
    public function woAllowListingOfferAction() {

        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {

                $offer_id = $request->getQuery('offer_id');

                $this->useModel('ListingOffer');
                $offer = $this->ListingOffer->findListingOfferById($offer_id);
                $date = new DateTime();
                $today = $date->format('Y-m-d H:i:s');
                $offer->allowed = $today;

                $this->useModel('AdminMailer');
                $offer->status = "publishing";
                $offer->without_card = "1";
                $this->useModel('ListingKeyword');
                $this->ListingKeyword->publishKeywords($offer->id);
                $is_waiting_card = 0;

                $offer->save();
                header('Content-Type:application/json');
                echo json_encode($offer->id.','.$is_waiting_card);
            }
        }
    }   
    
            /**
        * ajax通信先
          レクタングル広告のカードなし承認処理
          該当する審査待ちのレクタングル広告のステータスを、掲載中」(publishing)に変更。
          without_cardカラムには1を格納
        */

    public function woAllowRectangleOfferAction() {

        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {

                $offer_id = $request->getQuery('offer_id');

                $this->useModel('RectangleOffer');
                $offer = $this->RectangleOffer->findRectangleOfferById($offer_id);
                $date = new DateTime();
                $today = $date->format('Y-m-d H:i:s');
                $offer->allowed = $today;

                $this->useModel('AdminMailer');
                $offer->status = "publishing";
                $offer->without_card = "1";
                $is_waiting_card = '0';

                $offer->save();
                header('Content-Type:application/json');
                echo json_encode($offer->id.','.$is_waiting_card);
            }
        }
    }
            /**
        * ajax通信先
          ピックアップ広告のカードなし承認処理
          該当する審査待ちのピックアップ広告のステータスを、掲載中」(publishing)に変更。
          without_cardカラムには1を格納
        */
    public function woAllowPickupOfferAction() {

        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {

                $offer_id = $request->getQuery('offer_id');

                $this->useModel('PickupOffer');
                $offer = $this->PickupOffer->findPickupOfferById($offer_id);
                $date = new DateTime();
                $today = $date->format('Y-m-d H:i:s');
                $offer->allowed = $today;
                $display_area = $offer->display_area;

                $has_published =  $offer->has_published;
                if ($has_published == 0) {
                $offer->published_from = $this->PickupOffer->calPublishedFrom();
                 }
                $offer->published_to = $this->PickupOffer->calPublishedTo($offer->published_from, $offer->publish_week);
                $this->useModel('AdminMailer');
        
                    $offer->status = "publishing";
                    $offer->without_card = "1";
                    $is_waiting_card = '0';
                    $offer->has_published = 1;

                $offer->save();
                header('Content-Type:application/json');
                echo json_encode($offer->id.','.$is_waiting_card);
            }
        }
    }
           /**
        * ajax通信先
          リスティング広告非承認処理
          該当する審査待ちのリスティング広告のステータスを、非承認(not_allowed)に変更
        */
    public function notAllowListingOfferAction() {

        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
                $this->useModel('ListingOffer');
                $offer = $this->ListingOffer->findListingOfferById($request->getQuery('offer_id'));
                $offer->status = "not_allowed";
                $date = new DateTime();
                $today = $date->format('Y-m-d H:i:s');
                $offer->allowed = $today;
                $offer->updateColumn();
                header('Content-Type:application/json');
                echo json_encode($offer->id);
            }
        }
    }
          /**
        * ajax通信先
          レクタングル広告非承認処理
          該当する審査待ちのレクタングル広告のステータスを、非承認(not_allowed)に変更
        */
    public function notAllowRectangleOfferAction() {

        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
                $this->useModel('RectangleOffer');
                $offer = $this->RectangleOffer->findRectangleOfferById($request->getQuery('offer_id'));
                $offer->status = "not_allowed";
                $date = new DateTime();
                $today = $date->format('Y-m-d H:i:s');
                $offer->allowed = $today;
                $offer->updateColumn();
                header('Content-Type:application/json');
                echo json_encode($offer->id);
            }
        }
    }
          /**
        * ajax通信先
          ピックアップ広告非承認処理
          該当する審査待ちのピックアップ広告のステータスを、非承認(not_allowed)に変更
        */
    public function notAllowPickupOfferAction() {

        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
                $this->useModel('PickupOffer');
                $offer = $this->PickupOffer->findPickupOfferById($request->getQuery('offer_id'));
                $offer->status = "not_allowed";
                $date = new DateTime();
                $today = $date->format('Y-m-d H:i:s');
                $offer->allowed = $today;
                $offer->updateColumn();
                header('Content-Type:application/json');
                echo json_encode($offer->id);
            }
        }
    }
          /**
        * ajax通信先
          無料広告非承認処理
          該当する審査待ちの無料広告のステータスを、非承認(not_allowed)に変更
        */
    public function notAllowFreeOfferAction() {

        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
                $this->useModel('FreeOffer');
                $offer = $this->FreeOffer->findFreeOfferById($request->getQuery('offer_id'));
                $offer->status = "not_allowed";
                $date = new DateTime();
                $today = $date->format('Y-m-d H:i:s');
                $offer->allowed = $today;
                $offer->updateColumn();
                header('Content-Type:application/json');
                echo json_encode($offer->id);
            }
        }
    }   
          /**
        * 広告非承認理由選択画面
          入力した非承認理由、及び、getで受け取ったアカウントid,広告id,広告種類を、
          getでadmin/sendMessageに送る
        */
    public function rejectMessageAction() {

        $this->assets
                ->addJs('admin_scripts/notAllowOffer.js');

        $request = new Phalcon\Http\Request;

        if ($request->isGet() == true) {
            $account_id = $request->getQuery('account_id');
            $offer_id = $request->getQuery('offer_id');
            $offer_type = $request->getQuery('offer_type');
        } else {
            $account_id = '0';
            $offer_id = '0';
            $offer_type = '0';
        }
        $this->view->account_id = $account_id;
        $this->view->offer_id = $offer_id;
        $this->view->offer_type = $offer_type;
    }
         /**
        * admin/rejectMessageからgetで受け取った、アカウントid, 広告id、広告種類、非承認理由に基づき、
          広告が非承認となった旨のメールを出向者側へ送信
          及び、非承認理由をDB(unauthorized_offersテーブル)に保存
          admin/sentMessageへredirect
        */
    public function sendMessageAction() {

        $request = new Phalcon\Http\Request;

        if ($request->isGet() == true) {
            $account_id = $request->getQuery('account_id');
            $offer_id = $request->getQuery('offer_id');
            $offer_type = $request->getQuery('offer_type');
            $reason01 = $request->getQuery('reason01');
            $reason02 = $request->getQuery('reason02');
            $reason03 = $request->getQuery('reason03');
            $reason04 = $request->getQuery('reason04');
            $reason05 = $request->getQuery('reason05');
            $reason06 = $request->getQuery('reason06');
            $comment = $request->getQuery('comment');

            $reasons = [
                'account_id' => $account_id,
                'offer_id' => $offer_id,
                'offer_type' => $offer_type,
                'reason01' => $reason01,
                'reason02' => $reason02,
                'reason03' => $reason03,
                'reason04' => $reason04,
                'reason05' => $reason05,
                'reason06' => $reason06,
                'comment' => $comment
            ];

            $this->useModel('UnauthorizedOffer');
            $this->UnauthorizedOffer->resisterUnauthorizedReasons($reasons);
            $this->UnauthorizedOffer->sendUnauthorizedMail($offer_id, $offer_type, $reasons);

            $this->response->redirect('admin/sentMessage');
        }
    }

    public function sentMessageAction() {
        //viewのみ
    }
            /**
        * 承認済みの広告を、承認日付降順に表示
        */
    public function allowedOffersAction() {

        $this->assets
                ->addJs('admin_scripts/tabControl.js')
                ->addJs('admin_scripts/allowedOffers.js')
                ->addJs('admin_scripts/changeScope.js');

        /* Listing Offers */
        $scope = $this->calScope("today");
        $this->useModel('ListingOffer');
        //$listing_offers = $this->ListingOffer->findListingOffers($scope, 'all');
        $listing_offers = $this->ListingOffer->findAllowedListingOffers($scope, 'all');
        $listing_expense_sum = $this->calExpenseSum($listing_offers);
        $listing_cardless_expense_sum = $this->calCardlessExpenseSum($listing_offers);
        $this->view->listing_count = count($listing_offers);
        $this->view->listing_offers = $listing_offers;
        $this->view->listing_expense_sum = $listing_expense_sum;
        $this->view->listing_cardless_expense_sum = $listing_cardless_expense_sum;

        /* Rectangle Offers */
        $this->useModel('RectangleOffer');
        //$rectangle_offers = $this->RectangleOffer->findRectangleOffers($scope, 'all');
        $rectangle_offers = $this->RectangleOffer->findAllowedRectangleOffers($scope, 'all');
        $rectangle_expense_sum = $this->calExpenseSum($rectangle_offers);
        $rectangle_cardless_expense_sum = $this->calCardlessExpenseSum($rectangle_offers);
        $this->view->rectangle_count = count($rectangle_offers);
        $this->view->rectangle_offers = $rectangle_offers;
        $this->view->rectangle_expense_sum = $rectangle_expense_sum;
        $this->view->rectangle_cardless_expense_sum = $rectangle_cardless_expense_sum;

        /* Pickup Offers */
        $this->useModel('PickupOffer');
        //$pickup_offers = $this->PickupOffer->findPickupOffers($scope, 'all');
        $pickup_offers = $this->PickupOffer->findAllowedPickupOffers($scope, 'all');
        $pickup_expense_sum = $this->calExpenseSum($pickup_offers);
        $pickup_cardless_expense_sum = $this->calCardlessExpenseSum($pickup_offers);
        $this->view->pickup_count = count($pickup_offers);
        $this->view->pickup_offers = $pickup_offers;
        $this->view->pickup_expense_sum = $pickup_expense_sum;
        $this->view->pickup_cardless_expense_sum = $pickup_cardless_expense_sum;
        
        $this->useModel('FreeOffer');
        $free_offers = $this->FreeOffer->findAllowedFreeOffers($scope, 'all');
        
        $this->view->free_count = count($free_offers);
        $this->view->free_offers = $free_offers;
        $today = date('Y-m-d H:i:s');
        $six_month_ago = date('Y-m-d H:i:s', strtotime("$today - 6 month"));
        $this->view->six_month_ago = $six_month_ago;

        /* Date */
        $this->view->today = date("Y/m/d");
        $this->view->one_week_ago = date("Y/m/d", strtotime("-6 day"));
    }
            /**
        * ajax通信先
          admin/allowedOffersにおいてリスティング広告を検索した際、検索結果を表示
        */
    public function listingOfferSearchAction() {

        $request = new Phalcon\Http\Request;

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
                $term = $request->getQuery('term');
                /* Listing Offers */
                $scope = $this->calScope("today");
                $this->useModel('ListingOffer');
                $listing_offers = $this->ListingOffer->findListingOffersForSearch($scope, $term);
                $listing_expense_sum = $this->calExpenseSum($listing_offers);
                $this->view->listing_count = count($listing_offers);
                $this->view->listing_offers = $listing_offers;
                $this->view->listing_expense_sum = $listing_expense_sum;
            }
        }
    }
            /**
        * ajax通信先
          admin/allowedOffersにおいてレクタングル広告を検索した際、検索結果を表示
        */
    public function rectangleOfferSearchAction() {

        $request = new Phalcon\Http\Request;

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
                $term = $request->getQuery('term');
                /* Rectangle Offers */
                $scope = $this->calScope("today");
                $this->useModel('RectangleOffer');
                $rectangle_offers = $this->RectangleOffer->findRectangleOffersForSearch($scope, $term);
                $rectangle_expense_sum = $this->calExpenseSum($rectangle_offers);
                $this->view->rectangle_count = count($rectangle_offers);
                $this->view->rectangle_offers = $rectangle_offers;
                $this->view->rectangle_expense_sum = $rectangle_expense_sum;
            }
        }
    }
            /**
        * ajax通信先
          admin/allowedOffersにおいてピックアップ広告を検索した際、検索結果を表示
        */
    public function pickupOfferSearchAction() {

        $request = new Phalcon\Http\Request;

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
                $term = $request->getQuery('term');
                /* Pickup Offers */
                $this->useModel('PickupOffer');
                $pickup_offers = $this->PickupOffer->findPickupOffersForSearch($scope, $term);
                $pickup_expense_sum = $this->calExpenseSum($pickup_offers);
                $this->view->pickup_count = count($pickup_offers);
                $this->view->pickup_offers = $pickup_offers;
                $this->view->pickup_expense_sum = $pickup_expense_sum;
            }
        }
    }
            /**
        * ajax通信先
          admin/allowedOffersにおいて無料広告を検索した際、検索結果を表示
        */
    public function freeOfferSearchAction() {

        $request = new Phalcon\Http\Request;

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
                $term = $request->getQuery('term');
                /* Free Offers */
                $scope = $this->calScope("today");
                $this->useModel('FreeOffer');
                $free_offers = $this->FreeOffer->findFreeOffersForSearch($scope, $term);
                $this->view->free_offers = $free_offers;
                $this->view->free_count = count($free_offers);
                
            }
        }
    }   
          /**
        * admin/accountListsにおいてアカウント一覧を表示
        */
    public function accountListsAction() {

        $this->assets
                ->addJs('admin_scripts/accountLists.js');

        /* Account Data */
        $this->useModel('Account');
        $accounts = $this->Account->getAllAccountsForAdmin();

        $this->view->account_count = count($accounts);
        $this->view->accounts = $accounts;
    }
        
            /**
        * ajax通信先
          admin/accountListsにおいてアカウントを検索した際、検索結果を表示
        */
    public function accountSearchAction() {

        $request = new Phalcon\Http\Request;

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
                $term = $request->getQuery('term');
                /* Account Data */
                $this->useModel('Account');
                $accounts = $this->Account->getSearchResults($term);
                $this->view->account_count = count($accounts);
                $this->view->accounts = $accounts;
                $this->view->pick('/admin/accountLists');
            }
        }
    }
          /**
        * admin/accountListsのアカウント一覧において虫眼鏡マークをクリックした際に、
          アカウントの具体的中身を表示
        */
    public function accountListsContentAction() {

        $this->assets
                ->addJs('admin_scripts/changeScope.js')
                ->addJs('admin_scripts/accountListsChangeMonth.js');


        $request = new Phalcon\Http\Request;

        if ($request->isGet() == true) {
            $account_id = $request->getQuery('account_id');
        } else {
            $account_id = '0';
        }

        $this->useModel('Account');
        $account = $this->Account->findAccountById($account_id);
        $this->view->account = $account;

        /* Listing Offers */
        $scope = $this->calScope("today");
        $this->useModel('ListingOffer');
        $listing_offers = $this->ListingOffer->findListingOffers($scope, $account_id);
        $listing_expense_sum = $this->calExpenseSum($listing_offers);
        $listing_cardless_expense_sum = $this->calCardlessExpenseSum($listing_offers);
        $this->view->listing_count = count($listing_offers);
        $this->view->listing_offers = $listing_offers;
        $this->view->listing_expense_sum = $listing_expense_sum;
        $this->view->listing_cardless_expense_sum = $listing_cardless_expense_sum;

        /* Rectangle Offers */
        $this->useModel('RectangleOffer');
        $rectangle_offers = $this->RectangleOffer->findRectangleOffers($scope, $account_id);
        $rectangle_expense_sum = $this->calExpenseSum($rectangle_offers);
        $rectangle_cardless_expense_sum = $this->calCardlessExpenseSum($rectangle_offers);
        $this->view->rectangle_count = count($rectangle_offers);
        $this->view->rectangle_offers = $rectangle_offers;
        $this->view->rectangle_expense_sum = $rectangle_expense_sum;
        $this->view->rectangle_cardless_expense_sum = $rectangle_cardless_expense_sum;
        /* Pickup Offers */
        $this->useModel('PickupOffer');
        $pickup_offers = $this->PickupOffer->findPickupOffers($scope, $account_id);
        $pickup_expense_sum = $this->calExpenseSum($pickup_offers);
        $pickup_cardless_expense_sum = $this->calCardlessExpenseSum($pickup_offers);
        $this->view->pickup_count = count($pickup_offers);
        $this->view->pickup_offers = $pickup_offers;
        $this->view->pickup_expense_sum = $pickup_expense_sum;
        $this->view->pickup_cardless_expense_sum = $pickup_cardless_expense_sum;
        
        $this->useModel('FreeOffer');
        $free_offers = $this->FreeOffer->findFreeOffers($scope, $account_id);
 
     
        $this->view->free_offers = $free_offers;
        $this->view->free_count = count($free_offers);
        $today = date('Y-m-d H:i:s');
        $six_month_ago = date('Y-m-d H:i:s', strtotime("$today - 6 month"));
        $this->view->six_month_ago = $six_month_ago;
        
        /* Date */
        $this->view->today = date("Y/m/d");
        $this->view->one_week_ago = date("Y/m/d", strtotime("-6 day"));

        /* credit */
        $credit_scope = $this->changeMonth(-1);

        //$charge_to = $account->company_name;

        $this->useModel('ListingOffer');
        $listing_offers = $this->ListingOffer->findListingOffersForCharge($credit_scope, $account->id);
        $listing_expense_sum = $this->calExpenseSum($listing_offers);

        $this->useModel('RectangleOffer');
        $rectangle_offers = $this->RectangleOffer->findRectangleOffersForCharge($credit_scope, $account->id);
        $rectangle_expense_sum = $this->calExpenseSum($rectangle_offers);

        $year = $credit_scope['year'];
        $month = $credit_scope['month'];

        $this->useModel('PickupOffer');
        $pickup_offers = $this->PickupOffer->findPickupOffersForCharge($account->id, $credit_scope);
        $pickup_expense_sum = $this->calExpenseSum($pickup_offers);

        $offers = array_merge((array) $listing_offers, (array) $rectangle_offers, (array) $pickup_offers);
        $expense_sum = $listing_expense_sum + $rectangle_expense_sum + $pickup_expense_sum;
        $tax = floor( $expense_sum * 0.08 );

        //$offers = array_merge($offers, $each_offers);
        //$expense_sum += $each_expense_sum;

        $months = $this->calHisMonths($account_id);
        $this->view->months = $months;

        foreach ($offers as $key => $offer) {
            $offers[$key]->charge_to = $account->company_name;
        }
        
        $this->view->account_id = $account->id;
        $this->view->offers = $offers;
        $this->view->expense_sum = $expense_sum;
        $this->view->tax = $tax;

        $this->view->pay_month = $credit_scope['pay_month'];
        $this->view->pay_year = $credit_scope['pay_year'];
    }
            /**
        * ajax通信先
          admin/allowedOffers, admin/accountListsContentにおいて、それぞれの広告について対象期間を変更して「適用」を押した場合に、
          しかるべきデータを表示
        */
    public function accountListsChangeMonthAction() {

        $request = new Phalcon\Http\Request;

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
             
                $account_id = $request->getQuery('account_id');

                $this->useModel('Account');
                $account = $this->Account->findAccountById($account_id);
                $this->view->account = $account;
                
                /* Date */
                $this->view->today = date("Y/m/d");
                $this->view->one_week_ago = date("Y/m/d", strtotime("-6 day"));

                /* credit */
                $credit_scope = $this->changeMonth($request->getQuery('ago'));

                //$charge_to = $account->company_name;

                $this->useModel('ListingOffer');
                $listing_offers = $this->ListingOffer->findListingOffersForCharge($credit_scope, $account_id);
                $listing_expense_sum = $this->calExpenseSum($listing_offers);

                $this->useModel('RectangleOffer');
                $rectangle_offers = $this->RectangleOffer->findRectangleOffersForCharge($credit_scope, $account_id);
                $rectangle_expense_sum = $this->calExpenseSum($rectangle_offers);

                $year = $credit_scope['year'];
                $month = $credit_scope['month'];

                $this->useModel('PickupOffer');
                $pickup_offers = $this->PickupOffer->findPickupOffersForCharge($account_id, $credit_scope);
                $pickup_expense_sum = $this->calExpenseSum($pickup_offers);

                $offers = array_merge((array) $listing_offers, (array) $rectangle_offers, (array) $pickup_offers);
                $expense_sum = $listing_expense_sum + $rectangle_expense_sum + $pickup_expense_sum;
                $tax = floor( $expense_sum * 0.08 );

                //$offers = array_merge($offers, $each_offers);
                //$expense_sum += $each_expense_sum;

                $months = $this->calHisMonths($account_id);
                $this->view->months = $months;

                foreach ($offers as $key => $offer) {
                    $offers[$key]->charge_to = $account->company_name;
                }
                
                $this->view->offers = $offers;
                $this->view->expense_sum = $expense_sum;
                $this->view->tax = $tax;

                $this->view->pay_month = $credit_scope['pay_month'];
                $this->view->pay_year = $credit_scope['pay_year'];

                $this->view->start();
                $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
                $this->view->render('admin', 'creditcardInfo');
                $this->view->finish();

                $offers_data = $this->view->getContent();

                header('Content-Type:application/json');
                echo json_encode($offers_data);  

            }
        } else {
          $account_id = '0';
        }
    }

//    public function listingKeywordsAction() {
//
//        $this->assets
//          ->addJs('admin_scripts/changeScope.js');
//
//        $request = new \Phalcon\Http\Request;
//
//        if ($request->isGet() == true) {
//
//            //$account_id = $request->getQuery('account_id');
//            $offer_id = $request->getQuery('offer_id');
//
//            $this->useModel('ListingKeyword');
//            $keywords_values = $this->ListingKeyword->findKeywordsById('all', $offer_id);
//            //$offer_job_category = $keywords_data['offer_job_category'];
//            //$keywords_values = $keywords_data['parameters'];
//            $scope = $this->calScope("seven_days");
//            $keywords_variables = $this->ListingKeyword->calVariables($keywords_values, 'listing', $scope, true);
//            $keywords = $this->mergeValuesAndVariables($keywords_values, $keywords_variables);
//            $expense_sum = $this->calExpenseSum($keywords);
//
//            //$this->view->job_category = $offer_job_category;
//            $this->view->offer_id = $offer_id;
//            $this->view->keywords_count = count($keywords);
//            $this->view->keywords = $keywords;
//            $this->view->expense_sum = $expense_sum;
//            //$this->view->one_week_ago = date("Y/m/d", strtotime("-6 day"));
//            $days = [];
//            for($i=0; $i < 60; ++$i) {
//                array_push($days, str_replace("-", "/", date("Y-m-d", strtotime("-".$i." day"))));
//            }
//            $this->view->days = $days;
//            $this->view->today = date("Y/m/d");
//        }
//    }
            /**
        * admin/accountListsContentのリスティング広告表示欄において、検索ワードの虫眼鏡マークを押下した際、
          検索ワードの中身を表示
        */
    public function accountListingKeywordsAction() {

        $this->assets
                ->addJs('admin_scripts/changeScope.js');

        $request = new \Phalcon\Http\Request;

        if ($request->isGet() == true) {
            if ($this->basic->id == $request->getQuery('account_id') || $this->basic->is_admin == 1) {

                $scope = $this->calScope("today");

                $offer_id = $request->getQuery('offer_id');
                $this->useModel('ListingKeyword');
                $offer_job_category = $request->getQuery('offer_job_category');
                
                $keywords = $this->ListingKeyword->findKeywordsById($scope, $offer_id);
                $expense_sum = 0;
                foreach($keywords as $keyword) {
                    $expense_sum += $keyword->expense;
                }

                $this->view->job_category = $offer_job_category;
                $this->view->offer_id = $offer_id;
                $this->view->account_id = $request->getQuery('account_id');
                $this->view->keywords_count = count($keywords);
                $this->view->keywords = $keywords;
                $this->view->expense_sum = $expense_sum;
                $this->view->one_week_ago = date("Y/m/d", strtotime("-6 day"));
                $this->view->today = date("Y/m/d");
            } else {

                $this->response->redirect('admin/index');
            }
        }

        /* Date */
        $days = [];
        for ($i = 0; $i < 60; ++$i) {
            array_push($days, str_replace("-", "/", date("Y-m-d", strtotime("-" . $i . " day"))));
        }
        $this->view->days = $days;
        $this->view->today = date("Y/m/d");
    }
            /**
        * admin/allowedOffers(審査済み広告一覧)のリスティング広告表示欄において、検索ワードの虫眼鏡マークを押下した際、
          検索ワードの中身を表示
        */
    public function allowedListingKeywordsAction() {

        $this->assets
                ->addJs('admin_scripts/changeScope.js');

        $request = new \Phalcon\Http\Request;

        if ($request->isGet() == true) {
            if ($this->basic->id == $request->getQuery('account_id') || $this->basic->is_admin == 1) {

                $scope = $this->calScope("today");

                $offer_id = $request->getQuery('offer_id');
                $this->useModel('ListingOffer');
                $offer = $this->ListingOffer->findListingOfferById($offer_id);
                $without_card = $offer->without_card;
                $this->useModel('ListingKeyword');
                $offer_job_category = $request->getQuery('offer_job_category');
                $keywords = $this->ListingKeyword->findKeywordsById($scope, $offer_id);
                $expense_sum = 0;
                foreach($keywords as $keyword) {
                    $expense_sum += $keyword->expense;
                }
                $this->view->without_card = $without_card;
                $this->view->job_category = $offer_job_category;
                $this->view->offer_id = $offer_id;
                $this->view->account_id = $request->getQuery('account_id');
                $this->view->keywords_count = count($keywords);
                $this->view->keywords = $keywords;
                $this->view->expense_sum = $expense_sum;
                $this->view->one_week_ago = date("Y/m/d", strtotime("-6 day"));
                $this->view->today = date("Y/m/d");
            } else {

                $this->response->redirect('admin/index');
            }
        }

        /* Date */
        $days = [];
        for ($i = 0; $i < 60; ++$i) {
            array_push($days, str_replace("-", "/", date("Y-m-d", strtotime("-" . $i . " day"))));
        }
        $this->view->days = $days;
        $this->view->today = date("Y/m/d");
    }
            /**
        * クレジットカード決済情報の表示
        */
    public function creditAction() {

        $this->assets->addJs('admin_scripts/creditcardChangeMonth.js');
        $this->assets->addJs('admin_scripts/credit.js');

        /* Account Data */
        $this->useModel('Account');
        $accounts = $this->Account->getAllAccountsForAdmin(true);
        $account_count = count($accounts);

        if ($account_count > 0) {

            $scope = $this->changeMonth(-1);

            $offers = [];
            $expense_sum = 0;

            foreach ($accounts as $account) {

                $charge_to = $account->company_name;

                $this->useModel('ListingOffer');
                $listing_offers = $this->ListingOffer->findListingOffersForCharge($scope, $account->id);
                $listing_expense_sum = $this->calExpenseSum($listing_offers);

                $this->useModel('RectangleOffer');
                $rectangle_offers = $this->RectangleOffer->findRectangleOffersForCharge($scope, $account->id);
                $rectangle_expense_sum = $this->calExpenseSum($rectangle_offers);

                $year = $scope['year'];
                $month = $scope['month'];

                $this->useModel('PickupOffer');
                $pickup_offers = $this->PickupOffer->findPickupOffersForCharge($account->id, $scope);
                $pickup_expense_sum = $this->calExpenseSum($pickup_offers); 

                $each_offers = array_merge((array) $listing_offers, (array) $rectangle_offers, (array) $pickup_offers);
                foreach ($each_offers as $each_offer) {
                            $each_offer->charge_to = $charge_to;
                        }
                $each_expense_sum = $listing_expense_sum + $rectangle_expense_sum + $pickup_expense_sum;

                $offers = array_merge($offers, $each_offers);
                $expense_sum += $each_expense_sum;
            }
            
            $tax = floor( $expense_sum * 0.08 );

            $months = $this->calMyMonths();
            $this->view->months = $months;

            $this->view->offers = $offers;
            $this->view->expense_sum = $expense_sum;
            $this->view->tax = $tax;

            $this->view->pay_month = $scope['pay_month'];
            $this->view->pay_year = $scope['pay_year'];
        }
    }
            /**
        * ajax通信先
          admin/credit(クレジットカード決済情報表示画面)において、対象期間を変更して「適用」を押した場合に、
          しかるべきデータを表示
        */
    public function creditChangeMonthAction() {
      
        $request = new Phalcon\Http\Request;

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {

                $scope = $this->changeMonth($request->getQuery('ago'));
                
                /* Account Data */
                $this->useModel('Account');
                $accounts = $this->Account->getAllAccountsForAdmin(true);
                $account_count = count($accounts);

                if ($account_count > 0) {

                    $offers = [];
                    $expense_sum = 0;

                    foreach ($accounts as $account) {

                        $charge_to = $account->company_name;

                        $this->useModel('ListingOffer');
                        $listing_offers = $this->ListingOffer->findListingOffersForCharge($scope, $account->id);
                        $listing_expense_sum = $this->calExpenseSum($listing_offers);

                        $this->useModel('RectangleOffer');
                        $rectangle_offers = $this->RectangleOffer->findRectangleOffersForCharge($scope, $account->id);
                        $rectangle_expense_sum = $this->calExpenseSum($rectangle_offers);

                        $year = $scope['year'];
                        $month = $scope['month'];

                        $this->useModel('PickupOffer');
                        $pickup_offers = $this->PickupOffer->findPickupOffersForCharge($account->id, $scope);
                        $pickup_expense_sum = $this->calExpenseSum($pickup_offers);

                        $each_offers = array_merge((array) $listing_offers, (array) $rectangle_offers, (array) $pickup_offers);
                        foreach ($each_offers as $each_offer) {
                            $each_offer->charge_to = $charge_to;
                        }
                        $each_expense_sum = $listing_expense_sum + $rectangle_expense_sum + $pickup_expense_sum;
                        
                        $offers = array_merge($offers, $each_offers);
                        $expense_sum += $each_expense_sum;
                    }
                    
                    $tax = floor( $expense_sum * 0.08 );

                    $months = $this->calMonths();
                    $this->view->months = $months;
                    
                    $this->view->offers = $offers;
                    $this->view->expense_sum = $expense_sum;
                    $this->view->tax = $tax;

                    $this->view->pay_month = $scope['pay_month'];
                    $this->view->pay_year = $scope['pay_year'];

                    $this->view->start();
                    $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
                    $this->view->render('admin', 'creditcardInfo');
                    $this->view->finish();

                    $offers_data = $this->view->getContent();

                    header('Content-Type:application/json');
                    echo json_encode($offers_data);  
                }
            }
        }
    }
            /**
        * ajax通信先
          admin/credit(クレジットカード決済情報表示画面)において、アカウント名を検索した場合に、
          検索結果を表示
        */
    public function creditSearchAction() {

        $request = new Phalcon\Http\Request;

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
                $term = $request->getQuery('term');

                /* Account Data */
                $this->useModel('Account');
                $accounts = $this->Account->getSearchResults($term, true);
                $account_count = count($accounts);

                $scope = $this->changeMonth($request->getQuery('ago'));
                
                if ($account_count > 0) {

                    $offers = [];
                    $expense_sum = 0;
                   
                    foreach ($accounts as $account) {

                        $charge_to = $account->company_name;

                        $this->useModel('ListingOffer');
                        $listing_offers = $this->ListingOffer->findListingOffersForCharge($scope, $account->id);
                        $listing_expense_sum = $this->calExpenseSum($listing_offers);

                        $this->useModel('RectangleOffer');
                        $rectangle_offers = $this->RectangleOffer->findRectangleOffersForCharge($scope, $account->id);
                        $rectangle_expense_sum = $this->calExpenseSum($rectangle_offers);

                        $this->useModel('PickupOffer');
                        $pickup_offers = $this->PickupOffer->findPickupOffersForCharge($account->id, $scope);
                        $pickup_expense_sum = $this->calExpenseSum($pickup_offers);

                        $each_offers = array_merge((array)$listing_offers, (array)$rectangle_offers, (array)$pickup_offers);
                        foreach ($each_offers as $each_offer) {
                            $each_offer->charge_to = $charge_to;
                        }
                        $each_expense_sum = $listing_expense_sum + $rectangle_expense_sum + $pickup_expense_sum;

                        $offers = array_merge($offers, $each_offers);
                        $expense_sum += $each_expense_sum;
                    }
                    
                    $tax = floor( $expense_sum * 0.08 );
                    
                    $months = $this->calMonths();
                    $this->view->months = $months;
                    
                    $this->view->offers = $offers;
                    $this->view->expense_sum = $expense_sum;
//                    $this->view->tax = $tax;
                    $this->view->tax = 0;
                    $this->view->pay_month = $scope['pay_month'];
                    $this->view->pay_year = $scope['pay_year'];

                    $this->view->start();
                    $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
                    $this->view->render('admin', 'creditcardInfo');
                    $this->view->finish();

                    $offers_data = $this->view->getContent();

                    header('Content-Type:application/json');
                    echo json_encode($offers_data);

                } else {
                    $months = $this->calMonths();
                    $this->view->months = $months;
                    
                    $this->view->offers = array();
                    $this->view->expense_sum = 0;
                    $this->view->tax = 0;
                    $this->view->pay_month = $scope['pay_month'];
                    $this->view->pay_year = $scope['pay_year'];

                    $this->view->start();
                    $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
                    $this->view->render('admin', 'creditcardInfo');
                    $this->view->finish();

                    $offers_data = $this->view->getContent();

                    header('Content-Type:application/json');
                    echo json_encode($offers_data);
                }
            }
        }
    }

    public function infoAction() {
        echo $_SERVER['SERVER_NAME'];
        echo $_SERVER['SERVER_ADDR'];
        exit;
    }

}
