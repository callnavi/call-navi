<?php

use WebPay\WebPay;

class CreditController extends AdminControllerBase {
        /**
        *  webPay、カード情報入力フォーム
         　入力情報に基づき発行されたトークンを用いてcustomerオブジェクトを作成し、
         　そのcustomerオブジェクトのidをaccountsテーブルのcreditcard_idカラムに格納。
         　クレジットカード登録済みか否か(creditcard_idカラムの値が空か否か)に応じて,
           登録画面と変更画面の表示分けをする。
           
        */
    public function inputAction() {
      
        $this->assets->addJs('admin_scripts/credit/input.js');
        
        
        $webpay = new WebPay("live_secret_dMo0nX6lw0os5eAcDofOg55j");
        $this->useModel('Account');
        $account = $this->Account->findAccountForWebpay($this->basic->id);
        $request = new Phalcon\Http\Request;
        
        if($account->creditcard_id == "") {
          $this->view->is_new = 1;
          $thanks = 'credit/thanks';
        } else {
          $this->view->is_new = 0;
          $thanks = 'credit/modifyThanks';
        }
        
        if($request->isPost() == true) {
            
            $creditcard = $webpay->customer->create(array(
               "card"=> $request->getPost('webpay-token')
            ));
            $account->creditcard_id = $creditcard->id;
            $account->card_status = "before_approval";
            $this->useModel('ListingOffer');
            $this->ListingOffer->stopPublishingOffer($account->id);
            $this->useModel('RectangleOffer');
            $this->RectangleOffer->stopPublishingOffer($account->id);
            $this->useModel('PickupOffer');
            $this->PickupOffer->stopPublishingOffer($account->id);
            $account->updateColumn();
            
            if($account->creditcard_id != "") {
                
//                $this->useModel('ListingOffer');
//                $this->ListingOffer->publishWaitingOffer($account->id);
//                $this->useModel('RectangleOffer');
//                $this->RectangleOffer->publishWaitingOffer($account->id);
//                $this->useModel('PickupOffer');
//                $this->PickupOffer->publishWaitingOffer($account->id);
                
                $this->response->redirect($thanks);
            }       
        }

    }

    public function thanksAction() {  
        //viewのみ
    }
    
    public function modifyThanksAction() {
        //viewのみ
    }
        /**
        *  accountsテーブルのcreditcard_idカラムを空にし、カード情報が登録される
         　前の状態に戻す。
         　
        */
    public function deleteAction() {
        $this->useModel('Account');
        $account = $this->Account->findAccountForWebpay($this->basic->id);
        $account->creditcard_id = "";
        $account->card_status = "before_approval";
        $account->updateColumn();
        
        if($account->creditcard_id == "") {
            $this->useModel('ListingOffer');
            $this->ListingOffer->stopPublishingOffer($account->id);
            $this->useModel('RectangleOffer');
            $this->RectangleOffer->stopPublishingOffer($account->id);
            $this->useModel('PickupOffer');
            $this->PickupOffer->stopPublishingOffer($account->id);
        }      
    }

}               
