<?php

class RectangleController extends AdminControllerBase {

use ImageEditable;

use Mailable;

use ScopeCalculatable;

use VariablesCalculatable;
        /**
        * レクタングル広告作成フォーム入力場面。
         「確認画面へ進む」ボタン押下により、rectangle/makeConfirmへ、postで値を送る
           rectangle/makeConfirmから、「入力内容を修正する」で戻った場合には、postで受け取ったrectangle_offersテーブルのidに基づきDB(rectangle_offersテーブル)に格納されたデータを初期値として保持する事により入力内容が保持される
        */
    public function makeAction() {

        $this->assets->addJs('admin_scripts/rectangle/make.js');
        $this->assets->addCss('admin_css/error_red.css');
        $this->useModel('RectangleOffer');
        $this->view->average_click_price = $this->RectangleOffer->averageClickPrice();

        $request = new \Phalcon\Http\Request();
        //一度確認画面に進んだ後に広告作成フォームに戻った場合
        if ($request->isPost() == true) {

            $offer_id = $request->getPost('offer_id');
            $this->view->data = $this->RectangleOffer->findOnConfirmRectangleOfferById($offer_id);
            $this->view->post = 1;
        //新規に広告作成フォームを開いた場合
        } else {
            $this->view->post = 0;
        }

        $strDate = 'tomorrow'; //翌日の0時までキャッシュさせる場合
        $dt = new DateTime($strDate);
        header('Cache-Control: max-age=' . ($dt->format('U') - time()));
    }
        /**
        * レクタングル広告作成フォーム確認場面。
           rectangle/makeからpostで入力値を受け取り、入力に内容に応じてデータをDB(rectangle_offersテーブル)に格納、及び確認画面に表示。
           rectangle/makeで添付された画像をpublic/images/rectangkeに、「広告ID.png」という形式で保存及び確認画面に表示
          　データについて、statusカラムはjudging(審査中)、is_confirmedカラム(作成完了か否かを判断)は1となる
           入力内容の保存処理により既にis_confirmedカラムが1であるデータが存在する場合には、データ新規作成処理ではなく
          　データ更新処理となる。この場合、is_confirmedカラムは1のまま。
        */
    public function makeConfirmAction() {

        $request = new \Phalcon\Http\Request();
        if ($request->isPost() == true) {
            $offer_id_set = $request->getPost('offer_id_set');
            //新たにデータ作成となる場合($offer_id_setは-1となっている)
            if ($offer_id_set == "-1") {
                $data = $this->__setRectangleData($request->getPost());
                $data['account_id'] = $this->basic->id;
                $this->useModel('RectangleOffer');
                $this->view->data = $newRectangle = $this->RectangleOffer->addRectangleOffer($data);
            //既にあるデータの更新処理となる場合($offer_id_setは、更新されるべき広告レコードのIDとなっている)   
            } else {
                $data = $this->__setRectangleData($request->getPost());
                $this->useModel('RectangleOffer');
                $data['id'] = intval($offer_id_set);



                $data['account_id'] = $this->basic->id;

                $this->view->data = $newRectangle = $this->RectangleOffer->updateRectangleOffer($data, "editing");
                
            }
            
            $strDate = 'tomorrow'; //翌日の0時までキャッシュさせる場合
            $dt = new DateTime($strDate);
            header('Cache-Control: max-age=' . ($dt->format('U') - time()));



            if ($this->request->hasFiles() == true) {
               
                foreach ($this->request->getUploadedFiles() as $file) {
                    
                    $image = $this->getImage($file->getTempName(), $file->getType());
                    $image = $this->minimizeImage($image, 200);
                    $imageUrl = '/images/rectangle/' . $newRectangle->id . '.png';
                    $toPath = __DIR__ . '/../../../public' . $imageUrl;
                    if (file_exists($toPath)) {
                        unlink($toPath);
                    }
                    $this->locatePngImage($image, $toPath);
                }
            }

        }
              
    }
            /**
        *  レクタングル広告作成完了場面。
           既に存在するデータについて、statusカラムをjudging(審査中)とする。
           また、出向者管理者双方へ無料広告作成完了の旨のメールを送信。
        */
    public function makeThanksAction() {

        $request = new \Phalcon\Http\Request();

        if ($request->isPost() == true) {
            
            $this->useModel('RectangleOffer');
            $offer_id = $request->getPost('offer_id');


            $this->RectangleOffer->confirmRectangleOffer($offer_id);
            $this->RectangleOffer->checkIsAvailable($offer_id);

            $this->RectangleOffer->sendThanksMail($offer_id);
            $this->RectangleOffer->sendThanksAdminMail($offer_id);

            $strDate = 'tomorrow'; //翌日の0時までキャッシュさせる場合
            $dt = new DateTime($strDate);
            header('Cache-Control: max-age=' . ($dt->format('U') - time()));
        }
    }
        
           /**
        * ajax通信先
          レクタングル広告保存場面。(rectangle/makeにおいて、「入力内容を保存する」を押下した場合)
           rectangle/makeからpostで入力値を受け取り、入力内容に応じてDB(rectangle_offersテーブル)にデータを格納(ajax通信)
           rectangle/makeで添付された画像をpublic/images/rectangleに、「広告ID.png」という形式で保存
           statusカラムはediting(編集中)、is_confirmedカラムは1となる。
           入力内容の保存処理が既に一度以上行われており既にis_confirmedカラムが1であるデータが存在する場合には、データ新規作成処理ではなく
          　データ更新処理となる。
        */
    public function saveAction() {

        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        $request = new \Phalcon\Http\Request();

        if ($request->isPost() == true) {

            $offer_id_set = $request->getPost('offer_id_set');
            if ($offer_id_set == "-1") {
                $data = $this->__setRectangleData($request->getPost());
                $data['account_id'] = $this->basic->id;
                $this->useModel('RectangleOffer');
                $newRectangle = $this->RectangleOffer->saveRectangleOffer($data);
            } else {
                $data = $this->__setRectangleData($request->getPost());
                $this->useModel('RectangleOffer');
                $data['id'] = intval($offer_id_set);


                $data['account_id'] = $this->basic->id;

                $this->RectangleOffer->updateRectangleOffer($data, "editing");

                $newRectangle = new stdClass;
                $newRectangle->id = $data['id'];
            }
        }

        if ($this->request->hasFiles() == true) {
            foreach ($this->request->getUploadedFiles() as $file) {
                $image = $this->getImage($file->getTempName(), $file->getType());
                $image = $this->minimizeImage($image, 200);
                $imageUrl = '/images/rectangle/' . $newRectangle->id . '.png';
                $toPath = __DIR__ . '/../../../public' . $imageUrl;
                if (file_exists($toPath)) {
                    unlink($toPath);
                }
                $this->locatePngImage($image, $toPath);
            }
        }

        echo $newRectangle->id;
    }
        /**
        * レクタングル広告編集フォーム入力場面。
          rectangle_offersテーブルのidの値をgetで受け取る事により、該当するデータの値をフォーム上に初期値として表示
         「確認画面へ進む」ボタン押下により、rectangle/editConfirmへ、postで値を送る
           rectangle/editConfirmから、「入力内容を修正する」で戻った場合には、DB(free_offersテーブル)に格納されたデータを初期値として保持する事により入力内容が保持される
        */
    public function editAction() {

        $this->assets->addJs('admin_scripts/rectangle/edit.js');
        $this->assets->addCss('admin_css/error_red.css');

        $request = new \Phalcon\Http\Request();

        if ($request->isGet() == true) {
            if ($this->basic->id == $request->getQuery('account_id') or $this->basic->is_admin == 1) {
                $this->useModel('RectangleOffer');
                $this->view->data = $this->RectangleOffer->findRectangleOfferById($request->getQuery('offer_id'));

                $average_click_price = $this->RectangleOffer->averageClickPrice();

                $this->view->average_click_price = $average_click_price;
            } else {
                $this->response->redirect('customer/index');
            }

            $strDate = 'tomorrow'; //翌日の0時までキャッシュさせる場合
            $dt = new DateTime($strDate);
            header('Cache-Control: max-age=' . ($dt->format('U') - time()));
        }
    }
        /**
        * レクタングル広告作成フォーム確認場面。
           rectangle/editからpostで入力値を受け取り、入力に内容に応じて該当データを更新、及び確認画面に表示。
           rectangle/makeで添付された画像をpublic/images/rectangleに、「広告ID.png」という形式で保存及び確認画面に表示
          　statusカラムはediting(編集中)、is_confirmedカラムは1のまま
        */
    public function editConfirmAction() {

        $request = new \Phalcon\Http\Request();


        if ($request->isPost() == true) {
            $data = $this->__setRectangleData($request->getPost());
            $data['account_id'] = $this->basic->id;
            $data['id'] = $request->getPost('offer_id');
            $this->useModel('RectangleOffer');

            $this->view->data = $this->RectangleOffer->updateRectangleOffer($data, 'editing');

            
            if ($this->request->hasFiles() == true) {
                foreach ($this->request->getUploadedFiles() as $file) {
                    $image = $this->getImage($file->getTempName(), $file->getType());
                    $image = $this->minimizeImage($image, 200);
                    $imageUrl = '/images/rectangle/' . $data['id'] . '.png';
                    $toPath = __DIR__ . '/../../../public' . $imageUrl;
                    if (file_exists($toPath)) {
                        unlink($toPath);
                    }
                    $this->locatePngImage($image, $toPath);
                    $this->view->pictureUrl = $imageUrl;

                    $strDate = 'tomorrow'; //翌日の0時までキャッシュさせる場合
                    $dt = new DateTime($strDate);
                    header('Cache-Control: max-age=' . ($dt->format('U') - time()));
                }
            }
        }
        
    }
            /**
        * レクタングル広告編集完了場面。
          既に存在する該当データについて、statusカラムをediting(編集中)からjudging(審査中)に変更。
           また、出向者管理者双方へ無料広告作成完了の旨のメールを送信。
        */
    public function editThanksAction() {

        $request = new \Phalcon\Http\Request();

        if ($request->isGet() == true) {
            if ($request->getQuery('account_id') or $this->basic->is_admin == 1) {
                $this->useModel('RectangleOffer');
                $offer_id = $request->getQuery('offer_id');
                $offer = $this->RectangleOffer->findRectangleOfferById($offer_id);
                $offer->status = 'judging';
                $offer->is_confirmed = 1;
                $date = new DateTime();
                $today = $date->format('Y-m-d H:i:s');
                $offer->accepted = $today;
                $offer->updateColumn();
                $this->RectangleOffer->sendThanksMail($offer_id);
                $this->RectangleOffer->sendThanksAdminMail($offer_id);


                $strDate = 'tomorrow'; //翌日の0時までキャッシュさせる場合
                $dt = new DateTime($strDate);
                header('Cache-Control: max-age=' . ($dt->format('U') - time()));
            } else {
                $this->response->redirect("customer/index");
            }
        }
    }
        /**
        *  ajax通信先
           レクタングル広告作成フォーム保存場面。
           rectangle/editからpostで入力値を受け取り、入力に内容に応じて該当データを更新(ajax通信)
          　statusカラムはediting(編集中)、is_confirmedカラムは1のまま
        */
    public function editSaveAction() {

        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        $request = new \Phalcon\Http\Request();

        if ($request->isPost() == true) {
            $data = $this->__setRectangleData($request->getPost());
            $data['account_id'] = $this->basic->id;
            $data['id'] = $request->getPost('offer_id');
            $this->useModel('RectangleOffer');
            $newRectangle = $this->RectangleOffer->updateRectangleOffer($data, 'editing');



            if ($this->request->hasFiles() == true) {
                foreach ($this->request->getUploadedFiles() as $file) {
                    $image = $this->getImage($file->getTempName(), $file->getType());
                    $image = $this->minimizeImage($image, 200);
                    $imageUrl = '/images/rectangle/' . $data['id'] . '.png';
                    $toPath = __DIR__ . '/../../../public' . $imageUrl;
                    if (file_exists($toPath)) {
                        unlink($toPath);
                    }
                    $this->locatePngImage($image, $toPath);
                }
            }
        }
    }

    /*
      public function previewAction() {
      $this->assets->addCss('admin_css/error_red.css');
      $request = new \Phalcon\Http\Request();

      if ($request->isGet() == true) {
      if ($this->basic->id == $request->getQuery('account_id')) {
      $this->useModel('RectangleOffer');
      //                $this->view->data = $this->RectangleOffer->convertObjectToArray($request->getQuery('offer_id'));
      $this->view->data = $this->RectangleOffer->findRectangleOfferById($request->getQuery('offer_id'));
      } else {
      $this->response->redirect('customer/index');
      }
      }
      }
     */
        /**
        *  レクタングル広告プレビュー表示画面(管理画面の広告一覧から、プレビューの下の目マークを押下した場合)
          getで受け取ったfree_offersのidに基づき、該当するレクタングル広告のプレビューを表示
        */
    public function previewAction() {
        $this->assets->addCss('admin_css/error_red.css');
        $request = new \Phalcon\Http\Request();

        if ($request->isGet() == true) {

            $this->useModel('RectangleOffer');
//              
            $this->view->data = $this->RectangleOffer->findRectangleOfferById($request->getQuery('offer_id'));
        }
    }
       /**
        *  入力フォームに入力され、postで送られた各々のデータを一つの配列に格納するに際しての共通処理
        */
    private function __setRectangleData($inputData) {

        $outputData = [];

        $fields = ['title', 'has_pic', 'click_price', 'budget_max', 'detail_url'];
        foreach ($fields as $field) {
            if (isset($inputData[$field])) {
                $outputData[$field] = $inputData[$field];
            }
        }
        return $outputData;
    }
        /**
        *  ajax通信先
           customer/indexのレクタングル広告において対象期間を変更して「適用」を押下した場合において、しかるべきデータを表示
        */
    public function changeScopeAction() {

        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
                $rectangle_scope = $this->calScope($request->getQuery('rectangle_scope'));
                $this->useModel('RectangleOffer');
                $offers = $this->RectangleOffer->findRectangleOffers($rectangle_scope, $this->basic->id);
                $expense_sum = $this->calExpenseSum($offers);
                $cardless_expense_sum = $this->calCardlessExpenseSum($offers);
                $data = array("offers" => $offers, "expense_sum" => $expense_sum, "cardless_expense_sum" => $cardless_expense_sum);
                header('Content-Type:application/json');
                echo json_encode($data);
            }
        }
    }
        /**
        *  ajax通信先
           admin/allowedOffersのレクタングル広告において対象期間を変更して「適用」を押下した場合において、しかるべきデータを表示
        */
    public function changeScopeForAdminAction() {

        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);
      
        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
                $rectangle_scope = $this->calScope($request->getQuery('rectangle_scope'));
                $this->useModel('RectangleOffer');
                $offers = $this->RectangleOffer->findRectangleOffers($rectangle_scope, 'all');
                
                $expense_sum = $this->calExpenseSum($offers);
                $cardless_expense_sum = $this->calCardlessExpenseSum($offers);
                $data = array("offers" => $offers, "expense_sum" => $expense_sum, "cardless_expense_sum" => $cardless_expense_sum);
                header('Content-Type:application/json');
                echo json_encode($data);
            }
        }
    }
        /**
        *  ajax通信先
           admin/accountListsContentのレクタングル広告において対象期間を変更して「適用」を押下した場合において、しかるべきデータを表示
        */
    public function changeScopeForAccountsAction() {

        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);
      
        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
                $rectangle_scope = $this->calScope($request->getQuery('rectangle_scope'));
                $account_id = $request->getQuery('account_id');
                $this->useModel('RectangleOffer');
                $offers = $this->RectangleOffer->findRectangleOffers($rectangle_scope, $account_id);
                
                $expense_sum = $this->calExpenseSum($offers);
                $cardless_expense_sum = $this->calCardlessExpenseSum($offers);
                $data = array("offers" => $offers, "expense_sum" => $expense_sum, "cardless_expense_sum" => $cardless_expense_sum);
                header('Content-Type:application/json');
                echo json_encode($data);
            }
        }
    }   
        
        /**
        *  ajax通信先
           customer/indexにおいてレクタングル広告を一時停止した場合、該当するレクタングル広告のstatusカラムをcanceled(一時停止中)に変更
        */
    public function cancelOfferAction() {

        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
                $scope = $this->calScope($request->getQuery('scope'));
                $this->useModel('RectangleOffer');
                $offer = $this->RectangleOffer->findRectangleOfferById($request->getQuery('offer_id'));
                $offer->status = "canceled";
                $offer->updateColumn();
                $offers = $this->RectangleOffer->findRectangleOffers($scope, $this->basic->id);
                $data = array("offer_id" => $offer->id, "offers" => $offers);
                header('Content-Type:application/json');
                echo json_encode($data);
            }
        }
    }

        /**
        *  ajax通信先
           customer/indexにおいて一時停止中のレクタングル広告を有効にした場合、該当するレクタングル広告のstatusカラムをpublishing(掲載中)に変更
        */
    public function effectOfferAction() {

        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
                $scope = $this->calScope($request->getQuery('scope'));
                $this->useModel('RectangleOffer');
                $offer = $this->RectangleOffer->findRectangleOfferById($request->getQuery('offer_id'));
                $offer->status = "publishing";
                $offer->updateColumn();

                $offers = $this->RectangleOffer->findRectangleOffers($scope, $this->basic->id);
                $data = array("offer_id" => $offer->id, "offers" => $offers, "status" => $offer->status);
                header('Content-Type:application/json');
                echo json_encode($data);
            }
        }
    }
        /**
        *  ajax通信先
           customer/indexにおいてレクタングル広告を削除した場合、該当するレクタングル広告のdeletedカラムを1に変更
         　この場合、出稿者ページにおいては表示されなくなり、管理者ページにおいてはステータスが「ユーザー削除」として
         　表示されるようになる。
        */
    public function deleteOfferAction() {

        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
                $scope = $this->calScope($request->getQuery('scope'));
                $this->useModel('RectangleOffer');
                $offer = $this->RectangleOffer->findRectangleOfferById($request->getQuery('offer_id'));
                $offer->softDelete();
                $offers = $this->RectangleOffer->findRectangleOffers($scope, $this->basic->id);
                $count = count($offers);
                $expense_sum = $this->calExpenseSum($offers);
                $cardless_expense_sum = $this->calCardlessExpenseSum($offers);
                $data = array("offer_id" => $offer->id, "offers" => $offers, "count" => $count, "expense_sum" => $expense_sum, "cardless_expense_sum" => $cardless_expense_sum);
                header('Content-Type:application/json');
                echo json_encode($data);
            }
        }
    }
       /**
        *  ajax通信先
         　rectangle/make　及びrectangle/editにおいて、画像登録済みの場合のみ表示される「画像を削除する」ボタンを押下した際に、
         　該当する無料広告のhas_picカラムを1から0へと変更し、その広告データが画像未登録のものとして扱われるようにする
        */
    public function deletePictureAction() {
        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);
        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
                $this->useModel('RectangleOffer');
                $offer_id = $request->getQuery('offer_id');
                $offer = $this->RectangleOffer->findOnConfirmRectangleOfferById($offer_id);
                $offer->has_pic = 0;
                $offer->updateColumn();
            }
        }
    }
        /**
        *  ajax通信先
         　レクタングル広告編集画面において、上限予算が本日既にクリックされて生じている費用を下回らないかをチェック
        */
    public function validateBudgetmaxAction() {
        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);
        $scope = $this->calScope("today");
        if ($request->isGet() == true) {
           if ($request->isAjax() == true) {
           $offer_id = $request->getQuery('offer_id');
           $this->useModel('RectangleOffer');
           $offer = $this->RectangleOffer->findFirstById($offer_id);
           $click_price_number = $request->getQuery('click_price_number');
           $budget_max_number = $request->getQuery('budget_max_number');
           $budget_max = $request->getQuery('budget_max');
           $this->useModel('ClickLog');
           $click_logs = $this->ClickLog->findClickLogs($offer, 'rectangle', $scope, false);
           $expense = $this->calExpense($click_logs);
           echo ($budget_max_number >= $expense + $click_price_number || $budget_max == "") ? 1:0;
               }
         }      
    }

    
}
