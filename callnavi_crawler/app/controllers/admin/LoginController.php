<?php

class LoginController extends AdminControllerBase {
        /**
        * ログインフォーム入力場面
        */
    public function indexAction() {

        $this->assets->addJs('admin_scripts/login/index.js');
        $this->assets->addCss('admin_css/error_red.css');
    }
        /**
        * ログアウトした際、すべてのセッションを破棄する
        */
    public function logoutAction() {

        $this->session->destroy();
    }
        /**
        * ログインフォームに入力されたメールアドレスとパスワードの組み合わせから、
          ログイン可能か否かを判定する。ログイン可能な場合、/login/checkUserにリダイレクト
        */
    public function accountCheckAction() {
        $request = new \Phalcon\Http\Request();
        if ($request->isPost() == true) {
            $email = $request->getPost('email');
            $password = $request->getPost('password');

            $this->useModel('Account');
            $encryptedPassword = $this->Account->encryptPassword($password);
            $account = $this->Account->login($email, $encryptedPassword);

            /**
             * is_login : 0 ログインできない
             * 1 checkIsUserアクションへ
             */
            //echo ($account && $accout->deleted !== 1) ? 1 : 0;

            if (!$account) {
                $is_login = 0;
            } elseif ($account->deleted == 1) {
                $is_login = 0;
            } else {
                $is_login = 1;
            }
            echo $is_login;

            $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        }
    }
        /**
        * ログインしたアカウントが、出稿者のものか管理者のものか(accountsテーブルのis_adminカラムが1か0か)を判定。
         　管理者のものである場合、admin/indexへリダイレクト
         　出稿者のものである場合、作成した広告データが一つでも存在する場合customer/indexへ、
         　　　　　　　　　　　　　　　　　　　　　　　一つも存在しない場合customer/makeへredirect
        */
    public function checkIsUserAction() {

        if ($this->request->isPost()) {
            $email = $this->request->getPost('email');
            $password = $this->request->getPost('password');

            $this->useModel('Account');
            $encryptedPassword = $this->Account->encryptPassword($password);
            $account = $this->Account->login($email, $encryptedPassword);

            $this->_registerSession($account);

            $account_id = $account->id;
            $this->useModel('ListingOffer');
            $this->useModel('RectangleOffer');
            $this->useModel('PickupOffer');
            $this->useModel('FreeOffer');
            $my_listings = $this->ListingOffer->countMyOffers($account_id);
            $my_rectangles = $this->RectangleOffer->countMyOffers($account_id);
            $my_pickups = $this->PickupOffer->countMyOffers($account_id);
            $my_frees = $this->FreeOffer->countMyOffers($account_id);
    

            if ($account->is_admin) {
                return $this->response->redirect('admin/index');
            } elseif ($my_listings == 0 && $my_rectangles == 0 && $my_pickups == 0 && $my_frees ==0 ) {

                return $this->response->redirect('customer/make');
            } else {
                return $this->response->redirect('customer/index');
            }
        }
    }
       /**
        * ログインした場合において、アカウントのIDを、authという名のセッションで保持
        */
    private function _registerSession($account) {
        $this->session->set('auth', array('id' => $account->id));
    }

    /*
      public function checkDuplicateMailAction() {

      $request = new \Phalcon\Http\Request();
      if ($request->isPost() == true) {
      $email = $request->getPost('email');
      $this->useModel('Account');
      $account = $this->Account->findFirstByMail($email);


      if ($this->isLoggedIn()) {
      echo ($account && $account->id !== $this->basic->id) ? 1 : 0;
      } else {
      echo ($account) ? 1 : 0;
      }

      $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
      }

      }
     */
        
        /**
        * ajax通信先
          アカウント新規作成の際に、入力したメールアドレスが既に使われていないかをチェック
          使われていれば0を、使われていなければ1を返す
        */
    public function checkDuplicateMailForInputAction() {

        $request = new \Phalcon\Http\Request();

        if ($request->isPost() == true) {
            $email = $request->getPost('email');
            $this->useModel('Account');
            $account = $this->Account->findFirstByMail($email);
            echo ($account && $account->deleted == 0) ? 1 : 0;
            $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        }
    }
        /**
        * ajax通信先
          アカウント修正の際に、入力したメールアドレスが既に使われていないかをチェック
          使われていれば0を、使われていなければ1を返す
          アカウント入力の際とは異なり、そのアカウントの元のメールアドレスの場合は許容
        */
    public function checkDuplicateMailForAccountModifyAction() {

        $request = new \Phalcon\Http\Request();

        if ($request->isPost() == true) {
            $email = $request->getPost('email');
            $this->useModel('Account');
            $account = $this->Account->findFirstByMail($email);
            echo ($account && $account->id !== $this->basic->id) ? 1 : 0;
            $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
        }
    }
        /**
        * ajax通信先
          パスワード
          再設定画面(login/resetPassword)において、入力されたメールアドレスに合致するアカウントが
         　存在するか否かをチェック。
         　また、存在する場合、そのアカウントのIDを取得
          各々を、,を挟んだ文字列として返す
        */
    public function checkDuplicateMailForResetPasswordAction() {

        //echo 'e,w';exit;

        $request = new \Phalcon\Http\Request();
        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);

        if ($request->isPost() == true) {
            $email = $request->getPost('email');
            $this->useModel('Account');
            $account = $this->Account->findFirstByMail($email);
            echo!empty($account) ? 1 : 0;
            echo ',';
            echo!empty($account) ? $account->id : 0;
            // 1:使用済みアドレス
            // 0:未使用アドレス
            exit;
        }
    }

    public function resetPasswordAction() {

        //viewのみ
        $this->assets->addJs('admin_scripts/login/resetPassword.js');
        $this->assets->addCss('admin_css/error_red.css');
    }
       
    
        /**
        * パスワード再設定メール送信場面
         　任意のトークンを発行し、tokensテーブルに格納
         　及び、そのトークンに基づき、パスワード再設定メールを送信
         
        */
    public function resetPasswordSentAction() {

        $request = new \Phalcon\Http\Request();

        if ($request->isGet() == true) {

            $email = $request->getQuery('mail');
            $account_id = $request->getQuery('account_id');

            $this->view->email = $email;

            $token = md5(uniqid());

            $this->useModel('Token');
            $this->Token->addToken($token);

            $this->useModel('Account');
            $data = [
                'email' => $email,
                'account_id' => $account_id,
                'token' => $token
            ];
            $this->Account->sendResetPasswordMail($data);
        }
    }
        /**
        * パスワード再設定開始場面
         　パスワード再設定メールからのリンク先であり、アカウントのID及び発行されたトークンを、getで受け取る
          tokensテーブルに、受け取ったトークン値と一致するものが存在するか否かをチェックする。
         　存在する場合には、受け取ったアカウントIDを元に該当アカウントを取得し、そのアカウントIDと
           security_question_id(秘密の質問の種類を表すid)をviewに飛ばす。
           アカウントのidは、hidden値で、login/resetPasswordProcedureへとpostで送られる
        */
    public function resetPasswordStartAction() {

        $this->assets->addJs('admin_scripts/login/resetPasswordStart.js');
        $this->assets->addCss('admin_css/error_red.css');

        $request = new \Phalcon\Http\Request();

        if ($request->isGet() == true) {

            $account_id = $request->getQuery('account_id');
            $token = $request->getQuery('token');
            $this->useModel('Token');

            if ($this->Token->checkToken($token)) {
                $this->useModel('Account');
                $account = $this->Account->findFirstById($account_id);
                $this->view->security_question_id = $account->security_question_id;
                $this->view->account_id = $account_id;
            }
        }
    }
        /**
        * ajax通信先
           パスワード再設定処理
         　アカウントID、秘密の質問の答え、新たなパスワードをpostで受け取る
         　秘密の質問の答えが正しかった場合、パスワードを新たなものに更新
         
        */
    public function resetPasswordProcedureAction() {

        $request = new \Phalcon\Http\Request();

        if ($request->isPost() == true) {

            $security_question_answer = $request->getPost('security_question_answer');
            $new_password = $request->getPost('new_password');
            $account_id = $request->getPost('account_id');
        }

        $this->useModel('Account');
        $encrypted_password = $this->Account->encryptPassword($new_password);
        $account = $this->Account->findFirstById($account_id);

        $is_login = '0';
        if ($account->deleted == '1') {
            $is_login = '-2';
            echo $is_login;
            exit;
        }

        if ($security_question_answer != $account->security_question_answer) {
            $is_login = '-1';
            echo $is_login;
            exit;
        }

        if ($this->Account->updatePassword($account_id, $encrypted_password)) {
            $is_login = '1';
        }

        echo $is_login;

        $this->view->setRenderLevel(\Phalcon\Mvc\View::LEVEL_ACTION_VIEW);
    }

    public function resetPasswordCompleteAction() {
        //viewのみ
    }
        
        /**
        * すべてのセッションを破棄した上で、該当アカウントを論理削除(deletedカラムを1に変更)
          postで、該当アカウントIDを$register_idとして受け取る(セッションが破棄されるため)
        */
    public function accountModifyDeleteThanksAction() {

        $this->session->destroy();
        $request = new \Phalcon\Http\Request();

        if ($request->isPost() == true) {
            $register_id = $request->getPost('register_id');
            $this->useModel('Account');
            $this->Account->deleteAccount($register_id);
        }
    }

}
