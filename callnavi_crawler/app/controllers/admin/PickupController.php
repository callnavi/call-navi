<?php

class PickupController extends AdminControllerBase {

    use ImageEditable;

use Mailable;

use ScopeCalculatable;

    public function makeAction() { 
         //viewのみ
    }
        
          /**
        * ピックアップ広告作成フォーム入力場面。
         「確認画面へ進む」ボタン押下により、pickup/makeConfirmへ、postで値を送る
           pickup/makeConfirmから、「入力内容を修正する」で戻った場合には、postで受け取ったpickup_offersテーブルのidに基づきDB(pickup_offersテーブル)に格納されたデータを初期値として保持する事により入力内容が保持される
        */
    public function makeStartAction() {

        $this->assets->addJs('admin_scripts/pickup/makeStart.js');
        $this->assets->addCss('admin_css/error_red.css');

        $request = new \Phalcon\Http\Request();

        if ($request->isPost() == true) {
            $offer_id = $request->getPost('offer_id');

            $this->useModel('PickupOffer');
            $data = $this->PickupOffer->findOnConfirmPickupOfferById($offer_id);

            $this->useModel('PickupOfferDetail');
            $detail = $this->PickupOfferDetail->findPickupOfferDetailById($offer_id);

            $features = explode(":", $data->features);
            $hired_as = explode(":", $data->hired_as);

            $this->view->features = $features;
            $this->view->hired_as = $hired_as;
            $this->view->data = $data;
            $this->view->detail = $detail;
            $this->view->post = 1;
        } else {

            $this->view->post = 0;
        }

        $this->useModel('PickupOffer');




        




        $strDate = 'tomorrow'; //翌日の0時までキャッシュさせる場合
        $dt = new DateTime($strDate);
        header('Cache-Control: max-age=' . ($dt->format('U') - time()));
    }
        /**
        * ピックアップ広告作成フォーム確認場面。
           pickup/makeからpostで入力値を受け取り、入力に内容に応じてデータをDB(基本はpickup_offersテーブル、詳細ページフォーム入力内容のみpickup_offer_detailsテーブル)に格納、及び確認画面に表示。
           pickup/makeで添付された画像について、サムネイル画像をpublic/images/pickup/thumに、
　　　　　　　　　　　　　　　　　　　　　　　　　　 写真1をpublic/images/pickup/firstに、
　　　　　　　　　　　　　　                     写真2をpublic/images/pickup/secondに
                                             写真3をpublic/images/pickup/thirdに、
                                            広告ID.png」という形式で保存、　及び確認画面に表示
          　データについて、statusカラムはjudging(審査中)、is_confirmedカラム(作成完了か否かを判断)は1となる。
           入力内容の保存処理により既にis_confirmedカラムが1であるデータが存在する場合には、データ新規作成処理ではなく
          　データ更新処理となる。この場合、is_confirmedカラムは1のまま。また、データ更新処理の場合、listing_keywordsテーブルに格納する検索キーワードに関しては、
          従来の検索キーワードのdeletedカラムを1とする論理削除を行った上で新たな検索キーワードデータを格納する形となる。
        */
    public function makeConfirmAction() {
        $this->assets->addCss('admin_css/summary_fold.css');
        $this->assets->addJs('admin_scripts/pickup/makeConfirm.js');
        $request = new \Phalcon\Http\Request();


        if ($request->isPost() == true) {
            $offer_id_set = $request->getPost('offer_id_set');

            $data = $this->__setPickupData($request->getPost());


            $this->useModel('PickupOffer');





         

            $publish_week = $data['publish_week'];
            
        

            
            $data['account_id'] = $this->basic->id;
            if ($offer_id_set == "-1") {
                $this->view->data = $newOffer = $this->PickupOffer->addPickupOffer($data);
                $newOffer->expense1 = $this->PickupOffer->calExpense1($newOffer->publish_week);
                $data['offer_id'] = $newOffer->id;
                $this->useModel('PickupOfferDetail');
                if ($data['detail_content_type'] == "form") {
                    $this->PickupOfferDetail->addPickupOfferForm($data);
                } else if ($data['detail_content_type'] == "html") {
                    $this->PickupOfferDetail->addPickupOfferHtml($newOffer->id, $data['html']);
                }
            } else {
                $this->useModel('PickupOffer');
                $data['id'] = intval($offer_id_set);
                $this->view->data = $newOffer = $this->PickupOffer->updatePickupOffer($data, "editing");
                $newOffer->expense1 = $this->PickupOffer->calExpense1($newOffer->publish_week);
                $this->useModel('PickupOfferDetail');
                $data['offer_id'] = $data['id'];
                if ($data['detail_content_type'] == "form") {
                    $this->PickupOfferDetail->updatePickupOfferForm($data);
                } else if ($data['detail_content_type'] == "html") {
                    $this->PickupOfferDetail->updatePickupOfferHtml($data['offer_id'], $data['html']);
                } else {
                    $this->PickupOfferDetail->vacatePickupOfferDetail($data['offer_id']);
                }
            }

            $features = explode(":", $newOffer->features);
            $newOffer->features_array = $features;
            $hired_as = explode(":", $newOffer->hired_as);
            $today = date("Y/m/d");
            $published_from_display = date('Y/m/d', strtotime($newOffer->published_from));
            $this->view->data = $newOffer;
            $this->view->today = $today;
            
            $this->view->features = $features;
            $this->view->hired_as = $hired_as;
            $this->view->published_from_display = $published_from_display;

            

            $strDate = 'tomorrow'; //翌日の0時までキャッシュさせる場合
            $dt = new DateTime($strDate);
            header('Cache-Control: max-age=' . ($dt->format('U') - time()));
            if ($this->request->hasFiles() == true) {



                $uploaddir[0] = '/../../../public/images/pickup/thum/';
                
                $uploaddir[1] = '/../../../public/images/pickup/first/';
                $uploaddir[2] = '/../../../public/images/pickup/second/';
                $uploaddir[3] = '/../../../public/images/pickup/third/';



                for ($index = 0; $index <= 3; $index++) {
                    $uploadfile[$index] = $uploaddir[$index] . $data['offer_id'] . '.png';
                    if ($_FILES['file']['tmp_name'][$index] !== "") {
                        if (file_exists(__DIR__ . $uploadfile[$index])) {
                            unlink(__DIR__ . $uploadfile[$index]);
                        }
                        move_uploaded_file($_FILES['file']['tmp_name'][$index], __DIR__ . $uploadfile[$index]);
                    }
                    
                }
            }
        }
        
    }
           /**
        * ピックアップ広告作成完了場面。
           既に存在するデータについて、statusカラムをjudging(編集中)とする。
           また、出向者管理者双方へリスティング広告作成完了の旨のメールを送信。
        */
    public function makeThanksAction() {

        $request = new \Phalcon\Http\Request();

        if ($request->isPost() == true) {

            $offer_id = $request->getPost('offer_id');
            $this->useModel('PickupOffer');

            $this->PickupOffer->confirmPickupOffer($offer_id);





            $this->PickupOffer->sendThanksMail($offer_id);
            $this->PickupOffer->sendThanksAdminMail($offer_id);

            $strDate = 'tomorrow'; //翌日の0時までキャッシュさせる場合
            $dt = new DateTime($strDate);
            header('Cache-Control: max-age=' . ($dt->format('U') - time()));
        }
    }
          /**
        * ajax通信先
           ピックアップ広告保存場面。(pickup/makeにおいて、「入力内容を保存する」を押下した場合)
           pickup/makeからpostで入力値を受け取り、入力内容に応じてDB(pickup_offersテーブル)にデータを格納(ajax通信)
           listing/makeで添付されたサムネイル画像をpublic/images/pickup/thumに、
                                  写真1をpublic/images/pickup/firstに、
　　　　　　　　　　　　　　           写真2をpublic/images/pickup/secondに
                                  写真3をpublic/images/pickup/thirdに、「広告ID.png」という形式で保存
           statusカラムはediting(編集中)、is_confirmedカラムは1となる。
           入力内容の保存処理が既に一度以上行われており既にis_confirmedカラムが1であるデータが存在する場合には、データ新規作成処理ではなく
          　データ更新処理となる。
        */
    public function saveAction() {
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);
        $request = new \Phalcon\Http\Request();
        if ($request->isPost() == true) {
            $offer_id_set = $request->getPost('offer_id_set');

            $data = $this->__setPickupData($request->getPost());
            $data['account_id'] = $this->basic->id;
            $this->useModel('PickupOffer');

            
            if ($offer_id_set == "-1") {
                $newOffer = $this->PickupOffer->savePickupOffer($data);
                $data['offer_id'] = $newOffer->id;
                $this->useModel('PickupOfferDetail');


                if ($data['detail_content_type'] == "form") {
                    $this->PickupOfferDetail->addPickupOfferForm($data);
                } else if ($data['detail_content_type'] == "html") {
                    $this->PickupOfferDetail->addPickupOfferHtml($newOffer->id, $data['html']);
                }
            } else {
                $data['id'] = intval($offer_id_set);
                $this->PickupOffer->updatePickupOffer($data, "editing");
                $this->useModel('PickupOfferDetail');
                $data['offer_id'] = $data['id'];
                if ($data['detail_content_type'] == "form") {
                    $this->PickupOfferDetail->updatePickupOfferForm($data);
                } else if ($data['detail_content_type'] == "html") {
                    $this->PickupOfferDetail->updatePickupOfferHtml($data['offer_id'], $data['html']);
                } else {
                    $this->PickupOfferDetail->vacatePickupOfferDetail($data['offer_id']);
                }

                $newOffer = new stdClass;
                $newOffer->id = $data['id'];  
            }




            if ($this->request->hasFiles() == true) {



                $uploaddir[0] = '/../../../public/images/pickup/thum/';
                
                $uploaddir[1] = '/../../../public/images/pickup/first/';
                $uploaddir[2] = '/../../../public/images/pickup/second/';
                $uploaddir[3] = '/../../../public/images/pickup/third/';



                for ($index = 0; $index <= 3; $index++) {
                    $uploadfile[$index] = $uploaddir[$index] . $data['offer_id'] . '.png';
                    if ($_FILES['file']['tmp_name'][$index] !== "") {
                        if (file_exists(__DIR__ . $uploadfile[$index])) {
                            unlink(__DIR__ . $uploadfile[$index]);
                        }
                        move_uploaded_file($_FILES['file']['tmp_name'][$index], __DIR__ . $uploadfile[$index]);
                    }
                }
            }
        }
        
        echo $newOffer->id;

    }
        
        /**
        * ピックアップ広告編集フォーム入力場面。
          pickup_offersテーブルのidの値をgetで受け取る事により、該当するデータの値をフォーム上に初期値として表示
         「確認画面へ進む」ボタン押下により、pickup/editConfirmへ、postで値を送る
           pickup/editConfirmから、「入力内容を修正する」で戻った場合には、DB(pickup_offersテーブル)に格納されたデータを初期値として保持する事により入力内容が保持される
        */
    public function editAction() {

        $this->assets->addJs('admin_scripts/pickup/edit.js');
        $this->assets->addCss('admin_css/error_red.css');

        $request = new \Phalcon\Http\Request();

        if ($request->isGet() == true) {

            if ($this->basic->id == $request->getQuery('account_id') or $this->basic->is_admin == 1) {

                $this->useModel('PickupOffer');
                $data = $this->PickupOffer->findPickupOfferById($request->getQuery('offer_id'));

                $this->useModel('PickupOfferDetail');
                $detail = $this->PickupOfferDetail->findPickupOfferDetailById($request->getQuery('offer_id'));

                $features = explode(":", $data->features);
                $hired_as = explode(":", $data->hired_as);

                $this->view->detail = $this->PickupOfferDetail->findPickupOfferDetailById($request->getQuery('offer_id'));

                $this->view->features = $features;
                $this->view->hired_as = $hired_as;
                $this->view->data = $data;

                $strDate = 'tomorrow'; //翌日の0時までキャッシュさせる場合
                $dt = new DateTime($strDate);
                header('Cache-Control: max-age=' . ($dt->format('U') - time()));
            } else {
                $this->response->redirect('customer/index');
            }
        }
    }

    
        /**
        * ピックアップ広告作成フォーム確認場面。
           pickup/editからpostで入力値を受け取り、入力に内容に応じて該当データを更新、及び確認画面に表示。
           pickup/makeで添付されたサムネイル画像をpublic/images/pickup/thumに、
                                写真1をpublic/images/pickup/firstに、
　　　　　　　　　　　　　　         写真2をpublic/images/pickup/secondに
                                写真3をpublic/images/pickup/thirdに、
                                広告ID.png」という形式で保存及び確認画面に表示
            statusカラムはediting(編集中)、is_confirmedカラムは1のまま
        */
    public function editConfirmAction() {
        $this->assets->addCss('admin_css/summary_fold.css');
        $this->assets->addJs('admin_scripts/pickup/editConfirm.js');
        $request = new \Phalcon\Http\Request();

        if ($request->isPost() == true) {
            $data = $this->__setPickupData($request->getPost());
            $data['account_id'] = $this->basic->id;
            $data['id'] = $request->getPost('offer_id');

            $this->useModel('PickupOffer');

            $newOffer = $this->PickupOffer->updatePickupOffer($data, 'editing');  
            $newOffer->expense1 = $this->PickupOffer->calExpense1($newOffer->publish_week);

            $data['offer_id'] = $newOffer->id;
            $this->useModel('PickupOfferDetail');
            if ($data['detail_content_type'] == "form") {

                $this->PickupOfferDetail->updatePickupOfferForm($data);
            } else if ($data['detail_content_type'] == "html") {
                $this->PickupOfferDetail->updatePickupOfferHtml($newOffer->id, $data['html']);
            } else {
                $this->PickupOfferDetail->vacatePickupOfferDetail($newOffer->id);
            }
            $features = explode(":", $newOffer->features);
            $newOffer->features_array = $features;
            $hired_as = explode(":", $newOffer->hired_as);
            $today = date("Y/m/d");
            $published_from_display = date('Y/m/d', strtotime($newOffer->published_from));

            $this->view->data = $newOffer;
            $this->view->today = $today;
            
            $this->view->features = $features;
            $this->view->hired_as = $hired_as;
            $this->view->published_from_display = $published_from_display;

            $strDate = 'tomorrow'; //翌日の0時までキャッシュさせる場合
            $dt = new DateTime($strDate);
            header('Cache-Control: max-age=' . ($dt->format('U') - time()));


            if ($this->request->hasFiles() == true) {


                $uploaddir[0] = '/../../../public/images/pickup/thum/';
 
                $uploaddir[1] = '/../../../public/images/pickup/first/';
                $uploaddir[2] = '/../../../public/images/pickup/second/';
                $uploaddir[3] = '/../../../public/images/pickup/third/';



                for ($index = 0; $index <= 3; $index++) {
                    $uploadfile[$index] = $uploaddir[$index] . $data['offer_id'] . '.png';



                    if ($_FILES['file']['tmp_name'][$index] !== "") {


                        if (file_exists(__DIR__ . $uploadfile[$index])) {
                            unlink(__DIR__ . $uploadfile[$index]);
                        }
                        move_uploaded_file($_FILES['file']['tmp_name'][$index], __DIR__ . $uploadfile[$index]);
                    }


                }
                
            }
        }
        
    }
        
           /**
        * ピックアップ広告編集完了場面。
          既に存在する該当データについて、statusカラムをediting(編集中)からjudging(審査中)に変更。
           また、出向者管理者双方へ無料広告作成完了の旨のメールを送信。
        */
    public function editThanksAction() {

        $request = new \Phalcon\Http\Request();

        if ($request->isGet() == true) {
            if ($request->getQuery('account_id') == $this->basic->id or $this->basic->is_admin == 1) {
                $this->useModel('PickupOffer');
                $offer_id = $request->getQuery('offer_id');
                $offer = $this->PickupOffer->findPickupOfferById($offer_id);
                $offer->status = 'judging';
                $offer->is_confirmed = 1;

                $date = new DateTime();
                $today = $date->format('Y-m-d H:i:s');
                $offer->accepted = $today;
                $offer->updateColumn();
                $this->PickupOffer->sendThanksMail($offer_id);
                $this->PickupOffer->sendThanksAdminMail($offer_id);

                $strDate = 'tomorrow'; //翌日の0時までキャッシュさせる場合
                $dt = new DateTime($strDate);
                header('Cache-Control: max-age=' . ($dt->format('U') - time()));
            } else {
                $this->response->redirecct('customer/index');
            }
        }
    }
        
        /**
        *  ajax通信先
           ピックアップ広告作成フォーム保存場面。
           free/editからpostで入力値を受け取り、入力に内容に応じて該当データを更新(ajax通信)
          　statusカラムはediting(編集中)、is_confirmedカラムは1のまま
        */
    public function editSaveAction() {
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);
        $request = new \Phalcon\Http\Request();
        if ($request->isPost() == true) {
            $data = $this->__setPickupData($request->getPost());

            $data['account_id'] = $this->basic->id;
            $data['id'] = $request->getPost('offer_id');
            $this->useModel('PickupOffer');

            
            $newOffer = $this->PickupOffer->updatePickupOffer($data, 'editing');

            $data['offer_id'] = $newOffer->id;
            $this->useModel('PickupOfferDetail');

            if ($data['detail_content_type'] == "form") {
                $this->PickupOfferDetail->updatePickupOfferForm($data);
            } else if ($data['detail_content_type'] == "html") {

                $this->PickupOfferDetail->updatePickupOfferHtml($newOffer->id, $data['html']);
            } else {
                $this->PickupOfferDetail->vacatePickupOfferDetail($newOffer->id);
            }

            if ($this->request->hasFiles() == true) {


                $uploaddir[0] = '/../../../public/images/pickup/thum/';
                
                $uploaddir[1] = '/../../../public/images/pickup/first/';
                $uploaddir[2] = '/../../../public/images/pickup/second/';
                $uploaddir[3] = '/../../../public/images/pickup/third/';



                for ($index = 0; $index <= 3; $index++) {
                    $uploadfile[$index] = $uploaddir[$index] . $data['offer_id'] . '.png';

                    if ($_FILES['file']['tmp_name'][$index] !== "") {
                        if (file_exists(__DIR__ . $uploadfile[$index])) {
                            unlink(__DIR__ . $uploadfile[$index]);
                        }
                        move_uploaded_file($_FILES['file']['tmp_name'][$index], __DIR__ . $uploadfile[$index]);
                    }
                }
            }
        }
    }
        /**
        *  ピックアップ広告プレビュー表示画面(管理画面の広告一覧から、プレビューの下の目マークを押下した場合)
          getで受け取ったpickup_offersのidに基づき、該当するピックアップ広告のプレビューを表示
        */
    public function previewAction() {
        $this->assets->addJs('admin_scripts/pickup/preview.js');
        $this->assets->addCss('admin_css/error_red.css');
        
        $request = new \Phalcon\Http\Request();

        if ($request->isGet() == true) {

            $this->useModel('PickupOffer');
            $data = $this->PickupOffer->findPickupOfferById($request->getQuery('offer_id'));


            $message_sanitized = $this->PickupOffer->sanitize($data->message);


            $message_top = mb_strimwidth($message_sanitized, 0, 30, "...");

            $features = explode(":", $data->features);
            $data->features_array = $features;
            $hired_as = explode(":", $data->hired_as);
            $today = date("Y/m/d");
            $published_from_display = date('Y/m/d', strtotime($data->published_from));
            $this->view->data = $data;
            $this->view->today = $today;
            $this->view->message_top = $message_top;
            $this->view->features = $features;
            $this->view->hired_as = $hired_as;
            $this->view->published_from_display = $published_from_display;
        }
    }
        /**
        *  ajax通信先
           customer/indexにおいて一時停止中のピックアップ広告を有効にした場合、該当するピックアップ広告のstatusカラムをpublishing(掲載中)に変更
        */
    public function effectOfferAction() {

        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
                $scope = $this->calScope($request->getQuery('scope'));
                $this->useModel('PickupOffer');
                $offer = $this->PickupOffer->findPickupOfferById($request->getQuery('offer_id'));
                $offer->status = "publishing";
                $offer->published_from = date("Y-m-d H:i:s", strtotime($offer->published_from));
                $offer->published_to = date("Y-m-d H:i:s", strtotime($offer->published_to));
                $offer->updateColumn();
                $offers = $this->PickupOffer->findPickupOffers($scope, $this->basic->id);
                $data = array("offer_id" => $offer->id, "offers" => $offers, "status" => $offer->status);
                header('Content-Type:application/json');
                echo json_encode($data);
            }
        }
    }
/*
    public function resumeAction() {

        $request = new \Phalcon\Http\Request();

        if ($request->isGet() == true) {
            if ($this->basic->id == $request->getQuery('account_id')) {
                $this->useModel('PickupOffer');
//                $data = $this->PickupOffer->convertObjectToArray($request->getQuery('offer_id'));
//                $data['features_string'] = implode(":", $data['features']);
//                $data['hired_as_string'] = implode(":", $data['hired_as']);
//                $data['today'] = date("Y/m/d");
//                $data['published'] = $pickup_offer->calPublishWeek($data['published_from'], $data['published_to']);
//                $data['expense'] = $pickup_offer->calExpense($data['display_area'], $data['published']);
                $data = $this->PickupOffer->findPickupOfferById($request->getQuery('offer_id'));

                $features = explode(":", $data->features);
                $hired_as = explode(":", $data->hired_as);

                $data->publish_week = $data->publish_week;
                $published_from_str = strtotime($this->PickupOffer->calPublishedFrom($data->display_area));
                $data->published_from = date('Y/m/d', $published_from_str);
                $data->published_to = date('Y/m/d', strtotime("$data->publish_week week", $published_from_str));

                $this->view->data = $data;
                $this->view->features = $features;
                $this->view->hired_as = $hired_as;
            } else {
                $this->response->redirect('customer/index');
            }
        }
    }

    public function resumeThanksAction() {

        $request = new \Phalcon\Http\Request();

        if ($request->isGet() == true) {
            if ($this->basic->id == $request->getQuery('account_id')) {
                $this->useModel('PickupOffer');
                $data = $this->PickupOffer->findPickupOfferById($request->getQuery('offer_id'));

                $date = new DateTime();
                $today = $date->format('Y-m-d H:i:s');

                $published_from_str = strtotime($this->PickupOffer->calPublishedFrom($data->display_area));
                $data->published_from = date('Y-m-d H:i:s', $published_from_str);
                $data->published_to = date('Y-m-d H:i:s', strtotime("$data->publish_week week", $published_from_str));

                $data->status = $published_from_str === strtotime($today) ? "publishing" : "will_publish";

                $data->updateColumn();
            } else {
                $this->response->redirect('customer/index');
            }
        }
    }
*/     

       /**
        *  入力フォームに入力され、postで送られた各々のデータを一つの配列に格納するに際しての共通処理
        */
    private function __setPickupData($inputData) {

        $outputData = [];

        $fields = ['title', 'job_category', 'hired_as', 'hired_as_string', 'company_name', 'salary_term', 'salary_value_min', 'salary_value_max', 'salary_unit_min', 'workplace_prefecture', 'workplace_area', 'workplace_address', 'message', 'features', 'features_string', 'has_first_pic', 'has_second_pic', 'has_third_pic', 'has_thum_pic', 'display_area', 'detail_content_type', 'detail_url', 'html', 'job_description', 'requirement', 'office_hours', 'holidays', 'welfares', 'procedures', 'others', 'founded', 'president', 'capital', 'employees', 'business_description', 'company_url', 'publish_week'];
        foreach ($fields as $field) {
            if (isset($inputData[$field])) {
                $outputData[$field] = $inputData[$field];
            }
        }
        return $outputData;
    }
        
        /**
        *  ajax通信先
           customer/indexにおいてピックアップ広告を一時停止した場合、該当する無料広告のstatusカラムをcanceled(一時停止中)に変更
        */
    public function cancelOfferAction() {

        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
                $this->useModel('PickupOffer');
                $offer = $this->PickupOffer->findPickupOfferById($request->getQuery('offer_id'));
                $offer->status = "canceled";
                $offer->published_from = date("Y-m-d H:i:s", strtotime($offer->published_from));
                $offer->published_to = date("Y-m-d H:i:s", strtotime($offer->published_to));
                $offer->updateColumn();

                $data = array("offer_id" => $offer->id);
                header('Content-Type:application/json');
                echo json_encode($data);
            }
        }
    }
        
        /**
        *  ajax通信先
           customer/indexにおいてピックアップ広告を掲載終了した場合、該当する無料広告のstatusカラムをcompleted(掲載終了)に変更
        */
    public function completeOfferAction() {

        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
                $this->useModel('PickupOffer');
                $offer = $this->PickupOffer->findPickupOfferById($request->getQuery('offer_id'));
                $this->PickupOffer->completePickupOffer($offer);
                $this->useModel('PickupPayment');
                $published_to = date("Y/m/d", strtotime($offer->published_to));
                $scope = $this->calScope('all');
                $offers = $this->PickupOffer->findPickupOffers($scope, $this->basic->id);
                $expense_sum = $this->calExpenseSum($offers);
                //pickup_paymentsテーブルに置いて、該当offer_idのcompletedカラムを1にする。
                $pickup_payment = new PickupPayment;
                $pickup_payment->complete($offer->id);
                $data = array("offer_id" => $offer->id, "expense" => $offer->expense, "publish_week" => $offer->publish_week, "published_to" => $published_to, "expense_sum" => $expense_sum);
                header('Content-Type:application/json');
                echo json_encode($data);
            }
        }
    }
        /**
        *  ajax通信先
           customer/indexにおいてピックアップ広告を削除した場合、該当するピックアップ広告のdeletedカラムを1に変更
         　この場合、出稿者ページにおいては表示されなくなり、管理者ページにおいてはステータスが「ユーザー削除」として
         　表示されるようになる。
        */
    public function deleteOfferAction() {

        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
                $scope = $this->calScope('all');
                $this->useModel('PickupOffer');
                $offer = $this->PickupOffer->findPickupOfferById($request->getQuery('offer_id'));
                $this->useModel('PickupPayment');
                $offer->expense = $this->PickupPayment->calPayment($offer);
                $offer->softDelete();
                $offers = $this->PickupOffer->findPickupOffers($scope, $this->basic->id);
                $count = count($offers);
                $expense_sum = $this->calExpenseSum($offers);
                $cardless_expense_sum = $this->calCardlessExpenseSum($offers);
                $data = array("offer_id" => $offer->id, "count" => $count, "expense_sum" => $expense_sum, "cardless_expense_sum" => $cardless_expense_sum);
                header('Content-Type:application/json');
                echo json_encode($data);
            }
        }
    }

    public function resumeOfferAction() {
        
    }

    public function cssSampleAction() {
        $this->assets->addCss('admin_css/module_view.css');
    }

    public function htmlAreaAction() {
        
    }
        
        /**
        *  ajax通信先
         　pickup/make　及びpickup/editにおいて、サムネイル画像、写真1、写真2、写真3それぞれの画像登録済みの場合のみ表示される「画像を削除する」ボタンを押下した際に、
         　該当するピックアップ広告のhas_thum_pic(サムネイル画像), has_first_pic(写真1) has_second_pic(写真2), has_third_pic(写真3)カラムを1から0へと変更し、その広告データが画像未登録のものとして扱われるようにする
        */
    public function deletePictureAction() {
        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);
        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
                $this->useModel('PickupOffer');
                $offer_id = $request->getQuery('offer_id');
                $offer = $this->PickupOffer->findOnConfirmPickupOfferById($offer_id);
                $index = $request->getQuery('index');

                switch ($index) {

                    case 0:
                        $offer->has_thum_pic = 0;
                        break;
                    case 1:
                        $offer->has_logo_pic = 0;
                        break;
                    case 2:
                        $offer->has_first_pic = 0;
                        break;
                    case 3:
                        $offer->has_second_pic = 0;
                        break;
                    case 4:
                        $offer->has_third_pic = 0;
                        break;
                }

                $offer->updateColumn();
            }
        }
    }
    
    //ローカルテスト用
    // public function stopAction() {
    //     $this->useModel('PickupOffer');
    //     $offers = $this->PickupOffer->findStoppedOffers();
    //     echo $offers[0]->title;
    //     exit;
    // }
    
    // public function dayAction() {
    //     $today = date("Y/m/d");
    //     $one_day_ago = date('Y/m/d', strtotime("$today - 1 day"));
    //     echo $one_day_ago;
    //     exit;
        
    // }

}
