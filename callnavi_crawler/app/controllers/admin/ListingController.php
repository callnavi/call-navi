<?php

class ListingController extends AdminControllerBase {

    use ImageEditable;

    use ScopeCalculatable;
    
    use VariablesCalculatable;
           /**
        * リスティング広告作成フォーム入力場面。
         「確認画面へ進む」ボタン押下により、listing/makeConfirmへ、postで値を送る
           listing/makeConfirmから、「入力内容を修正する」で戻った場合には、postで受け取ったlisting_offersテーブルのidに基づきDB(listing_offersテーブル)に格納されたデータを初期値として保持する事により入力内容が保持される
        */
    
    public function makeAction() {

        $this->assets->addJs('admin_scripts/listing/make.js');
        $this->assets->addCss('admin_css/error_red.css');

        $this->useModel('ListingOffer');

        $this->view->average_click_price = $this->ListingOffer->averageClickPrice();


        $request = new \Phalcon\Http\Request();
         //一度確認画面に進んだ後に広告作成フォームに戻った場合
        if ($request->isPost() == true) {

            $offer_id = $request->getPost('offer_id');



            $data = $this->ListingOffer->findOnConfirmListingOfferById($offer_id);
            $hired_as = explode(":", $data->hired_as);
            $features = explode(":", $data->features);
            $data->features_array = $features;
            $this->useModel('ListingKeyword');

            $keywords = $this->ListingKeyword->implodeKeywords($offer_id);

            $this->view->data = $data;
            $this->view->keywords = $keywords;

            $this->view->features = $features;
            $this->view->hired_as = $hired_as;
            $this->view->post = 1;
        //新規に広告作成フォームを開いた場合
        } else {
            $this->view->post = 0;
        }


        $strDate = 'tomorrow'; //翌日の0時までキャッシュさせる場合
        $dt = new DateTime($strDate);
        header('Cache-Control: max-age=' . ($dt->format('U') - time()));
    }

        /**
        * リスティング広告作成フォーム確認場面。
           listing/makeからpostで入力値を受け取り、入力に内容に応じてデータをDB(基本はlisting_offersテーブル、検索キーワードのみlisting_keywordsテーブル)に格納、及び確認画面に表示。
           listing/makeで添付された画像をpublic/images/listingに、「広告ID.png」という形式で保存及び確認画面に表示
          　データについて、statusカラムはediting(審査中)、is_confirmedカラム(作成完了か否かを判断)は1となる
           入力内容の保存処理により既にis_confirmedカラムが1であるデータが存在する場合には、データ新規作成処理ではなく
          　データ更新処理となる。この場合、is_confirmedカラムは1のまま。また、データ更新処理の場合、listing_keywordsテーブルに格納する検索キーワードに関しては、
          従来の検索キーワードのdeletedカラムを1とする論理削除を行った上で新たな検索キーワードデータを格納する形となる。
        */
    public function makeConfirmAction() {
        $this->assets->addCss('admin_css/summary_fold.css');
        $request = new \Phalcon\Http\Request();
        
        if ($request->isPost() == true) {

            $offer_id_set = $request->getPost("offer_id_set");



            $data = $this->__setListingData($request->getPost());




            $data['allkeywords_string'] = $request->getPost('allkeywords_string');



            $allkeywords = explode(",", $data['allkeywords_string']);
           
            $data['account_id'] = $this->basic->id;
            $this->useModel('ListingOffer');
            
            

           //新たにデータ作成となる場合($offer_id_setは-1となっている)
            if ($offer_id_set == "-1") {
                $this->view->data = $newOffer = $this->ListingOffer->addListingOffer($data);
                $this->useModel('ListingKeyword');
                $allkeywords_string = $request->getPost('allkeywords_string');
                $allkeywords = explode(",", $allkeywords_string);
                $newListing = $this->ListingKeyword->addKeywords($allkeywords, $newOffer->id);
           //既にあるデータの更新処理となる場合($offer_id_setは、更新されるべき広告レコードのIDとなっている)
                
            } else {
                 $data['id'] = intval($offer_id_set);
                $this->view->data = $newOffer = $this->ListingOffer->updateListingOffer($data, "editing");
                $this->useModel('ListingKeyword');
                $this->ListingKeyword->softDeleteKeywords($newOffer->id);
                $this->ListingKeyword->addKeywords($allkeywords, $newOffer->id);
                
            }
            

            $keywords = $this->ListingKeyword->implodeKeywords($newOffer->id);
            

            $allkeywords = explode("\n", $keywords);
            $keywords_display = "";
            foreach ($allkeywords as $eachkeyword) {
                $keywords_display .= $eachkeyword . "<br>";
            }

            
            $hired_as = explode(":", $newOffer->hired_as);
            $features = explode(":", $newOffer->features);



            $this->view->keywords_display = $keywords_display;
            $this->view->keywords_flag = 1;

            $this->view->features = $features;
            $this->view->hired_as = $hired_as;
            
            

            $strDate = 'tomorrow'; //翌日の0時までキャッシュさせる場合
            $dt = new DateTime($strDate);
            header('Cache-Control: max-age=' . ($dt->format('U') - time()));





            if ($this->request->hasFiles() == true) {
                foreach ($this->request->getUploadedFiles() as $file) {
                    $image = $this->getImage($file->getTempName(), $file->getType());
                    $image = $this->minimizeImage($image, 200);
                    $imageUrl = '/images/listing/' . $newOffer->id . '.png';
                    $toPath = __DIR__ . '/../../../public' . $imageUrl;
                    if (file_exists($toPath)) {
                        unlink($toPath);
                    }
                    $this->locatePngImage($image, $toPath);
                }
            }
        }
    }
           /**
        * リスティング広告作成完了場面。
           既に存在するデータについて、statusカラムをjudging(審査中)とする。。
           また、出向者管理者双方へリスティング広告作成完了の旨のメールを送信。
        */
    public function makeThanksAction() {

        $request = new \Phalcon\Http\Request();

        if ($request->isPost() == true) {
            $offer_id = $request->getPost('offer_id');

            $this->useModel('ListingOffer');
            $this->ListingOffer->confirmListingOffer($offer_id);
            $this->ListingOffer->checkIsAvailable($offer_id);



            $this->ListingOffer->sendThanksMail($offer_id);
            $this->ListingOffer->sendThanksAdminMail($offer_id);

            $strDate = 'tomorrow'; //翌日の0時までキャッシュさせる場合
            $dt = new DateTime($strDate);
            header('Cache-Control: max-age=' . ($dt->format('U') - time()));
        }
    }
           /**
        * ajax通信先
          　リスティング広告保存場面。(listing/makeにおいて、「入力内容を保存する」を押下した場合)
           listing/makeからpostで入力値を受け取り、入力内容に応じてDB(pickup_offersテーブル)にデータを格納(ajax通信)
           listing/makeで添付された画像をpublic/images/listingに、「広告ID.png」という形式で保存
           statusカラムはediting(編集中)、is_confirmedカラムは1となる。
           入力内容の保存処理が既に一度以上行われており既にis_confirmedカラムが1であるデータが存在する場合には、データ新規作成処理ではなく
          　データ更新処理となる。データ更新処理の場合、listing_keywordsテーブルに格納する検索キーワードに関しては、
          従来の検索キーワードのdeletedカラムを1とする論理削除を行った上で新たな検索キーワードデータを格納する形となる。
        */
    public function saveAction() {



        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);
        $request = new \Phalcon\Http\Request();

        if ($request->isPost() == true) {

            $data = $this->__setListingData($request->getPost());
            $offer_id_set = $request->getPost('offer_id_set');
            $data['account_id'] = $this->basic->id;
            $allkeywords_string = $request->getPost('allkeywords_string');
            $allkeywords = explode(",", $allkeywords_string);
            $this->useModel('ListingOffer');
            if ($offer_id_set == "-1") {
                $newOffer = $this->ListingOffer->saveListingOffer($data);
                $this->useModel('ListingKeyword');


                if ($data['keywords'] !== "") {
                    $newListing = $this->ListingKeyword->saveKeywords($allkeywords, $newOffer->id);
                }
            } else {
                $data['id'] = intval($offer_id_set);
                $newOffer = $this->ListingOffer->updateListingOffer($data, "editing");
                $this->useModel('ListingKeyword');
                $this->ListingKeyword->softDeleteKeywords($data['id']);

                if ($data['keywords'] !== "") {
                    $this->ListingKeyword->saveKeywords($allkeywords, $data['id']);
                }
                $data['offer_id'] = $data['id'];

                $newOffer = new stdClass;
                $newOffer->id = $data['id'];
            }




            if ($this->request->hasFiles() == true) {
                foreach ($this->request->getUploadedFiles() as $file) {
                    $image = $this->getImage($file->getTempName(), $file->getType());
                    $image = $this->minimizeImage($image, 200);
                    $imageUrl = '/images/listing/' . $newOffer->id . '.png';
                    $toPath = __DIR__ . '/../../../public' . $imageUrl;
                    $this->locatePngImage($image, $toPath);
                }
            }
        }
        echo $newOffer->id;
    }
        
        /**
        * リスティング広告編集フォーム入力場面。
          listing_offersテーブルのidの値をgetで受け取る事により、該当するデータの値をフォーム上に初期値として表示
         「確認画面へ進む」ボタン押下により、listing/editConfirmへ、postで値を送る
           listing/editConfirmから、「入力内容を修正する」で戻った場合には、DB(listing_offersテーブル)に格納されたデータを初期値として保持する事により入力内容が保持される
        */
    public function editAction() {

        $this->assets->addJs('admin_scripts/listing/edit.js');
        $this->assets->addCss('admin_css/error_red.css');

        $request = new \Phalcon\Http\Request();

        if ($request->isGet() == true) {
            if ($this->basic->id == $request->getQuery('account_id') or $this->basic->is_admin == 1) {
                $offer_id = $request->getQuery('offer_id');
                $this->useModel('ListingOffer');
//                $data = $this->ListingOffer->convertObjectToArray($offer_id);
//                $data['hired_as'] = explode(":", $data['hired_as']);
//                $data['features'] = explode(":", $data['features']);
//                $this->useModel('ListingKeywotd');
//                $data['keywords'] = $this->ListingKeyword->implodeKeywords($offer_id);
                $data = $this->ListingOffer->findListingOfferById($offer_id);
                $hired_as = explode(":", $data->hired_as);
                $features = explode(":", $data->features);
                $this->useModel('ListingKeyword');
                $keywords = $this->ListingKeyword->implodeKeywords($offer_id);

                $this->view->data = $data;
                $this->view->hired_as = $hired_as;
                $this->view->features = $features;
                $this->view->keywords = $keywords;
                $this->view->average_click_price = $this->ListingOffer->averageClickPrice();

                $strDate = 'tomorrow'; //翌日の0時までキャッシュさせる場合
                $dt = new DateTime($strDate);
                header('Cache-Control: max-age=' . ($dt->format('U') - time()));
            } else {
                $this->response->redirect("customer/index");
            }
        }
    }
        
        /**
        * リスティング広告作成フォーム確認場面。
           listing/editからpostで入力値を受け取り、入力に内容に応じて該当データを更新、及び確認画面に表示。
           データ更新処理において、listing_keywordsテーブルに格納する検索キーワードに関しては、
          従来の検索キーワードのdeletedカラムを1とする論理削除を行った上で新たな検索キーワードデータを格納する形となる。
           listing/makeで添付された画像をpublic/images/listingに、「広告ID.png」という形式で保存及び確認画面に表示
          　statusカラムはediting(編集中)、is_confirmedカラムは1のまま
        */
    public function editConfirmAction() {
        $this->assets->addCss('admin_css/summary_fold.css');
        $request = new \Phalcon\Http\Request();

        if ($request->isPost() == true) {
            $data = $this->__setListingData($request->getPost());
            $data['account_id'] = $this->basic->id;
            $data['id'] = $request->getPost('id');
            $data['features_string'] = implode(":", $data['features']);
            $data['hired_as_string'] = implode(":", $data['hired_as']);
            $this->useModel('ListingOffer');
            $this->view->data = $newOffer = $this->ListingOffer->updateListingOffer($data, 'editing');
            $this->useModel('ListingKeyword');
            $this->ListingKeyword->softDeleteKeywords($data['id']);
            $allkeywords_string = $request->getPost('allkeywords_string');
            $allkeywords = explode(",", $allkeywords_string);
            $this->ListingKeyword->addKeywords($allkeywords, $newOffer->id);



         


            $keywords = $this->ListingKeyword->implodeKeywords($newOffer->id);
            

            $allkeywords = explode("\n", $keywords);
            $keywords_display = "";
            foreach ($allkeywords as $eachkeyword) {
                $keywords_display .= $eachkeyword . "<br>";
            }

            
            $hired_as = explode(":", $newOffer->hired_as);
            $features = explode(":", $newOffer->features);



            $this->view->keywords_display = $keywords_display;
            $this->view->keywords_flag = 1;
            $this->view->features = $features;
            $this->view->hired_as = $hired_as;
            
            if ($this->request->hasFiles() == true) {

                foreach ($this->request->getUploadedFiles() as $file) {
                    $image = $this->getImage($file->getTempName(), $file->getType());
                    $image = $this->minimizeImage($image, 200);
                    $imageUrl = '/images/listing/' . $newOffer->id . '.png';

                    $toPath = __DIR__ . '/../../../public' . $imageUrl;

                    $this->locatePngImage($image, $toPath);

                    $this->view->pictureUrl = $imageUrl;


                    $strDate = 'tomorrow'; //翌日の0時までキャッシュさせる場合
                    $dt = new DateTime($strDate);
                    header('Cache-Control: max-age=' . ($dt->format('U') - time()));
                }
            }
        }

        

        

    }
            /**
        * リスティング広告編集完了場面。
          既に存在する該当データについて、statusカラムをediting(編集中)からjudging(審査中)に変更。
           また、出向者管理者双方へ無料広告作成完了の旨のメールを送信。
        */

    public function editThanksAction() {

        $request = new \Phalcon\Http\Request();
        if ($request->isGet() == true) {
            if ($request->getQuery('account_id') == $this->basic->id or $this->basic->is_admin == 1) {
                $offer_id = $request->getQuery('offer_id');
                $this->useModel('ListingOffer');
                $offer = $this->ListingOffer->findListingOfferById($offer_id);
                $offer->status = 'judging';
                $offer->is_confirmed = 1;
                $date = new DateTime();
                $today = $date->format('Y-m-d H:i:s');
                $offer->accepted = $today;

                $offer->updateColumn();
                

                $this->ListingOffer->sendThanksMail($offer_id);
                $this->ListingOffer->sendThanksAdminMail($offer_id);

                $strDate = 'tomorrow'; //翌日の0時までキャッシュさせる場合
                $dt = new DateTime($strDate);
                header('Cache-Control: max-age=' . ($dt->format('U') - time()));
            } else {
                $this->response->redirect('customer/index');
            }
        }
    }
        
         /**
        *  ajax通信先
           リスティング広告作成フォーム保存場面。
           listing/editからpostで入力値を受け取り、入力に内容に応じて該当データを更新(ajax通信)
           データ更新処理において、listing_keywordsテーブルに格納する検索キーワードに関しては、
          従来の検索キーワードのdeletedカラムを1とする論理削除を行った上で新たな検索キーワードデータを格納する形となる。
          　statusカラムはediting(編集中)、is_confirmedカラムは1のまま
        */
    public function editSaveAction() {

//        $request = new \Phalcon\Http\Request();
//
//        if ($request->isPost() == true) {

        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);
        $request = new \Phalcon\Http\Request();
        if ($request->isPost() == true) {
            $data = $this->__setListingData($request->getPost());
            $data['account_id'] = $this->basic->id;
            $data['id'] = $request->getPost('id');
            $this->useModel('ListingOffer');
            $newOffer = $this->ListingOffer->updateListingOffer($data, 'editing');
            $this->useModel('ListingKeyword');
            $this->ListingKeyword->softDeleteKeywords($data['id']);
            $allkeywords_string = $request->getPost('allkeywords_string');
            $allkeywords = explode(",", $allkeywords_string);

            if ($data['keywords'] !== "") {
                $this->ListingKeyword->saveKeywords($allkeywords, $newOffer->id);
            }

            if ($this->request->hasFiles() == true) {
                foreach ($this->request->getUploadedFiles() as $file) {
                    $image = $this->getImage($file->getTempName(), $file->getType());
                    $image = $this->minimizeImage($image, 200);
                    $imageUrl = '/images/listing/' . $data['id'] . '.png';
                    $toPath = __DIR__ . '/../../../public' . $imageUrl;
                    $this->locatePngImage($image, $toPath);
                }
            }
        }
    }

        /**
        *  リスティング広告プレビュー表示画面(管理画面の広告一覧から、プレビューの下の目マークを押下した場合)
          getで受け取ったlisting_offersのidに基づき、該当する無料広告のプレビューを表示
        */

    public function previewAction() {
        $this->assets->addCss('admin_css/error_red.css');
        //$this->assets->addCss('admin_css/message.css');
        $request = new \Phalcon\Http\Request();
        
        if ($request->isGet() == true) {

            $offer_id = $request->getQuery('offer_id');
            $this->useModel('ListingOffer');
//                $data = $this->ListingOffer->convertObjectToArray($offer_id);
//                $data['features'] = explode(":", $data['features']);
//                $this->userModel('ListingKeyword');
            $data = $this->ListingOffer->findListingOfferById($offer_id);
            $hired_as = explode(":", $data->hired_as);
            $features = explode(":", $data->features);
            $data->features_array = $features;
            $this->useModel('ListingKeyword');
            //$data->keywords = $this->ListingKeyword->keywords;
            $keywords = $this->ListingKeyword->implodeKeywords($offer_id);
            $keywords_data = $this->ListingKeyword->findListingKeywordByOfferId($offer_id);

            $keywords_flag = isset($keywords_data->offer_id) ? 1 : 0;
            $allkeywords = explode("\n", $keywords);
            $keywords_display = "";
            foreach ($allkeywords as $eachkeyword) {
                $keywords_display .= $eachkeyword . "<br>";
            }

            $message_sanitized = $this->ListingOffer->sanitize($data->job_description);
            $message_top = mb_strimwidth($message_sanitized, 0, 30, "...");

            $this->view->data = $data;
            $this->view->keywords_display = $keywords_display;
            $this->view->keywords_flag = $keywords_flag;
            $this->view->features = $features;
            $this->view->hired_as = $hired_as;
            $this->view->message_top = $message_top;
        }
    }

         /**
        *  入力フォームに入力され、postで送られた各々のデータを一つの配列に格納するに際しての共通処理
        */
    private function __setListingData($inputData) {

        $outputData = [];
        // $fields = ['title', 'job_category', 'hired_as', 'company_name', 'salary_term', 'salary_value_min', 'salary_value_max', 'salary_unit_min', 'workplace_prefecture', 'workplace_area', 'workplace_address', 'job_description', 'features', 'keywords', 'click_price', 'budget_max', 'detail_url'];
        $fields = ['title', 'job_category', 'hired_as', 'hired_as_string', 'company_name', 'keywords', 'salary_term', 'salary_value_min', 'salary_value_max', 'salary_unit_min', 'workplace_prefecture', 'workplace_area', 'workplace_address', 'job_description', 'features', 'features_string', 'keywords', 'has_pic', 'click_price', 'budget_max', 'detail_url'];
        foreach ($fields as $field) {
            if (isset($inputData[$field])) {
                $outputData[$field] = $inputData[$field];
            }
        }

        return $outputData;
    }
        
        /**
        *  ajax通信先
           listing/indexのリスティング広告において対象期間を変更して「適用」を押下した場合において、しかるべきデータを表示
        */
    public function changeScopeAction() {

        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
                $listing_scope = $this->calScope($request->getQuery('listing_scope'));
                $this->useModel('ListingOffer');
                $offers = $this->ListingOffer->findListingOffers($listing_scope, $this->basic->id);
                $expense_sum = $this->calExpenseSum($offers);
                $cardless_expense_sum = $this->calCardlessExpenseSum($offers);
                $data = array("offers" => $offers, "expense_sum" => $expense_sum, "cardless_expense_sum" => $cardless_expense_sum);
                header('Content-Type:application/json');
                echo json_encode($data);
            }
        }
    }
        
        /**
        *  ajax通信先
           admin/allowedOffersのリスティング広告において対象期間を変更して「適用」を押下した場合において、しかるべきデータを表示
        */
    public function changeScopeForAdminAction() {

        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if ($request->isGet() == true) {
            
            if ($request->isAjax() == true) {
                
                $listing_scope = $this->calScope($request->getQuery('listing_scope'));
                $this->useModel('ListingOffer');
                $offers = $this->ListingOffer->findListingOffers($listing_scope, 'all');
                
                
                $expense_sum = $this->calExpenseSum($offers);
                $cardless_expense_sum = $this->calCardlessExpenseSum($offers);
                 
                $data = array("offers" => $offers, "expense_sum" => $expense_sum, "cardless_expense_sum" => $cardless_expense_sum);
                
                header('Content-Type:application/json');
                echo json_encode($data);
               
             
            }
        }
    }
        
        /**
        *  ajax通信先
           admin/allowedOffersのリスティング広告において対象期間を変更して「適用」を押下した場合において、しかるべきデータを表示
        */
    public function changeScopeForAccountsAction() {

        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
                $listing_scope = $this->calScope($request->getQuery('listing_scope'));
                $account_id = $request->getQuery('account_id');
                $this->useModel('ListingOffer');
                $offers = $this->ListingOffer->findListingOffers($listing_scope, $account_id);
                $expense_sum = $this->calExpenseSum($offers);
                $cardless_expense_sum = $this->calCardlessExpenseSum($offers);
                $data = array("offers" => $offers, "expense_sum" => $expense_sum, "cardless_expense_sum" => $cardless_expense_sum);
                header('Content-Type:application/json');
                echo json_encode($data);
            }
        }
    }

        /**
        *  ajax通信先
           customer/indexにおいてリスティング広告を一時停止した場合、該当するリスティング広告のstatusカラムをcanceled(一時停止中)に変更
        */
    public function cancelOfferAction() {

        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
                $scope = $this->calScope($request->getQuery('scope'));
                $this->useModel('ListingOffer');
                $offer = $this->ListingOffer->findListingOfferById($request->getQuery('offer_id'));
                $offer->status = "canceled";
                $offer->updateColumn();
                $this->useModel('ListingKeyword');
                $keywords = $this->ListingKeyword->findKeywordsById($scope, $request->getQuery('offer_id'));
                foreach ($keywords as $keyword) {
                    $keyword->status = "canceled";
                    $keyword->updateColumn();
                }
                $offers = $this->ListingOffer->findListingOffers($scope, $this->basic->id);
                $data = array("offer_id" => $offer->id, "offers" => $offers);
                //$data = "a";
                header('Content-Type:application/json');
                echo json_encode($data);
            }
        }
    }
        
        /**
        *  ajax通信先
           customer/indexにおいて一時停止中のリスティング広告を有効にした場合、該当するリスティング広告のstatusカラムをpublishing(掲載中)に変更
        */
    public function effectOfferAction() {

        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
                $scope = $this->calScope($request->getQuery('scope'));
                $this->useModel('ListingOffer');
                $offer = $this->ListingOffer->findListingOfferById($request->getQuery('offer_id'));
                $offer->status = "publishing";
                $offer->updateColumn();
                $this->useModel('ListingKeyword');
                $keywords = $this->ListingKeyword->findKeywordsById($scope, $request->getQuery('offer_id'));
                foreach ($keywords as $keyword) {
                    $keyword->status = $offer->status;
                    $keyword->updateColumn();
                }
                $offers = $this->ListingOffer->findListingOffers($scope, $this->basic->id);
                $data = array("offer_id" => $offer->id, "offers" => $offers, "status" => $offer->status);
                header('Content-Type:application/json');
                echo json_encode($data);
            }
        }
    }
        
        /**
        *  ajax通信先
           customer/indexにおいてリスティング広告を削除した場合、該当するリスティング広告のdeletedカラムを1に変更
         　この場合、出稿者ページにおいては表示されなくなり、管理者ページにおいてはステータスが「ユーザー削除」として
         　表示されるようになる。
        */
    public function deleteOfferAction() {

        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
                $scope = $this->calScope($request->getQuery('scope'));
                $this->useModel('ListingOffer');
                $offer = $this->ListingOffer->findListingOfferById($request->getQuery('offer_id'));
                $offer->softDelete();
                $this->useModel('ListingKeyword');
                $this->ListingKeyword->softDeleteKeywords($request->getQuery('offer_id'));
                $offers = $this->ListingOffer->findListingOffers($scope, $this->basic->id);
                $expense_sum = $this->calExpenseSum($offers);
                $cardless_expense_sum = $this->calCardlessExpenseSum($offers);
                $count = count($offers);
                $data = array("offer_id" => $offer->id, "offers" => $offers, "expense_sum" => $expense_sum, "cardless_expense_sum" => $cardless_expense_sum, "count" => $count);
                header('Content-Type:application/json');
                echo json_encode($data);
            }
        }
    }
        /**
        *  customer/indexのリスティング広告において、検索ワードの虫眼鏡アイコンを押下した際、該当する検索ワードの一覧を表示
        */
    public function KeywordAction() {

        $this->assets
                ->addJs('admin_scripts/changeScope.js')
                ->addJs('admin_scripts/customer/effectOffer.js')
                ->addJs('admin_scripts/customer/cancelOffer.js')
                ->addJs('admin_scripts/customer/deleteOffer.js');

        $request = new \Phalcon\Http\Request;

        if ($request->isGet() == true) {
            if ($this->basic->id == $request->getQuery('account_id')) {

                $scope = $this->calScope("today");
                $offer_id = $request->getQuery('offer_id');
                $this->useModel('ListingOffer');
                $offer = $this->ListingOffer->findListingOfferById($offer_id);
                $without_card = $offer->without_card;
                $this->useModel('ListingKeyword');
                $offer_job_category = $request->getQuery('offer_job_category');
                $keywords = $this->ListingKeyword->findKeywordsById($scope, $offer_id);
                
                $offer = $this->ListingOffer->findFirstById($offer_id);
                $expense_sum = 0;
                foreach ($keywords as $keyword) {
                    $expense_sum += $keyword->expense;
                }
                $this->view->without_card = $without_card;
                $this->view->job_category = $offer_job_category;
                $this->view->offer_id = $offer_id;
                $this->view->account_id = $request->getQuery('account_id');
                $this->view->keywords_count = count($keywords);
                $this->view->keywords = $keywords;
                $this->view->expense_sum = $expense_sum;
                $this->view->one_week_ago = date("Y/m/d", strtotime("-6 day"));
                $this->view->today = date("Y/m/d");
            } else {

                $this->response->redirect('customer/index');
            }
        }
    }
        /**
        *  ajax通信先
           listing/keyword , admin/allowedListingKeywords, /admin/accountListingKeywordsにおいて、
           対象期間を変更して「適用」を押下した際にしかるべきデータを表示
        */
    public function changeKeywordScopeAction() {

        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {

                $scope = $this->calScope($request->getQuery('keyword_scope'));

                $offer_id = $request->getQuery('offer_id');
                $this->useModel('ListingKeyword');
                $keywords = $this->ListingKeyword->findKeywordsById($scope, $offer_id);
                
                $expense_sum = 0;
                foreach($keywords as $keyword) {
                       $expense_sum += $keyword->expense;
                }
                $data = array("keywords" => $keywords, "expense_sum" => $expense_sum);
                header('Content-Type:application/json');
                echo json_encode($data);
            }
        }
    }


         /**
        *  ajax通信先
           listing/keywordにおいて、
           該当する検索ワード(listing_keywordsテーブル)のstatusカラムをcanceledに変更
        */
    public function cancelKeywordAction() {

        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
                $this->useModel('ListingKeyword');
                $keyword = $this->ListingKeyword->findListingKeywordById($request->getQuery('keyword_id'));
                $keyword->status = "canceled";
                $keyword->updateColumn();
                $data = array("keyword_id" => $keyword->id);
                header('Content-Type:application/json');
                echo json_encode($data);
            }
        }
    }
        /**
        *  ajax通信先
           listing/keywordにおいて、
           該当する検索ワード(listing_keywordsテーブル)のstatusカラムをpublishingに変更
        */
    public function effectKeywordAction() {

        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {

                $this->useModel('ListingOffer');
                $offer = $this->ListingOffer->findListingOfferById($request->getQuery('offer_id'));
                if ($offer->status == 'publishing' || $offer->status == 'waiting_card') {
                    $this->useModel('ListingKeyword');
                    $keyword = $this->ListingKeyword->findListingKeywordById($request->getQuery('keyword_id'));
                    $this->useModel('Account');
                    $account = $this->Account->findAccountForWebpay($this->basic->id);
                    //task
//                    if ($account->creditcard_id != "") {
                        $keyword->status = "publishing";
//                    } else {
//                        $keyword->status = "waiting_card";
//                    }
                    $keyword->updateColumn();
                    $data = array("keyword_id" => $keyword->id, "status" => $keyword->status, "offer_status" => $offer->status);
                } else {
                    $data = array("keyword_id" => 0, "status" => '', "offer_status" => $offer->status);
                }

                header('Content-Type:application/json');
                echo json_encode($data);
            }
        }
    }
        /**
        *  ajax通信先
           listing/keywordにおいて、
           該当する検索ワード(listing_keywordsテーブル)を論理削除(deletedカラムを1に変更)
        */
    public function deleteKeywordAction() {

        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);

        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
                $scope = $this->calScope($request->getQuery('scope'));
                $this->useModel('ListingKeyword');
                $keyword = $this->ListingKeyword->findListingKeywordById($request->getQuery('keyword_id'));
                $keyword->softDelete();
                $keywords = $this->ListingKeyword->findKeywordsById($scope, $request->getQuery('offer_id'));
                $expense_sum = $this->calExpenseSum($keywords);
                $count = count($keywords);
                $data = array("keyword_id" => $keyword->id, "keywords" => $keywords, "expense_sum" => $expense_sum, "count" => $count);
                header('Content-Type:application/json');
                echo json_encode($data);
            }
        }
    }
        
        /**
        *  ajax通信先
         　listing/make　及びlisting/editにおいて、画像登録済みの場合のみ表示される「画像を削除する」ボタンを押下した際に、
         　該当するリスティング広告のhas_picカラムを1から0へと変更し、その広告データが画像未登録のものとして扱われるようにする
        */
    public function deletePictureAction() {
        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);
        if ($request->isGet() == true) {
            if ($request->isAjax() == true) {
                $this->useModel('ListingOffer');
                $offer_id = $request->getQuery('offer_id');
                $offer = $this->ListingOffer->findOnConfirmListingOfferById($offer_id);
                $offer->has_pic = 0;
                $offer->updateColumn();  
            }
        }
    }
        
        /**
        *  ajax通信先
         　リスティング広告編集画面において、上限予算が本日既にクリックされて生じている費用を下回らないかをチェック
        */
    public function validateBudgetmaxAction() {
        $request = new Phalcon\Http\Request;
        $this->view->setRenderLevel(Phalcon\Mvc\View::LEVEL_NO_RENDER);
        $scope = $this->calScope("today");
        if ($request->isGet() == true) {
           if ($request->isAjax() == true) {
           $offer_id = $request->getQuery('offer_id');
           $this->useModel('ListingOffer');
           $offer = $this->ListingOffer->findFirstById($offer_id);
           $click_price_number = $request->getQuery('click_price_number');
           $budget_max_number = $request->getQuery('budget_max_number');
           $budget_max = $request->getQuery('budget_max');
           $this->useModel('ClickLog');
           $click_logs = $this->ClickLog->findClickLogs($offer, 'listing', $scope, false);
           $expense = $this->calExpense($click_logs);
           echo ($budget_max_number >= $expense + $click_price_number || $budget_max == "") ? 1:0;
               }
         }
     }    
     
    public function keywordsSampleAction() {
           //viewのみ　検索ワードの入力例を表示
    }
}
