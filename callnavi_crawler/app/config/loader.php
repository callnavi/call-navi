<?php

$loader = new \Phalcon\Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerDirs(
        array(
            $config->application->controllersDir,
            $config->application->controllersAdminDir,
            $config->application->controllersUserDir,
            $config->application->controllerTraitsDir,
            $config->application->modelsDir,
            $config->application->modelTraitsDir,
            $config->application->crawlerDir,
            $config->application->batchDir,
            $config->application->vendorDir,
            $config->application->tasksDir,
        )
)->register();
