<?php

return new \Phalcon\Config(array(
    'db_production' => array(
        'adapter' => 'Mysql',
        'host' => 'localhost',
        'username' => 'crawler',
        'password' => 'crawler@123',
        'dbname' => 'callnavi_db',
        'charset' => 'utf8',
    ),
	
    'db_crawler' => array(
        'adapter' => 'Mysql',
        'host' => 'localhost',
        'username' => 'crawler',
        'password' => 'crawler@123',
        'dbname' => 'callnavi_db',
        'charset' => 'utf8',
    ),

//    'db_test' => array(
//        'adapter'     => 'Mysql',
//        'host'        => '10.168.138.227',
//        'username'    => 'fireduser',
//        'password'    => 'ubPqNqAU',
//        'dbname'      => 'rec_db',
//    ),
    'db_development' => array(
        'adapter' => 'Mysql',
        'host' => 'localhost',
        'dbname' => 'callnavi',
        'username' => 'root',
        'password' => 'pass',
        'charset' => 'utf8',
    ),
    
    'db_test' => array(
        'adapter' => 'Mysql',
        'host' => 'localhost',
        'dbname' => 'callnavitest',
        'username' => 'root',
        'password' => 'pass',
        'charset' => 'utf8',
    ),
    
    'db_local_dev' => array(
        'adapter' => 'Mysql',
        'host' => 'localhost',
        'dbname' => 'callnavi',
        'username' => 'root',
        'password' => 'pass',
        'charset' => 'utf8',
    ),
    
    'application' => array(
        'controllersDir' => __DIR__ . '/../../app/controllers/',
        'controllersAdminDir' => __DIR__ . '/../../app/controllers/admin/',
        'controllersUserDir' => __DIR__ . '/../../app/controllers/user/',
        'controllerTraitsDir' => __DIR__ . '/../../app/controllers/traits/',
        'modelsDir' => __DIR__ . '/../../app/models/',
        'modelTraitsDir' => __DIR__ . '/../../app/models/traits/',
        'crawlerDir' => __DIR__ . '/../../app/models/crawlers/',
        'batchDir' => __DIR__ . '/../../app/models/batches/',
        'viewsDir' => __DIR__ . '/../../app/views/',
        'vendorDir' => __DIR__ . '/../../app/vendor/',
        'tasksDir' => __DIR__ . '/../../app/console/tasks/',
        'pluginsDir' => __DIR__ . '/../../app/plugins/',
        'libraryDir' => __DIR__ . '/../../app/library/',
        'cacheDir' => __DIR__ . '/../../app/cache/',
        'baseUri' => '/',
        'logDir' => __DIR__ . '/../../app/logs/',
    ),
    'mail' => array(
        'fromName' => 'CallNavi運営チーム',
        'fromMail' => 'info@callnavi.jp',
        'smtp' => array(
            'server' => 'localhost',
            'port' => 25,
//            'security' => 'ssl',
//            'username' => 'info@callnavi.jp',
//            'password' => 'password',
        ),
    ),
        ));

// 開発テスト
//const PUBLIC_KEY = "test_public_3Hu2so84D3TC8AJ80j9XX7Qa";
//const SECRET_KEY = "test_secret_eid6Br0ob2z70yaaKUarg9Zq";

// Wizさまアカウント　※開発用
//const PUBLIC_KEY = "test_public_du76K9gwd3r119k46W2QM2AC";
//const SECRET_KEY = "test_secret_6D38Bj3v5cBF9hYdfK7Nl7RM";

// Wizさまアカウント　※本番用
//const PUBLIC_KEY = "live_public_9fk2lXgEA31Yfut8vY9zq606";
//const SECRET_KEY = "live_secret_bET6VR9E3e4q9fof746IobTv";

// メタテスト環境用 公開可能鍵
//const PUBLIC_KEY = "test_public_0gnbCG5uW3Mrc2V9Nj210bb9";

// メタテスト環境用 非公開鍵
//const SECRET_KEY = "test_secret_gpl02c7Ns7nQ2w4bjPc3vaAy";


// メタ商用環境用 公開可能鍵
const PUBLIC_KEY = "live_public_0ov99oako5bn5lSfo460zc15";

// メタ商用環境用 非公開鍵
const SECRET_KEY = "live_secret_dMo0nX6lw0os5eAcDofOg55j";
