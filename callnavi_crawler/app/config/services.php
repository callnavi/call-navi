<?php

use Phalcon\DI\FactoryDefault;
use Phalcon\Mvc\View;
use Phalcon\Mvc\Url as UrlResolver;
use Phalcon\Db\Adapter\Pdo\Mysql as DbAdapter;
use Phalcon\Mvc\View\Engine\Volt as VoltEngine;
use Phalcon\Mvc\Model\Metadata\Memory as MetaDataAdapter;
use Phalcon\Session\Adapter\Files as SessionAdapter;

/**
 * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
 */
$di = new FactoryDefault();

/**
 * The URL component is used to generate all kind of urls in the application
 */
$di->set('url', function () use ($config) {
    $url = new UrlResolver();
    $url->setBaseUri($config->application->baseUri);

    return $url;
}, true);

/**
 * Setting up the view component
 */
$di->set('view', function () use ($config) {

    $view = new View();

    $view->setViewsDir($config->application->viewsDir);

    $view->registerEngines(array(
        '.volt' => function ($view, $di) use ($config) {

    $volt = new VoltEngine($view, $di);

    $volt->setOptions(array(
        'compiledPath' => $config->application->cacheDir,
        'compiledSeparator' => '_'
    ));

    return $volt;
},
        '.phtml' => 'Phalcon\Mvc\View\Engine\Php'
    ));

    return $view;
}, true);

/**
 * Database connection is created based in the parameters defined in the configuration file
 */
$di->set('db', function () use ($config) {
    if (
            $_SERVER['SERVER_NAME'] == 'callnavi.jp' or
            //開発用サーバ
            $_SERVER['SERVER_NAME'] == 'test.callnavi.jp' or
            $_SERVER['SERVER_ADDR'] == '10.0.177.93'
    ) {
        return new DbAdapter(array(
            'host' => $config->db_production->host,
            'username' => $config->db_production->username,
            'password' => $config->db_production->password,
            'dbname' => $config->db_production->dbname,
            'charset' => $config->db_production->charset,
        ));
    } elseif (
        
                $_SERVER['SERVER_NAME'] == 'local-callnavi.jp'
        ) {
            return new DbAdapter(array(
                'host' => $config->db_local_dev->host,
                'username' => $config->db_local_dev->username,
                'password' => $config->db_local_dev->password,
                'dbname' => $config->db_local_dev->dbname,
                'charset' => $config->db_local_dev->charset,
            ));
    } elseif (
            //クローラー用サーバ
            $_SERVER['SERVER_ADDR'] == '10.0.177.94'
    ) {
        return new DbAdapter(array(
            'host' => $config->db_crawler->host,
            'username' => $config->db_crawler->username,
            'password' => $config->db_crawler->password,
            'dbname' => $config->db_crawler->dbname,
            'charset' => $config->db_crawler->charset,
        ));
    } else {
        return new DbAdapter(array(
            'host' => $config->db_development->host,
            'username' => $config->db_development->username,
            'password' => $config->db_development->password,
            'dbname' => $config->db_development->dbname,
            'charset' => $config->db_development->charset,
        ));
    }
});


/**
 * If the configuration specify the use of metadata adapter use it or use memory otherwise
 */
$di->set('modelsMetadata', function () {
    return new MetaDataAdapter();
});

/**
 * Start the session the first time some component request the session service
 */
$di->set('session', function () {
    $session = new SessionAdapter();
    $session->start();

    return $session;
});

