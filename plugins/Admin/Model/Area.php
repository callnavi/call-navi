<?php
App::uses('AppModel', 'Model');
/**
 * ContentsEmail Model
 *
 */
class Area extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $useTable = 'area';
	
	
	public function getGroup()
	{
		$db = $this->getDataSource();
		
		$query = 'select id, name, alias, `group`, callcenter_amount from area where level=0 order by `group`, priority';
		
		return $db->fetchAll($query);
	}
	
	public function getCityByProvinceAlias($provinceAlias)
	{
		$db = $this->getDataSource();
		$query = 'select id, name, alias, `group` from area where level=0 order by `group`, priority';
		return $db->fetchAll($query);
	}

	//return city id and city name by province aliasname  
	public function getCityByAliasName($alias)
	{
		$db = $this->getDataSource();

		$query = 'SELECT id, name FROM area WHERE parent_id = (SELECT id FROM area WHERE alias = :alias)';
		return $db->fetchAll($query, array('alias' => $alias));
	}
}
