<?php
App::uses('AppModel', 'Model');
/**
 * ContentsEmail Model
 *
 */
class Feedback extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $useTable = 'feedback';
}
