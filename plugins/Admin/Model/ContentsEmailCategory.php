<?php
App::uses('AppModel', 'Model');
/**
 * ContentsEmail Model
 *
 */
class ContentsEmailCategory extends AdminAppModel {

/**
 * Display field
 *
 * @var string
 */
	public $useTable = 'contents_email_category';
	public $table = 'contents_email_category';

}
