<?php
App::uses('AppModel', 'Model');
/**
 * ContentsEmail Model
 *
 */
class BlogsCategory extends AppModel {

/**
 * Display field
 *
 * @var string
 */
	public $primaryKey = 'cat_id';
	public $useTable = 'blogs_category';
	
}
