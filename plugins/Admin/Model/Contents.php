<?php
App::uses('AppModel', 'Model');
/**
 * ContentsEmail Model
 *
 */
class Contents extends AdminAppModel {

/**
 * Display field
 *
 * @var string
 */
	public $useTable = 'contents';

}
