<?php
App::uses('AppModel', 'Model');
/**
 * ContentsEmail Model
 *
 */
class PickUp extends AppAdminModel {

/**
 * Display field
 *
 * @var string
 */
	public $useTable = 'pick_up';
	
	/**
	 * 
	 * @return array (
	 *                => NAME,
	 *                => URL,
	 *                => IMAGE,
	 *                => DATE,
	 *                => VIEW
	 *                )
	 */
	public function getPickUp()
	{
		$data =  $this->find('first', [
			'fields' => array('id','type','article_id','status'),
			'conditions' => array('status' => 1),
			'order' => array('modified DESC')
		]);

		if($data)
		{
			$pickup = $data['PickUp'];
			switch($pickup['type'])
			{
				//Type content
				case 1:
					$modelContent = ClassRegistry::init('Contents');
					$rsContent =  $modelContent->find('first', [
						 'fields' => array(
							 			   'id',
										   'priority',
										   'Contents.status',
										   'site_id',
										   'province_id',
										   'url',
										   'category',
										   'view', 
										   'title_hint',
										   'title',
										   'image',
										   'category_alias',
                    					   'title_alias',
							 			   'Contents.created','category.category',
                    					   'TIMESTAMPDIFF(hour, Contents.created, NOW() ) AS totalHour'
										  ),
						'conditions' => array('Contents.status' => 1,'Contents.id' => $pickup['article_id'] ),
						'joins' => array(
							array(
								'table' => 'category',
								'type' => 'LEFT',
								'conditions' => array(
									'category.category_alias = Contents.category_alias',
								),
							),
						)
					]);
					
					$image = $rsContent['Contents']['image'];
					if (!empty($image)){
						$pos = strpos($image, "ccwork");
						if($pos === false)
						{
							$image= '/upload/contents/'.$image;
						}							
					}
					else
					{
						$image = '/img/instead/img_no-image_' . sprintf("%02d", rand(1, 10)) . '.jpg';
					}
					$title = $rsContent['Contents']['title'];
					$url = '/contents/'
							.$rsContent['Contents']['category_alias']
							.'/'
							.$rsContent['Contents']['title_alias'];
					
					$createDate = $rsContent['Contents']['created'];
					$view = $rsContent['Contents']['view'];

					$id = $rsContent['Contents']['id'];
					$cat_names = $rsContent['category']['category'];
					$cat_alias = $rsContent['Contents']['category_alias'];
					$title_alias = $rsContent['Contents']['title_alias'];
					
					return array(
						'title' 	  => $title,
						'url' 		  => $url,
						'image' 	  => $image,
						'createDate'  => $createDate,
						'view'		  => $view,
						'id' 		  => $id,
						'cat_names'   => $cat_names,
						'cat_alias'   => $cat_alias,
						'title_alias' => $title_alias
					);
					
				//Type blog
				case 2:
					return [1];
					break;
				//Type news
				case 3:
					return [2];
					break;
			}
		}
		
		return null;
	}

}
