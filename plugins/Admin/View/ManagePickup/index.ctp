<?php
$this->set('title', 'コールナビ｜仕事管理');
$this->start('css'); ?>
<link rel="stylesheet" href="<?php echo $this->base; ?>/admin/css/common.css" type="text/css" media="all">
<link rel="stylesheet" href="<?php echo $this->base; ?>/admin/css/manage-pickup.css" type="text/css" media="all">
<?php $this->end('css'); ?>
<?php
$selectedBlogs = '';
$selectedContents = '';
if (!empty($params['type'])) {
    if ($params['type'] == 2)
        $selectedBlogs = 'selected';
    else
        $selectedContents = 'selected';
}
?>

<div id="manage-pickup" class="container-fluid">
    <div class="row">
        <div class="col-md-2 padding-right-10">
            <?php echo $this->element('../Elements/left_menu'); ?>
        </div>

        <div class="col-md-10 padding-left-10 padding-top-40">
            <label class="control-label" >Picked Up</label>
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <td width="100px"><a href="<?php echo $pickup['url']; ?>" target="_blank"><img
                                src='<?php echo $pickup['image']; ?>'></a></td>
                    <td class="current_pickup"><?php echo '<a href="' . $pickup['url'] . '" target="_blank" >' . $pickup['title'] . '</a>'; ?></td>
                    <td class="box-date"><?php echo $pickup['createDate']; ?></td>
                    <td width="100px"><b>Picked Up</b></td>
                </tr>
                </tbody>
            </table>

            <div class="search-form">
                <form method="get" action="/admin/ManagePickup" class="form-horizontal form-row-5">

                    <div class="col-md-7 padding-5">
                        <?php echo $this->Form->input('keywords',
                            array('class' => 'form-control',
                                'label' => false,
                                'div' => false,
                                'value' => !empty($params['keywords']) ? $params['keywords'] : '',
                                'id' => 'keywords',
                                'placeholder' => '検索キーワードを入力してください',
                                'name' => 'keywords'));
                        ?>
                    </div>

                    <div class="col-md-3 padding-5">
                        <select name="type" id="type" class="form-control">
                            <option value="">All</option>
                            <option value="1" <?php echo $selectedContents; ?> >Content</option>
                            <option value="2" <?php echo $selectedBlogs; ?> >Blog</option>
                        </select>
                    </div>

                    <div class="col-md-2 padding-5">
                        <input type="submit" class="btn btn-primary pull-right" value="検索"/>
                    </div>
                </form>

            </div>

            <table class="table table-bordered">
                <thead>
                <tr>
                    <th>画像</th>
                    <th>タイトル</th>
                    <th class="box-date">作成日</th>
                    <th class="box-last">....</th>
                </tr>
                </thead>
                <tbody>
                <?php
                if (!empty($arrListData)) {
                    foreach ($arrListData as $record):
                    
                        if ($type == 1) {
                            $record = $record['contents'];
                            $record['type'] = 1;
                        } else if ($type == 2) {
                            $record = $record['blogs'];
                            $record['type'] = 2;
                        } else {
                            $record = $record[0];
                        }
                        ?>
                        <tr>
                            <?php
                            $src = "";
                            if ($record['type'] == 1) {
                                if (!empty($record['image'])) {
                                    $image = $record['image'];
                                    $pos = strpos($image, "ccwork");
                                    if ($pos !== false)
                                        $src = $image;
                                    else
                                        $src = '/upload/contents/' . $image;
                                }
                            } else {
                                $src = '/upload/blogs/' . $record['image'];
                            }
                            ?>
                            <?php
                            if( $record['title'] <> ""){
                            ?>
                            <td width="100px"><img src='<?php echo "$src"; ?>'></td>
                            <td><?php echo $record['title']; ?></td>
                            <td class="box-date"><?php echo $record['created']; ?></td>
                            <td><?php
                                echo $this->Html->link('Pick Up',
                                    array(
                                        'controller' => 'ManagePickup',
                                        'action' => 'edit',
                                        $record['type'],
                                        $record['id']
                                    ),
                                    array(
                                        'confirm' => '本当にこれを更新したいですか？',
                                        'class' => 'btn btn-primary btn-xs'
                                    ));
                                ?>
                            </td>
                            <?php
                            }
                            ?>
                        </tr>
                        <?php
                    
                    endforeach;
                } else { ?>
                    <tr>
                        <td colspan="4">
                            <p>データなし</p>
                        </td>
                    </tr>
                <?php } ?>
                </tbody>
            </table>
            <?php echo $this->element('../Elements/custom_pagination',
                array('pageCount' => $pageCount,
                    'currentPage' => $currentPage
                )); ?>

        </div>


    </div>
</div>