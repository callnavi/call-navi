<?php
$this->set('title', 'コールナビ｜CC管理');
$this->start('css'); ?>
	<link rel="stylesheet" href="<?php echo $this->base;?>/admin/css/common.css" type="text/css" media="all">
	<link rel="stylesheet" href="<?php echo $this->base;?>/admin/css/manage-news.css" type="text/css" media="all">
<?php $this->end('css'); ?>

<div id="manage-news" class="container-fluid">
	<div class="row">
		<div class="col-md-2 padding-right-10">
			<?php echo $this->element('../Elements/left_menu'); ?> 
		</div>
		<div class="col-md-10 padding-left-10">
			<div class="col-md-12 no-padding">
				<a href="<?php echo $this->Html->url(array('controller' => 'ManageCallCenter', 'action' => 'add',  ), true);?>" class="btn btn-primary pull-right btn-top" >新規登録</a>
			</div>
			<div class="search-form">
				<?php echo $this->Form->create('SearchCallCenter', array('url' => 'index','class' => 'form-horizontal form-row-5', 'type' => 'get')); ?>
					<div class="col-md-4 padding-5">
						<?php echo $this->Form->input('keywords',
							array('class' => 'form-control',
								'label' => false,
								'div' => false,
								'value' =>!empty($keywords) ? $keywords : '',
								'id'    => 'keywords',
								'placeholder' =>'検索キーワードを入力してください'));
						?>
					</div>

					<div class="col-md-3 padding-5">
						<select name="province" id="province_id" class="form-control">
							<option  value="" >すべての都道府県</option>
							<?php foreach($province as $item):
								$row=$item['Area'];
								$selected = '';
								if($searchProvince == $row['id'])
									$selected = 'selected';
								?>
									<option <?php echo $selected;?>  value="<?php echo $row['id'];?>" ><?php echo $row['name'] ;?></option>
								<?php  endforeach ?>
						</select>
					</div>

					<div class="col-md-3 padding-5 ">
						<select  name="city_id" id="city_id" class="form-control">
						</select>
					</div>

					<div class="col-md-2 padding-5">
						<input type="submit" class="btn btn-primary pull-right" value="検索"/>
					</div>
				<?php echo $this->Form->end(null); ?>

			</div>
			<div class="col-md-12 no-padding">
				<p><?php echo $total ;?>件中 <?php echo $numberStartEnd['start'] ;?> - <?php echo $numberStartEnd['end'] ;?>件を表示</p>
			</div>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>会社名</th>
						<th>電話番号</th>
						<th>住所</th>
						<th>都道府県</th>
						<th class="box-date">区市町村</th>
						<th class="box-last">....</th>
					</tr>
				</thead>
				<tbody>
				<?php if(!empty($list)) {
					foreach ($list as $record):
						$post = $record['Callcenter'];
						$city = $record['Area']['name'];
						$provinces = $record['area_parent']['parent_name'];
						?>
						<tr>
							<td><?php echo $post['company_name']; ?></td>
							<td width="10%"><?php echo $post['tel']; ?></td>
							<td><?php echo $post['address']; ?></td>
							<td class="box-company"><?php echo $provinces; ?></td>
							<td class="box-date"><?php echo $city; ?></td>
							<td>
								<a type="button" class="btn btn-primary btn-xs" href="<?php echo $this->Html->url(array('controller' => 'ManageCallCenter', 'action' => 'edit', $post['id']), true); ?>">修正</a>
								
								
								<?php
									echo $this->Html->link('削除',
                                                           array(
                                                               'controller' =>'ManageCallCenter',
                                                               'action'     =>'delete',
                                                               
                                                               $post['id']
                                                           ),
                                                           array(
                                                               'confirm'=>'本当に削除しますか？',
                                                                'class'      =>'btn btn-default btn-xs'
                                                                ));
								?>
							</td>

						</tr>
					<?php endforeach;
				}else{ ?>
					<tr>
						<td colspan="6">
							<p>データなし</p>
						</td>
					</tr>
				<?php }?>
				</tbody>
			 </table>
			<?php echo $this->element('../Elements/pagination'); ?>
      </div>
   </div>
</div>

<script>
	$( document ).ready(function() {

		<?php
		$searchCity = !empty($searchCity) ? $searchCity : null;
		if(!empty($searchCity)){ ?>
			$( "#city_id").show();
		<?php }else{ ?>
			$( "#city_id").hide();
		<?php } ?>

		<?php if(!empty($searchProvince)){ ?>
			getListCityByAjax(<?php echo $searchProvince; ?> );
		<?php } ?>

		$( "#province_id" ).change(function() {
			$("#city_id option").remove()
			var listCity;
			$.ajax({
				url: "<?php echo $this->Html->url(array('controller' => 'ManageCallCenter', 'action' => 'getListCity'), true); ?>",
				data: { provinceId: $(this).val() },
				method: "post",
				async: false,
				//method: 'post',
				//dataType: 'json',
				success: function(result){
					listCity= $.parseJSON(result) ;
				},
				error: function(error){
					console.log(error);
				},
				complete: function(xhr,status){
					console.log(status);
				},
			});

			if(listCity.length >0){
				$( "#city_id").show();
				$("#city_id").append("<option value=''>すべての区市町村</option>");
				for (var x =0; x< listCity.length; x++){
					var id = listCity[x].Area.id ;
					var name =  listCity[x].Area.name;
					if( id == "<?php echo $searchCity ;?>")
						$("#city_id").append("<option selected value='"+id+"'>"+name+"</option>");
					else
						$("#city_id").append("<option value='"+id+"'>"+name+"</option>");
				}
			}else{
				$( "#city_id").hide();
			}
		});

	});

	function getListCityByAjax(val){
		var listCity;
		$.ajax({
			url: "<?php echo $this->Html->url(array('controller' => 'ManageCallCenter', 'action' => 'getListCity'), true); ?>",
			data: { provinceId: val },
			method: "post",
			async: false,
			//method: 'post',
			//dataType: 'json',
			success: function(result){
				listCity= $.parseJSON(result) ;
			},
			error: function(error){
				console.log(error);
			},
			complete: function(xhr,status){
				console.log(status);
			},
		});

		if(listCity.length >0){
			$( "#city_id").show();
			$("#city_id").append("<option value=''>すべての区市町村</option>");
			for (var x =0; x< listCity.length; x++){
				var id = listCity[x].Area.id ;
				var name =  listCity[x].Area.name;
				if( id == "<?php echo $searchCity ;?>")
					$("#city_id").append("<option selected value='"+id+"'>"+name+"</option>");
				else
					$("#city_id").append("<option value='"+id+"'>"+name+"</option>");
			}
		}else{
			$( "#city_id").hide();
		}

	}

</script>