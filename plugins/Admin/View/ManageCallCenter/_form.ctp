<?php
$post = !empty($item['Callcenter']) ? $item['Callcenter'] : null;
$btn = !empty($item['Callcenter']) ? '修正' : '新規登録';
$frmId = !empty($item['Callcenter']) ? 'ManagerCallCenterEditForm' : 'ManagerCallCenterAddForm';
$ajaxUrl = !empty($item['Callcenter']) ?
    $this->Html->url(array('controller' => 'ManageCallCenter', 'action' => 'edit', $post['id']), true) :
    $this->Html->url(array('controller' => 'ManageCallCenter', 'action' => 'add'), true);
$provinceId= !empty($item['area_parent']['province_id']) ? $item['area_parent']['province_id'] : null;;
//pr($item);die;
$this->start('css');
echo $this->Html->css('fileinput');
$this->end('css');

$this->start('script');
echo $this->Html->script('jquery.validate.min');
echo $this->Html->script('fileinput');?>
<script src="https://maps.google.com/maps/api/js?sensor=false&libraries=geometry"></script>
<?php $this->end('script');

?>
<div id="frm-callcenter" class="news-form-container">
    <?php echo $this->Form->create('ManagerCallCenter', array('type' => 'file','class' => 'form-horizontal')); ?>
    <label  class="control-title">CC管理</label>

    <input type="submit" class="btn btn-primary pull-right" value="<?php echo $btn ;?>"/>

    <div class="form-group">
        <label  class="control-label">会社名<span class="require">※必須</span></label>
        <?php echo $this->Form->input('company_name',
            array('class' => 'form-control',
                'label' => false,
                'value' =>$post['company_name'],
                'id'    => 'company_name',
                'placeholder' =>''));
        ?>
    </div>

    <div class="form-group">
        <label  class="control-label ">郵便番号</label>
        <?php echo $this->Form->input('post_code',
            array('class' => 'form-control',
                'label' => false,
                'value' =>$post['post_code'],
                'id'    => 'post_code',
                'placeholder' =>''));
        ?>
    </div>

    <div class="form-group">
        <label  class="control-label location">都道府県</label>
        <select name="data[ManagerCallCenter][province]" id="province_id" class="form-control">
            <option  value="" >すべての都道府県</option>
            <?php foreach($province as $pro):
                $row=$pro['Area'];
                if($provinceId == $row['id'] ){?>
                    <option  value="<?php echo $row['id'];?>" selected><?php echo $row['name'] ;?></option>
                <?php }else{ ?>
                    <option  value="<?php echo $row['id'];?>" ><?php echo $row['name'] ;?></option>
                <?php } endforeach ?>
        </select>

        <select name="data[ManagerCallCenter][city_id]" id="city_id" class="form-control">
        </select>
    </div>
    <div class="form-group">
        <label  class="control-label">住所<span class="require">※必須</span></label>
        <?php echo $this->Form->input('address',
            array('class' => 'form-control',
                'label' => false,
                'value' =>$post['address'],
                'id'    => 'address',
                'placeholder' =>''));
        ?>
    </div>

    <div class="form-group">
        <div class="width-50">
            <label  class="control-label">電話番号</label>
            <?php echo $this->Form->input('tel',
                array('class' => 'form-control',
                    'label' => false,
                    'value' =>$post['tel'],
                    'id'    => 'tel',
                    'placeholder' =>''));
            ?>
        </div>

        <div class="width-50">
            <label  class="control-label">ホームページ</label>
            <?php echo $this->Form->input('link',
                array('class' => 'form-control',
                    'label' => false,
                    'value' =>$post['link'],
                    'id'    => 'link',
                    'placeholder' =>''));
            ?>
        </div>
    </div>

    <div class="form-group">
        <div class="width-50">
            <label  class="control-label">業種</label>
            <?php echo $this->Form->input('business',
                array('class' => 'form-control',
                    'label' => false,
                    'value' =>$post['business'],
                    'id'    => 'business',
                    'placeholder' =>''));
            ?>
        </div>

        <div class="width-50">
            <label  class="control-label">メール</label>
            <?php echo $this->Form->input('contact',
                array('class' => 'form-control',
                    'label' => false,
                    'value' =>$post['contact'],
                    'id'    => 'contact',
                    'placeholder' =>''));
            ?>
        </div>
    </div>
	
    <div><label  class="control-label">マニュアル:</label> <a target="_blank" href="/img/callcenter/callcenter_manual.png">私をクリック</a></div>
    <a target="_blank" href="https://maps.google.co.jp/">Google マップ</a>
    <br/><br/>
	<div class="form-group">
			<div class="width-50">
				<label  class="control-label">LAT</label>
				<?php echo $this->Form->input('lat',
					array('class' => 'form-control',
						'label' => false,
						'value' => $post['lat'],
						'id'    => 'lat',
						'required'    => 'required',
						'placeholder' =>''));
				?>
			</div>
			
			<div class="width-50">
				<label  class="control-label">LNG</label>
				<?php echo $this->Form->input('lng',
					array('class' => 'form-control',
						'label' => false,
						'value' =>$post['lng'],
						'id'    => 'lng',
						'required'    => 'required',
						'placeholder' =>''));
				?>
			</div>
	</div>
   
    <div class="form-group">
        <input type="submit" class="btn btn-primary pull-right" value="<?php echo $btn ;?>"/>
        <input type="button" style="margin-right: 15px;" class="btn btn-primary pull-right btn-preview" value="SP-プレビュ" data-id="SP"/>
        <input type="button" style="margin-right: 15px;" class="btn btn-primary pull-right btn-preview" value="PC-プレビュ" data-id="PC"/>
    </div>

    <?php echo $this->Form->end(null); ?>
    
    <script>
        $( document ).ready(function() {
//Check validate form
            var form = $("#<?php echo $frmId; ?>");
            form.validate({
                errorPlacement: function errorPlacement(error, element) { element.before(error); },
                onkeyup: false,
                rules: {
                    "data[ManagerCallCenter][company_name]": {
                        required:true,
                    },
                    "data[ManagerCallCenter][address]": {
                        required:true,
                    },

                },

            });

            jQuery.extend(jQuery.validator.messages, {
                required: "この項目は必須です。",
            });

            $("#img").fileinput({
                initialPreview: [ <?php if(!empty($post['img'])){ ?> '<img src="/upload/call_center/<?php echo $this->base.$post['img'] ?>" class="" >' <?php } ?>],
                showUpload: false,
                showRemove: false,
                maxFileSize: 1024,
                msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
                allowedFileExtensions: ["jpg", "gif", "png"],
                overwriteInitial: true,
                allowedFileTypes: ["image"],
                previewFileType: ["image"]
            });

            // get city list
            <?php
            if(!empty($post['city_id'])){ ?>
                $( "#city_id").show();
            <?php }else{ ?>
                $( "#city_id").hide();
            <?php } ?>

            <?php if(!empty($provinceId)){ ?>
                getListCityByAjax(<?php echo $provinceId; ?> );
            <?php } ?>

            $( "#province_id" ).change(function() {
                $("#city_id option").remove()
                var listCity;
                $.ajax({
                    url: "<?php echo $this->Html->url(array('controller' => 'ManageCallCenter', 'action' => 'getListCity'), true); ?>",
                    data: { provinceId: $(this).val() },
                    method: "post",
                    async: false,
                    //method: 'post',
                    //dataType: 'json',
                    success: function(result){
                        listCity= $.parseJSON(result) ;
                    },
                    error: function(error){
                        console.log(error);
                    },
                    complete: function(xhr,status){
                        console.log(status);
                    },
                });

                if(listCity.length >0){
                    $( "#city_id").show();
                    $("#city_id").append("<option value=''>すべての区市町村</option>");
                    for (var x =0; x< listCity.length; x++){
                        var id = listCity[x].Area.id ;
                        var name =  listCity[x].Area.name;
                        if( id == "<?php echo $post['city_id'] ;?>")
                            $("#city_id").append("<option selected value='"+id+"'>"+name+"</option>");
                        else
                            $("#city_id").append("<option value='"+id+"'>"+name+"</option>");
                    }
                }else{
                    $( "#city_id").hide();
                }
            });

            $('.btn-preview').click(function () {
                if ($('#<?php echo $frmId; ?>').valid()) {
                    var input = $("<input>").attr("name", "isPreview").hide().val("1");
                    //kind preview PC or SP
                    var kindPreview = $(this).attr('data-id');
                    $('#<?php echo $frmId; ?>').append(input);
                    $.ajax({
                        url: "<?php echo $ajaxUrl ;?>",
                        async: false,
                        type: "post",
                        data: $("#<?php echo $frmId; ?>").serialize(),
                        success: function () {
                            if(kindPreview == 'PC')
                            {
                                window.open(
                                    document.location.origin + "/callcenter_matome/preview/<?php echo !empty($item['Callcenter']['id']) ?
                                $item['Callcenter']['id'] : $lastId ; ?>/"+ kindPreview, '_blank'
                                );
                            }
                            else
                            {
                                window.open(
                                    document.location.origin + "/callcenter_matome/preview/<?php echo !empty($item['Callcenter']['id']) ?
                                $item['Callcenter']['id'] : $lastId ; ?>/"+ kindPreview, '_blank', "width=414,height=650,resizeable,scrollbars"
                                );
                            }
                            <?php if(empty($item['Callcenter'])){ ?>

                            window.location.href = document.location.origin + "/admin/ManageCallCenter/edit/<?php echo $lastId;?>";

                            <?php } ?>

                        }
                    });
                }
            });
        });

        function getListCityByAjax(val){
            var listCity;
            $.ajax({
                url: "<?php echo $this->Html->url(array('controller' => 'ManageCallCenter', 'action' => 'getListCity'), true); ?>",
                data: { provinceId: val },
                method: "post",
                async: false,
                //method: 'post',
                //dataType: 'json',
                success: function(result){
                    listCity= $.parseJSON(result) ;
                },
                error: function(error){
                    console.log(error);
                },
                complete: function(xhr,status){
                    console.log(status);
                },
            });

            if(listCity.length >0){
                $( "#city_id").show();
                $("#city_id").append("<option value=''>すべての区市町村</option>");
                for (var x =0; x< listCity.length; x++){
                    var id = listCity[x].Area.id ;
                    var name =  listCity[x].Area.name;
                    if( id == "<?php echo $post['city_id'] ;?>")
                        $("#city_id").append("<option selected value='"+id+"'>"+name+"</option>");
                    else
                        $("#city_id").append("<option value='"+id+"'>"+name+"</option>");
                }
            }else{
                $( "#city_id").hide();
            }

        }
    </script>


</div>