<?php
$this->set('title', 'コールナビ｜Login');
$this->start('css'); ?>
<link rel="stylesheet" href="<?php echo $this->base;?>/admin/css/login.css" type="text/css" media="all">
<?php $this->end('css');

$this->start('script');
echo $this->Html->script('jquery.validate.min');
$this->end('script');

?>



<div class="login-container">

	<?php echo $this->Form->create('Login', array('class' => 'form-horizontal')); ?>

		<div class="form-group">
			<label for="email" class="control-label">メールアドレス <span class="require">必須</span></label>
			<?php echo $this->Form->input('email',
				array('class' => 'form-control',
					'label' => false,
					'div' => false,
					'id'    => 'email',
					'placeholder' => 'mail@address.com')); ?>
		</div>

		<div class="form-group">
			<label for="password" class="control-label">Passwords <span class="require">必須</span></label>
			<?php echo $this->Form->input('password',
				array('class' => 'form-control',
					'label' => false,
					'div' => false,
					'id'    => 'password',
					'placeholder' =>'Password'));
			?>
			<?php if(!empty($errorMess)){ ?>
				<label  generated="true" class="error"><?php echo $errorMess ;?></label>
			<?php } ?>
		</div>

		<div class="form-group">
			<input type="submit" class="btn btn-primary pull-right" value="Login"/>
		</div>

	<?php echo $this->Form->end(null); ?>

</div>

<script>
	$( document ).ready(function() {

//Check validate form
		var form = $("#LoginLoginForm");
		form.validate({
			errorPlacement: function errorPlacement(error, element) { element.before(error); },
			onkeyup: false,
			rules: {
				'data[Login][email]': {
					required: true,
					email: true
				},
				'data[Login][password]': {
					required:true,
				},
			},

		});

		jQuery.extend(jQuery.validator.messages, {
			required: "この項目は必須です。",
			email: "メールアドレスが不正です。",
		});

	});
</script>