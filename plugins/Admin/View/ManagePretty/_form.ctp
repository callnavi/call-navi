<?php
    $post = !empty($this->request->data['Pretty']) ? $this->request->data['Pretty'] : null;
    $btn = !empty($this->request->data['Pretty']) ? '修正' : '新規登録';
?>
<div id="edit-blogs-categoy">
    <?php echo $this->Form->create('Pretty', array('type'=>'file','class' => 'pretty_form')); ?>
    <label  class="control-title">美女美男図鑑</label>

    <div class="form-group" >
        <label class="control-label">憧れの人<span class="require">※必須</span></label>
        <?php
        echo $this->Form->input('name', array(
            'class'=>'form-control',
            'id'=>'name',
            'label' => false,
            'div'=>false,
            'required' => true,
            'value' =>$post['name'],));
        ?>
    </div>

    <div class="form-group" >
        <label class="control-label">イニシャル</label>
        <?php
        echo $this->Form->input('nick_name', array(
          'class'=>'form-control',
          'id'=>'nick_name',
          'div'=>false,
          'label' => false,
          'value' =>$post['nick_name'],));
        ?>
    </div>

    <div class="form-group" >
        <label class="control-label">勤務地（会社名）<span class="require">※必須</span></label>
        <?php
        echo $this->Form->input('company_name', array(
            'class'=>'form-control',
            'id'=>'company_name',
            'div'=>false,
            'label' => false,
            'required' => true,
            'value' =>$post['company_name'],));
        ?>
    </div>

    <div class="form-group">
        <label class="control-label">写真01（最大サイズ：1メガバイト）</label>
        <?php echo $this->Form->input('pic_1',
            array('type' => 'file',
                'class' => 'form-control',
                'label' => false,
                'div'=>false,
                'id' => 'pic_1')); ?>
    </div>


    <div class="form-group">
        <label class="control-label">写真02（最大サイズ：1メガバイト）</label>
        <?php echo $this->Form->input('pic_2',
            array('type' => 'file',
                'class' => 'form-control',
                'label' => false,
                'div'=>false,
                'id' => 'pic_2')); ?>
    </div>

    <div class="form-group" >
       <p><label class="control-label">生年月日</label></p>
        <?php
        echo $this->Form->input('birthday', array(
          'class'=>'selectpicker',
          'type' => 'date',
          'id'=>'birthday',
          'dateFormat' => 'DMY',
          'empty' => array(
            'month' => '月',
            'day'   => '日',
            'year'  => '年'
          ),
          'minYear' => date('Y')-70,
          'maxYear' => date('Y'),
          'div'=>false,
          'label' => false,
          'value' =>$post['birthday'],));
        ?>
    </div>

    <div class="form-group input-gender">
        <p><label class="control-label">性別</label></p>
        <?php
        $options=array('男'=>'男','女'=>'女');
        $attributes=array('legend'=>false, 'value' => (!empty($this->request->data['Pretty']['gender'])) ? $this->request->data['Pretty']['gender'] : '男' );
        echo $this->Form->radio('gender',$options,$attributes);
        ?>
    </div>
    <div class="form-group" >
        <label class="control-label">出身地</label>
        <?php
        echo $this->Form->input('address', array(
          'class'=>'form-control',
          'id'=>'address',
          'div'=>false,
          'label' => false,
          'value' =>$post['address'],));
        ?>
    </div>
    <div class="form-group" >
        <label class="control-label">PV</label>
        <?php
        echo $this->Form->input('point_pv', array(
          'class'=>'form-control',
          'id'=>'point_pv',
          'div'=>false,
          'label' => false,
          'value' =>$post['point_pv'],));
        ?>
    </div>

	
	<div class="form-group input-gender">
        <p><label class="control-label">新ラベル</label></p>
        <?php
        $options=array('0'=>'いいえ','1'=>'はい');
        $attributes=array('legend'=>false, 'value' => (!empty($this->request->data['Pretty']['is_new'])) ? 
						  								$this->request->data['Pretty']['is_new'] : '0' );
        echo $this->Form->radio('is_new',$options,$attributes);
        ?>
    </div>

    <div class="form-group input-gender">
        <p><label class="control-label">Is Active Button Link</label></p>
        <?php
        $options=array('0'=>'OFF','1'=>'ON');
        $attributes=array('legend'=>false, 'value' => (!empty($this->request->data['Pretty']['is_active'])) ?
            $this->request->data['Pretty']['is_active'] : '0' );
        echo $this->Form->radio('is_active',$options,$attributes);
        ?>
    </div>

    <div class="form-group" >
        <label class="control-label">フリーワード</label>
        <?php
        echo $this->Form->input('pretty_keyword', array(
            'class'=>'form-control',
            'id'=>'pretty_keyword',
            'div'=>false,
            'label' => false,
            'value' =>$post['pretty_keyword'],));
        ?>
        <p id="count_job_secret"></p>
    </div>


    <div class="form-group">
        <input type="submit" class="btn btn-primary pull-right" value="<?php echo $btn ;?>"/>
    </div>


    <?php
    echo $this->Form->end();
    ?>





</div>

<script>
    $( document ).ready(function() {


        $("#pic_1").fileinput({
            initialPreview: [ <?php if(!empty($post['pic_1'])){ ?> '<img src="/upload/pretty/<?php echo $post['pic_1'] ?>" class="image" >' <?php } ?>],
            showUpload: false,
            showRemove: false,
            maxFileSize: 2048,
            msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
            allowedFileExtensions: ["jpg", "gif", "png"],
            overwriteInitial: true,
            allowedFileTypes: ["image"],
            previewFileType: ["image"]
        });

        $("#pic_2").fileinput({
            initialPreview: [ <?php if(!empty($post['pic_2'])){ ?> '<img src="/upload/pretty/<?php echo $post['pic_2'] ?>" class="image" >' <?php } ?>],
            showUpload: false,
            showRemove: false,
            maxFileSize: 1024,
            msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
            allowedFileExtensions: ["jpg", "gif", "png"],
            overwriteInitial: true,
            allowedFileTypes: ["image"],
            previewFileType: ["image"]
        });
        $( "#pretty_keyword" ).keyup(function() {
            var _text = $(this).val();
            var _count_job_secret = $('#count_job_secret');
            if(_text != '' && _text != undefined) {
                get_total_secret(_text,_count_job_secret );
            }else {
                _count_job_secret.html('There is no jobs with this keywords')
            }
        });

        get_total_secret($( "#pretty_keyword" ).val(), $('#count_job_secret') );

    });

    function get_total_secret(_text,_count_job_secret) {
        $.ajax({
            url: "/secret/countcondition",
            data: {keyword: _text},
            method: "GET",
            async: false,
            //dataType: 'json',
            success: function (result) {
                console.log(result);
                var count = result == 0 ? 'no jobs' : result+' jobs';
                _count_job_secret.html('There is '+count+' with this keywords')
            },
            error: function (error) {
                console.log(error);
            },
            complete: function (xhr, status) {
                //console.log(status);
            }
        });
    }
</script>
