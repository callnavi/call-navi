﻿<?php
$this->set('title', '美女美男図鑑｜ブログカテゴリー管理');

$this->start('css'); ?>
	<link rel="stylesheet" href="<?php echo $this->base;?>/admin/css/common.css" type="text/css" media="all">
<link rel="stylesheet" href="<?php echo $this->base;?>/admin/css/pretty.css" type="text/css" media="all">
<?php $this->end('css'); ?>

<div id="manage-news" class="container">
	<div class="row">
		<div class="col-md-3 padding-right-10">
			<?php echo $this->element('../Elements/left_menu'); ?> 
		</div>
		<div class="col-md-9 padding-left-10">
            <form method="POST" action="" accept-charset="UTF-8" enctype="multipart/form-data">
                <div class="title"><strong>■CSVを選択してアップロードしてください</strong></div>
                <div class="form-group">
                    <input name="csv" required type="file" class="form-control">
                </div>
                <?php if(!empty($status)){?>
                    <div class="form-group">
                        <?php echo $status; ?>
                    </div>
                <?php } ?>
                <div class="div-submit">
                    <button type="submit" class="btn btn-primary btn-sm">アップロード</button>
                </div>
            </form>
      <br>
          <p>Guide:</p>
          <p class="text-center"><img src="/plugins/Admin/webroot/images/example.png" </p>
      </div>
   </div>
</div>