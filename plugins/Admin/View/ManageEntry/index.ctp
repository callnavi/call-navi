<?php
$this->set('title', 'コールナビ｜会社管理');
$this->start('css'); ?>
	<link rel="stylesheet" href="<?php echo $this->base;?>/admin/css/common.css" type="text/css" media="all">
	<link rel="stylesheet" href="<?php echo $this->base;?>/admin/css/manage-news.css" type="text/css" media="all">
<?php $this->end('css'); ?>

<div id="manage-news" class="container-fluid">
	<div class="row">
		<div class="col-md-2 padding-right-10">
			<?php echo $this->element('../Elements/left_menu'); ?> 
		</div>

		<div class="col-md-10 padding-left-10">
			<div class="col-md-12 no-padding">
				<a href="<?php echo $this->Html->url(array('controller' => 'ManageEntry', 'action' => 'add',  ), true);?>" class="btn btn-primary pull-right btn-top" >新規登録</a>
                
                <div class="search-form">
                    <?php echo $this->Form->create('SearchEntry', array('url' => 'index','class' => 'form-horizontal form-row-5', 'type' => 'get')); ?>
                    <div class="col-md-10 padding-5">
                        <?php echo $this->Form->input('keywords',
                            array('class' => 'form-control',
                                'label' => false,
                                'div' => false,
                                'value' =>!empty($keywords) ? $keywords : '',
                                'id'    => 'keywords',
                                'placeholder' =>'検索キーワードを入力してください'));
                        ?>
                    </div>

                    

                    <div class="col-md-2 padding-5">
                        <input type="submit" class="btn btn-primary pull-right" value="検索"/>
                    </div>
                    <?php echo $this->Form->end(null); ?>

                </div>            
            </div>

            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th width="200px">管理ID</th>
                        <th width="150px">応募日付</th>
                        <th>氏名</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(!empty($listEntry)): ?>
                        <?php foreach ($listEntry as $entry): ?>
                            <?php $entry = $entry['Entry']; ?>
                            <tr>
                                <td><?php echo $entry['apply_code'] ?></td>
                                <td><?php echo $entry['created'] ?></td>
                                <td><?php echo $entry['full_name'] ?></td>
                                <td><a class="btn btn-primary btn-xs" href="/admin/ManageEntry/view/<?php echo $entry['id']; ?>">詳細を見</a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr>
                            <td colspan="5">
                                <p>データなし</p>
                            </td>
                        </tr>
                    <?php endif; ?>
                </tbody>
             </table>


             <?php echo $this->element('../Elements/pagination'); ?>

        </div>
    </div>
</div>
