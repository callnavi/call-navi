<?php
$this->set('title', 'コールナビ｜会社管理');
$this->start('css'); ?>
	<link rel="stylesheet" href="<?php echo $this->base;?>/admin/css/common.css" type="text/css" media="all">
	<link rel="stylesheet" href="<?php echo $this->base;?>/admin/css/manage-entry.css" type="text/css" media="all">
<?php $this->end('css'); ?>
<?php
        $classification="";
    if(isset($entry['apply_code']) || $entry['apply_code'] <> ""){
        $classification = $this->Code->genClassificationData($entry['apply_code']);
    }
?>
<div id="manage-entry" class="container-fluid">
	<div class="row">
		<div class="col-md-2 padding-right-10">
			<?php echo $this->element('../Elements/left_menu'); ?> 
		</div>

		<div class="col-md-10 padding-left-10 padding-top-40">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th colspan="2">管理ID:<?php echo $entry['apply_code']; ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(!empty($entry)): ?>
                        <tr>
                            <td width="150px;">お名前</td>
                            <td><?php echo $entry['full_name']; ?></td>
                        </tr>
                        <tr>
                            <td width="150px;">ふりがな</td>
                            <td><?php echo $entry['phonetic_name']; ?></td>
                        </tr>
                        <tr>
                            <td width="150px;">生年月日</td>
                            <td><?php echo $entry['birth_date']; ?></td>
                        </tr>
                        <tr>
                            <td width="150px;">性別</td>
                            <td><?php echo $entry['gender']; ?></td>
                        </tr>
                        <tr>
                            <td width="150px;">電話番号</td>
                            <td><?php echo $entry['phone_number']; ?></td>
                        </tr>
                        <tr>
                            <td width="150px;">メールアドレス</td>
                            <td><?php echo $entry['e-mail']; ?></td>
                        </tr>
                        <tr>
                            <td width="150px;">履歴書</td>
                            <td><a href="/admin/ManageEntry/download_cv1/<?php echo $entry['id']; ?>"><?php echo $entry['cv1_name']; ?></td>
                        </tr>
                        <tr>
                            <td width="150px;">職務経歴書</td>
                            <td><a href="/admin/ManageEntry/download_cv2/<?php echo $entry['id']; ?>"><?php echo $entry['cv2_name']; ?></a></td>
                        </tr>
                        <tr>
                            <td width="150px;">スキル</td>
                            <td><?php echo $entry['skill']; ?></td>
                        </tr>
                        <tr>
                            <td width="150px;">勤務可能時間</td>
                            <td><?php echo $entry['working_time']; ?></td>
                        </tr>
                        <tr>
                            <td width="150px;">種別</td>
                            <td><?php echo $classification; ?></td>
                        </tr>
                        <tr>
                            <td width="150px;">備考欄</td>
                            <td><?php echo $entry['content']; ?></td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
            <div class="text-center"><a class="btn btn-primary" href="/admin/ManageEntry">戻る</a></div>
        </div>
    </div>
</div>
