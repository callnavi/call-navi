<?php
$this->set('title', 'コールナビ｜ブログカテゴリー管理');
$this->start('css'); ?>
    <link rel="stylesheet" href="<?php echo $this->base;?>/admin/css/common.css" type="text/css" media="all">
    <link rel="stylesheet" href="<?php echo $this->base;?>/admin/css/blogs.css" type="text/css" media="all">
    <link rel="stylesheet" href="<?php echo $this->base;?>/admin/css/manage-blogs-categoy.css" type="text/css" media="all">
<?php $this->end('css'); ?>

<div id="manage-blogs-categoy-edit" class="container-fluid">
    <div class="row">
        <div class="col-md-2 padding-right-10">
            <?php echo $this->element('../Elements/left_menu'); ?>
        </div>
        <div class="col-md-8 padding-left-10">
            <div id="edit-blogs-categoy" ><?php echo $this->element('../BlogsCategory/_form'); ?>
                <br>
            </div>
        </div>
    </div>
</div>
