﻿<?php
$this->set('title', 'コールナビ｜ブログカテゴリー管理');
$this->start('css'); ?>
	<link rel="stylesheet" href="<?php echo $this->base;?>/admin/css/common.css" type="text/css" media="all">
	<link rel="stylesheet" href="<?php echo $this->base;?>/admin/css/blogs.css" type="text/css" media="all">
<?php $this->end('css'); ?>

<div id="manage-news" class="container-fluid">
	<div class="row">
		<div class="col-md-2 padding-right-10">
			<?php echo $this->element('../Elements/left_menu'); ?> 
		</div>

		<div class="col-md-10 padding-left-10">
			<div class="div_btn_post">
				<a href="<?php echo $this->Html->url(array('controller' => 'BlogsCategory', 'action' => 'add',  ), true);?>" class="btn btn-primary pull-right btn-top" >新規登録</a>
				<div class="clearfix"></div>
			</div>

			<div class="search-form">
				<?php echo $this->Form->create('SearchBlogCategory', array('url' => 'index','class' => 'form-horizontal form-row-5', 'type' => 'get')); ?>
				<div class="col-md-10 padding-5">
					<?php echo $this->Form->input('keywords',
						array('class' => 'form-control',
							'label' => false,
							'div' => false,
							'value' =>!empty($keywords) ? $keywords : '',
							'id'    => 'keywords',
							'placeholder' =>'検索キーワードを入力してください'));
					?>
				</div>

				<div class="col-md-2 padding-5">
					<input type="submit" class="btn btn-primary pull-right" value="検索"/>
				</div>
				<?php echo $this->Form->end(null); ?>

			</div>
			<div class="col-md-12 no-padding">
				<p><?php echo $total ;?>件中 <?php echo $numberStartEnd['start'] ;?> - <?php echo $numberStartEnd['end'] ;?>件を表示</p>
			</div>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th width="20%">タイトル</th>
						<th>簡単な説明</th>
						<th class="box-date">作成日</th>
						<th class="box-last">....</th>
					</tr>
				</thead>
				<tbody>
					<?php
					if(!empty($list)) {
						foreach ($list as $record):
							$post = $record['BlogsCategory'];
							$created = strtotime($post['created']) ?>
							<tr>
								<td><?php echo $post['title']; ?></td>
								<td><?php echo $post['short']; ?></td>
								<td class="box-date"><?php echo $post['created']; ?></td>
								<td>
									<a type="button" class="btn btn-primary btn-xs" href="<?php echo $this->Html->url(array('controller' => 'BlogsCategory', 'action' => 'edit', $post['cat_id']), true); ?>">修正</a>
									<?php
									echo $this->Html->link('削除',
                                                           array(
                                                               'controller' =>'BlogsCategory',
                                                               'action'     =>'delete',
                                                               
                                                               $post['cat_id']
                                                           ),
                                                           array(
                                                               'confirm'=>'本当に削除しますか？',
                                                                'class'      =>'btn btn-default btn-xs'
                                                                ));
								?>
								</td>

							</tr>
						<?php endforeach;
					}else{?>
						<tr>
							<td colspan="4">
								<p>データなし</p>
							</td>
						</tr>
					<?php }?>

				</tbody>
			 </table>
            <?php echo $this->element('../Elements/pagination'); ?>
      </div>
   </div>
</div>