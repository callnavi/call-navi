﻿<?php
    $post = !empty($item['BlogsCategory']) ? $item['BlogsCategory'] : null;
    $btn = !empty($item['BlogsCategory']) ? '修正' : '新規登録';
    $frmId = !empty($item['BlogsCategory']) ? 'BlogsCategoryEditForm' : 'BlogsCategoryAddForm';

    $this->start('css');
        echo $this->Html->css('fileinput');
        echo $this->element('../Elements/froala_editor_css');
    $this->end('css');

    $this->start('script');
        echo $this->Html->script('jquery.validate.min');
        echo $this->Html->script('fileinput');
        echo $this->element('../Elements/froala_editor_js');
    $this->end('script');
?>
<div id="edit-blogs-categoy">
    <?php echo $this->Form->create('BlogsCategory', array('type'=>'file','class' => ''));
    ?>
    <label  class="control-title">ブログカテゴリー管理</label>

    <input type="submit" class="btn btn-primary pull-right" value="<?php echo $btn ;?>"/>

    <div class="form-group" >
        <label class="control-label">タイトル<span class="require">※必須</span></label>
        <?php
        echo $this->Form->input('title', array(
            'class'=>'form-control',
            'id'=>'title',
            'label' => false,
            'div'=>false,
            'value' =>$post['title'],));
        ?>
    </div>

    <div class="form-group" >
        <label class="control-label">カテゴリーエイリアス <span class="require">※必須</span></label>
        <?php
        echo $this->Form->input('category_alias', array(
            'class'=>'form-control',
            'id'=>'category_alias',
            'div'=>false,
            'label' => false,
            'value' =>$post['category_alias'],));
        ?>
    </div>

    <div class="form-group">
        <label class="control-label">画像（最大サイズ：1メガバイト）</label>
        <?php echo $this->Form->input('pic',
            array('type' => 'file',
                'class' => 'form-control',
                'label' => false,
                'div'=>false,
                'id' => 'pic')); ?>
    </div>


    <div class="form-group">
        <label class="control-label">アバター（最大サイズ：1メガバイト）</label>
        <?php echo $this->Form->input('avatar',
            array('type' => 'file',
                'class' => 'form-control',
                'label' => false,
                'div'=>false,
                'id' => 'avatar')); ?>
    </div>

    <div class="form-group" >
        <label class="control-label">好きな色</label>
        <?php
        echo $this->Form->input('color', array(
          'class'=>'form-control',
          'id'=>'color',
          'div'=>false,
          'label' => false,
          'value' =>$post['color'],));
        ?>
    </div>
    <div class="form-group" >
        <label class="control-label">座右の銘</label>
        <?php
        echo $this->Form->input('motto', array(
          'class'=>'form-control',
          'id'=>'motto',
          'div'=>false,
          'label' => false,
          'value' =>$post['motto'],));
        ?>
    </div>
    <div class="form-group" >
        <label class="control-label">興味があるもの</label>
        <?php
        echo $this->Form->input('interested', array(
          'class'=>'form-control',
          'id'=>'interested',
          'div'=>false,
          'label' => false,
          'value' =>$post['interested'],));
        ?>
    </div>


    <div class="form-group">
        <label class="control-label">簡単な説明</label>
        <?php
        echo $this->Form->textarea('short', array(
            'class'=>'form-control froalaEditor',
            'id'=>'short',
            'value' =>$post['short'],
            'rows'=> 7));
        ?>
    </div>

    <div class="form-group">
        <label class="control-label">説明</label>
        <?php
        echo $this->Form->textarea('description', array(
            'class'=>'form-control froalaEditor','id'=>'description',
            'value' =>$post['description'],
            'rows'=> 7,));
        ?>
    </div>


    <div class="seo">
        <div class="title">Search Engine Optimization</div>

        <div class="form-group" >
            <label class="control-label">Friendly Title</label>
            <?php
            echo $this->Form->input('friendly_title', array(
              'class'=>'form-control',
              'id'=>'friendly_title',
              'label' => false,
              'div'=>false,
              'value' =>$post['friendly_title'],));
            ?>
            <p>Title display in search engines is limited to 70 chars.</p>
        </div>
        <div class="form-group" >
            <label class="control-label">Meta Description</label>
            <?php
            echo $this->Form->textarea('metadesc', array(
              'class'=>'form-control',
              'id'=>'metadesc',
              'rows' => 6,
              'label' => false,
              'div'=>false,
              'value' =>$post['metadesc'],));
            ?>
            <p>The meta description will be limited to 155 chars (because of date display).</p>
        </div>
        <div class="form-group" >
            <label class="control-label">Meta Keyword</label>
            <?php
            echo $this->Form->input('metakey', array(
              'class'=>'form-control',
              'id'=>'metakey',
              'label' => false,
              'div'=>false,
              'value' =>$post['metakey'],));
            ?>
        </div>

    </div>


    <div class="form-group">
        <input type="submit" class="btn btn-primary pull-right" value="<?php echo $btn ;?>"/>
    </div>


    <?php
    echo $this->Form->end();
    ?>





</div>

<script>
    $( document ).ready(function() {

//Check validate form
        var form = $("#<?php echo $frmId; ?>");
        form.validate({
            errorPlacement: function errorPlacement(error, element)
            { element.prev().after(error); },
            onkeyup: false,
            rules: {
                "data[BlogsCategory][title]": {
                    required:true,
                },
                "data[BlogsCategory][category_alias]":  {
                    required:true,
                }

            },

        });

        jQuery.extend(jQuery.validator.messages, {
            required: "この項目は必須です。",
        });

        $("#pic").fileinput({
            initialPreview: [ <?php if(!empty($post['pic'])){ ?> '<img src="/upload/blogs-category/<?php echo $this->base.$post['pic'] ?>" class="" >' <?php } ?>],
            showUpload: false,
            showRemove: false,
            maxFileSize: 2048,
            msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
            allowedFileExtensions: ["jpg", "gif", "png"],
            overwriteInitial: true,
            allowedFileTypes: ["image"],
            previewFileType: ["image"]
        });

        $("#avatar").fileinput({
            initialPreview: [ <?php if(!empty($post['avatar'])){ ?> '<img src="/upload/blogs-category/avatar/<?php echo $this->base.$post['avatar'] ?>" class="" >' <?php } ?>],
            showUpload: false,
            showRemove: false,
            maxFileSize: 1024,
            msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
            allowedFileExtensions: ["jpg", "gif", "png"],
            overwriteInitial: true,
            allowedFileTypes: ["image"],
            previewFileType: ["image"]
        });

        $('.froalaEditor').froalaEditor({
            language: 'ja',
            height: 200,
        })
    });
</script>
