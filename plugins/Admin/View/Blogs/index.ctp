﻿<?php
$this->set('title', 'コールナビ｜ブログ管理');
$this->start('css'); ?>
	<link rel="stylesheet" href="<?php echo $this->base;?>/admin/css/common.css" type="text/css" media="all">
	<link rel="stylesheet" href="<?php echo $this->base;?>/admin/css/blogs.css" type="text/css" media="all">
<?php $this->end('css');
?>

<div id="manage-news" class="container-fluid">
	<div class="row">
		<div class="col-md-2 padding-right-10">
			<?php echo $this->element('../Elements/left_menu'); ?> 
		</div>
		<div class="col-md-10 padding-left-10">
			<div class="div_btn_post">
				<a href="<?php echo $this->Html->url(array('controller' => 'Blogs', 'action' => 'add',  ), true);?>" class="btn btn-primary pull-right btn-top" >新規登録</a>
				<div class="clearfix"></div>
			</div>

			<div class="search-form">
				<?php echo $this->Form->create('SearchCompany', array('url' => 'index','class' => 'form-horizontal form-row-5', 'type' => 'get')); ?>
				<div class="col-md-7 padding-5">
					<?php echo $this->Form->input('keywords',
						array('class' => 'form-control',
							'label' => false,
							'div' => false,
							'value' =>!empty($keywords) ? $keywords : '',
							'id'    => 'keywords',
							'placeholder' =>'検索キーワードを入力してください'));
					?>
				</div>

				<div class="col-md-3 padding-5">
					<select name="blog_category" id="blog_category" class="form-control">
						<option  value="" >ブログカテゴリー</option>
						<?php foreach ($category as $record):
							$post=$record['BlogsCategory'];
							$selected = '';
							if($getBlogCategory == $post['cat_id'])
								$selected = 'selected';
							?>
							<option <?php echo $selected; ?>  value="<?php echo $post['cat_id'];?>" ><?php echo $post['title'] ;?></option>
						<?php  endforeach ?>
					</select>
				</div>

				<div class="col-md-2 padding-5">
					<input type="submit" class="btn btn-primary pull-right" value="検索"/>
				</div>
				<?php echo $this->Form->end(null); ?>

			</div>
			<div class="col-md-12 no-padding">
				<p><?php echo $total ;?>件中 <?php echo $numberStartEnd['start'] ;?> - <?php echo $numberStartEnd['end'] ;?>件を表示</p>
			</div>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>画像</th>
						<th>タイトル</th>
						<th>カテゴリー</th>
						<th class="box-date">作成日</th>
						<th class="box-last">....</th><!---->
					</tr>
				</thead>
				<tbody>
					<?php
					if(!empty($list)) {
						foreach ($list as $record):
							$post = $record['Blogs'];
							$created = strtotime($post['created']) ?>
							<tr>
								<td width="100px">
									<?php if(!empty($post['pic'])){ ?>
										<img src="/upload/blogs/<?php echo $this->base.$post['pic'] ?>" class="" >
									<?php } ?>
								</td>
								<td><?php echo $post['title']; ?></td>
								<td><?php echo $record['blogs_category']['title']; ?></td>
								<td class="box-date"><?php echo $post['created']; ?></td>
								<td width="110px">
									<a type="button" class="btn btn-primary btn-xs" href="<?php echo $this->Html->url(array('controller' => 'Blogs', 'action' => 'edit', $post['id']), true); ?>">修正</a>
									<?php
									echo $this->Html->link('削除',
                                                           array(
                                                               'controller' =>'Blogs',
                                                               'action'     =>'delete',
                                                               
                                                               $post['id']
                                                           ),
                                                           array(
                                                               'confirm'=>'本当に削除しますか？',
                                                                'class'      =>'btn btn-default btn-xs'
                                                                ));
								?>
								</td>

							</tr>
						<?php endforeach;
					}else{?>
						<tr>
							<td colspan="5">
								<p>データなし</p>
							</td>
						</tr>
					<?php }?>
				</tbody>
			 </table>
            <?php echo $this->element('../Elements/pagination'); ?>
      </div>
   </div>
</div>