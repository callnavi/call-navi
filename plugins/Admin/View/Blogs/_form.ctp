﻿<?php
    $post = !empty($item['Blogs']) ? $item['Blogs'] : null;
    $btn = !empty($item['Blogs']) ? '修正' : '新規登録';
    $frmId = !empty($item['Blogs']) ? 'BlogsEditForm' : 'BlogsAddForm';
    $ajaxUrl = !empty($item['Blogs']) ?
        $this->Html->url(array('controller' => 'Blogs', 'action' => 'edit' ,$post['id'] ), true) :
        $this->Html->url(array('controller' => 'Blogs', 'action' => 'add'  ), true);

    $this->start('css');
        echo $this->Html->css('fileinput');
        echo $this->element('../Elements/froala_editor_css');
        echo $this->element('../Elements/tagit_css');
    $this->end('css');

    $this->start('script');
        echo $this->Html->script('jquery.validate.min');
        echo $this->Html->script('fileinput');
        echo $this->element('../Elements/froala_editor_js');
        echo $this->element('../Elements/tagit_js');
    $this->end('script');
?>
<div id="edit-blogs-categoy">
    <?php echo $this->Form->create('Blogs', array('type'=>'file','class' => ''));
    ?>
    <label  class="control-title">ブログ管理</label>

    <input type="submit" class="btn btn-primary pull-right" value="<?php echo $btn ;?>"/>
    <input type="button" style="margin-right: 15px;" class="btn btn-primary pull-right btn-preview" value="プレビュー" />

    <div class="form-group">
        <label  class="control-label">カテゴリー<span class="require">※必須</span></label>
        <select name="data[Blogs][cat_id]" id="cat_id" class="form-control">
            <?php foreach($category as $item):
                $row=$item['BlogsCategory'];
                if($post['cat_id'] == $row['cat_id'] ){?>
                    <option  value="<?php echo $row['cat_id'];?>" selected><?php echo $row['title'] ;?></option>
                <?php }else{ ?>
                    <option  value="<?php echo $row['cat_id'];?>" ><?php echo $row['title'] ;?></option>
                <?php } endforeach ?>
        </select>
    </div>

    <div class="form-group" >
        <label class="control-label">タイトル<span class="require">※必須</span></label>
        <?php
        echo $this->Form->input('title', array(
            'class'=>'form-control',
            'id'=>'title',
            'label' => false,
            'div'=>false,
            'value' =>$post['title'],));
        ?>
    </div>
    

    <div class="form-group">
        <label class="control-label">画像（最大サイズ：1メガバイト）</label>
        <?php echo $this->Form->input('pic',
            array('type' => 'file',
                'class' => 'form-control',
                'label' => false,
                'div'=>false,
                'id' => 'pic')); ?>
    </div>

    <div class="form-group">
        <label class="control-label">説明</label>
        <?php
        echo $this->Form->textarea('description', array(
            'class'=>'form-control froalaEditor','id'=>'description',
            'value' =>$post['description'],
            'rows'=> 7,));
        ?>
    </div>

    <div class="form-group" >
        <label class="control-label">タグ付け</label>
        <?php
        echo $this->Form->input('tag', array(
            'class'=>'form-control',
            'id'=>'tag',
            'label' => false,
            'div'=>false,
            'value' =>$post['tag'],));
        ?>
    </div>

    <div class="seo">
        <div class="title">Search Engine Optimization</div>

        <div class="form-group" >
            <label class="control-label">Friendly Title</label>
            <?php
            echo $this->Form->input('friendly_title', array(
              'class'=>'form-control',
              'id'=>'friendly_title',
              'label' => false,
              'div'=>false,
              'value' =>$post['friendly_title'],));
            ?>
            <p>Title display in search engines is limited to 70 chars.</p>
        </div>
        <div class="form-group" >
            <label class="control-label">Meta Description</label>
            <?php
            echo $this->Form->textarea('metadesc', array(
              'class'=>'form-control',
              'id'=>'metadesc',
              'rows' => 6,
              'label' => false,
              'div'=>false,
              'value' =>$post['metadesc'],));
            ?>
            <p>The meta description will be limited to 155 chars (because of date display).</p>
        </div>
        <div class="form-group" >
            <label class="control-label">Meta Keyword</label>
            <?php
            echo $this->Form->input('metakey', array(
              'class'=>'form-control',
              'id'=>'metakey',
              'label' => false,
              'div'=>false,
              'value' =>$post['metakey'],));
            ?>
        </div>

    </div>


    <div class="form-group">
        <input type="submit" class="btn btn-primary pull-right" value="<?php echo $btn ;?>"/>
        <input type="button" style="margin-right: 15px;" class="btn btn-primary pull-right btn-preview" value="SP-プレビュ" data-id="SP"/>
        <input type="button" style="margin-right: 15px;" class="btn btn-primary pull-right btn-preview" value="PC-プレビュ" data-id="PC"/>
    </div>


    <?php
    echo $this->Form->end();
    ?>





</div>

<script>
    $( document ).ready(function() {

        //Check validate form
        var form = $("#<?php echo $frmId; ?>");
        form.validate({
            errorPlacement: function errorPlacement(error, element)
            { element.prev().after(error); },
            onkeyup: false,
            rules: {
                "data[Blogs][title]": {
                    required:true,
                },
                "data[Blogs][company]":  {
                    required:true,
                }

            },

        });

        jQuery.extend(jQuery.validator.messages, {
            required: "この項目は必須です。",
        });

        $("#pic").fileinput({
            initialPreview: [ <?php if(!empty($post['pic'])){ ?> '<img src="/upload/blogs/<?php echo $this->base.$post['pic'] ?>" class="" >' <?php } ?>],
            showUpload: false,
            showRemove: false,
            maxFileSize: 2048,
            msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
            allowedFileExtensions: ["jpg", "gif", "png"],
            overwriteInitial: true,
            allowedFileTypes: ["image"],
            previewFileType: ["image"]
        });

        $('.froalaEditor').froalaEditor({
            language: 'ja',
            height: 200,
        })

        $('#tag').tagit();

        $('.btn-preview').click(function() {
            if ($('#<?php echo $frmId; ?>').valid()) {
                var input = $("<input>").attr("name", "isPreview").hide().val("1");
                //kind preview PC or SP
                var kindPreview = $(this).attr('data-id');
                $('#<?php echo $frmId; ?>').append(input);
                $.ajax({
                    url: "<?php echo $ajaxUrl ;?>",
                    async: false,
                    type: "post",
                    data: $("#<?php echo $frmId; ?>").serialize(),
                    success: function () {
                        if(kindPreview == 'PC')
                        {
                            window.open(
                                document.location.origin + "/blog/preview/<?php echo !empty($post['id']) ? $post['id'] : $lastId ; ?>/"+ kindPreview, '_blank'
                            );
                        }
                        else
                        {
                            window.open(
                                document.location.origin + "/blog/preview/<?php echo !empty($post['id']) ? $post['id'] : $lastId ; ?>/"+ kindPreview, '_blank', "width=414,height=650,resizeable,scrollbars"
                            );
                        }
                        <?php if(empty($post['id'])){ ?>
                            window.location.href = document.location.origin +"/admin/Blogs/edit/<?php echo $lastId;?>";
                        <?php } ?>

                    }
                });
            }
        });
    });
</script>
