<?php 
	$post = !empty($item['CorporatePr']) ? $item['CorporatePr'] : null;
	$btn = !empty($item['CorporatePr']) ? '修正' : '新規登録';
	$frmId = !empty($item['CorporatePr']) ? 'ManagerCompanyEditForm' : 'ManagerCompanyAddForm';
    $ajaxUrl = !empty($item['CorporatePr']) ?
        $this->Html->url(array('controller' => 'ManageCompany', 'action' => 'edit' ,$post['id'] ), true) :
        $this->Html->url(array('controller' => 'ManageCompany', 'action' => 'add'  ), true);

    // validate for double id post back
    if(empty($corporate_code_1)){
        if ($post['corporate_code'] ){
            $corporate_Code = $post['corporate_code'];
        }else{
            $corporate_Code = $this->App->generateNewCompanyCode();
        }        
    }else{
        $corporate_Code = $corporate_code_1;
    }


$this->start('css');
        echo $this->Html->css('fileinput');
        echo $this->element('../Elements/froala_editor_css');
    $this->end('css');
	
	$this->start('script');
        echo $this->Html->script('jquery.validate.min');
        echo $this->Html->script('fileinput');
        echo $this->element('../Elements/froala_editor_js');
        echo $this->Html->script('init-googlemap'); ?>
        <script src="https://maps.google.com/maps/api/js?libraries=geometry&key=<?php
            echo Configure::read('webconfig.google_map_api_key'); ?>"></script>
    <?php $this->end('script'); ?>

<div class="news-form-container">
	<?php echo $this->Form->create('ManagerCompany', array('type' => 'file','class' => 'form-horizontal')); ?>
    <label  class="control-title">会社管理</label>

        <input type="submit" class="btn btn-primary pull-right" value="<?php echo $btn ;?>"/>
        <input type="button" style="margin-right: 15px;" class="btn btn-primary pull-right btn-preview" value="プレビュー" />

        
        <div class="form-group">
        <label class="control-label" >法人ID <span style="color:red;"><?php if (!empty($error_message_code)) { echo($error_message_code); }  ?></span></label>
		        <?php echo $this->Form->input('corporate_code',
		            array('class' => 'form-control',
                        'label' => false,
                        'div' => false,
                        'value' =>$corporate_Code,
                        'id'    => 'corporate_code',
                        'placeholder' =>'C0001'));
				?>
                
                <?php echo $this->Form->hidden('corporate_code_hidden',
		            array(
                        'value' =>$post['corporate_code'],
                        'id'    => 'corporate_code_check',
                        ));
				?>
        </div>

        <!-- delete data of 企業PRのロゴ field in company -->

		
		<div class="form-group">
		    <label  class="control-label">企業メッセージ</label>
		            <?php echo $this->Form->textarea('corporate',
                        array('class' => 'form-control textarea',
                            'label' => false,
                            'id'    => 'corporate',
                            'value' =>$post['corporate'],
                            'rows'=> 10,
                            'placeholder' => '')); ?>
		</div>

		<div class="form-group">
		    <label class="control-label">社長はこんな人</label><br>
			<label class="control-label">社長のアバター（最大サイズ：1メガバイト）</label>
			<?php echo $this->Form->input('president_avata',
				array('type' => 'file',
					'class' => '',
					'label' => false,
					'id' => 'president_avata')); ?>
		    <label class="control-label" >お名前</label>
		        <?php echo $this->Form->input('president_name',
		            array('class' => 'form-control',
                        'label' => false,
                        'div' => false,
                        'value' =>$post['president_name'],
                        'id'    => 'president_name',
                        'placeholder' =>''));
				?>

            <label class="control-label" style="margin-top:10px">ご紹介文</label>
            <?php echo $this->Form->textarea('president_description',
                array('class' => 'form-control textarea',
                    'label' => false,
                    'id'    => 'president_description',
                    'value' =>$post['president_description'],
                    'rows'=> 10,
                    'placeholder' => '')); ?>
		</div>

        <!-- delete data of コールナビ編集部コメント field in company -->


    <label  class="control-title">企業基本情報</label>
        <div class="form-group">
            <label class="control-label">企業名</label>
            <?php echo $this->Form->input('company_name',
                array('class' => 'form-control ',
                    'label' => false,
                    'div' => false,
                    'value' =>$post['company_name'],
                    'id'    => 'company_name',
                    'placeholder' =>''));
            ?>
        </div>

        <div class="form-group">
            <label class="control-label">WEBサイト</label><br>
            <?php echo $this->Form->input('website',
                array('class' => 'form-control',
                    'label' => false,
                    'value' =>$post['website'],
                    'id'    => 'website',
                    'placeholder' =>''));
            ?>
        </div>

        <div class="form-group">
            <label  class="control-label">事業内容</label>
            <?php echo $this->Form->textarea('business',
                array('class' => 'form-control textarea',
                    'label' => false,
                    'id'    => 'business',
                    'value' =>$post['business'],
                    'rows'=> 5,
                    'placeholder' => '')); ?>
        </div>

        <div class="form-group">
            <label  class="control-label">本社・その他事業所</label>
            <?php echo $this->Form->textarea('other_location',
                array('class' => 'form-control textarea',
                    'label' => false,
                    'id'    => 'other_location',
                    'value' =>$post['other_location'],
                    'rows'=> 5,
                    'placeholder' => '')); ?>
        </div>

        <div class="form-group">
            <label  class="control-label">代表者</label>
            <?php echo $this->Form->textarea('representative',
                array('class' => 'form-control textarea',
                    'label' => false,
                    'id'    => 'representative',
                    'value' =>$post['representative'],
                    'rows'=> 5,
                    'placeholder' => '')); ?>
        </div>

        <div class="form-group">
            <label  class="control-label">設立日</label>
            
            <?php echo $this->Form->input('establishment_date',
                array('class' => 'form-control',
                    'label' => false,
                    'value' =>$post['establishment_date'],
                    'id'    => 'establishment_date',
                    'placeholder' =>''));
            ?>
        </div>

		<div class="form-group">
            <label  class="control-label">資本金</label>
            <?php echo $this->Form->textarea('capital',
                array('class' => 'form-control textarea',
                    'label' => false,
                    'id'    => 'capital',
                    'value' =>$post['capital'],
                    'rows'=> 5,
                    'placeholder' => '')); ?>
        </div>

		<div class="form-group">
            <label  class="control-label">従業員数</label>
            <?php echo $this->Form->textarea('employee_number',
                array('class' => 'form-control textarea',
                    'label' => false,
                    'id'    => 'employee_number',
                    'value' =>$post['employee_number'],
                    'rows'=> 5,
                    'placeholder' => '')); ?>
        </div>
        
        <!-- delete data of Map Location field in company -->


		<div class="form-group">
			<input type="submit" class="btn btn-primary pull-right" value="<?php echo $btn ;?>"/>
            <input type="button" style="margin-right: 15px;" class="btn btn-primary pull-right btn-preview" value="SP-プレビュ" data-id="SP"/>
            <input type="button" style="margin-right: 15px;" class="btn btn-primary pull-right btn-preview" value="PC-プレビュ" data-id="PC"/>
		</div>

	<?php echo $this->Form->end(null); ?>
	
<script>
    $( document ).ready(function() {

//Check validate form
        var form = $("#<?php echo $frmId; ?>");
        form.validate({
            errorPlacement: function errorPlacement(error, element) { element.before(error); },
            onkeyup: false,
            rules: {},

        });
		
		jQuery.extend(jQuery.validator.messages, {
            required: "この項目は必須です。",
        });
		
		$("#president_avata").fileinput({			
			initialPreview: [ <?php if(!empty($post['president_avata'])){ ?> '<img src="/upload/secret/companies/<?php echo $this->base.$post['president_avata'] ?>" class="" >' <?php } ?>],
			showUpload: false,
			showRemove: false,
			maxFileSize: 1024,
			msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
			allowedFileExtensions: ["jpg", "gif", "png"],
			overwriteInitial: true,
			allowedFileTypes: ["image"],
			previewFileType: ["image"]
		});

		$("#corporate_logo").fileinput({
			initialPreview: [ <?php if(!empty($post['corporate_logo'])){ ?> '<img src="/upload/secret/companies/<?php echo $this->base.$post['corporate_logo'] ?>" class="" >' <?php } ?>],
			showUpload: false,
			showRemove: false,
			maxFileSize: 1024,
			msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
			allowedFileExtensions: ["jpg", "gif", "png"],
			overwriteInitial: true,
			allowedFileTypes: ["image"],
			previewFileType: ["image"]
		});

        $('.textarea').froalaEditor({
            language: 'ja',
            height: 200,
        })


        // check validate in froalaEditor
        $( "#<?php echo $frmId; ?>" ).submit(function( event ) {
            var validateTextarea = true
            $( ".froalaEditor-require" ).each(function( index ) {
                var content = $(this).froalaEditor('html.get', true);
                if(content == ''){
                    $(this).parent().find( ".control-label" ).after( "<label class='error'>この項目は必須です。</label>" );
                    $(this).parent().find( ".fr-element" ).focus();
                    validateTextarea = false;
                }
            });

            return validateTextarea;
        });

        $('.froalaEditor-require').on('froalaEditor.keyup', function (e, editor, keyupEvent) {
            var content = $(this).froalaEditor('html.get', true);
            if(content == '')
                $(this).parent().find( ".control-label" ).after( "<label class='error'>この項目は必須です。</label>" );
            else
                $(this).parent().find( ".error").remove();
        });

        $('.btn-preview').click(function() {
            if ($('#<?php echo $frmId; ?>').valid()) {
                var input = $("<input>").attr("name", "isPreview").hide().val("1");
                //kind preview PC or SP
                var kindPreview = $(this).attr('data-id');

                $('#<?php echo $frmId; ?>').append(input);
                $.ajax({
                    url: "<?php echo $ajaxUrl ;?>",
                    async: false,
                    type: "post",
                    data: $("#<?php echo $frmId; ?>").serialize(),
                    success: function () {
                        if(kindPreview == 'PC')
                        {
                            window.open(
                                document.location.origin + "/secret/previewCompany/<?php echo !empty($post['id']) ? $post['id'] : $lastId ; ?>/"+ kindPreview, '_blank'
                            );
                        }
                        else
                        {
                            window.open(
                                document.location.origin + "/secret/previewCompany/<?php echo !empty($post['id']) ? $post['id'] : $lastId ; ?>/"+ kindPreview, '_blank', "width=414,height=650,resizeable,scrollbars"
                            );
                        }
                        
                        <?php if(empty($post['id'])){ ?>
                        window.location.href = document.location.origin +"/admin/ManageCompany/edit/<?php echo $lastId;?>";
                        <?php } ?>

                    }
                });
            }
        });

        // set google map
        // input lat, lng , inforwindow, zoom
        <?php $latLng = !empty($post['lat']) && !empty($post['lng']) ? $post['lat'].' , '.$post['lng'] : '0,0'; ?>
        var contentString = "<h4 style='font-weight: bold;'><?php echo $post['company_name']; ?></h4><a href='<?php echo $post['website']; ?>' ><p><?php echo $post['website']; ?></p></a><p><?php echo $post['office_location']; ?> </p> ";
        initMap( <?php echo $latLng;?>,contentString, 18   )
        $( "#find_location" ).click(function() {
            var address =$("#map_location").val();
            var p = null;
            $.ajaxSetup({
                async: false
            });
            $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&sensor=false', null, function (data) {
                var p = data.results[0].geometry.location;
                $('#lat').attr('value', p.lat);
                $('#lng').attr('value', p.lng);
                initMap(p.lat, p.lng, contentString, 18 );
            });
        });

    });
</script>

</div>