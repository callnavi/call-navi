﻿<?php
	
	$this->start('css');
        echo $this->Html->css('fileinput');
        echo $this->element('../Elements/tagit_css');
    $this->end('css');
	
	$this->start('script');
        echo $this->Html->script('jquery.validate.min');
        echo $this->Html->script('fileinput');
        echo $this->element('../Elements/tagit_js');
    $this->end('script');

?>
<div class="news-form-container">
	<?php echo $this->Form->create('ManageMeta', array('type' => 'file','class' => 'form-horizontal')); ?>
    <label  class="control-title">Meta管理</label>

    <input type="submit" class="btn btn-primary pull-right" value="修正"/>

    <div class="form-group">
        <label  class="control-label">Meta Desciption</label>
        <?php echo $this->Form->textarea('meta_desciption',
            array('class' => 'form-control',
                'label' => false,
                'id'    => 'meta_desciption',
                'value' =>  !empty(Cache::read('meta_desciption','meta')) ? Cache::read('meta_desciption','meta') : '',
                'rows'=> 10,
                'placeholder' => '')); ?>
    </div>

    <div class="form-group" >
        <label class="control-label">Meta Keywords</label>
        <?php
        echo $this->Form->input('meta_keywords', array(
            'class'=>'form-control',
            'id'=>'meta_keywords',
            'label' => false,
            'div'=>false,
            'value' =>!empty(Cache::read('meta_keywords','meta')) ? Cache::read('meta_keywords','meta') : '',
            ));
        ?>
    </div>


    <div class="form-group">
       <label class="control-label">OGP Image（最大サイズ：1メガバイト）</label>
        <?php echo $this->Form->input('ogp_img',
            array('type' => 'file',
                'class' => '',
                'label' => false,
                'id' => 'ogp_img')); ?>
    </div>

    <div class="form-group">
        <label class="control-label">Apple Touch Image（最大サイズ：1メガバイト）</label>
        <?php echo $this->Form->input('apple_touch_img',
            array('type' => 'file',
                'class' => '',
                'label' => false,
                'id' => 'apple_touch_img')); ?>
    </div>

    <div class="form-group">
        <input type="submit" class="btn btn-primary pull-right" value="修正"/>
    </div>

	<?php echo $this->Form->end(null); ?>
	
<script>
    $( document ).ready(function() {

		$("#ogp_img").fileinput({
			initialPreview: [ <?php if(!empty(Cache::read('ogp_img','meta'))){ ?> '<img src="/upload/meta/<?php echo $this->base.Cache::read('ogp_img','meta') ?>" class="" >' <?php } ?> ],
			showUpload: false,
			showRemove: false,
			maxFileSize: 1024,
			msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
			allowedFileExtensions: ["jpg", "gif", "png"],
			overwriteInitial: true,
			allowedFileTypes: ["image"],
			previewFileType: ["image"]
		});

        $("#apple_touch_img").fileinput({
            initialPreview: [ <?php if(!empty(Cache::read('apple_touch_img','meta'))){ ?> '<img src="/upload/meta/<?php echo $this->base.Cache::read('apple_touch_img','meta') ?>" class="" >' <?php } ?> ],
            showUpload: false,
            showRemove: false,
            maxFileSize: 1024,
            msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
            allowedFileExtensions: ["jpg", "gif", "png"],
            overwriteInitial: true,
            allowedFileTypes: ["image"],
            previewFileType: ["image"]
        });

        $('#meta_keywords').tagit();


    });
</script>

	
</div>