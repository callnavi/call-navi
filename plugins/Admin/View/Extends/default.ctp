<?php echo $this->fetch('content'); ?>

<?php $this->start('_meta'); ?>
<meta name="title" content="<?php echo $this->fetch('title'); ?>">
<meta name="description" content="<?php echo $this->fetch('description'); ?>">
<meta name="keywords" content="<?php echo $this->fetch('keywords'); ?>">
<?php $this->end('_meta'); ?>


<?php $this->start('_css'); ?>
<link rel="stylesheet" href="<?php echo $this->base;?>/plugins/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="/wp-content/themes/twentysixteen/style.css?ver=4.4.2">
<link rel="stylesheet" href="<?php echo $this->base;?>/css/style.css">
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<?php $this->end('_css'); ?>


<?php $this->start('_js'); ?>
<script type="text/javascript" src="<?php echo $this->base;?>/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo $this->base;?>/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
<?php $this->end('_js'); ?>


