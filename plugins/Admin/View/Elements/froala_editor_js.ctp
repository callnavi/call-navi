<!--  start  text editor js-->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/mode/xml/xml.min.js"></script>
<script type="text/javascript" src="<?php echo $this->base;?>/admin/plugins/froala-editor/js/froala_editor.min.js"></script>
<script type="text/javascript" src="<?php echo $this->base;?>/admin/plugins/froala-editor/js/plugins/fullscreen.min.js"></script>
<script type="text/javascript" src="<?php echo $this->base;?>/admin/plugins/froala-editor/js/plugins/code_beautifier.min.js"></script>
<script type="text/javascript" src="<?php echo $this->base;?>/admin/plugins/froala-editor/js/plugins/code_view.min.js"></script>
<script type="text/javascript" src="<?php echo $this->base;?>/admin/plugins/froala-editor/js/plugins/draggable.min.js"></script>
<script type="text/javascript" src="<?php echo $this->base;?>/admin/plugins/froala-editor/js/plugins/font_size.min.js"></script>
<script type="text/javascript" src="<?php echo $this->base;?>/admin/plugins/froala-editor/js/plugins/font_family.min.js"></script>
<script type="text/javascript" src="<?php echo $this->base;?>/admin/plugins/froala-editor/js/plugins/paragraph_style.min.js"></script>
<script type="text/javascript" src="<?php echo $this->base;?>/admin/plugins/froala-editor/js/plugins/lists.min.js"></script>
<script type="text/javascript" src="<?php echo $this->base;?>/admin/plugins/froala-editor/js/plugins/image.min.js"></script>
<script type="text/javascript" src="<?php echo $this->base;?>/admin/plugins/froala-editor/js/plugins/link.min.js"></script>
<script type="text/javascript" src="<?php echo $this->base;?>/admin/plugins/froala-editor/js/plugins/video.min.js"></script>
<script type="text/javascript" src="<?php echo $this->base;?>/admin/plugins/froala-editor/js/plugins/image_manager.min.js"></script>
<script type="text/javascript" src="<?php echo $this->base;?>/admin/plugins/froala-editor/js/plugins/align.min.js"></script>
<script type="text/javascript" src="<?php echo $this->base;?>/admin/plugins/froala-editor/js/plugins/callnavi_content_wrapper.js"></script>
<script type="text/javascript" src="<?php echo $this->base;?>/admin/plugins/froala-editor/js/languages/ja.js"></script>
<!--  end  text editor js-->