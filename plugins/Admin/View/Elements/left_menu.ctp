<?php
	$currentController = strtolower($this->params['controller']);
	$menu_list = array(
		'ManageCompany' => '会社管理',
		'ManageJob' => '仕事管理',
        'ManageEntry' => '応募管理',
		'ManageContents' => 'コンテンツ管理',
		'BlogsCategory' => 'ブログカテゴリー管理',
		'Blogs' => 'ブログ管理',
		'ManageNews' => 'ニュース管理',
		'ManageCallCenter' => 'CC管理',
		'ManageFeedback' => '意見管理',
		'ManagePretty' => '美女美男図鑑',
		'ManagePickup' => 'Pick up',
		//'ManageMeta' => 'Meta管理',
		//'UploadCsv' => 'Upload CSV',
	);
?>

<ul class="list-group left-menu-admin">
	
	<?php foreach ($menu_list as $key=>$name): ?>
	<?php 
			$activeMenu = '';
			if(strtolower($key) == $currentController)
			{
				$activeMenu = 'active-menu';
			}
	?>
	<li class="list-group-item <?php echo $activeMenu; ?>">
        <a href="<?php echo $this->Html->url(array('controller' => $key, 'action' => 'index' ), true);?>"><?php echo $name; ?></a>
    </li>
	<?php endforeach; ?>

    <li class="list-group-item" id="item-logout">
        <a href="#" data-toggle="modal" data-target=".bs-example-modal-sm">ログアウト</a>
    </li>

    <!-- Small modal -->
   <div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
		<div class="modal-dialog modal-sm modal-logout">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title">ログアウト?</h4>
				</div>

				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">クローズ</button>
					<a href="<?php echo $this->Html->url(array('controller' => 'auth', 'action' => 'logout' ), true);?>" class="btn btn-primary">ログアウト</a>
				</div>
			</div>
		</div>
	</div>
</ul>
