<!--  start  text editor css-->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css">
<link rel="stylesheet" href="<?php echo $this->base;?>/admin/plugins/froala-editor/css/froala_editor.css">
<link rel="stylesheet" href="<?php echo $this->base;?>/admin/plugins/froala-editor/css/froala_style.css">
<link rel="stylesheet" href="<?php echo $this->base;?>/admin/plugins/froala-editor/css/plugins/fullscreen.min.css">
<link rel="stylesheet" href="<?php echo $this->base;?>/admin/plugins/froala-editor/css/plugins/code_view.css">
<link rel="stylesheet" href="<?php echo $this->base;?>/admin/plugins/froala-editor/css/plugins/image.css">
<link rel="stylesheet" href="<?php echo $this->base;?>/admin/plugins/froala-editor/css/plugins/image_manager.css">
<link rel="stylesheet" href="<?php echo $this->base;?>/admin/plugins/froala-editor/css/plugins/callnavi_contents.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/codemirror/5.3.0/codemirror.min.css">
<!--  end  text editor css-->