<?php
	$currentUrl = '/admin/ManagePickup';
	$currentUrl .= $currentQueryString;

	$paramPage = ($currentUrl == '/admin/ManagePickup') ? '?page=' : '&page=';

	$modulus = 6;
	$haft  = (int)$modulus/2;
 
	if(($modulus+1) >= $pageCount)
	{
		$start = 1;
		$end = $pageCount;
	}
	else
	{
		if(($currentPage - $haft) <= 1)
		{
			$start = 1;
			$end = $start + $modulus;
		}
		else
		{
			if($currentPage > ($pageCount - $haft))
			{
				$end = $pageCount;
				$start = $end - $modulus;
			}
			else
			{
				$start = $currentPage - $haft;
				$end = $currentPage + $haft; 
			}
		}
	}
?>


<ul class="pagination">
	<?php if($pageCount > 1): ?>

		<?php if($currentPage <= 1): ?>
			<li class="disabled"><a href="#" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
		<?php else: ?>
			<li><a href="<?php echo $currentUrl . $paramPage . ($currentPage - 1); ?>" aria-label="Previous"><span aria-hidden="true">&laquo;</span></a></li>
		<?php endif; ?>
		

		
		
		<?php for($i=$start; $i<=$end; $i++): ?>
			<?php if ($currentPage == $i): ?>
			<li class="active"><a href="#"><?php echo $i; ?><span class="sr-only">(current)</span></a></li>
			<?php else: ?>
			<li><a href="<?php echo $currentUrl . $paramPage . $i; ?>"><?php echo $i; ?></a></li>
			<?php endif; ?>
		<?php endfor; ?>
		
		
		<?php if($pageCount >= $currentPage+$modulus): ?>
			<li class="disabled"><a href="#" aria-label="Previous"><span aria-hidden="true">... </span></a></li>
			<li><a href="<?php echo $currentUrl . $paramPage . $pageCount; ?>"><?php echo $pageCount; ?></a></li>
		<?php endif; ?>

		
		<?php if($currentPage >= $pageCount): ?>
			<li class="disabled"><a href="#" aria-label="Previous"><span aria-hidden="true">&raquo;</span></a></li>
		<?php else: ?>
	        <li><a href="<?php echo $currentUrl . $paramPage . ($currentPage + 1) ; ?>" aria-label="Next"><span aria-hidden="true">&raquo;</span></a></li>
		<?php endif; ?>
	<?php endif; ?>
</ul>

