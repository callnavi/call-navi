<?php
  
  $actionName = $this->request->params['action'];

	$post = !empty($item['Contents']) ? $item['Contents'] : null;
	$btn = !empty($item['Contents']) ? '修正' : '新規登録';
	$frmId = !empty($item['Contents']) ? 'ManagerContentsEditForm' : 'ManagerContentsAddForm';

    $ajaxUrl = !empty($item['Contents']) ?
        $this->Html->url(array('controller' => 'ManageContents', 'action' => 'edit' ,$post['id'] ), true) :
        $this->Html->url(array('controller' => 'ManageContents', 'action' => 'add'  ), true);
	
	$this->start('css');
        echo $this->Html->css('fileinput');
        echo $this->element('../Elements/froala_editor_css');
    $this->end('css');
	
	$this->start('script');
        echo $this->Html->script('jquery.validate.min');
        echo $this->Html->script('fileinput');
        echo $this->element('../Elements/froala_editor_js');
    $this->end('script');
 ?>

<!-- custom froala editor -->
 <?php $this->start('script'); ?>
    <script type="text/javascript" src="<?php echo $this->base;?>/admin/plugins/froala-editor/js/plugins/callnavi_content_wrapper.js"></script>
<?php $this->end('script'); ?>

<div class="news-form-container">
	<?php echo $this->Form->create('ManagerContents', array('type' => 'file','class' => 'form-horizontal')); ?>
    <label  class="control-title">コンテンツ管理</label>

        <input type="submit" class="btn btn-primary pull-right" value="<?php echo $btn ;?>"/>
        <input type="button" style="margin-right: 15px;" class="btn btn-primary pull-right btn-preview" value="プレビュー" />

        <div class="form-group">
            <label  class="control-label">カテゴリー<span class="require">※必須</span></label>
            <select name="data[ManagerContents][category_alias]" id="category_id" class="form-control">
                <?php foreach($category as $item):
                    $row=$item['Categories'];
                    if($post['category_alias'] == $row['category_alias'] ){?>
                        <option  value="<?php echo $row['category_alias'];?>" selected><?php echo $row['category'] ;?></option>
                    <?php }else{ ?>
                        <option  value="<?php echo $row['category_alias'];?>" ><?php echo $row['category'] ;?></option>
                <?php } endforeach ?>
            </select>
        </div>
		
      	<hr>
       
        <div class="form-group">
            <label  class="control-label">会社名</label>
            <select name="data[ManagerContents][company_id]" id="company_id" class="form-control">
                <option value="0">選択してください</option>
                 <?php foreach($companies as $item):
                    $row=$item['CorporatePr'];
                    if($post['company_id'] == $row['id'] ){?>
                        <option  value="<?php echo $row['id'];?>" selected><?php echo $row['company_name'] ;?></option>
                    <?php }else{ ?>
                        <option  value="<?php echo $row['id'];?>" ><?php echo $row['company_name'] ;?></option>
                <?php } endforeach ?>
            </select>
        </div>
        
         <div class="form-group">
            <label  class="control-label">限定求人</label>
            <select data-selected="<?php echo $post['secret_job_id']; ?>" name="data[ManagerContents][secret_job_id]" id="secret_job_id" class="form-control"></select>
        </div>

		<hr>
        <div class="form-group">
            <label class="control-label" >会社名 (手で入力)</label>
            <?php echo $this->Form->input('company_name',
                array('class' => 'form-control',
                    'label' => false,
                    'value' =>$post['company_name'],
                    'id'    => 'company_name',
                    'placeholder' =>''));
            ?>
		</div>
        
        <div class="form-group">
            <label class="control-label" >代表者</label>
            <?php echo $this->Form->input('ceo_name',
                array('class' => 'form-control',
                    'label' => false,
                    'value' =>$post['ceo_name'],
                    'id'    => 'ceo_name',
                    'placeholder' =>''));
            ?>
		</div>
        
        <hr>

       

		<div class="form-group">
            <label class="control-label" >タイトル <span class="require">※必須</span></label>
            <?php echo $this->Form->input('title',
                array('class' => 'form-control',
                    'label' => false,
                    'value' =>$post['title'],
                    'id'    => 'title',
                    'placeholder' =>''));
            ?>
		</div>

        <div class="form-group">
            <label class="control-label" >タイトル別名<span class="require">※必須</span></label>
            <?php echo $this->Form->input('title_alias',
                array('class' => 'form-control',
                    'label' => false,
                    'value' =>$post['title_alias'],
                    'id'    => 'title_alias',
                    'placeholder' =>''));
            ?>
        </div>

        <div class="form-group">
          <label class="control-label" >ディスクリプション</label>
          <?php echo $this->Form->input('meta_description',
            array('class' => 'form-control',
              'label' => false,
              'type' => 'textarea',
              'value' =>$post['meta_description'],
              'id'    => 'meta_description',
              'rows' => 4,
              'placeholder' =>'Description'));
          ?>
        </div>
        <div class="form-group">
          <label class="control-label" >キーワード</label>
          <?php echo $this->Form->input('meta_keyword',
            array('class' => 'form-control',
              'label' => false,
              'type' => 'textarea',
              'value' =>$post['meta_keyword'],
              'id'    => 'meta_keyword',
              'rows' => 3,
              'placeholder' =>'Keyword'));
          ?>
        </div>

        <div class="form-group">
            <label class="control-label" >画像（最大サイズ：1メガバイト）</label>
            <?php echo $this->Form->input('image',
                array('type' => 'file',
                    'class' => 'form-control',
                    'label' => false,
                    'id' => 'image')); ?>
        </div>
		
        
		<!--content_introduction-->
        <div class="form-group">
          <label class="control-label" >導入文</label>
          <?php echo $this->Form->input('content_introduction',
            array('class' => 'form-control',
              'label' => false,
              'type' => 'textarea',
              'value' =>$post['content_introduction'],
              'id'    => 'content_introduction',
              'rows' => 4,
              'placeholder' =>'導入文'));
          ?>
        </div>
        
        
        <!--image_profile-->
        <div class="form-group">
            <label class="control-label" >プロフィール画像（PC）</label>
            <?php echo $this->Form->input('image_profile',
                array('type' => 'file',
                    'class' => 'form-control',
                    'label' => false,
                    'id' => 'image_profile')); ?>
        </div>
        
        <!--image_profile_sp-->
        <div class="form-group">
            <label class="control-label" >プロフィール画像（SP）</label>
            <?php echo $this->Form->input('image_profile_sp',
                array('type' => 'file',
                    'class' => 'form-control',
                    'label' => false,
                    'id' => 'image_profile_sp')); ?>
        </div>
        

        <div class="form-group content--p">
            <label  class="control-label">コンテンツ</label>
            <?php echo $this->Form->textarea('content',
                array('class' => 'form-control textarea',
                    'label' => false,
                    'id'    => 'content',
                    'value' =>$post['content'],
                    'rows'=> 10,
                    'placeholder' => '')); ?>
        </div>

         <div class="form-group content--p">
            <label  class="control-label">コンテンツ(after オリジナル求人詳細)</label>
            <?php echo $this->Form->textarea('content4',
                array('class' => 'form-control textarea',
                    'label' => false,
                    'id'    => 'content4',
                    'value' =>$post['content4'],
                    'rows'=> 10,
                    'placeholder' => '')); ?>
        </div>


		<div class="form-group">
			<label class="control-label">作成日</label>
			<?php echo $this->Form->input('created',
				array('class' => '',
					'label' => false,
					'type' => 'date',
					'id' => 'created',
					'value' => !empty($post['created']) ? $post['created'] : null ,
					'dateFormat' => 'YMD',
					'required' => false,
					'monthNames' => array(1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9,10=>10,11=>11,12=>12)
				)); ?>
    	</div>

		<div class="form-group">
			<input type="submit" class="btn btn-primary pull-right" value="<?php echo $btn ;?>"/>
<!--			<small>[PC-プレビュ] and [SP-プレビュ] are building</small>-->
<!--
            <input type="button" style="margin-right: 15px;" class="btn btn-primary pull-right btn-preview" value="SP-プレビュ" data-id="SP"/>
            <input type="button" style="margin-right: 15px;" class="btn btn-primary pull-right btn-preview" value="PC-プレビュ" data-id="PC"/>
-->
		</div>

	<?php echo $this->Form->end(null); ?>
	
<script>
    $( document ).ready(function() {

//Check validate form
        var form = $("#<?php echo $frmId; ?>");
        form.validate({
            errorPlacement: function errorPlacement(error, element) { element.before(error); },
            onkeyup: false,
            rules: {
                "data[ManagerContents][title_alias]": {
                    required:true,
                },
                "data[ManagerContents][title]": {
                    required: true,
                },

            },

        });
		
		jQuery.extend(jQuery.validator.messages, {
            required: "この項目は必須です。",
        });
		
		
		<?php
			function check_image_and_send($key, $post)
			{
				if(!empty($post[$key])){
					$pos = strpos($post[$key], "ccwork");
					if($pos !== false)
						$src = $post['image'];
					else
						$src= '/upload/contents/'.$post[$key];
				}else
					$src= null;
				
				if($src){ 
					echo '\'<img src="'.$src.'" class="" >\'';
				};
			}
		    
		?>
		
		
		$("#image").fileinput({
			initialPreview: [ <?php check_image_and_send('image', $post); ?> ],
			showUpload: false,
			showRemove: false,
			maxFileSize: 1024,
			msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
			allowedFileExtensions: ["jpg", "gif", "png"],
			overwriteInitial: true,
			allowedFileTypes: ["image"],
			previewFileType: ["image"]
		});
		
		$("#image_profile").fileinput({
			initialPreview: [ <?php check_image_and_send('image_profile', $post); ?> ],
			showUpload: false,
			showRemove: false,
			maxFileSize: 1024,
			msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
			allowedFileExtensions: ["jpg", "gif", "png"],
			overwriteInitial: true,
			allowedFileTypes: ["image"],
			previewFileType: ["image"]
		});
		
		$("#image_profile_sp").fileinput({
			initialPreview: [ <?php check_image_and_send('image_profile_sp', $post); ?> ],
			showUpload: false,
			showRemove: false,
			maxFileSize: 1024,
			msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
			allowedFileExtensions: ["jpg", "gif", "png"],
			overwriteInitial: true,
			allowedFileTypes: ["image"],
			previewFileType: ["image"]
		});
		
		
		
        $('textarea.textarea').froalaEditor({
            language: 'ja',
            height: 300,
            //enter: $.FroalaEditor.ENTER_BR,
            toolbarButtons: [
							 'fullscreen', 
							 '|',	
                             'bold', 
                             'italic', 
                             'underline', 
                             'strikeThrough', 
                             'subscript', 
                             'superscript', 
                             'fontFamily', 
                             'fontSize', 
                             '|', 
                             'color', 
                             'emoticons', 
                             'inlineStyle', 
                             'paragraphStyle', 
                             '|', 
                             'paragraphFormat', 
                             'align', 
                             'formatOL', 
                             'formatUL', 
                             'outdent', 
                             'indent', 
                             'quote', 
                             'insertHR', 
                             '|', 
                             'insertLink',
                             'insertImage', 
                             'insertVideo', 
                             'insertFile', 
                             'insertTable', 
                             'undo', 
                             'redo', 
                             'clearFormatting', 
                             'selectAll', 
                             'html',
							 '|',
                             'runda_title_wrapper',
                             '|',
                             'runda_topic_wrapper',
							 '|',
                             'runda_image_wrapper'
                            ]
        })

        $('.btn-preview').click(function() {
            if ($('#<?php echo $frmId; ?>').valid()) {
                var input = $("<input>").attr("name", "isPreview").hide().val("1");
                //kind preview PC or SP
                var kindPreview = $(this).attr('data-id');
                $('#<?php echo $frmId; ?>').append(input);
                $.ajax({
                    url: "<?php echo $ajaxUrl ;?>",
                    async: false,
                    type: "post",
                    data: $("#<?php echo $frmId; ?>").serialize(),
                    success: function () {
                        if(kindPreview == 'PC')
                        {
                            window.open(
                                document.location.origin + "/contents/preview/<?php echo !empty($post['id']) ? $post['id'] : $lastId ; ?>/"+ kindPreview, '_blank'
                            );
                        }
                        else
                        {
                            window.open(
                                document.location.origin + "/contents/preview/<?php echo !empty($post['id']) ? $post['id'] : $lastId ; ?>/"+ kindPreview, '_blank', "width=414,height=650,resizeable,scrollbars"
                            );
                        }
                        <?php if(empty($post)){ ?>
                            window.location.href = document.location.origin +"/admin/ManageContents/edit/<?php echo $lastId;?>";
                        <?php } ?>

                    }
                });
            }
        });

        //set selected value for category_id is interview
        var catName = "interview";
        if(catName)
        {
            $('#category_id option[value=' + catName + ']').attr('selected','selected');
        }

        var actionName = '<?php echo $actionName; ?>';
        $('#category_id').change(function(){
            var category = $(this).val();
            if(category != 'interview')
            {
                if(actionName == 'add')
                {
                  	window.location.href = '/admin/ManageContents/add?cat=' + category;
                }
                else
                {
                	var postId = '<?php echo $post['id']; ?>';
                	window.location.href = '/admin/ManageContents/edit/' + postId +'?cat=' +category;
                }
            }
        });

        //event change select box company
        $('#company_id').change(function() {
            
            var id = this.value;
            reloadInterview(id);
        });


        //load interview
        reloadInterview($('#company_id').val());


        function reloadInterview(company_id)
        {
            var id = company_id;
            var secret_id = $('#secret_job_id').data('selected');
            var url = '/admin/ManageInterview/getSecretJobByCompanyId/' + id;
            $.ajax({
                url: url,
                type: 'GET',
                success: function (resp) {
                    var secretJobs = $.parseJSON(resp);
                    $('#secret_job_id').empty();
                    $('#secret_job_id').append($("<option></option>")
                                          .attr("value", 0)
                                          .text("選択してください"));
					
					
					
                    for(i = 0; i < secretJobs.length; i++)
                    {
                        if(secret_id == secretJobs[i]['CareerOpportunity'].id)
                        {
                            $('#secret_job_id').append($("<option></option>")
                                          .attr("value",secretJobs[i]['CareerOpportunity'].id)
                                          .attr("selected", true)
                                          .text(secretJobs[i]['CareerOpportunity'].job));
                        }
                        else
                        {
                            $('#secret_job_id').append($("<option></option>")
                                        .attr("value",secretJobs[i]['CareerOpportunity'].id)
                                        .text(secretJobs[i]['CareerOpportunity'].job));
                        }
                    }
                }
            });
        }

    });
</script>

	
</div>