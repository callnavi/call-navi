<?php 
App::uses('SessionHelper', 'View/Helper');
class FlashExHelper extends AppHelper {
    
    private $saveTemp = null;
    
    public function get()
    {
        if(CakeSession::check('FlashExOTSS'))
        {
            $this->saveTemp = CakeSession::read('FlashExOTSS');
            CakeSession::delete('FlashExOTSS');
            return $this->saveTemp;
        }
    }
    
    public function firstKey($keyName)
    {
        if($this->saveTemp)
        {
            if(isset($this->saveTemp[$keyName][0]))
            {
                return $this->saveTemp[$keyName][0];
            }
        }
        return '';
    }
}

?>