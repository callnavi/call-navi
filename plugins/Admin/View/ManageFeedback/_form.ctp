﻿<?php 
	$post = !empty($item['CareerOpportunities']) ? $item['CareerOpportunities'] : null;
	$btn = !empty($item['CareerOpportunities']) ? '修正' : '新規登録';
	$frmId = !empty($item['CareerOpportunities']) ? 'ManagerJobEditForm' : 'ManagerJobAddForm';
	$ajaxUrl = !empty($item['CareerOpportunities']) ?
        $this->Html->url(array('controller' => 'ManageJob', 'action' => 'edit' ,$post['id'] ), true) :
        $this->Html->url(array('controller' => 'ManageJob', 'action' => 'add'  ), true);

    $this->start('css');
        echo $this->Html->css('fileinput');
        echo $this->element('../Elements/froala_editor_css');
    $this->end('css');
	
	$this->start('script');
        echo $this->Html->script('jquery.validate.min');
        echo $this->Html->script('fileinput');
        echo $this->element('../Elements/froala_editor_js');
    $this->end('script');

 ?>
<div class="job-form-container">
	<?php echo $this->Form->create('ManagerJob', array('type' => 'file','class' => 'form-horizontal')); ?>
    <label  class="control-title">仕事管理</label>

        <input type="submit" class="btn btn-primary pull-right" value="<?php echo $btn ;?>"/>
        <input type="button" style="margin-right: 15px;" class="btn btn-primary pull-right btn-preview" value="プレビュー" />

		<div class="form-group">
		    <label  class="control-label">募集コールセンター名 <span class="require">※必須</span></label>
		    <select name="data[ManagerJob][company_id]" id="company_id" class="form-control">
				<?php foreach($companyList as $company):
					$row=$company['CorporatePr'];
                    $selected = '';
                    if($row['id'] == $post['company_id']){
                        $selected = 'selected';
                    }
                    ?>
					<option <?php echo $selected;?> value="<?php echo $row['id'] ?> "><?php echo $row['company_name'] ;?></option>
				<?php endforeach ?>
			</select>
		</div>

		<div class="form-group">
		    <label class="control-label">職種<span class="require">※必須</span></label>
		        <?php echo $this->Form->input('job',
		            array('class' => 'form-control',
                        'label' => false,
                        'value' =>$post['job'],
                        'id'    => 'job',
						'div'=>false,
                        'placeholder' =>''));
				?>
		</div>
		
		<div class="form-group">
		    <label  class="control-label">雇用形態 <span class="require">※必須</span></label>
				<?php 
					$options=array('正社員'=>'正社員','アルバイト'=>'アルバイト');
                    $post['employment'] = strstr($post['employment'],",") != -1 ? explode(',',$post['employment']): $post['employment'];
                    $selected = array();
                    foreach ($post['employment'] as $employment){
                        array_push($selected, $employment);
                    }
                    echo $this->Form->input('employment', array('multiple' => 'checkbox','label' => false, 'options' => $options, 'selected' => $selected));
                ?>
				
		</div>
		
		<div class="form-group">
		    <label  class="control-label">求人特徴　(複数選択可） <span class="require">※必須</span></label>
				<?php
					$options = array('服装髪型自由' => '服装髪型自由', 'フリーター歓迎' =>'フリーター歓迎', '即日勤務OK' => '即日勤務OK', '駅近' => '駅近','日払い' => '日払い', '主婦歓迎' => '主婦歓迎','転勤なし' => '転勤なし', '短期' => '短期', '登録制' => '登録制','入社祝い金' => '入社祝い金','交通費支給' => '交通費支給' );
                    if(!empty($post['work_features'])) {
                        $post['work_features'] = strstr($post['work_features'], ",") != -1 ? explode(',', $post['work_features']) : $post['work_features'];

                        $selected = array();
                        foreach ($post['work_features'] as $work_features) {
                            array_push($selected, $work_features);
                        }
                    }else{
                        $post['work_features'] ='';
                    }
					echo $this->Form->input('work_features', array('multiple' => 'checkbox','label' => false, 'options' => $options, 'selected' => $selected));
				?>

		</div>

        <div class="form-group">
            <label  class="control-label">どんな仕事か？<span class="require">※必須</span></label>
            <?php echo $this->Form->textarea('what_job',
                array('class' => 'form-control froalaEditor-require',
                    'label' => false,
                    'id'    => 'what_job',
                    'value' => !empty($post['what_job']) ? $post['what_job'] : null,
                    'rows'=> 5,
                    'placeholder' => '')); ?>
        </div>

        <div class="form-group">
            <label class="control-label">勤務地<span class="require">※必須</span></label>
            <?php echo $this->Form->textarea('work_location',
                array('class' => 'form-control froalaEditor-require',
                    'label' => false,
                    'value' => !empty($post['work_location']) ? $post['work_location'] : null,
                    'id'    => 'work_location',
					'rows'=> 5,
                    'placeholder' =>''));
            ?>
        </div>
		
		<div class="form-group">
            <label class="control-label">アクセス<span class="require">※必須</span></label>
            <?php echo $this->Form->textarea('access',
                array('class' => 'form-control froalaEditor-require',
                    'label' => false,
                    'value' => !empty($post['access']) ? $post['access'] : null,
                    'id'    => 'access',
					'rows'=> 5,
                    'placeholder' =>''));
            ?>
        </div>
		
		<div class="form-group">
            <label class="control-label">どんな人が働いていますか？</label>
            <?php echo $this->Form->textarea('what_kind_person',
                array('class' => 'form-control',
                    'label' => false,
                    'value' => !empty($post['what_kind_person']) ? $post['what_kind_person'] : null,
                    'id'    => 'what_kind_person',
					'rows'=> 5,
                    'placeholder' =>''));
            ?>
        </div>
		
		<div class="form-group">
            <label class="control-label">スタッフからのメッセージ</label><br>

            <label class="control-label">上級アバター（最大サイズ：1メガバイト）</label>
            <?php echo $this->Form->input('senior_avata',
                array('type' => 'file',
                    'class' => '',
                    'label' => false,
                    'id' => 'senior_avata')); ?>

            <label class="control-label" >スタッフのお名前</label>
			<?php echo $this->Form->input('senior_name_of_staff',
				array('class' => 'form-control',
					'label' => false,
                    'value' => !empty($post['senior_name_of_staff']) ? $post['senior_name_of_staff'] : null,
					'id'    => 'senior_name_of_staff',
					'div'=>false,
					'placeholder' =>''));
			?>
				
			<label class="control-label" style="margin-top:10px">入社歴</label>
            <?php echo $this->Form->textarea('senior_joining_history',
                array('class' => 'form-control',
                    'label' => false,
                    'value' => !empty($post['senior_joining_history']) ? $post['senior_joining_history'] : null,
                    'id'    => 'senior_joining_history',
					'rows'=> 5,
                    'placeholder' =>''));
            ?>
			
			<label class="control-label" style="margin-top:10px">インタビュー内容</label>
            <?php echo $this->Form->textarea('senior_employee_interview',
                array('class' => 'form-control',
                    'label' => false,
                    'value' => !empty($post['senior_employee_interview']) ? $post['senior_employee_interview'] : null,
                    'id'    => 'senior_employee_interview',
					'rows'=> 5,
                    'placeholder' =>''));
            ?>
        </div>
	<label  class="control-title">募集要項情報</label>
	
        <div class="form-group">
            <label  class="control-label">仕事内容<span class="require">※必須</span></label>
            <?php echo $this->Form->textarea('specific_job',
                array('class' => 'form-control froalaEditor-require',
                    'label' => false,
                    'id'    => 'specific_job',
                    'value' => !empty($post['specific_job']) ? $post['specific_job'] : null,
                    'rows'=> 5,
                    'placeholder' => '')); ?>
        </div>
		
		<div class="form-group">
		    <label class="control-label">対象となる方<span class="require">※必須</span></label>
            <?php echo $this->Form->textarea('person_of_interest',
                array('class' => 'form-control froalaEditor-short froalaEditor-require',
                    'label' => false,
                    'id'    => 'person_of_interest',
                    'value' => !empty($post['person_of_interest']) ? $post['person_of_interest'] : null,
                    'rows'=> 5,
                    'placeholder' => '')); ?>
		</div>
		
		<div class="form-group">
		    <label class="control-label">勤務時間<span class="require">※必須</span></label>
            <?php echo $this->Form->textarea('working_hours',
                array('class' => 'form-control froalaEditor-short froalaEditor-require',
                    'label' => false,
                    'id'    => 'working_hours',
                    'value' => !empty($post['working_hours']) ? $post['working_hours'] : null,
                    'rows'=> 5,
                    'placeholder' => '')); ?>
		</div>
		
		<div class="form-group">
		    <label class="control-label">給与<span class="require">※必須</span></label>
            <?php echo $this->Form->textarea('salary',
                array('class' => 'form-control froalaEditor-short froalaEditor-require',
                    'label' => false,
                    'id'    => 'salary',
                    'value' => !empty($post['salary']) ? $post['salary'] : null,
                    'rows'=> 5,
                    'placeholder' => '')); ?>
		</div>

        <div class="form-group">
            <label class="control-label">業種<span class="require">※必須</span></label>
            <?php echo $this->Form->input('business',
                array('class' => 'form-control',
                    'label' => false,
                    'value' => !empty($post['business']) ? $post['business'] : null,
                    'id'    => 'business',
                    'div'=>false,
                    'placeholder' =>''));
            ?>
        </div>

        <div class="form-group">
            <label class="control-label">時給<span class="require">※必須</span></label>
            <?php echo $this->Form->input('hourly_wage',
                array('class' => 'form-control',
                    'label' => false,
                    'value' => !empty($post['hourly_wage']) ? $post['hourly_wage'] : null,
                    'id'    => 'hourly_wage',
                    'div'=>false,
                    'placeholder' =>''));
            ?>
        </div>
		
		<div class="form-group">
		    <label class="control-label">こんな経験・スキルが活かせます</label>
            <?php echo $this->Form->textarea('skill_experience',
                array('class' => 'form-control froalaEditor-short',
                    'label' => false,
                    'id'    => 'skill_experience',
                    'value' => !empty($post['skill_experience']) ? $post['skill_experience'] : null,
                    'rows'=> 5,
                    'placeholder' => '')); ?>
		</div>
		
		<div class="form-group">
		    <label class="control-label">休日・休暇<span class="require">※必須</span></label>
            <?php echo $this->Form->textarea('holiday_vacation',
                array('class' => 'form-control froalaEditor-short froalaEditor-require',
                    'label' => false,
                    'id'    => 'holiday_vacation',
                    'value' => !empty($post['holiday_vacation']) ? $post['holiday_vacation'] : null,
                    'rows'=> 5,
                    'placeholder' => '')); ?>
		</div>
		
		<div class="form-group">
		    <label class="control-label">待遇・福利厚生<span class="require">※必須</span></label>
            <?php echo $this->Form->textarea('health_welfare',
                array('class' => 'form-control froalaEditor-short froalaEditor-require',
                    'label' => false,
                    'id'    => 'health_welfare',
                    'value' => !empty($post['health_welfare']) ? $post['health_welfare'] : null,
                    'rows'=> 5,
                    'placeholder' => '')); ?>
		</div>
		
		<div class="form-group">
		    <label class="control-label">応募方法<span class="require">※必須</span></label>
            <?php echo $this->Form->textarea('application_method',
                array('class' => 'form-control froalaEditor-short froalaEditor-require',
                    'label' => false,
                    'id'    => 'application_method',
                    'value' => !empty($post['application_method']) ? $post['application_method'] : null,
                    'rows'=> 5,
                    'placeholder' => '')); ?>
		</div>
		
		<div class="form-group">
		    <label class="control-label">入社特典<span class="require">※必須</span></label>
            <?php echo $this->Form->textarea('benefit',
                array('class' => 'form-control froalaEditor-short froalaEditor-require',
                    'label' => false,
                    'id'    => 'benefit',
                    'value' => !empty($post['benefit']) ? $post['benefit'] : null,
                    'rows'=> 5,
                    'placeholder' => '')); ?>
		</div>
		
		<div class="form-group">
		    <label class="control-label">応募先メールアドレス<span class="require">※必須</span></label>
		        <?php echo $this->Form->input('application_email',
		            array('class' => 'form-control ',
                        'label' => false,
                        'value' => !empty($post['application_email']) ? $post['application_email'] : null,
                        'id'    => 'application_email',
						'div'=>false,
                        'placeholder' =>''));
				?>
		</div>
		
		<div class="form-group">
		    <label class="control-label">お問い合わせ先<span class="require">※必須</span></label>
		        <?php echo $this->Form->input('contact',
		            array('class' => 'form-control',
                        'label' => false,
                        'value' => !empty($post['contact']) ? $post['contact'] : null,
                        'id'    => 'contact',
						'div'=>false,
                        'placeholder' =>''));
				?>
		</div>

        <div class="form-group">
            <label  class="control-label">その他備考</label>
            <?php echo $this->Form->textarea('other_remark',
                array('class' => 'form-control',
                    'label' => false,
                    'id'    => 'other_remark',
                    'value' => !empty($post['other_remark']) ? $post['other_remark'] : null,
                    'rows'=> 5,
                    'placeholder' => '')); ?>
        </div>

        <div class="form-group">
            <label  class="control-label">サムネイル（最大サイズ：1メガバイト）</label>
            <?php echo $this->Form->input('thumb_1',
                array('type' => 'file',
                    'class' => 'form-control',
                    'label' => false,
                    'id' => 'thumb_1')); ?>

            <?php echo $this->Form->input('thumb_2',
                array('type' => 'file',
                    'class' => 'form-control',
                    'label' => false,
                    'id' => 'thumb_2')); ?>

            <?php echo $this->Form->input('thumb_3',
                array('type' => 'file',
                    'class' => 'form-control',
                    'label' => false,
                    'id' => 'thumb_3')); ?>
        </div>

		<div class="form-group">
            <label class="btn-publish">
                <input type="checkbox" class="" <?php echo !empty($post['status']) ? "checked" : "" ;?> name="data[ManagerJob][status]" value="1" id="ManagerJobStatus">パブリッシュ
            </label>
			<input type="submit" class="btn btn-primary pull-right" value="<?php echo $btn ;?>"/>
            <input type="button" style="margin-right: 15px;"  class="btn btn-primary pull-right btn-preview" value="プレビュー" />
		</div>

	<?php echo $this->Form->end(null); ?>

<script>
    $( document ).ready(function() {

//Check validate form
        var form = $("#<?php echo $frmId; ?>");
        form.validate({
            errorPlacement: function errorPlacement(error, element) 
			{ element.prev().after(error); },
            onkeyup: false,
            rules: {
                "data[ManagerJob][company]": {
                    required:true,
                },
                "data[ManagerJob][job]": {
                    required: true,
                },
                "data[ManagerJob][employment]":  {
                    required:true,
                },
				"data[ManagerJob][work_features][]":  {
                    required:true,
                },
                "data[ManagerJob][hourly_wage]":  {
                    required:true,
                },
                "data[ManagerJob][business]":  {
                    required:true,
                },
				"data[ManagerJob][application_email]":  {
                    required:true,
                },
				"data[ManagerJob][contact]":  {
                    required:true,
                },

            },

        });
		
		jQuery.extend(jQuery.validator.messages, {
            required: "この項目は必須です。",
        });

        $("#senior_avata").fileinput({
            initialPreview: [ <?php if(!empty($post['senior_avata'])){ ?> '<img src="/upload/secret/jobs/<?php echo $this->base.$post['senior_avata'] ?>" class="" >' <?php } ?>],
            showUpload: false,
            showRemove: false,
            maxFileSize: 1024,
            msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
            allowedFileExtensions: ["jpg", "gif", "png"],
            overwriteInitial: true,
            allowedFileTypes: ["image"],
            previewFileType: ["image"]
        });

        $("#thumb_1").fileinput({
            initialPreview: [ <?php if(!empty($post['thumb_1'])){ ?> '<img src="/upload/secret/jobs/<?php echo $this->base.$post['thumb_1'] ?>" class="" >' <?php } ?>],
            showUpload: false,
            showRemove: false,
            maxFileSize: 1024,
            msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
            allowedFileExtensions: ["jpg", "gif", "png"],
            overwriteInitial: true,
            allowedFileTypes: ["image"],
            previewFileType: ["image"]
        });

        $("#thumb_2").fileinput({
            initialPreview: [ <?php if(!empty($post['thumb_2'])){ ?> '<img src="/upload/secret/jobs/<?php echo $this->base.$post['thumb_2'] ?>" class="" >' <?php } ?>],
            showUpload: false,
            showRemove: false,
            maxFileSize: 1024,
            msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
            allowedFileExtensions: ["jpg", "gif", "png"],
            overwriteInitial: true,
            allowedFileTypes: ["image"],
            previewFileType: ["image"]
        });

        $("#thumb_3").fileinput({
            initialPreview: [ <?php if(!empty($post['thumb_3'])){ ?> '<img src="/upload/secret/jobs/<?php echo $this->base.$post['thumb_3'] ?>" class="" >' <?php } ?>],
            showUpload: false,
            showRemove: false,
            maxFileSize: 1024,
            msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
            allowedFileExtensions: ["jpg", "gif", "png"],
            overwriteInitial: true,
            allowedFileTypes: ["image"],
            previewFileType: ["image"]
        });

        $('.froalaEditor-short').froalaEditor({
            language: 'ja',
            height: 100,
        })
        $('textarea').froalaEditor({
            language: 'ja',
            height: 200,
        })

        $('.btn-preview').click(function() {
            if ($('#<?php echo $frmId; ?>').valid()) {
                var input = $("<input>").attr("name", "isPreview").hide().val("1");
                $('#<?php echo $frmId; ?>').append(input);
                $.ajax({
                    url: "<?php echo $ajaxUrl ;?>",
                    async: false,
                    type: "post",
                    data: $("#<?php echo $frmId; ?>").serialize(),
                    success: function () {
                        window.open(
                            document.location.origin + "/secret/preview/<?php echo !empty($item['CareerOpportunities']['id']) ?
                            $item['CareerOpportunities']['id'] : $lastId ; ?>", '_blank'
                        );
                        <?php if(empty($item['CareerOpportunities'])){ ?>

                        window.location.href = document.location.origin +"/admin/ManageJob/edit/<?php echo $lastId;?>";

                        <?php } ?>

                    }
                });
            }
        });

        // check validate in froalaEditor
        $( "#<?php echo $frmId; ?>" ).submit(function( event ) {
            var validateTextarea = true
            $( ".froalaEditor-require" ).each(function( index ) {
                var content = $(this).froalaEditor('html.get', true);
                if(content == ''){
                    $(this).parent().find( ".control-label" ).after( "<label class='error'>この項目は必須です。</label>" );
                    $(this).parent().find( ".fr-element" ).focus();
                    validateTextarea = false;
                }
            });

            return validateTextarea;
        });

        $('.froalaEditor-require').on('froalaEditor.keyup', function (e, editor, keyupEvent) {
            var content = $(this).froalaEditor('html.get', true);
            if(content == '')
                $(this).parent().find( ".control-label" ).after( "<label class='error'>この項目は必須です。</label>" );
            else
                $(this).parent().find( ".error").remove();
        });

    });

</script>
	
</div>