<?php
	$this->set('title', 'コールナビ｜意見管理');
	$this->start('css'); 
?>
	<link rel="stylesheet" href="<?php echo $this->base;?>/admin/css/common.css" type="text/css" media="all">
	<link rel="stylesheet" href="<?php echo $this->base;?>/admin/css/manage-job.css" type="text/css" media="all">
<?php $this->end('css'); ?>

<?php 
	$limit = $this->Paginator->params()['limit'];
	$total = $this->Paginator->params()['count'];
	$currentPage = $this->Paginator->params()['page'];
	$pageCount = $this->Paginator->params()['pageCount'];
	$start = (($currentPage - 1) * $limit) + 1;
    $end = $currentPage == $pageCount ? $total : $currentPage * $limit;
?>

<div id="" class="container-fluid feedback">
	<div class="row">
		<div class="col-md-2 padding-right-10">
			<?php echo $this->element('../Elements/left_menu'); ?> 
		</div>

		<div class="col-md-10 padding-left-10">
			<div class="search-form">
				<?php echo $this->Form->create('Searchjob', array('url' => 'index','class' => 'form-horizontal form-row-5', 'type' => 'get')); ?>
				<div class="col-md-7 padding-5">
					<?php echo $this->Form->input('keywords',
						array('class' => 'form-control',
							'label' => false,
							'div' => false,
							'value' =>!empty($keywords) ? $keywords : '',
							'id'    => 'keywords',
							'placeholder' =>'検索キーワードを入力してください'));
					?>
				</div>
				<div class="col-md-2">
					<input type="submit" class="btn btn-primary" value="検索"/>
				</div>
				<?php echo $this->Form->end(null); ?>

			</div>
			<div class="col-md-12 no-padding">
				<p><?php echo $total ;?>件中 
				   <?php if ($total>0): ?>
				   <?php echo $start ;?> - <?php echo $end ;?>件を表示
				   <?php endif; ?>
				</p>
			</div>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th style="width: 40px"></th>
						<th>内容</th>
						<th class="box-date" style="width: 180px">作成日</th>
					</tr>
				</thead>
				<tbody>
					<?php
					if(!empty($list)) {
						foreach ($list as $index=>$record):
							$post = $record['Feedback'];
							$created = strtotime($post['created']) ?>
							<tr>
								<td><?php echo $index+1; ?></td>
								<td><?php echo $post['message']; ?></td>
								<td class="box-date"><?php echo $post['created']; ?></td>
							</tr>
						<?php endforeach;
					}else{ ?>
						<tr>
							<td colspan="4">
								<p>データなし</p>
							</td>
						</tr>
					<?php }?>
				</tbody>
			 </table>
            <?php echo $this->element('../Elements/pagination'); ?>
      </div>
   </div>
</div>