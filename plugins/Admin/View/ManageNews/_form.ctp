﻿<?php 
	$post = !empty($item[0]['econtents']) ? $item[0]['econtents'] : null;
	$btn = !empty($item[0]['econtents']) ? '修正' : '新規登録';
	$frmId = !empty($item[0]['econtents']) ? 'ManageNewsEditForm' : 'ManageNewsAddForm';
    $ajaxUrl = !empty($item[0]['econtents']) ?
        $this->Html->url(array('controller' => 'ManageNews', 'action' => 'edit' ,$post['id'] ), true) :
        $this->Html->url(array('controller' => 'ManageNews', 'action' => 'add'  ), true);
	
	$this->start('css');
        echo $this->Html->css('fileinput');
        echo $this->element('../Elements/froala_editor_css');
        echo $this->element('../Elements/select2_css');
    $this->end('css');
	
	$this->start('script');
        echo $this->Html->script('jquery.validate.min');
        echo $this->Html->script('fileinput');
        echo $this->element('../Elements/froala_editor_js');
        echo $this->element('../Elements/select2_js');
    $this->end('script');
$catNames = !empty($item) ? explode(',',$item[0]['group_cat']['cat_names']) : null;
?>
<div class="news-form-container">
	<?php echo $this->Form->create('ManageNews', array('type' => 'file','class' => 'form-horizontal')); ?>
    <label  class="control-title">ニュース管理</label>

        <input type="submit" class="btn btn-primary pull-right" value="<?php echo $btn ;?>"/>
        <input type="button" style="margin-right: 15px;" class="btn btn-primary pull-right btn-preview" value="プレビュー" />

        <div class="form-group">
            <label  class="control-label">カテゴリ<span class="require">※必須</span></label>
            <select name="data[ManageNews][Categories][]" id="Categories" class="form-control" multiple >
                <?php foreach($catList as $cat) {
                    $param = $cat['Categories'];
                    $selected = '';
                    if (!empty($catNames)) {
                        foreach ($catNames as $cat) {
                            if ($cat == $param['category']) {
                                $selected = ' selected="selected" ';
                                break;
                            }
                        }
                    }
                    ?>
                    <option <?php echo $selected;?> value="<?php echo $param['email_to'];?>"><?php echo $param['category'] ;?></option>
                <?php } ?>
            </select>
        </div>

		<div class="form-group">
		   <label class="control-label">表示イメージ（最大サイズ：2メガバイト）</label>
			<?php echo $this->Form->input('featured_picture',
				array('type' => 'file',
					'class' => '',
					'label' => false,
					'id' => 'featured_picture')); ?>
		</div>

        <div class="form-group">
            <label class="control-label" >タイトル<span class="require">※必須</span></label>
            <?php echo $this->Form->input('Title',
                array('class' => 'form-control',
                    'label' => false,
                    'value' =>$post['Title'],
                    'id'    => 'Title',
                    'placeholder' =>''));
            ?>
        </div>
		
		<div class="form-group">
		    <label  class="control-label">件名</label>
		            <?php echo $this->Form->textarea('subject',
                        array('class' => 'form-control textarea',
                            'label' => false,
                            'id'    => 'subject',
                            'value' =>$post['subject'],
                            'rows'=> 10,
                            'placeholder' => '')); ?>
		</div>

        <div class="form-group">
            <label  class="control-label">サブタイトル</label>
            <?php echo $this->Form->textarea('Subtitle',
                array('class' => 'form-control textarea',
                    'label' => false,
                    'id'    => 'Subtitle',
                    'value' =>$post['Subtitle'],
                    'rows'=> 10,
                    'placeholder' => '')); ?>
        </div>

        <div class="form-group">
            <label  class="control-label">概要</label>
            <?php echo $this->Form->textarea('Excerpt',
                array('class' => 'form-control textarea',
                    'label' => false,
                    'id'    => 'Excerpt',
                    'value' =>$post['Excerpt'],
                    'rows'=> 10,
                    'placeholder' => '')); ?>
        </div>

        <div class="form-group">
            <label  class="control-label">コンテンツ</label>
            <?php echo $this->Form->textarea('content',
                array('class' => 'form-control textarea',
                    'label' => false,
                    'id'    => 'content',
                    'value' =>$post['content'],
                    'rows'=> 10,
                    'placeholder' => '')); ?>
        </div>

        <div class="form-group">
            <label class="control-label" >関連リンク</label>
            <?php echo $this->Form->input('relation_link',
                array('class' => 'form-control',
                    'label' => false,
                    'value' =>$post['relation_link'],
                    'id'    => 'relation_link',
                    'placeholder' =>''));
            ?>
        </div>

        <div class="form-group">
            <label class="control-label" >コンテンツ引用元</label>
            <?php echo $this->Form->input('source',
                array('class' => 'form-control',
                    'label' => false,
                    'value' =>$post['source'],
                    'id'    => 'source',
                    'placeholder' =>''));
            ?>
        </div>

		<div class="form-group">
			<input type="submit" class="btn btn-primary pull-right" value="<?php echo $btn ;?>"/>
            <input type="button" style="margin-right: 15px;" class="btn btn-primary pull-right btn-preview" value="プレビュー" />
		</div>

	<?php echo $this->Form->end(null); ?>
	
<script>
    $( document ).ready(function() {

    //Check validate form
        var form = $("#<?php echo $frmId; ?>");
        form.validate({
            errorPlacement: function errorPlacement(error, element) { element.before(error); },
            onkeyup: false,
            rules: {
                "data[ManageNews][Title]": {
                    required:true,
                },
                "data[ManageNews][Categories][]": {
                    required:true,
                },

            },

        });
		
		jQuery.extend(jQuery.validator.messages, {
            required: "この項目は必須です。",
        });
        <?php

            if(!empty($post['featured_picture'])){
                $pos = strpos($post['featured_picture'], "http://");
                if($pos !== false)
                    $src = $post['featured_picture'];
                else
                    $src= '/upload/news/'.$post['featured_picture'];
            }else
                $src= null;
        ?>
		$("#featured_picture").fileinput({
			initialPreview: [ <?php if(!empty($src)){ ?> '<img src="<?php echo $src ?>" class="" >' <?php } ?>],
			showUpload: false,
			showRemove: false,
			maxFileSize: 2048,
			msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
			allowedFileExtensions: ["jpg", "gif", "png"],
			overwriteInitial: true,
			allowedFileTypes: ["image"],
			previewFileType: ["image"]
		});

        $('#content').froalaEditor({
            language: 'ja',
            height: 400,
        })

        $('.textarea').froalaEditor({
            language: 'ja',
            //height: 200,
        })

        $("#Categories").select2({
            placeholder: "Select a category",
            allowClear: true
        });

        $('.btn-preview').click(function() {
            if ($('#<?php echo $frmId; ?>').valid()) {
                var input = $("<input>").attr("name", "isPreview").hide().val("1");
                $('#<?php echo $frmId; ?>').append(input);
                $.ajax({
                    url: "<?php echo $ajaxUrl ;?>",
                    async: false,
                    type: "post",
                    data: $("#<?php echo $frmId; ?>").serialize(),
                    success: function () {
                        window.open(
                            document.location.origin + "/news/preview/<?php echo !empty($post['id']) ? $post['id'] : $lastId ; ?>", '_blank'
                        );
                        <?php if(empty($item[0]['econtents'])){ ?>
                            window.location.href = document.location.origin +"/admin/ManageNews/edit/<?php echo $lastId;?>";
                        <?php } ?>

                    }
                });
            }
        });
    });
</script>

	
</div>