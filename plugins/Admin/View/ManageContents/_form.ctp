<?php
    $actionName = $this->request->params['action'];
    $catName = $this->request->query('cat');
    

	$post = !empty($item['Contents']) ? $item['Contents'] : null;
	$btn = !empty($item['Contents']) ? '修正' : '新規登録';
	$frmId = !empty($item['Contents']) ? 'ManagerContentsEditForm' : 'ManagerContentsAddForm';

    $ajaxUrl = !empty($item['Contents']) ?
        $this->Html->url(array('controller' => 'ManageContents', 'action' => 'edit' ,$post['id'] ), true) :
        $this->Html->url(array('controller' => 'ManageContents', 'action' => 'add'  ), true);
	
	$this->start('css');
        echo $this->Html->css('fileinput');
        echo $this->element('../Elements/froala_editor_css');
    $this->end('css');
	
	$this->start('script');
        echo $this->Html->script('jquery.validate.min');
        echo $this->Html->script('fileinput');
        echo $this->element('../Elements/froala_editor_js');
    $this->end('script');
 ?>

<div class="news-form-container">
	<?php echo $this->Form->create('ManagerContents', array('type' => 'file','class' => 'form-horizontal')); ?>
    <label  class="control-title">コンテンツ管理</label>

        <input type="submit" class="btn btn-primary pull-right" value="<?php echo $btn ;?>"/>
        <input type="button" style="margin-right: 15px;" class="btn btn-primary pull-right btn-preview" value="プレビュー" />

        <div class="form-group">
            <label  class="control-label">カテゴリー<span class="require">※必須</span></label>
            <select name="data[ManagerContents][category_alias]" id="category_id" class="form-control">
                <?php foreach($category as $item):
                    $row=$item['Categories'];
                    if($post['category_alias'] == $row['category_alias'] ){?>
                        <option  value="<?php echo $row['category_alias'];?>" selected><?php echo $row['category'] ;?></option>
                    <?php }else{ ?>
                        <option  value="<?php echo $row['category_alias'];?>" ><?php echo $row['category'] ;?></option>
                <?php } endforeach ?>
            </select>
        </div>

		<div class="form-group">
            <label class="control-label" >タイトル <span class="require">※必須</span></label>
            <?php echo $this->Form->input('title',
                array('class' => 'form-control',
                    'label' => false,
                    'value' =>$post['title'],
                    'id'    => 'title',
                    'placeholder' =>''));
            ?>
		</div>

        <div class="form-group">
            <label class="control-label" >タイトル別名<span class="require">※必須</span></label>
            <?php echo $this->Form->input('title_alias',
                array('class' => 'form-control',
                    'label' => false,
                    'value' =>$post['title_alias'],
                    'id'    => 'title_alias',
                    'placeholder' =>''));
            ?>
        </div>

        <div class="form-group">
          <label class="control-label" >ディスクリプション</label>
          <?php echo $this->Form->input('meta_description',
            array('class' => 'form-control',
              'label' => false,
              'type' => 'textarea',
              'value' =>$post['meta_description'],
              'id'    => 'meta_description',
              'rows' => 4,
              'placeholder' =>'Description'));
          ?>
        </div>
        <div class="form-group">
          <label class="control-label" >キーワード</label>
          <?php echo $this->Form->input('meta_keyword',
            array('class' => 'form-control',
              'label' => false,
              'type' => 'textarea',
              'value' =>$post['meta_keyword'],
              'id'    => 'meta_keyword',
              'rows' => 3,
              'placeholder' =>'Keyword'));
          ?>
        </div>

        <div class="form-group">
            <label class="control-label" >画像z（最大サイズ：1メガバイト）</label>
            <?php echo $this->Form->input('image',
                array('type' => 'file',
                    'class' => 'form-control',
                    'label' => false,
                    'id' => 'image')); ?>
        </div>


    <div class="form-group">
		    <label  class="control-label">説明 </label>
		            <?php echo $this->Form->textarea('description',
                        array('class' => 'form-control textarea',
                            'label' => false,
                            'id'    => 'description',
                            'value' =>$post['description'],
                            'rows'=> 10,
                            'placeholder' => '')); ?>
		</div>

        <div class="form-group">
            <label  class="control-label">コンテンツ</label>
            <?php echo $this->Form->textarea('content',
                array('class' => 'form-control textarea',
                    'label' => false,
                    'id'    => 'content',
                    'value' =>$post['content'],
                    'rows'=> 10,
                    'placeholder' => '')); ?>
        </div>

		<div class="form-group">
			<label class="control-label">作成日</label>
			<?php echo $this->Form->input('created',
				array('class' => '',
					'label' => false,
					'type' => 'date',
					'id' => 'created',
					'value' => !empty($post['created']) ? $post['created'] : null ,
					'dateFormat' => 'YMD',
					'required' => false,
					'monthNames' => array(1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9,10=>10,11=>11,12=>12)
				)); ?>
    	</div>

		<div class="form-group">
			<input type="submit" class="btn btn-primary pull-right" value="<?php echo $btn ;?>"/>
            <input type="button" style="margin-right: 15px;" class="btn btn-primary pull-right btn-preview" value="SP-プレビュ" data-id="SP"/>
            <input type="button" style="margin-right: 15px;" class="btn btn-primary pull-right btn-preview" value="PC-プレビュ" data-id="PC"/>
		</div>

	<?php echo $this->Form->end(null); ?>
	
<script>
    $( document ).ready(function() {

//Check validate form
        var form = $("#<?php echo $frmId; ?>");
        form.validate({
            errorPlacement: function errorPlacement(error, element) { element.before(error); },
            onkeyup: false,
            rules: {
                "data[ManagerContents][title_alias]": {
                    required:true,
                },
                "data[ManagerContents][title]": {
                    required: true,
                },

            },

        });
		
		jQuery.extend(jQuery.validator.messages, {
            required: "この項目は必須です。",
        });
		<?php

		    if(!empty($post['image'])){
		        $pos = strpos($post['image'], "ccwork");
		        if($pos !== false)
		            $src = $post['image'];
                else
		            $src= '/upload/contents/'.$post['image'];
		    }else
		        $src= null;
		?>
		$("#image").fileinput({
			initialPreview: [ <?php if(!empty($src)){ ?> '<img src="<?php echo $src ?>" class="" >' <?php } ?>],
			showUpload: false,
			showRemove: false,
			maxFileSize: 1024,
			msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
			allowedFileExtensions: ["jpg", "gif", "png"],
			overwriteInitial: true,
			allowedFileTypes: ["image"],
			previewFileType: ["image"]
		});
        $('textarea.textarea').froalaEditor({
            language: 'ja',
            height: 200,
        })

        $('.btn-preview').click(function() {
            if ($('#<?php echo $frmId; ?>').valid()) {
                var input = $("<input>").attr("name", "isPreview").hide().val("1");
                //kind preview PC or SP
                var kindPreview = $(this).attr('data-id');
                $('#<?php echo $frmId; ?>').append(input);
                $.ajax({
                    url: "<?php echo $ajaxUrl ;?>",
                    async: false,
                    type: "post",
                    data: $("#<?php echo $frmId; ?>").serialize(),
                    success: function () {
                        if(kindPreview == 'PC')
                        {
                            window.open(
                                document.location.origin + "/contents/preview/<?php echo !empty($post['id']) ? $post['id'] : $lastId ; ?>/"+ kindPreview, '_blank'
                            );
                        }
                        else
                        {
                            window.open(
                                document.location.origin + "/contents/preview/<?php echo !empty($post['id']) ? $post['id'] : $lastId ; ?>/"+ kindPreview, '_blank', "width=414,height=650,resizeable,scrollbars"
                            );
                        }
                        <?php if(empty($post)){ ?>
                            window.location.href = document.location.origin +"/admin/ManageContents/edit/<?php echo $lastId;?>";
                        <?php } ?>

                    }
                });
            }
        });


        //set selected value for category_id if url has param cat
        var catName = "<?php echo isset($catName) ? $catName : '' ?>";
        if(catName)
        {
            $('#category_id option[value=' + catName + ']').attr('selected','selected');
        }


        var actionName = '<?php echo $actionName; ?>';
        
        $('#category_id').change(function(){
            var category = $(this).val();
            if(category == 'interview')
            {
                if(actionName == 'add')
                {
                    window.location.href = '/admin/ManageContents/add-interview';
                }
                else
                {
                    var postId = '<?php echo $post['id']; ?>';
                    window.location.href = '/admin/ManageContents/edit-interview/'+ postId;
                }
            }
        });
    });
</script>

	
</div>