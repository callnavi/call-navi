<?php
$this->set('title', 'コールナビ｜コンテンツ管理');
$this->start('css'); ?>
<link rel="stylesheet" href="<?php echo $this->base;?>/admin/css/common.css" type="text/css" media="all">
<link rel="stylesheet" href="<?php echo $this->base;?>/admin/css/manage-news.css" type="text/css" media="all">
<?php $this->end('css'); ?>

<div id="manage-news-edit" class="container-fluid">
    <div class="row">
        <div class="col-md-2 padding-right-10">
            <?php echo $this->element('../Elements/left_menu'); ?>
        </div>
        <div class="col-md-8 padding-left-10">
            <div id="edit-news" ><?php echo $this->element('../ManageContents/_form'); ?>
                <br>
            </div>
        </div>
    </div>
</div>
