<?php
$post = !empty($item['CareerOpportunities']) ? $item['CareerOpportunities'] : null;
$btn = !empty($item['CareerOpportunities']) ? '修正' : '新規登録';
$frmId = !empty($item['CareerOpportunities']) ? 'ManagerJobEditForm' : 'ManagerJobAddForm';
$ajaxUrl = !empty($item['CareerOpportunities']) ?
    $this->Html->url(array('controller' => 'ManageJob', 'action' => 'edit', $post['id']), true) :
    $this->Html->url(array('controller' => 'ManageJob', 'action' => 'add'), true);

$company_name = "";
$website = "";
$office_location = "";
if(!empty($item['CareerOpportunities'])){
foreach ($companyList as $company){
    $row = $company['CorporatePr'];
    $selected = '';
        if ($row['id'] == $post['company_id']) {
            $company_name = $row['company_name'];
            $website = $row['website'];
            $office_location = $row['office_location'];
            
        }
    }   
}
$this->start('css');
echo $this->Html->css('fileinput');
echo $this->element('../Elements/froala_editor_css');
$this->end('css');

$this->start('script');
echo $this->Html->script('jquery.validate.min');
echo $this->Html->script('fileinput');
echo $this->element('../Elements/froala_editor_js');
echo $this->Html->script('init-googlemap'); ?>
<script src="https://maps.google.com/maps/api/js?libraries=geometry&key=<?php
            echo Configure::read('webconfig.google_map_api_key'); ?>"></script>
<?php
$this->end('script');


    // validate for double id post back
    if(empty($job_code_1)){
        if ($post['job_code'] ){
            $job_code = $post['job_code'];
        }else{
            //delete generated code
            //$job_code =  $this->App->generateNewJobCode();
            $job_code =  "";
        }        
    }else{
        $job_code = $job_code_1;
    }
	

?>
<div class="job-form-container">
    <?php echo $this->Form->create('ManagerJob', array('type' => 'file', 'class' => 'form-horizontal')); ?>
    <label class="control-title" style="font-size: 18px;
    margin-bottom: 30px;">仕事管理</label>

    <input type="submit" class="btn btn-primary pull-right" value="<?php echo $btn; ?>"/>
    <input type="button" style="margin-right: 15px;" class="btn btn-primary pull-right btn-preview" value="プレビュー"/>
    

    <div class="form-group">
        <label class="control-label">求人ID<!--<span class="require">※必須</span>--> <span style="color:red;"><?php if (!empty($error_message_code)) { echo($error_message_code); }  ?></span></label>
        <?php echo $this->Form->input('job_code',
            array('class' => 'form-control',
                'label' => false,
                'value' => $job_code,
                'id' => 'job_code',
                'div' => false,
                'placeholder' => ''));
        ?>
                <?php echo $this->Form->hidden('job_code_hidden',
		            array(
                        'value' =>$post['job_code'],
                        'id'    => 'job_code_check',
                        ));
				?>        
    </div>
    
    <div class="form-group">
        <label class="control-label">募集コールセンター名 <!--<span class="require">※必須</span>--></label>
        <select name="data[ManagerJob][company_id]" id="company_id" class="form-control">
            <?php foreach ($companyList as $company):
                $row = $company['CorporatePr'];
                $selected = '';
                if ($row['id'] == $post['company_id']) {
                    $selected = 'selected';
                }
                ?>
                <option <?php echo $selected; ?>
                    value="<?php echo $row['id'] ?> "><?php echo $row['company_name']; ?></option>
            <?php endforeach ?>
        </select>
    </div>


    <div class="form-group">
        <label class="control-label">募集コールセンター名（支店名）<!--<span class="require">※必須</span>--></label>
        <?php echo $this->Form->input('branch_name',
            array('class' => 'form-control',
                'label' => false,
                'value' => $post['branch_name'],
                'id' => 'job',
                'div' => false,
                'placeholder' => ''));
        ?>
    </div>


    
    <div class="form-group">
        <label class="control-label">求人タイトル<!--<span class="require">※必須</span>--></label>
        <?php echo $this->Form->input('job',
            array('class' => 'form-control',
                'label' => false,
                'value' => $post['job'],
                'id' => 'job',
                'div' => false,
                'placeholder' => ''));
        ?>
    </div>

        <div class="form-group">
        <label class="control-label">お祝い金</label>
        <?php echo $this->Form->input('celebration',
            array('class' => 'form-control',
                'label' => false,
                'value' => $post['celebration'],
                'id' => 'job',
                'div' => false,
                'placeholder' => ''));
        ?>
    </div>    

    <!-- Field from last company -->
		<div class="form-group">
		   <label class="control-label">トップ画像（最大サイズ：1メガバイト）</label>
			<?php echo $this->Form->input('corporate_logo',
				array('type' => 'file',
					'class' => '',
					'label' => false,
					'id' => 'corporate_logo')); ?>
		</div>
        
        <!-- delete data of 企業PR field in company -->


        <div class="form-group">
            <label  class="control-label">コールナビ編集部コメント</label>
            <?php echo $this->Form->textarea('comment',
                array('class' => 'form-control textarea',
                    'label' => false,
                    'id'    => 'comment',
                    'value' =>$post['comment'],
                    'rows'=> 5,
                    'placeholder' => '')); ?>
        </div>

        <div class="form-group">
            <label class="control-label lbl-map-location">Map Location</label>
            <?php echo $this->Form->input('map_location',
                array('class' => 'form-control input-map-location ',
                    'label' => false,
                    'div' => false,
                    'value' =>$post['map_location'],
                    'id'    => 'map_location',
                    'placeholder' =>''));
            ?>
            <input id="find_location" type="button" style="" class="btn btn-primary pull-right btn-map-location" value="Find Localtion" />
            <div id="map"></div>

            <?php echo $this->Form->input('lat',
                array(
                    'type' => 'hidden',
                    'id'=> 'lat',
                    'value' =>$post['lat'],
                )); ?>
            <?php echo $this->Form->input('lng',
                array(
                    'type' => 'hidden',
                    'id'=>'lng',
                    'value' =>$post['lng'],
                )); ?>
        </div>

    <div class="form-group">
        <label class="control-label">公開日</label>
        <?php echo $this->Form->input('release_date',
            array('class' => '',
                'label' => false,
                'type' => 'date',
                'id' => 'release_date',
                'value' => !empty($post['release_date']) ? $post['release_date'] : null ,
                'dateFormat' => 'YMD',
                'required' => false,
				'monthNames' => array(1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9,10=>10,11=>11,12=>12)
            )); ?>
    </div>
    
    <div class="form-group">
        <label class="control-label">全地域の検索</label>
        <input type="checkbox" class="" <?php echo !empty($post['is_search']) ? "checked" : ""; ?>
               name="data[ManagerJob][is_search]" value="1" id="ManagerJob_Is_search">
         <div>全地域の検索でもHITできるようにする。</div>
    </div>

    <div class="form-group">
        <label class="control-label">雇用形態 <!--<span class="require">※必須</span>--></label>
        <?php
		$options = array();
		$emData = Configure::read('webconfig.employment_data');
		foreach( $emData as $em)
		{
			$options[$em] = $em;
		}
        $post['employment'] = strstr($post['employment'], ",") != -1 ? 
							  explode(',', $post['employment']) : $post['employment'];
        $selected = array();
        foreach ($post['employment'] as $employment) {
            array_push($selected, $employment);
        }
        echo $this->Form->input('employment', array('multiple' => 'checkbox', 
													'label' => false, 
													'options' => $options, 
													'selected' => $selected));
        ?>

    </div>

    <div class="form-group">
        <label class="control-label">求人特徴　(複数選択可） <!--<span class="require">※必須</span>--></label>
        <?php
        $options = array('服装髪型自由' => '服装髪型自由', 'フリーター歓迎' => 'フリーター歓迎', '即日勤務OK' => '即日勤務OK', '駅近' => '駅近', '日払い' => '日払い', '主婦歓迎' => '主婦歓迎', '転勤なし' => '転勤なし', '短期' => '短期', '登録制' => '登録制', '入社祝い金' => '入社祝い金', '交通費支給' => '交通費支給');
        if (!empty($post['work_features'])) {
            $post['work_features'] = strstr($post['work_features'], ",") != -1 ? explode(',', $post['work_features']) : $post['work_features'];

            $selected = array();
            foreach ($post['work_features'] as $work_features) {
                array_push($selected, $work_features);
            }
        } else {
            $post['work_features'] = '';
        }
        echo $this->Form->input('work_features', array('multiple' => 'checkbox', 'label' => false, 'options' => $options, 'selected' => $selected));
        ?>

    </div>

    <div class="form-group">
        <label class="control-label">仕事内容（概要）<!--<span class="require">※必須</span>--></label>
        <?php echo $this->Form->textarea('what_job',
            array('class' => 'form-control  textarea',
                'label' => false,
                'id' => 'what_job',
                'value' => !empty($post['what_job']) ? $post['what_job'] : null,
                'rows' => 5,
                'placeholder' => '')); ?>
    </div>

    <div class="form-group">
        <label class="control-label">勤務地<!--<span class="require">※必須</span>-->
            <span>
            <input type="checkbox" class="" <?php echo !empty($post['map_show']) ? "checked" : ""; ?>
					   name="data[ManagerJob][map_show]" value="1" id="ManagerJob_map_show">表示
            </span>
        </label>
        <?php echo $this->Form->textarea('work_location',
            array('class' => 'form-control  textarea',
                'label' => false,
                'value' => !empty($post['work_location']) ? $post['work_location'] : null,
                'id' => 'work_location',
                'rows' => 5,
                'placeholder' => ''));
        ?>
    </div>

    <div class="form-group">
        <label class="control-label">アクセス<!--<span class="require">※必須</span>--></label>
        <?php echo $this->Form->textarea('access',
            array('class' => 'form-control  textarea',
                'label' => false,
                'value' => !empty($post['access']) ? $post['access'] : null,
                'id' => 'access',
                'rows' => 5,
                'placeholder' => ''));
        ?>
    </div>

    <div class="form-group">
        <label class="control-label">どんな人が働いていますか？</label>
        <?php echo $this->Form->textarea('what_kind_person',
            array('class' => 'form-control textarea',
                'label' => false,
                'value' => !empty($post['what_kind_person']) ? $post['what_kind_person'] : null,
                'id' => 'what_kind_person',
                'rows' => 5,
                'placeholder' => ''));
        ?>
    </div>

    <div class="form-group">
        <label class="control-label">スタッフからのメッセージ</label><br>

        <label class="control-label">上級アバター（最大サイズ：1メガバイト）</label>
        <?php echo $this->Form->input('senior_avata',
            array('type' => 'file',
                'class' => '',
                'label' => false,
                'id' => 'senior_avata')); ?>

        <label class="control-label">スタッフのお名前</label>
        <?php echo $this->Form->input('senior_name_of_staff',
            array('class' => 'form-control',
                'label' => false,
                'value' => !empty($post['senior_name_of_staff']) ? $post['senior_name_of_staff'] : null,
                'id' => 'senior_name_of_staff',
                'div' => false,
                'placeholder' => ''));
        ?>

        <label class="control-label" style="margin-top:10px">インタビュータイトル</label>
        <?php echo $this->Form->textarea('senior_joining_history',
            array('class' => 'form-control textarea',
                'label' => false,
                'value' => !empty($post['senior_joining_history']) ? $post['senior_joining_history'] : null,
                'id' => 'senior_joining_history',
                'rows' => 5,
                'placeholder' => ''));
        ?>

        <label class="control-label" style="margin-top:10px">インタビュー内容</label>
        <?php echo $this->Form->textarea('senior_employee_interview',
            array('class' => 'form-control textarea',
                'label' => false,
                'value' => !empty($post['senior_employee_interview']) ? $post['senior_employee_interview'] : null,
                'id' => 'senior_employee_interview',
                'rows' => 5,
                'placeholder' => ''));
        ?>
    </div>
    <label class="control-title">募集要項情報</label>

    <div class="form-group">
        <label class="control-label">仕事内容（詳細）<!--<span class="require">※必須</span>--></label>
        <?php echo $this->Form->textarea('specific_job',
            array('class' => 'form-control  textarea',
                'label' => false,
                'id' => 'specific_job',
                'value' => !empty($post['specific_job']) ? $post['specific_job'] : null,
                'rows' => 5,
                'placeholder' => '')); ?>
    </div>

    <div class="form-group">
        <label class="control-label">対象となる方<!--<span class="require">※必須</span>--></label>
        <?php echo $this->Form->textarea('person_of_interest',
            array('class' => 'form-control froalaEditor-short ',
                'label' => false,
                'id' => 'person_of_interest',
                'value' => !empty($post['person_of_interest']) ? $post['person_of_interest'] : null,
                'rows' => 5,
                'placeholder' => '')); ?>
    </div>

    <div class="form-group">
        <label class="control-label">勤務時間<!--<span class="require">※必須</span>--></label>
        <?php echo $this->Form->textarea('working_hours',
            array('class' => 'form-control froalaEditor-short ',
                'label' => false,
                'id' => 'working_hours',
                'value' => !empty($post['working_hours']) ? $post['working_hours'] : null,
                'rows' => 5,
                'placeholder' => '')); ?>
    </div>

    <div class="form-group">
        <label class="control-label">給与<!--<span class="require">※必須</span>--></label>
        <?php echo $this->Form->textarea('salary',
            array('class' => 'form-control froalaEditor-short ',
                'label' => false,
                'id' => 'salary',
                'value' => !empty($post['salary']) ? $post['salary'] : null,
                'rows' => 5,
                'placeholder' => '')); ?>
    </div>

    <div class="form-group">
        <label class="control-label">職種<!--<span class="require">※必須</span>--></label>
        <?php echo $this->Form->input('business',
            array('class' => 'form-control',
                'label' => false,
                'value' => !empty($post['business']) ? $post['business'] : null,
                'id' => 'business',
                'div' => false,
                'placeholder' => ''));
        ?>
    </div>

    <div class="form-group">
        <label class="control-label">給与（見出し）<!--<span class="require">※必須</span>--></label>
        <?php echo $this->Form->input('hourly_wage',
            array('class' => 'form-control',
                'label' => false,
                'value' => !empty($post['hourly_wage']) ? $post['hourly_wage'] : null,
                'id' => 'hourly_wage',
                'div' => false,
                'placeholder' => ''));
        ?>
    </div>

    <div class="form-group">
        <label class="control-label">こんな経験・スキルが活かせます</label>
        <?php echo $this->Form->textarea('skill_experience',
            array('class' => 'form-control froalaEditor-short',
                'label' => false,
                'id' => 'skill_experience',
                'value' => !empty($post['skill_experience']) ? $post['skill_experience'] : null,
                'rows' => 5,
                'placeholder' => '')); ?>
    </div>

    <div class="form-group">
        <label class="control-label">休日・休暇<!--<span class="require">※必須</span>--></label>
        <?php echo $this->Form->textarea('holiday_vacation',
            array('class' => 'form-control froalaEditor-short ',
                'label' => false,
                'id' => 'holiday_vacation',
                'value' => !empty($post['holiday_vacation']) ? $post['holiday_vacation'] : null,
                'rows' => 5,
                'placeholder' => '')); ?>
    </div>

    <div class="form-group">
        <label class="control-label">待遇・福利厚生<!--<span class="require">※必須</span>--></label>
        <?php echo $this->Form->textarea('health_welfare',
            array('class' => 'form-control froalaEditor-short ',
                'label' => false,
                'id' => 'health_welfare',
                'value' => !empty($post['health_welfare']) ? $post['health_welfare'] : null,
                'rows' => 5,
                'placeholder' => '')); ?>
    </div>

    <div class="form-group">
        <label class="control-label">応募方法<!--<span class="require">※必須</span>--></label>
        <?php echo $this->Form->textarea('application_method',
            array('class' => 'form-control froalaEditor-short ',
                'label' => false,
                'id' => 'application_method',
                'value' => !empty($post['application_method']) ? $post['application_method'] : null,
                'rows' => 5,
                'placeholder' => '')); ?>
    </div>

    <div class="form-group">
        <label class="control-label">入社特典</label>
        <?php echo $this->Form->textarea('benefit',
            array('class' => 'form-control froalaEditor-short',
                'label' => false,
                'id' => 'benefit',
                'value' => !empty($post['benefit']) ? $post['benefit'] : null,
                'rows' => 5,
                'placeholder' => '')); ?>
    </div>

    <div class="form-group">
        <label class="control-label">応募先メールアドレス<!--<span class="require">※必須</span>--></label>
        <?php echo $this->Form->input('application_email',
            array('class' => 'form-control ',
                'label' => false,
                'value' => !empty($post['application_email']) ? $post['application_email'] : null,
                'id' => 'application_email',
                'div' => false,
                'placeholder' => ''));
        ?>
    </div>

   <!-- <div class="form-group">
        <label class="control-label">お問い合わせ先<span class="require">※必須</span></label>
        <?php /*echo $this->Form->input('contact',
            array('class' => 'form-control',
                'label' => false,
                'value' => !empty($post['contact']) ? $post['contact'] : null,
                'id' => 'contact',
                'div' => false,
                'placeholder' => ''));*/
        ?>
    </div>-->

    <div class="form-group">
        <label class="control-label">その他備考</label>
        <?php echo $this->Form->textarea('other_remark',
            array('class' => 'form-control textarea',
                'label' => false,
                'id' => 'other_remark',
                'value' => !empty($post['other_remark']) ? $post['other_remark'] : null,
                'rows' => 5,
                'placeholder' => '')); ?>
    </div>

    <div class="form-group" id="image_thumb">
        <label class="control-label">サムネイル（最大サイズ：1メガバイト）</label>
        <?php echo $this->Form->input('thumb_1',
            array('type' => 'file',
                'class' => 'form-control',
                'label' => false,
                'id' => 'thumb_1',
                'data-id' => 1,)); ?>

        <?php echo $this->Form->input('thumb_2',
            array('type' => 'file',
                'class' => 'form-control',
                'label' => false,
                'id' => 'thumb_2',
                'data-id' => 2,)); ?>

        <?php echo $this->Form->input('thumb_3',
            array('type' => 'file',
                'class' => 'form-control',
                'label' => false,
                'id' => 'thumb_3',
                'data-id' => 3,)); ?>

        <?php echo $this->Form->input('delete_thumb_1',
            array('class' => 'form-control',
                'label' => false,
                'value' => '',
                'type'  => 'hidden',
                'id'    => 'delete_thumb_1' )); ?>

        <?php echo $this->Form->input('delete_thumb_2',
            array('class' => 'form-control',
                'label' => false,
                'value' => '',
                'type'  => 'hidden',
                'id'    => 'delete_thumb_2' )); ?>

        <?php echo $this->Form->input('delete_thumb_3',
            array('class' => 'form-control',
                'label' => false,
                'value' => '',
                'type'  => 'hidden',
                'id'    => 'delete_thumb_3' )); ?>
    </div>
    <div class="row">
    	<div class="col-md-4">
			<div class="form-group">
					<label class="control-label">作成日</label>
					<?php echo $this->Form->input('created',
						array('class' => '',
							'label' => false,
							'type' => 'date',
							'id' => 'created',
							'value' => !empty($post['created']) ? $post['created'] : null ,
							'dateFormat' => 'YMD',
							'required' => false,
							'monthNames' => array(1=>1,2=>2,3=>3,4=>4,5=>5,6=>6,7=>7,8=>8,9=>9,10=>10,11=>11,12=>12)
						)); ?>
			</div>
		</div>
		<div class="col-md-8">
			<div class="form-group">
				<?php if (!empty($post['id'])): ?>
				<label class="control-label">共有</label>
				<a href="<?php echo $this->app->getShareLink("/SecretShare/index/".$post['id'], $post['id']); ?>" target="_blank" style="font-size: 14px; margin-left: 20px;">1日</a>
				<a href="<?php echo $this->app->getShareLink("/SecretShare/index/".$post['id'], $post['id'], 3); ?>" target="_blank" style="font-size: 14px; margin-left: 20px;">3日</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
  
   
   <div class="row">
  		<div class="col-md-12">
  			<label class="control-label">履歴書-職務経歴書</label>
  		</div>
   		<div class="col-md-2">
			<div class="form-group">
				<label class="control-label" for="ManagerJob_is_upload_cv">ショー</label>
				<input type="checkbox" class="" <?php echo !empty($post['is_upload_cv']) ? "checked" : ""; ?>
					   name="data[ManagerJob][is_upload_cv]" value="1" id="ManagerJob_is_upload_cv">
			</div>
   		</div>
   		<div class="col-md-10">
   			<div class="form-group">
				<label class="control-label" for="ManagerJob_required_upload_cv">必須</label>
				<input type="checkbox" class="" <?php echo !empty($post['required_upload_cv']) ? "checked" : ""; ?>
					   name="data[ManagerJob][required_upload_cv]" value="1" id="ManagerJob_required_upload_cv">
			</div>
   		</div>
   </div>
    <div class="form-group">
        <label class="btn-publish">
            <input type="checkbox" class="" <?php echo !empty($post['status']) ? "checked" : ""; ?>
                   name="data[ManagerJob][status]" value="1" id="ManagerJobStatus">パブリッシュ
        </label>
        <input type="submit" class="btn btn-primary pull-right" value="<?php echo $btn; ?>"/>
        <input type="button" style="margin-right: 15px;" class="btn btn-primary pull-right btn-preview" value="SP-プレビュ" data-id="SP"/>
        <input type="button" style="margin-right: 15px;" class="btn btn-primary pull-right btn-preview" value="PC-プレビュ" data-id="PC"/>
    </div>

    <?php echo $this->Form->end(null); ?>

    <script>
        $(document).ready(function () {

//Check validate form
/*            var form = $("#<?php echo $frmId; ?>");
            form.validate({
                errorPlacement: function errorPlacement(error, element) {
                    element.prev().after(error);
                },
                onkeyup: false,
                rules: {
                    "data[ManagerJob][job_code]": {
                        required: true,
                    },
                    "data[ManagerJob][branch_name]": {
                        required: true,
                    },
                    "data[ManagerJob][company]": {
                        required: true,
                    },
                    "data[ManagerJob][job]": {
                        required: true,
                    },
                    "data[ManagerJob][employment][]": {
                        required: true,
                    },
                    "data[ManagerJob][work_features][]": {
                        required: true,
                    },
                    "data[ManagerJob][hourly_wage]": {
                        required: true,
                    },
                    "data[ManagerJob][business]": {
                        required: true,
                    },
                    "data[ManagerJob][application_email]": {
                        required: true,
                    },
                    "data[ManagerJob][application_email]": {
                        required: true,
                    },                    
                    "data[ManagerJob][branch_name]": {
                        required: true,
                    },

                },

            });
            jQuery.extend(jQuery.validator.messages, {
                required: "この項目は必須です。",
            });
*/

            $("#senior_avata").fileinput({
                initialPreview: [<?php if(!empty($post['senior_avata'])){ ?> '<img src="/upload/secret/jobs/<?php echo $this->base.$post['senior_avata'] ?>" class="" >' <?php } ?>],
                showUpload: false,
                showRemove: false,
                maxFileSize: 1024,
                msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
                allowedFileExtensions: ["jpg", "gif", "png"],
                overwriteInitial: true,
                allowedFileTypes: ["image"],
                previewFileType: ["image"]
            });
            
            $("#corporate_logo").fileinput({
                initialPreview: [ <?php if(!empty($post['corporate_logo'])){ ?> '<img src="/upload/secret/jobs/<?php echo $this->base.$post['corporate_logo'] ?>" class="" >' <?php } ?>],
                showUpload: false,
                showRemove: false,
                maxFileSize: 1024,
                msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
                allowedFileExtensions: ["jpg", "gif", "png"],
                overwriteInitial: true,
                allowedFileTypes: ["image"],
                previewFileType: ["image"]
            });

            
            $("#thumb_1").fileinput({
                initialPreview: [<?php if(!empty($post['thumb_1'])){ ?> '<img src="/upload/secret/jobs/<?php echo $this->base.$post['thumb_1'] ?>" class="" >' <?php } ?>],
                showUpload: false,
                showRemove: false,
                maxFileSize: 1024,
                msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
                allowedFileExtensions: ["jpg", "gif", "png"],
                overwriteInitial: true,
                allowedFileTypes: ["image"],
                previewFileType: ["image"]
            });

            $("#thumb_2").fileinput({
                initialPreview: [<?php if(!empty($post['thumb_2'])){ ?> '<img src="/upload/secret/jobs/<?php echo $this->base.$post['thumb_2'] ?>" class="" >' <?php } ?>],
                showUpload: false,
                showRemove: false,
                maxFileSize: 1024,
                msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
                allowedFileExtensions: ["jpg", "gif", "png"],
                overwriteInitial: true,
                allowedFileTypes: ["image"],
                previewFileType: ["image"]
            });

            $("#corporate_logo").fileinput({
                initialPreview: [ <?php if(!empty($post['corporate_logo'])){ ?> '<img src="/upload/secret/companies/<?php echo $this->base.$post['corporate_logo'] ?>" class="" >' <?php } ?>],
                showUpload: false,
                showRemove: false,
                maxFileSize: 1024,
                msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
                allowedFileExtensions: ["jpg", "gif", "png"],
                overwriteInitial: true,
                allowedFileTypes: ["image"],
                previewFileType: ["image"]
            });

            $("#thumb_3").fileinput({
                initialPreview: [<?php if(!empty($post['thumb_3'])){ ?> '<img src="/upload/secret/jobs/<?php echo $this->base.$post['thumb_3'] ?>" class="" >' <?php } ?>],
                showUpload: false,
                showRemove: false,
                maxFileSize: 1024,
                msgProgress: 'Loading file {index} of {files} - {name} - {percent}% completed.',
                allowedFileExtensions: ["jpg", "gif", "png"],
                overwriteInitial: true,
                allowedFileTypes: ["image"],
                previewFileType: ["image"]
            });

            $('.froalaEditor-short').froalaEditor({
                language: 'ja',
                height: 100,
            })
            $('.textarea').froalaEditor({
                language: 'ja',
                height: 200,
            })

            $('.btn-preview').click(function () {
                if ($('#<?php echo $frmId; ?>').valid()) {
                    var input = $("<input>").attr("name", "isPreview").hide().val("1");
                    //kind preview PC or SP
                    var kindPreview = $(this).attr('data-id');

                    $('#<?php echo $frmId; ?>').append(input);
                    $.ajax({
                        url: "<?php echo $ajaxUrl ;?>",
                        async: false,
                        type: "post",
                        data: $("#<?php echo $frmId; ?>").serialize(),
                        success: function () {
                            if(kindPreview == 'PC')
                            {
                                window.open(
                                    document.location.origin + "/secret/preview/<?php echo !empty($item['CareerOpportunities']['id']) ?
                                $item['CareerOpportunities']['id'] : $lastId ; ?>/"+ kindPreview, '_blank'
                                );
                            }
                            else
                            {
                                window.open(
                                    document.location.origin + "/secret/preview/<?php echo !empty($item['CareerOpportunities']['id']) ?
                                $item['CareerOpportunities']['id'] : $lastId ; ?>/"+ kindPreview, '_blank', "width=414,height=650,resizeable,scrollbars"
                                );
                            }
                            <?php if(empty($item['CareerOpportunities'])){ ?>

                            window.location.href = document.location.origin + "/admin/ManageJob/edit/<?php echo $lastId;?>";

                            <?php } ?>

                        }
                    });
                }
            });

            // check validate in froalaEditor

            $("#<?php echo $frmId; ?>").submit(function (event) {
                var validateTextarea = true
                $(".froalaEditor-require").each(function (index) {
                    var content = $(this).froalaEditor('html.get', true);
                    if (content == '') {
                        $(this).parent().find(".control-label").after("<label class='error'>この項目は必須です。</label>");
                        $(this).parent().find(".fr-element").focus();
                        validateTextarea = false;
                    }
                });

                return validateTextarea;
            });

            $('.froalaEditor-require').on('froalaEditor.keyup', function (e, editor, keyupEvent) {
                var content = $(this).froalaEditor('html.get', true);
                if (content == '')
                    $(this).parent().find(".control-label").after("<label class='error'>この項目は必須です。</label>");
                else
                    $(this).parent().find(".error").remove();
            });


            //delete thumb image
            $('#image_thumb .fileinput-remove').click(function() {
                var parentElement = this.parentElement.parentElement;
                fileInput = parentElement.querySelector('input[type="file"]');
                dataId = $(fileInput).attr("data-id");
                
                $('#delete_thumb_' + dataId).val("delete_image");
            });

            

        });

        // set google map
        // input lat, lng , inforwindow, zoom
        <?php $latLng = !empty($post['lat']) && !empty($post['lng']) ? $post['lat'].' , '.$post['lng'] : '0,0'; ?>
        var contentString = "<h4 style='font-weight: bold;'><?php echo $company_name; ?></h4><a href='<?php echo $website; ?>' ><p><?php echo  $website; ?></p></a><p><?php echo $office_location; ?> </p> ";
        initMap( <?php echo $latLng;?>,contentString, 18   )
        $( "#find_location" ).click(function() {
            var address =$("#map_location").val();
            var p = null;
            $.ajaxSetup({
                async: false
            });
            $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&sensor=false', null, function (data) {
                var p = data.results[0].geometry.location;
                $('#lat').attr('value', p.lat);
                $('#lng').attr('value', p.lng);
                initMap(p.lat, p.lng, contentString, 18 );
            });
        });
    </script>

</div>