<?php
$this->set('title', 'コールナビ｜仕事管理');
$this->start('css'); ?>
	<link rel="stylesheet" href="<?php echo $this->base;?>/admin/css/common.css" type="text/css" media="all">
	<link rel="stylesheet" href="<?php echo $this->base;?>/admin/css/manage-job.css" type="text/css" media="all">
<?php $this->end('css'); ?>

<div id="manage-job" class="container-fluid">
	<div class="row">
		<div class="col-md-2 padding-right-10">
			<?php echo $this->element('../Elements/left_menu'); ?> 
		</div>

		<div class="col-md-10 padding-left-10">
			<div class="col-md-12 no-padding">

				<a href="<?php echo $this->Html->url(array('controller' => 'ManageJob', 'action' => 'add',  ), true);?>" class="btn btn-primary pull-right btn-top" >新規登録</a>
				<a href="<?php echo $this->Html->url(array('controller' => 'UploadCsv', 'action' => 'UploadJob',  ), true);?>" class="btn btn-primary pull-right" >CSVアップロード</a>
			</div>

			<div class="search-form">
				<?php echo $this->Form->create('Searchjob', array('url' => 'index','class' => 'form-horizontal form-row-5', 'type' => 'get')); ?>
				<div class="col-md-7 padding-5">
					<?php echo $this->Form->input('keywords',
						array('class' => 'form-control',
							'label' => false,
							'div' => false,
							'value' =>!empty($keywords) ? $keywords : '',
							'id'    => 'keywords',
							'placeholder' =>'検索キーワードを入力してください'));
					?>
				</div>

				<div class="col-md-3 padding-5">
					<select name="company" id="company_id" class="form-control">
						<option  value="" >すべての企業メッセージ</option>
						<?php foreach ($getListCompany as $record):
							$post=$record['CorporatePr'];
							$selected = '';
							if($getSearchCompany == $post['company_name'])
								$selected = 'selected';
							?>
							<option <?php echo $selected; ?>  value="<?php echo $post['id'];?>" ><?php echo $post['company_name'] ;?></option>
						<?php  endforeach ?>
					</select>
				</div>

				<div class="col-md-2 padding-5">
					<input type="submit" class="btn btn-primary pull-right" value="検索"/>
				</div>
				<?php echo $this->Form->end(null); ?>

			</div>
			<div class="col-md-12 no-padding">
				<p><?php echo $total ;?>件中 <?php echo $numberStartEnd['start'] ;?> - <?php echo $numberStartEnd['end'] ;?>件を表示</p>
			</div>
			<table class="table table-bordered">
				<thead>
					<tr>
                        <th width="40px">NO.</th>
                        <th width="100px">求人ID</th>
						<th>募集コールセンター名</th>
						<th>求人タイトル</th>
						<th class="box-date">作成日</th>
						<th width="90px">パブリッシュ</th>
						<th class="box-last">....</th>
					</tr>
				</thead>
				<tbody>
					<?php
                    $num = $numberStartEnd['start'] - 1	;
					if(!empty($list)) {
						foreach ($list as $record):
							$num += 1;
                            $post = $record['CareerOpportunities'];
							$company_name = $record['CorporatePr']['company_name'];
                            $company_code = $record['CorporatePr']['corporate_code'];
                            $job_id = $post['id'];
                            $job_code = $post['job_code'];
                            $href = "/secret/".$job_id;
                            
							$created = strtotime($post['created']) ?>
							<tr>
                                <td><?php echo ("<a href='".$href."' target='_blank'>".$job_id."</a>") ?></td>
                                <td><?php echo $company_code .  "-". $job_code; ?></td>
								<td><?php echo  $company_name; ?></td>
								<td><?php echo $post['job']; ?></td>
								<td class="box-date"><?php echo $post['created']; ?></td>
								<td><?php echo ($post['status']?'はい':'いいえ'); ?></td>
								<td>
									<a type="button" class="btn btn-primary btn-xs" href="<?php echo $this->Html->url(array('controller' => 'ManageJob', 'action' => 'edit', $post['id']), true); ?>">修正</a>
									<?php
									echo $this->Html->link('削除',
                                                           array(
                                                               'controller' =>'ManageJob',
                                                               'action'     =>'delete',
                                                               
                                                               $post['id']
                                                           ),
                                                           array(
                                                               'confirm'=>'本当に削除しますか？',
                                                                'class'      =>'btn btn-default btn-xs'
                                                                ));
								?>
								</td>

							</tr>
						<?php endforeach;
					}else{ ?>
						<tr>
							<td colspan="4">
								<p>データなし</p>
							</td>
						</tr>
					<?php }?>
				</tbody>
			 </table>
            <?php echo $this->element('../Elements/pagination'); ?>
      </div>
   </div>
</div>