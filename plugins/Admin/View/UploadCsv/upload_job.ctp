<?php $this->start('css'); ?>
	<link rel="stylesheet" href="<?php echo $this->base;?>/admin/css/common.css" type="text/css" media="all">
    <link href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/css/select2.min.css" rel="stylesheet" />
<?php $this->end('css'); ?>

<?php $this->start('script'); ?>
    <script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<?php $this->end('script'); ?>

<div id="manage-news" class="container">
	<div class="row">
		<div class="col-md-3 padding-right-10">
			<?php echo $this->element('../Elements/left_menu'); ?> 
		</div>
		<div class="col-md-9 padding-left-10" style="margin-top: 40px;">
            <form method="POST" action="" accept-charset="UTF-8" enctype="multipart/form-data">
                <div class="title"><strong>■分類します</strong></div>
                <div class="form-group">
                    <select name="company" id="company" class="form-control">
                       	<option value="0">[ Use 募集コールセンター名 in CSV ]</option>
                        <?php foreach($companyList as $company):?>
                            <option value="<?php echo $company['CorporatePr']['id'];?>"><?php echo $company['CorporatePr']['company_name'];?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                <div class="title"><strong>■CSVを選択してアップロードしてください</strong></div>
                <div class="form-group">
                    <input name="csv" required type="file" class="form-control">
                </div>
                <?php if(!empty($status)){?>
                    <div class="form-group">
                        <?php echo $status; ?>
                    </div>
                <?php } ?>
                <div class="div-submit">
                    <button type="submit" class="btn btn-primary btn-sm">アップロード</button>
                    <a href="/demo-csv/career_opportunities.csv" class="btn btn-primary btn-sm">CSVフォーマット</a>
                </div>
            </form>
            
      </div>
   </div>
   <div class="row">
   		<div class="col-md-12">
   			<?php echo $this->element('../UploadCsv/_result'); ?>
	   	</div>
   </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('select').select2();
    });
</script>