<?php $this->start('css'); ?>
	<link rel="stylesheet" href="<?php echo $this->base;?>/admin/css/common.css" type="text/css" media="all">
<?php $this->end('css'); ?>

<div id="manage-news" class="container">
	<div class="row">
		<div class="col-md-3 padding-right-10">
			<?php echo $this->element('../Elements/left_menu'); ?> 
		</div>
		<div class="col-md-9 padding-left-10" style="margin-top: 40px;">
            <form method="POST" action="" accept-charset="UTF-8" enctype="multipart/form-data">
                <div class="title"><strong>■CSVを選択してアップロードしてください</strong></div>
                <div class="form-group">
                    <input name="csv" required type="file" class="form-control">
                </div>
                <?php if(!empty($status)){?>
                    <div class="form-group">
                        <?php echo $status; ?>
                    </div>
                <?php } ?>
                <div class="div-submit">
                    <button type="submit" class="btn btn-primary btn-sm">アップロード</button>
                    <a href="/demo-csv/corporate_prs.csv" class="btn btn-primary btn-sm">CSVフォーマット</a>
                </div>
            </form>
      </div>
   </div>
   <div class="row">
   		<div class="col-md-12">
   			<?php echo $this->element('../UploadCsv/_result'); ?>
	   	</div>
   </div>
</div>