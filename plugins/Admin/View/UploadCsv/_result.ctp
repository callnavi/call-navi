<?php if (isset($result)): ?>
<style>
	.wrapper-tb{
		width: 100%;
		overflow-x: auto;
	}
	.wrapper-tb .table{
		width: auto;
		table-layout: fixed;
	}
	.wrapper-tb .table th{
		white-space: nowrap;
	}
	.wrapper-tb .table td{
    	max-width: 500px;
		max-height: 150px;
		overflow: auto;
		white-space: nowrap;
	}
	.error{
		color: red;
	}
</style>
<h3>CSV情報: <?php echo count($result)-1; ?>件

<?php if (isset($log_name)): ?>
<small>
	<a target="_blank" href="/admin/UploadCsv/ReadLog?log=<?php echo $log_name; ?>">link of log</a>
</small>
<?php endif; ?>
</h3>

<div class="wrapper-tb">
	<table class="table table-bordered">
		<thead>
			<tr>
			<?php
				if(!empty($result[0]))
				{
					foreach( $result[0] as $index=>$header)
					{
						echo "<th data-key='$index'>$header</th>";
					}
					unset($result[0]);
				}
			?>
			</tr>
		</thead>
		<tbody>
			<?php 
				if(!empty($result))
				{
					foreach( $result as $record)
					{
						echo "<tr>";
						foreach( $record as $key=>$item)
						{
							echo "<td data-key='$key'><div class='inner-data'>$item</div></td>";
						}
						echo "</tr>";
					}
				}
			?>
		</tbody>
	</table>
</div>
<div style="margin-top: 40px"></div>
<?php endif; ?>

<div class="test_mode_bottom">test mode CSV <a href="/admin/UploadCsv/TestMode">Turn on</a>/<a href="/admin/UploadCsv/TestMode?testmode=false">Turn off</a></div>