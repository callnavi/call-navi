<!DOCTYPE html>
<html lang="ja">
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $this->fetch('title'); ?>
	</title>
	<meta name="robots" content="noindex, follow">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel='stylesheet' id='contact-form-7-css'  href='/wp-content/themes/twentysixteen/style.css' type='text/css' media='all' />
	<?php
		echo $this->Html->meta('icon');
		echo $this->fetch('_meta');
		echo $this->fetch('_css');
		echo $this->fetch('_head');
	?>
	
</head>
<body>
	<div id="container">
		<div id="header">
		    <?php echo $this->element('Header/header_wp'); ?>
		</div>
		<div id="content">
			<?php echo $this->fetch('content'); ?>
		</div>
		<div id="footer">
			<?php echo $this->element('Footer/footer_wp'); ?>
		</div>
	</div>
	<?php echo $this->fetch('_js'); ?>
	<?php echo $this->fetch('_footer'); ?>
	<?php echo $this->element('sql_dump'); ?>
</body>
</html>
