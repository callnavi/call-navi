jQuery(document).ready(function () {
    var pathname = window.location.pathname;
    pathname = pathname.split("/");
    switch(pathname[2]) {
        case "ManageJob":
        case "manage_job":
            activeManageJob();
            break;
        case "ManageContents":
        case "manage_contents":
            activeManageContents();
            break;
        case "BlogsCategory":
        case "blogs_category":
            activeManageBlogsCategory();
            break;
        case "Blogs":
        case "blogs":
            activeManageBlogs();
            break;
        case "ManageNews":
        case "manage_news":
            activeManageNews();
            break;
        case "ManageCallCenter":
        case "manage_call_center":
            activeManageCallCenter();
            break;
        case "ManageMeta":
            activeManageMeta();
            break;
        default:
            activeManageCompany();
            break;
    }

    switch(pathname[3]) {
        case "UploadJob":
            activeManageJob();
            break;
    }


    function activeManageCompany()
    {
        jQuery("ul.left-menu-admin li.list-group-item").removeClass("active-menu");
        jQuery("#item-company").addClass("active-menu");
    }

    function activeManageJob()
    {
        jQuery("ul.left-menu-admin li.list-group-item").removeClass("active-menu");
        jQuery("#item-job").addClass("active-menu");
    }

    function activeManageContents()
    {
        jQuery("ul.left-menu-admin li.list-group-item").removeClass("active-menu");
        jQuery("#item-content").addClass("active-menu");
    }

    function activeManageBlogsCategory()
    {
        jQuery("ul.left-menu-admin li.list-group-item").removeClass("active-menu");
        jQuery("#item-blog-category").addClass("active-menu");
    }

    function activeManageBlogs()
    {
        jQuery("ul.left-menu-admin li.list-group-item").removeClass("active-menu");
        jQuery("#item-blog").addClass("active-menu");
    }

    function activeManageNews()
    {
        jQuery("ul.left-menu-admin li.list-group-item").removeClass("active-menu");
        jQuery("#item-news").addClass("active-menu");
    }

    function activeManageCallCenter()
    {
        jQuery("ul.left-menu-admin li.list-group-item").removeClass("active-menu");
        jQuery("#item-callcenter").addClass("active-menu");
    }

    function activeManageMeta()
    {
        jQuery("ul.left-menu-admin li.list-group-item").removeClass("active-menu");
        jQuery("#item-meta").addClass("active-menu");
    }
});
