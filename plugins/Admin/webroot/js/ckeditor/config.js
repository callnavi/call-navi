﻿/*
Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/
var source = "na/"; 
CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
    config.language = 'en';
	// config.uiColor = '#AADC6E';
    config.filebrowserBrowseUrl = '/'+source+'js/kcfinder/browse.php?type=files';
    config.filebrowserImageBrowseUrl = '/'+source+'js/kcfinder/browse.php?type=images';
    config.filebrowserFlashBrowseUrl = '/'+source+'js/kcfinder/browse.php?type=flash';
    config.filebrowserUploadUrl = '/'+source+'js/kcfinder/upload.php?type=files';
    config.filebrowserImageUploadUrl = '/'+source+'js/kcfinder/upload.php?type=images';
    config.filebrowserFlashUploadUrl = '/'+source+'js/kcfinder/upload.php?type=flash';
};
