(function () {
	var class_list = {
		default:{
			h1:'',
			h2:'column-box_contents__ttl u-fs--xxxlh--interview u-fwb u-mt60',
			h2u40:'column-box_contents__ttl u-fs--xxxlh--interview u-fwb u-mt40',
			h2border:'column-box_contents__ttl column-box_contents__ttl--border u-fs--xxxlh--interview u-fwb u-mt60',
			h3:'column-box_contents__subTtl--interview u-fs--xxlh--contents u-fwb',
			h360:'column-box_contents__subTtl--interview u-fs--xxlh--contents u-fwb u-mt60',
			p:'column-box_contents__text u-fs--lh--contents u-mt15',
		}
	};
	var get_class = function(obj)
	{
		var rs = class_list['default'][obj] || '';
		return rs;
	}
	
	var getAndReplace = function(obj, text)
	{
		if(obj.selection.element())
		{
			text = obj.selection.element().innerText || text;
			obj.selection.element().outerHTML = '';
		}
		return text;
	}
	//Big title
	$.FroalaEditor.DefineIcon('runda_title_wrapper', {NAME: 'Big title', template: 'text'});
	$.FroalaEditor.RegisterCommand('runda_title_wrapper', {
      title: '',
      focus: true,
      undo: true,
      refreshAfterCallback: true,
      callback: function () {
		 var html = '<div class="content--big_title">'+
						'<hr class="hr--v3">'+
						'<h3 class="_h2 mg-0">コンテンツ</h3>'+
						'<hr class="hr--v3">'+
					'</div>'+
			 		'<p></p>'
		 ;
         this.html.insert(html);
      }
    });
	
	
	//Topic
	$.FroalaEditor.DefineIcon('runda_topic_wrapper', {NAME: 'Topic', template: 'text'});
	$.FroalaEditor.RegisterCommand('runda_topic_wrapper', {
      title: '',
      focus: true,
      undo: true,
      refreshAfterCallback: true,
      callback: function () {
		 var html = '<div class="content--small_title">タイトル</div>'+
			 		'<p><span class="p--green-span">熊沢：</span> <span>コンテンツ</span></p>';
         this.html.insert(html);
      }
    });
	
	//CEO image
	$.FroalaEditor.DefineIcon('runda_image_wrapper', {NAME: 'Image', template: 'text'});
	$.FroalaEditor.RegisterCommand('runda_image_wrapper', {
      title: '',
      focus: true,
      undo: true,
      refreshAfterCallback: true,
      callback: function () {
		 var html = '<div class="display-table p--imggrp">'+
						'<div class="p--imggrp__img"><img src="/img/noimage/no_image.jpg" alt=""></div>'+
						'<div class="p--imggrp__txt">コンテンツ</div>'+
					'</div>'+
			 		'<p></p>';
         this.html.insert(html);
      }
    });
})()