<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ManageJobController extends AdminAppController {

	public $uses = array('CareerOpportunities','CorporatePr');
	public $helpers = array('Paginator','Html');
    //28 july 2017 add new code component
    public $components = array('Session','Code');
    public $paginate = array();
	
/**
 * This controller does not use a model
 *
 * @var array
 */
	public function beforeFilter() {
		parent::beforeFilter();
		$this->isPublic();
	}
    
    public function index()
    {
		$conditions ='';
		if($this->request->is('get')){
			// search parameter
			$data = $this->request->query;
			$getKeywords = isset($data['keywords']) ? $data['keywords'] :'' ;
			$getSearchCompany = isset($data['company']) ? $data['company'] :'' ;

			$keywords = !empty($getKeywords) ? "CareerOpportunities.job like '%".$getKeywords."%'" :'' ;
			$SearchCompany = !empty($getSearchCompany) ? "CorporatePr.id = '".$getSearchCompany."'" :'' ;
			$conditions = array('AND'=>array($keywords,$SearchCompany));

			$this->set("keywords",$getKeywords );
			$this->set("getSearchCompany",$getSearchCompany );
		}
         //28 july 2017 add get corporate code
		$this->paginate = array(
			'fields' => array('CareerOpportunities.id',
						'CareerOpportunities.job',
                        'CareerOpportunities.job_code',
						'CareerOpportunities.employment',
						'CareerOpportunities.work_features',
						'CareerOpportunities.what_job',
						'CareerOpportunities.work_location',
						'CareerOpportunities.access',
						'CareerOpportunities.what_kind_person',
						'CareerOpportunities.senior_name_of_staff',
						'CareerOpportunities.senior_joining_history',
						'CareerOpportunities.senior_employee_interview',
						'CareerOpportunities.specific_job',
						'CareerOpportunities.person_of_interest',
						'CareerOpportunities.working_hours',
						'CareerOpportunities.salary',
						'CareerOpportunities.skill_experience',
						'CareerOpportunities.holiday_vacation',
						'CareerOpportunities.health_welfare',
						'CareerOpportunities.application_method',
						'CareerOpportunities.benefit',
						'CareerOpportunities.application_email',
						'CareerOpportunities.contact',
						'CareerOpportunities.other_remark',
						'CareerOpportunities.created',
						'CareerOpportunities.status',
						'CorporatePr.company_name',
                        'CorporatePr.corporate_code'),
			'limit' => 20,
			'conditions'=>$conditions,
			'joins' => array(
				array(
					'alias' => 'CorporatePr',
					'table' => 'corporate_prs',
					'type' => 'LEFT',
					'conditions' => array(
						'CareerOpportunities.company_id = CorporatePr.id',
					),
				),
			),
			'order' => array(
				'CareerOpportunities.id' => 'DESC'
			)
		);
		
		$val= $this->paginate('CareerOpportunities');
		$this->set("list",$val);

		//get list company
		$getListCompany = $this->CorporatePr->find("all",array(
			'fields' => array('company_name', 'id','website' ,'office_location'),
			'order' => array(
				'CorporatePr.created' => 'DESC'
			),
		));

		$this->set("getListCompany",$getListCompany);

		//get total article
		$total = $this->CareerOpportunities->find("count",array(
			'conditions'=>$conditions,
			'joins' => array(
				array(
					'alias' => 'CorporatePr',
					'table' => 'corporate_prs',
					'type' => 'LEFT',
					'conditions' => array(
						'CareerOpportunities.company_id = CorporatePr.id',
					),
				),
			),
		));
		$this->set("total",$total);

		// get index in this page
		$AppController = new AdminAppController;
		$numberStartEnd = $AppController->getNumberStartEndinPage($this->params['paging']['CareerOpportunities']);
		$this->set("numberStartEnd",$numberStartEnd);
    }
	// function get list company
	public function get_company(){
		$arr = $this->CorporatePr->find('all', array(
			'fields' => array('CorporatePr.id','CorporatePr.company_name','CorporatePr.website','CorporatePr.office_location'),
		));
		return $arr;
	}
	
	public function add(){
		// get list company
		$companyList= $this->get_company();
		$this->set("companyList",$companyList);

		$lastId = $this->CareerOpportunities->query("SHOW TABLE STATUS WHERE name = 'career_opportunities'");
		$this->set('lastId', $lastId[0]['TABLES']['Auto_increment']);

		if($this->request->is('post')){
                $isPreview = !empty($this->request->data['isPreview'])? $this->request->data['isPreview']  : null;

                $param['CareerOpportunities'] = $this->request->data['ManagerJob'];
                $param = $this->setParamsPhotos($param);
                $param['CareerOpportunities']['status'] = !empty($isPreview) ? 0 : isset($param['CareerOpportunities']['status'])?$param['CareerOpportunities']['status']:0;

                $param['CareerOpportunities']['status'] = !empty($param['CareerOpportunities']['status']) ? $param['CareerOpportunities']['status'] :0;
                if(!empty($param['CareerOpportunities']['work_features']))
                    $param['CareerOpportunities']['work_features'] = implode(',',$param['CareerOpportunities']['work_features']);
                if(!empty($param['CareerOpportunities']['employment']))
                    $param['CareerOpportunities']['employment'] = implode(',',$param['CareerOpportunities']['employment']);
                
                //28 july 2017 generate code new function combine with company id
                //$param['CareerOpportunities']['job_code'] = $this->generateNewJobCode($this->request->data['ManagerJob']['company_id']);
                //pr($param['CareerOpportunities']);die;
                $flag = $this->checkCodeJob($this->request->data['ManagerJob']['job_code'] , $this->request->data['ManagerJob']['job_code_hidden'], $this->request->data['ManagerJob']['company_id'], '0');

                if($flag == true){
                    $this->CareerOpportunities->create();

	                if ($this->CareerOpportunities->save($param)) {
	                    $this->Session->setFlash(__('The article has been saved.'));
	                    return $this->redirect('/admin/ManageJob/edit/'.$this->CareerOpportunities->getInsertID());
	                } else {
	                    $this->Session->setFlash(__('The article could not be saved. Please, try again.'));
	                }
                }else {
                	$this->set('job_code_1',$this->request->data['ManagerJob']['job_code'] );
                	$this->set('error_message_code','Input another id!.');  
	            }
	   
		}
	}
	
	public function edit($id= null){
		$item = $this->CareerOpportunities->find('first', array(
			'fields' => array('CareerOpportunities.*'),
			'joins' => array(
				array(
					'alias' => 'CorporatePr',
					'table' => 'corporate_prs',
					'type' => 'LEFT',
					'conditions' => array(
						'CareerOpportunities.company_id = CorporatePr.id',
					),
				),
			),
			'conditions' => array('CareerOpportunities.id' => $id),
		));
		$this->set("item",$item);

		// get list company
		$companyList= $this->get_company();
		$this->set("companyList",$companyList);

		if($this->request->is('post')){
             //28 july 2017 add sending company data to check if the company or job code change
            $item = $this->CareerOpportunities->find('first', array(
                'fields' => array('CareerOpportunities.*'),
                'joins' => array(
                    array(
                        'alias' => 'CorporatePr',
                        'table' => 'corporate_prs',
                        'type' => 'LEFT',
                        'conditions' => array(
                            'CareerOpportunities.company_id = CorporatePr.id',
                        ),
                    ),
                ),
                'conditions' => array('CareerOpportunities.id' => $id),
            ));
            $flag = $this->checkCodeJob($this->request->data['ManagerJob']['job_code'] , $this->request->data['ManagerJob']['job_code_hidden'], $this->request->data['ManagerJob']['company_id'], $item['CareerOpportunities']['company_id']);
            if($flag == true){
                if (!$this->CareerOpportunities->exists($id)) {
                    throw new NotFoundException(__('Invalid article'));
                }

                $param['CareerOpportunities'] = $this->request->data['ManagerJob'];
                if(empty($param['CareerOpportunities']['is_search']))
                {	
                    $param['CareerOpportunities']['is_search'] = 0;
                }

                if(empty($param['CareerOpportunities']['is_upload_cv']))
                {	
                    $param['CareerOpportunities']['is_upload_cv'] = 0;
                }

                if(empty($param['CareerOpportunities']['required_upload_cv']))
                {	
                    $param['CareerOpportunities']['required_upload_cv'] = 0;
                }
				
				if(empty($param['CareerOpportunities']['map_show']))
                {	
                    $param['CareerOpportunities']['map_show'] = 0;
                }

                $param = $this->setParamsPhotos($param);
                $param['CareerOpportunities']['status'] = !empty($param['CareerOpportunities']['status']) ? $param['CareerOpportunities']['status'] :0;
                if(!empty($param['CareerOpportunities']['work_features']))
                    $param['CareerOpportunities']['work_features'] = implode(',', $param['CareerOpportunities']['work_features']);

                if(!empty($param['CareerOpportunities']['employment']))
                    $param['CareerOpportunities']['employment'] = implode(',',$param['CareerOpportunities']['employment']);


                //delete image thumb
                if($param['CareerOpportunities']['delete_thumb_1'] && empty($param['CareerOpportunities']['thumb_1']))
                {
                    $param['CareerOpportunities']['thumb_1'] = null;
                }
                if($param['CareerOpportunities']['delete_thumb_2'] && empty($param['CareerOpportunities']['thumb_2']))
                {
                    $param['CareerOpportunities']['thumb_2'] = null;
                }
                if($param['CareerOpportunities']['delete_thumb_3'] && empty($param['CareerOpportunities']['thumb_3']))
                {
                    $param['CareerOpportunities']['thumb_3'] = null;
                }


                $this->CareerOpportunities->id=$id;
                $this->CareerOpportunities->set($param);
                if ($this->CareerOpportunities->save()) {
                    $this->Session->setFlash(__('The article has been saved.'));
                    return $this->redirect('/admin/ManageJob/edit/'.$id);
                } else {
                    $this->Session->setFlash(__('The article could not be saved. Please, try again.'));
                }
            }else{
                $this->set('job_code_1',$this->request->data['ManagerJob']['job_code'] );
                $this->set('error_message_code','Input another id!.');  
            }
		}
	}
	
	public function delete($id= null){
		$this->CareerOpportunities->id = $id;
		
		if ($this->CareerOpportunities->delete()) {
			$this->Session->setFlash(__('article deleted'));
		} 
		else{
			$this->Session->setFlash(__('The article could not be saved. Please, try again.'));
		}
		
		$this->redirect('/admin/ManageJob');
	}

	private function setParamsPhotos($params = null){

		if(empty($params['CareerOpportunities']['corporate_logo']['name'])) {
			unset($params['CareerOpportunities']['corporate_logo']);
		}else{
			$params['CareerOpportunities']['corporate_logo'] = $this->upload_img_jobs($params['CareerOpportunities']['corporate_logo']);
		}
        
		if(empty($params['CareerOpportunities']['senior_avata']['name'])) {
			unset($params['CareerOpportunities']['senior_avata']);
		}else{
			$params['CareerOpportunities']['senior_avata'] = $this->upload_img_jobs($params['CareerOpportunities']['senior_avata']);
		}

		if(empty($params['CareerOpportunities']['thumb_1']['name'])) {
			unset($params['CareerOpportunities']['thumb_1']);
		}else{
			$params['CareerOpportunities']['thumb_1'] = $this->upload_img_jobs($params['CareerOpportunities']['thumb_1']);
		}

		if(empty($params['CareerOpportunities']['thumb_2']['name'])) {
			unset($params['CareerOpportunities']['thumb_2']);
		}else{
			$params['CareerOpportunities']['thumb_2'] = $this->upload_img_jobs($params['CareerOpportunities']['thumb_2']);
		}

		if(empty($params['CareerOpportunities']['thumb_3']['name'])) {
			unset($params['CareerOpportunities']['thumb_3']);
		}else{
			$params['CareerOpportunities']['thumb_3'] = $this->upload_img_jobs($params['CareerOpportunities']['thumb_3']);
		}

		return $params;
	}

	// function upload img into /upload/secret/jobs forder
    private function upload_img_jobs( $img=null){

        if(!empty($img)){
            $path= WWW_ROOT . '/upload/secret/jobs/';
            $ext = explode(".",$img['name']);
            $img['name'] = $ext[0].'-'.rand(0,99999).'.'.$ext[sizeof($ext)-1];
            if(!file_exists($path)){
                mkdir($path, 0755, true);
            }
            move_uploaded_file($img['tmp_name'],$path .$img['name']);
            return $img['name'];
        }

    }
    // validation for checking funtion
     //28 july 2017 new validation for check code
    public function checkCodeJob($Job_id,$hidden_id,$company_id, $company_id_old){
        $count = ""; 
        $flag = false;

        if($Job_id == $hidden_id && str_replace(' ','',$company_id) ==  str_replace(' ','',$company_id_old)  ){
            $flag = true;
        }else{
            $count = $this->CareerOpportunities->find('count', array(
                'conditions' => array('job_code' => $Job_id,
                                     'company_id' => $company_id)
            ));            
            if($count == 0 ){
                $flag=true;
            }
            
        }
        return $flag;
    }    
     //28 july 2017 add new fucntion
    // Function For generate New Company ID
    public function generateNewJobCode($company_code){
		$results = $this->CareerOpportunities->query("SELECT max(job_code) as max_job_code FROM career_opportunities where company_id = '".  $company_code ."';");
        $result_value = "";
        $return_result = "";
        
            
        if(strlen($results[0][0]['max_job_code']) > 4){
            $result_value = (substr($results[0][0]['max_job_code'],1,4));
            do {            
                //make new result from max id
                $result_value = $result_value + 1;
                 
                // Make the format of the code string
                $return_result = $this->Code->codeGenerated("J","5",$result_value);  
                
             

            } while ($this->Code->CheckIdAvailable("career_opportunities","job_code",$return_result) == false );    
        }
        
        return $return_result;
    }  

         //11 august 2018 add new fucntion
    // Function For generate New Company ID
    public function generateNewJobCodeAdd(){
		$results = $this->CareerOpportunities->query("SELECT max(job_code) as max_job_code FROM career_opportunities;");
        $result_value = "";
        $return_result = "";
        
            
        if(strlen($results[0][0]['max_job_code']) > 4){
            $result_value = (substr($results[0][0]['max_job_code'],1,4));
            do {            
                //make new result from max id
                $result_value = $result_value + 1;
                 
                // Make the format of the code string
                $return_result = $this->Code->codeGenerated("J","5",$result_value);  
                
             

            } while ($this->Code->CheckIdAvailable("career_opportunities","job_code",$return_result) == false );    
        }
        
        return $return_result;
    }       
	
}
