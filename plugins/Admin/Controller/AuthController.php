<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class AuthController extends AdminAppController {

    public $uses = array('Users');
    public $helpers = array('Html');
    public $components = array('Session',);
    public $paginate = array();
    
    public function index(){

    }
	
	 public function login(){
        $errorMess =null;
        if ($this->request->is('post')) {
            //check exist user
            $data = $this->Users->find('first', array('conditions'=>array('user_mail'=>$this->request->data['Login']['email'], 'passwords'=>md5('WZ$$---[*(&^%$#%$^%%$^%$]*'.$this->request->data['Login']['password'].'*[&*#@%#$^@#$%^&%$#]----@@WZ'))));
            if(sizeof($data)){
                $this->Session->write('User',$data);
                $this->redirect(array("controller" => "ManageCompany", "action" => "index"));
            }else{
                $errorMess = '会社のメールアドレスまたはパスワードが正しくありません';
            }
        }

         $this->set("errorMess", $errorMess);
    }

    public function logout(){
        $this->Session->destroy();
        $this->redirect('/login');
    }

}
