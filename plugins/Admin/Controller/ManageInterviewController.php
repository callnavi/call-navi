<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ManageInterviewController extends AdminAppController {

	public $uses = array('Categories', 'Contents', 'CorporatePr', 'CareerOpportunity');
	public $helpers = array('Paginator','Html');
    public $components = array('Session');
    public $paginate = array();
	

/**
 * This controller does not use a model
 *
 * @var array
 */
	public function beforeFilter() {
		parent::beforeFilter();
		$this->isPublic();
	}
    
	
	public function add(){
		//get list category
		$this->getCategoryContents();
		// get list company
		$this->getCompanies();
		
		$lastId = $this->Contents->query("SHOW TABLE STATUS WHERE name = 'contents'");
		$this->set('lastId', $lastId[0]['TABLES']['Auto_increment']);

		if($this->request->is('post')){
			$isPreview = !empty($this->request->data['isPreview'])? $this->request->data['isPreview']  : null;
			$param['Contents'] = $this->request->data['ManagerContents'];
			$param['Contents']['status'] = !empty($isPreview) ? 0 : 1;
			
			//set image not null
			$param = $this->setParamsPhotos($param);
			$param = $this->setParamsPhotosByName($param, 'image_profile');
			$param = $this->setParamsPhotosByName($param, 'image_profile_sp');
			
			//save
			$this->Contents->create();
			
			if ($this->Contents->save($param)) {
				$this->Session->setFlash(__('The article has been saved.'));
				return $this->redirect('/admin/ManageContents');
			} else {
				$this->Session->setFlash(__('The article could not be saved. Please, try again.'));
			}
		}
	}
	
	public function edit($id= null){

		$item = $this->Contents->find('first', array(
			'conditions' => array('id' => $id),
		));
		
		//get list companies
		$this->getCompanies();
		
		//get list category
		$this->getCategoryContents();
		//get contents data
		$this->set("item",$item);
			
		if($this->request->is('post')){
			
			if (!$this->Contents->exists($id)) {
				throw new NotFoundException(__('Invalid article'));
			}
			$param['Contents'] = $this->request->data['ManagerContents'];
			//set image not null
			$param = $this->setParamsPhotos($param);
			$param = $this->setParamsPhotosByName($param, 'image_profile');
			$param = $this->setParamsPhotosByName($param, 'image_profile_sp');
			
			//pr($param);
			//die;
            // upload img into database
			$this->Contents->id=$id;
			$this->Contents->set($param);
			if ($this->Contents->save()) {
				$this->Session->setFlash(__('The article has been saved.'));
				return $this->redirect('/admin/ManageContents');
			} else {
				$this->Session->setFlash(__('The article could not be saved. Please, try again.'));
			}
			
		}
	}
	
	public function delete($id= null){
		$this->Contents->id = $id;
		
		if ($this->Contents->delete()) {
			$this->Session->setFlash(__('article deleted'));
		} 
		else{
			$this->Session->setFlash(__('The article could not be saved. Please, try again.'));
		}
		
		$this->redirect('/admin/ManageContents');
	}

	private function getCategoryContents(){
		$list = $this->Categories->find('all',array(
			'fields' => array('id', 'category', 'category_alias'),
			'conditions' => array('category_type' => 2)
		));
		$this->set("category",$list);
	}

	private function getCompanies(){
		$companies = $this->CorporatePr->find('all',array(
			'fields' => array('id', 'company_name'),
			'conditions' => array('status' => 1)
		));

		$this->set("companies",$companies);
	}

	private function setParamsPhotos($params = null){

		if(empty($params['Contents']['image']['name'])) {
			unset($params['Contents']['image']);
		}else{
			$params['Contents']['image'] = $this->upload_img_contents($params['Contents']['image']);
		}


		return $params;
	}
	
	
	private function setParamsPhotosByName($params = null, $imageName){

		if(empty($params['Contents'][$imageName]['name'])) {
			unset($params['Contents'][$imageName]);
		}else{
			$params['Contents'][$imageName] = $this->upload_img_contents($params['Contents'][$imageName]);
		}


		return $params;
	}
	
    // function upload img into /upload/secret/companies forder
    private function upload_img_contents($img=null){

        if(!empty($img)){
            $path= WWW_ROOT . '/upload/contents/';
            $ext = explode(".",$img['name']);
            $img['name'] = $ext[0].'-'.rand(0,9999).'.'.$ext[sizeof($ext)-1];
            if(!file_exists($path)){
                mkdir($path, 0755, true);
            }
            move_uploaded_file($img['tmp_name'],$path .$img['name']);
            return $img['name'];
        }

    }


    public function getSecretJobByCompanyId($id)
    {
    	$companies = $this->CareerOpportunity->find('all',array(
			'fields' => array('id', 'job'),
			'conditions' => array('company_id' => $id, 'status' => 1)
		));

		echo json_encode($companies);
		die;
    }
	
}
