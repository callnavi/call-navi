<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ManageMetaController extends AdminAppController {

	public $uses = array();
	public $helpers = array('Paginator','Html');
    public $components = array('Session');
    public $paginate = array();
	

/**
 * This controller does not use a model
 *
 * @var array
 */
	public function beforeFilter() {
		parent::beforeFilter();
		$this->isPublic();
	}
    
    public function index()
    {
		if($this->request->is("POST")) {
			$data = $this->request->data;
			$data = $this->setParamsPhotos($data);
			Cache::write('meta_desciption', $data['ManageMeta']['meta_desciption'],'meta');
			Cache::write('meta_keywords', $data['ManageMeta']['meta_keywords'], 'meta');
			if(!empty($data['ManageMeta']['ogp_img']))
				Cache::write('ogp_img', $data['ManageMeta']['ogp_img'], 'meta');
			if(!empty($data['ManageMeta']['apple_touch_img']))
				Cache::write('apple_touch_img', $data['ManageMeta']['apple_touch_img'], 'meta');
		}


    }

	private function setParamsPhotos($params = null){

		if(empty($params['ManageMeta']['ogp_img']['name'])) {
			unset($params['ManageMeta']['ogp_img']);
		}else {
			$params['ManageMeta']['ogp_img'] = $this->upload_img_meta($params['ManageMeta']['ogp_img']);
		}

		if(empty($params['ManageMeta']['apple_touch_img']['name'])) {
			unset($params['ManageMeta']['apple_touch_img']);
		}else{
			$params['ManageMeta']['apple_touch_img'] = $this->upload_img_meta($params['ManageMeta']['apple_touch_img']);
		}


		return $params;
	}

	// function upload img into /upload/secret/companies forder
	private function upload_img_meta( $img=null){

		if(!empty($img)){
			$path= WWW_ROOT . '/upload/meta/';
			if(!file_exists($path)){
				mkdir($path, 0777, true);
			}
			move_uploaded_file($img['tmp_name'],$path .$img['name']);

			return $img['name'];
		}

	}

}
