<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AdminAppController extends Controller {
	public function beforeFilter() {
    	parent::beforeFilter();
		header('X-Robots-Tag: noindex, nofollow, noarchive, nosnippet, noimageindex');
	}
	
    public function isPublic(){
        if(!$this->Session->check('User')){
            $this->redirect('/login');
        }
    }

    //check_html
    function check_html ($str, $strip = "")
    {
        global $vnT;

        $AllowableHTML = ($strip != "nohtml") ? $strip : "" ;
        $txt = strip_tags($str,$AllowableHTML);

        preg_match_all("/<([^>]+)>/i",$AllowableHTML,$allTags,PREG_PATTERN_ORDER);
        foreach ($allTags[1] as $tag){
            $txt = preg_replace("/<".$tag."[^>]*>/i","<".$tag.">",$txt);
        }


        $txt = str_replace("\n\r","",$txt);
        $txt = str_replace("\n","",$txt);
        $txt = str_replace("\r","",$txt);
        return $txt;
    }


    // cut_string
    function cut_string ($str, $len, $more=null)
    {
        if ($str == '' || $str == NULL) return $str;
        if (is_array($str)) return $str;
        $str = trim($str);
        if (strlen($str) <= $len) return $str;
        $str = substr($str, 0, $len);
        if ($str != '')
        {
            if (! substr_count($str, " "))
            {
                if ($more) $str .= " ...";
                return $str;
            }
            while (strlen($str) && ($str[strlen($str) - 1] != " "))
                $str = substr($str, 0, - 1);
            $str = substr($str, 0, - 1);
            if ($more) $str .= " ...";
        }
        return $str;
    }

    // get index of start and end in this page
    // in put is $this->params['paging']['model_name']
    // return $numberStartEnd['start'] is index statrt and $numberStartEnd['end'] is index end in this page
    public function getNumberStartEndinPage($arr = null){
        if(!empty($arr)){
            if($arr['count'] != 0) {
                $start = (($arr['page'] - 1) * 20) + 1;
                $end = $arr['page'] == $arr['pageCount'] ? $arr['count'] : $arr['page'] * 20;
            }else{
                $start = 0;
                $end = 0;
            }
            $numberStartEnd = array('start' => $start, 'end' => $end );

            return $numberStartEnd;
        }
    }


}