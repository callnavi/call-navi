<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class BlogsController extends AdminAppController {

    public $uses = array('Blogs','BlogsCategory');
    public $helpers = array('Paginator','Html');
    public $components = array('Session');
    public $paginate = array();


    /**
     * This controller does not use a model
     *
     * @var array
     */
    public function beforeFilter() {
        parent::beforeFilter();
        $this->isPublic();
    }

    public function index()
    {
        $conditions ='';
        if($this->request->is('get')){
            // search parameter
            $data = $this->request->query;
            $getKeywords = isset($data['keywords']) ? $data['keywords'] :'' ;
            $getBlogCategory = isset($data['blog_category']) ? $data['blog_category'] :'' ;

            $keywords = !empty($getKeywords) ? "Blogs.title like '%".$getKeywords."%' OR Blogs.description like '%".$getKeywords."%'" :'' ;
            $blogCategory = !empty($getBlogCategory) ? "blogs_category.cat_id like '".$getBlogCategory."'" :'' ;
            $conditions = array('AND'=>array($keywords,$blogCategory));

            $this->set("keywords",$getKeywords );
            $this->set("getBlogCategory",$getBlogCategory );
        }

        $this->paginate = array(
            'fields' => array('created', 'title', 'short', 'id','pic', 'blogs_category.title'),
            'joins' => array(
                array(
                    'table' => 'blogs_category',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'blogs_category.cat_id = Blogs.cat_id',
                    ),
                ),
            ),
            'conditions'=>$conditions,
            'limit' => 20,
            'order' => array(
                'created' => 'DESC'
            )
        );

        $val= $this->paginate('Blogs');
        //pr($val);die;
        $this->set("list",$val);

        $this->getCategoryBlogs();

        //get total article
        $total = $this->Blogs->find("count",array(
            'conditions'=>$conditions,
            'joins' => array(
                array(
                    'table' => 'blogs_category',
                    'type' => 'LEFT',
                    'conditions' => array(
                        'blogs_category.cat_id = Blogs.cat_id',
                    ),
                ),
            ),
        ));
        $this->set("total",$total);

        // get index in this page
        $AppController = new AdminAppController;
        $numberStartEnd = $AppController->getNumberStartEndinPage($this->params['paging']['Blogs']);
        $this->set("numberStartEnd",$numberStartEnd);
    }

    public function add(){
        $this->getCategoryBlogs();
        $lastId = $this->Blogs->query("SHOW TABLE STATUS WHERE name = 'blogs'");
        $this->set('lastId', $lastId[0]['TABLES']['Auto_increment']);
        if($this->request->is('Post')):
            $isPreview = !empty($this->request->data['isPreview'])? $this->request->data['isPreview']  : null;
            $param['Blogs'] = $this->request->data['Blogs'];
            $param = $this->setParamsPhotos($param);
            $param['Blogs']['status'] = !empty($isPreview) ? 0 : 1;
                
            if(empty($param['Blogs']['friendly_title'])){
                $param['Blogs']['friendly_title'] = $param['Blogs']['title'];
            }

            if(empty($param['Blogs']['metadesc'])){
                $param['Blogs']['metadesc'] = $this->cut_string($this->check_html($param['Blogs']['description'],'nohtml'),150,"");
            }


            $ok = $this->Blogs->save($param['Blogs']);
            if ($ok) {
                $this->Session->setFlash(__('The article has been saved.'));
                return $this->redirect('/admin/Blogs');
            } else {
                $this->Session->setFlash(__('The article could not be saved. Please, try again.'));
            }

        endif;


    }

    public function edit($id= null){
        $item = $this->Blogs->find('first', array(
            'conditions' => array('id' => $id),
        ));
        $this->getCategoryBlogs();
        $this->set("item",$item);

        if($this->request->is('post')){
            if (!$this->Blogs->exists($id)) {
                throw new NotFoundException(__('Invalid article'));
            }
            $param['Blogs'] = $this->request->data['Blogs'];
            $param = $this->setParamsPhotos($param);

            $this->Blogs->id=$id;
            $this->Blogs->set($param);

            $ok = $this->Blogs->save();
            if ($ok) {
                $this->Session->setFlash(__('The article has been saved.'));
                return $this->redirect('/admin/Blogs');
            } else {
                $this->Session->setFlash(__('The article could not be saved. Please, try again.'));
            }
        }
    }

    public function delete($catId= null){

        $this->Blogs->id = $catId;

        if ($this->Blogs->delete()) {
            $this->Session->setFlash(__('article deleted'));
        }
        else{
            $this->Session->setFlash(__('The article could not be saved. Please, try again.'));
        }

        $this->redirect('/admin/Blogs');

    }

    //get list blog category
    private function getCategoryBlogs(){
        $list = $this->BlogsCategory->find('all',array(
            'fields' => array('cat_id', 'title'),
        ));
        $this->set("category",$list);
    }

    private function setParamsPhotos($params = null){

        if(empty($params['Blogs']['pic']['name'])) {
            unset($params['Blogs']['pic']);
        }else{
            $params['Blogs']['pic'] = $this->upload_img_blogs($params['Blogs']['pic']);
        }
        return $params;
    }
    // function upload img into /upload/secret/companies forder
    private function upload_img_blogs($img=null){
        
        if( !empty($img)){
            $path= WWW_ROOT . '/upload/blogs/';
            $ext = explode(".",$img['name']);
            $img['name'] = $ext[0].'-'.rand(0,999999).'.'.$ext[sizeof($ext)-1];
            if(!file_exists($path)){
                mkdir($path, 0755, true);
            }
            move_uploaded_file($img['tmp_name'],$path .$img['name']);

            return $img['name'];
        }

    }

}
