<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class UploadImageController extends AdminAppController {

    public $uses = array('');
    public $helpers = array('Html');
    public $components = array('Session',);
    public $paginate = array();
    
    public function index(){
        //upload img in textarea
        if($this->request->is("Post")) {
            $img = $this->request->params['form']['file'];
            $path = WWW_ROOT . '/upload/textarea/';
            $ext = explode(".", $img['name']);
            $img['name'] = $ext[0] . rand(0, 9999) . '.' . $ext[sizeof($ext) - 1];
            $imgUrl = '/upload/textarea/';
            if (!file_exists($path)) {
                mkdir($path, 0755, true);
            }
            move_uploaded_file($img['tmp_name'], $path . $img['name']);
            $img = '//' . $_SERVER['HTTP_HOST'] . $imgUrl . $img['name'];

            $response = new StdClass;
            $response->link = $img;
            echo stripslashes(json_encode($response));
            die;
        }
    }
	


}
