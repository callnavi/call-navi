<?php
App::uses('AppController', 'Controller');

/**
 * Upgraded by Xuong
 * Upgraded at: 2017-10-11
 * Description: upload Company and Job by CSV
 */
class UploadCsvController extends AdminAppController {

	public $uses = array('CorporatePr','CareerOpportunities');
	public $helpers = array('Paginator','Html');
    public $components = array('Session','Csv');
    public $paginate = array();
	public $status ='';
	
	public function beforeFilter() {
		parent::beforeFilter();
        $this->isPublic();
	}
    public $company =null;
	
    /**
     * action: upload Company
     */
    public function index()
    {
		//pr($this->AppUploadCSVModel->hello());
		if ($this->request->is('post')) {
			$model = 'CorporatePrsUploadCSVModel';
			$this->loadModel($model);
			$this->uploadCommonFunction($model);
        }
    }
	
    /**
     * action: upload Job
     */
    public function UploadJob()
    {
        $companyList = $this->CorporatePr->find('all', array(
            'fields'=>array('id','company_name'),
        ));
        $this->set("companyList", $companyList);
        if ($this->request->is('post')) {
			$model = 'CareerOpportunityUploadCSVModel';
			$this->loadModel($model);
			$this->{$model}->company = $this->request->data['company'];
			$this->uploadCommonFunction($model);
        }
    }
	
	public function ReadLog()
	{
		$file = $this->request->query['log'];
		if($file)
		{
			header('Content-Type: application/json; charset=utf-8');
			echo $this->Csv->getTMPReport($file);
		}
		die;
	}
	
	public function TestMode()
	{
		if(isset($this->request->query['testmode']) && $this->request->query['testmode']=='false')
		{
			$this->Session->delete('test_mode');
			echo 'deleted test mode, CSV will be insert/update into database <a href="/admin">go back Admin</a>';
		}
		else
		{
			$this->Session->write('test_mode', 'true');
			echo 'allow test mode, now you can test CSV without insert/update into database <a href="/admin">go back Admin</a>';
		}
		die;
	}
	
	/**
	 * a common check and show result for actions
	 * @param string $model CorporatePr|CareerOpportunities
	 */
	private function uploadCommonFunction($model)
	{
		$result = array();
		$csv = $this->Csv;
		if ($csv->checkFile()) {
			$file = $csv->getFile();
			$result = $this->{$model}->updateByCsv($csv,$file);
			$logName = $csv->backupTMPReport($result);
			if ($result){
				$this->status = $this->status . 'アップロード完了しました';//upload success
			} else {
				$this->status = $this->status . 'アップロード失敗しました。再処理してください。';//upload not success
			}
		} else {
			$this->status = $this->status . 'アップロード失敗しました。再処理してください。';//File type must be *.csv
		}
		$this->set('status', $this->status);
		$this->set('result', $result);
		$this->set('log_name', $logName);
	}
}
