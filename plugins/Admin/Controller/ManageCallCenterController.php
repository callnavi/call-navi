<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ManageCallCenterController extends AdminAppController {

	public $uses = array('Area','Callcenter');
	public $helpers = array('Paginator','Html');
    public $components = array('Session','RequestHandler');
    public $paginate = array();
	

/**
 * This controller does not use a model
 *
 * @var array
 */
	public function beforeFilter() {
		parent::beforeFilter();
		$this->isPublic();
	}
    
    public function index()
    {
		$conditions = '';
		if($this->request->is('get')){
			// search parameter
//			$data = $this->request->data['SearchCallCenter'];
			$data = $this->request->query;
			$getKeywords = isset($data['keywords']) ? $data['keywords'] :'' ;
			$getProvinceId = isset($data['province']) ? $data['province'] :'' ;
			$getCityId = isset($data['city_id']) ? $data['city_id'] :'' ;

			$keywords = !empty($getKeywords) ? "Callcenter.company_name like '%".$getKeywords."%'" :'' ;
			$searchProvince = !empty($getProvinceId) ? "area_parent.id = '".$getProvinceId."'" :'' ;
			$searchCity = !empty($getCityId) ? "Area.id = '".$getCityId."'" :'' ;
			$conditions = array('AND'=>array($keywords,$searchProvince,$searchCity));

			$this->set("keywords",$getKeywords );
			$this->set("searchProvince",$getProvinceId );
			$this->set("searchCity",$getCityId );
		}
		$this->paginate = array(
			'fields' => array(
				'id',
				'company_name',
				'address',
				'tel',
				'Area.name',
				'area_parent.name as parent_name'),
			'limit' => 20,
			'joins' => array(
				array(
					'alias' => 'Area',
					'table' => 'area',
					'type' => 'LEFT',
					'conditions' => array(
						'Area.id = Callcenter.city_id',
					),
				),
				array(
					'alias' => 'area_parent',
					'table' => 'area',
					'type' => 'LEFT',
					'conditions' => array(
						'area_parent.id = Area.parent_id',
					),
				),
			),
			'order' => array(
				'Callcenter.created' => 'DESC'
			),
			'conditions'=>$conditions,
		);

		$data= $this->paginate('Callcenter');

		$this->set("list",$data);

		//get total record
		$total = $this->Callcenter->find('count', array(
			'joins' => array(
				array(
					'alias' => 'Area',
					'table' => 'area',
					'type' => 'LEFT',
					'conditions' => array(
						'Area.id = Callcenter.city_id',
					),
				),
				array(
					'alias' => 'area_parent',
					'table' => 'area',
					'type' => 'LEFT',
					'conditions' => array(
						'area_parent.id = Area.parent_id',
					),
				),
			),
			'conditions'=>$conditions,
		));
		$this->set("total",$total);

		// get index in this page
		$AppController = new AdminAppController;
		$numberStartEnd = $AppController->getNumberStartEndinPage($this->params['paging']['Callcenter']);
		$this->set("numberStartEnd",$numberStartEnd);

		//debug($this->Callcenter->getDataSource()->getLog(false, false)); //show last sql query
		//get list province
		$province = $this->Area->find('all',array(
			'fields' => array('id','name'),
			'conditions' => array(
				'parent_id' => 0,
			),
		));


		$this->set("province",$province);
		//pr($data);die;

    }
	
	public function add(){
		//get list province
		$province = $this->Area->find('all',array(
			'conditions' => array(
				'parent_id' => 0,
			),
		));
		$this->set("province",$province);

		$lastId = $this->Callcenter->query("SHOW TABLE STATUS WHERE name = 'callcenter'");
		$this->set('lastId', $lastId[0]['TABLES']['Auto_increment']);
		
		if($this->request->is('post')){
			$param['Callcenter'] = $this->request->data['ManagerCallCenter'];

			$this->Callcenter->create();
			
			if ($this->Callcenter->save($param)) {
				$CallcenterAmount = $this->getCallcenterAmount($param['Callcenter']['province']);
				$this->Area->query("UPDATE `area` SET `callcenter_amount` = ".$CallcenterAmount."  WHERE id =". $param['Callcenter']['province']);
				$this->Session->setFlash(__('The article has been saved.'));
				return $this->redirect('/admin/ManageCallCenter');
			} else {
				$this->Session->setFlash(__('The article could not be saved. Please, try again.'));
			}
		}
	}
	
	public function edit($id= null){
		// get company data
		$item =$this->Callcenter->find('first', array(
			'fields' => array(
				'Callcenter.*',
				'area_parent.id as province_id'),
			'joins' => array(
				array(
					'alias' => 'Area',
					'table' => 'area',
					'type' => 'LEFT',
					'conditions' => array(
						'Area.id = Callcenter.city_id',
					),
				),
				array(
					'alias' => 'area_parent',
					'table' => 'area',
					'type' => 'LEFT',
					'conditions' => array(
						'area_parent.id = Area.parent_id',
					),
				),
			),
			'conditions' => array(
				'Callcenter.id' => $id,
			),
		));

		//get list province
		$province = $this->Area->find('all',array(
			'conditions' => array(
				'parent_id' => 0,
			),
		));


		//pr($city);die;

		$this->set("province",$province);
		$this->set("item",$item);

		if($this->request->is('post')){
			if (!$this->Callcenter->exists($id)) {
				throw new NotFoundException(__('Invalid article'));
			}
			$param['Callcenter'] = $this->request->data['ManagerCallCenter'];
			//pr($param);die;

			$this->Callcenter->id=$id;
			$this->Callcenter->set($param);
			
			if ($this->Callcenter->save()) {
				$CallcenterAmount = $this->getCallcenterAmount($param['Callcenter']['province']);
				$this->Area->query("UPDATE `area` SET `callcenter_amount` = ".$CallcenterAmount."  WHERE id =". $param['Callcenter']['province']);
				$this->Session->setFlash(__('The article has been saved.'));
				return $this->redirect('/admin/ManageCallCenter');
			} else {
				$this->Session->setFlash(__('The article could not be saved. Please, try again.'));
			}
		}
	}
	
	public function delete($id= null){

		$provinceId = $this->Callcenter->find('first', array(
			'fields' => array(
				'Area.parent_id'),
			'conditions' => array(
				'Callcenter.id ' => $id,
			),
			'joins' => array(
				array(
					'alias' => 'Area',
					'table' => 'area',
					'type' => 'LEFT',
					'conditions' => array(
						'Area.id = Callcenter.city_id',
					),
				)
			),
		));
		//pr($provinceId);die;
		$this->Callcenter->id = $id;
		
		if ($this->Callcenter->delete()) {
			$CallcenterAmount = $this->getCallcenterAmount($provinceId['Area']['parent_id']);
			$this->Area->query("UPDATE `area` SET `callcenter_amount` = ".$CallcenterAmount." - 1 WHERE id =". $provinceId['Area']['parent_id']);
			$this->Session->setFlash(__('article deleted'));
		} 
		else{
			$this->Session->setFlash(__('The article could not be saved. Please, try again.'));
		}
		
		$this->redirect('/admin/ManageCallCenter');
	}

	public function getListCity(){
//		$this->autoRender = false;
//		if ( $this->RequestHandler->isAjax() ) {
//			Configure::write ( 'debug', 0 );
//		}
		$provinceId = $this->request->data;
		$conditions = !empty($provinceId) ? array('parent_id ' => $provinceId['provinceId']) : null;
		$city = $this->Area->find('all',array(
			'fields' => array('id','name'),
			'conditions' => $conditions,
		));

		echo json_encode($city);die;
	}

	private function getCallcenterAmount($provicenId = null){
		$callcenterAmount = $this->Callcenter->find('count', array(
			'fields' => array(
				'id',
				'company_name',
				'address',
				'tel',
				'Area.name',
				'area_parent.name as parent_name'),
			'limit' => 20,
			'joins' => array(
				array(
					'alias' => 'Area',
					'table' => 'area',
					'type' => 'LEFT',
					'conditions' => array(
						'Area.id = Callcenter.city_id',
					),
				),
				array(
					'alias' => 'area_parent',
					'table' => 'area',
					'type' => 'LEFT',
					'conditions' => array(
						'area_parent.id = Area.parent_id',
					),
				),
			),
			'order' => array(
				'Callcenter.created' => 'DESC'
			),
			'conditions'=>array(
				"area_parent.id" =>$provicenId,
			),
		));
		return $callcenterAmount ;
	}

	
	
}
