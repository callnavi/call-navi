<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class BlogsCategoryController extends AdminAppController {

	public $uses = array('BlogsCategory');
	public $helpers = array('Paginator','Html');
    public $components = array('Session');
    public $paginate = array();
	

/**
 * This controller does not use a model
 *
 * @var array
 */
	public function beforeFilter() {
		parent::beforeFilter();
		$this->isPublic();
	}
    
    public function index()
    {
		$conditions ='';
		if($this->request->is('get')){
			// search parameter
			$data = $this->request->query;
			$getKeywords = isset($data['keywords']) ? $data['keywords'] :'' ;

			$keywords[0] = !empty($getKeywords) ? "BlogsCategory.title like '%".$getKeywords."%'" :'' ;
			$keywords[1] = !empty($getKeywords) ? "BlogsCategory.short like '%".$getKeywords."%'" :'' ;
			$keywords[1] = !empty($getKeywords) ? "BlogsCategory.description like '%".$getKeywords."%'" :'' ;
			$conditions = array('OR'=>$keywords);

			$this->set("keywords",$getKeywords );
		}

		$this->paginate = array(
			'fields' => array('created', 'title', 'short', 'cat_id'),
			'limit' => 20,
			'conditions'=>$conditions,
			'order' => array(
				'created' => 'DESC'
			)
		);

		$val= $this->paginate('BlogsCategory');
		$this->set("list",$val);

		//get total article
		$total = $this->BlogsCategory->find("count",array(
			'conditions'=>$conditions,
		));
		$this->set("total",$total);

		// get index in this page
		$AppController = new AdminAppController;
		$numberStartEnd = $AppController->getNumberStartEndinPage($this->params['paging']['BlogsCategory']);
		$this->set("numberStartEnd",$numberStartEnd);
    }
	
	public function add(){
		if($this->request->is('Post')):
			$param['BlogsCategory'] = $this->request->data['BlogsCategory'];
			$param = $this->setParamsPhotos($param);
			if(empty($param['BlogsCategory']['friendly_title'])){
				$param['BlogsCategory']['friendly_title'] = $param['BlogsCategory']['title'];
			}

			if(empty($param['BlogsCategory']['metadesc'])){
				$param['BlogsCategory']['metadesc'] = $this->cut_string($this->check_html($param['BlogsCategory']['short'],'nohtml'),150,"");
			}

			$ok = $this->BlogsCategory->save($param['BlogsCategory']);
			if ($ok) {
				$this->Session->setFlash(__('The article has been saved.'));
				return $this->redirect('/admin/BlogsCategory');
			} else {
				$this->Session->setFlash(__('The article could not be saved. Please, try again.'));
			}

		endif;

		
	}
	
	public function edit($cat_id= null){
		$item = $this->BlogsCategory->find('first', array(
			'conditions' => array('cat_id' => $cat_id),
		));
		$this->set("item",$item);
			
		if($this->request->is('post')){
			if (!$this->BlogsCategory->exists($cat_id)) {
				throw new NotFoundException(__('Invalid article'));
			}
			$param['BlogsCategory'] = $this->request->data['BlogsCategory'];
			$param = $this->setParamsPhotos($param);
			$this->BlogsCategory->id=$cat_id;
			$this->BlogsCategory->set($param);
			if ($this->BlogsCategory->save()) {
				$this->Session->setFlash(__('The article has been saved.'));
				return $this->redirect('/admin/BlogsCategory');
			} else {
				$this->Session->setFlash(__('The article could not be saved. Please, try again.'));
			}
		}
	}
	
	public function delete($cat_id= null){

		$this->BlogsCategory->id = $cat_id;

		if ($this->BlogsCategory->delete()) {
			$this->Session->setFlash(__('article deleted'));
		}
		else{
			$this->Session->setFlash(__('The article could not be saved. Please, try again.'));
		}

		$this->redirect('/admin/BlogsCategory');

	}
	private function setParamsPhotos($params = null){

		if(empty($params['BlogsCategory']['pic']['name'])) {
			unset($params['BlogsCategory']['pic']);
		}else{
			$params['BlogsCategory']['pic'] = $this->upload_img_blogs_category($params['BlogsCategory']['pic'], 0);
		}

		if(empty($params['BlogsCategory']['avatar']['name'])) {
			unset($params['BlogsCategory']['avatar']);
		}else{
			$params['BlogsCategory']['avatar'] = $this->upload_img_blogs_category($params['BlogsCategory']['avatar'],1);
		}
		return $params;
	}
    // function upload img into /upload/secret/companies forder
    private function upload_img_blogs_category($img=null, $isAvata = 0){

        if( !empty($img)){
            $path= $isAvata == 1 ? WWW_ROOT . '/upload/blogs-category/avatar/' : WWW_ROOT . '/upload/blogs-category/';
            $ext = explode(".",$img['name']);
            $img['name'] = $ext[0].'-'.rand(0,999999).'.'.$ext[sizeof($ext)-1];
            if(!file_exists($path)){
                mkdir($path, 0755, true);
            }
            move_uploaded_file($img['tmp_name'],$path .$img['name']);

            return $img['name'];
        }

    }

	
}
