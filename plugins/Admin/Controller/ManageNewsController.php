<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ManageNewsController extends AdminAppController {

	public $uses = array('ContentsEmail','Categories','ContentsEmailCategory');
	public $helpers = array('Paginator','Html');
    public $components = array('Session');
    public $paginate = array();
	

/**
 * This controller does not use a model
 *
 * @var array
 */
	public function beforeFilter() {
		parent::beforeFilter();
		$this->isPublic();
	}
    
    public function index()
    {
		$page = isset($_GET["page"])?intval($_GET["page"]):1;
		$size = 20;

		$getKeywords = null;
		$getCategory = null;
		if($this->request->is('get')){
			// search parameter
			$data = $this->request->query;
			$getKeywords = isset($data['keywords']) ? $data['keywords'] :'' ;
			$getCategory = isset($data['category']) ? $data['category'] :'' ;

			$this->set("keywords",$getKeywords );
			$this->set("getCategory",$getCategory );
		}

		$data = $this->ContentsEmail->getList($page,$size,$getCategory,null,$getKeywords,1);
		$total = $this->ContentsEmail->countListByAlias($getCategory,$getKeywords, 1);


		$catList= $this->Categories->find('all', array(
			'conditions' => array('category_type' => 1 ),
		));
		//pr($data);die;
		$this->set("catList",$catList);
		$pageCount = ceil($total/$size);
		$this->set("total", $total);
		$this->set("list",$data);
		$this->set("currentPage",$page);
		$this->set("pageCount",$pageCount);

		// get index in this page
		$numberStartEnd = $this->getNumberStartEndinPageNews($total, $page, $pageCount);
		$this->set("numberStartEnd",$numberStartEnd);

    }
	
	public function add(){
		$catList= $this->Categories->find('all', array(
			'conditions' => array('category_type' => 1 ),
		));
		$this->set("catList",$catList);

		$lastId = $this->ContentsEmail->query("SHOW TABLE STATUS WHERE name = 'contents_email'");
		$this->set('lastId', $lastId[0]['TABLES']['Auto_increment']);

		if($this->request->is('post')){
			$isPreview = !empty($this->request->data['isPreview'])? $this->request->data['isPreview']  : null;
			$param['ContentsEmail'] = $this->request->data['ManageNews'];
			$param['ContentsEmail']['status'] = !empty($isPreview) ? 0 : 1;

			//insert into content email category
			$category = $param['ContentsEmail']['Categories'];
			unset($param['ContentsEmail']['Categories']);

			$param = $this->setParamsPhotos($param);
			$this->ContentsEmail->create();
			
			if ($this->ContentsEmail->save($param)) {
				$this->updateContentsEmailCategory($category,  $this->ContentsEmail->getLastInsertID());
				$this->Session->setFlash(__('The article has been saved.'));
				return $this->redirect('/admin/ManageNews');
			} else {
				$this->Session->setFlash(__('The article could not be saved. Please, try again.'));
			}
		}
	}
	
	public function edit($id= null){
		$item =$this->ContentsEmail->getDetail($id, 1);

		$catList= $this->Categories->find('all', array(
			'conditions' => array('category_type' => 1 ),
		));

		$this->set("item",$item);
		$this->set("catList",$catList);

		if($this->request->is('post')){
			if (!$this->ContentsEmail->exists($id)) {
				throw new NotFoundException(__('Invalid article'));
			}
			$param['ContentsEmail'] = $this->request->data['ManageNews'];
			//insert into content email category
			$category = $param['ContentsEmail']['Categories'];
			unset($param['ContentsEmail']['Categories']);

			$param = $this->setParamsPhotos($param);
            // upload img into database
			$this->ContentsEmail->id=$id;
			$this->ContentsEmail->set($param);
			if ($this->ContentsEmail->save()) {
				$this->updateContentsEmailCategory($category, $id);
				$this->Session->setFlash(__('The article has been saved.'));
				return $this->redirect('/admin/ManageNews');
			} else {
				$this->Session->setFlash(__('The article could not be saved. Please, try again.'));
			}
		}
	}
	
	public function delete($id= null){
		$this->ContentsEmail->id = $id;
		$param['ContentsEmail']['status'] = 2;
		$this->ContentsEmail->set($param);
		if ($this->ContentsEmail->save()) {
			$this->Session->setFlash(__('article deleted'));
		} 
		else{
			$this->Session->setFlash(__('The article could not be saved. Please, try again.'));
		}
		
		$this->redirect('/admin/ManageNews');
	}

	private function setParamsPhotos($params = null){

		if(empty($params['ContentsEmail']['featured_picture']['name'])) {
			unset($params['ContentsEmail']['featured_picture']);
		}else{
			$params['ContentsEmail']['featured_picture'] = $this->upload_img_company($params['ContentsEmail']['featured_picture']);
		}
		return $params;
	}

    // function upload img into /upload/secret/companies forder
    private function upload_img_company( $img=null){

        if(!empty($img)){
            $path= WWW_ROOT . '/upload/news/';
            $ext = explode(".",$img['name']);
            $img['name'] = $ext[0].'-'.rand(0,99999).'.'.$ext[sizeof($ext)-1];
            if(!file_exists($path)){
                mkdir($path, 0755, true);
            }
            move_uploaded_file($img['tmp_name'],$path .$img['name']);

            return $img['name'];
        }

    }

	private function updateContentsEmailCategory($catArr =null,$contentEmailId = null){
		$this->ContentsEmailCategory->useTable = 'contents_email_category';

		foreach($catArr as $catEmail){
			$isExist = $this->ContentsEmailCategory->find('first', array(
				'conditions' => array('content_id' => $contentEmailId,'email_to' => $catEmail ),
			));

			if(empty($isExist)){

				$param['ContentsEmailCategory'] = array('content_id'=>$contentEmailId,'email_to'=>$catEmail );
				$this->ContentsEmailCategory->create();
				$this->ContentsEmailCategory->save($param);
			}
		}
	}

	private function getNumberStartEndinPageNews($count, $page, $pageCount){

		if($count != 0) {
			$start = (($page - 1) * 20) + 1;
			$end = $page == $pageCount ? $count : $page * 20;
		}else{
			$start = 0;
			$end = 0;
		}

		return array('start' => $start, 'end' => $end );

	}
}
