<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');
/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ManageEntryController extends AdminAppController {

	public $uses = array('Entry','CareerOpportunities','CorporatePr');
	public $helpers = array('Paginator','Html');
    public $components = array('Session', 'NewFormatId');
    public $paginate = array();
	

/**
 * This controller does not use a model
 *
 * @var array
 */
	public function beforeFilter() {
		parent::beforeFilter();
		$this->isPublic();
	}
    

    /*
		Created at: 2017-07-18
		Author: Binh
		Des: search list entry apply
	*/
    public function index()
    {
    	//new format 
        //$this->NewFormatId->index();
    	
    	//set condition
    	$conditions ='';
		if($this->request->is('get'))
		{
			$data = $this->request->query;
			$keywords = isset($data['keywords']) ? $data['keywords'] :'' ;
			$this->set("keywords",$keywords);

			$keywords = '%'.trim($keywords).'%';

			$conditions = array(
	        	'OR' => array(
		            array('apply_code LIKE' => $keywords),
		            array('full_name LIKE' => $keywords),
		            array('created LIKE' => str_replace('/','-',$keywords))
		        )
	        );
		}

		
		$this->paginate = array(
			'fields' => array('apply_code', 'created', 'full_name', 'id'),
			'conditions'=>$conditions,
			'limit' => 20,
			'order' => array(
				'created' => 'DESC'
			)
		);

		$listEntry = $this->paginate('Entry');
		$this->set("listEntry", $listEntry);

    }

    public function view($id)
    {
    	if($id)
    	{
    		$entry = $this->Entry->findById($id);
    		if($entry)
    		{
    			$entry = $entry['Entry'];
    			$this->set("entry", $entry);
    		}
    	}
    	else
    	{
    		$this->redirect('/admin/ManageEntry');
    	}
    }

    /*
		Created at: 2017-07-19
		Author: Binh
		Des: download cv1 of entry
		$id: id of entry
	*/
    public function download_cv1($id)
    {
    	$this->autoRender = false;
    	if($id)
    	{
    		$entry = $this->Entry->findById($id);
    		if($entry)
    		{
    			$entry = $entry['Entry'];
				$result = $this->Entry->downloadFile($entry['cv1_path'], $entry['created'], $entry['job_id']);

				if(!$result)
				{
					$this->redirect('/admin/ManageEntry/view/' . $id);
				}
    		}
    	}
    }

    /*
		Created at: 2017-07-19
		Author: Binh
		Des: download cv2 of entry
		$id: id of entry
	*/
    public function download_cv2($id)
    {
    	$this->autoRender = false;
    	if($id)
    	{
    		$entry = $this->Entry->findById($id);
    		if($entry)
    		{
    			$entry = $entry['Entry'];
				$result = $this->Entry->downloadFile($entry['cv2_path'], $entry['created'], $entry['job_id']);

				if(!$result)
				{
					$this->redirect('/admin/ManageEntry/view/' . $id);
				}
    		}
    	}
    }
	
	public function add(){
		
	}
	
	public function edit($id= null){

		
	}
	
	public function delete($id= null){
		
	}

	
}
