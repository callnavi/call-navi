<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ManageCompanyController extends AdminAppController {

	public $uses = array('CorporatePr');
	public $helpers = array('Paginator','Html');
    public $components = array('Session');
    public $paginate = array();
	

/**
 * This controller does not use a model
 *
 * @var array
 */
	public function beforeFilter() {
		parent::beforeFilter();
		$this->isPublic();
	}
    
    public function index()
    {
		$conditions ='';
		if($this->request->is('get')){
			// search parameter
			$data = $this->request->query;
			$getKeywords = isset($data['keywords']) ? $data['keywords'] :'' ;
			$getSearchCompany = isset($data['company']) ? $data['company'] :'' ;

			$keywords = !empty($getKeywords) ? "CorporatePr.corporate like '%".$getKeywords."%' OR CorporatePr.company_name like '%".$getKeywords."%'" :'' ;
			$SearchCompany = !empty($getSearchCompany) ? "CorporatePr.company_name like '".$getSearchCompany."'" :'' ;
			$conditions = array('AND'=>array($keywords,$SearchCompany));

			$this->set("keywords",$getKeywords );
			$this->set("getSearchCompany",$getSearchCompany );
		}

		$this->paginate = array(
			'fields' => array('CorporatePr.*'),
			'limit' => 20,
			'order' => array(
				'CorporatePr.created' => 'DESC'
			),
			'conditions'=>$conditions,
		);
		
		$val= $this->paginate('CorporatePr');
		$this->set("list",$val);

		//get list company
		$getListCompany = $this->CorporatePr->find("all",array(
			'fields' => array('company_name'),
			'order' => array(
				'CorporatePr.created' => 'DESC'
			),
		));

		$this->set("getListCompany",$getListCompany);

		//get total article
		$total = $this->CorporatePr->find("count",array(
			'conditions'=>$conditions,
		));
		$this->set("total",$total);

		// get index in this page
		$AppController = new AdminAppController;
		$numberStartEnd = $AppController->getNumberStartEndinPage($this->params['paging']['CorporatePr']);
		$this->set("numberStartEnd",$numberStartEnd);
    }
	
	public function add(){
		$lastId = $this->CorporatePr->query("SHOW TABLE STATUS WHERE name = 'corporate_prs'");
		$this->set('lastId', $lastId[0]['TABLES']['Auto_increment']);

		if($this->request->is('post')){
            $flag = $this->checkCodeCompany($this->request->data['ManagerCompany']['corporate_code'] , $this->request->data['ManagerCompany']['corporate_code']);
            if($flag == true){
                $isPreview = !empty($this->request->data['isPreview'])? $this->request->data['isPreview']  : null;
                $param['CorporatePr'] = $this->request->data['ManagerCompany'];
                $param['CorporatePr']['status'] = !empty($isPreview) ? 0 : 1;
                $param = $this->setParamsPhotos($param);



                $this->CorporatePr->create();

                if ($this->CorporatePr->save($param)) {
                    $this->Session->setFlash(__('The article has been saved.'));
                    return $this->redirect('/admin/ManageCompany');
                } else {
                    $this->Session->setFlash(__('The article could not be saved. Please, try again.'));
                }
            }else{
                $this->set('corporate_code_1',$this->request->data['ManagerCompany']['corporate_code']);
                $this->set('error_message_code','Input another id!.');
            }
		}
	}
	
	public function edit($id= null){

		$item = $this->CorporatePr->find('first', array(
			'conditions' => array('id' => $id),
		));
		
         
        $this->set("item",$item);
        
		if($this->request->is('post')){
            
            //Check validation if the code already exist or not
            $flag = $this->checkCodeCompany($this->request->data['ManagerCompany']['corporate_code'] , $this->request->data['ManagerCompany']['corporate_code_hidden']);

            if($flag == true){
                if (!$this->CorporatePr->exists($id)) {
                    throw new NotFoundException(__('Invalid article'));
                }
                $param['CorporatePr'] = $this->request->data['ManagerCompany'];
                $param = $this->setParamsPhotos($param);
                // upload img into database
                $this->CorporatePr->id=$id;                
                $this->CorporatePr->set($param);                
                if ($this->CorporatePr->save()) {
                    $this->Session->setFlash(__('The article has been saved.'));
                    return $this->redirect('/admin/ManageCompany');
                } else {
                    $this->Session->setFlash(__('The article could not be saved. Please, try again.'));
                }
            }else{
                $this->set('corporate_code_1',$this->request->data['ManagerCompany']['corporate_code']);
                $this->set('error_message_code','Input another id!.');
            }
		}
	}
	
	public function delete($id= null){
		$this->CorporatePr->id = $id;
		
		if ($this->CorporatePr->delete()) {
			$this->Session->setFlash(__('article deleted'));
		} 
		else{
			$this->Session->setFlash(__('The article could not be saved. Please, try again.'));
		}
		
		$this->redirect('/admin/ManageCompany');
	}

	private function setParamsPhotos($params = null){

		if(empty($params['CorporatePr']['corporate_logo']['name'])) {
			unset($params['CorporatePr']['corporate_logo']);
		}else{
			$params['CorporatePr']['corporate_logo'] = $this->upload_img_company($params['CorporatePr']['corporate_logo']);
		}

		if(empty($params['CorporatePr']['president_avata']['name'])) {
			unset($params['CorporatePr']['president_avata']);
		}else{
			$params['CorporatePr']['president_avata'] = $this->upload_img_company($params['CorporatePr']['president_avata']);
		}

		return $params;
	}

    // function upload img into /upload/secret/companies forder
    private function upload_img_company( $img=null){

        if(!empty($img)){
            $path= WWW_ROOT . '/upload/secret/companies/';
            $ext = explode(".",$img['name']);
            $img['name'] = $ext[0].'-'.rand(0,99999).'.'.$ext[sizeof($ext)-1];
            if(!file_exists($path)){
                mkdir($path, 0755, true);
            }
            move_uploaded_file($img['tmp_name'],$path .$img['name']);

            return $img['name'];
        }

    }
    // validation for checking funtion
    public function checkCodeCompany($company_id,$hidden_id){
        $flag = false;
        if($company_id == $hidden_id){
            $flag = true;
        }else{
            $count = $this->CorporatePr->find('count', array(
                'conditions' => array('corporate_code' => $company_id)
            ));            
            if($count == 0 ){
                $flag=true;
            }
            
        }
        return $flag;
    }
	
}
