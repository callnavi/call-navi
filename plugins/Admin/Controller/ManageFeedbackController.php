<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ManageFeedbackController extends AdminAppController {

	public $uses = array('Feedback');
	public $helpers = array('Paginator','Html');
    public $components = array('Session');
    public $paginate = array();
	

/**
 * This controller does not use a model
 *
 * @var array
 */
	public function beforeFilter() {
		parent::beforeFilter();
		$this->isPublic();
	}
    
    public function index()
    {
		$conditions =array();
		if($this->request->is('get')){
			// search parameter
			$data = $this->request->query;
			$getKeywords = isset($data['keywords']) ? $data['keywords'] :'' ;
			$keywords = !empty($getKeywords) ? "Feedback.message like '%".$getKeywords."%'" :'' ;
			$conditions = array('AND'=>array($keywords));
			$this->set("keywords",$getKeywords );
		}
		
		$this->paginate = array(
			'fields' => array('Feedback.id',
						'Feedback.message',
						'Feedback.created'),
			'limit' => 30,
			'conditions'=>$conditions,
			'order' => array(
				'Feedback.created' => 'DESC'
			)
		);
		
		$val= $this->paginate('Feedback');
		
		$this->set("list",$val);

		$this->set("total",1);

		// get index in this page
		$this->set("numberStartEnd",99);
    }
	
	public function delete($id= null){
		$this->ContentsEmail->id = $id;
		$param['ContentsEmail']['status'] = 2;
		$this->ContentsEmail->set($param);
		if ($this->ContentsEmail->save()) {
			$this->Session->setFlash(__('article deleted'));
		} 
		else{
			$this->Session->setFlash(__('The article could not be saved. Please, try again.'));
		}
		
		$this->redirect('/admin/ManageNews');
	}
	
	private function getNumberStartEndinPageNews($count, $page, $pageCount){

		if($count != 0) {
			$start = (($page - 1) * 20) + 1;
			$end = $page == $pageCount ? $count : $page * 20;
		}else{
			$start = 0;
			$end = 0;
		}

		return array('start' => $start, 'end' => $end );

	}
}
