<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ManagePrettyController extends AdminAppController {

	public $uses = array('Pretty');
	public $helpers = array('Paginator','Html');
    public $components = array('Session','Csv');
    public $paginate = array();
	

/**
 * This controller does not use a model
 *
 * @var array
 */
	public function beforeFilter() {
		parent::beforeFilter();
		$this->isPublic();
	}
	public $status ='';
    public function index()
    {
		$conditions ='';
		if($this->request->is('get')){
			// search parameter
			$data = $this->request->query;
			$getKeywords = isset($data['keywords']) ? $data['keywords'] :'' ;

			$keywords[0] = !empty($getKeywords) ? "Pretty.name like '%".$getKeywords."%'" :'' ;
			$keywords[1] = !empty($getKeywords) ? "Pretty.company_name like '%".$getKeywords."%'" :'' ;
			$keywords[1] = !empty($getKeywords) ? "Pretty.address like '%".$getKeywords."%'" :'' ;
			$conditions = array('OR'=>$keywords);

			$this->set("keywords",$getKeywords );
		}

		$this->paginate = array(
			'fields' => array(),
			'limit' => 20,
			'conditions'=>$conditions,
			'order' => array(
				'created' => 'DESC'
			)
		);

		$val= $this->paginate('Pretty');
		$this->set("list",$val);

		//get total article
		$total = $this->Pretty->find("count",array(
			'conditions'=>$conditions,
		));
		$this->set("total",$total);

		// get index in this page
		$AppController = new AdminAppController;
		$numberStartEnd = $AppController->getNumberStartEndinPage($this->params['paging']['Pretty']);
		$this->set("numberStartEnd",$numberStartEnd);
		}


	public function add(){
		if($this->request->is('Post')):
			$param['Pretty'] = $this->request->data['Pretty'];
			$param['Pretty']['status'] = 1;
			$param = $this->setParamsPhotos($param);

			$ok = $this->Pretty->save($param['Pretty']);
			if ($ok) {
				$this->Session->setFlash(__('The pretty has been saved.'));
				return $this->redirect('/admin/ManagePretty');
			} else {
				$this->Session->setFlash(__('The pretty could not be saved. Please, try again.'));
			}

		endif;

		
	}
	
	public function edit($id =null){



		if (!$id && empty($this->request->data)) {
			$this->Session->setFlash(__d('croogo', 'Invalid pretty'), 'default', array('class' => 'error'));
			$this->redirect(array('action' => 'index'));
		}

		if (!empty($this->request->data)) {

			if (!$this->Pretty->exists($id)) {
				throw new NotFoundException(__('Invalid pretty'));
			}
			$param['Pretty'] = $this->request->data['Pretty'];
			$param = $this->setParamsPhotos($param);
			$this->Pretty->id=$id;
			if ($this->Pretty->save($param['Pretty'])) {
				$this->Session->setFlash(__('The pretty has been saved.'));
				return $this->redirect('/admin/ManagePretty');
			} else {
				$this->Session->setFlash(__('The pretty could not be saved. Please, try again.'));
			}
		}

		if (empty($this->request->data)) {
			$this->request->data = $this->Pretty->read(null, $id);
		}


	}
	
	public function delete($id= null){

		$this->Pretty->id = $id;

		if ($this->Pretty->delete()) {
			$this->Session->setFlash(__('The pretty has been deleted'));
		}
		else{
			$this->Session->setFlash(__('The pretty could not be saved. Please, try again.'));
		}

		$this->redirect('/admin/ManagePretty');

	}
	private function setParamsPhotos($params = null){

		if(empty($params['Pretty']['pic_1']['name'])) {
			unset($params['Pretty']['pic_1']);
		}else{
			$params['Pretty']['pic_1'] = $this->upload_img_pretty($params['Pretty']['pic_1']);
		}

		if(empty($params['Pretty']['pic_2']['name'])) {
			unset($params['Pretty']['pic_2']);
		}else{
			$params['Pretty']['pic_2'] = $this->upload_img_pretty($params['Pretty']['pic_2']);
		}
		return $params;
	}
    // function upload img into /upload/secret/companies forder
    private function upload_img_pretty($img=null){

        if( !empty($img)){
            $path=  WWW_ROOT . '/upload/pretty/';
            $ext = explode(".",$img['name']);
            $img['name'] = $ext[0].'-'.rand(0,999999).'.'.$ext[sizeof($ext)-1];
            if(!file_exists($path)){
                mkdir($path, 0755, true);
            }
            move_uploaded_file($img['tmp_name'],$path .$img['name']);

            return $img['name'];
        }

    }


	public function UploadCsv()
	{
		if ($this->request->is('post')) {
			$csv = $this->Csv;
			//pr($csv->checkFile());die;
			if ($csv->checkFile()) {
				$file = $csv->getFile();
				if ($this->updateByCsv($file)) {
					$this->status = $this->status . 'アップロード完了しました';//upload success
					$this->set('status', $this->status);
					$this->redirect($this->referer());
				} else {

					$this->status = $this->status . 'アップロード失敗しました。再処理してください。';//upload not success
					$this->set('status', $this->status);
				}

			} else {
				$this->status = $this->status . 'アップロード失敗しました。再処理してください。';//File type must be *.csv
				$this->set('status', $this->status);
			}
		}
	}


	private  function updateByCsv($file= null){

		$obj = null;
		$obj = $this->changeCsvToObj($file);
		if(empty($obj))
		{
			return [];
		}

		//update to database
		$resultData = [];
		//pr($obj);
		foreach( $obj as $record)
		{
			$data = $this->updateToDatabase($record);
			$resultData[] = $data;
		}
		return $resultData;
	}

	private function changeCsvToObj($file)
	{
		$csv =  $this->Csv;
		$obj = $csv->convertToObject(
			[
				"0" => "pic_1",
				"1" => "pic_2",
				"2" => "nick_name",
				"3" => "birthday",
				"4" => "address",
				"5" => "name",
				"6" => "company_name",
				"7" => "point_pv",
				"8" => "gender"
			],
			$file
		);
		return $obj;
	}



	private function updateToDatabase($record= null)
	{
				if(empty($record['pic_1']) && empty($record['pic_2']) ){
					return;
				}
				$record['gender'] = (!empty($record['gender'])) ? $record['gender'] : "男";
				$record['point_pv'] = (!empty($record['point_pv'])) ? $record['point_pv'] : 0;
				$record['birthday'] = date('Y-m-d',strtotime($record['birthday']));
				$record['created']= date("Y-m-d H:i:s");
				$record['modified']= date("Y-m-d H:i:s");
				$this->Pretty->create();
				if($this->Pretty->save($record)) {

				}else{

					debug($this->Pretty->validationErrors); //show validationErrors
					debug($this->Pretty->getDataSource()->getLog(false, false)); //show last sql query
					die;
				}


	}

	
}
