<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class ManagePickupController extends AdminAppController {

	public $uses = array('PickUp');
	public $helpers = array('Paginator','Html');
    public $components = array('Session');
    public $paginate = array();

/**
 * This controller does not use a model
 *
 * @var array
 */
	public function beforeFilter() {
		parent::beforeFilter();
		$this->isPublic();
	}
    
    public function index()
    {
		$keywords = '';			
    	$currentPage = 1;
		$size = 20;

		$params = $this->request->query;
		$currentQueryString = '';
		$currentQueryString = $this->generateQueryString($params);
		$type = 0;
		if(!empty($params['type']))
		{
			$type = $params['type'];
		}

		if(!empty($params['page']))
		{
			$currentPage = $params['page'];
		}

		$pickup = $this->PickUp->getPickUp();
		//pr($pickup);die;
		$query = $this->setCondition($params);
		
		$count_query = "select count(*) as total from ( $query ) as temp";
		$paginate_query = $query . $this->pagination($currentPage, $size);

		//pr($paginate_query);die;
		$db = ConnectionManager::getDataSource('default');
		$arrListData = $db->query($paginate_query);

		$total = $db->query($count_query);
		$total = intval($total[0][0]['total']);

		if($total % $size == 0)
        {
            $pageCount = $total / $size;
        }
        else
        {
            $pageCount = intval($total / $size) + 1;
        }
		$this->set('params', $params);
        $this->set('type', $type);
		$this->set('pickup', $pickup);
		$this->set('arrListData', $arrListData);
		$this->set('pageCount', $pageCount);
		$this->set('currentQueryString', $currentQueryString);
		$this->set('currentPage', $currentPage);

    }

    public function pagination($page, $size)
	{
	    if($page)
	    	$skip = ($page-1)* $size;
	    else
	    	$skip = 0;
	    return " limit $skip, $size";
	}

	public function generateQueryString($params)
	{
		$currentUrl = '';

		if($params)
		{
			$count = 0;
			foreach ($params as $key => $value) {
				if($key != 'page')
				{
					if($count == 0)
						$currentUrl .= "?" . $key . "=" . $value;
					else
						$currentUrl .= "&" . $key . "=" . $value;
				}

				$count++;
			}
		}

		return $currentUrl;
	}

	public function replaceSqlString($string)
	{
		$string = str_replace("'","\'",$string);
		return $string;
	}

	public function setCondition($params)
	{
		$query_contents = ' select id as id, title as title, image as image, created as created, 1 as type
					from contents 
					where status = 1 condition';
		$query_blogs = ' select id as id, title as title, pic as image, created as created, 2 as type
					from blogs
					where status = 1 condition';
		$union = ' union';

		$condition = '';
		if(!empty($params['keywords']))
		{
			$keywords = $params['keywords'];
			$keywords  = $this->replaceSqlString($keywords);

			$condition .= " and title like '%" . $keywords . "%'";
		}

		$query_contents = str_replace('condition', $condition, $query_contents);
		$query_blogs = str_replace('condition', $condition, $query_blogs);

		if(!empty($params['type']))
		{
			$type = $params['type'];

			if($type == 1)
			{
				$query = $query_contents;
			}
			else
			{
				$query = $query_blogs;
			}
		}
		else
		{
			$query = $query_contents . $union . $query_blogs;
		}
		return $query;
	}


	public function edit($type = null, $id = null)
	{
		if($type && $id)
		{
			$pickup = $this->PickUp->find('first');
			
			if(!empty($pickup['PickUp']['id']))
			{
				$this->PickUp->id = $pickup['PickUp']['id'];
				$data = array('type' => $type, 'article_id' => $id);

				$this->PickUp->save($data);

				return $this->redirect('/admin/ManagePickup');
			}
		}
	}
}
